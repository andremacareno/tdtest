LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE    := avutil 

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
    LOCAL_SRC_FILES := ./ffmpeg/armv7-a/libavutil.a
else
    ifeq ($(TARGET_ARCH_ABI),armeabi)
	LOCAL_SRC_FILES := ./ffmpeg/armv5te/libavutil.a
    else
        ifeq ($(TARGET_ARCH_ABI),x86)
	    LOCAL_SRC_FILES := ./ffmpeg/i686/libavutil.a
        endif
    endif
endif

include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE    := avformat

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
    LOCAL_SRC_FILES := ./ffmpeg/armv7-a/libavformat.a
else
    ifeq ($(TARGET_ARCH_ABI),armeabi)
	LOCAL_SRC_FILES := ./ffmpeg/armv5te/libavformat.a
    else
        ifeq ($(TARGET_ARCH_ABI),x86)
	    LOCAL_SRC_FILES := ./ffmpeg/i686/libavformat.a
        endif
    endif
endif

include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE    := avcodec 

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
    LOCAL_SRC_FILES := ./ffmpeg/armv7-a/libavcodec.a
else
    ifeq ($(TARGET_ARCH_ABI),armeabi)
	LOCAL_SRC_FILES := ./ffmpeg/armv5te/libavcodec.a
    else
        ifeq ($(TARGET_ARCH_ABI),x86)
	    LOCAL_SRC_FILES := ./ffmpeg/i686/libavcodec.a
        endif
    endif
endif

include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE 	:= opus_backport
LOCAL_CFLAGS 	:= -w -std=gnu99 -O2 -DNULL=0 -DSOCKLEN_T=socklen_t -DLOCALE_NOT_USED -D_LARGEFILE_SOURCE=1 -D_FILE_OFFSET_BITS=64
LOCAL_CFLAGS 	+= -Drestrict='' -D__EMX__ -DOPUS_BUILD -DFIXED_POINT -DUSE_ALLOCA -DHAVE_LRINT -DHAVE_LRINTF -fno-math-errno
LOCAL_CFLAGS 	+= -DANDROID_NDK -DDISABLE_IMPORTGL -fno-strict-aliasing -fprefetch-loop-arrays -DAVOID_TABLES -DANDROID_TILE_BASED_DECODE -DANDROID_ARMV6_IDCT -ffast-math
LOCAL_CPPFLAGS 	:= -DBSD=1 -ffast-math -O2 -funroll-loops
LOCAL_LDLIBS 	:= -ljnigraphics -llog
ifeq ($(TARGET_ARCH_ABI),armeabi)
	LOCAL_ARM_MODE  := thumb
else
	LOCAL_ARM_MODE  := arm
endif

LOCAL_SRC_FILES     := \
./opus/src/opus.c \
./opus/src/opus_decoder.c \
./opus/src/opus_encoder.c \
./opus/src/opus_multistream.c \
./opus/src/opus_multistream_encoder.c \
./opus/src/opus_multistream_decoder.c \
./opus/src/repacketizer.c \
./opus/src/analysis.c \
./opus/src/mlp.c \
./opus/src/mlp_data.c

LOCAL_SRC_FILES     += \
./opus/silk/CNG.c \
./opus/silk/code_signs.c \
./opus/silk/init_decoder.c \
./opus/silk/decode_core.c \
./opus/silk/decode_frame.c \
./opus/silk/decode_parameters.c \
./opus/silk/decode_indices.c \
./opus/silk/decode_pulses.c \
./opus/silk/decoder_set_fs.c \
./opus/silk/dec_API.c \
./opus/silk/enc_API.c \
./opus/silk/encode_indices.c \
./opus/silk/encode_pulses.c \
./opus/silk/gain_quant.c \
./opus/silk/interpolate.c \
./opus/silk/LP_variable_cutoff.c \
./opus/silk/NLSF_decode.c \
./opus/silk/NSQ.c \
./opus/silk/NSQ_del_dec.c \
./opus/silk/PLC.c \
./opus/silk/shell_coder.c \
./opus/silk/tables_gain.c \
./opus/silk/tables_LTP.c \
./opus/silk/tables_NLSF_CB_NB_MB.c \
./opus/silk/tables_NLSF_CB_WB.c \
./opus/silk/tables_other.c \
./opus/silk/tables_pitch_lag.c \
./opus/silk/tables_pulses_per_block.c \
./opus/silk/VAD.c \
./opus/silk/control_audio_bandwidth.c \
./opus/silk/quant_LTP_gains.c \
./opus/silk/VQ_WMat_EC.c \
./opus/silk/HP_variable_cutoff.c \
./opus/silk/NLSF_encode.c \
./opus/silk/NLSF_VQ.c \
./opus/silk/NLSF_unpack.c \
./opus/silk/NLSF_del_dec_quant.c \
./opus/silk/process_NLSFs.c \
./opus/silk/stereo_LR_to_MS.c \
./opus/silk/stereo_MS_to_LR.c \
./opus/silk/check_control_input.c \
./opus/silk/control_SNR.c \
./opus/silk/init_encoder.c \
./opus/silk/control_codec.c \
./opus/silk/A2NLSF.c \
./opus/silk/ana_filt_bank_1.c \
./opus/silk/biquad_alt.c \
./opus/silk/bwexpander_32.c \
./opus/silk/bwexpander.c \
./opus/silk/debug.c \
./opus/silk/decode_pitch.c \
./opus/silk/inner_prod_aligned.c \
./opus/silk/lin2log.c \
./opus/silk/log2lin.c \
./opus/silk/LPC_analysis_filter.c \
./opus/silk/LPC_inv_pred_gain.c \
./opus/silk/table_LSF_cos.c \
./opus/silk/NLSF2A.c \
./opus/silk/NLSF_stabilize.c \
./opus/silk/NLSF_VQ_weights_laroia.c \
./opus/silk/pitch_est_tables.c \
./opus/silk/resampler.c \
./opus/silk/resampler_down2_3.c \
./opus/silk/resampler_down2.c \
./opus/silk/resampler_private_AR2.c \
./opus/silk/resampler_private_down_FIR.c \
./opus/silk/resampler_private_IIR_FIR.c \
./opus/silk/resampler_private_up2_HQ.c \
./opus/silk/resampler_rom.c \
./opus/silk/sigm_Q15.c \
./opus/silk/sort.c \
./opus/silk/sum_sqr_shift.c \
./opus/silk/stereo_decode_pred.c \
./opus/silk/stereo_encode_pred.c \
./opus/silk/stereo_find_predictor.c \
./opus/silk/stereo_quant_pred.c

LOCAL_SRC_FILES     += \
./opus/silk/fixed/LTP_analysis_filter_FIX.c \
./opus/silk/fixed/LTP_scale_ctrl_FIX.c \
./opus/silk/fixed/corrMatrix_FIX.c \
./opus/silk/fixed/encode_frame_FIX.c \
./opus/silk/fixed/find_LPC_FIX.c \
./opus/silk/fixed/find_LTP_FIX.c \
./opus/silk/fixed/find_pitch_lags_FIX.c \
./opus/silk/fixed/find_pred_coefs_FIX.c \
./opus/silk/fixed/noise_shape_analysis_FIX.c \
./opus/silk/fixed/prefilter_FIX.c \
./opus/silk/fixed/process_gains_FIX.c \
./opus/silk/fixed/regularize_correlations_FIX.c \
./opus/silk/fixed/residual_energy16_FIX.c \
./opus/silk/fixed/residual_energy_FIX.c \
./opus/silk/fixed/solve_LS_FIX.c \
./opus/silk/fixed/warped_autocorrelation_FIX.c \
./opus/silk/fixed/apply_sine_window_FIX.c \
./opus/silk/fixed/autocorr_FIX.c \
./opus/silk/fixed/burg_modified_FIX.c \
./opus/silk/fixed/k2a_FIX.c \
./opus/silk/fixed/k2a_Q16_FIX.c \
./opus/silk/fixed/pitch_analysis_core_FIX.c \
./opus/silk/fixed/vector_ops_FIX.c \
./opus/silk/fixed/schur64_FIX.c \
./opus/silk/fixed/schur_FIX.c

LOCAL_SRC_FILES     += \
./opus/celt/bands.c \
./opus/celt/celt.c \
./opus/celt/celt_encoder.c \
./opus/celt/celt_decoder.c \
./opus/celt/cwrs.c \
./opus/celt/entcode.c \
./opus/celt/entdec.c \
./opus/celt/entenc.c \
./opus/celt/kiss_fft.c \
./opus/celt/laplace.c \
./opus/celt/mathops.c \
./opus/celt/mdct.c \
./opus/celt/modes.c \
./opus/celt/pitch.c \
./opus/celt/celt_lpc.c \
./opus/celt/quant_bands.c \
./opus/celt/rate.c \
./opus/celt/vq.c \
./opus/celt/arm/armcpu.c \
./opus/celt/arm/arm_celt_map.c

LOCAL_SRC_FILES     += \
./opus/ogg/bitwise.c \
./opus/ogg/framing.c \
./opus/opusfile/info.c \
./opus/opusfile/internal.c \
./opus/opusfile/opusfile.c \
./opus/opusfile/stream.c

LOCAL_C_INCLUDES    := \
./opus/include \
./opus/silk \
./opus/silk/fixed \
./opus/celt \
./opus/ \
./opus/opusfile \


LOCAL_SRC_FILES     += \
./jni.c \
./audio.c
include $(BUILD_SHARED_LIBRARY)


WEBP_CFLAGS := -Wall -DANDROID -DHAVE_MALLOC_H -DHAVE_PTHREAD -DWEBP_USE_THREAD

ifeq ($(APP_OPTIM),release)
  WEBP_CFLAGS += -finline-functions -ffast-math \
                 -ffunction-sections -fdata-sections
  ifeq ($(findstring clang,$(NDK_TOOLCHAIN_VERSION)),)
    WEBP_CFLAGS += -frename-registers -s
  endif
endif

ifneq ($(findstring armeabi-v7a, $(TARGET_ARCH_ABI)),)
  # Setting LOCAL_ARM_NEON will enable -mfpu=neon which may cause illegal
  # instructions to be generated for armv7a code. Instead target the neon code
  # specifically.
  NEON := c.neon
  USE_CPUFEATURES := yes
else
  NEON := c
endif

dec_srcs := \
    ./libwebp/src/dec/alpha.c \
    ./libwebp/src/dec/buffer.c \
    ./libwebp/src/dec/frame.c \
    ./libwebp/src/dec/idec.c \
    ./libwebp/src/dec/io.c \
    ./libwebp/src/dec/quant.c \
    ./libwebp/src/dec/tree.c \
    ./libwebp/src/dec/vp8.c \
    ./libwebp/src/dec/vp8l.c \
    ./libwebp/src/dec/webp.c \

demux_srcs := \
    ./libwebp/src/demux/anim_decode.c \
    ./libwebp/src/demux/demux.c \

dsp_dec_srcs := \
    ./libwebp/src/dsp/alpha_processing.c \
    ./libwebp/src/dsp/alpha_processing_mips_dsp_r2.c \
    ./libwebp/src/dsp/alpha_processing_sse2.c \
    ./libwebp/src/dsp/alpha_processing_sse41.c \
    ./libwebp/src/dsp/argb.c \
    ./libwebp/src/dsp/argb_mips_dsp_r2.c \
    ./libwebp/src/dsp/argb_sse2.c \
    ./libwebp/src/dsp/cpu.c \
    ./libwebp/src/dsp/dec.c \
    ./libwebp/src/dsp/dec_clip_tables.c \
    ./libwebp/src/dsp/dec_mips32.c \
    ./libwebp/src/dsp/dec_mips_dsp_r2.c \
    ./libwebp/src/dsp/dec_neon.$(NEON) \
    ./libwebp/src/dsp/dec_sse2.c \
    ./libwebp/src/dsp/dec_sse41.c \
    ./libwebp/src/dsp/filters.c \
    ./libwebp/src/dsp/filters_mips_dsp_r2.c \
    ./libwebp/src/dsp/filters_sse2.c \
    ./libwebp/src/dsp/lossless.c \
    ./libwebp/src/dsp/lossless_mips_dsp_r2.c \
    ./libwebp/src/dsp/lossless_neon.$(NEON) \
    ./libwebp/src/dsp/lossless_sse2.c \
    ./libwebp/src/dsp/rescaler.c \
    ./libwebp/src/dsp/rescaler_mips32.c \
    ./libwebp/src/dsp/rescaler_mips_dsp_r2.c \
    ./libwebp/src/dsp/rescaler_neon.$(NEON) \
    ./libwebp/src/dsp/rescaler_sse2.c \
    ./libwebp/src/dsp/upsampling.c \
    ./libwebp/src/dsp/upsampling_mips_dsp_r2.c \
    ./libwebp/src/dsp/upsampling_neon.$(NEON) \
    ./libwebp/src/dsp/upsampling_sse2.c \
    ./libwebp/src/dsp/yuv.c \
    ./libwebp/src/dsp/yuv_mips32.c \
    ./libwebp/src/dsp/yuv_mips_dsp_r2.c \
    ./libwebp/src/dsp/yuv_sse2.c \

dsp_enc_srcs := \
    ./libwebp/src/dsp/cost.c \
    ./libwebp/src/dsp/cost_mips32.c \
    ./libwebp/src/dsp/cost_mips_dsp_r2.c \
    ./libwebp/src/dsp/cost_sse2.c \
    ./libwebp/src/dsp/enc.c \
    ./libwebp/src/dsp/enc_avx2.c \
    ./libwebp/src/dsp/enc_mips32.c \
    ./libwebp/src/dsp/enc_mips_dsp_r2.c \
    ./libwebp/src/dsp/enc_neon.$(NEON) \
    ./libwebp/src/dsp/enc_sse2.c \
    ./libwebp/src/dsp/enc_sse41.c \
    ./libwebp/src/dsp/lossless_enc.c \
    ./libwebp/src/dsp/lossless_enc_mips32.c \
    ./libwebp/src/dsp/lossless_enc_mips_dsp_r2.c \
    ./libwebp/src/dsp/lossless_enc_neon.$(NEON) \
    ./libwebp/src/dsp/lossless_enc_sse2.c \
    ./libwebp/src/dsp/lossless_enc_sse41.c \

enc_srcs := \
    ./libwebp/src/enc/alpha.c \
    ./libwebp/src/enc/analysis.c \
    ./libwebp/src/enc/backward_references.c \
    ./libwebp/src/enc/config.c \
    ./libwebp/src/enc/cost.c \
    ./libwebp/src/enc/delta_palettization.c \
    ./libwebp/src/enc/filter.c \
    ./libwebp/src/enc/frame.c \
    ./libwebp/src/enc/histogram.c \
    ./libwebp/src/enc/iterator.c \
    ./libwebp/src/enc/near_lossless.c \
    ./libwebp/src/enc/picture.c \
    ./libwebp/src/enc/picture_csp.c \
    ./libwebp/src/enc/picture_psnr.c \
    ./libwebp/src/enc/picture_rescale.c \
    ./libwebp/src/enc/picture_tools.c \
    ./libwebp/src/enc/quant.c \
    ./libwebp/src/enc/syntax.c \
    ./libwebp/src/enc/token.c \
    ./libwebp/src/enc/tree.c \
    ./libwebp/src/enc/vp8l.c \
    ./libwebp/src/enc/webpenc.c \

mux_srcs := \
    ./libwebp/src/mux/anim_encode.c \
    ./libwebp/src/mux/muxedit.c \
    ./libwebp/src/mux/muxinternal.c \
    ./libwebp/src/mux/muxread.c \

utils_dec_srcs := \
    ./libwebp/src/utils/bit_reader.c \
    ./libwebp/src/utils/color_cache.c \
    ./libwebp/src/utils/filters.c \
    ./libwebp/src/utils/huffman.c \
    ./libwebp/src/utils/quant_levels_dec.c \
    ./libwebp/src/utils/random.c \
    ./libwebp/src/utils/rescaler.c \
    ./libwebp/src/utils/thread.c \
    ./libwebp/src/utils/utils.c \

utils_enc_srcs := \
    ./libwebp/src/utils/bit_writer.c \
    ./libwebp/src/utils/huffman_encode.c \
    ./libwebp/src/utils/quant_levels.c \


################################################################################
# libwebpdecoder

include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
    $(dec_srcs) \
    $(dsp_dec_srcs) \
    $(utils_dec_srcs) \

LOCAL_CFLAGS := $(WEBP_CFLAGS)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/libwebp/src

# prefer arm over thumb mode for performance gains
LOCAL_ARM_MODE := arm
LOCAL_STATIC_LIBRARIES := cpufeatures

LOCAL_MODULE := webpdecoder_static

include $(BUILD_STATIC_LIBRARY)
include $(CLEAR_VARS)
LOCAL_WHOLE_STATIC_LIBRARIES := webpdecoder_static
LOCAL_MODULE := webpdecoder
include $(BUILD_SHARED_LIBRARY)
################################################################################
# libwebp

include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
    $(dsp_enc_srcs) \
    $(enc_srcs) \
    $(utils_enc_srcs) \
	./libwebp/swig/libwebp_java_wrap.c \

LOCAL_CFLAGS := $(WEBP_CFLAGS)
LOCAL_C_INCLUDES := $(LOCAL_PATH)/libwebp/src

# prefer arm over thumb mode for performance gains
LOCAL_ARM_MODE := arm

LOCAL_WHOLE_STATIC_LIBRARIES := webpdecoder_static

LOCAL_MODULE := webp

include $(BUILD_SHARED_LIBRARY)

################################################################################

include $(CLEAR_VARS)
LOCAL_PRELINK_MODULE := false

LOCAL_MODULE 	:= ffmpeg_backport
LOCAL_CFLAGS 	:= -w -std=c11 -Os -DNULL=0 -DSOCKLEN_T=socklen_t -DLOCALE_NOT_USED -D_LARGEFILE_SOURCE=1 -D_FILE_OFFSET_BITS=64
LOCAL_CFLAGS 	+= -Drestrict='' -D__EMX__ -DOPUS_BUILD -DFIXED_POINT -DUSE_ALLOCA -DHAVE_LRINT -DHAVE_LRINTF -fno-math-errno
LOCAL_CFLAGS 	+= -DANDROID_NDK -DDISABLE_IMPORTGL -fno-strict-aliasing -fprefetch-loop-arrays -DAVOID_TABLES -DANDROID_TILE_BASED_DECODE -DANDROID_ARMV6_IDCT -ffast-math -D__STDC_CONSTANT_MACROS
LOCAL_CPPFLAGS 	:= -DBSD=1 -ffast-math -Os -funroll-loops -std=c++11
LOCAL_LDLIBS 	:= -ljnigraphics -llog -lz
LOCAL_STATIC_LIBRARIES := avformat avcodec avutil
ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
    LOCAL_ARM_MODE := arm
    LOCAL_CPPFLAGS += -DLIBYUV_NEON
    LOCAL_CFLAGS += -DLIBYUV_NEON
else
    ifeq ($(TARGET_ARCH_ABI),armeabi)
	LOCAL_ARM_MODE  := arm

    else
        ifeq ($(TARGET_ARCH_ABI),x86)
	    LOCAL_ARM_MODE  := arm
	    LOCAL_SRC_FILE += \
	    ./libyuv/source/row_x86.asm
        endif
    endif
endif

LOCAL_SRC_FILES     += \
./libyuv/source/compare_common.cc \
./libyuv/source/compare_gcc.cc \
./libyuv/source/compare_neon64.cc \
./libyuv/source/compare_win.cc \
./libyuv/source/compare.cc \
./libyuv/source/convert_argb.cc \
./libyuv/source/convert_from_argb.cc \
./libyuv/source/convert_from.cc \
./libyuv/source/convert_jpeg.cc \
./libyuv/source/convert_to_argb.cc \
./libyuv/source/convert_to_i420.cc \
./libyuv/source/convert.cc \
./libyuv/source/cpu_id.cc \
./libyuv/source/mjpeg_decoder.cc \
./libyuv/source/mjpeg_validate.cc \
./libyuv/source/planar_functions.cc \
./libyuv/source/rotate_any.cc \
./libyuv/source/rotate_argb.cc \
./libyuv/source/rotate_common.cc \
./libyuv/source/rotate_gcc.cc \
./libyuv/source/rotate_mips.cc \
./libyuv/source/rotate_neon64.cc \
./libyuv/source/rotate_win.cc \
./libyuv/source/rotate.cc \
./libyuv/source/row_any.cc \
./libyuv/source/row_common.cc \
./libyuv/source/row_gcc.cc \
./libyuv/source/row_mips.cc \
./libyuv/source/row_neon64.cc \
./libyuv/source/row_win.cc \
./libyuv/source/scale_any.cc \
./libyuv/source/scale_argb.cc \
./libyuv/source/scale_common.cc \
./libyuv/source/scale_gcc.cc \
./libyuv/source/scale_mips.cc \
./libyuv/source/scale_neon64.cc \
./libyuv/source/scale_win.cc \
./libyuv/source/scale.cc \
./libyuv/source/video_common.cc

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
    LOCAL_CFLAGS += -DLIBYUV_NEON
    LOCAL_SRC_FILES += \
        ./libyuv/source/compare_neon.cc.neon    \
        ./libyuv/source/rotate_neon.cc.neon     \
        ./libyuv/source/row_neon.cc.neon        \
        ./libyuv/source/scale_neon.cc.neon
endif

LOCAL_SRC_FILES     += \
./jni_gif.c \
./video.c \
./gifvideo.cpp \

LOCAL_C_INCLUDES    := \
./libyuv/include \
./ffmpeg/include
include $(BUILD_SHARED_LIBRARY)
$(call import-module,android/cpufeatures)
