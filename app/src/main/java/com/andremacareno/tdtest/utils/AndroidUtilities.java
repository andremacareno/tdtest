package com.andremacareno.tdtest.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.andremacareno.tdtest.TelegramApplication;

import java.util.Hashtable;

/**
 * Created by Andrew on 11.05.2015.
 */
public class AndroidUtilities {
    public static float density = TelegramApplication.sharedApplication().getResources().getDisplayMetrics().density;
    private static final Hashtable<String, Typeface> typefaceCache = new Hashtable<>();
    private static int statusBarHeight = 0;
    public static void showKeyboard(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager inputManager = (InputMethodManager)view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public static boolean isKeyboardShown(View view) {
        if (view == null) {
            return false;
        }
        InputMethodManager inputManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        return inputManager.isActive(view);
    }

    public static void hideKeyboard(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (!imm.isActive()) {
            return;
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public static int dp(int pixels)
    {
        if (pixels == 0) {
            return 0;
        }
        return (int)Math.ceil(density * pixels);
    }
    public static Typeface getTypeface(String assetPath) {
        synchronized (typefaceCache) {
            if (!typefaceCache.containsKey(assetPath)) {
                try {
                    Typeface t = Typeface.createFromAsset(TelegramApplication.sharedApplication().getAssets(), assetPath);
                    typefaceCache.put(assetPath, t);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
            return typefaceCache.get(assetPath);
        }
    }
    public static boolean haveCamera()
    {
        Context context = TelegramApplication.sharedApplication().getApplicationContext();
        PackageManager packageManager = context.getPackageManager();
        return packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }
    public static int getStatusBarHeight() {
        if(Build.VERSION.SDK_INT < 19)
            return 0;
        if(statusBarHeight != 0)
            return statusBarHeight;
        int resourceId = TelegramApplication.sharedApplication().getApplicationContext().getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = TelegramApplication.sharedApplication().getApplicationContext().getResources().getDimensionPixelSize(resourceId);
        }
        return statusBarHeight;
    }
}
