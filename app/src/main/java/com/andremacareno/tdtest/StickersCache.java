package com.andremacareno.tdtest;

import android.util.Log;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ConcurrentHashMap;

public class StickersCache {
    private static final String TAG = "StickersCache";
    private static ArrayList<TdApi.StickerSet> stickerSets = new ArrayList<>();
    private static ConcurrentHashMap<Long, TdApi.StickerSet> stickerSetsById = new ConcurrentHashMap<>();
    private static ArrayList<String> imageCacheKeys = new ArrayList<>();
    private static ConcurrentHashMap<Integer, TdApi.Sticker> stickersByFileId = new ConcurrentHashMap<>();
    public static ArrayList<TdApi.StickerSet> getStickerSets()
    {
        return stickerSets;
    }
    public static TdApi.Sticker getStickerByFileId(int fileId)
    {
        return stickersByFileId.get(fileId);
    }
    private static volatile boolean loadingStickers = false;
    private static volatile boolean stickersLoaded = false;
    private static volatile int stickerSetsRemaining;
    public static void checkStickers() {
        if (!loadingStickers && !stickersLoaded) {
            loadStickers();
        }
    }
    public static void invalidate()
    {
        try
        {
            stickersLoaded = false;
            stickerSets.clear();
            stickerSetsById.clear();
            imageCacheKeys.clear();
            stickersByFileId.clear();
            NotificationCenter.getInstance().postNotification(NotificationCenter.didStickersUpdated, null);
        }
        catch(Exception ignored) {}
    }
    private static void loadStickers()
    {
        if (loadingStickers) {
            return;
        }
        TdApi.GetStickerSets getStickerSetsFunc = new TdApi.GetStickerSets(true);
        TG.getClientInstance().send(getStickerSetsFunc, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() != TdApi.StickerSets.CONSTRUCTOR)
                    return;
                stickerSetsRemaining = ((TdApi.StickerSets) object).sets.length;
                for (TdApi.StickerSetInfo info : ((TdApi.StickerSets) object).sets) {
                    TdApi.GetStickerSet getStickerSetFunc = new TdApi.GetStickerSet(info.id);
                    TG.getClientInstance().send(getStickerSetFunc, new Client.ResultHandler() {
                        @Override
                        public void onResult(TdApi.TLObject object) {
                            if (object.getConstructor() != TdApi.StickerSet.CONSTRUCTOR)
                                return;
                            TdApi.StickerSet set = (TdApi.StickerSet) object;
                            for (TdApi.Sticker sticker : set.stickers)
                                stickersByFileId.put(sticker.sticker.id, sticker);
                            stickerSets.add(set);
                            stickerSetsById.put(set.id, set);
                            stickerSetsRemaining--;
                            if (stickerSetsRemaining <= 0) {
                                Collections.sort(stickerSets, new Comparator<TdApi.StickerSet>() {
                                    @Override
                                    public int compare(TdApi.StickerSet stickerSet, TdApi.StickerSet stickerSet2) {
                                        if(stickerSet.rating == stickerSet2.rating)
                                            return 0;
                                        else if(stickerSet.rating < stickerSet2.rating)
                                            return 1;
                                        return -1;
                                    }
                                });
                                if (BuildConfig.DEBUG)
                                    Log.d(TAG, stickerSets.toString());
                                loadingStickers = false;
                                stickersLoaded = true;
                                NotificationCenter.getInstance().postNotification(NotificationCenter.didStickersLoaded, null);
                            }
                        }
                    });
                }
            }
        });
    }
    private static void updateSetRating(final long id)
    {
        if (loadingStickers)
            return;
        TdApi.GetStickerSet getStickerSetFunc = new TdApi.GetStickerSet(id);
        TG.getClientInstance().send(getStickerSetFunc, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object == null || object.getConstructor() != TdApi.StickerSet.CONSTRUCTOR)
                    return;
                TdApi.StickerSet loaded = (TdApi.StickerSet) object;
                TdApi.StickerSet cached = stickerSetsById.get(id);
                if(cached == null)
                    return;
                cached.rating = loaded.rating;
                Collections.sort(stickerSets, new Comparator<TdApi.StickerSet>() {
                    @Override
                    public int compare(TdApi.StickerSet stickerSet, TdApi.StickerSet stickerSet2) {
                        if(stickerSet.rating == stickerSet2.rating)
                            return 0;
                        else if(stickerSet.rating < stickerSet2.rating)
                            return 1;
                        return -1;
                    }
                });
            }
        });
    }
    public static void didStickerSent(TdApi.Sticker sticker)
    {
        TdApi.StickerSet set = stickerSetsById.get(sticker.setId);
        if(set == null)
            return;
        updateSetRating(set.id);
    }
    public static void addImageCacheKey(String key)
    {
        imageCacheKeys.add(key);
    }
    public static void removeImageCacheKey(String key)
    {
        imageCacheKeys.remove(key);
    }
}