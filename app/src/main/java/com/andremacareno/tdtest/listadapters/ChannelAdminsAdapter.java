package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.fragments.EditChannelMembersFragment;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.ChatParticipantModel;
import com.andremacareno.tdtest.models.CurrentUserModel;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.viewcontrollers.ChannelAdminsListController;
import com.andremacareno.tdtest.views.AvatarImageView;
import com.andremacareno.tdtest.views.ChannelAdminsListItem;

import java.util.ArrayList;

/**
 * Created by andremacareno on 04/04/15.
 */
public class ChannelAdminsAdapter extends RecyclerView.Adapter<ChannelAdminsAdapter.ViewHolder> {
    private final String TAG = "ChatParticipantsAdapter";
    protected final ArrayList<ChatParticipantModel> adminsList;
    protected EditChannelMembersFragment.ChannelAdminsDelegate delegate;
    protected ChannelAdminsListController controller;
    public ChannelAdminsAdapter(ArrayList<ChatParticipantModel> data, EditChannelMembersFragment.ChannelAdminsDelegate delegate, ChannelAdminsListController controller)
    {
        setHasStableIds(true);
        this.adminsList = data;
        this.delegate = delegate;
        this.controller = controller;
    }
    @Override
    public long getItemId(int position)
    {
        synchronized(adminsList)
        {
            return adminsList.get(position).getId();
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View v = new ChannelAdminsListItem(viewGroup.getContext());
        v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        synchronized(adminsList)
        {
            ChatParticipantModel itm = adminsList.get(i);
            ((ChannelAdminsListItem) viewHolder.itemView).fillLayout(itm);
        }
    }
    @Override
    public void onViewAttachedToWindow(ViewHolder viewHolder)
    {
        UserModel itm;
        synchronized(adminsList) {
            itm = adminsList.get(viewHolder.getAdapterPosition());
        }
        FileModel photo = itm.getPhoto();
        AvatarImageView iv = viewHolder.item.getAvatarImageView();
        int fileId = photo.getFileId();
        if(fileId != 0)
            TelegramImageLoader.getInstance().loadImage(photo, itm.getPhotoKey(), iv, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), false);
        else
            TelegramImageLoader.getInstance().loadPlaceholder(itm.getId(), itm.getInitials(), itm.getPhotoPlaceholderKey(), iv, false);
        if(getItemCount() - viewHolder.getAdapterPosition() < 8 && controller != null)
            controller.loadMoreParticipants();
    }

    @Override
    public int getItemCount() {
        synchronized(adminsList)
        {
            if(adminsList.isEmpty() && controller != null)
                controller.loadMoreParticipants();
            return adminsList.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        public ChannelAdminsListItem item;
        public ViewHolder(View itemView) {
            super(itemView);
            this.item = (ChannelAdminsListItem) itemView;
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(delegate == null || getAdapterPosition() < 0 || item == null)
                return;
            synchronized(adminsList)
            {
                ChatParticipantModel user = adminsList.get(getAdapterPosition());
                delegate.openAdminProfile(user.getId());
            }
        }

        @Override
        public boolean onLongClick(View view) {
            try
            {
                synchronized(adminsList)
                {
                    ChatParticipantModel user = adminsList.get(getAdapterPosition());
                    if(delegate != null && delegate.isCreator() && CurrentUserModel.getInstance().getUserModel().getId() != user.getId()) {
                        delegate.didRemoveAlertRequested(user.getId());
                        return true;
                    }
                }
            }
            catch(Exception e) { e.printStackTrace(); }
            return false;
        }
    }
}
