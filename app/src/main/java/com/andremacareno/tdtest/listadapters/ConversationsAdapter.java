package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andremacareno.tdtest.models.ChatModel;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.ConversationsListViewController;
import com.andremacareno.tdtest.views.ChatListItemLayout;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import gnu.trove.set.hash.TLongHashSet;

/**
 * Created by andremacareno on 04/04/15.
 */
public class ConversationsAdapter extends RecyclerView.Adapter<ConversationsAdapter.ViewHolder> {
    private final String TAG = "ConversationsAdapter";
    private final ArrayList<ChatModel> dialogListRef;
    private WeakReference<ConversationsListViewController> controller;

    private TLongHashSet typingChats;

    public ConversationsAdapter(ArrayList<ChatModel> data, ConversationsListViewController c)
    {
        setHasStableIds(true);
        this.dialogListRef = data;
        this.controller = new WeakReference<ConversationsListViewController>(c);
        this.typingChats = new TLongHashSet();
    }
    @Override
    public long getItemId(int position)
    {
        synchronized(dialogListRef)
        {
            return dialogListRef.get(position).getId();
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        //View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chatlist_item, viewGroup, false);
        View v = new ChatListItemLayout(viewGroup.getContext());
        v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(68)));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        synchronized(dialogListRef) {
            ChatModel itm = dialogListRef.get(i);
            ((ChatListItemLayout) viewHolder.itemView).fillLayout(itm);
        }
    }
    @Override
    public void onViewAttachedToWindow(ViewHolder viewHolder)
    {
        int position = viewHolder.getAdapterPosition();
        if(position < 0)
            return;
        ChatModel itm;
        synchronized(dialogListRef)
        {
            itm = dialogListRef.get(position);
        }
        FileModel photo = itm.getPhoto();
        if(photo.getFileId() == 0)
            ((ChatListItemLayout) viewHolder.itemView).loadPlaceholder(itm);
        else
            ((ChatListItemLayout) viewHolder.itemView).loadAvatar(itm);
        ((ChatListItemLayout) viewHolder.itemView).checkStatus(itm);
        synchronized(dialogListRef)
        {
            if(dialogListRef.size() - position <= 8)
                controller.get().loadMoreConversations();
        }
    }
    @Override
    public int getItemCount() {
       synchronized(dialogListRef)
       {
           if(dialogListRef.size() == 0)
               controller.get().loadMoreConversations();
           return dialogListRef.size();
       }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(controller.get() != null)
                controller.get().didItemClicked(getAdapterPosition());
        }
    }
    public void setTyping(long chatId)
    {
        typingChats.add(chatId);
    }
    public void unsetTyping(long chatId)
    {
        typingChats.remove(chatId);
    }
}
