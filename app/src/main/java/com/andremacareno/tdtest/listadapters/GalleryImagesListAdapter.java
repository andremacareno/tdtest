package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.GalleryImageEntry;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.GalleryRecentPhotosController;
import com.andremacareno.tdtest.views.AttachMenuView;
import com.andremacareno.tdtest.views.GalleryImageItem;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.hash.TIntHashSet;

/**
 * Created by Andrew on 01.05.2015.
 */
public class GalleryImagesListAdapter extends RecyclerView.Adapter<GalleryImagesListAdapter.ViewHolder> {
    private TIntObjectMap<GalleryImageEntry> idToEntry;
    private ArrayList<GalleryImageEntry> entries;
    private TIntHashSet selectedIds = new TIntHashSet();
    private AttachMenuView.AttachMenuDelegate delegate;
    private WeakReference<GalleryRecentPhotosController> controller;
    public GalleryImagesListAdapter(GalleryRecentPhotosController c, TIntObjectMap<GalleryImageEntry> entryMap, ArrayList<GalleryImageEntry> data)
    {
        setHasStableIds(true);
        this.controller = new WeakReference<GalleryRecentPhotosController>(c);
        this.idToEntry = entryMap;
        this.entries = data;
    }
    public void setDelegate(AttachMenuView.AttachMenuDelegate delegateRef) { this.delegate = delegateRef; }
    @Override
    public long getItemId(int position)
    {
        return entries.get(position).getThumbId();
    }
    @Override
    public GalleryImagesListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = new GalleryImageItem(parent.getContext());
        v.setPadding(AndroidUtilities.dp(4), AndroidUtilities.dp(4), AndroidUtilities.dp(4), AndroidUtilities.dp(4));
        v.setLayoutParams(new LinearLayout.LayoutParams(AndroidUtilities.dp(104), AndroidUtilities.dp(104)));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GalleryImagesListAdapter.ViewHolder holder, int position) {
        GalleryImageEntry entry = entries.get(position);
        if(selectedIds.contains(entry.getThumbId()))
            holder.item.markSelected(false);
        else
            holder.item.markUnselected(false);
    }
    @Override
    public void onViewAttachedToWindow(ViewHolder holder) {
        int position = holder.getAdapterPosition();
        if(position < 0)
            return;
        GalleryImageEntry entry = entries.get(position);
        TelegramImageLoader.getInstance().loadGalleryThumb(entry, holder.item.getImageView());
    }

    @Override
    public int getItemCount() {
        return entries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public GalleryImageItem item;
        public ViewHolder(View itemView) {
            super(itemView);
            this.item = (GalleryImageItem) itemView;
            item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(controller.get() != null)
                toggleSelection(getAdapterPosition(), this.item);
        }
    }
    private void toggleSelection(int position, GalleryImageItem itm)
    {
        if(position < 0)
            return;
        GalleryImageEntry entry = entries.get(position);
        if(selectedIds.contains(entry.getThumbId()))
        {
            selectedIds.remove(entry.getThumbId());
            itm.markUnselected(true);
            if(delegate != null)
                delegate.didQuickSendSelectionCountUpdated(selectedIds.size());
        }
        else
        {
            selectedIds.add(entry.getThumbId());
            itm.markSelected(true);
            if(delegate != null)
                delegate.didQuickSendSelectionCountUpdated(selectedIds.size());
        }
    }
    public TIntHashSet getSelectionIds()
    {
        return this.selectedIds;
    }
    public void clearSelection()
    {
        selectedIds.clear();
        if(delegate != null)
            delegate.didQuickSendSelectionCountUpdated(0);
        notifyItemRangeChanged(0, entries.size());
    }
}
