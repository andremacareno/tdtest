package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.fragments.SharedMediaFragment;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.SharedImageModel;
import com.andremacareno.tdtest.models.SharedMedia;
import com.andremacareno.tdtest.models.SharedMediaSectionModel;
import com.andremacareno.tdtest.models.SharedVideoModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.SharedMediaViewController;
import com.andremacareno.tdtest.views.SharedImageItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import gnu.trove.set.hash.TIntHashSet;

/**
 * Created by andremacareno on 19/08/15.
 */
public class SharedMediaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements SharedMediaViewController.MediaListAdapter {
    private static final int SHARED_IMAGE = 0;
    private static final int SHARED_VIDEO = 1;
    private static final int SECTION = 2;
    private ArrayList<SharedMedia> media;
    private SharedMediaViewController controller;
    private SharedMediaFragment.SharedImageDelegate delegate;

    //selection mode
    private boolean selectionModeEnabled = false;
    private TIntHashSet selectedIds = new TIntHashSet();

    public SharedMediaAdapter(SharedMediaViewController controllerRef)
    {
        controller = controllerRef;
        media = controller.getImages();
    }
    @Override
    public int getItemViewType(int position)
    {
        SharedMedia itm = media.get(position);
        if(itm.isDummy())
            return SECTION;
        SharedImageModel item = (SharedImageModel) itm;
        if(item.isVideo())
            return SHARED_VIDEO;
        return SHARED_IMAGE;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == SECTION)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sharedmedia_section, parent, false);
            return new SectionViewHolder(v);
        }
        View v = new SharedImageItem(parent.getContext());
        v.setLayoutParams(new ViewGroup.LayoutParams(AndroidUtilities.dp(121), AndroidUtilities.dp(121)));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position) == SECTION)
            return;
        SharedImageModel itm = (SharedImageModel) media.get(position);
        ((ViewHolder)holder).itm.isVideo(itm.isVideo());
        if(itm.isVideo())
            ((ViewHolder)holder).itm.setDuration(((SharedVideoModel) itm).getDuration());
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        int pos = holder.getAdapterPosition();
        if(pos >= 0)
        {
            int type = getItemViewType(pos);
            if(type == SECTION) {
                ((SectionViewHolder)holder).label.setText(((SharedMediaSectionModel) media.get(pos)).getSectionLabel());
                return;
            }
            SharedImageModel itm = (SharedImageModel) media.get(pos);
            if(selectionModeEnabled && selectedIds.contains(itm.getMessageId()))
                ((ViewHolder) holder).itm.check(false);
            else
                ((ViewHolder) holder).itm.uncheck(false, selectionModeEnabled);
            TelegramImageLoader.getInstance().loadImage(itm.getPreview().getPhoto(), CommonTools.makeFileKey(itm.getPreview().getPhoto().getFileId()), ((ViewHolder)holder).itm.getPreviewContainer(), itm.isVideo() ? TelegramImageLoader.POST_PROCESSING_BLUR : TelegramImageLoader.POST_PROCESSING_NOTHING, itm.getPreview().getWidth(), itm.getPreview().getHeight(), false);
            if((getItemCount() - pos) <= 5 && controller != null)
                controller.searchImages();
        }
    }

    @Override
    public int getItemCount() {
        if(media.size() == 0 && controller != null)
            controller.searchImages();
        return media.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public SharedImageItem itm;
        public ViewHolder(View itemView) {
            super(itemView);
            this.itm = (SharedImageItem) itemView;
            this.itm.setOnClickListener(this);
            this.itm.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if(position >= 0)
            {
                SharedImageModel img = (SharedImageModel) media.get(position);
                if(delegate != null) {
                    if(selectionModeEnabled)
                    {
                        if(selectedIds.contains(img.getMessageId()))
                        {
                            selectedIds.remove(img.getMessageId());
                            itm.uncheck(true, true);
                        }
                        else
                        {
                            selectedIds.add(img.getMessageId());
                            itm.check(true);
                        }
                        delegate.didSelectionCountChanged(selectedIds.size());
                    }
                    else
                    {
                        delegate.openPhoto(img);
                    }
                }
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if(selectionModeEnabled)
                return false;
            int position = getAdapterPosition();
            if(position < 0)
                return false;
            SharedImageModel img = (SharedImageModel) media.get(position);
            if(delegate != null)
            {
                selectionModeEnabled = true;
                selectedIds.add(img.getMessageId());
                delegate.enableSelectionMode();
                notifyDataSetChanged();
                itm.check(true);
            }
            return true;
        }
    }
    public class SectionViewHolder extends RecyclerView.ViewHolder {
        public TextView label;
        public SectionViewHolder(View itemView) {
            super(itemView);
            this.label = (TextView) itemView.findViewById(R.id.section_text);
            itemView.setClickable(false);
        }
    }
    public void setDelegate(SharedMediaFragment.SharedImageDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    @Override
    public int getSelectionCount() {
        return this.selectedIds.size();
    }
    @Override
    public int[] getSelectedMessages()
    {
        if(selectedIds.size() == 0)
            return null;
        int[] arr = new int[this.selectedIds.size()];
        this.selectedIds.toArray(arr);
        return arr;
    }

    @Override
    public List<SharedMedia> getSelectedModels() {
        if(controller == null || selectedIds.isEmpty())
            return null;
        SharedMedia[] media = new SharedMedia[getSelectionCount()];
        int[] keys = this.selectedIds.toArray();
        int i = 0;
        for(int selected : keys)
        {
            SharedMedia data = controller.getImageById(selected);
            media[i++] = data;
        }
        return Arrays.asList(media);
    }

    @Override
    public void disableSelectionMode()
    {
        selectionModeEnabled = false;
        this.selectedIds.clear();
        notifyDataSetChanged();
    }
}
