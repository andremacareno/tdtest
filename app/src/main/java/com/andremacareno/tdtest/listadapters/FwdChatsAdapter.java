package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.andremacareno.tdtest.viewcontrollers.FwdChatsController;
import com.andremacareno.tdtest.views.FwdChatGridItem;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

import gnu.trove.set.hash.TLongHashSet;

/**
 * Created by andremacareno on 04/04/15.
 */
public class FwdChatsAdapter extends RecyclerView.Adapter<FwdChatsAdapter.ViewHolder> {
    private final String TAG = "FwdChatsAdapter";
    private final ArrayList<TdApi.Chat> chats;
    private final ArrayList<TdApi.Chat> searchResults;
    private FwdChatsController.FwdChatsDelegate delegate;
    private final TLongHashSet selection = new TLongHashSet();
    private boolean selfForward = false;
    private long currentChat = 0;
    public FwdChatsAdapter(ArrayList<TdApi.Chat> data, ArrayList<TdApi.Chat> searchResults, FwdChatsController.FwdChatsDelegate delegateRef)
    {
        setHasStableIds(true);
        this.chats = data;
        this.searchResults = searchResults;
        this.delegate = delegateRef;
    }
    public void setCurrentChatId(long chatId)
    {
        this.currentChat = chatId;
    }
    @Override
    public long getItemId(int position)
    {
        if(delegate != null && !delegate.inSearchMode())
        {
            synchronized(chats)
            {
                return chats.get(position).id;
            }
        }
        else if(delegate != null)
        {
            synchronized(searchResults)
            {
                return searchResults.get(position).id;
            }
        }
        return 0;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        //View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chatlist_item, viewGroup, false);
        View v = new FwdChatGridItem(viewGroup.getContext());
        v.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        TdApi.Chat itm = null;
        if(delegate != null && delegate.inSearchMode())
        {
            synchronized(searchResults)
            {
                itm = searchResults.get(i);
            }
        }
        else if(delegate != null)
        {
            synchronized(chats) {
                itm = chats.get(i);
            }
        }
        if(itm != null)
            ((FwdChatGridItem) viewHolder.itemView).fillLayout(itm);
    }
    public void notifySearchModeStateChanged()
    {
        notifyDataSetChanged();
    }
    @Override
    public void onViewAttachedToWindow(ViewHolder viewHolder)
    {
        int position = viewHolder.getAdapterPosition();
        if(position < 0)
            return;
        TdApi.Chat itm = null;
        boolean loadMore = false;
        if(delegate != null && delegate.inSearchMode())
        {
            synchronized(searchResults)
            {
                itm = searchResults.get(position);
                loadMore = false;
            }
        }
        else if(delegate != null)
        {
            synchronized(chats)
            {
                itm = chats.get(position);
                if(chats.size() - position <= 8)
                    loadMore = true;
            }
        }
        if(itm != null)
        {
            if(selection.contains(itm.id) || (itm.id == currentChat && selfForward)) {
                ((FwdChatGridItem) viewHolder.itemView).check(false);
            }
            else {
                ((FwdChatGridItem) viewHolder.itemView).uncheck(false);
            }
            TdApi.File photo = itm.photo.small;
            if(photo.id == 0)
                ((FwdChatGridItem) viewHolder.itemView).loadPlaceholder(itm);
            else
                ((FwdChatGridItem) viewHolder.itemView).loadAvatar(itm);
        }
        if(loadMore && delegate != null)
            delegate.loadMoreConversations();
    }
    @Override
    public int getItemCount() {
       if(delegate != null && delegate.inSearchMode())
       {
           synchronized(searchResults)
           {
               return searchResults.size();
           }
       }
       else if(delegate != null)
        {
            synchronized(chats)
            {
               if(chats.size() == 0 && delegate != null)
                   delegate.loadMoreConversations();
               return chats.size();
            }
        }
        return 0;
    }
    public void clearSelection()
    {
        selection.clear();
        selfForward = false;
    }
    public boolean forwardToSelf() { return this.selfForward; }
    public long[] getSelection()
    {
        return selection.toArray();
    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(getAdapterPosition() < 0)
                return;
            TdApi.Chat chat = null;
            if(delegate != null && delegate.inSearchMode())
            {
                synchronized(searchResults)
                {
                    chat = searchResults.get(getAdapterPosition());
                }
            }
            else if(delegate != null)
            {
                synchronized(chats)
                {
                    chat = chats.get(getAdapterPosition());
                }
            }
            if(chat != null && (selection.contains(chat.id) || (chat.id == currentChat && selfForward))) {
                ((FwdChatGridItem) itemView).uncheck(true);
                if(chat.id == currentChat)
                    selfForward = false;
                else
                    selection.remove(chat.id);
            }
            else {
                ((FwdChatGridItem) itemView).check(true);
                if(chat.id == currentChat)
                    selfForward = true;
                else
                    selection.add(chat.id);

            }
            if(delegate != null && getAdapterPosition() >= 0)
                delegate.toggleSelection(getAdapterPosition());
        }
    }
}
