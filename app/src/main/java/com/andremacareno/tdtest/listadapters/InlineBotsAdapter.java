package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.images.AnimationView;
import com.andremacareno.tdtest.images.RecyclingImageView;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.InlineQueryResultWrap;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.views.InlineTextItemView;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

/**
 * Created by Andrew on 23.04.2015.
 */
public class InlineBotsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<InlineQueryResultWrap> results;
    private ResultSelectionListener selectionCallback;

    private final int INLINE_TEXT = 0;
    private final int INLINE_PHOTO = 1;
    private final int INLINE_ANIM = 2;

    public InlineBotsAdapter(ArrayList<InlineQueryResultWrap> resultsList)
    {
        this.results = resultsList;
    }
    @Override
    public int getItemViewType(int position)
    {
        TdApi.InlineQueryResult res = results.get(position).getResult();
        if(res.getConstructor() == TdApi.InlineQueryResultVideo.CONSTRUCTOR || res.getConstructor() == TdApi.InlineQueryResultArticle.CONSTRUCTOR)
            return INLINE_TEXT;
        else if(res.getConstructor() == TdApi.InlineQueryResultPhoto.CONSTRUCTOR || res.getConstructor() == TdApi.InlineQueryResultCachedPhoto.CONSTRUCTOR)
            return INLINE_PHOTO;
        else if(res.getConstructor() == TdApi.InlineQueryResultAnimatedGif.CONSTRUCTOR || res.getConstructor() == TdApi.InlineQueryResultAnimatedMpeg4.CONSTRUCTOR || res.getConstructor() == TdApi.InlineQueryResultCachedAnimation.CONSTRUCTOR)
            return INLINE_ANIM;
        return INLINE_TEXT;
    }
    public void setSelectionCallback(ResultSelectionListener callback)
    {
        this.selectionCallback = callback;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if(viewType == INLINE_PHOTO)
        {
            View v = new RecyclingImageView(viewGroup.getContext());
            v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT));
            int padding = AndroidUtilities.dp(2);
            v.setPadding(padding, padding, padding, padding);
            return new PhotoQueryResultHolder(v);
        }
        else if(viewType == INLINE_ANIM)
        {
            View v = new AnimationView(viewGroup.getContext());
            v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT));
            int padding = AndroidUtilities.dp(2);
            v.setPadding(padding, padding, padding, padding);
            return new AnimQueryResultHolder(v);
        }
        else
        {
            View v = new InlineTextItemView(viewGroup.getContext());
            v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            return new ArticleQueryResultHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int i) {
    }
    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder)
    {
        int position = holder.getAdapterPosition();
        if(position >= 0)
        {
            TdApi.InlineQueryResult item = results.get(position).getResult();
            if(item.getConstructor() == TdApi.InlineQueryResultVideo.CONSTRUCTOR)
                ((ArticleQueryResultHolder) holder).item.attachQueryResult((TdApi.InlineQueryResultVideo) item);
            else if(item.getConstructor() == TdApi.InlineQueryResultPhoto.CONSTRUCTOR || item.getConstructor() == TdApi.InlineQueryResultCachedPhoto.CONSTRUCTOR)
            {
                ImageView iv = ((PhotoQueryResultHolder) holder).item;
                TelegramImageLoader.getInstance().loadInlinePhoto(item, iv);
            }
            else if(item.getConstructor() == TdApi.InlineQueryResultAnimatedGif.CONSTRUCTOR || item.getConstructor() == TdApi.InlineQueryResultAnimatedMpeg4.CONSTRUCTOR || item.getConstructor() == TdApi.InlineQueryResultCachedAnimation.CONSTRUCTOR)
            {
                AnimationView av = ((AnimQueryResultHolder) holder).item;
                TelegramImageLoader.getInstance().loadInlineAnimation(item, av);
            }
            else
                ((ArticleQueryResultHolder) holder).item.attachQueryResult(item.getConstructor() == TdApi.InlineQueryResultArticle.CONSTRUCTOR ? (TdApi.InlineQueryResultArticle) item : null);
        }
        if((getItemCount() - position) < 5)
            selectionCallback.loadMore();
    }
    @Override
    public int getItemCount() {
        return results.size();
    }

    public abstract class InlineQueryHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        public InlineQueryHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void onClick(View v) {
            try
            {
                if(BuildConfig.DEBUG)
                    Log.d("InlineResultAdapter", "result clicked");
                InlineQueryResultWrap result = results.get(getAdapterPosition());
                if(result != null && selectionCallback != null)
                    selectionCallback.onResultSelected(result);
            }
            catch(Exception ignored) {}
        }
    }
    public class ArticleQueryResultHolder extends InlineQueryHolder
    {
        public InlineTextItemView item;
        public ArticleQueryResultHolder(View itemView) {
            super(itemView);
            item = (InlineTextItemView) itemView;
            itemView.setOnClickListener(this);
        }

    }
    public class PhotoQueryResultHolder extends InlineQueryHolder
    {
        public RecyclingImageView item;
        public PhotoQueryResultHolder(View itemView) {
            super(itemView);
            item = (RecyclingImageView) itemView;
            itemView.setOnClickListener(this);
        }
    }
    public class AnimQueryResultHolder extends InlineQueryHolder
    {
        public AnimationView item;
        public AnimQueryResultHolder(View itemView) {
            super(itemView);
            item = (AnimationView) itemView;
            itemView.setOnClickListener(this);
        }
    }

    public interface ResultSelectionListener
    {
        public void onResultSelected(InlineQueryResultWrap result);
        public void loadMore();
    }
}
