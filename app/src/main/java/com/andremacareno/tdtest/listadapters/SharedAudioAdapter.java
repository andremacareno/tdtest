package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.fragments.SharedMediaFragment;
import com.andremacareno.tdtest.models.AudioTrackModel;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.SharedMedia;
import com.andremacareno.tdtest.models.SharedMediaSectionModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.SharedMediaViewController;
import com.andremacareno.tdtest.views.SharedAudioItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import gnu.trove.set.hash.TIntHashSet;

/**
 * Created by andremacareno on 19/08/15.
 */
public class SharedAudioAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements SharedMediaViewController.MediaListAdapter {
    private static final int SHARED_AUDIO = 0;
    private static final int SECTION = 1;
    private ArrayList<SharedMedia> media;
    private SharedMediaViewController controller;
    private SharedMediaFragment.SharedAudioDelegate delegate;

    //selection mode
    private boolean selectionModeEnabled = false;
    private TIntHashSet selectedIds = new TIntHashSet();

    public SharedAudioAdapter(SharedMediaViewController controllerRef)
    {
        controller = controllerRef;
        media = controller.getAudio();
    }
    @Override
    public int getItemViewType(int position)
    {
        SharedMedia itm = media.get(position);
        if(itm.isDummy())
            return SECTION;
        return SHARED_AUDIO;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == SECTION)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sharedmedia_section, parent, false);
            return new SectionViewHolder(v);
        }
        View v = new SharedAudioItem(parent.getContext());
        v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(50)));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position) == SECTION)
            return;
        final AudioTrackModel msg = (AudioTrackModel) media.get(position);
        ((ViewHolder)holder).itm.setTrackName(msg.getSongName());
        ((ViewHolder)holder).itm.setBandName(msg.getSongPerformer());
        ((ViewHolder)holder).itm.getDownloadButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FileModel.DownloadState state = msg.getTrack().getDownloadState();
                if(state == FileModel.DownloadState.EMPTY)
                    ((ViewHolder)holder).itm.startDownload();
                else if(state == FileModel.DownloadState.LOADING)
                    ((ViewHolder)holder).itm.cancelDownload();
                else if(state == FileModel.DownloadState.LOADED && delegate != null) {
                    delegate.requestPlayTrack(msg);
                }
            }
        });
        ((ViewHolder)holder).itm.setAttachedMsgId(msg.getMessageId());
        ((ViewHolder)holder).itm.attachDelegateToFile(msg.getTrack());
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        int pos = holder.getAdapterPosition();
        if(pos >= 0)
        {
            int type = getItemViewType(pos);
            if(type == SECTION) {
                ((SectionViewHolder)holder).label.setText(((SharedMediaSectionModel) media.get(pos)).getSectionLabel());
                return;
            }
            AudioTrackModel itm = (AudioTrackModel) media.get(pos);
            ((ViewHolder)holder).itm.setSelectionMode(selectionModeEnabled);
            if(selectionModeEnabled && selectedIds.contains(itm.getMessageId()))
                ((ViewHolder) holder).itm.check(false);
            else if(selectionModeEnabled)
                ((ViewHolder) holder).itm.uncheck(false);
            if((getItemCount() - pos) <= 5 && controller != null)
                controller.searchAudio();
        }
    }

    @Override
    public int getItemCount() {
        if(media.size() == 0 && controller != null)
            controller.searchAudio();
        return media.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public SharedAudioItem itm;
        public ViewHolder(View itemView) {
            super(itemView);
            this.itm = (SharedAudioItem) itemView;
            this.itm.setOnClickListener(this);
            this.itm.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if(position >= 0)
            {
                AudioTrackModel audio = (AudioTrackModel) media.get(position);
                if(delegate != null) {
                    if(selectionModeEnabled)
                    {
                        if(selectedIds.contains(audio.getMessageId()))
                        {
                            selectedIds.remove(audio.getMessageId());
                            itm.uncheck(true);
                        }
                        else
                        {
                            selectedIds.add(audio.getMessageId());
                            itm.check(true);
                        }
                        delegate.didSelectionCountChanged(selectedIds.size());
                    }
                    else
                    {
                        AudioTrackModel msg = (AudioTrackModel) media.get(position);
                        FileModel.DownloadState state = msg.getTrack().getDownloadState();
                        if(state == FileModel.DownloadState.EMPTY)
                            itm.startDownload();
                        else if(state == FileModel.DownloadState.LOADING)
                            itm.cancelDownload();
                        else if(state == FileModel.DownloadState.LOADED && delegate != null) {
                            delegate.requestPlayTrack(msg);
                        }
                    }
                }
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if(selectionModeEnabled)
                return false;
            int position = getAdapterPosition();
            if(position < 0)
                return false;
            AudioTrackModel audio = (AudioTrackModel) media.get(position);
            if(delegate != null)
            {
                selectionModeEnabled = true;
                selectedIds.add(audio.getMessageId());
                delegate.enableSelectionMode();
                itm.setSelectionMode(true);
                itm.check(true);
                notifyDataSetChanged();
            }
            return true;
        }
    }
    public class SectionViewHolder extends RecyclerView.ViewHolder {
        public TextView label;
        public SectionViewHolder(View itemView) {
            super(itemView);
            this.label = (TextView) itemView.findViewById(R.id.section_text);
            itemView.setClickable(false);
        }
    }
    public void setDelegate(SharedMediaFragment.SharedAudioDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    @Override
    public int getSelectionCount() {
        return this.selectedIds.size();
    }
    @Override
    public int[] getSelectedMessages()
    {
        if(selectedIds.size() == 0)
            return null;
        int[] arr = new int[this.selectedIds.size()];
        this.selectedIds.toArray(arr);
        return arr;
    }


    @Override
    public List<SharedMedia> getSelectedModels() {
        if(controller == null || selectedIds.isEmpty())
            return null;
        SharedMedia[] media = new SharedMedia[getSelectionCount()];
        int[] keys = this.selectedIds.toArray();
        int i = 0;
        for(int selected : keys)
        {
            SharedMedia data = controller.getAudioById(selected);
            media[i++] = data;
        }
        return Arrays.asList(media);
    }

    @Override
    public void disableSelectionMode()
    {
        selectionModeEnabled = false;
        this.selectedIds.clear();
        notifyDataSetChanged();
    }
}
