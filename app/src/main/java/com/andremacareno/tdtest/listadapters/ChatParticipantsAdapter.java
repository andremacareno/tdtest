package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.ChatParticipantsListController;
import com.andremacareno.tdtest.views.AvatarImageView;
import com.andremacareno.tdtest.views.ChatParticipantListItem;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by andremacareno on 04/04/15.
 */
public class ChatParticipantsAdapter extends RecyclerView.Adapter<ChatParticipantsAdapter.ViewHolder> {
    private final String TAG = "ChatParticipantsAdapter";
    protected ArrayList<UserModel> contactsListRef;
    protected WeakReference<ChatParticipantsListController> controller;
    public ChatParticipantsAdapter(ArrayList<UserModel> data, ChatParticipantsListController c)
    {
        setHasStableIds(true);
        this.contactsListRef = data;
        this.controller = new WeakReference<ChatParticipantsListController>(c);
    }
    @Override
    public long getItemId(int position)
    {
        return contactsListRef.get(position).getId();
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        //View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chatmembers_list_item, viewGroup, false);
        View v = new ChatParticipantListItem(viewGroup.getContext());
        v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(72)));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        UserModel itm = contactsListRef.get(i);
        ((ChatParticipantListItem) viewHolder.itemView).fillLayout(itm);
    }
    @Override
    public void onViewAttachedToWindow(ViewHolder viewHolder)
    {
        UserModel itm = contactsListRef.get(viewHolder.getAdapterPosition());
        FileModel photo = itm.getPhoto();
        AvatarImageView iv = ((ChatParticipantListItem) viewHolder.itemView).getAvatarImageView();
        int fileId = photo.getFileId();
        if(fileId != 0)
            TelegramImageLoader.getInstance().loadImage(photo, itm.getPhotoKey(), iv, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), false);
        else
            TelegramImageLoader.getInstance().loadPlaceholder(itm.getId(), itm.getInitials(), itm.getPhotoPlaceholderKey(), iv, false);
    }

    @Override
    public int getItemCount() {
       return contactsListRef.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(controller.get() != null)
                controller.get().didItemClicked(getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            if(controller.get() != null)
                controller.get().didItemLongClicked(getAdapterPosition());
            return true;
        }
    }
}
