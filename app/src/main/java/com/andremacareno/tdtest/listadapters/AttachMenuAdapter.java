package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.TelegramAction;
import com.andremacareno.tdtest.viewcontrollers.ChatAttachListController;

import java.lang.ref.WeakReference;

/**
 * Created by Andrew on 01.05.2015.
 */
public class AttachMenuAdapter extends RecyclerView.Adapter<AttachMenuAdapter.ViewHolder> {
    private TelegramAction[] actions;
    private int quickSendSelectionCount = 0;
    private WeakReference<ChatAttachListController> controller;
    public AttachMenuAdapter(ChatAttachListController c)
    {
        setHasStableIds(true);
        this.controller = new WeakReference<ChatAttachListController>(c);
        this.actions = new TelegramAction[] {
                 new TelegramAction(TelegramAction.ATTACH_TAKE_A_PHOTO, TelegramApplication.sharedApplication().getResources().getString(R.string.take_a_photo), R.drawable.ic_attach_photo),
                 new TelegramAction(TelegramAction.ATTACH_CHOOSE_FROM_GALLERY, null, R.drawable.ic_attach_gallery),
                 new TelegramAction(TelegramAction.ATTACH_CONTACT, TelegramApplication.sharedApplication().getString(R.string.contact), R.drawable.ic_contact_gray),
                 new TelegramAction(TelegramAction.ATTACH_AUDIO, TelegramApplication.sharedApplication().getString(R.string.audio), R.drawable.ic_attach_audio_gray)
        };
    }
    @Override
    public long getItemId(int position)
    {
        return actions[position].getAction();
    }
    @Override
    public AttachMenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(TelegramApplication.sharedApplication()).inflate(R.layout.attachment_action_itm, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AttachMenuAdapter.ViewHolder holder, int position) {
        TelegramAction act = actions[position];
        holder.img.setImageResource(act.getImageResource());
        if(position == 1)
        {
            if(quickSendSelectionCount == 0)
                act.setText(TelegramApplication.sharedApplication().getResources().getString(R.string.choose_from_gallery));
            else if(quickSendSelectionCount == 1)
                act.setText(TelegramApplication.sharedApplication().getResources().getString(R.string.quicksend_one_photo));
            else
                act.setText(String.format(TelegramApplication.sharedApplication().getResources().getString(R.string.quicksend_two_or_more), quickSendSelectionCount));
        }
        holder.txt.setText(act.getText());
    }

    @Override
    public int getItemCount() {
        return actions.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView img;
        public TextView txt;
        public ViewHolder(View itemView) {
            super(itemView);
            this.img = (ImageView) itemView.findViewById(R.id.action_icon);
            this.txt = (TextView) itemView.findViewById(R.id.action_text);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(controller.get() != null)
                controller.get().didActionChosen(actions[getAdapterPosition()]);
        }
    }
    public void updateQuickSendSelectionCount(int newCount)
    {
        this.quickSendSelectionCount = newCount;
        notifyItemChanged(1);
    }
    public int getQuickSendSelectionCount() { return this.quickSendSelectionCount; }
}
