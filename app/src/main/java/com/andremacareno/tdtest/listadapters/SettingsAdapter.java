package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.TelegramAction;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.SettingsListController;
import com.andremacareno.tdtest.views.SettingsItemView;

import java.lang.ref.WeakReference;

/**
 * Created by Andrew on 01.05.2015.
 */
public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.ViewHolder> {
    private TelegramAction[] actions;
    private WeakReference<SettingsListController> controller;
    public SettingsAdapter(SettingsListController c)
    {
        setHasStableIds(true);
        this.controller = new WeakReference<SettingsListController>(c);
        this.actions = new TelegramAction[] {
                 new TelegramAction(TelegramAction.SETTINGS_PASSCODE_LOCK, null, R.drawable.ic_setlock)
        };
    }
    @Override
    public long getItemId(int position)
    {
        return actions[position].getAction();
    }
    @Override
    public SettingsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = new SettingsItemView(parent.getContext());
        v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(54)));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SettingsAdapter.ViewHolder holder, int position) {
        TelegramAction act = actions[position];
        if(act.getAction() == TelegramAction.SETTINGS_PASSCODE_LOCK)
        {
            boolean passcodeLockEnabled = TelegramApplication.sharedApplication().sharedPreferences().getBoolean(Constants.PREFS_PASSCODE_LOCK_ENABLED, false);
            if(passcodeLockEnabled)
                ((SettingsItemView) holder.itemView).fillLayout(act.getImageResource(), R.string.passcode_lock, R.string.enabled, R.style.Settings_EnabledStatement);
            else
                ((SettingsItemView) holder.itemView).fillLayout(act.getImageResource(), R.string.passcode_lock, R.string.disabled, R.style.Settings_DisabledStatement);
        }
    }

    @Override
    public int getItemCount() {
        return actions.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(controller.get() != null)
                controller.get().didActionChosen(actions[getAdapterPosition()]);
        }
    }
}
