package com.andremacareno.tdtest.listadapters.menu;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.andremacareno.tdtest.R;

/**
 * Created by andremacareno on 31/08/15.
 */
public class MenuAdapter extends ArrayAdapter<MenuItem> {
    public MenuAdapter(Context context, MenuItem[] objects) {
        super(context, R.layout.menu_item, objects);
    }
}
