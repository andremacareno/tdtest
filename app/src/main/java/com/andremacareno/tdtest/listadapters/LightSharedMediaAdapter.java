package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.SharedImageModel;
import com.andremacareno.tdtest.models.SharedMedia;
import com.andremacareno.tdtest.models.SharedVideoModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.SharedMediaLightListViewController;
import com.andremacareno.tdtest.views.SharedImageItem;

import java.util.ArrayList;

import gnu.trove.set.hash.TIntHashSet;

/**
 * Created by andremacareno on 19/08/15.
 */
public class LightSharedMediaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int SHARED_IMAGE = 0;
    private static final int SHARED_VIDEO = 1;
    private ArrayList<SharedMedia> media;
    private SharedMediaLightListViewController controller;

    //selection mode
    private TIntHashSet selectedIds = new TIntHashSet();

    public LightSharedMediaAdapter(SharedMediaLightListViewController controllerRef)
    {
        controller = controllerRef;
        media = controller.getImages();
    }
    @Override
    public int getItemViewType(int position)
    {
        SharedMedia itm = media.get(position);
        SharedImageModel item = (SharedImageModel) itm;
        if(item.isVideo())
            return SHARED_VIDEO;
        return SHARED_IMAGE;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = new SharedImageItem(parent.getContext());
        v.setLayoutParams(new ViewGroup.LayoutParams(AndroidUtilities.dp(100), AndroidUtilities.dp(100
        )));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SharedImageModel itm = (SharedImageModel) media.get(position);
        ((ViewHolder)holder).itm.isVideo(itm.isVideo());
        if(itm.isVideo())
            ((ViewHolder)holder).itm.setDuration(((SharedVideoModel) itm).getDuration());
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        int pos = holder.getAdapterPosition();
        if(pos >= 0)
        {
            SharedImageModel itm = (SharedImageModel) media.get(pos);
            TelegramImageLoader.getInstance().loadImage(itm.getPreview().getPhoto(), CommonTools.makeFileKey(itm.getPreview().getPhoto().getFileId()), ((ViewHolder)holder).itm.getPreviewContainer(), itm.isVideo() ? TelegramImageLoader.POST_PROCESSING_BLUR : TelegramImageLoader.POST_PROCESSING_NOTHING, itm.getPreview().getWidth(), itm.getPreview().getHeight(), false);
            if((getItemCount() - pos) <= 5 && controller != null)
                controller.searchImages();
        }
    }

    @Override
    public int getItemCount() {
        if(media.size() == 0 && controller != null)
            controller.searchImages();
        return media.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public SharedImageItem itm;
        public ViewHolder(View itemView) {
            super(itemView);
            this.itm = (SharedImageItem) itemView;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(controller != null) {
                int position = getAdapterPosition();
                if(position >= 0)
                {
                    SharedMedia item = media.get(position);
                    controller.didItemClicked(item);
                }
            }
        }
    }
}
