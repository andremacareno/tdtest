package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.ChatParticipantModel;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.viewcontrollers.ChooseAdminsListController;
import com.andremacareno.tdtest.views.AvatarImageView;
import com.andremacareno.tdtest.views.ChooseAdminParticipantListItem;

import java.util.ArrayList;

/**
 * Created by andremacareno on 04/04/15.
 */
public class ChooseAdminsAdapter extends RecyclerView.Adapter<ChooseAdminsAdapter.ViewHolder> {
    private final String TAG = "ChatParticipantsAdapter";
    protected final ArrayList<ChatParticipantModel> contactsListRef;
    protected ChooseAdminsListController.ChooseAdminsDelegate delegate;
    private boolean areAllAdmins;
    public ChooseAdminsAdapter(ArrayList<ChatParticipantModel> data, ChooseAdminsListController.ChooseAdminsDelegate delegate, boolean areAllAdmins)
    {
        setHasStableIds(true);
        this.contactsListRef = data;
        this.delegate = delegate;
        this.areAllAdmins = areAllAdmins;
    }
    @Override
    public long getItemId(int position)
    {
        synchronized(contactsListRef)
        {
            return contactsListRef.get(position).getId();
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View v = new ChooseAdminParticipantListItem(viewGroup.getContext());
        v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        synchronized(contactsListRef)
        {
            ChatParticipantModel itm = contactsListRef.get(i);
            ((ChooseAdminParticipantListItem) viewHolder.itemView).fillLayout(itm);
        }
    }
    @Override
    public void onViewAttachedToWindow(ViewHolder viewHolder)
    {
        UserModel itm;
        synchronized(contactsListRef) {
            itm = contactsListRef.get(viewHolder.getAdapterPosition());
        }
        FileModel photo = itm.getPhoto();
        AvatarImageView iv = viewHolder.item.getAvatarImageView();
        int fileId = photo.getFileId();
        if(fileId != 0)
            TelegramImageLoader.getInstance().loadImage(photo, itm.getPhotoKey(), iv, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), false);
        else
            TelegramImageLoader.getInstance().loadPlaceholder(itm.getId(), itm.getInitials(), itm.getPhotoPlaceholderKey(), iv, false);
        if(BuildConfig.DEBUG)
            Log.d("Adapter", String.format("areAllAdmins = %s", String.valueOf(areAllAdmins)));
        viewHolder.item.checkbox.setVisibility(areAllAdmins ? View.GONE : View.VISIBLE);
    }
    public void setAllAdminsFlag(boolean allAdmins) {
        this.areAllAdmins = allAdmins;
        if(getItemCount() > 0)
            notifyItemRangeChanged(0, getItemCount());
    }
    @Override
    public int getItemCount() {
        synchronized(contactsListRef)
        {
            return contactsListRef.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ChooseAdminParticipantListItem item;
        public ViewHolder(View itemView) {
            super(itemView);
            this.item = (ChooseAdminParticipantListItem) itemView;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(delegate == null || getAdapterPosition() < 0 || item == null || areAllAdmins)
                return;
            boolean revoke = item.checkbox.isChecked();
            synchronized(contactsListRef)
            {
                ChatParticipantModel user = contactsListRef.get(getAdapterPosition());
                if(user.getCurrentRole().getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor())
                    return;
                item.checkbox.setChecked(!revoke);
                if(revoke)
                    delegate.revokeEditorRights(user.getId());
                else
                    delegate.grantEditorRights(user.getId());
            }
        }
    }
}
