package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.Country;
import com.andremacareno.tdtest.views.CountryListItem;

import java.util.ArrayList;

/**
 * Created by Andrew on 23.04.2015.
 */
public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.CountryViewHolder> {
    public interface CountryListDelegate
    {
        public void didCountrySelected(Country country);
    }
    ArrayList<Country> countries;
    private CountryListDelegate delegate;
    public CountriesAdapter(ArrayList<Country> data, CountryListDelegate delegateRef)
    {
        this.countries = data;
        this.delegate = delegateRef;
        setHasStableIds(true);
    }
    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        int resource = R.layout.country_list_item;
        View view = LayoutInflater.from(TelegramApplication.sharedApplication()).inflate(resource, viewGroup, false);
        return new CountryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CountryViewHolder holder, int i) {
        Country item = countries.get(i);
        holder.item.setCountryName(item.getFullName());
        holder.item.setCountryCode(item.getCodeWithPlus());
        item = null;
    }
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    @Override
    public int getItemCount() {
        return countries.size();
    }

    public class CountryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public CountryListItem item;
        public CountryViewHolder(View itemView) {
            super(itemView);
            item = (CountryListItem) itemView;
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if(position >= 0)
            {
                Country c = countries.get(position);
                if(delegate != null)
                    delegate.didCountrySelected(c);
            }
        }
    }
}
