package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.ChatSearchResult;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.GlobalSearchResult;
import com.andremacareno.tdtest.models.MessageSearchResult;
import com.andremacareno.tdtest.models.SectionSearchResult;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.views.SearchChatsListItem;
import com.andremacareno.tdtest.views.SearchMessagesListItem;
import com.andremacareno.tdtest.views.SearchPublicChatsListItem;

import java.util.ArrayList;

/**
 * Created by andremacareno on 04/04/15.
 */
public class GlobalSearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "ConversationsAdapter";
    private final ArrayList<GlobalSearchResult> resultsList;
    private int totalTypes = 0;
    private final int SECTION = totalTypes++;
    private final int MESSAGE = totalTypes++;
    private final int CHAT = totalTypes++;
    private final int PUBLIC_CHAT = totalTypes++;
    GlobalSearchResult.GlobalSearchDelegate delegate;

    public GlobalSearchAdapter(ArrayList<GlobalSearchResult> data, GlobalSearchResult.GlobalSearchDelegate delegateRef)
    {
        this.resultsList = data;
        this.delegate = delegateRef;
    }

    @Override
    public int getItemViewType(int position)
    {
        synchronized(resultsList)
        {
            GlobalSearchResult itm = resultsList.get(position);
            if(itm.getResultType() == GlobalSearchResult.ResultType.SECTION)
                return SECTION;
            else if(itm.getResultType() == GlobalSearchResult.ResultType.CHAT_CONTACT)
                return CHAT;
            else if(itm.getResultType() == GlobalSearchResult.ResultType.GLOBAL)
                return PUBLIC_CHAT;
            return MESSAGE;
        }
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        //View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chatlist_item, viewGroup, false);
        if(i == MESSAGE)
        {
            SearchMessagesListItem v = new SearchMessagesListItem(viewGroup.getContext());
            v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(68)));
            return new ViewHolder(v);
        }
        else if(i == SECTION)
        {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_section, viewGroup, false);
            return new SectionViewHolder(v);
        }
        else if(i == CHAT)
        {
            View v = new SearchChatsListItem(viewGroup.getContext());
            v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(68)));
            return new ChatViewHolder(v);
        }
        View v = new SearchPublicChatsListItem(viewGroup.getContext());
        v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(68)));
        return new PublicChatViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        synchronized(resultsList) {
            GlobalSearchResult itm = resultsList.get(i);
            if(itm.getResultType() == GlobalSearchResult.ResultType.MESSAGE)
                ((SearchMessagesListItem) viewHolder.itemView).fillLayout((MessageSearchResult) itm);
            else if(itm.getResultType() == GlobalSearchResult.ResultType.SECTION)
                ((TextView) viewHolder.itemView).setText(((SectionSearchResult) itm).getText());
            else if(itm.getResultType() == GlobalSearchResult.ResultType.CHAT_CONTACT)
                ((SearchChatsListItem) viewHolder.itemView).fillLayout((ChatSearchResult) itm);
            else if(itm.getResultType() == GlobalSearchResult.ResultType.GLOBAL)
                ((SearchPublicChatsListItem) viewHolder.itemView).fillLayout((ChatSearchResult) itm);
        }
    }
    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder viewHolder)
    {
        int position = viewHolder.getAdapterPosition();
        if(position < 0)
            return;
        GlobalSearchResult itm;
        synchronized(resultsList)
        {
            itm = resultsList.get(position);
        }
        if(itm.getResultType() == GlobalSearchResult.ResultType.MESSAGE)
        {
            MessageSearchResult cast = (MessageSearchResult) itm;
            FileModel photo = cast.getPhoto();
            if(photo.getFileId() == 0)
                ((SearchMessagesListItem) viewHolder.itemView).loadPlaceholder(cast);
            else
                ((SearchMessagesListItem) viewHolder.itemView).loadAvatar(cast);
            synchronized(resultsList)
            {
                if(resultsList.size() - position <= 8 && delegate != null)
                    delegate.loadMoreMessages();
            }
        }
        else if(itm.getResultType() == GlobalSearchResult.ResultType.GLOBAL || itm.getResultType() == GlobalSearchResult.ResultType.CHAT_CONTACT)
        {
            ChatSearchResult cast = (ChatSearchResult) itm;
            if(itm.getResultType() == GlobalSearchResult.ResultType.GLOBAL)
            {
                if(cast.getChatObj().photo.small.id == 0)
                    ((SearchPublicChatsListItem) viewHolder.itemView).loadPlaceholder(cast);
                else
                    ((SearchPublicChatsListItem) viewHolder.itemView).loadAvatar(cast);
            }
            else
            {
                if(cast.getChatObj().photo.small.id == 0)
                    ((SearchChatsListItem) viewHolder.itemView).loadPlaceholder(cast);
                else
                    ((SearchChatsListItem) viewHolder.itemView).loadAvatar(cast);
            }
            synchronized(resultsList)
            {
                if(resultsList.size() - position <= 8 && delegate != null)
                    delegate.loadMoreMessages();
            }
        }
    }
    @Override
    public int getItemCount() {
       synchronized(resultsList)
       {
           return resultsList.size();
       }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(getAdapterPosition() < 0)
                return;
            synchronized(resultsList)
            {
                GlobalSearchResult result = resultsList.get(getAdapterPosition());
                if(delegate != null && result.getResultType() == GlobalSearchResult.ResultType.MESSAGE)
                    delegate.didMessageSelected((MessageSearchResult) resultsList.get(getAdapterPosition()));
            }
        }
    }
    public class ChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ChatViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(getAdapterPosition() < 0)
                return;
            synchronized(resultsList)
            {
                GlobalSearchResult result = resultsList.get(getAdapterPosition());
                if(delegate != null && result.getResultType() == GlobalSearchResult.ResultType.CHAT_CONTACT)
                    delegate.didChatSelected((ChatSearchResult) resultsList.get(getAdapterPosition()));
            }
        }
    }
    public class PublicChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public PublicChatViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(getAdapterPosition() < 0)
                return;
            synchronized(resultsList)
            {
                GlobalSearchResult result = resultsList.get(getAdapterPosition());
                if(delegate != null && result.getResultType() == GlobalSearchResult.ResultType.GLOBAL)
                    delegate.didChatSelected((ChatSearchResult) resultsList.get(getAdapterPosition()));
            }
        }
    }
    public class SectionViewHolder extends RecyclerView.ViewHolder {
        public TextView label;
        public SectionViewHolder(View itemView) {
            super(itemView);
            this.label = (TextView) itemView.findViewById(R.id.section_text);
            itemView.setClickable(false);
        }
    }
}
