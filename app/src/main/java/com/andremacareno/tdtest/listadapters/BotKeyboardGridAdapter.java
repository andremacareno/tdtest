package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andremacareno.tdtest.Emoji;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.BotKeyboardModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.BotCmdListController;

/**
 * Created by Andrew on 23.04.2015.
 */
public class BotKeyboardGridAdapter extends RecyclerView.Adapter<BotKeyboardGridAdapter.BotKeyboardHolder> {
    private BotKeyboardModel kbdModel;
    private BotCmdListController.BotCmdListControllerDelegate delegate;
    public BotKeyboardGridAdapter()
    {
        this.kbdModel = null;
    }
    private int sixteenDp;
    public void attachKeyboardModel(BotKeyboardModel model)
    {
        if(kbdModel == null)
            this.kbdModel = model;
        else
            this.kbdModel.updateModel(model);
        notifyDataSetChanged();
    }
    public void setDelegate(BotCmdListController.BotCmdListControllerDelegate delegate)
    {
        this.delegate = delegate;
        sixteenDp = AndroidUtilities.dp(16);
    }
    @Override
    public BotKeyboardHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.kbd_item, viewGroup, false);
        return new BotKeyboardHolder(v);
    }

    @Override
    public void onBindViewHolder(BotKeyboardHolder holder, int i) {
        if(kbdModel == null)
            return;
        try
        {
            int columnCount = kbdModel.getRowSize(kbdModel.rowOfButton(i));
            if(columnCount >= 6)
                holder.itemView.setPadding(0, holder.itemView.getPaddingTop(), 0, holder.itemView.getPaddingBottom());
            else
                holder.itemView.setPadding(sixteenDp, holder.itemView.getPaddingTop(), sixteenDp, holder.itemView.getPaddingBottom());
        }
        finally {
            String str = kbdModel.getButtons().get(i);
            holder.textView.setText(Emoji.replaceEmoji(str, holder.textView.getPaint().getFontMetricsInt(), sixteenDp, false));
        }
    }
    @Override
    public int getItemCount() {
        if(kbdModel == null)
            return 0;
        return kbdModel.getButtons().size();
    }

    public class BotKeyboardHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public TextView textView;
        public BotKeyboardHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.button_text);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            if(pos >= 0)
            {
                String str = kbdModel.getButtons().get(pos);
                if(delegate != null)
                    delegate.didKeyboardButtonPressed(str, kbdModel.isOneTimeKeyboard());
            }
        }
    }
}
