package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.TelegramAction;
import com.andremacareno.tdtest.viewcontrollers.MessageMenuListController;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Andrew on 01.05.2015.
 */
public class MessageMenuAdapter extends RecyclerView.Adapter<MessageMenuAdapter.ViewHolder> {
    private ArrayList<TelegramAction> actions;
    private WeakReference<MessageMenuListController> controller;
    public MessageMenuAdapter(MessageMenuListController c, ArrayList<TelegramAction> actionList)
    {
        this.controller = new WeakReference<MessageMenuListController>(c);
        this.actions = actionList;
    }
    @Override
    public MessageMenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(TelegramApplication.sharedApplication()).inflate(R.layout.attachment_action_itm, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MessageMenuAdapter.ViewHolder holder, int position) {
        TelegramAction act = actions.get(position);
        holder.img.setImageResource(act.getImageResource());
        holder.txt.setText(act.getText());
    }

    @Override
    public int getItemCount() {
        return actions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView img;
        public TextView txt;
        public ViewHolder(View itemView) {
            super(itemView);
            this.img = (ImageView) itemView.findViewById(R.id.action_icon);
            this.txt = (TextView) itemView.findViewById(R.id.action_text);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(controller.get() != null && getAdapterPosition() >= 0)
                controller.get().didActionChosen(actions.get(getAdapterPosition()));
        }
    }
}
