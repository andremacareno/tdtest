package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andremacareno.tdtest.models.BotCommandModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.views.BotCommandListItem;

import java.util.ArrayList;

/**
 * Created by Andrew on 23.04.2015.
 */
public class BotCommandListAdapter extends RecyclerView.Adapter<BotCommandListAdapter.BotCommandHolder> {
    ArrayList<BotCommandModel> commands;
    private CommandSelectionListener cmdSelectionCallback;
    public BotCommandListAdapter(ArrayList<BotCommandModel> commandsRef)
    {
        this.commands = commandsRef;
    }
    public void setCommandSelectionCallback(CommandSelectionListener callback)
    {
        this.cmdSelectionCallback = callback;
    }
    @Override
    public BotCommandHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = new BotCommandListItem(viewGroup.getContext());
        v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(38)));
        return new BotCommandHolder(v);
    }

    @Override
    public void onBindViewHolder(BotCommandHolder holder, int i) {
        BotCommandModel item = commands.get(i);
        holder.item.fillLayout(item);
    }
    @Override
    public void onViewAttachedToWindow(BotCommandHolder holder)
    {
        int position = holder.getAdapterPosition();
        if(position >= 0)
            holder.item.updatePhoto(commands.get(position));
    }
    @Override
    public int getItemCount() {
        return commands.size();
    }

    public class BotCommandHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public BotCommandListItem item;
        public BotCommandHolder(View itemView) {
            super(itemView);
            item = (BotCommandListItem) itemView;
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            BotCommandModel cmd = commands.get(getAdapterPosition());
            if(cmd != null && cmdSelectionCallback != null)
                cmdSelectionCallback.onCommandSelected(cmd);
        }
    }
    public interface CommandSelectionListener
    {
        public void onCommandSelected(BotCommandModel cmd);
    }
}
