package com.andremacareno.tdtest.listadapters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.andremacareno.tdtest.AudioPlaybackService;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.MessageVoicePlayer;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.fragments.BaseChatFragment;
import com.andremacareno.tdtest.images.AnimationView;
import com.andremacareno.tdtest.images.RecyclingImageView;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.AnimationMessageModel;
import com.andremacareno.tdtest.models.AudioMessageModel;
import com.andremacareno.tdtest.models.BotHeaderMessageModel;
import com.andremacareno.tdtest.models.ContactMessageModel;
import com.andremacareno.tdtest.models.DateSectionMessageModel;
import com.andremacareno.tdtest.models.DocumentMessageModel;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.GeoPointMessageModel;
import com.andremacareno.tdtest.models.GroupChatChangePhotoMessageModel;
import com.andremacareno.tdtest.models.MessageModel;
import com.andremacareno.tdtest.models.NewMessagesMessageModel;
import com.andremacareno.tdtest.models.PhotoMessageModel;
import com.andremacareno.tdtest.models.PhotoSizeModel;
import com.andremacareno.tdtest.models.StickerMessageModel;
import com.andremacareno.tdtest.models.TextMessageModel;
import com.andremacareno.tdtest.models.VenueMessageModel;
import com.andremacareno.tdtest.models.VideoMessageModel;
import com.andremacareno.tdtest.models.VoiceMessageModel;
import com.andremacareno.tdtest.models.WebPageMessageModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.ChatController;
import com.andremacareno.tdtest.views.AudioDownloadButton;
import com.andremacareno.tdtest.views.AudioDownloadView;
import com.andremacareno.tdtest.views.AvatarImageView;
import com.andremacareno.tdtest.views.BotHeaderView;
import com.andremacareno.tdtest.views.DocumentDownloadView;
import com.andremacareno.tdtest.views.ImageUploadView;
import com.andremacareno.tdtest.views.MessageStatusView;
import com.andremacareno.tdtest.views.ReplyView;
import com.andremacareno.tdtest.views.ScrollButtonDelegate;
import com.andremacareno.tdtest.views.SwipeMenuLayout;
import com.andremacareno.tdtest.views.VideoDownloadView;
import com.andremacareno.tdtest.views.VoiceDownloadView;
import com.andremacareno.tdtest.views.WebPagePreview;

import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;
import java.util.List;

import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.hash.TIntHashSet;

/**
 * Created by andremacareno on 04/04/15.
 */
public class BaseChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private final String TAG = "BaseChatAdapter";
    private int totalMessages = 0;
    protected final int MESSAGE_TEXT = totalMessages++;
    protected final int MESSAGE_CHAT_SERVICE = totalMessages++;
    protected final int MESSAGE_DATE_SEPARATOR = totalMessages++;
    protected final int MESSAGE_UNREAD_SEPARATOR = totalMessages++;
    protected final int MESSAGE_TEXT_FORWARDED = totalMessages++;
    protected final int MESSAGE_PHOTO = totalMessages++;
    protected final int MESSAGE_CONTACT = totalMessages++;
    protected final int MESSAGE_AUDIO = totalMessages++;
    protected final int MESSAGE_GEOPOINT = totalMessages++;
    protected final int MESSAGE_DOCUMENT = totalMessages++;
    protected final int MESSAGE_STICKER = totalMessages++;
    protected final int MESSAGE_VIDEO = totalMessages++;
    protected final int MESSAGE_VOICE = totalMessages++;
    protected final int MESSAGE_WEB_PAGE = totalMessages++;
    protected final int MESSAGE_VENUE = totalMessages++;
    protected final int MESSAGE_BOT_HEADER = totalMessages++;
    protected final int MESSAGE_ANIMATION = totalMessages++;

    private BaseChatFragment.ChatDelegate chatDelegate;
    private final List<MessageModel> msgListRef;
    private WeakReference<ChatController> controller;
    private final int defaultMargin, increasedMargin, specialMargin, specialIncreasedMargin;
    private TIntObjectHashMap<MessageStatusView.UpdateChatReadDelegate> statusViewDelegates = new TIntObjectHashMap<MessageStatusView.UpdateChatReadDelegate>();
    private final TIntHashSet selection = new TIntHashSet();
    public BaseChatAdapter(List<MessageModel> data, ChatController c)
    {
        defaultMargin = AndroidUtilities.dp(40);
        increasedMargin = AndroidUtilities.dp(80);
        specialMargin = AndroidUtilities.dp(56);
        specialIncreasedMargin = AndroidUtilities.dp(96);
        this.msgListRef = data;
        this.controller = new WeakReference<>(c);
    }
    @Override
    public int getItemViewType(int position) {
        synchronized(msgListRef)
        {
            MessageModel model = msgListRef.get(position);
            if(model.isPseudoMessage())
            {
                if(model.getSuitableConstructor() == Constants.MODEL_DATE_SEPARATOR)
                    return MESSAGE_DATE_SEPARATOR;
                else if(model.getSuitableConstructor() == Constants.MODEL_UNREAD_SEPARATOR)
                    return MESSAGE_UNREAD_SEPARATOR;
                else if(model.getSuitableConstructor() == Constants.MODEL_BOT_CHAT_HEADER)
                    return MESSAGE_BOT_HEADER;
            }
            if(model.isChatServiceMessage())
                return MESSAGE_CHAT_SERVICE;
            else if(model.isForwarded() && model.getSuitableConstructor() == TdApi.MessageText.CONSTRUCTOR)
                return MESSAGE_TEXT_FORWARDED;
            if(model.getSuitableConstructor() == TdApi.MessageText.CONSTRUCTOR || model.getSuitableConstructor() == TdApi.MessageUnsupported.CONSTRUCTOR || model.getSuitableConstructor() == TdApi.MessageDeleted.CONSTRUCTOR)
                return MESSAGE_TEXT;
            else if(model.getSuitableConstructor() == TdApi.MessagePhoto.CONSTRUCTOR)
                return MESSAGE_PHOTO;
            else if(model.getSuitableConstructor() == TdApi.MessageContact.CONSTRUCTOR)
                return MESSAGE_CONTACT;
            else if(model.getSuitableConstructor() == TdApi.MessageAudio.CONSTRUCTOR)
                return MESSAGE_AUDIO;
            else if(model.getSuitableConstructor() == TdApi.MessageLocation.CONSTRUCTOR)
                return MESSAGE_GEOPOINT;
            else if(model.getSuitableConstructor() == TdApi.MessageDocument.CONSTRUCTOR)
                return MESSAGE_DOCUMENT;
            else if(model.getSuitableConstructor() == TdApi.MessageSticker.CONSTRUCTOR)
                return MESSAGE_STICKER;
            else if(model.getSuitableConstructor() == TdApi.MessageVoice.CONSTRUCTOR)
                return MESSAGE_VOICE;
            else if(model.getSuitableConstructor() == TdApi.MessageWebPage.CONSTRUCTOR)
                return MESSAGE_WEB_PAGE;
            else if(model.getSuitableConstructor() == TdApi.MessageVenue.CONSTRUCTOR)
                return MESSAGE_VENUE;
            else if(model.getSuitableConstructor() == TdApi.MessageAnimation.CONSTRUCTOR)
                return MESSAGE_ANIMATION;
            return MESSAGE_VIDEO;
        }
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(BuildConfig.DEBUG)
            Log.d(TAG, String.format("onCreateViewHolder: %d", viewType));
        if(viewType == MESSAGE_BOT_HEADER)
        {
            View v = new BotHeaderView(parent.getContext());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            lp.setMargins(AndroidUtilities.dp(16), 0, AndroidUtilities.dp(32), 0);
            lp.gravity = Gravity.CENTER_VERTICAL;
            v.setLayoutParams(lp);
            return new BotHeaderViewHolder(v);
        }
        int resource = 0;
        if(viewType == MESSAGE_DATE_SEPARATOR)
            resource = R.layout.chat_date_separator;
        else if(viewType == MESSAGE_UNREAD_SEPARATOR)
            resource = R.layout.new_messages_separator;
        else if(viewType == MESSAGE_TEXT)
            resource = R.layout.message_text;
        else if(viewType == MESSAGE_TEXT_FORWARDED)
            resource = R.layout.message_forwarded_text;
        else if(viewType == MESSAGE_PHOTO)
            resource = R.layout.message_photo;
        else if(viewType == MESSAGE_AUDIO)
            resource = R.layout.message_audio;
        else if(viewType == MESSAGE_CONTACT)
            resource = R.layout.message_contact;
        else if(viewType == MESSAGE_DOCUMENT)
            resource = R.layout.message_document;
        else if(viewType == MESSAGE_GEOPOINT)
            resource = R.layout.message_geopoint;
        else if(viewType == MESSAGE_CHAT_SERVICE)
            resource = R.layout.chat_service_msg;
        else if(viewType == MESSAGE_VIDEO)
            resource = R.layout.message_video;
        else if(viewType == MESSAGE_STICKER)
            resource = R.layout.message_sticker;
        else if(viewType == MESSAGE_VOICE)
            resource = R.layout.message_voice;
        else if(viewType == MESSAGE_WEB_PAGE)
            resource = R.layout.message_webpage;
        else if(viewType == MESSAGE_VENUE)
            resource = R.layout.message_venue;
        else if(viewType == MESSAGE_ANIMATION)
            resource = R.layout.message_animation;
        View view;
        if(viewType == MESSAGE_DATE_SEPARATOR || viewType == MESSAGE_UNREAD_SEPARATOR || viewType == MESSAGE_CHAT_SERVICE)
            view = LayoutInflater.from(TelegramApplication.sharedApplication()).inflate(resource, parent, false);
        else
        {
            view = new SwipeMenuLayout(parent.getContext());
            view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            View content = LayoutInflater.from(TelegramApplication.sharedApplication()).inflate(resource, (ViewGroup) view, false);
            View replyMenu = LayoutInflater.from(TelegramApplication.sharedApplication()).inflate(R.layout.quick_reply_menu, (ViewGroup) view, false);
            View shareMenu = LayoutInflater.from(TelegramApplication.sharedApplication()).inflate(R.layout.quick_share, (ViewGroup) view, false);
            content.setId(R.id.smContentView);
            replyMenu.setId(R.id.smRightMenuView);
            shareMenu.setId(R.id.smLeftMenuView);
            ((ViewGroup) view).addView(content);
            ((ViewGroup) view).addView(replyMenu);
            ((ViewGroup) view).addView(shareMenu);
            ((SwipeMenuLayout) view).didViewComplete();
        }
        if(viewType == MESSAGE_DATE_SEPARATOR)
            return new DateSeparatorViewHolder(view);
        else if(viewType == MESSAGE_UNREAD_SEPARATOR)
            return new UnreadSeparatorViewHolder(view);
        else if(viewType == MESSAGE_TEXT)
            return new MessageTextViewHolder(view);
        else if(viewType == MESSAGE_TEXT_FORWARDED)
            return new ForwardedMessageTextViewHolder(view);
        else if(viewType == MESSAGE_PHOTO)
            return new MessagePhotoViewHolder(view);
        else if(viewType == MESSAGE_AUDIO)
            return new MessageAudioViewHolder(view);
        else if(viewType == MESSAGE_CONTACT)
            return new MessageContactViewHolder(view);
        else if(viewType == MESSAGE_DOCUMENT)
            return new MessageDocumentViewHolder(view);
        else if(viewType == MESSAGE_GEOPOINT)
            return new MessageGeoPointViewHolder(view);
        else if(viewType == MESSAGE_CHAT_SERVICE)
            return new ServiceMsgViewHolder(view);
        else if(viewType == MESSAGE_VIDEO)
            return new MessageVideoViewHolder(view);
        else if(viewType == MESSAGE_STICKER)
            return new MessageStickerViewHolder(view);
        else if(viewType == MESSAGE_VOICE)
            return new MessageVoiceViewHolder(view);
        else if(viewType == MESSAGE_WEB_PAGE)
            return new MessageWebPageViewHolder(view);
        else if(viewType == MESSAGE_VENUE)
            return new MessageVenueViewHolder(view);
        else if(viewType == MESSAGE_ANIMATION)
            return new MessageAnimationViewHolder(view);
        return null;
    }
    @Override
    public int getItemCount() {
       if(msgListRef.isEmpty())
       {
           final ChatController c = getController();
           if(c != null) {
               c.checkLoading(c.start_message);
           }
       }
       return msgListRef.size();
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder)
    {
        int position = holder.getAdapterPosition();
        if(position < 0 || position >= getItemCount())
            return;
        int viewType = getItemViewType(position);
        if(viewType != MESSAGE_DATE_SEPARATOR && viewType != MESSAGE_UNREAD_SEPARATOR && viewType != MESSAGE_CHAT_SERVICE && viewType != MESSAGE_BOT_HEADER)
        {
            try
            {
                ((DefaultViewHolder) holder).onItemDetachFromWindow();
            }
            catch(Exception e) { e.printStackTrace(); }
        }
    }
    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder)
    {
        int position = holder.getAdapterPosition();
        if(position < 0 || position >= getItemCount())
            return;
        int viewType = getItemViewType(position);
        synchronized(msgListRef)
        {
            int itemsBelow = msgListRef.size() - position;
            ScrollButtonDelegate scrollButtonDelegate = getController().getScrollButtonDelegate();
            if(scrollButtonDelegate != null)
                scrollButtonDelegate.onChatScroll(itemsBelow, msgListRef.size());
            MessageModel itm = msgListRef.get(position);
            if(viewType == MESSAGE_CHAT_SERVICE && itm.getSuitableConstructor() == TdApi.MessageChatChangePhoto.CONSTRUCTOR)
            {
                GroupChatChangePhotoMessageModel cast = (GroupChatChangePhotoMessageModel) itm;
                if(cast.getNewChatPhoto() != null) {
                    ((ServiceMsgViewHolder) holder).newChatPhoto.setVisibility(View.VISIBLE);
                    TelegramImageLoader.getInstance().loadImage(cast.getNewChatPhoto(), cast.getNewChatPhotoKey(), ((ServiceMsgViewHolder) holder).newChatPhoto, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, AndroidUtilities.dp(60), AndroidUtilities.dp(60), false);
                }
                else
                    ((ServiceMsgViewHolder) holder).newChatPhoto.setVisibility(View.GONE);
            }
            else if(viewType == MESSAGE_CHAT_SERVICE)
                ((ServiceMsgViewHolder) holder).newChatPhoto.setVisibility(View.GONE);
            if(viewType == MESSAGE_BOT_HEADER)
            {
                ((BotHeaderViewHolder) holder).header.fillHeader((BotHeaderMessageModel) itm);
                if(getItemCount() == 1)
                    ((BotHeaderViewHolder) holder).header.centerView(true);
                else
                    ((BotHeaderViewHolder) holder).header.centerView(false);
            }
            else if(viewType == MESSAGE_DATE_SEPARATOR) {
                ((DateSeparatorViewHolder) holder).dateTextView.setText(((DateSectionMessageModel) itm).getHeaderDate());
            }
            if(!itm.isPseudoMessage() && (position >= msgListRef.size() - 5 || position <= 5))
                getController().checkLoading(msgListRef.get(position).getMessageId());
            if(itm.isPseudoMessage() || itm.isChatServiceMessage())
                return;
            DefaultViewHolder defaultHolder = (DefaultViewHolder) holder;
            defaultHolder.onPreDraw(itm);
        }
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(BuildConfig.DEBUG)
            Log.d(TAG, String.format("onBindViewHolder: position = %d; className = %s; itemCount = %d", position, holder.getClass().getName(), getItemCount()));
        if(holder == null)
            return;
        int viewType = getItemViewType(position);
        synchronized(msgListRef)
        {
            MessageModel itm = msgListRef.get(position);
            if(viewType == MESSAGE_UNREAD_SEPARATOR)
                ((UnreadSeparatorViewHolder) holder).unreadTextView.setText(((NewMessagesMessageModel) itm).getHeaderString());
            else if(viewType == MESSAGE_CHAT_SERVICE)
                ((ServiceMsgViewHolder) holder).serviceMessageText.setText(((TextMessageModel) itm).getText());
            else if(!itm.isPseudoMessage())
                ((DefaultViewHolder) holder).onDraw(itm);
            ChatController c = getController();
            if(c != null && (position >= getItemCount() - 5 || position <= 5))
                c.checkLoading(itm.getMessageId());
        }
    }

    protected ChatController getController() { return this.controller.get(); }

    public class DateSeparatorViewHolder extends RecyclerView.ViewHolder
    {
        public TextView dateTextView;
        public DateSeparatorViewHolder(View itemView) {
            super(itemView);
            this.dateTextView = (TextView) itemView.findViewById(R.id.date);
        }
    }
    public class UnreadSeparatorViewHolder extends RecyclerView.ViewHolder
    {
        public TextView unreadTextView;
        public UnreadSeparatorViewHolder(View itemView)
        {
            super(itemView);
            this.unreadTextView = (TextView) itemView.findViewById(R.id.new_msgs);
        }
    }
    public class BotHeaderViewHolder extends RecyclerView.ViewHolder
    {
        public BotHeaderView header;
        public BotHeaderViewHolder(View itemView) {
            super(itemView);
            this.header = (BotHeaderView) itemView;
        }
    }

    public abstract class DefaultViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        private AvatarImageView avatar;
        private TextView dateView;
        private TextView userNameView;
        private TextView viewsCountView;
        private MessageStatusView msgStatusView;
        private View.OnClickListener avatarClickListener, viaBotClickListener;
        private ReplyView replyView;
        private TextView viaBotStatic, viaBotDynamic;
        protected SwipeMenuLayout swipeContainer;
        protected View quickReplyButton, quickShareButton;
        protected View.OnClickListener selectionClickListener;
        private QuickActionClickListener quickReplyClick, quickShareClick;
        public DefaultViewHolder(View itemView) {
            super(itemView);
            swipeContainer = (SwipeMenuLayout) itemView;
            selectionClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!isInSelectionMode()) {
                        DefaultViewHolder.this.onClick(view);
                        return;
                    }
                    int pos = getAdapterPosition();
                    if(pos >= 0)
                    {
                        synchronized(msgListRef)
                        {
                            getController().selectMessage(msgListRef.get(pos).getMessageId());
                        }
                    }
                }
            };
            quickReplyButton = swipeContainer.getRightMenuView();
            quickShareButton = swipeContainer.getLeftMenuView();
            quickReplyClick = new QuickActionClickListener();
            quickShareClick = new QuickActionClickListener();
            quickReplyClick.reply = true;
            quickShareClick.reply = false;
            quickReplyButton.setOnClickListener(quickReplyClick);
            quickShareButton.setOnClickListener(quickShareClick);
            this.avatar = (AvatarImageView) itemView.findViewById(R.id.avatar);
            this.viewsCountView = (TextView) itemView.findViewById(R.id.msg_views);
            this.dateView = (TextView) itemView.findViewById(R.id.msg_date);
            this.userNameView = (TextView) itemView.findViewById(R.id.msg_author);
            this.msgStatusView = (MessageStatusView) itemView.findViewById(R.id.msg_status);
            this.replyView = (ReplyView) itemView.findViewById(R.id.reply_view);
            this.replyView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isInSelectionMode()) {
                        selectionClickListener.onClick(view);
                        return;
                    }
                    int pos = getAdapterPosition();
                    if(pos >= 0)
                    {
                        synchronized(msgListRef)
                        {
                            try
                            {
                                MessageModel msg = msgListRef.get(pos);
                                int replyTo = msg.getReplyInfo().getReplyToMessageId();
                                getController().requestHighlight(replyTo);
                                getController().jumpTo(replyTo);
                            }
                            catch(Exception ignored) {}
                        }
                    }
                }
            });
            this.replyView.setOnLongClickListener(this);
            this.viaBotStatic = (TextView) itemView.findViewById(R.id.msg_via_bot_static);
            this.viaBotDynamic = (TextView) itemView.findViewById(R.id.msg_via_bot_dynamic);
            this.avatarClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    synchronized(msgListRef)
                    {
                        int position = getAdapterPosition();
                        if(position >= 0)
                        {
                            if(!isInSelectionMode()) {
                                int userId = msgListRef.get(getAdapterPosition()).getFromId();
                                if(msgListRef.get(getAdapterPosition()).messageInChannelChat() && !msgListRef.get(getAdapterPosition()).messageInSupergroup())
                                    getController().didChannelAvatarClicked(msgListRef.get(getAdapterPosition()).getChatId());
                                else
                                    getController().didAvatarClicked(userId);
                            }
                            else
                            {
                                getController().selectMessage(msgListRef.get(getAdapterPosition()).getMessageId());
                            }
                        }
                    }
                }
            };
            this.viaBotClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    synchronized(msgListRef)
                    {
                        int position = getAdapterPosition();
                        if(position >= 0)
                        {
                            String username = msgListRef.get(getAdapterPosition()).getViaUsername();
                            if(username != null && username.length() > 1) {
                                if(!isInSelectionMode())
                                    getController().didViaBotClicked(username);
                                else
                                    getController().selectMessage(msgListRef.get(getAdapterPosition()).getMessageId());
                            }
                        }
                    }
                }
            };
            this.avatar.setOnLongClickListener(this);
            this.avatar.setOnClickListener(avatarClickListener);
            this.userNameView.setOnClickListener(avatarClickListener);
            this.userNameView.setOnLongClickListener(this);
            swipeContainer.getContentView().setOnClickListener(this);
            swipeContainer.getContentView().setOnLongClickListener(this);
        }
        public void onPreDraw(MessageModel itm)
        {
            boolean highlightRequested = getController() != null && getController().getHighlightRequest() == itm.getMessageId();
            itemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), (highlightRequested || selection.contains(itm.getMessageId())) ? R.color.chat_selection : R.color.transparent));
            if(highlightRequested && getController() != null)
                getController().onHighlight();
            if(chatDelegate != null && !chatDelegate.canSendMessage())
                swipeContainer.setDisableRightButton(true);
            else if(chatDelegate != null)
                swipeContainer.setDisableRightButton(false);
            FileModel photo = itm.getFromPhoto();
            int fileId = photo.getFileId();
            if(fileId != 0)
                TelegramImageLoader.getInstance().loadImage(photo, itm.getFromPhotoKey(), avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), false);
            else
                TelegramImageLoader.getInstance().loadPlaceholder(itm.getFromId(), itm.getFromInitials(), itm.getFromPlaceholderKey(), avatar, false);
            if(itm.getReplyInfo() != null) {
                adjustForReply(true);
                replyView.setVisibility(View.VISIBLE);
                replyView.fill(itm.getReplyInfo());
            }
            else {
                adjustForReply(false);
                replyView.setVisibility(View.GONE);
            }
            msgStatusView.attachMessage(itm);
            if(BuildConfig.DEBUG)
                Log.d(TAG, String.format("adding status delegate %d", itm.getMessageId()));
            statusViewDelegates.put(itm.getMessageId(), msgStatusView.getDelegate());
            msgStatusView.getDelegate().processUpdateChatRead(getController().getLastReadOutMessageId());
            quickReplyClick.attachedMsg = itm;
            quickShareClick.attachedMsg = itm;
        }
        public void onDraw(MessageModel itm)
        {
            if(!statusViewDelegates.containsKey(itm.getMessageId()))
                statusViewDelegates.put(itm.getMessageId(), msgStatusView.getDelegate());
            if(itm.messageInChannelChat() && !itm.messageInSupergroup()) {
                viewsCountView.setText(itm.getViewsCount());
                viewsCountView.setVisibility(View.VISIBLE);
            }
            else
            {
                viewsCountView.setVisibility(View.GONE);
            }
            dateView.setText(itm.getDate());
            userNameView.setText(itm.getFromUsername());
            if(itm.getViaUsername() != null && itm.getViaId() != 0)
            {
                viaBotStatic.setVisibility(View.VISIBLE);
                viaBotDynamic.setVisibility(View.VISIBLE);
                viaBotDynamic.setText(itm.getViaUsername());
                viaBotDynamic.setOnClickListener(viaBotClickListener);
            }
            else
            {
                viaBotDynamic.setVisibility(View.GONE);
                viaBotStatic.setVisibility(View.GONE);
            }
        }
        public void onItemDetachFromWindow()
        {
            quickReplyClick.attachedMsg = null;
            synchronized(msgListRef)
            {
                int pos = getAdapterPosition();
                if(pos >= 0)
                {
                    MessageModel msg = msgListRef.get(pos);
                    msgStatusView.attachMessage(null);
                    if(BuildConfig.DEBUG)
                        Log.d(TAG, String.format("removing status delegate %d", msg.getMessageId()));
                    statusViewDelegates.remove(msg.getMessageId());
                }
            }
        }
        @Override
        public void onClick(View v)
        {
            try
            {
                if(!isInSelectionMode())
                {
                    int position = getAdapterPosition();
                    if(position >= 0)
                    {
                        getController().didItemClicked(position);
                    }
                }
            }
            catch(Exception ignored) {}
        }
        @Override
        public boolean onLongClick(View v)
        {
            if(swipeContainer.isOpening() || swipeContainer.isOpen())
                return false;
            /*if(v != swipeContainer.getContentView())
                return false;*/
            if(!isInSelectionMode())
            {
                int position = getAdapterPosition();
                if(position >= 0)
                {
                    getController().didItemClicked(position);
                    return true;
                }
            }
            return false;
        }
        protected abstract void adjustForReply(boolean hasReply);
    }
    public class MessageTextViewHolder extends DefaultViewHolder {
        private TextView text;
        public MessageTextViewHolder(View itemView) {
            super(itemView);
            this.text = (TextView) itemView.findViewById(R.id.message_text);
        }
        @Override
        public void onDraw(MessageModel itm)
        {
            super.onDraw(itm);
            TextMessageModel textMessage = ((TextMessageModel) itm);
            textMessage.setDelegate(chatDelegate);
            this.text.setText(textMessage.getText());
            this.text.setMovementMethod(LinkMovementMethod.getInstance());
            this.text.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if(swipeContainer.isOpening())
                        return false;
                    int position = getAdapterPosition();
                    if (position >= 0) {
                        getController().didItemClicked(position);
                        return true;
                    }
                    return false;
                }
            });
        }

        @Override
        protected void adjustForReply(boolean hasReply) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) text.getLayoutParams();
            if(hasReply && lp.topMargin != increasedMargin) {
                lp.topMargin = increasedMargin;
                text.setLayoutParams(lp);
            }
            else if(!hasReply && lp.topMargin != defaultMargin)
            {
                lp.topMargin = defaultMargin;
                text.setLayoutParams(lp);
            }
        }
    }

    public class ForwardedMessageTextViewHolder extends DefaultViewHolder
    {
        //forwarded message
        private AvatarImageView fwd_avatar;
        private TextView fwd_date;
        private TextView fwd_username;
        private TextView fwd_message_text;
        private TextView fwd_views_count;
        private View fwd_container;
        private View.OnClickListener fwdAvatarClickListener;

        public ForwardedMessageTextViewHolder(View itemView)
        {
            super(itemView);
            //fwd_msg
            this.fwd_container = itemView.findViewById(R.id.fwd_container);
            this.fwd_avatar = (AvatarImageView) itemView.findViewById(R.id.fwd_avatar);
            this.fwd_date = (TextView) itemView.findViewById(R.id.fwd_msg_date);
            this.fwd_username = (TextView) itemView.findViewById(R.id.fwd_username);
            this.fwd_message_text = (TextView) itemView.findViewById(R.id.fwd_message_text);
            this.fwd_views_count = (TextView) itemView.findViewById(R.id.fwd_msg_views);
            fwdAvatarClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    int fwdFromId;
                    boolean channel;
                    if(position >= 0)
                    {
                        synchronized(msgListRef)
                        {
                            channel = msgListRef.get(position).forwardedFromChannel();
                            fwdFromId = msgListRef.get(position).getForwardFromId();
                            if(!isInSelectionMode())
                            {
                                if(channel)
                                    getController().didChannelAvatarClicked(fwdFromId);
                                else
                                    getController().didAvatarClicked(fwdFromId);
                            }
                            else
                                getController().selectMessage(msgListRef.get(position).getMessageId());
                        }
                    }
                }
            };
            this.fwd_avatar.setOnClickListener(fwdAvatarClickListener);
            this.fwd_avatar.setOnLongClickListener(this);
            this.fwd_message_text.setOnLongClickListener(this);
        }
        @Override
        protected void adjustForReply(boolean hasReply) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) fwd_container.getLayoutParams();
            if(hasReply && lp.topMargin != increasedMargin) {
                lp.topMargin = increasedMargin;
                fwd_container.setLayoutParams(lp);
            }
            else if(!hasReply && lp.topMargin != defaultMargin)
            {
                lp.topMargin = defaultMargin;
                fwd_container.setLayoutParams(lp);
            }
        }
        @Override
        public void onPreDraw(MessageModel itm)
        {
            super.onPreDraw(itm);
            FileModel photo = itm.getForwardFromPhoto();
            int fileId = photo.getFileId();
            if(fileId != 0)
                TelegramImageLoader.getInstance().loadImage(photo, itm.getForwardFromPhotoKey(), fwd_avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), false);
            else
                TelegramImageLoader.getInstance().loadPlaceholder(itm.getForwardFromId(), itm.getForwardFromInitials(), itm.getForwardFromPlaceholderKey(), fwd_avatar, false);
        }
        @Override
        public void onDraw(MessageModel itm)
        {
            super.onDraw(itm);
            this.fwd_date.setText(itm.getForwardDate());
            this.fwd_username.setText(itm.getForwardFromUsername());
            TextMessageModel textMessage = ((TextMessageModel) itm);
            textMessage.setDelegate(chatDelegate);
            this.fwd_message_text.setMovementMethod(LinkMovementMethod.getInstance());
            this.fwd_message_text.setText(textMessage.getText());
            if(textMessage.forwardedFromChannel())
            {
                this.fwd_views_count.setVisibility(View.VISIBLE);
                this.fwd_views_count.setText(textMessage.getViewsCount());
            }
            else
                this.fwd_views_count.setVisibility(View.GONE);
        }
    }

    public class MessagePhotoViewHolder extends DefaultViewHolder
    {
        private View imgContainer;
        private RecyclingImageView imageView;
        private TextView caption;
        private ImageUploadView uploadView;
        private View.OnClickListener imageClickListener;
        private int imgMargin, imgIncreasedMargin;
        public MessagePhotoViewHolder(View itemView)
        {
            super(itemView);
            imgMargin = AndroidUtilities.dp(4);
            imgIncreasedMargin = AndroidUtilities.dp(44);
            imageClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isInSelectionMode())
                    {
                        selectionClickListener.onClick(v);
                        return;
                    }
                    synchronized(msgListRef)
                    {
                        MessageModel model = msgListRef.get(getAdapterPosition());
                        if(model.getSuitableConstructor() == TdApi.MessagePhoto.CONSTRUCTOR && chatDelegate != null)
                            chatDelegate.didPhotoClicked((PhotoMessageModel) model);
                    }
                }
            };
            imgContainer = itemView.findViewById(R.id.msg_photo_container);
            imageView = (RecyclingImageView) imgContainer.findViewById(R.id.msg_photo);
            caption = (TextView) imgContainer.findViewById(R.id.photo_caption);
            uploadView = (ImageUploadView) itemView.findViewById(R.id.img_upload);
            imgContainer.setOnClickListener(imageClickListener);
        }
        @Override
        protected void adjustForReply(boolean hasReply) {
            RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) imgContainer.getLayoutParams();
            RelativeLayout.LayoutParams lp2 = (RelativeLayout.LayoutParams) uploadView.getLayoutParams();
            if(hasReply && lp1.topMargin != imgIncreasedMargin) {
                lp1.topMargin = imgIncreasedMargin;
                lp2.topMargin = specialIncreasedMargin;
                imgContainer.setLayoutParams(lp1);
                uploadView.setLayoutParams(lp2);
            }
            else if(!hasReply && lp1.topMargin != imgMargin)
            {
                lp1.topMargin = imgMargin;
                lp2.topMargin = specialMargin;
                imgContainer.setLayoutParams(lp1);
                uploadView.setLayoutParams(lp2);
            }
        }
        @Override
        public void onPreDraw(MessageModel itm)
        {
            super.onPreDraw(itm);
            PhotoMessageModel msg = (PhotoMessageModel) itm;
            PhotoSizeModel previewObject = msg.getPreview();
            if(msg.getCaption() != null && !msg.getCaption().isEmpty())
            {
                caption.setVisibility(View.VISIBLE);
                caption.setText(msg.getCaption());
            }
            else
                caption.setVisibility(View.GONE);
            if(msg.isSending() && msg.isSentFromThisClient() && msg.getViaId() == 0)
            {
                Log.d("BaseChatAdapter", "uploading".concat(previewObject.getPhoto().getFile().toString()));
                uploadView.setVisibility(View.VISIBLE);
                uploadView.setAttachedMsgId(msg.getMessageId());
                uploadView.attachDelegateToFile(previewObject.getPhoto());
            }
            else
                uploadView.setVisibility(View.GONE);
            if(previewObject == null)
                return;
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
            lp.width = msg.getLayoutParamsWidth();
            lp.height = msg.getLayoutParamsHeight();
            imageView.setLayoutParams(lp);
            TelegramImageLoader.getInstance().loadImage(previewObject.getPhoto(), msg.getPreviewKey(), imageView, TelegramImageLoader.POST_PROCESSING_NOTHING, 0, 0, false);
        }
    }
    public class MessageAnimationViewHolder extends DefaultViewHolder
    {
        private AnimationView animView;
        private ImageUploadView uploadView;
        private int imgMargin, imgIncreasedMargin;
        public MessageAnimationViewHolder(View itemView)
        {
            super(itemView);
            imgMargin = AndroidUtilities.dp(4);
            imgIncreasedMargin = AndroidUtilities.dp(44);
            animView = (AnimationView) itemView.findViewById(R.id.msg_anim);
            uploadView = (ImageUploadView) itemView.findViewById(R.id.anim_upload);
            animView.setOnClickListener(selectionClickListener);
            animView.setOnLongClickListener(this);
        }
        @Override
        protected void adjustForReply(boolean hasReply) {
            RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) animView.getLayoutParams();
            RelativeLayout.LayoutParams lp2 = (RelativeLayout.LayoutParams) uploadView.getLayoutParams();
            if(hasReply && lp1.topMargin != imgIncreasedMargin) {
                lp1.topMargin = imgIncreasedMargin;
                lp2.topMargin = specialIncreasedMargin;
                animView.setLayoutParams(lp1);
                uploadView.setLayoutParams(lp2);
            }
            else if(!hasReply && lp1.topMargin != imgMargin)
            {
                lp1.topMargin = imgMargin;
                lp2.topMargin = specialMargin;
                animView.setLayoutParams(lp1);
                uploadView.setLayoutParams(lp2);
            }
        }
        @Override
        public void onPreDraw(MessageModel itm)
        {
            super.onPreDraw(itm);
            AnimationMessageModel msg = (AnimationMessageModel) itm;
            if(msg.isSending() && msg.isSentFromThisClient() && msg.getViaId() == 0)
            {
                uploadView.setVisibility(View.VISIBLE);
                uploadView.setAttachedMsgId(msg.getMessageId());
                uploadView.attachDelegateToFile(FileCache.getInstance().getFileModel(msg.getAnimation().animation));
            }
            else
                uploadView.setVisibility(View.GONE);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) animView.getLayoutParams();
            lp.width = msg.getLayoutParamsWidth();
            lp.height = msg.getLayoutParamsHeight();
            animView.setLayoutParams(lp);
            TelegramImageLoader.getInstance().loadAnimation(msg.getAnimation(), msg.getKey(), animView);
        }
    }
    public class MessageStickerViewHolder extends DefaultViewHolder
    {
        private RecyclingImageView imageView;
        public MessageStickerViewHolder(View itemView)
        {
            super(itemView);
            imageView = (RecyclingImageView) itemView.findViewById(R.id.sticker);
        }
        @Override
        public void onPreDraw(MessageModel itm)
        {
            super.onPreDraw(itm);
            imageView.setOnClickListener(selectionClickListener);
            StickerMessageModel msg = (StickerMessageModel) itm;
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
            lp.width = msg.getSticker().width;
            lp.height = msg.getSticker().height;
            imageView.setLayoutParams(lp);
            TelegramImageLoader.getInstance().loadSticker(msg.getSticker(), TelegramImageLoader.CMD_RETURN_STICKER_FULL_SIZE, imageView);
        }
        @Override
        protected void adjustForReply(boolean hasReply) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
            if(hasReply && lp.topMargin != increasedMargin) {
                lp.topMargin = increasedMargin;
                imageView.setLayoutParams(lp);
            }
            else if(!hasReply && lp.topMargin != defaultMargin)
            {
                lp.topMargin = defaultMargin;
                imageView.setLayoutParams(lp);
            }
        }
    }
    public class MessageVoiceViewHolder extends DefaultViewHolder
    {
        private VoiceDownloadView downloadView;

        public MessageVoiceViewHolder(View itemView)
        {
            super(itemView);
            downloadView = (VoiceDownloadView) itemView.findViewById(R.id.audio_view);
        }
        @Override
        protected void adjustForReply(boolean hasReply) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) downloadView.getLayoutParams();
            if(hasReply && lp.topMargin != increasedMargin) {
                lp.topMargin = increasedMargin;
                downloadView.setLayoutParams(lp);
            }
            else if(!hasReply && lp.topMargin != defaultMargin)
            {
                lp.topMargin = defaultMargin;
                downloadView.setLayoutParams(lp);
            }
        }
        @Override
        public void onDraw(MessageModel itm)
        {
            super.onDraw(itm);
            final VoiceMessageModel msg = (VoiceMessageModel) itm;
            downloadView.setDuration(msg.getDuration());
            final MessageVoicePlayer.VoiceMessageDelegate voicePlaybackDelegate = new MessageVoicePlayer.VoiceMessageDelegate() {
                @Override
                public void didPlaybackStarted() {
                    if(BuildConfig.DEBUG)
                        Log.d(TAG, "didPlaybackStarted");
                    msg.setPlaybackState(true);
                    ((AudioDownloadButton) downloadView.getDownloadButton()).setPlaybackState(true);
                    AudioPlaybackService playbackService = TelegramApplication.sharedApplication().getAudioPlaybackService();
                    if(playbackService != null)
                        playbackService.pausePlayback();
                }

                @Override
                public void didPlaybackStopped() {
                    msg.setPlaybackState(false);
                    msg.resetStartMark();
                    downloadView.updateProgress(0.0f);
                    downloadView.setPlaybackState(false);
                }

                @Override
                public void didPlaybackProgressUpdated() {
                    if(msg.getPlaybackState() == false) {
                        msg.setPlaybackState(true);
                        downloadView.setPlaybackState(true);
                    }
                    downloadView.updateProgress(msg.getStartFrom());
                }

                @Override
                public void didPlaybackPaused() {
                    msg.setPlaybackState(false);
                    downloadView.setPlaybackState(false);
                }

                @Override
                public String getDelegateKey() {
                    return msg.getDelegateKey();
                }
            };
            msg.getAudioFileRef().addDelegate(voicePlaybackDelegate);
            downloadView.setSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean fromUser) {
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    msg.startPlaybackFrom((float) seekBar.getProgress() / 100);
                    MessageVoicePlayer.getInstance().seek(msg);
                }
            });
            downloadView.setPlaybackState(msg.getPlaybackState());
            downloadView.updateProgress(msg.getStartFrom());
            downloadView.getDownloadButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FileModel.DownloadState state = msg.getAudioFileRef().getDownloadState();
                    if(state == FileModel.DownloadState.EMPTY)
                        downloadView.startDownload();
                    else if(state == FileModel.DownloadState.LOADING)
                        downloadView.cancelDownload();
                    else if(state == FileModel.DownloadState.LOADED && chatDelegate != null) {
                        MessageVoicePlayer.getInstance().play(msg);
                    }
                }
            });
            downloadView.setAttachedMsgId(msg.getMessageId());
            downloadView.attachDelegateToFile(msg.getAudioFileRef());
        }
        @Override
        public void onItemDetachFromWindow()
        {
            super.onItemDetachFromWindow();
            int position = getAdapterPosition();
            if(position >= 0)
            {
                synchronized(msgListRef)
                {
                    VoiceMessageModel msg = (VoiceMessageModel) msgListRef.get(position);
                    msg.getAudioFileRef().removeVoiceMsgDelegate(msg.getDelegateKey());
                }
            }
            downloadView.setAttachedMsgId(0);
            downloadView.attachDelegateToFile(null);
        }
    }
    public class MessageAudioViewHolder extends DefaultViewHolder
    {
        private AudioDownloadView downloadView;

        public MessageAudioViewHolder(View itemView)
        {
            super(itemView);
            downloadView = (AudioDownloadView) itemView.findViewById(R.id.audio_download_view);
        }
        @Override
        public void onDraw(MessageModel itm)
        {
            super.onDraw(itm);
            final AudioMessageModel msg = (AudioMessageModel) itm;
            downloadView.setTrackName(msg.getTrackName());
            downloadView.setBandName(msg.getBandName());
            downloadView.getDownloadButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FileModel.DownloadState state = msg.getAudioFileRef().getDownloadState();
                    if(state == FileModel.DownloadState.EMPTY)
                        downloadView.startDownload();
                    else if(state == FileModel.DownloadState.LOADING)
                        downloadView.cancelDownload();
                    else if(state == FileModel.DownloadState.LOADED && chatDelegate != null) {
                        chatDelegate.didPlayerLaunchRequested(msg.getTrackModel());
                    }
                }
            });
            downloadView.setAttachedMsgId(msg.getMessageId());
            downloadView.attachDelegateToFile(msg.getAudioFileRef());
        }
        @Override
        public void onItemDetachFromWindow()
        {
            super.onItemDetachFromWindow();
            downloadView.setAttachedMsgId(0);
            downloadView.attachDelegateToFile(null);
        }

        @Override
        protected void adjustForReply(boolean hasReply) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) downloadView.getLayoutParams();
            if(hasReply && lp.topMargin != increasedMargin) {
                lp.topMargin = increasedMargin;
                downloadView.setLayoutParams(lp);
            }
            else if(!hasReply && lp.topMargin != defaultMargin)
            {
                lp.topMargin = defaultMargin;
                downloadView.setLayoutParams(lp);
            }
        }
    }

    public class MessageContactViewHolder extends DefaultViewHolder
    {
        private AvatarImageView avatar;
        private TextView nameTextView;
        private TextView phoneTextView;
        private View contactContainer;
        private View.OnClickListener contactPhotoClickListener;
        public MessageContactViewHolder(View itemView)
        {
            super(itemView);
            contactContainer = itemView.findViewById(R.id.contact_container);
            avatar = (AvatarImageView) itemView.findViewById(R.id.contact_avatar);
            nameTextView = (TextView) itemView.findViewById(R.id.username);
            phoneTextView = (TextView) itemView.findViewById(R.id.text);
            contactPhotoClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    int userId;
                    if(position >= 0)
                    {
                        synchronized(msgListRef)
                        {
                            if(!isInSelectionMode())
                            {
                                userId = ((ContactMessageModel)msgListRef.get(getAdapterPosition())).getContactUserId();
                                getController().didAvatarClicked(userId);
                            }
                            else
                                getController().selectMessage(msgListRef.get(getAdapterPosition()).getMessageId());
                        }
                    }
                }
            };
            avatar.setOnClickListener(contactPhotoClickListener);
        }
        @Override
        protected void adjustForReply(boolean hasReply) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) contactContainer.getLayoutParams();
            if(hasReply && lp.topMargin != increasedMargin) {
                lp.topMargin = increasedMargin;
                contactContainer.setLayoutParams(lp);
            }
            else if(!hasReply && lp.topMargin != defaultMargin)
            {
                lp.topMargin = defaultMargin;
                contactContainer.setLayoutParams(lp);
            }
        }
        @Override
        public void onPreDraw(MessageModel itm)
        {
            super.onPreDraw(itm);
            ContactMessageModel msg = (ContactMessageModel) itm;
            FileModel photo = msg.getContactPhoto();
            if(photo != null)
            {
                int fileId = photo.getFileId();
                if(fileId != 0)
                    TelegramImageLoader.getInstance().loadImage(photo, msg.getContactPhotoKey(), avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), false);
            }
        }
        @Override
        public void onDraw(MessageModel itm)
        {
            super.onDraw(itm);
            ContactMessageModel msg = (ContactMessageModel) itm;
            nameTextView.setText(msg.getContactDisplayName());
            phoneTextView.setText(msg.getContactPhoneNumber());
        }
    }

    public class MessageDocumentViewHolder extends DefaultViewHolder
    {
        private DocumentDownloadView downloadView;

        public MessageDocumentViewHolder(View itemView) {
            super(itemView);
            downloadView = (DocumentDownloadView) itemView.findViewById(R.id.document_download);
        }
        @Override
        public void onPreDraw(MessageModel itm)
        {
            super.onPreDraw(itm);
            final DocumentMessageModel msg = (DocumentMessageModel) itm;
            downloadView.setHasThumb(msg.hasThumb());
            if(msg.hasThumb()) {
                downloadView.getThumbImageView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FileModel.DownloadState state = msg.getDocFileRef().getDownloadState();
                        if(state == FileModel.DownloadState.LOADED && chatDelegate != null)
                            chatDelegate.didFileOpenRequested(msg.getDocFileRef().getFile().path, msg.getMimeType());
                    }
                });
                TelegramImageLoader.getInstance().loadImage(msg.getThumbnail().getPhoto(), CommonTools.makeFileKey(msg.getThumbnail().getPhoto().getFileId()), downloadView.getThumbImageView(), TelegramImageLoader.POST_PROCESSING_NOTHING, AndroidUtilities.dp(80), AndroidUtilities.dp(80), false);
            }
            downloadView.setDocumentName(((DocumentMessageModel) itm).getDocName());
            downloadView.getDownloadButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FileModel.DownloadState state = msg.getDocFileRef().getDownloadState();
                    if(state == FileModel.DownloadState.EMPTY)
                        downloadView.startDownload();
                    else if(state == FileModel.DownloadState.LOADING)
                        downloadView.cancelDownload();
                    else if(state == FileModel.DownloadState.LOADED && chatDelegate != null)
                        chatDelegate.didFileOpenRequested(msg.getDocFileRef().getFile().path, msg.getMimeType());
                }
            });
            downloadView.setAttachedMsgId(msg.getMessageId());
            downloadView.attachDelegateToFile(msg.getDocFileRef());
        }
        @Override
        protected void adjustForReply(boolean hasReply) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) downloadView.getLayoutParams();
            if(hasReply && lp.topMargin != increasedMargin) {
                lp.topMargin = increasedMargin;
                downloadView.setLayoutParams(lp);
            }
            else if(!hasReply && lp.topMargin != defaultMargin)
            {
                lp.topMargin = defaultMargin;
                downloadView.setLayoutParams(lp);
            }
        }
        @Override
        public void onItemDetachFromWindow()
        {
            super.onItemDetachFromWindow();
            downloadView.setAttachedMsgId(0);
            downloadView.attachDelegateToFile(null);
        }
    }

    public class MessageGeoPointViewHolder extends DefaultViewHolder
    {
        private RecyclingImageView locationImg;
        public MessageGeoPointViewHolder(View itemView)
        {
            super(itemView);
            locationImg = (RecyclingImageView) itemView.findViewById(R.id.location_img);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    synchronized(msgListRef)
                    {
                        int position = getAdapterPosition();
                        if(position >= 0 && chatDelegate != null)
                        {
                            GeoPointMessageModel msg = (GeoPointMessageModel) msgListRef.get(position);
                            chatDelegate.didLocationClicked(msg.getLocation());
                        }
                    }
                }
            });
        }
        @Override
        protected void adjustForReply(boolean hasReply) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) locationImg.getLayoutParams();
            if(hasReply && lp.topMargin != increasedMargin) {
                lp.topMargin = increasedMargin;
                locationImg.setLayoutParams(lp);
            }
            else if(!hasReply && lp.topMargin != defaultMargin)
            {
                lp.topMargin = defaultMargin;
                locationImg.setLayoutParams(lp);
            }
        }
        @Override
        public void onPreDraw(MessageModel itm)
        {
            super.onPreDraw(itm);
            GeoPointMessageModel msg = (GeoPointMessageModel) itm;
            TelegramImageLoader.getInstance().loadLocation(msg.getLocation(), msg.getLocationKey(), locationImg, AndroidUtilities.dp(150), AndroidUtilities.dp(100));
        }
    }
    public class MessageVenueViewHolder extends DefaultViewHolder
    {
        private RecyclingImageView img;
        private TextView title;
        private TextView address;
        private View venueContainer;
        public MessageVenueViewHolder(View itemView)
        {
            super(itemView);
            venueContainer = itemView.findViewById(R.id.venue_info_container);
            img = (RecyclingImageView) itemView.findViewById(R.id.location_pic);
            title = (TextView) itemView.findViewById(R.id.venue_title);
            address = (TextView) itemView.findViewById(R.id.venue_address);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    synchronized(msgListRef)
                    {
                        int position = getAdapterPosition();
                        if(position >= 0 && chatDelegate != null)
                        {
                            VenueMessageModel msg = (VenueMessageModel) msgListRef.get(position);
                            chatDelegate.didLocationClicked(msg.getLocation());
                        }
                    }
                }
            });
        }
        @Override
        public void onPreDraw(MessageModel itm)
        {
            super.onPreDraw(itm);
            VenueMessageModel msg = (VenueMessageModel) itm;
            TelegramImageLoader.getInstance().loadLocation(msg.getLocation(), msg.getLocationKey(), img, AndroidUtilities.dp(50), AndroidUtilities.dp(50));
        }
        @Override
        public void onDraw(MessageModel itm)
        {
            super.onDraw(itm);
            VenueMessageModel msg = (VenueMessageModel) itm;
            title.setText(msg.getTitle());
            address.setText(msg.getAddress());
        }
        @Override
        protected void adjustForReply(boolean hasReply) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) venueContainer.getLayoutParams();
            if(hasReply && lp.topMargin != increasedMargin) {
                lp.topMargin = increasedMargin;
                venueContainer.setLayoutParams(lp);
            }
            else if(!hasReply && lp.topMargin != defaultMargin)
            {
                lp.topMargin = defaultMargin;
                venueContainer.setLayoutParams(lp);
            }
        }
    }
    public class MessageVideoViewHolder extends DefaultViewHolder
    {
        private VideoDownloadView downloadView;
        public MessageVideoViewHolder(View itemView) {
            super(itemView);
            downloadView = (VideoDownloadView) itemView.findViewById(R.id.video_download_view);
        }
        @Override
        public void onPreDraw(MessageModel itm)
        {
            super.onPreDraw(itm);
            final VideoMessageModel msg = (VideoMessageModel) itm;
            PhotoSizeModel previewObject = msg.getThumb();
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) downloadView.getThumbImageView().getLayoutParams();
            lp.width = msg.getLayoutParamsWidth();
            lp.height = msg.getLayoutParamsHeight();
            downloadView.getThumbImageView().setLayoutParams(lp);
            TelegramImageLoader.getInstance().loadImage(previewObject.getPhoto(), msg.getThumbKey(), downloadView.getThumbImageView(), TelegramImageLoader.POST_PROCESSING_BLUR, 0, 0, false);
            downloadView.getDownloadButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FileModel.DownloadState state = msg.getVideoFileRef().getDownloadState();
                    if(state == FileModel.DownloadState.EMPTY)
                        downloadView.startDownload();
                    else if(state == FileModel.DownloadState.LOADING)
                        downloadView.cancelDownload();
                    else if(state == FileModel.DownloadState.LOADED && chatDelegate != null) {
                        chatDelegate.didFileOpenRequested(msg.getVideoFileRef().getFile().path, "video/*");
                    }
                }
            });
            downloadView.setAttachedMsgId(msg.getMessageId());
            downloadView.attachDelegateToFile(msg.getVideoFileRef());
        }
        @Override
        protected void adjustForReply(boolean hasReply) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) downloadView.getLayoutParams();
            if(hasReply && lp.topMargin != increasedMargin) {
                lp.topMargin = increasedMargin;
                downloadView.setLayoutParams(lp);
            }
            else if(!hasReply && lp.topMargin != defaultMargin)
            {
                lp.topMargin = defaultMargin;
                downloadView.setLayoutParams(lp);
            }
        }
    }
    public class MessageWebPageViewHolder extends DefaultViewHolder
    {
        private TextView text;
        private WebPagePreview preview;
        public MessageWebPageViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.message_text);
            preview = (WebPagePreview) itemView.findViewById(R.id.web_page_preview);

            preview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (BuildConfig.DEBUG)
                        Log.d(TAG, "preview click");
                    if(chatDelegate != null) {
                        int position = getAdapterPosition();
                        if(position >= 0) {
                            synchronized(msgListRef)
                            {
                                chatDelegate.didUrlClicked(((WebPageMessageModel) msgListRef.get(position)).getPageContainer().url, false);
                            }
                        }
                    }
                }
            });
            preview.setPhotoClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (BuildConfig.DEBUG)
                        Log.d(TAG, "photoClick");
                        if (chatDelegate != null) {
                            int position = getAdapterPosition();
                            if (position >= 0) {
                                synchronized (msgListRef) {
                                    chatDelegate.didPhotoClicked(((WebPageMessageModel) msgListRef.get(position)));
                                }
                            }
                        }
                }
            });
            preview.setOnLongClickListener(this);
        }
        @Override
        public void onDraw(MessageModel itm)
        {
            super.onDraw(itm);
            WebPageMessageModel textMessage = ((WebPageMessageModel) itm);
            textMessage.setDelegate(chatDelegate);
            text.setText(((WebPageMessageModel) itm).getText());
            text.setMovementMethod(LinkMovementMethod.getInstance());
            this.text.setOnLongClickListener(this);
            preview.attachWebPage(((WebPageMessageModel) itm).getPageContainer());
        }
        @Override
        protected void adjustForReply(boolean hasReply) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) text.getLayoutParams();
            if(hasReply && lp.topMargin != increasedMargin) {
                lp.topMargin = increasedMargin;
                text.setLayoutParams(lp);
            }
            else if(!hasReply && lp.topMargin != defaultMargin)
            {
                lp.topMargin = defaultMargin;
                text.setLayoutParams(lp);
            }
        }
        @Override
        public void onPreDraw(MessageModel itm)
        {
            super.onPreDraw(itm);
            preview.setPhoto((WebPageMessageModel) itm);
        }
    }
    public void updateId(int oldId, int newId)
    {
        if(!statusViewDelegates.containsKey(oldId))
            return;
        try
        {
            if(BuildConfig.DEBUG)
                Log.d(TAG, String.format("removing status delegate %d; adding %d", oldId, newId));
            MessageStatusView.UpdateChatReadDelegate delegate = statusViewDelegates.get(oldId);
            statusViewDelegates.remove(oldId);
            statusViewDelegates.put(newId, delegate);
            if(getController() != null)
                delegate.processUpdateChatRead(getController().getLastReadOutMessageId());
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    public void updateStatusViews()
    {
        if(getController() == null)
            return;
        int[] keys = statusViewDelegates.keys();
        int lastReadOutbox = getController().getLastReadOutMessageId();
        for(int k : keys)
        {
            MessageStatusView.UpdateChatReadDelegate delegate = statusViewDelegates.get(k);
            if(delegate == null)
                continue;
            delegate.processUpdateChatRead(lastReadOutbox);
        }
    }
    public boolean hasInSelection(int msgId)
    {
        return selection.contains(msgId);
    }
    public void toggleSelection(int msgId, int index)
    {
        synchronized(msgListRef)
        {
            MessageModel msg = msgListRef.get(index);
            if(msg.getMessageId() == msgId)
            {
                if(selection.contains(msgId))
                    selection.remove(msgId);
                else
                    selection.add(msgId);
                notifyItemChanged(index);
            }
        }
    }
    public void removeFromSelectionSet(int messageId)
    {
        if(selection.contains(messageId))
            selection.remove(messageId);
    }
    public void clearSelectionSet()
    {
        selection.clear();
    }
    public boolean isInSelectionMode() { return !this.selection.isEmpty(); }
    public int[] getSelection() { return this.selection.toArray(); }
    public int getSelectionCount() { return this.selection.size(); }
    public void setChatDelegate(BaseChatFragment.ChatDelegate delegate)
    {
        this.chatDelegate = delegate;
    }

    public class ServiceMsgViewHolder extends RecyclerView.ViewHolder
    {
        public TextView serviceMessageText;
        public ImageView newChatPhoto;
        public ServiceMsgViewHolder(View itemView)
        {
            super(itemView);
            this.serviceMessageText = (TextView) itemView.findViewById(R.id.chat_service_message);
            this.serviceMessageText.setMovementMethod(LinkMovementMethod.getInstance());
            this.newChatPhoto = (ImageView) itemView.findViewById(R.id.chat_new_photo);
        }
    }
    private class QuickActionClickListener implements View.OnClickListener
    {
        private MessageModel attachedMsg;
        private boolean reply;

        @Override
        public void onClick(View view) {
            if(attachedMsg == null || chatDelegate == null)
                return;
            if(reply)
                chatDelegate.didQuickReplyRequested(attachedMsg);
            else
                chatDelegate.didQuickShareRequested(attachedMsg);
        }
    }

}
