package com.andremacareno.tdtest.listadapters.menu;

/**
 * Created by andremacareno on 31/08/15.
 */
public class MenuItem {
    private int id;
    private String value;
    public MenuItem(int _id, String _value)
    {
        this.id = _id;
        this.value = _value;
    }
    public int getId() { return this.id; }
    public String getValue() { return this.value; }
    @Override
    public String toString() { return this.value; }
}
