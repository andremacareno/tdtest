package com.andremacareno.tdtest.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.images.RecyclingImageView;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.ContactsListViewController;
import com.andremacareno.tdtest.viewcontrollers.NewChatContactsListViewController;
import com.andremacareno.tdtest.views.ContactsListItemLayout;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import gnu.trove.set.hash.TIntHashSet;

/**
 * Created by andremacareno on 04/04/15.
 */
public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactsHolder> {
    private final String TAG = "ContactsAdapter";
    protected ArrayList<UserModel> contactsListRef;
    protected WeakReference<ContactsListViewController> controller;
    private final TIntHashSet selectedContacts = new TIntHashSet();
    private boolean selectable;
    public ContactsAdapter(ArrayList<UserModel> data, ContactsListViewController c, boolean selectable)
    {
        setHasStableIds(true);
        this.contactsListRef = data;
        this.controller = c == null ? null : new WeakReference<ContactsListViewController>(c);
        this.selectable = selectable;
    }
    @Override
    public long getItemId(int position)
    {
        return contactsListRef.get(position).getId();
    }
    @Override
    public ContactsHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(72));
        View v = new ContactsListItemLayout(viewGroup.getContext());
        v.setLayoutParams(lp);
        return new ContactsHolder(v);
    }

    @Override
    public void onBindViewHolder(ContactsHolder viewHolder, int i) {
        UserModel itm = contactsListRef.get(i);
        ((ContactsListItemLayout) viewHolder.itemView).fillLayout(itm);
    }
    @Override
    public void onViewAttachedToWindow(ContactsHolder holder)
    {
        int i = holder.getAdapterPosition();
        if (i < 0)
            return;
        UserModel itm = contactsListRef.get(i);
        FileModel photo = itm.getPhoto();
        RecyclingImageView iv = ((ContactsListItemLayout) holder.itemView).getAvatarImageView();
        int fileId = photo.getFileId();
        if(fileId != 0)
            TelegramImageLoader.getInstance().loadImage(photo, itm.getPhotoKey(), iv, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), false);
        else
            TelegramImageLoader.getInstance().loadPlaceholder(itm.getId(), itm.getInitials(), itm.getPhotoPlaceholderKey(), iv, false);
        if(isSelected(i))
            ((ContactsListItemLayout) holder.itemView).check(false);
        else
            ((ContactsListItemLayout) holder.itemView).uncheck(false);
    }

    private void selectItem(int position)
    {
        if(selectedContacts.size() == Constants.MAX_CHAT_PARTICIPANTS)
            return;     //too many chat participants
        UserModel u = contactsListRef.get(position);
        selectedContacts.add(u.getId());
    }
    private void unselectItem(int position)
    {
        UserModel u = contactsListRef.get(position);
        selectedContacts.remove(u.getId());
    }

    public void toggleSelection(int position)
    {
        if(isSelected(position))
            unselectItem(position);
        else
            selectItem(position);
        ((NewChatContactsListViewController)controller.get()).didChatParticipantsCountChanged(selectedContacts.size());
    }

    private boolean isSelected(int position)
    {
        if(!selectable)
            return false;
        UserModel u = contactsListRef.get(position);
        return selectedContacts.contains(u.getId());
    }
    @Override
    public int getItemCount() {
       return contactsListRef.size();
    }
    public int[] getParticipantIds()
    {
        return selectedContacts.toArray(new int[selectedContacts.size()]);
    }
    public class ContactsHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ContactsHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(selectable)
            {
                int i = getAdapterPosition();
                if(i >= 0)
                {
                    if(isSelected(i))
                        ((ContactsListItemLayout) itemView).uncheck(true);
                    else
                        ((ContactsListItemLayout) itemView).check(true);
                    toggleSelection(i);
                }
            }
            else
            {
                if(controller != null && controller.get() != null)
                    controller.get().didItemClicked(getAdapterPosition());
            }
        }
    }
}
