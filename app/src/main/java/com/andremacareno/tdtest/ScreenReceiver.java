package com.andremacareno.tdtest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;

public class ScreenReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationCenter.getInstance().postNotification(NotificationCenter.didScreenStateChanged, intent.getAction());
    }
}
