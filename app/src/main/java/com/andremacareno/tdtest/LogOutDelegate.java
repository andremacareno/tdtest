package com.andremacareno.tdtest;

/**
 * Created by Andrew on 16.04.2015.
 */
public interface LogOutDelegate {
    public void didLogOutRequested();
}
