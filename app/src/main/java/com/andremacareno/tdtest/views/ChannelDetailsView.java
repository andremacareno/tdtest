package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.andremacareno.tdtest.ChannelCache;
import com.andremacareno.tdtest.R;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 22/06/15.
 */
public class ChannelDetailsView extends LinearLayout {
    public interface ChannelDetailsDelegate
    {
        public void didChannelLinkClicked();
        public void didChannelDescriptionClicked();
    }
    private ChannelLinkView channel_link;
    private ChannelDescriptionView channel_description;
    private int boundChannel = 0;
    private ChannelDetailsDelegate delegate;
    public ChannelDetailsView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.channel_details, this);
        setOrientation(VERTICAL);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        channel_link = (ChannelLinkView) v.findViewById(R.id.link);
        channel_description = (ChannelDescriptionView) v.findViewById(R.id.channel_description);
        channel_link.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (delegate != null)
                    delegate.didChannelLinkClicked();
            }
        });
        channel_description.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (delegate != null)
                    delegate.didChannelDescriptionClicked();
            }
        });
    }
    private void fillLayout()
    {
        if(boundChannel == 0)
            return;
        TdApi.ChannelFull channel = ChannelCache.getInstance().getChannelFull(boundChannel);
        String link = channel.channel.username.isEmpty() ? null : "/".concat(channel.channel.username.toLowerCase());
        String description = channel.about;
        if(link == null && description.isEmpty())
        {
            setVisibility(GONE);
            return;
        }
        setVisibility(VISIBLE);
        if(link == null)
            channel_link.setVisibility(View.GONE);
        else {
            channel_link.setVisibility(VISIBLE);
            channel_link.setLink(link);
        }
        if(description.isEmpty())
            channel_description.setVisibility(View.GONE);
        else
        {
            channel_description.setVisibility(View.VISIBLE);
            channel_description.setAbout(description);
        }
    }
    public void bindChannel(int channel)
    {
        if(channel == 0)
            return;
        this.boundChannel = channel;
        fillLayout();
    }
    public void setDelegate(ChannelDetailsDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    public void updateChannelDescription(String description)
    {
        if(description.isEmpty() && channel_link.isEmpty())
        {
            setVisibility(GONE);
            return;
        }
        else
            setVisibility(VISIBLE);
        if(description.isEmpty())
            channel_description.setVisibility(View.GONE);
        else
        {
            channel_description.setVisibility(View.VISIBLE);
            channel_description.setAbout(description);
        }
        if(!channel_link.isEmpty() ) {
            channel_link.setVisibility(View.VISIBLE);
        }
        else
            channel_link.setVisibility(View.GONE);
    }

    public void updateChannelLink(String username)
    {
        if(username != null && !username.isEmpty() && channel_description.isEmpty())
        {
            setVisibility(GONE);
            return;
        }
        else
            setVisibility(VISIBLE);
        if(channel_description.isEmpty())
            channel_description.setVisibility(View.GONE);
        else
            channel_description.setVisibility(View.VISIBLE);

        if(!channel_link.isEmpty() ) {
            channel_link.setVisibility(View.VISIBLE);
        }
        else
            channel_link.setVisibility(View.GONE);

        if(username != null && !username.isEmpty()) {
            setVisibility(VISIBLE);
            channel_link.setVisibility(View.VISIBLE);
            channel_link.setLink("/".concat(username));
        }
        else
            channel_link.setVisibility(View.GONE);
    }
}
