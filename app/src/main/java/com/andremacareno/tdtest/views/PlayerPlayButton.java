package com.andremacareno.tdtest.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 22/06/15.
 */
public class PlayerPlayButton extends View {
    private final int DOWNLOAD_BTN_SIZE = isInEditMode() ? 144 : AndroidUtilities.dp(48);
    private final Drawable downloadDrawable = getResources().getDrawable(R.drawable.ic_download_blue);

    private final Drawable pauseDownloadDrawable = getResources().getDrawable(R.drawable.ic_file_pause_blue);

    private final Drawable playDrawable = getResources().getDrawable(R.drawable.ic_playerplay);

    private final Drawable pausePlaybackDrawable = getResources().getDrawable(R.drawable.ic_playerpause);

    private Paint lightBlueCirclePaint;
    private Paint darkBlueCirclePaint;
    private FileModel.DownloadState fileDownloadState;
    private boolean isPlaying;

    public PlayerPlayButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.fileDownloadState = FileModel.DownloadState.EMPTY;
        this.isPlaying = false;
        init();
    }
    private void init()
    {
        if(isInEditMode())
            playDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-18, DOWNLOAD_BTN_SIZE/2-22, DOWNLOAD_BTN_SIZE/2+22, DOWNLOAD_BTN_SIZE/2+22);
        else
        {
            final int eighteenDp = AndroidUtilities.dp(18)/2;
            final int twelveDp = AndroidUtilities.dp(12)/2;
            final int fourteenDp = AndroidUtilities.dp(14)/2;
            final int thirteenDp = AndroidUtilities.dp(13)/2;
            final int fifteenDp = AndroidUtilities.dp(15)/2;
            downloadDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-twelveDp, DOWNLOAD_BTN_SIZE/2-eighteenDp, DOWNLOAD_BTN_SIZE/2+twelveDp, DOWNLOAD_BTN_SIZE/2+eighteenDp);
            pauseDownloadDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-twelveDp, DOWNLOAD_BTN_SIZE/2-fourteenDp, DOWNLOAD_BTN_SIZE/2+twelveDp, DOWNLOAD_BTN_SIZE/2+fourteenDp);
            playDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-twelveDp, DOWNLOAD_BTN_SIZE/2-fifteenDp, DOWNLOAD_BTN_SIZE/2+fourteenDp, DOWNLOAD_BTN_SIZE/2+fifteenDp);
            pausePlaybackDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-twelveDp, DOWNLOAD_BTN_SIZE/2-fourteenDp, DOWNLOAD_BTN_SIZE/2+twelveDp, DOWNLOAD_BTN_SIZE/2+fourteenDp);
        }
        lightBlueCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        lightBlueCirclePaint.setColor(ContextCompat.getColor(getContext(), R.color.light_blue_bg));
        darkBlueCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        darkBlueCirclePaint.setColor(ContextCompat.getColor(getContext(), R.color.dark_blue_bg));
    }
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(DOWNLOAD_BTN_SIZE, DOWNLOAD_BTN_SIZE);
    }
    @Override
    public void onDraw(Canvas canvas)
    {
        if(isInEditMode())
        {
            canvas.drawCircle(DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, darkBlueCirclePaint);
            playDrawable.draw(canvas);
            return;
        }
        if(fileDownloadState == FileModel.DownloadState.LOADED)
        {
            canvas.drawCircle(DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, darkBlueCirclePaint);
            if(!isPlaying)
                playDrawable.draw(canvas);
            else
                pausePlaybackDrawable.draw(canvas);
        }
        else
        {
            canvas.drawCircle(DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, lightBlueCirclePaint);
            if(fileDownloadState == FileModel.DownloadState.EMPTY)
                downloadDrawable.draw(canvas);
            else if(fileDownloadState == FileModel.DownloadState.LOADING)
                pauseDownloadDrawable.draw(canvas);
        }
    }
    public void changeDownloadState(FileModel.DownloadState newState)
    {
        this.fileDownloadState = newState;
        postInvalidate();
    }
    public void setPlayFlag(boolean flag)
    {
        this.isPlaying = flag;
        postInvalidate();
    }
    public FileModel.DownloadState getDownloadState() { return this.fileDownloadState; }
}
