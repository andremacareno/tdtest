package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;

import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.viewcontrollers.ConversationsListViewController;
import com.andremacareno.tdtest.viewcontrollers.ViewControllable;
import com.andremacareno.tdtest.viewcontrollers.ViewController;

/**
 * Created by andremacareno on 04/04/15.
 */
public class ConversationsListView extends BaseListView implements ViewControllable {
    private ConversationsListViewController controller;
    private LayoutManager lm;
    public ConversationsListView(Context context) {
        super(context);
        init();
    }

    public ConversationsListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ConversationsListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init()
    {
        if(lm == null)
            lm = new LinearLayoutManager(TelegramApplication.sharedApplication().getApplicationContext());
        setLayoutManager(lm);
        //setItemAnimator(null);
    }
    @Override
    public void setController(ViewController vc)
    {
        this.controller = (ConversationsListViewController) vc;
    }

    @Override
    public ViewController getController() {
        return controller;
    }
    public int getScrollPosition()
    {
        return ((LinearLayoutManager) lm).findFirstVisibleItemPosition();
    }
}
