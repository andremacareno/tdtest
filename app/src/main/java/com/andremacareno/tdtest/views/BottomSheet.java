package com.andremacareno.tdtest.views;

/**
 * Created by andremacareno on 28/02/16.
 */
public interface BottomSheet {
    void onClose(boolean dismissed);
    //methods for slide animations; should return relative value
    void setYFraction(float fraction);
    float getYFraction();
}
