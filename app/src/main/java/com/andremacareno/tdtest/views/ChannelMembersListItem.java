package com.andremacareno.tdtest.views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.ChatParticipantModel;

/**
 * Created by andremacareno on 05/05/15.
 */
public class ChannelMembersListItem extends FrameLayout {
    public AvatarImageView avatar;
    public TextView contactName;
    public TextView status;
    protected ColorStateList defaultTextColor;


    public ChannelMembersListItem(Context context) {
        super(context);
        init();
    }
    public ChannelMembersListItem(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    protected void init()
    {
        View itemView = inflate(getContext(), R.layout.channel_admins_list_item, this);
        this.avatar = (AvatarImageView) itemView.findViewById(R.id.avatar);
        this.contactName = (TextView) itemView.findViewById(R.id.contact_name);
        this.status = (TextView) itemView.findViewById(R.id.status);
        setBackgroundResource(R.drawable.profile_list_selector);
        defaultTextColor = this.status.getTextColors();
    }
    public AvatarImageView getAvatarImageView() { return this.avatar; }
    public void fillLayout(ChatParticipantModel itm)
    {
        contactName.setText(itm.getDisplayName());
        if(!itm.isBot())
            status.setText(itm.getStringStatus());
        else
            status.setText(itm.canBotReadMessages() ? R.string.has_access_to_messages : R.string.has_no_access_to_messages);
        if(itm.isOnline() && !itm.isBot())
            status.setTextColor(ContextCompat.getColor(TelegramApplication.sharedApplication().getApplicationContext(), R.color.vk_unread_text));
        else
            status.setTextColor(defaultTextColor);
    }
}