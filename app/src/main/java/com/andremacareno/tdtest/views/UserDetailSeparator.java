package com.andremacareno.tdtest.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 24/08/15.
 */
public class UserDetailSeparator extends View {
    private Paint linePaint;
    public UserDetailSeparator(Context context) {
        super(context);
        init();
    }

    public UserDetailSeparator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
        linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        linePaint.setStrokeWidth((float) AndroidUtilities.dp(1));
        linePaint.setColor(0xffd0d0d0);
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= 16) {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                invalidate();
            }
        });
    }
    @Override
    protected void onDraw(Canvas canvas)
    {
        if(getWidth() == 0)
            return;
        canvas.drawLine((float) AndroidUtilities.dp(72), 0, (float) getMeasuredWidth(), (float) AndroidUtilities.dp(1), linePaint);
    }

}
