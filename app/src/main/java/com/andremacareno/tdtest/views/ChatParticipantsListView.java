package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.util.AttributeSet;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.listadapters.ExactlyHeightLayoutManager;
import com.andremacareno.tdtest.viewcontrollers.ViewControllable;
import com.andremacareno.tdtest.viewcontrollers.ViewController;

/**
 * Created by andremacareno on 04/04/15.
 */
public class ChatParticipantsListView extends BaseListView implements ViewControllable {
    private ViewController controller;
    private LayoutManager lm;
    public ChatParticipantsListView(Context context) {
        super(context);
        init();
    }

    public ChatParticipantsListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChatParticipantsListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init()
    {
        if(lm == null)
            lm = new ExactlyHeightLayoutManager(TelegramApplication.sharedApplication().getApplicationContext(), getResources().getDimensionPixelSize(R.dimen.default_list_item_height));
        setLayoutManager(lm);
        setItemAnimator(new DefaultItemAnimator());
    }
    @Override
    public void setController(ViewController vc)
    {
        this.controller = vc;
    }

    @Override
    public ViewController getController() {
        return controller;
    }
    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
    public void setNewElementCount(int count)
    {
        ((ExactlyHeightLayoutManager) lm).setSpecifiedHeight(count * getResources().getDimensionPixelSize(R.dimen.default_list_item_height));
    }
}
