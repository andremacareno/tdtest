package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.andremacareno.tdtest.AudioPlaybackService;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.AudioTrackModel;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 29/06/15.
 */
public class AudioDownloadView extends DownloadView {
    private final String downloadingStringFormat = getResources().getString(R.string.downloading_x_of_y);
    private String trackNameString;
    private String bandNameString;
    private AudioDownloadButton downloadButton;
    private TextView trackName;
    private TextView bandName;
    private RadialProgressView radialProgressView;
    private FileModel.FileModelDelegate delegate;
    private AudioPlaybackService.SimplePlaybackDelegate audioDelegate;

    public AudioDownloadView(Context context) {
        super(context);
    }

    public AudioDownloadView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void didDownloadStarted() {
        float progress = ((float) attachedFile.getDownloadedSize() / (float) attachedFile.getFullSize());
        radialProgressView.setVisibility(View.VISIBLE);
        radialProgressView.setProgress(progress, false);
        this.bandName.setText(String.format(this.downloadingStringFormat, CommonTools.sizeToString(attachedFile.getDownloadedSize()), CommonTools.sizeToString(attachedFile.getFullSize())));
    }

    @Override
    protected void didDownloadCancelled() {
        radialProgressView.setVisibility(View.GONE);
        this.bandName.setText(bandNameString);
    }

    @Override
    public DownloadButton getDownloadButton() {
        return this.downloadButton;
    }

    @Override
    protected FileModel.FileModelDelegate getDelegate() {
        if(delegate == null)
            delegate = new FileModel.FileModelDelegate() {
                @Override
                public String getDelegateKey() {
                    return generateDelegateKey();
                }
                @Override
                public void didFileDownloadStarted() {
                    didDownloadStarted();
                    getDownloadButton().invalidate();
                }
                @Override
                public void didFileProgressUpdated() {
                    if(attachedFile.getDownloadState() != FileModel.DownloadState.LOADING)
                        return;
                    float progress = ((float) attachedFile.getDownloadedSize() / (float) attachedFile.getFullSize());
                    bandName.setText(String.format(downloadingStringFormat, CommonTools.sizeToString(attachedFile.getDownloadedSize()), CommonTools.sizeToString(attachedFile.getFullSize())));
                    radialProgressView.setProgress(progress, true);
                }

                @Override
                public void didFileDownloaded() {
                    bandName.setText(bandNameString);
                    radialProgressView.setProgress(1.0f, false);
                    radialProgressView.setVisibility(View.GONE);
                    downloadButton.invalidate();
                }

                @Override
                public void didFileDownloadCancelled() {
                    didDownloadCancelled();
                    getDownloadButton().invalidate();
                }
            };
        return delegate;
    }

    @Override
    protected void init()
    {
        int DOWNLOAD_BTN_SIZE = getResources().getDimensionPixelSize(R.dimen.download_btn_size);
        View v = inflate(getContext(), R.layout.audioview, this);
        radialProgressView = (RadialProgressView) v.findViewById(R.id.radial_progress);
        radialProgressView.setProgressColor(ContextCompat.getColor(getContext(), R.color.dark_blue_bg));
        int oneDp = isInEditMode() ? 3 : AndroidUtilities.dp(1);
        radialProgressView.setProgressRect(oneDp, oneDp, DOWNLOAD_BTN_SIZE - oneDp, DOWNLOAD_BTN_SIZE - oneDp);
        radialProgressView.setProgress(0, false);
        radialProgressView.setBackground(getResources().getDrawable(R.color.transparent), true, true);
        radialProgressView.setVisibility(View.GONE);
        downloadButton = (AudioDownloadButton) v.findViewById(R.id.audio_download_btn);
        trackName = (TextView) v.findViewById(R.id.track_name);
        bandName = (TextView) v.findViewById(R.id.band_name);
    }
    @Override
    protected void fillLayout()
    {
        if(attachedFile == null)
            return;
        FileModel.DownloadState state = attachedFile.getDownloadState();
        this.trackName.setText(trackNameString);
        if(state == FileModel.DownloadState.LOADING)
        {
            radialProgressView.setVisibility(View.VISIBLE);
            radialProgressView.setProgress((float) attachedFile.getDownloadedSize() / attachedFile.getFullSize(), false);
            this.bandName.setText(String.format(this.downloadingStringFormat, CommonTools.sizeToString(attachedFile.getDownloadedSize()), CommonTools.sizeToString(attachedFile.getFullSize())));
        }
        else {
            radialProgressView.setVisibility(View.GONE);
            this.bandName.setText(bandNameString);
        }
    }
    @Override
    public void attachDelegateToFile(final FileModel file)
    {
        super.attachDelegateToFile(file);
        AudioPlaybackService playbackService = TelegramApplication.sharedApplication().getAudioPlaybackService();
        if(playbackService != null)
        {
            if(file == null && audioDelegate != null)
            {
                playbackService.removeSimpleDelegate(audioDelegate);
                return;
            }
            if(audioDelegate == null)
                audioDelegate = new AudioPlaybackService.SimplePlaybackDelegate() {
                    @Override
                    public void didPlaybackStarted(FileModel track) {
                        if(attachedFile.getFileId() == track.getFileId())
                            downloadButton.setPlaybackState(true);
                    }

                    @Override
                    public void didPlaybackPaused(FileModel track) {
                        if(attachedFile.getFileId() == track.getFileId())
                            downloadButton.setPlaybackState(false);
                    }

                    @Override
                    public void didPlaybackStopped() {
                        downloadButton.setPlaybackState(false);
                    }

                    @Override
                    public String getDelegateKey() {
                        return String.format("audiodelegate%d_%d", getAttachedMsgId(), file.getFileId());
                    }
                };
            playbackService.addSimpleDelegate(audioDelegate);
            AudioTrackModel nowPlaying = playbackService.nowPlaying();
            if(nowPlaying == null)
                downloadButton.setPlaybackState(false);
            else
            {
                if(!playbackService.isAudioPlaying())
                    downloadButton.setPlaybackState(false);
                else if(nowPlaying.getTrack().getFileId() == file.getFileId())
                    downloadButton.setPlaybackState(true);
                else
                    downloadButton.setPlaybackState(false);
            }
        }
    }
    public void setTrackName(String str) { this.trackNameString = str; }
    public void setBandName(String str) { this.bandNameString = str; }
}
