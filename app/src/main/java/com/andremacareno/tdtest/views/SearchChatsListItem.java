package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Emoji;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.ChatSearchResult;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 05/05/15.
 */
public class SearchChatsListItem extends FrameLayout {
    private final int AVATAR_POST_PROCESSING_MASK = TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE;
    private int avatarSize;
    private AvatarImageView avatar;
    private TextView chatName;
    private TextView unreadCount;
    private View unreadCountLayout;

    public SearchChatsListItem(Context context) {
        super(context);
        init();
    }
    public SearchChatsListItem(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    private void init()
    {
        View itemView = inflate(getContext(), R.layout.search_chat_result, this);
        setClickable(true);
        setBackgroundResource(R.drawable.list_selector);
        this.avatar = (AvatarImageView) itemView.findViewById(R.id.avatar);
        this.chatName = (TextView) itemView.findViewById(R.id.dialog_name);
        this.unreadCountLayout = itemView.findViewById(R.id.unread_count);
        this.unreadCount = (TextView) itemView.findViewById(R.id.unreadCountTextView);
        this.avatarSize = getResources().getDimensionPixelSize(R.dimen.avatar_size);
    }
    public AvatarImageView getAvatarImageView() { return this.avatar; }
    public void fillLayout(ChatSearchResult chat)
    {
        chatName.setText(Emoji.replaceEmoji(chat.getChatObj().title, chatName.getPaint().getFontMetricsInt(), AndroidUtilities.dp(16), false));
        if(chat.getChatObj().unreadCount <= 0)
            unreadCountLayout.setVisibility(View.GONE);
        else {
            unreadCountLayout.setVisibility(View.VISIBLE);
            unreadCountLayout.setBackgroundResource(R.drawable.ic_badge);
            unreadCount.setVisibility(View.VISIBLE);
            unreadCount.setText(String.valueOf(chat.getChatObj().unreadCount));
        }
    }
    public void loadAvatar(ChatSearchResult itm)
    {
        TdApi.File file = CommonTools.isLowDPIScreen() ? itm.getChatObj().photo.small : itm.getChatObj().photo.big;
        String newKey = CommonTools.makeFileKey(file.id);
        FileModel photo = FileCache.getInstance().getFileModel(file);
        TelegramImageLoader.getInstance().loadImage(photo, newKey, avatar, AVATAR_POST_PROCESSING_MASK, avatarSize, avatarSize, false);
    }
    public void loadPlaceholder(ChatSearchResult itm)
    {
        TelegramImageLoader.getInstance().loadPlaceholder(itm.getPeer(), itm.getInitials(), itm.getPlaceholderKey(), avatar, false);
    }
}
