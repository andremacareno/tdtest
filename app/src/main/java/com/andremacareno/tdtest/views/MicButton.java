package com.andremacareno.tdtest.views;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.MessageVoiceRecorder;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.fragments.BaseChatFragment;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 18/08/15.
 */
public class MicButton extends ImageButton {
    private OnTouchListener micButtonTouchListener;
    private MessageVoiceRecorder.RecordDelegate recordDelegate;
    private BigMicButton.BigMicButtonDelegate bigButtonDelegate;
    private BaseChatFragment.ChatDelegate chatDelegate;
    private boolean dragging = false;
    private float touchX;
    private float translationCancelThreshold;
    private boolean thresholdReached = false;
    private boolean recording = false;
    private static Handler startRecordingHandler = new Handler();

    public MicButton(Context context) {
        super(context);
        init();
    }

    public MicButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        translationCancelThreshold = ((float) TelegramApplication.sharedApplication().getResources().getDisplayMetrics().widthPixels) * 0.35f;
        recordDelegate = new MessageVoiceRecorder.RecordDelegate() {
            @Override
            public void onRecordStarted() {
                recording = true;
                if(bigButtonDelegate != null)
                    bigButtonDelegate.didRecordStarted();
                if(BuildConfig.DEBUG)
                {
                    Log.d("MicButton", "start notification received");
                }
            }

            @Override
            public void didVolumeReceived(float volumeRelative) {
                if(bigButtonDelegate != null)
                    bigButtonDelegate.didVolumeLevelReceived(volumeRelative);
                if(BuildConfig.DEBUG)
                    Log.d("MicButton", String.format("volume = %f", volumeRelative));
            }

            @Override
            public void didVoiceRecorded(TdApi.InputMessageVoice message) {
                recording = false;
                if(bigButtonDelegate != null)
                    bigButtonDelegate.didRecordStopped();
                /*if(BuildConfig.DEBUG)
                    Log.d("MicButton", "record finish notification received; msg: ".concat(message.toString()));*/
                if(chatDelegate != null)
                    chatDelegate.didVoiceMessageRecorded(message);
            }

            @Override
            public void onRecordError() {
                recording = false;
                if(bigButtonDelegate != null)
                    bigButtonDelegate.didRecordStopped();
            }
        };
        micButtonTouchListener = new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int mask = motionEvent.getAction() & MotionEvent.ACTION_MASK;
                if(mask == MotionEvent.ACTION_DOWN)
                {
                    touchX = motionEvent.getRawX();
                    startRecordingHandler.removeCallbacksAndMessages(null);
                    startRecordingHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dragging = true;
                            if(bigButtonDelegate != null)
                                bigButtonDelegate.didRecordStarted();
                            MessageVoiceRecorder.getInstance().startRecording(recordDelegate);
                        }
                    }, 200);
                    return true;
                }
                else if(mask == MotionEvent.ACTION_UP)
                {
                    startRecordingHandler.removeCallbacksAndMessages(null);
                    if(thresholdReached)
                    {
                        thresholdReached = false;
                        return false;
                    }
                    dragging = false;
                    if(bigButtonDelegate != null)
                        bigButtonDelegate.didRecordStopped();
                    if(bigButtonDelegate != null)
                    {
                        bigButtonDelegate.didMicButtonMoved(0, translationCancelThreshold);
                        setTranslationX(0);
                    }
                    if(recording)
                        MessageVoiceRecorder.getInstance().requestStopRecording();
                    return true;
                }
                else if(mask == MotionEvent.ACTION_MOVE)
                {
                    float translation = motionEvent.getRawX() - touchX;
                    if(!dragging)
                        return false;
                    if(bigButtonDelegate != null)
                    {
                        if(Math.abs(translation) >= translationCancelThreshold)
                        {
                            thresholdReached = true;
                            translation = 0;
                            if(recording)
                                MessageVoiceRecorder.getInstance().requestCancelRecording();
                        }
                        bigButtonDelegate.didMicButtonMoved(translation, translationCancelThreshold);
                        setTranslationX(translation);
                        if(thresholdReached)
                        {
                            dragging = false;
                            bigButtonDelegate.didRecordStopped();
                        }
                        return true;
                    }
                }
                return false;
            }
        };
        setImageDrawable(getResources().getDrawable(R.drawable.ic_mic));
        setOnTouchListener(micButtonTouchListener);
    }
    public void setBigButtonDelegate(BigMicButton.BigMicButtonDelegate delegate)
    {
        this.bigButtonDelegate = delegate;
    }

    public void setChatDelegate(BaseChatFragment.ChatDelegate delegate)
    {
        this.chatDelegate = delegate;
    }
}
