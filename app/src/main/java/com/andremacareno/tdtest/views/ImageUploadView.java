package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.FileModel;

/**
 * Created by andremacareno on 29/06/15.
 */
public class ImageUploadView extends UploadView {
    private String uploadingStringFormat;
    private TextView uploadingText;
    private ProgressBar uploadingProgress;
    private FileModel.FileModelDelegate delegate;
    public ImageUploadView(Context context) {
        super(context);
    }

    public ImageUploadView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void didUploadStarted() {
        setVisibility(View.VISIBLE);
        int progress = 100 * attachedFile.getDownloadedSize() / attachedFile.getFullSize();
        uploadingProgress.setProgress(progress);
        this.uploadingText.setText(String.format(this.uploadingStringFormat, progress));
    }

    @Override
    protected void didUploadCancelled() {
        setVisibility(View.GONE);
    }

    @Override
    protected FileModel.FileModelDelegate getDelegate() {
        if(delegate == null)
            delegate = new FileModel.FileModelDelegate() {
                @Override
                public String getDelegateKey() {
                    return generateDelegateKey();
                }

                @Override
                public void didFileDownloadStarted() {
                    didUploadStarted();
                }

                @Override
                public void didFileProgressUpdated() {
                    Log.d("ImageUploadView", "progress updated");
                    int progress = 100 * attachedFile.getDownloadedSize() / attachedFile.getFullSize();
                    if(progress == 100)
                        didUploadCancelled();
                    else
                    {
                        uploadingProgress.setProgress(progress);
                        uploadingText.setText(String.format(uploadingStringFormat, progress));
                    }
                }

                @Override
                public void didFileDownloaded() {
                    setVisibility(View.GONE);
                }

                @Override
                public void didFileDownloadCancelled() {
                    didUploadCancelled();
                }
            };
        return delegate;
    }

    @Override
    protected void init()
    {
        uploadingStringFormat = getResources().getString(R.string.uploading_format);
        View v = inflate(getContext(), R.layout.img_upload, this);
        uploadingProgress = (ProgressBar) v.findViewById(R.id.uploading_pb);
        uploadingText = (TextView) v.findViewById(R.id.uploading_text);
    }
    @Override
    protected void fillLayout()
    {
        if(attachedFile == null)
            return;
        setVisibility(View.VISIBLE);
        int progress = 100 * attachedFile.getDownloadedSize() / attachedFile.getFullSize();
        uploadingProgress.setProgress(progress);
        this.uploadingText.setText(String.format(this.uploadingStringFormat, progress));
    }
}
