package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by andremacareno on 22/06/15.
 */
public class ChannelLinkView extends FrameLayout {
    private TextView linkTextView;

    public ChannelLinkView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.channel_link, this);
        linkTextView = (TextView) v.findViewById(R.id.text);
    }
    public void setLink(String link)
    {
        linkTextView.setText(link);
    }
    public boolean isEmpty() { return linkTextView.getText().toString().isEmpty(); }
}
