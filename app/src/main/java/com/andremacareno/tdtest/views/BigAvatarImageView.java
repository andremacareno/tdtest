package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;

import com.andremacareno.tdtest.R;

/**
 * Created by Andrew on 25.04.2015.
 */
public class BigAvatarImageView extends AvatarImageView {
    public BigAvatarImageView(Context context) {
        super(context);
    }

    public BigAvatarImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    @Override
    public void onMeasure(int measureWidthSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(getResources().getDimensionPixelSize(R.dimen.big_avatar_size), getResources().getDimensionPixelSize(R.dimen.big_avatar_size));
    }
}
