package com.andremacareno.tdtest.views.actionviews;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.views.ChannelView;

/**
 * Created by andremacareno on 12/05/15.
 */
public class CreateChannelChooseTypeActionView extends ActionView {
    private static final String TAG = "ProfileActionView";
    public FrameLayout chatContent;
    public ChannelView channelView;
    public ImageButton backButton;
    public ImageButton menuButton;
    public ImageButton notificationsButton;


    public CreateChannelChooseTypeActionView(Context context) {
        super(context);
    }


    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void initView() {
        View v = inflate(getContext(), R.layout.action_view_channel_profile, this);
        chatContent = (FrameLayout) v.findViewById(R.id.chat_info);
        channelView = (ChannelView) v.findViewById(R.id.channelview);
        backButton = (ImageButton) v.findViewById(R.id.chat_back_btn);
        menuButton = (ImageButton) v.findViewById(R.id.menu_btn);
        notificationsButton = (ImageButton) v.findViewById(R.id.notifications_btn);
        notificationsButton.setVisibility(View.GONE);
        menuButton.setVisibility(View.GONE);
    }
    public void setBackButtonClickListener(OnClickListener listener)
    {
        backButton.setOnClickListener(listener);
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), AndroidUtilities.dp(148));
    }

}
