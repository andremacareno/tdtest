package com.andremacareno.tdtest.views;

import android.content.Context;
import android.widget.FrameLayout;

public class DummyFrameLayout extends FrameLayout {

    int cellHeight;

    public DummyFrameLayout(Context context) {
        this(context, 8);
    }

    public DummyFrameLayout(Context context, int height) {
        super(context);
        cellHeight = height;
    }

    public void setHeight(int height) {
        cellHeight = height;
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(cellHeight, MeasureSpec.EXACTLY));
    }
}