package com.andremacareno.tdtest.views.actionviews;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.views.AvatarImageView;

/**
 * Created by andremacareno on 12/05/15.
 */
public class ChatActionView extends ActionView {
    public interface ChatSearchViewDelegate
    {
        void searchAboveRequested();
        void searchBelowRequested();
        void didQuerySubmit(String query);
    }
    private static final String TAG = "ChatActionView";
    public AvatarImageView avatar;
    public FrameLayout chatContent;
    public FrameLayout chatSearch;
    public TextView firstLine;
    public TextView secondLine;
    public ImageButton backButton;
    public ImageButton menuButton;
    private PopupMenu contextMenu = null;
    public EditText searchEditText;
    public View searchAbove, searchBelow;
    private ChatSearchViewDelegate searchDelegate;

    public View selectionModeContainer;
    public ImageButton disableButton;
    public TextView mediaCountTextView;
    public ImageButton forwardButton;
    public ImageButton delButton;
    private final AccelerateInterpolator accInterpolator = new AccelerateInterpolator(1.7f);
    private final DecelerateInterpolator decInterpolator = new DecelerateInterpolator(1.3f);

    public ChatActionView(Context context) {
        super(context);
    }


    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void initView() {
        View v = inflate(getContext(), R.layout.action_view_chat, this);
        chatContent = (FrameLayout) v.findViewById(R.id.chat_info);
        chatSearch = (FrameLayout) v.findViewById(R.id.chat_search);
        avatar = (AvatarImageView) chatContent.findViewById(R.id.avatar);
        firstLine = (TextView) chatContent.findViewById(R.id.chat_title);
        secondLine = (TextView) chatContent.findViewById(R.id.chat_subtitle);
        backButton = (ImageButton) v.findViewById(R.id.chat_back_btn);
        menuButton = (ImageButton) v.findViewById(R.id.chat_menu);
        menuButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            if(contextMenu != null)
                contextMenu.show();
            }
        });
        searchEditText = (EditText) chatSearch.findViewById(R.id.chat_search_edittext);
        searchAbove = chatSearch.findViewById(R.id.search_above);
        searchBelow = chatSearch.findViewById(R.id.search_below);
        View.OnClickListener onClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(searchDelegate == null)
                    return;
                if(view == searchAbove)
                    searchDelegate.searchAboveRequested();
                else if(view == searchBelow)
                    searchDelegate.searchBelowRequested();
            }
        };
        searchAbove.setOnClickListener(onClickListener);
        searchBelow.setOnClickListener(onClickListener);
        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i == EditorInfo.IME_ACTION_SEARCH && searchDelegate != null)
                {
                    String str = textView.getText().toString();
                    if(str.length() > 0)
                        searchDelegate.didQuerySubmit(str);
                    return true;
                }
                return false;
            }
        });
        selectionModeContainer = v.findViewById(R.id.chat_selection_mode);
        disableButton = (ImageButton) selectionModeContainer.findViewById(R.id.disable_multiselection);
        mediaCountTextView = (TextView) selectionModeContainer.findViewById(R.id.selection_count);
        forwardButton = (ImageButton) selectionModeContainer.findViewById(R.id.fwd_selection);
        delButton = (ImageButton) selectionModeContainer.findViewById(R.id.del_selection);
    }
    public void setBackButtonClickListener(OnClickListener listener)
    {
        backButton.setOnClickListener(listener);
    }
    public void setMiddlePartClickListener(OnClickListener listener)
    {
        chatContent.setOnClickListener(listener);
    }
    public void setClearSelectionClickListener(OnClickListener listener)
    {
        disableButton.setOnClickListener(listener);
    }
    public void setMenu(PopupMenu menu)
    {
        this.contextMenu = menu;
    }
    public void setSearchMode(boolean search)
    {
        if(search)
        {
            menuButton.setVisibility(View.INVISIBLE);
            chatContent.setVisibility(View.INVISIBLE);
            chatSearch.setVisibility(View.VISIBLE);
            focus();
        }
        else
        {
            chatSearch.setVisibility(View.INVISIBLE);
            clear(false);
            menuButton.setVisibility(View.VISIBLE);
            chatContent.setVisibility(View.VISIBLE);
        }
    }
    public void focus()
    {
        searchEditText.requestFocus();
        //AndroidUtilities.showKeyboard(searchEditText);
        ((InputMethodManager) searchEditText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }
    public void clear(boolean nextFragment)
    {
        if(!nextFragment)
            searchEditText.setText("");
        AndroidUtilities.hideKeyboard(searchEditText);
    }
    public void setDelegate(ChatSearchViewDelegate delegateRef) { this.searchDelegate = delegateRef; }
    public void setSelectionMode(boolean selectionMode, boolean animated)
    {
        if(selectionMode) {
            if(animated)
                slideDownSelectionBar();
            else
                selectionModeContainer.setTranslationY(0);
        }
        else
        {
            if(animated)
                slideUpSelectionBar();
            else
                selectionModeContainer.setTranslationY(-AndroidUtilities.dp(56));
        }
    }

    private void slideDownSelectionBar()
    {
        ObjectAnimator animator = ObjectAnimator.ofFloat(selectionModeContainer, "translationY", -AndroidUtilities.dp(56), 0).setDuration(180);
        animator.setInterpolator(decInterpolator);
        animator.start();
        selectionModeContainer.setVisibility(View.VISIBLE);
    }
    private void slideUpSelectionBar()
    {
        ObjectAnimator animator = ObjectAnimator.ofFloat(selectionModeContainer, "translationY", 0, -AndroidUtilities.dp(56)).setDuration(180);
        animator.setInterpolator(accInterpolator);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                //selectionModeContainer.setVisibility(View.INVISIBLE);
            }
        });
        animator.start();
        animator.setInterpolator(accInterpolator);
        animator.start();
    }
    public void changeSelectionCount(int newCount)
    {
        mediaCountTextView.setText(String.valueOf(newCount));
    }
    public void setDelButtonVisibility(boolean show)
    {
        delButton.setVisibility(show ? View.VISIBLE : View.GONE);
    }
    public void setDelButtonClickListener(View.OnClickListener listener)
    {
        delButton.setOnClickListener(listener);
    }
    public void setShareButtonClickListener(View.OnClickListener listener)
    {
        forwardButton.setOnClickListener(listener);
    }
}
