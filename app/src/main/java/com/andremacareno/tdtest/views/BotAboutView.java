package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by andremacareno on 22/06/15.
 */
public class BotAboutView extends FrameLayout {
    private TextView bot_info;

    public BotAboutView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.user_details_about, this);
        bot_info = (TextView) v.findViewById(R.id.text);
    }
    public void setAbout(String about)
    {
        bot_info.setText(about);
    }

}
