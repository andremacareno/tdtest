package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.images.RecyclingImageView;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 29/06/15.
 */
public class VideoDownloadView extends DownloadView {
    private VideoDownloadButton downloadButton;
    private RecyclingImageView thumbImageView;
    private RadialProgressView radialProgressView;
    private FileModel.FileModelDelegate delegate;
    public VideoDownloadView(Context context) {
        super(context);
    }

    public VideoDownloadView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void didDownloadStarted() {
        radialProgressView.setVisibility(View.VISIBLE);
        radialProgressView.setProgress((float) attachedFile.getDownloadedSize() / attachedFile.getFullSize(), false);
        radialProgressView.setProgressColor(ContextCompat.getColor(getContext(), R.color.white));
    }

    @Override
    protected void didDownloadCancelled() {
        radialProgressView.setProgress(1.0f, false);
        radialProgressView.setVisibility(View.GONE);
    }

    @Override
    public DownloadButton getDownloadButton() {
        return this.downloadButton;
    }

    @Override
    protected FileModel.FileModelDelegate getDelegate() {
        if(delegate == null)
            delegate = new FileModel.FileModelDelegate() {
                @Override
                public String getDelegateKey() {
                    return generateDelegateKey();
                }

                @Override
                public void didFileDownloadStarted() {
                    didDownloadStarted();
                    getDownloadButton().invalidate();
                }

                @Override
                public void didFileProgressUpdated() {
                    if(attachedFile.getDownloadState() != FileModel.DownloadState.LOADING)
                        return;
                    radialProgressView.setProgress((float) attachedFile.getDownloadedSize() / attachedFile.getFullSize(), true);
                }

                @Override
                public void didFileDownloaded() {
                    radialProgressView.setProgress(1.0f, false);
                    radialProgressView.setVisibility(View.GONE);
                    downloadButton.invalidate();
                }

                @Override
                public void didFileDownloadCancelled() {
                    didDownloadCancelled();
                    getDownloadButton().invalidate();
                }
            };
        return delegate;
    }

    @Override
    protected void init()
    {
        View v = inflate(getContext(), R.layout.video_downloadview, this);
        radialProgressView = (RadialProgressView) v.findViewById(R.id.radial_progress);
        downloadButton = (VideoDownloadButton) v.findViewById(R.id.ic_dl);
        thumbImageView = (RecyclingImageView) v.findViewById(R.id.msg_video_background);
    }
    @Override
    protected void fillLayout()
    {
        if(attachedFile == null)
            return;
        FileModel.DownloadState state = attachedFile.getDownloadState();
        if(state != FileModel.DownloadState.LOADING)
        {
            radialProgressView.setVisibility(View.GONE);
        }
        else
        {
            radialProgressView.setVisibility(View.VISIBLE);
            radialProgressView.setProgress((float) attachedFile.getDownloadedSize() / attachedFile.getFullSize(), false);
        }
        int DOWNLOAD_BTN_SIZE = getResources().getDimensionPixelSize(R.dimen.download_btn_size);
        int oneDp = AndroidUtilities.dp(1);
        radialProgressView.setProgressRect(oneDp, oneDp, DOWNLOAD_BTN_SIZE-oneDp, DOWNLOAD_BTN_SIZE-oneDp);
        radialProgressView.setProgressColor(ContextCompat.getColor(getContext(), R.color.white));
        radialProgressView.setProgress(0, false);
        radialProgressView.setBackground(getResources().getDrawable(R.color.transparent), true, true);
    }

    public RecyclingImageView getThumbImageView() { return this.thumbImageView; }
}
