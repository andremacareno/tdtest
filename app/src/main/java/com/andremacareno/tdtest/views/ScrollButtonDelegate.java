package com.andremacareno.tdtest.views;

/**
 * Created by andremacareno on 20/05/15.
 */
public interface ScrollButtonDelegate {
    public void onChatScroll(int itemsBelow, int itemsTotal);
}
