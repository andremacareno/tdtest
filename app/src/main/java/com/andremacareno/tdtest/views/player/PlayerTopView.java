package com.andremacareno.tdtest.views.player;

import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.images.RecyclingImageView;

/**
 * Created by andremacareno on 15/08/15.
 */
public class PlayerTopView extends FrameLayout {
    private RecyclingImageView albumArtImageView;
    private ImageButton backButton;
    private ImageButton playlistButton;
    private ImageButton repeatButton;
    private ImageButton shuffleButton;

    private boolean shuffleActive = false;
    private boolean repeatActive = false;
    private boolean darkMode = false;
    private PlayerView.PlayerDelegate delegate;
    public PlayerTopView(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        setBackgroundResource(R.color.player_top_bg);
        View v = inflate(getContext(), R.layout.player_top, this);
        albumArtImageView = (RecyclingImageView) v.findViewById(R.id.cover);
        backButton = (ImageButton) v.findViewById(R.id.back);
        playlistButton = (ImageButton) v.findViewById(R.id.playlist);
        repeatButton = (ImageButton) v.findViewById(R.id.repeat);
        shuffleButton = (ImageButton) v.findViewById(R.id.shuffle);
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(delegate != null)
                    delegate.didBackButtonPressed();
            }
        });
        playlistButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(delegate != null)
                    delegate.didPlaylistButtonPressed();
            }
        });
        repeatButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(delegate != null) {
                    delegate.didRepeatButtonPressed();
                    updateRepeatButtonState(!repeatActive);
                }
            }
        });
        shuffleButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(delegate != null) {
                    delegate.didShuffleButtonPressed();
                    updateShuffleButtonState(!shuffleActive);
                }
            }
        });
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= 16) {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                getLayoutParams().width = getWidth();

                getLayoutParams().height = getWidth();
                requestLayout();
            }
        });
    }
    public RecyclingImageView getCoverView() { return this.albumArtImageView; }
    public void setDelegate(PlayerView.PlayerDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    public void updateRepeatButtonState(boolean active)
    {
        repeatActive = active;
        if(active)
            repeatButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_repeat_blue));
        else
            repeatButton.setImageDrawable(getResources().getDrawable(darkMode ? R.drawable.ic_repeat_white : R.drawable.ic_repeat_grey));
    }
    public void updateShuffleButtonState(boolean active)
    {
        shuffleActive = active;
        if(active)
            shuffleButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_shuffle_blue));
        else
            shuffleButton.setImageDrawable(getResources().getDrawable(darkMode ? R.drawable.ic_shuffle_white : R.drawable.ic_shuffle_grey));
    }

    public void changeButtons(boolean dark) {
        if(darkMode == dark)
            return;
        darkMode = dark;
        backButton.setImageDrawable(getResources().getDrawable(dark ? R.drawable.ic_back : R.drawable.ic_back_grey));
        playlistButton.setImageDrawable(getResources().getDrawable(dark ? R.drawable.ic_playlist_white : R.drawable.ic_playlist_grey));
        if(!shuffleActive)
            shuffleButton.setImageDrawable(getResources().getDrawable(dark ? R.drawable.ic_shuffle_white : R.drawable.ic_shuffle_grey));
        if(!repeatActive)
            repeatButton.setImageDrawable(getResources().getDrawable(dark ? R.drawable.ic_repeat_white : R.drawable.ic_repeat_grey));
    }
}
