package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.util.AttributeSet;

import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.listadapters.ExactlyHeightLayoutManager;
import com.andremacareno.tdtest.viewcontrollers.SettingsListController;
import com.andremacareno.tdtest.viewcontrollers.ViewControllable;
import com.andremacareno.tdtest.viewcontrollers.ViewController;

/**
 * Created by andremacareno on 04/04/15.
 */
public class SettingsListView extends BaseListView implements ViewControllable {
    private SettingsListController controller;
    private LayoutManager lm;
    public SettingsListView(Context context) {
        super(context);
        init();
    }

    public SettingsListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SettingsListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init()
    {
        if(isInEditMode())
            return;
        if(lm == null)
            lm = new ExactlyHeightLayoutManager(TelegramApplication.sharedApplication().getApplicationContext());
        setLayoutManager(lm);
        setItemAnimator(new DefaultItemAnimator());
    }
    @Override
    public void setController(ViewController vc)
    {
        this.controller = (SettingsListController) vc;
    }

    @Override
    public ViewController getController() {
        return controller;
    }
}
