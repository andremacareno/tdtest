package com.andremacareno.tdtest.views.player;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.images.RecyclingImageView;
import com.andremacareno.tdtest.models.AudioTrackModel;

/**
 * Created by andremacareno on 15/08/15.
 */
public class PlayerView extends RelativeLayout {
    public interface PlayerDelegate
    {
        public void didPlayButtonPressed();
        public void didPreviousTrackButtonPressed();
        public void didNextTrackButtonPressed();
        public void didBackButtonPressed();
        public void didPlaylistButtonPressed();
        public void didRepeatButtonPressed();
        public void didShuffleButtonPressed();
        public void didSeekBarMoved(int timestamp);
        public void didSeekBarDragStarted();
        public void didSeekBarDragEnded();
        public void didCoverDarknessDetected(boolean dark);
    }
    private PlayerTopView top;
    private SeekBar seekBar;
    private PlayerBottomView bottom;
    private PlayerDelegate delegate;
    public PlayerView(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        RelativeLayout.LayoutParams topLP = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        topLP.addRule(ALIGN_PARENT_TOP);
        top = new PlayerTopView(getContext());
        top.setLayoutParams(topLP);
        top.setId(R.id.player_top_part);
        seekBar = (SeekBar) LayoutInflater.from(getContext()).inflate(R.layout.player_seekbar, this, false);
        bottom = new PlayerBottomView(getContext());
        bottom.setId(R.id.player_bottom_part);
        ((RelativeLayout.LayoutParams)bottom.getLayoutParams()).addRule(RelativeLayout.BELOW, R.id.player_top_part);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean fromUser) {
                if(delegate != null && fromUser)
                    delegate.didSeekBarMoved(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if(delegate != null)
                    delegate.didSeekBarDragStarted();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(delegate != null)
                    delegate.didSeekBarDragEnded();
            }
        });
        ((RelativeLayout.LayoutParams)seekBar.getLayoutParams()).addRule(RelativeLayout.BELOW, R.id.player_top_part);
        addView(top);
        addView(bottom);
        addView(seekBar);
    }
    public void setDelegate(PlayerDelegate delegateRef)
    {
        this.delegate = delegateRef;
        top.setDelegate(delegate);
        bottom.setDelegate(delegate);
    }
    public void setSeekBarMax(int max)
    {
        seekBar.setMax(max);
    }
    public void updateSeekBar(int progress)
    {
        seekBar.setProgress(progress);
    }
    public void updatePlayButton(boolean isPlaying)
    {
        bottom.updatePlayButton(isPlaying);
    }
    public void updateTime(String elapsed, String remaining)
    {
        bottom.updateTime(elapsed, remaining);
    }
    public void updateSongData(AudioTrackModel track)
    {
        bottom.bindTrack(track);
    }
    public RecyclingImageView getCoverView() { return this.top.getCoverView(); }
    public int getSeekBarPosition()
    {
        return seekBar.getProgress();
    }
    public void updateRepeatButtonState(boolean active)
    {
        top.updateRepeatButtonState(active);
    }
    public void updateShuffleButtonState(boolean active)
    {
        top.updateShuffleButtonState(active);
    }
    public void changeButtons(boolean dark) { top.changeButtons(dark); }
}
