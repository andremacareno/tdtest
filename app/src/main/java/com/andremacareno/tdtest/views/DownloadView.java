package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.models.FileModel;

/**
 * Created by andremacareno on 17/08/15.
 */
public abstract class DownloadView extends FrameLayout {
    protected FileModel attachedFile;
    private int attachedMsgId;
    public DownloadView(Context context) {
        super(context);
        init();
    }

    public DownloadView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public void attachDelegateToFile(FileModel file)
    {
        if(attachedFile != null)
            attachedFile.removeDelegate(getDelegate());
        if(file == null)
            return;
        this.attachedFile = file;
        attachedFile.addDelegate(getDelegate());
        getDownloadButton().attachFile(file);
        fillLayout();
    }
    public void startDownload()
    {
        FileCache.getInstance().downloadFile(attachedFile);
        didDownloadStarted();
        getDownloadButton().invalidate();
    }
    public void cancelDownload()
    {
        FileCache.getInstance().cancelDownload(attachedFile);
        didDownloadCancelled();
        getDownloadButton().invalidate();
    }
    public void setAttachedMsgId(int msgId)
    {
        this.attachedMsgId = msgId;
    }
    public int getAttachedMsgId() { return this.attachedMsgId; }
    protected String generateDelegateKey()
    {
        return String.format("file%d_%d_delegate", getAttachedMsgId(), attachedFile == null ? 0 : attachedFile.getFileId());
    }
    protected abstract void didDownloadStarted();
    protected abstract void didDownloadCancelled();
    public abstract DownloadButton getDownloadButton();
    protected abstract void fillLayout();
    protected abstract FileModel.FileModelDelegate getDelegate();
    protected abstract void init();
}
