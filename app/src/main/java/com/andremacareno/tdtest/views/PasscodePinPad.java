package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v7.widget.GridLayout;
import android.util.AttributeSet;
import android.view.View;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.viewcontrollers.passcode.PasscodePinPadController;

/**
 * Created by Andrew on 12.05.2015.
 */
public class PasscodePinPad extends GridLayout {
    public enum PinPadButton {ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, ZERO, BACKSPACE}
    //buttons
    private PasscodePinButton one;
    private PasscodePinButton two;
    private PasscodePinButton three;
    private PasscodePinButton four;
    private PasscodePinButton five;
    private PasscodePinButton six;
    private PasscodePinButton seven;
    private PasscodePinButton eight;
    private PasscodePinButton nine;
    private PasscodePinButton zero;
    private PasscodePinButton bs;

    private OnClickListener pinButtonClickListener;
    //pin pad controller
    private PasscodePinPadController controller;


    public PasscodePinPad(Context context) {
        super(context);
        init();
    }

    public PasscodePinPad(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PasscodePinPad(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    public void setPinPadControllerReference(PasscodePinPadController controllerRef)
    {
        this.controller = controllerRef;
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.passcode_pinpad, this);
        one = (PasscodePinButton) v.findViewById(R.id.passcode_one);
        two = (PasscodePinButton) v.findViewById(R.id.passcode_two);
        three = (PasscodePinButton) v.findViewById(R.id.passcode_three);
        four = (PasscodePinButton) v.findViewById(R.id.passcode_four);
        five = (PasscodePinButton) v.findViewById(R.id.passcode_five);
        six = (PasscodePinButton) v.findViewById(R.id.passcode_six);
        seven = (PasscodePinButton) v.findViewById(R.id.passcode_seven);
        eight = (PasscodePinButton) v.findViewById(R.id.passcode_eight);
        nine = (PasscodePinButton) v.findViewById(R.id.passcode_nine);
        zero = (PasscodePinButton) v.findViewById(R.id.passcode_zero);
        bs = (PasscodePinButton) v.findViewById(R.id.passcode_bs);

        pinButtonClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(controller == null)
                    return;
                int id = view.getId();
                if(id == R.id.passcode_one)
                    controller.didPinPadButtonPressed(PinPadButton.ONE);
                else if(id == R.id.passcode_two)
                    controller.didPinPadButtonPressed(PinPadButton.TWO);
                else if(id == R.id.passcode_three)
                    controller.didPinPadButtonPressed(PinPadButton.THREE);
                else if(id == R.id.passcode_four)
                    controller.didPinPadButtonPressed(PinPadButton.FOUR);
                else if(id == R.id.passcode_five)
                    controller.didPinPadButtonPressed(PinPadButton.FIVE);
                else if(id == R.id.passcode_six)
                    controller.didPinPadButtonPressed(PinPadButton.SIX);
                else if(id == R.id.passcode_seven)
                    controller.didPinPadButtonPressed(PinPadButton.SEVEN);
                else if(id == R.id.passcode_eight)
                    controller.didPinPadButtonPressed(PinPadButton.EIGHT);
                else if(id == R.id.passcode_nine)
                    controller.didPinPadButtonPressed(PinPadButton.NINE);
                else if(id == R.id.passcode_zero)
                    controller.didPinPadButtonPressed(PinPadButton.ZERO);
                else
                    controller.didPinPadButtonPressed(PinPadButton.BACKSPACE);
            }
        };
        one.setOnClickListener(pinButtonClickListener);
        two.setOnClickListener(pinButtonClickListener);
        three.setOnClickListener(pinButtonClickListener);
        four.setOnClickListener(pinButtonClickListener);
        five.setOnClickListener(pinButtonClickListener);
        six.setOnClickListener(pinButtonClickListener);
        seven.setOnClickListener(pinButtonClickListener);
        eight.setOnClickListener(pinButtonClickListener);
        nine.setOnClickListener(pinButtonClickListener);
        zero.setOnClickListener(pinButtonClickListener);
        bs.setOnClickListener(pinButtonClickListener);
    }
}
