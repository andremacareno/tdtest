package com.andremacareno.tdtest.views;
/*
	Based on: https://github.com/TUBB/SwipeMenuRecyclerView/blob/master/library/src/main/java/com/tubb/smrv/SwipeMenuLayout.java
	Modified by Andre Macareno to support bidirectional swipe
 */

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ScrollerCompat;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;

import com.andremacareno.tdtest.R;

public class SwipeMenuLayout extends FrameLayout {

	public static final int STATE_CLOSE = 0;
	public static final int STATE_OPEN = 1;

	private View mContentView;
	private boolean opening = false;
	private View mLeftMenuView;
	private View mRightMenuView;
	private View mMenuView;
	private int mDownX;
	private int state = STATE_CLOSE;
	private GestureDetectorCompat mGestureDetector;
	private GestureDetector.OnGestureListener mGestureListener;
	private boolean isFling;
	private ScrollerCompat mOpenScroller;
	private ScrollerCompat mCloseScroller;
	private int mBaseX;
	private Interpolator interpolator;
	private ViewConfiguration mViewConfiguration;
	private boolean swipeEnable = true;
	private int animDuration = 180;
	private boolean disableRight = false;
	private boolean disableLeft = false;

	public SwipeMenuLayout(Context context) {
		super(context);
		init();
	}
	private void init()
	{
		interpolator = new DecelerateInterpolator(1.1f);
		mCloseScroller = ScrollerCompat.create(getContext(), interpolator);
		mOpenScroller = ScrollerCompat.create(getContext(), interpolator);
		mGestureListener = new GestureDetector.SimpleOnGestureListener() {
			@Override
			public boolean onDown(MotionEvent e) {
				isFling = false;
				return true;
			}
			@Override
			public void onLongPress(MotionEvent e)
			{
				isFling = false;
			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2,
								   float velocityX, float velocityY) {
				if(mViewConfiguration == null)
					return false;
				if (velocityX > mViewConfiguration.getScaledMinimumFlingVelocity() || velocityY > mViewConfiguration.getScaledMinimumFlingVelocity())
					isFling = true;
				return isFling;
			}
		};
		mGestureDetector = new GestureDetectorCompat(getContext(),
				mGestureListener);
	}
	public void didViewComplete() {
		setClickable(true);
		mContentView = findViewById(R.id.smContentView);
		if (mContentView == null) {
			return;
		}
		mRightMenuView = findViewById(R.id.smRightMenuView);
		if (mRightMenuView == null) {
			return;
		}
		mLeftMenuView = findViewById(R.id.smLeftMenuView);
		if (mLeftMenuView == null) {
			return;
		}
		mViewConfiguration = ViewConfiguration.get(getContext());
	}

	public boolean onSwipe(MotionEvent event) {
		mGestureDetector.onTouchEvent(event);
		if(mLeftMenuView == null || mContentView == null || mRightMenuView == null)
			return false;
		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				mDownX = (int) event.getRawX();
				isFling = false;
				return false;
			case MotionEvent.ACTION_MOVE:
				int dis = (int) (mDownX - event.getRawX());
				if(dis < 0) {
					if(state == STATE_CLOSE && !disableLeft)
						mMenuView = mLeftMenuView;
				}
				else if(dis > 0) {
					if(state == STATE_CLOSE && !disableRight)
						mMenuView = mRightMenuView;
				}
				if(Math.abs(dis) > 10)
					opening = true;
				if (state == STATE_OPEN && mMenuView != null) {
					dis += mMenuView.getWidth() * (mMenuView == mRightMenuView ? 1 : -1);
				}
				swipe(dis);
				break;
			case MotionEvent.ACTION_UP:
				opening = false;
				if ((isFling || (mMenuView != null && Math.abs(mDownX - event.getRawX()) > (mMenuView.getWidth() / 3)))) {

					if(Math.signum(mDownX - event.getRawX()) == SwipeMenuRecyclerView.DIRECTION_LEFT)
					{
						if(mMenuView == mLeftMenuView && state == STATE_OPEN)
							smoothCloseMenu();
						else if(mMenuView == mRightMenuView && state == STATE_CLOSE)
							smoothOpenMenu();
					}
					else
					{
						if(mMenuView == mLeftMenuView && state == STATE_CLOSE)
							smoothOpenMenu();
						else if(mMenuView == mRightMenuView && state == STATE_OPEN)
							smoothCloseMenu();
					}
				} else {
					smoothCloseMenu();
					return false;
				}
				break;
		}
		return true;
	}

	public boolean isOpen() {
		return state == STATE_OPEN;
	}

	private void swipe(int dis) {
		if(mMenuView == null || mContentView == null)
			return;
		if (Math.signum(dis) != (mMenuView == mRightMenuView ? 1 : -1)) {
			dis = 0;
		}
		else if (mMenuView != null && Math.abs(dis) > mMenuView.getWidth()) {
			dis = mMenuView.getWidth() * (mMenuView == mRightMenuView ? 1 : -1);
			state = STATE_OPEN;
		}

		LayoutParams lp = (LayoutParams) mContentView.getLayoutParams();
		int lGap = getPaddingLeft() + lp.leftMargin;
		mContentView.layout(lGap - dis,
				mContentView.getTop(),
				lGap + ViewCompat.getMeasuredWidthAndState(mContentView) - dis,
				mContentView.getBottom());
		//mSwipeDirection == SwipeMenuRecyclerView.DIRECTION_LEFT; else
		if (mMenuView == mRightMenuView) {
			mMenuView.layout(getMeasuredWidth() - dis, mMenuView.getTop(),
					getMeasuredWidth() + ViewCompat.getMeasuredWidthAndState(mMenuView) - dis,
					mMenuView.getBottom());
		} else if(mMenuView == mLeftMenuView) {
			mMenuView.layout(-ViewCompat.getMeasuredWidthAndState(mMenuView) - dis, mMenuView.getTop(),
					-dis, mMenuView.getBottom());
		}
	}
	public boolean isOpening() { return opening; }
	@Override
	public void computeScroll() {
		if (state == STATE_OPEN) {
			if (mOpenScroller.computeScrollOffset()) {
				swipe(mOpenScroller.getCurrX() * (mMenuView == mRightMenuView ? 1 : -1));
				postInvalidate();
			}
		} else {
			if (mCloseScroller.computeScrollOffset()) {
				swipe((mBaseX - mCloseScroller.getCurrX()) * (mMenuView == mRightMenuView ? 1 : -1));
				postInvalidate();
			}
		}
	}

	public void smoothCloseMenu() {
		closeOpenedMenu();
	}

	public void closeOpenedMenu() {
		state = STATE_CLOSE;
		if (mMenuView == mRightMenuView) {
			mBaseX = -mContentView.getLeft();
			mCloseScroller.startScroll(0, 0, mRightMenuView.getWidth(), 0, animDuration);
		} else {
			mBaseX = mLeftMenuView.getRight();
			mCloseScroller.startScroll(0, 0, mLeftMenuView.getWidth(), 0, animDuration);
		}
		postInvalidate();
	}

	public void smoothOpenMenu() {
		state = STATE_OPEN;
		if (mMenuView == mRightMenuView) {
			mOpenScroller.startScroll(-mContentView.getLeft(), 0, mRightMenuView.getWidth(), 0, animDuration);
		} else {
			mOpenScroller.startScroll(mContentView.getLeft(), 0, mLeftMenuView.getWidth(), 0, animDuration);
		}
		postInvalidate();
	}

	public View getRightMenuView() {
		return mRightMenuView;
	}
	public View getLeftMenuView() {
		return mLeftMenuView;
	}

	public View getContentView() {
		return mContentView;
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		if(mContentView == null)
			return;
		LayoutParams lp = (LayoutParams) mContentView.getLayoutParams();
		int lGap = getPaddingLeft() + lp.leftMargin;
		int tGap = getPaddingTop() + lp.topMargin;
		mContentView.layout(lGap,
				tGap,
				lGap + ViewCompat.getMeasuredWidthAndState(mContentView),
				tGap + ViewCompat.getMeasuredHeightAndState(mContentView));
		lp = (LayoutParams) mRightMenuView.getLayoutParams();
		tGap = getPaddingTop() + lp.topMargin;
		mRightMenuView.layout(getMeasuredWidth(), tGap,
				getMeasuredWidth() + ViewCompat.getMeasuredWidthAndState(mRightMenuView),
				tGap + ViewCompat.getMeasuredHeightAndState(mRightMenuView));
		lp = (LayoutParams) mLeftMenuView.getLayoutParams();
		tGap = getPaddingTop() + lp.topMargin;
		mLeftMenuView.layout(-ViewCompat.getMeasuredWidthAndState(mLeftMenuView), tGap,
				0, tGap + ViewCompat.getMeasuredHeightAndState(mLeftMenuView));
	}

	public void setSwipeEnable(boolean swipeEnable) {
		this.swipeEnable = swipeEnable;
	}
	public void setDisableRightButton(boolean disable)
	{
		this.disableRight = disable;
	}
	public void setDisableLeftButton(boolean disable)
	{
		this.disableLeft = disable;
	}

	public boolean isSwipeEnable() {
		return swipeEnable;
	}

}