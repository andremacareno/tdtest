package com.andremacareno.tdtest.views;

import android.support.v4.widget.DrawerLayout;
import android.view.View;

import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;

/**
 * Created by Andrew on 11.05.2015.
 */
public class NavDrawerListener extends NotificationObserver implements DrawerLayout.DrawerListener {
    private int actionToSend;
    private boolean dontNotify = true;
    private Runnable closeRunnable;
    private Runnable refreshAvatarRunnable;
    public NavDrawerListener(Runnable closeDrawableRunnable, Runnable refreshAvatarRunnable) {
        super(NotificationCenter.didNavDrawerActionSelected);
        this.refreshAvatarRunnable = refreshAvatarRunnable;
        this.closeRunnable = closeDrawableRunnable;
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        if(refreshAvatarRunnable != null)
            refreshAvatarRunnable.run();
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        if(dontNotify)
            return;
        dontNotify = true;
        NotificationCenter.getInstance().postNotification(NotificationCenter.didNavDrawerClosed, actionToSend);
    }

    @Override
    public void onDrawerStateChanged(int newState) {
    }
    public void setAction(int act) {
        dontNotify = false;
        this.actionToSend = act;
    }

    @Override
    public void didNotificationReceived(Notification notification) {
        setAction((int) notification.getObject());
        closeRunnable.run();
    }
}
