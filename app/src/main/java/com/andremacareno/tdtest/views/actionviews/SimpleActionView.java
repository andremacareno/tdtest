package com.andremacareno.tdtest.views.actionviews;

import android.content.Context;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by Andrew on 13.05.2015.
 */
public abstract class SimpleActionView extends ActionView {
    public ImageButton doneButton;
    public ImageButton backButton;
    public ImageButton menuButton;
    public TextView title;
    public SimpleActionView(Context context) {
        super(context);
    }
    @Override
    protected void initView()
    {
        View v = inflate(getContext(), R.layout.simple_action_view, this);
        backButton = (ImageButton) v.findViewById(R.id.back);
        doneButton = (ImageButton) v.findViewById(R.id.done);
        menuButton = (ImageButton) v.findViewById(R.id.menu);
        title = (TextView) v.findViewById(R.id.title);
    }
}
