package com.andremacareno.tdtest.views;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 29/06/15.
 */
public class VoiceDownloadView extends DownloadView {
    private String durationString;
    private AudioDownloadButton downloadButton;
    private TextView duration;
    private SeekBar audioSeekbar;
    private RadialProgressView radialProgressView;
    private FileModel.FileModelDelegate delegate;
    public VoiceDownloadView(Context context) {
        super(context);
    }

    public VoiceDownloadView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void didDownloadStarted() {
        radialProgressView.setVisibility(VISIBLE);
        radialProgressView.setProgress((float) attachedFile.getDownloadedSize() / attachedFile.getFullSize(), false);
    }

    @Override
    protected void didDownloadCancelled() {
        radialProgressView.setVisibility(GONE);
    }

    @Override
    public DownloadButton getDownloadButton() {
        return this.downloadButton;
    }

    @Override
    protected FileModel.FileModelDelegate getDelegate() {
        if(delegate == null)
            delegate = new FileModel.FileModelDelegate() {
                @Override
                public String getDelegateKey() {
                    return generateDelegateKey();
                }

                @Override
                public void didFileProgressUpdated() {
                    radialProgressView.setProgress((float) attachedFile.getDownloadedSize() / attachedFile.getFullSize(), true);
                }

                @Override
                public void didFileDownloaded() {
                    if(Build.VERSION.SDK_INT >= 16)
                        audioSeekbar.getThumb().mutate().setAlpha(255);
                    else
                        audioSeekbar.setThumb(getResources().getDrawable(R.drawable.voicemsg_seekbar_thumb));
                    radialProgressView.setProgress(1.0f, false);
                    radialProgressView.setVisibility(GONE);
                    downloadButton.invalidate();
                }

                @Override
                public void didFileDownloadCancelled() {
                    didDownloadCancelled();
                    getDownloadButton().invalidate();
                }
                @Override
                public void didFileDownloadStarted() {
                    didDownloadStarted();
                    getDownloadButton().invalidate();
                }
            };
        return delegate;
    }

    @Override
    protected void init()
    {
        int DOWNLOAD_BTN_SIZE = getResources().getDimensionPixelSize(R.dimen.download_btn_size);
        View v = inflate(getContext(), R.layout.voice, this);
        audioSeekbar = (SeekBar) v.findViewById(R.id.audio_pb);
        radialProgressView = (RadialProgressView) v.findViewById(R.id.radial_progress);
        radialProgressView.setProgressColor(ContextCompat.getColor(getContext(), R.color.dark_blue_bg));
        int oneDp = AndroidUtilities.dp(1);
        radialProgressView.setProgressRect(oneDp, oneDp, DOWNLOAD_BTN_SIZE-oneDp, DOWNLOAD_BTN_SIZE-oneDp);
        radialProgressView.setProgress(0, false);
        radialProgressView.setBackground(getResources().getDrawable(R.color.transparent), true, true);
        radialProgressView.setVisibility(GONE);
        downloadButton = (AudioDownloadButton) v.findViewById(R.id.ic_dl);
        duration = (TextView) v.findViewById(R.id.audio_duration);
    }
    @Override
    protected void fillLayout()
    {
        if(attachedFile == null)
            return;
        FileModel.DownloadState state = attachedFile.getDownloadState();
        this.duration.setText(durationString);

        if(state == FileModel.DownloadState.EMPTY) {
            if(Build.VERSION.SDK_INT >= 16)
                audioSeekbar.getThumb().mutate().setAlpha(0);
            else
                audioSeekbar.setThumb(getResources().getDrawable(R.color.transparent));
        }
        else if(state == FileModel.DownloadState.LOADED)
        {
            if(Build.VERSION.SDK_INT >= 16)
                audioSeekbar.getThumb().mutate().setAlpha(255);
            else
                audioSeekbar.setThumb(getResources().getDrawable(R.drawable.voicemsg_seekbar_thumb));
        }
        if(state == FileModel.DownloadState.LOADING)
        {
            radialProgressView.setVisibility(VISIBLE);
            radialProgressView.setProgress((float) attachedFile.getDownloadedSize() / attachedFile.getFullSize(), false);
        }
        else {
            radialProgressView.setVisibility(GONE);
        }
    }
    public void setDuration(String str) { this.durationString = str; }
    public void setPlaybackState(boolean isPlaying)
    {
        downloadButton.setPlaybackState(isPlaying);
    }
    public void setSeekBarChangeListener(SeekBar.OnSeekBarChangeListener listener)
    {
        audioSeekbar.setOnSeekBarChangeListener(listener);
    }
    public void updateProgress(float progress)
    {
        audioSeekbar.setProgress((int) (progress * 100));
    }

}
