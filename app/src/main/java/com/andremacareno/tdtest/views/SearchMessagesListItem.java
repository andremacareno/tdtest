package com.andremacareno.tdtest.views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.Emoji;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.MessageSearchResult;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 05/05/15.
 */
public class SearchMessagesListItem extends FrameLayout {
    private final int AVATAR_POST_PROCESSING_MASK = TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE;
    private AvatarImageView avatar;
    private TextView dialogName;
    private TextView messagePreview;
    private TextView date;
    private ColorStateList defaultTextColor = null;
    private int blueColor;
    private int avatarSize;

    public SearchMessagesListItem(Context context) {
        super(context);
        init();
    }
    public SearchMessagesListItem(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    private void init()
    {
        View itemView = inflate(getContext(), R.layout.search_messages_item, this);
        setClickable(true);
        setBackgroundResource(R.drawable.list_selector);
        this.blueColor = ContextCompat.getColor(getContext(), R.color.vk_unread_text);
        this.avatar = (AvatarImageView) itemView.findViewById(R.id.avatar);
        this.dialogName = (TextView) itemView.findViewById(R.id.dialog_name);
        dialogName.setTypeface(AndroidUtilities.getTypeface("rmedium.ttf"));
        this.messagePreview = (TextView) itemView.findViewById(R.id.msg_preview);
        this.date = (TextView) itemView.findViewById(R.id.message_date);
        if(defaultTextColor == null)
            defaultTextColor = this.messagePreview.getTextColors();
        this.avatarSize = getResources().getDimensionPixelSize(R.dimen.avatar_size);
    }
    public AvatarImageView getAvatarImageView() { return this.avatar; }
    public void fillLayout(MessageSearchResult itm)
    {
        dialogName.setText(Emoji.replaceEmoji(itm.getFrom(), dialogName.getPaint().getFontMetricsInt(), AndroidUtilities.dp(16), false));
        messagePreview.setText(Emoji.replaceEmoji(itm.getText(), dialogName.getPaint().getFontMetricsInt(), AndroidUtilities.dp(16), false));
        date.setText(itm.getDate());
        if(itm.isServiceMessage())
            messagePreview.setTextColor(blueColor);
        else
            messagePreview.setTextColor(defaultTextColor);
    }
    public void loadAvatar(MessageSearchResult itm)
    {
        String newKey = itm.getPhotoKey();
        FileModel photo = itm.getPhoto();
        TelegramImageLoader.getInstance().loadImage(photo, newKey, avatar, AVATAR_POST_PROCESSING_MASK, avatarSize, avatarSize, false);
    }
    public void loadPlaceholder(MessageSearchResult itm)
    {
        TelegramImageLoader.getInstance().loadPlaceholder(itm.getPeer(), itm.getInitials(), itm.getPlaceholderKey(), avatar, false);
    }
    @Override
    public void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
    }
}
