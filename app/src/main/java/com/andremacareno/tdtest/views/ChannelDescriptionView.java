package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by andremacareno on 22/06/15.
 */
public class ChannelDescriptionView extends FrameLayout {
    private TextView channel_description;

    public ChannelDescriptionView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.channel_description, this);
        channel_description = (TextView) v.findViewById(R.id.text);
    }
    public void setAbout(String about)
    {
        channel_description.setText(about);
    }
    public boolean isEmpty() { return channel_description.getText().toString().isEmpty(); }
}
