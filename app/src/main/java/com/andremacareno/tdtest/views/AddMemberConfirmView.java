package com.andremacareno.tdtest.views;

import android.content.Context;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 06/12/15.
 */
public class AddMemberConfirmView extends LinearLayout {
    private static final int MIN_FWD = 0;
    private static final int MAX_FWD = 300;
    private EditText fwdMessagesEditText;
    private String username;
    public AddMemberConfirmView(Context context, String u) {
        super(context);
        this.username = u;
        init();
    }
    private void init()
    {
        final int margin = AndroidUtilities.dp(16);
        setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        setPadding(margin, margin, margin, margin);
        setOrientation(VERTICAL);
        View v = inflate(getContext(), R.layout.add_member_confirm, this);
        fwdMessagesEditText = (EditText) v.findViewById(R.id.fwdEditText);
        fwdMessagesEditText.setFilters(new InputFilter[]{new NumberInputRangeFilter(MIN_FWD, MAX_FWD)});
        TextView confirmMessageTextView = (TextView) v.findViewById(R.id.txt);
        confirmMessageTextView.setText(String.format(getResources().getString(R.string.add_member_confirm), username));
    }
    public int getForwardNumber()
    {
        try {
            return Integer.valueOf(fwdMessagesEditText.getText().toString());
        }
        catch(Exception e) {
            return 0;
        }
    }
}
