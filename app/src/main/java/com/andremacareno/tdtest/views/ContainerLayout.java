package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.andremacareno.tdtest.views.actionviews.ActionView;

/**
 * Created by Andrew on 12.05.2015.
 */
public class ContainerLayout extends FrameLayout {
    private boolean actionViewOverlay = false;
    private ActionView actionView;
    public ContainerLayout(Context context) {
        super(context);
    }

    public ContainerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ContainerLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void enableActionViewOverlay()
    {
        this.actionViewOverlay = true;
        requestLayout();
    }
    public void disableActionViewOverlay()
    {
        this.actionViewOverlay = false;
        requestLayout();
    }
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
    {

    }
}
