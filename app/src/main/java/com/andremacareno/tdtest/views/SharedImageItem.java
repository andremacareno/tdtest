package com.andremacareno.tdtest.views;

import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.images.RecyclingImageView;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 15/08/15.
 */
public class SharedImageItem extends RelativeLayout {
    private RecyclingImageView preview;
    private TextView durationTextView;
    private View checkContainer;
    private RevealingCheckView checkIcon;
    private View playIcon;
    public SharedImageItem(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.shared_img, this);
        durationTextView = (TextView) v.findViewById(R.id.video_duration);
        checkContainer = v.findViewById(R.id.btn_check_container);
        checkIcon = (RevealingCheckView) checkContainer.findViewById(R.id.btn_check);
        playIcon = v.findViewById(R.id.video_play_btn);
        preview = (RecyclingImageView) v.findViewById(R.id.preview);
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= 16) {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                getLayoutParams().width = getWidth();
                getLayoutParams().height = getWidth();
                requestLayout();
            }
        });
        int padding = AndroidUtilities.dp(2);
        setPadding(0, padding, padding*2, padding);
    }
    public RecyclingImageView getPreviewContainer() { return this.preview; }
    public void setDuration(String duration) {
        this.durationTextView.setText(duration);
    }
    public void isVideo(boolean video)
    {
        if(video)
        {
            durationTextView.setVisibility(View.VISIBLE);
            playIcon.setVisibility(View.VISIBLE);
        }
        else
        {
            durationTextView.setVisibility(View.GONE);
            playIcon.setVisibility(View.GONE);
        }
    }
    public void check(boolean animated)
    {
        checkContainer.setVisibility(View.VISIBLE);
        checkIcon.check(animated);
    }
    public void uncheck(boolean animated, boolean selectionMode)
    {
        if(!selectionMode) {
            checkContainer.setVisibility(View.GONE);
            return;
        }
        checkContainer.setVisibility(View.VISIBLE);
        checkIcon.uncheck(animated);
    }

}
