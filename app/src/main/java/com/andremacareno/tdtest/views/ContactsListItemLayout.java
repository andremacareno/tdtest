package com.andremacareno.tdtest.views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.images.RecyclingImageView;
import com.andremacareno.tdtest.models.UserModel;

/**
 * Created by andremacareno on 05/05/15.
 */
public class ContactsListItemLayout extends FrameLayout {
    private SelectableAvatarImageView avatar;
    private TextView contactName;
    private TextView status;
    private ColorStateList defaultTextColor = null;
    private RevealingCheckView check;


    public ContactsListItemLayout(Context context) {
        super(context);
        setBackgroundResource(R.drawable.list_selector);
        init();
    }
    public ContactsListItemLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setBackgroundResource(R.drawable.list_selector);
        init();
    }

    private void init()
    {
        View itemView = inflate(getContext(), R.layout.contactlist_item_layout, this);
        this.avatar = (SelectableAvatarImageView) itemView.findViewById(R.id.chat_avatar);
        this.check = (RevealingCheckView) itemView.findViewById(R.id.user_check);
        this.contactName = (TextView) itemView.findViewById(R.id.contact_name);
        this.status = (TextView) itemView.findViewById(R.id.status);
        if(defaultTextColor == null)
            defaultTextColor = this.status.getTextColors();
    }
    public void check(boolean animate)
    {
        this.avatar.select(animate);
        this.check.check(animate);
    }
    public void uncheck(boolean animate)
    {
        this.avatar.deselect(animate);
        this.check.uncheck(animate);
    }
    public RecyclingImageView getAvatarImageView() { return this.avatar; }
    public void fillLayout(UserModel itm)
    {
        contactName.setText(itm.getDisplayName());
        status.setText(itm.getStringStatus());
        if(itm.isOnline())
            status.setTextColor(ContextCompat.getColor(getContext(), R.color.vk_unread_text));
        else
            status.setTextColor(defaultTextColor);
    }
}
