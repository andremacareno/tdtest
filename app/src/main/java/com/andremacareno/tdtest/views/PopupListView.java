package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v7.widget.ListPopupWindow;
import android.view.View;
import android.widget.AdapterView;

import com.andremacareno.tdtest.listadapters.menu.MenuItem;

/**
 * Created by andremacareno on 31/08/15.
 */
public abstract class PopupListView extends ListPopupWindow {
    public PopupListView(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try
                {
                    MenuItem item = (MenuItem) adapterView.getAdapter().getItem(i);
                    didItemClicked(item);
                }
                catch(Exception e) { e.printStackTrace(); }
                finally
                {
                    dismiss();
                }
            }
        });
        setModal(true);
    }
    protected abstract void didItemClicked(MenuItem item);
}
