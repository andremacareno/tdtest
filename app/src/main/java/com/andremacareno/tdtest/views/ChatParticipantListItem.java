package com.andremacareno.tdtest.views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.UserModel;

/**
 * Created by andremacareno on 05/05/15.
 */
public class ChatParticipantListItem extends FrameLayout {
    protected AvatarImageView avatar;
    protected TextView contactName;
    protected TextView status;
    protected ColorStateList defaultTextColor = null;


    public ChatParticipantListItem(Context context) {
        super(context);
        init();
    }
    public ChatParticipantListItem(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    protected void init()
    {
        View itemView = inflate(getContext(), R.layout.chat_participants_item, this);
        setBackgroundResource(R.drawable.profile_list_selector);
        this.avatar = (AvatarImageView) itemView.findViewById(R.id.avatar);
        this.contactName = (TextView) itemView.findViewById(R.id.contact_name);
        this.status = (TextView) itemView.findViewById(R.id.status);
        if(defaultTextColor == null)
            defaultTextColor = this.status.getTextColors();
    }
    public AvatarImageView getAvatarImageView() { return this.avatar; }
    public void fillLayout(UserModel itm)
    {
        contactName.setText(itm.getDisplayName());
        if(!itm.isBot())
            status.setText(itm.getStringStatus());
        else
            status.setText(itm.canBotReadMessages() ? R.string.has_access_to_messages : R.string.has_no_access_to_messages);
        if(itm.isOnline() && !itm.isBot())
            status.setTextColor(ContextCompat.getColor(getContext(), R.color.vk_unread_text));
        else
            status.setTextColor(defaultTextColor);
    }
}
