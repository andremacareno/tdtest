package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by andremacareno on 22/06/15.
 */
public class UserNameView extends FrameLayout {
    private TextView user_name;

    public UserNameView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.user_details_name, this);
        user_name = (TextView) v.findViewById(R.id.text);
    }
    public void setUserName(String name)
    {
        user_name.setText(name);
    }

}
