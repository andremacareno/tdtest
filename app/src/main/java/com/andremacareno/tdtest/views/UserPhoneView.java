package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by andremacareno on 22/06/15.
 */
public class UserPhoneView extends FrameLayout {
    private TextView user_phone;

    public UserPhoneView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.user_details_phone, this);
        user_phone = (TextView) v.findViewById(R.id.text);
    }
    public void setPhone(String phone)
    {
        user_phone.setText(phone);
    }

}
