package com.andremacareno.tdtest.views.actionviews;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by Andrew on 13.05.2015.
 */
public class ChatListActionView extends ActionView {
    private static final String TAG = "ChatListActionView";
    public ImageButton openMenuButton;
    public ImageButton lockButton;
    public ImageButton searchButton;
    public ImageButton clearSearchButton;
    public TextView connectionStateText;
    public EditText searchEditText;
    public ChatListActionView(Context context) {
        super(context);
    }
    @Override
    protected void initView()
    {
        View v = inflate(getContext(), R.layout.action_view_roster, this);
        openMenuButton = (ImageButton) v.findViewById(R.id.chatlist_navdrawer_btn);
        lockButton = (ImageButton) v.findViewById(R.id.passcode_lock_btn);
        searchButton = (ImageButton) v.findViewById(R.id.search_btn);
        clearSearchButton = (ImageButton) v.findViewById(R.id.clear_search_btn);
        connectionStateText = (TextView) v.findViewById(R.id.chatlist_title);
        searchEditText = (EditText) v.findViewById(R.id.search_edittext);
    }

    @Override
    public Object getTag() {
        return TAG;
    }
}
