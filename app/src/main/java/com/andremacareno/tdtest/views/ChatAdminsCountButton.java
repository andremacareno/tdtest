package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by andremacareno on 20/08/15.
 */
public class ChatAdminsCountButton extends RelativeLayout {
    private TextView adminsCount;
    public ChatAdminsCountButton(Context context) {
        super(context);
        init();
    }

    public ChatAdminsCountButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.chat_admins, this);
        adminsCount = (TextView) v.findViewById(R.id.admins_count);
    }
    public void setAdmins(int count)
    {
        this.adminsCount.setText(String.valueOf(count));
    }
    public void setAllAdmins()
    {
        this.adminsCount.setText(R.string.all);
    }
}
