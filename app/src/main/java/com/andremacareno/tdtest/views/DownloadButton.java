package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.andremacareno.tdtest.models.FileModel;

/**
 * Created by andremacareno on 17/08/15.
 */
public abstract class DownloadButton extends View {
    protected FileModel attachedFile;
    public DownloadButton(Context context) {
        super(context);
        init();
    }

    public DownloadButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public void attachFile(FileModel file)
    {
        this.attachedFile = file;
        invalidate();
    }
    protected abstract void init();
}
