package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.UserModel;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 22/06/15.
 */
public class UserView extends FrameLayout {
    private AvatarImageView avatar;
    private final int avatarSize = isInEditMode() ? 168 : TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size);
    private TextView user_name;
    private TextView user_status;
    private UserModel boundUser;
    public UserView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.user_view, this);
        avatar = (AvatarImageView) v.findViewById(R.id.avatar);
        user_name = (TextView) v.findViewById(R.id.name);
        user_status = (TextView) v.findViewById(R.id.status);
    }
    private void fillLayout()
    {
        if(boundUser == null)
            return;
        if(boundUser.isDeletedAccount())
            user_name.setText(R.string.unknown_user);
        else
           user_name.setText(boundUser.getDisplayName());
        if(boundUser.isBot())
            user_status.setText(R.string.bot);
        else if(boundUser.getId() == 333000 || boundUser.getId() == 777000)
            user_status.setText(R.string.service_notifications);
        else
            user_status.setText(boundUser.getStringStatus());
        String initials = boundUser.getInitials();
        FileModel avatarFileModel = boundUser.getPhoto();
        if(avatarFileModel == null || avatarFileModel.getFileId() == 0) {
            String placeholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, boundUser.getId());
            TelegramImageLoader.getInstance().loadPlaceholder(boundUser.getId(), initials, placeholderKey, avatar, false);
        }
        else {
            String avatarKey = CommonTools.makeFileKey(avatarFileModel.getFileId());
            TelegramImageLoader.getInstance().loadImage(avatarFileModel, avatarKey, avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, avatarSize, avatarSize, false);
        }
    }
    public void bindUser(UserModel user)
    {
        this.boundUser = user;
        fillLayout();
    }
    public void updateUserStatus(TdApi.UserStatus newStatus)
    {
        if(boundUser.isBot() || boundUser.getId() == 333000 || boundUser.getId() == 777000)
            return;
        user_status.setText(CommonTools.statusToText(newStatus));
    }
    public void updateDisplayName(String newTitle)
    {
        user_name.setText(newTitle);
    }
    public void updateUserPhoto(TdApi.File newPhoto)
    {
        if(newPhoto.id == 0)
        {
            String placeholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, boundUser.getId());
            TelegramImageLoader.getInstance().loadPlaceholder(boundUser.getId(), boundUser.getInitials(), placeholderKey, avatar, false);
        }
        else
        {
            String avatarKey = CommonTools.makeFileKey(newPhoto.id);
            TelegramImageLoader.getInstance().loadImage(FileCache.getInstance().getFileModel(newPhoto), avatarKey, avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, avatarSize, avatarSize, true);
        }
    }
}
