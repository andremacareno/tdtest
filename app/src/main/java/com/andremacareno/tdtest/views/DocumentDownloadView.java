package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.images.RecyclingImageView;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 29/06/15.
 */
public class DocumentDownloadView extends DownloadView {
    private final String downloadingStringFormat = TelegramApplication.sharedApplication().getResources().getString(R.string.downloading_x_of_y);
    private String documentNameString;
    private DocumentDownloadButton downloadButton;
    private ImageDownloadButton imgDownloadButton;
    private RecyclingImageView thumbImageView;
    private TextView docName;
    private TextView docSize;
    private RadialProgressView radialProgressView;
    private FileModel.FileModelDelegate delegate;
    private volatile boolean hasThumb = false;
    public DocumentDownloadView(Context context) {
        super(context);
    }

    public DocumentDownloadView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void didDownloadStarted() {
        radialProgressView.setVisibility(View.VISIBLE);
        radialProgressView.setProgress((float) attachedFile.getDownloadedSize() / attachedFile.getFullSize(), false);
        this.docSize.setText(String.format(this.downloadingStringFormat, CommonTools.sizeToString(attachedFile.getDownloadedSize()), CommonTools.sizeToString(attachedFile.getFullSize())));
    }

    @Override
    protected void didDownloadCancelled() {
        docSize.setText(CommonTools.sizeToString(attachedFile.getFullSize()));
        radialProgressView.setProgress(1.0f, false);
        radialProgressView.setVisibility(View.GONE);
    }

    @Override
    public DownloadButton getDownloadButton() {
        return hasThumb ? this.imgDownloadButton : this.downloadButton;
    }

    @Override
    protected FileModel.FileModelDelegate getDelegate() {
        if(delegate == null)
            delegate = new FileModel.FileModelDelegate() {
                @Override
                public String getDelegateKey() {
                    return generateDelegateKey();
                }

                @Override
                public void didFileDownloadStarted() {
                    didDownloadStarted();
                    checkRadialProgress();
                    getDownloadButton().invalidate();
                }

                @Override
                public void didFileProgressUpdated() {
                    if(attachedFile.getDownloadState() != FileModel.DownloadState.LOADING)
                        return;
                    docSize.setText(String.format(downloadingStringFormat, CommonTools.sizeToString(attachedFile.getDownloadedSize()), CommonTools.sizeToString(attachedFile.getFullSize())));
                    radialProgressView.setProgress((float) attachedFile.getDownloadedSize() / attachedFile.getFullSize(), true);
                }

                @Override
                public void didFileDownloaded() {
                    docSize.setText(CommonTools.sizeToString(attachedFile.getDownloadedSize()));
                    radialProgressView.setProgress(1.0f, false);
                    radialProgressView.setVisibility(View.GONE);
                    if(hasThumb)
                        imgDownloadButton.setVisibility(View.GONE);
                    else
                       downloadButton.invalidate();
                }

                @Override
                public void didFileDownloadCancelled() {
                    didDownloadCancelled();
                    getDownloadButton().invalidate();
                }
            };
        return delegate;
    }

    @Override
    protected void init()
    {
        View v = inflate(getContext(), R.layout.documentview, this);
        radialProgressView = (RadialProgressView) v.findViewById(R.id.radial_progress);
        downloadButton = (DocumentDownloadButton) v.findViewById(R.id.doc_download);
        imgDownloadButton = (ImageDownloadButton) v.findViewById(R.id.img_doc_download);
        thumbImageView = (RecyclingImageView) v.findViewById(R.id.img_doc);
        docName = (TextView) v.findViewById(R.id.doc_name);
        docSize = (TextView) v.findViewById(R.id.doc_size);
    }
    @Override
    protected void fillLayout()
    {
        if(attachedFile == null)
            return;
        FileModel.DownloadState state = attachedFile.getDownloadState();
        this.docName.setText(documentNameString);
        if(state != FileModel.DownloadState.LOADING)
        {
            docSize.setText(CommonTools.sizeToString(attachedFile.getFullSize()));
            radialProgressView.setVisibility(View.GONE);
        }
        else
        {
            radialProgressView.setVisibility(View.VISIBLE);
            radialProgressView.setProgress((float) attachedFile.getDownloadedSize() / attachedFile.getFullSize(), false);
            this.docSize.setText(String.format(this.downloadingStringFormat, CommonTools.sizeToString(attachedFile.getDownloadedSize()), CommonTools.sizeToString(attachedFile.getFullSize())));
        }
    }
    private void setLayout()
    {
        int DOWNLOAD_BTN_SIZE = getResources().getDimensionPixelSize(R.dimen.download_btn_size);
        int oneDp = AndroidUtilities.dp(1);
        checkRadialProgress();
        radialProgressView.setProgressRect(oneDp, oneDp, DOWNLOAD_BTN_SIZE-oneDp, DOWNLOAD_BTN_SIZE-oneDp);
        radialProgressView.setProgress(0, false);
        radialProgressView.setBackground(getResources().getDrawable(R.color.transparent), true, true);
        radialProgressView.setVisibility(View.GONE);

        if(hasThumb)
        {
            thumbImageView.setVisibility(View.VISIBLE);
            if(attachedFile != null && attachedFile.getDownloadState() == FileModel.DownloadState.LOADED)
                imgDownloadButton.setVisibility(View.GONE);
            else
               imgDownloadButton.setVisibility(View.VISIBLE);
            downloadButton.setVisibility(View.GONE);
            docName.setPadding(AndroidUtilities.dp(96), docName.getPaddingTop(), docName.getPaddingRight(), docName.getPaddingRight());
            docSize.setPadding(AndroidUtilities.dp(96), docSize.getPaddingTop(), docSize.getPaddingRight(), docSize.getPaddingRight());
        }
        else
        {
            thumbImageView.setVisibility(View.GONE);
            imgDownloadButton.setVisibility(View.GONE);
            downloadButton.setVisibility(View.VISIBLE);
            docName.setPadding(AndroidUtilities.dp(56), docName.getPaddingTop(), docName.getPaddingRight(), docName.getPaddingRight());
            docSize.setPadding(AndroidUtilities.dp(56), docSize.getPaddingTop(), docSize.getPaddingRight(), docSize.getPaddingRight());
        }
    }
    private void checkRadialProgress()
    {
        if(!hasThumb) {
            ((FrameLayout.LayoutParams)radialProgressView.getLayoutParams()).setMargins(0, 0, 0, 0);
            radialProgressView.setProgressColor(ContextCompat.getColor(getContext(), R.color.dark_blue_bg));
        }
        else {
            ((FrameLayout.LayoutParams)radialProgressView.getLayoutParams()).setMargins(AndroidUtilities.dp(20), 0, 0, 0);
            radialProgressView.setProgressColor(ContextCompat.getColor(getContext(), R.color.white));
        }
    }
    public void setHasThumb(boolean hasThumb)
    {
        this.hasThumb = hasThumb;
        setLayout();
    }

    public RecyclingImageView getThumbImageView() { return this.thumbImageView; }
    public void setDocumentName(String str) { this.documentNameString = str; }
}
