package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.ReplyMessageShortModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 24/02/16.
 */
public class ReplyView extends LinearLayout {
    private ImageView thumb;
    private TextView username, content;
    public ReplyView(Context context) {
        super(context);
        init();
    }

    public ReplyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        setOrientation(HORIZONTAL);
        View v = inflate(getContext(), R.layout.reply_view, this);
        thumb = (ImageView) v.findViewById(R.id.reply_thumb);
        username = (TextView) v.findViewById(R.id.reply_user);
        content = (TextView) v.findViewById(R.id.reply_content);
    }
    public void fill(ReplyMessageShortModel reply)
    {
        if(reply.getThumb() == null)
            thumb.setVisibility(View.GONE);
        else
        {
            thumb.setVisibility(View.VISIBLE);
            TelegramImageLoader.getInstance().loadImage(reply.getThumb(), String.format("reply_thumb_%d", reply.getThumb().getFileId()), thumb, TelegramImageLoader.POST_PROCESSING_NOTHING, AndroidUtilities.dp(34), AndroidUtilities.dp(34), false);
        }
        username.setText(reply.getUsername());
        content.setText(reply.getText());
    }

}
