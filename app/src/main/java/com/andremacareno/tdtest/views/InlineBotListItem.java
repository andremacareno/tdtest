package com.andremacareno.tdtest.views;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.InlineBotShortModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 12/08/15.
 */
public class InlineBotListItem extends RelativeLayout {

    private BotCommandAvatarImageView botIcon;
    private TextView botDescription;
    private TextView botUsername;
    public InlineBotListItem(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.commands_list_item, this);
        //setPadding(0, AndroidUtilities.dp(4), 0, AndroidUtilities.dp(4));
        setClickable(true);
        setBackgroundResource(R.drawable.list_selector);
        botIcon = (BotCommandAvatarImageView) v.findViewById(R.id.bot_icon);
        botDescription = (TextView) v.findViewById(R.id.command);
        botUsername = (TextView) v.findViewById(R.id.description);
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), AndroidUtilities.dp(38));
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    public void fillLayout(InlineBotShortModel model)
    {
        botDescription.setText(model.getBotName());
        botUsername.setText(model.getBotUsername());
    }
    public void updatePhoto(InlineBotShortModel botModel)
    {
        if(botModel == null)
            return;
        FileModel model = botModel.getPhoto();
        if(model.getFileId() == 0)
        {
            String placeholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, botModel.getUserObj().id);
            TelegramImageLoader.getInstance().loadPlaceholder(botModel.getUserObj().id, botModel.getInitials(), placeholderKey, botIcon, false);
        }
        else
        {
            String avatarKey = CommonTools.makeFileKey(model.getFileId());
            TelegramImageLoader.getInstance().loadImage(model, avatarKey, botIcon, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, AndroidUtilities.dp(28), AndroidUtilities.dp(28), false);
        }
    }
}
