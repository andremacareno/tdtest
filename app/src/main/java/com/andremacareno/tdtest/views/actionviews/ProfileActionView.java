package com.andremacareno.tdtest.views.actionviews;

import android.content.Context;
import android.view.MenuInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.PopupMenu;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.views.UserView;

/**
 * Created by andremacareno on 12/05/15.
 */
public class ProfileActionView extends ActionView {
    private static final String TAG = "ProfileActionView";
    public FrameLayout chatContent;
    public UserView userView;
    public ImageButton backButton;
    public ImageButton menuButton;
    public ImageButton notificationsButton;
    public PopupMenu notificationsMenu;
    public PopupMenu defaultMenu;


    public ProfileActionView(Context context) {
        super(context);
    }


    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void initView() {
        View v = inflate(getContext(), R.layout.action_view_profile, this);
        chatContent = (FrameLayout) v.findViewById(R.id.chat_info);
        userView = (UserView) v.findViewById(R.id.userview);
        backButton = (ImageButton) v.findViewById(R.id.chat_back_btn);
        menuButton = (ImageButton) v.findViewById(R.id.menu_btn);
        notificationsButton = (ImageButton) v.findViewById(R.id.notifications_btn);
        notificationsMenu = new PopupMenu(getContext(), notificationsButton);
        defaultMenu = new PopupMenu(getContext(), menuButton);
        MenuInflater inflater = notificationsMenu.getMenuInflater();
        inflater.inflate(R.menu.notifications, notificationsMenu.getMenu());
        notificationsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notificationsMenu.show();
            }
        });
    }
    public void setBackButtonClickListener(OnClickListener listener)
    {
        backButton.setOnClickListener(listener);
    }
    public void hideNotificationsButton()
    {
        notificationsButton.setVisibility(View.GONE);
    }
    public void hideMenuButton()
    {
        menuButton.setVisibility(View.GONE);
    }
    public void setMenuResource(int res)
    {
        if(defaultMenu.getMenu() != null)
            defaultMenu.getMenu().clear();
        MenuInflater inflater = defaultMenu.getMenuInflater();
        inflater.inflate(res, defaultMenu.getMenu());
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                defaultMenu.show();
            }
        });
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), AndroidUtilities.dp(148));
    }

}
