package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.utils.AndroidUtilities;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 05/05/15.
 */
public class InlineTextItemView extends RelativeLayout {
    private TextView title;
    private TextView description;
    private InlineResultImageView thumb;


    public InlineTextItemView(Context context) {
        super(context);
        init();
    }
    public InlineTextItemView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    private void init()
    {
        View itemView = inflate(getContext(), R.layout.inline_article, this);
        setBackground(ContextCompat.getDrawable(getContext(), R.drawable.profile_list_selector));
        int padding = AndroidUtilities.dp(16);
        setPadding(padding, padding, padding, padding);
        this.title = (TextView) itemView.findViewById(R.id.inline_title);
        this.description = (TextView) itemView.findViewById(R.id.inline_description);
        this.thumb = (InlineResultImageView) itemView.findViewById(R.id.thumb);
    }
    public void attachQueryResult(TdApi.InlineQueryResultArticle article)
    {
        if(article == null)
            return;
        title.setText(article.title);
        description.setText(article.description);
        TelegramImageLoader.getInstance().loadArticleThumb(article.thumbUrl, thumb);
    }
    public void attachQueryResult(TdApi.InlineQueryResultVideo video)
    {
        if(video == null)
            return;
        title.setText(video.title);
        description.setText(video.description);
        TelegramImageLoader.getInstance().loadArticleThumb(video.thumbUrl, thumb);
    }
}
