package com.andremacareno.tdtest.views.player;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.models.AudioTrackModel;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.views.AudioDownloadButton;
import com.andremacareno.tdtest.views.RadialProgressView;

/**
 * Created by andremacareno on 15/08/15.
 */
public class PlayerBottomView extends LinearLayout {
    private TextView timeElapsed;
    private TextView timeRemaining;
    private TextView performer;
    private TextView songName;
    private ImageButton playPrevious;
    private ImageButton playNext;
    private AudioDownloadButton playbackButton;
    private FileModel.FileModelDelegate downloadDelegate;
    //private AudioDownloadButton downloadButton;
    private RadialProgressView radialProgress;
    private PlayerView.PlayerDelegate delegate;
    private AudioTrackModel boundTrack = null;
    public PlayerBottomView(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        setOrientation(VERTICAL);
        setBackgroundResource(R.color.white);
        setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        setPadding(AndroidUtilities.dp(16), 0, AndroidUtilities.dp(16), 0);
        View v = inflate(getContext(), R.layout.player_bottom, this);
        radialProgress = (RadialProgressView) v.findViewById(R.id.player_radial_progress);
        int DOWNLOAD_BTN_SIZE = getResources().getDimensionPixelSize(R.dimen.download_btn_size);
        int oneDp = AndroidUtilities.dp(1);
        radialProgress.setProgressRect(oneDp, oneDp, DOWNLOAD_BTN_SIZE-oneDp, DOWNLOAD_BTN_SIZE-oneDp);
        radialProgress.setProgress(0, false);
        radialProgress.setBackground(getResources().getDrawable(R.color.transparent), true, true);
        radialProgress.setProgressColor(ContextCompat.getColor(getContext(), R.color.dark_blue_bg));
        timeElapsed = (TextView) v.findViewById(R.id.time_elapsed);
        timeRemaining = (TextView) v.findViewById(R.id.time_remaining);
        performer = (TextView) v.findViewById(R.id.song_performer);
        songName = (TextView) v.findViewById(R.id.song_title);
        playPrevious = (ImageButton) v.findViewById(R.id.previous_track);
        playNext = (ImageButton) v.findViewById(R.id.next_track);
        playbackButton = (AudioDownloadButton) v.findViewById(R.id.playback_btn);
        playbackButton.setPlaybackState(true);
        //playbackButton.set(FileModel.DownloadState.LOADED);
        playbackButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(boundTrack == null)
                    return;
                if(boundTrack.getTrack().getDownloadState() == FileModel.DownloadState.LOADED && delegate != null)
                    delegate.didPlayButtonPressed();
                else if(boundTrack.getTrack().getDownloadState() == FileModel.DownloadState.LOADING)
                    FileCache.getInstance().cancelDownload(boundTrack.getTrack());
                else if(boundTrack.getTrack().getDownloadState() == FileModel.DownloadState.EMPTY)
                    FileCache.getInstance().downloadFile(boundTrack.getTrack());
            }
        });
        playPrevious.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(delegate != null)
                    delegate.didPreviousTrackButtonPressed();
            }
        });
        playNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(delegate != null)
                    delegate.didNextTrackButtonPressed();
            }
        });

    }
    public void updateTime(String elapsed, String remaining)
    {
        timeElapsed.setText(elapsed);
        timeRemaining.setText(remaining);
    }
    public void setDelegate(PlayerView.PlayerDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    public void updatePlayButton(boolean isPlaying)
    {
        playbackButton.setPlaybackState(isPlaying);
    }
    public void updateSongData(String name, String performer)
    {
        this.performer.setText(performer);
        this.songName.setText(name);
    }
    public void bindTrack(AudioTrackModel track)
    {
        try {
            boundTrack = track;
            final FileModel attachedFile = track.getTrack();
            downloadDelegate = new FileModel.FileModelDelegate() {
                @Override
                public String getDelegateKey() {
                    return String.format("track%d_playerbutton_delegate", attachedFile.getFileId());
                }
                @Override
                public void didFileDownloadStarted() {
                    didDownloadStarted();
                    playbackButton.invalidate();
                }
                @Override
                public void didFileProgressUpdated() {
                    if(attachedFile.getDownloadState() != FileModel.DownloadState.LOADING)
                        return;
                    float progress = ((float) attachedFile.getDownloadedSize() / (float) attachedFile.getFullSize());
                    radialProgress.setProgress(progress, true);
                }

                @Override
                public void didFileDownloaded() {
                    radialProgress.setProgress(1.0f, false);
                    radialProgress.setVisibility(View.INVISIBLE);
                    playbackButton.invalidate();
                    if(boundTrack != null && boundTrack.getTrack().getFileId() == attachedFile.getFileId())
                        boundTrack.getTrack().removeDelegate(this);
                }

                @Override
                public void didFileDownloadCancelled() {
                    radialProgress.setVisibility(View.INVISIBLE);
                    playbackButton.invalidate();
                }
            };
            if(attachedFile.getDownloadState() != FileModel.DownloadState.LOADED)
            {
                radialProgress.setVisibility(View.VISIBLE);
                boundTrack.getTrack().addDelegate(downloadDelegate);
            }
            updateSongData(track.getSongName(), track.getSongPerformer());
            playbackButton.attachFile(track.getTrack());
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    private void didDownloadStarted() {
        if(boundTrack == null)
            return;
        FileModel attachedFile = boundTrack.getTrack();
        float progress = ((float) attachedFile.getDownloadedSize() / (float) attachedFile.getFullSize());
        radialProgress.setVisibility(View.VISIBLE);
        radialProgress.setProgress(progress, false);
    }
}
