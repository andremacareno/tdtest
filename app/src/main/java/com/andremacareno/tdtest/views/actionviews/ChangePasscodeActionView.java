package com.andremacareno.tdtest.views.actionviews;

import android.content.Context;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by Andrew on 13.05.2015.
 */
public abstract class ChangePasscodeActionView extends ActionView {
    public ImageButton backButton;
    public TextView passcodeTypeTextView;
    public View passcodeTypeMenu;
    public ChangePasscodeActionView(Context context) {
        super(context);
    }
    @Override
    protected void initView()
    {
        View v = inflate(getContext(), R.layout.action_view_set_passcode, this);
        backButton = (ImageButton) v.findViewById(R.id.back);
        passcodeTypeTextView = (TextView) v.findViewById(R.id.passcode_type_text);
        passcodeTypeMenu = v.findViewById(R.id.passcode_type_caret);
    }
}
