package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by andremacareno on 25/07/15.
 */
public class SettingsItemView extends RelativeLayout {
    private View icon;
    private TextView primaryText;
    private TextView secondaryText;
    public SettingsItemView(Context context) {
        super(context);
        init();
    }

    public SettingsItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.settings_item, this);
        setBackgroundResource(R.drawable.profile_list_selector);
        icon = v.findViewById(R.id.icon);
        primaryText = (TextView) v.findViewById(R.id.settings_title);
        secondaryText = (TextView) v.findViewById(R.id.settings_state);
    }
    public void fillLayout(int iconRes, int primaryTextRes, int secondaryTextRes, int secondaryTextResStyle)
    {
        if(iconRes != 0)
            icon.setBackgroundResource(iconRes);
        else
            icon.setVisibility(View.INVISIBLE);

        if(primaryTextRes != 0)
            primaryText.setText(primaryTextRes);
        else
            primaryText.setVisibility(View.INVISIBLE);

        if(secondaryTextRes != 0) {
            secondaryText.setTextAppearance(getContext(), secondaryTextResStyle);
            secondaryText.setText(secondaryTextRes);
        }
        else
            secondaryText.setVisibility(View.INVISIBLE);
    }
}
