package com.andremacareno.tdtest.views;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.BotCommandModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 12/08/15.
 */
public class BotCommandListItem extends RelativeLayout {
    private BotCommandAvatarImageView botIcon;
    private TextView cmd;
    private TextView description;
    public BotCommandListItem(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.commands_list_item, this);
        //setPadding(0, AndroidUtilities.dp(4), 0, AndroidUtilities.dp(4));
        setClickable(true);
        setBackgroundResource(R.drawable.list_selector);
        botIcon = (BotCommandAvatarImageView) v.findViewById(R.id.bot_icon);
        cmd = (TextView) v.findViewById(R.id.command);
        description = (TextView) v.findViewById(R.id.description);
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), AndroidUtilities.dp(38));
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    public void fillLayout(BotCommandModel model)
    {
        cmd.setText(model.getDisplayCommand());
        description.setText(model.getCmdDescription());
    }
    public void updatePhoto(BotCommandModel model)
    {
        if(model == null || model.getBotInfo() == null)
            return;
        if(model.getBotInfo().getPhoto().getFileId() == 0)
        {
            String placeholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, model.getBotInfo().getId());
            TelegramImageLoader.getInstance().loadPlaceholder(model.getBotInfo().getId(), model.getBotInfo().getInitials(), placeholderKey, botIcon, false);
        }
        else
        {
            String avatarKey = CommonTools.makeFileKey(model.getBotInfo().getPhoto().getFileId());
            TelegramImageLoader.getInstance().loadImage(model.getBotInfo().getPhoto(), avatarKey, botIcon, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, AndroidUtilities.dp(28), AndroidUtilities.dp(28), false);
        }
    }
}
