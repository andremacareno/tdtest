package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.listadapters.ExactlyHeightLayoutManager;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.ChatAttachListController;
import com.andremacareno.tdtest.viewcontrollers.ViewControllable;
import com.andremacareno.tdtest.viewcontrollers.ViewController;

/**
 * Created by andremacareno on 04/04/15.
 */
public class ChatAttachListView extends BaseListView implements ViewControllable {
    private ChatAttachListController controller;
    private LayoutManager lm;
    public ChatAttachListView(Context context) {
        super(context);
        init();
    }

    public ChatAttachListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChatAttachListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init()
    {
        if(isInEditMode())
            return;
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
        if(lm == null)
            lm = new ExactlyHeightLayoutManager(TelegramApplication.sharedApplication().getApplicationContext(), AndroidUtilities.dp(208));
        setLayoutManager(lm);
        setHasFixedSize(true);
    }
    @Override
    public void setController(ViewController vc)
    {
        this.controller = (ChatAttachListController) vc;
    }

    @Override
    public ViewController getController() {
        return controller;
    }
    public void setNewElementCount(int count)
    {
        ((ExactlyHeightLayoutManager) lm).setSpecifiedHeight(count * AndroidUtilities.dp(52));
    }
}
