package com.andremacareno.tdtest.views;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.BotHeaderMessageModel;

/**
 * Created by andremacareno on 27/08/15.
 */
public class BotHeaderView extends RelativeLayout {
    private TextView infoTextView;
    private boolean isCentered = false;
    public BotHeaderView(Context context) {
        super(context);
        init();
    }

    public BotHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.chat_bot_initial_msg, this);
        infoTextView = (TextView) v.findViewById(R.id.help_text);
        setClickable(false);
    }

    public void fillHeader(BotHeaderMessageModel msg)
    {
        if(msg != null)
            infoTextView.setText(msg.getDescription());
    }
    public void centerView(boolean center)
    {
        if(center && isCentered)
            return;
        if(!center && !isCentered)
            return;
        if(!center)
        {
            RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) getLayoutParams();
            isCentered = false;
            lp.setMargins(lp.leftMargin, 0, lp.rightMargin, 0);
            requestLayout();
        }
        if(getHeight() == 0)
        {
            getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (Build.VERSION.SDK_INT >= 16) {
                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                    continueCenterView();
                }
            });
        }
        else
            continueCenterView();
    }
    private void continueCenterView()
    {
        RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) getLayoutParams();
        int parentHeight = ((ViewGroup) getParent()).getHeight();
        if(parentHeight != 0)
        {
            isCentered = true;
            int margin = (parentHeight - getHeight()) / 2;
            lp.setMargins(lp.leftMargin, margin, lp.rightMargin, margin);
            requestLayout();
        }
    }
}
