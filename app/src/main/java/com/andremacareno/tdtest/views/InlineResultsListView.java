package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;

import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.viewcontrollers.ViewControllable;
import com.andremacareno.tdtest.viewcontrollers.ViewController;

/**
 * Created by andremacareno on 04/04/15.
 */
public class InlineResultsListView extends BaseListView implements ViewControllable {
    private ViewController controller;
    private LayoutManager lm;
    public InlineResultsListView(Context context) {
        super(context);
        init();
    }

    public InlineResultsListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public InlineResultsListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init()
    {
        if(lm == null) {
            lm = new LinearLayoutManager(TelegramApplication.sharedApplication().getApplicationContext());
        }
        setLayoutManager(lm);
        //setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void setController(ViewController vc) {
        this.controller = vc;
    }

    @Override
    public ViewController getController() {
        return controller;
    }
    public void changeOrientation(boolean vertical)
    {
        try {
            stopScroll();
            ((LinearLayoutManager) lm).setOrientation(vertical ? LinearLayoutManager.VERTICAL : LinearLayoutManager.HORIZONTAL);
        }
        catch(Exception e) { e.printStackTrace(); }
    }
}
