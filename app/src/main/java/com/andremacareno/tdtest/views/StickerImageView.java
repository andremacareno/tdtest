package com.andremacareno.tdtest.views;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;

import com.andremacareno.tdtest.images.RecyclingImageView;

/**
 * Created by Andrew on 25.04.2015.
 */
public class StickerImageView extends RecyclingImageView {
    public StickerImageView(Context context) {
        super(context);
    }
    public StickerImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    @Override
    public void setPressed(boolean pressed)
    {
        if(getDrawable() != null && getDrawable() instanceof BitmapDrawable)
        {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) getDrawable();
            Paint paint = bitmapDrawable.getPaint();
            boolean hasFilter = paint != null && paint.getColorFilter() != null;
            if (hasFilter && !pressed) {
                bitmapDrawable.setColorFilter(null);
            } else if (!hasFilter && pressed) {
                bitmapDrawable.setColorFilter(new PorterDuffColorFilter(0xffdddddd, PorterDuff.Mode.MULTIPLY));
            }
        }
        super.setPressed(pressed);
    }
}
