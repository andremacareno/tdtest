package com.andremacareno.tdtest.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 17/08/15.
 */
public class DocumentDownloadButton extends DownloadButton {
    private int DOWNLOAD_BTN_SIZE;
    private Drawable downloadDrawable;

    private Drawable pauseDrawable;

    private Drawable openDrawable;
    private Paint circlePaint;

    public DocumentDownloadButton(Context context) {
        super(context);
    }

    public DocumentDownloadButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(DOWNLOAD_BTN_SIZE, DOWNLOAD_BTN_SIZE);
    }


    @Override
    protected void init()
    {
        DOWNLOAD_BTN_SIZE = getResources().getDimensionPixelSize(R.dimen.download_btn_size);
        downloadDrawable = getResources().getDrawable(R.drawable.ic_download_blue);
        pauseDrawable = getResources().getDrawable(R.drawable.ic_file_pause_blue);
        openDrawable = getResources().getDrawable(R.drawable.ic_file);
        if(isInEditMode())
            downloadDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-18, DOWNLOAD_BTN_SIZE/2-27, DOWNLOAD_BTN_SIZE/2+18, DOWNLOAD_BTN_SIZE/2+27);
        else
        {
            final int eighteenDp = AndroidUtilities.dp(18)/2;
            final int twelveDp = AndroidUtilities.dp(12)/2;
            final int fourteenDp = AndroidUtilities.dp(14)/2;
            final int fifteenDp = AndroidUtilities.dp(15)/2;
            downloadDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-twelveDp, DOWNLOAD_BTN_SIZE/2-eighteenDp, DOWNLOAD_BTN_SIZE/2+twelveDp, DOWNLOAD_BTN_SIZE/2+eighteenDp);
            pauseDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-twelveDp, DOWNLOAD_BTN_SIZE/2-fourteenDp, DOWNLOAD_BTN_SIZE/2+twelveDp, DOWNLOAD_BTN_SIZE/2+fourteenDp);
            openDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-fifteenDp, DOWNLOAD_BTN_SIZE/2-fourteenDp, DOWNLOAD_BTN_SIZE/2+fifteenDp, DOWNLOAD_BTN_SIZE/2+fourteenDp);
        }
        circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        circlePaint.setColor(ContextCompat.getColor(getContext(), R.color.light_blue_bg));
    }
    @Override
    public void onDraw(Canvas canvas)
    {
        canvas.drawCircle(DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, circlePaint);
        if(isInEditMode())
            downloadDrawable.draw(canvas);
        if(attachedFile == null)
            return;
        if(attachedFile.getDownloadState() == FileModel.DownloadState.EMPTY)
            downloadDrawable.draw(canvas);
        else if(attachedFile.getDownloadState() == FileModel.DownloadState.LOADING)
            pauseDrawable.draw(canvas);
        else
            openDrawable.draw(canvas);
    }
}
