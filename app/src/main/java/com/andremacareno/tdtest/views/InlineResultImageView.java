package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;

import com.andremacareno.tdtest.images.RecyclingImageView;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by Andrew on 25.04.2015.
 */
public class InlineResultImageView extends RecyclingImageView {
    public InlineResultImageView(Context context) {
        super(context);
    }

    public InlineResultImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    @Override
    public void onMeasure(int measureWidthSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(AndroidUtilities.dp(35), AndroidUtilities.dp(35));
    }
}
