package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.ChatParticipantModel;

/**
 * Created by andremacareno on 05/05/15.
 */
public class ChannelAdminsListItem extends FrameLayout {
    public AvatarImageView avatar;
    public TextView contactName;
    public TextView status;


    public ChannelAdminsListItem(Context context) {
        super(context);
        init();
    }
    public ChannelAdminsListItem(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    protected void init()
    {
        View itemView = inflate(getContext(), R.layout.channel_admins_list_item, this);
        this.avatar = (AvatarImageView) itemView.findViewById(R.id.avatar);
        this.contactName = (TextView) itemView.findViewById(R.id.contact_name);
        this.status = (TextView) itemView.findViewById(R.id.status);
        setBackgroundResource(R.drawable.profile_list_selector);
    }
    public AvatarImageView getAvatarImageView() { return this.avatar; }
    public void fillLayout(ChatParticipantModel itm)
    {
        contactName.setText(itm.getDisplayName());
        status.setText(itm.getCurrentRole().getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() ? R.string.creator : R.string.administrator);
    }
}