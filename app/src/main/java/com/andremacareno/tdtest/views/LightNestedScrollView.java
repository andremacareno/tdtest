package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;

/**
 * Created by andremacareno on 06/12/15.
 */
public class LightNestedScrollView extends NestedScrollView {
    public LightNestedScrollView(Context context) {
        super(context);
    }

    public LightNestedScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LightNestedScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
