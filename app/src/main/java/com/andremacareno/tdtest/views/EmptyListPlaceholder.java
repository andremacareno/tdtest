package com.andremacareno.tdtest.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;

import com.andremacareno.tdtest.R;

/**
 * Created by Andrew on 23.04.2015.
 */
public class EmptyListPlaceholder extends View {
    private final String TAG = "EmptyListPlaceholder";
    private String textToDraw;
    private Paint paint;
    private float textOriginX = 0;
    private float textOriginY = 0;
    public static int TOP = 0;
    public static int CENTER = 1;
    public static int BOTTOM = 2;
    public int location;

    public EmptyListPlaceholder(Context context) {
        super(context);
        init();
    }

    public EmptyListPlaceholder(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        processAttributes(attrs);
    }

    public EmptyListPlaceholder(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        processAttributes(attrs);
    }
    private void processAttributes(AttributeSet attrs)
    {
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.EmptyListPlaceholderAttrs,
                0, 0);
        try {
            textToDraw = a.getString(R.styleable.EmptyListPlaceholderAttrs_text);
            String locationStr = a.getString(R.styleable.EmptyListPlaceholderAttrs_location);
            if(locationStr == null)
                location = CENTER;
            else if(locationStr.equals("top"))
                location = TOP;
            else if(locationStr.equals("bottom"))
                location = BOTTOM;
            else
                location = CENTER;
        } finally {
            a.recycle();
        }
    }
    private void init()
    {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(ContextCompat.getColor(getContext(), R.color.empty_view_textcolor));
        paint.setTextSize(getResources().getDimensionPixelSize(R.dimen.empty_list));
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= 16) {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                textOriginX = getWidth() / 2.0f;
                if(location == CENTER)
                    textOriginY = getHeight() / 2.0f;
                else if(location == TOP)
                    textOriginY = getHeight() * 0.25f;
                else if(location == BOTTOM)
                    textOriginY = getHeight() * 0.75f;
                postInvalidate();
            }
        });
    }
    public void setText(@StringRes int res)
    {
        this.textToDraw = getResources().getString(res);
        postInvalidate();
    }
    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        Log.d("EmptyListPlaceholder", String.format("drawing text at %f %f", textOriginX, textOriginY));
        if(textToDraw != null && textOriginX != 0 && textOriginY != 0) {
            canvas.drawText(textToDraw, textOriginX, textOriginY, paint);
        }
    }
}
