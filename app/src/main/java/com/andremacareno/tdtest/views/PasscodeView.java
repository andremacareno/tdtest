package com.andremacareno.tdtest.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.viewcontrollers.passcode.PasscodeViewController;

/**
 * Created by andremacareno on 02/08/15.
 */
public class PasscodeView extends FrameLayout {
    private View v;
    public PasscodeView(Context context, PasscodeViewController.PasscodeType type) {
        super(context);
        init(type);
    }
    public void init(PasscodeViewController.PasscodeType type)
    {
        if(type == PasscodeViewController.PasscodeType.PIN)
            v = LayoutInflater.from(getContext()).inflate(R.layout.passcode_pin_layout, this, false);
        else if(type == PasscodeViewController.PasscodeType.PASSWORD)
            v = LayoutInflater.from(getContext()).inflate(R.layout.passcode_password_layout, this, false);
        else if(type == PasscodeViewController.PasscodeType.GESTURE)
            v = LayoutInflater.from(getContext()).inflate(R.layout.passcode_gesture_layout, this, false);
        else
            v = LayoutInflater.from(getContext()).inflate(R.layout.passcode_pattern_layout, this, false);
        v.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        addView(v);
    }
    public void passcodeTypeChanged(PasscodeViewController.PasscodeType newType)
    {
        removeView(v);
        init(newType);
    }
    public View getLayout() { return this.v; }
}
