package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.andremacareno.tdtest.fragments.SharedMediaFragment;
import com.andremacareno.tdtest.viewcontrollers.SharedMediaViewController;
import com.andremacareno.tdtest.viewcontrollers.ViewControllable;
import com.andremacareno.tdtest.viewcontrollers.ViewController;

/**
 * Created by andremacareno on 13/08/15.
 */
public class SharedMediaListView extends RecyclerView implements ViewControllable{
    private LinearLayoutManager audioLayoutManager;
    private GridLayoutManager imagesLayoutManager;
    private SharedMediaViewController viewController;
    private SharedMediaFragment.SharedMediaType currentType;
    public SharedMediaListView(Context context) {
        super(context);
        init();
    }

    public SharedMediaListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        setBackgroundColor(0xffffffff);
        audioLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        imagesLayoutManager = new GridLayoutManager(getContext(), 3);
        setMotionEventSplittingEnabled(false);
    }

    public void setDisplayMode(SharedMediaFragment.SharedMediaType type)
    {
        setAdapter(null);
        this.currentType = type;
        if(type == SharedMediaFragment.SharedMediaType.AUDIO)
            setLayoutManager(audioLayoutManager);
        else
            setLayoutManager(imagesLayoutManager);
        if(viewController != null)
            viewController.didDisplayModeChanged(type);
    }

    @Override
    public void setController(ViewController vc) {
        this.viewController = (SharedMediaViewController) vc;
    }
    public SharedMediaFragment.SharedMediaType getMediaType() { return this.currentType; }
    @Override
    public ViewController getController() {
        return viewController;
    }
}
