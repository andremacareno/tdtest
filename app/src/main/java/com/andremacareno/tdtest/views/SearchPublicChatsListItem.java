package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Emoji;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.ChatSearchResult;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 05/05/15.
 */
public class SearchPublicChatsListItem extends RelativeLayout {
    private final int AVATAR_POST_PROCESSING_MASK = TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE;
    private int avatarSize;
    private AvatarImageView avatar;
    private TextView chatName;
    private TextView username;

    public SearchPublicChatsListItem(Context context) {
        super(context);
        init();
    }
    public SearchPublicChatsListItem(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    private void init()
    {
        View itemView = inflate(getContext(), R.layout.search_public_chat_result, this);
        setClickable(true);
        setBackgroundResource(R.drawable.list_selector);
        this.avatar = (AvatarImageView) itemView.findViewById(R.id.avatar);
        this.chatName = (TextView) itemView.findViewById(R.id.chat_name);
        this.username = (TextView) itemView.findViewById(R.id.chat_username);
        this.avatarSize = getResources().getDimensionPixelSize(R.dimen.avatar_size);
    }
    public AvatarImageView getAvatarImageView() { return this.avatar; }
    public void fillLayout(ChatSearchResult chat)
    {
        chatName.setText(Emoji.replaceEmoji(chat.getChatObj().title, chatName.getPaint().getFontMetricsInt(), AndroidUtilities.dp(16), false));
        username.setText(chat.getUsername());
    }
    public void loadAvatar(ChatSearchResult itm)
    {
        TdApi.File file = CommonTools.isLowDPIScreen() ? itm.getChatObj().photo.small : itm.getChatObj().photo.big;
        String newKey = CommonTools.makeFileKey(file.id);
        FileModel photo = FileCache.getInstance().getFileModel(file);
        TelegramImageLoader.getInstance().loadImage(photo, newKey, avatar, AVATAR_POST_PROCESSING_MASK, avatarSize, avatarSize, false);
    }
    public void loadPlaceholder(ChatSearchResult itm)
    {
        TelegramImageLoader.getInstance().loadPlaceholder(itm.getPeer(), itm.getInitials(), itm.getPlaceholderKey(), avatar, false);
    }
}
