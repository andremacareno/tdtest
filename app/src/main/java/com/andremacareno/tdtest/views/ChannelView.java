package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.FileModel;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 22/06/15.
 */
public class ChannelView extends FrameLayout {
    private AvatarImageView avatar;
    private final int avatarSize = isInEditMode() ? 168 : TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size);
    private TextView channel_name;
    private TextView channel_second_line;
    private int channelId = 0;
    public ChannelView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.channel_view, this);
        avatar = (AvatarImageView) v.findViewById(R.id.avatar);
        channel_name = (TextView) v.findViewById(R.id.name);
        channel_second_line = (TextView) v.findViewById(R.id.second_line);
    }
    private void fillLayout()
    {
        if(channelId == 0)
            return;
        TdApi.Chat chat = ChatCache.getInstance().getChatByChannelId(channelId);
        if(chat == null || chat.type.getConstructor() != TdApi.ChannelChatInfo.CONSTRUCTOR)
            return;
        channel_name.setText(chat.title);
        TdApi.Channel channel = ((TdApi.ChannelChatInfo) chat.type).channel;
        channel_second_line.setText(CommonTools.generateChannelDescription(channelId));
        String initials = CommonTools.getInitialsByChatInfo(chat);
        FileModel avatarFileModel = FileCache.getInstance().getFileModel(CommonTools.isLowDPIScreen() ? chat.photo.small : chat.photo.big);
        if(avatarFileModel == null || avatarFileModel.getFileId() == 0) {
            String placeholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, channelId);
            TelegramImageLoader.getInstance().loadPlaceholder(channelId, initials, placeholderKey, avatar, false);
        }
        else {
            String avatarKey = CommonTools.makeFileKey(avatarFileModel.getFileId());
            TelegramImageLoader.getInstance().loadImage(avatarFileModel, avatarKey, avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, avatarSize, avatarSize, false);
        }
    }
    public void bindChannel(int id)
    {
        this.channelId = id;
        fillLayout();
    }

    public void updateTitle(String newTitle)
    {
        channel_name.setText(newTitle);
    }
    public void updateUserPhoto(FileModel newPhoto)
    {
        if(channelId == 0)
            return;
        if(newPhoto.getFileId() == 0)
        {
            TdApi.Chat chat = ChatCache.getInstance().getChatByChannelId(channelId);
            if(chat == null || chat.type.getConstructor() != TdApi.ChannelChatInfo.CONSTRUCTOR)
                return;
            String placeholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, channelId);
            String initials = CommonTools.getInitialsByChatInfo(chat);
            TelegramImageLoader.getInstance().loadPlaceholder(channelId, initials, placeholderKey, avatar, false);
        }
        else
        {
            String avatarKey = CommonTools.makeFileKey(newPhoto.getFileId());
            TelegramImageLoader.getInstance().loadImage(newPhoto, avatarKey, avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, avatarSize, avatarSize, true);
        }
    }
}
