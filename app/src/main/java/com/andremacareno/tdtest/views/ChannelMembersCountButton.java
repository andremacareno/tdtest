package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by andremacareno on 20/08/15.
 */
public class ChannelMembersCountButton extends RelativeLayout {
    private TextView membersCount;
    public ChannelMembersCountButton(Context context) {
        super(context);
        init();
    }

    public ChannelMembersCountButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.channel_members, this);
        membersCount = (TextView) v.findViewById(R.id.members_count);
    }
    public void setMembersCount(int count)
    {
        this.membersCount.setText(String.valueOf(count));
    }
}
