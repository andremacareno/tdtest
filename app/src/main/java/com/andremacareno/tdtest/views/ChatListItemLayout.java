package com.andremacareno.tdtest.views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.Emoji;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.ChatModel;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 05/05/15.
 */
public class ChatListItemLayout extends FrameLayout {
    private final int TEMPORARY_MESSAGE_ID = 1000000000;
    private final int AVATAR_POST_PROCESSING_MASK = TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE;
    private AvatarImageView avatar;
    private TextView dialogName;
    private TextView messagePreview;
    private TextView date;
    private TextView unreadCount;
    private View unreadCountLayout;
    private ColorStateList defaultTextColor = null;
    private Drawable groupChatDrawable;
    private Drawable clockDrawable;
    private int blueColor;
    private int avatarSize;
    private Drawable unreadDrawable;
    private Drawable mutedChatDrawable;
    private ColorMatrixColorFilter grayscaleFilter = null;

    public ChatListItemLayout(Context context) {
        super(context);
        init();
    }
    public ChatListItemLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    private void init()
    {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);

        grayscaleFilter = new ColorMatrixColorFilter(matrix);
        View itemView = inflate(getContext(), R.layout.chatlist_item_layout, this);
        setClickable(true);
        setBackgroundResource(R.drawable.list_selector);
        this.blueColor = ContextCompat.getColor(getContext(), R.color.vk_unread_text);
        this.avatar = (AvatarImageView) itemView.findViewById(R.id.avatar);
        this.dialogName = (TextView) itemView.findViewById(R.id.dialog_name);
        dialogName.setTypeface(AndroidUtilities.getTypeface("rmedium.ttf"));
        this.messagePreview = (TextView) itemView.findViewById(R.id.msg_preview);
        this.date = (TextView) itemView.findViewById(R.id.message_date);
        this.unreadCountLayout = itemView.findViewById(R.id.unread_count);
        this.unreadCount = (TextView) itemView.findViewById(R.id.unreadCountTextView);
        if(defaultTextColor == null)
            defaultTextColor = this.messagePreview.getTextColors();
        this.groupChatDrawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_group);
        this.clockDrawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_clock);
        this.unreadDrawable = ContextCompat.getDrawable(getContext(), R.drawable.unread_status_circle);
        this.mutedChatDrawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_mute);
        this.avatarSize = getResources().getDimensionPixelSize(R.dimen.avatar_size);
    }
    public AvatarImageView getAvatarImageView() { return this.avatar; }
    public void fillLayout(ChatModel itm)
    {
        dialogName.setText(Emoji.replaceEmoji(itm.getDisplayName(), dialogName.getPaint().getFontMetricsInt(), AndroidUtilities.dp(16), false));
        messagePreview.setText(Emoji.replaceEmoji(itm.getTopMessage(), dialogName.getPaint().getFontMetricsInt(), AndroidUtilities.dp(16), false));
        //if(itm.isServiceTopMessage() || isTyping)
        date.setText(itm.getTopMessageDate());
        int topMessageDate = itm.getTopMessageDateInt();
        if(topMessageDate == 0)
        {
            unreadCount.setVisibility(View.GONE);
            unreadCountLayout.setBackgroundResource(R.drawable.ic_error);
            unreadCountLayout.setVisibility(View.VISIBLE);
        }
        else
        {
            if(itm.getUnreadCount() <= 0)
                unreadCountLayout.setVisibility(View.GONE);
            else {
                unreadCountLayout.setVisibility(View.VISIBLE);
                unreadCountLayout.setBackgroundResource(R.drawable.ic_badge);
                /*if(itm.isMuted() && !LocalConfig.getInstance().isChatsMutedByDefault())
                    unreadCountLayout.getBackground().setColorFilter(grayscaleFilter);
                else
                    unreadCountLayout.getBackground().clearColorFilter();*/
                unreadCount.setVisibility(View.VISIBLE);
                unreadCount.setText(itm.getStringUnreadCount());
            }
        }
        if(itm.isServiceTopMessage())
            messagePreview.setTextColor(blueColor);
        else
            messagePreview.setTextColor(defaultTextColor);
        dialogName.setCompoundDrawablesWithIntrinsicBounds(null, null, itm.isMuted() ? mutedChatDrawable : null, null);
        checkStatus(itm);
    }
    public void loadAvatar(ChatModel itm)
    {
        String newKey = itm.getKey();
        FileModel photo = itm.getPhoto();
        TelegramImageLoader.getInstance().loadImage(photo, newKey, avatar, AVATAR_POST_PROCESSING_MASK, avatarSize, avatarSize, false);
    }
    public void loadPlaceholder(ChatModel itm)
    {
        TelegramImageLoader.getInstance().loadPlaceholder(itm.getPeer(), itm.getInitials(), itm.getPlaceholderKey(), avatar, false);
    }
    @Override
    public void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
    }
    public void checkStatus(ChatModel itm)
    {
        if(itm.getTopMessageId() >= TEMPORARY_MESSAGE_ID)
            date.setCompoundDrawablesWithIntrinsicBounds(clockDrawable, null, null, null);
        else if(itm.isOut() && itm.getLastReadOutboxMessageId() < itm.getTopMessageId())
            date.setCompoundDrawablesWithIntrinsicBounds(unreadDrawable, null, null, null);
        else
            date.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
    }
}
