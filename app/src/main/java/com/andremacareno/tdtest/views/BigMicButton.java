package com.andremacareno.tdtest.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 25/08/15.
 */
public class BigMicButton extends FrameLayout {
    public interface BigMicButtonDelegate
    {
        public void didRecordStarted();
        public void didRecordStopped();
        public void didVolumeLevelReceived(float percent);
        public void didMicButtonMoved(float translation, float threshold);
    }
    private View voiceIndicator;
    private float lastScale = 1.0f;
    private static final int maxVolume = AndroidUtilities.dp(120);
    private static final int minVolume = AndroidUtilities.dp(92);

    private final TimeInterpolator interpolator = new AccelerateInterpolator(1.4f);
    public BigMicButton(Context context) {
        super(context);
        init();
    }

    public BigMicButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        voiceIndicator = new View(getContext());
        voiceIndicator.setBackgroundResource(R.drawable.voice_volume);
        voiceIndicator.setLayoutParams(new LayoutParams(maxVolume, maxVolume, Gravity.CENTER));

        View voiceShadow = new View(getContext());
        voiceShadow.setBackgroundResource(R.drawable.voice_shadow);
        voiceShadow.setLayoutParams(new LayoutParams(AndroidUtilities.dp(87), AndroidUtilities.dp(87), Gravity.CENTER));

        View voiceEnabled = new View(getContext());
        voiceEnabled.setBackgroundResource(R.drawable.voice_enabled);
        voiceEnabled.setLayoutParams(new LayoutParams(AndroidUtilities.dp(84), AndroidUtilities.dp(84), Gravity.CENTER));

        View micButton = new View(getContext());
        micButton.setBackgroundResource(R.drawable.ic_mic_white);
        micButton.setLayoutParams(new LayoutParams(AndroidUtilities.dp(16), AndroidUtilities.dp(24), Gravity.CENTER));

        addView(voiceIndicator);
        addView(voiceShadow);
        addView(voiceEnabled);
        addView(micButton);
    }
    public void voiceRecordingEnabled()
    {
        if(Build.VERSION.SDK_INT >= 21)
            reveal(false);
        else
            animatePreLollipop(false);
    }

    public void voiceRecordingDisabled()
    {
        if(Build.VERSION.SDK_INT >= 21)
            reveal(true);
        else
            animatePreLollipop(true);
    }
    private void animatePreLollipop(boolean hide)
    {
        AnimatorSet animator = new AnimatorSet();
        if(!hide)
        {
            animator.playTogether(
                    ObjectAnimator.ofFloat(this, "scaleX", 0, 1.0f),
                    ObjectAnimator.ofFloat(this, "scaleY", 0, 1.0f)
            );
        }
        else
        {
            animator.playTogether(
                    ObjectAnimator.ofFloat(this, "scaleX", 1.0f, 0),
                    ObjectAnimator.ofFloat(this, "scaleY", 1.0f, 0)
            );
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    voiceIndicator.setScaleX(1.0f);
                    voiceIndicator.setScaleY(1.0f);
                    setVisibility(View.GONE);
                }
            });
        }
        animator.setInterpolator(interpolator);
        animator.setDuration(250);
        animator.start();
        if(!hide)
            setVisibility(View.VISIBLE);
    }
    @SuppressLint("NewApi")
    private void reveal(final boolean hide)
    {
        int cx = hide ? maxVolume / 2 : maxVolume;
        int cy = hide ? maxVolume / 2 : maxVolume;

        int finalRadius = hide ? maxVolume / 2 : maxVolume;

        Animator anim =
                ViewAnimationUtils.createCircularReveal(this, cx, cy, hide ? finalRadius : 0, hide ? 0 : finalRadius);
        anim.setDuration(250);
        if(!hide)
        {
            anim.setInterpolator(interpolator);
            setVisibility(View.VISIBLE);
            anim.start();
        }
        else
        {
            anim.setInterpolator(interpolator);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    voiceIndicator.setScaleX(1.0f);
                    voiceIndicator.setScaleY(1.0f);
                    setVisibility(View.GONE);
                }
            });
            // start the animation
            anim.start();
        }
    }
    public void updateVolume(float volumeRel)
    {
        final float finalScale = 0.85f + volumeRel * 0.15f;
        AnimatorSet scale = new AnimatorSet();
        scale.playTogether(ObjectAnimator.ofFloat(voiceIndicator, "scaleX", lastScale, finalScale), ObjectAnimator.ofFloat(voiceIndicator, "scaleY", lastScale, finalScale));
        scale.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                lastScale = finalScale;
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        scale.setDuration(200);
        scale.setInterpolator(interpolator);
        scale.start();
    }
}
