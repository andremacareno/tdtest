package com.andremacareno.tdtest.views.player;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by andremacareno on 15/08/15.
 */
public class MiniPlayerView extends LinearLayout {
    public interface MiniPlayerDelegate
    {
        public void didPlayButtonPressed();
        public void didCloseButtonPressed();
        public void didOpenPlayerRequested();
    }
    private ImageButton playButton;
    private ImageButton closeButton;
    private TextView songName;
    private ProgressBar playbackProgress;
    private MiniPlayerDelegate delegate;
    private final StyleSpan boldText = new StyleSpan(android.graphics.Typeface.BOLD);
    public MiniPlayerView(Context context) {
        super(context);
        init();
    }

    public MiniPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        setOrientation(VERTICAL);
        setBackgroundResource(R.color.white);
        View v = inflate(getContext(), R.layout.mini_player, this);
        playButton = (ImageButton) v.findViewById(R.id.miniplayer_play);
        playButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(delegate != null)
                    delegate.didPlayButtonPressed();
            }
        });
        closeButton = (ImageButton) v.findViewById(R.id.miniplayer_close);
        closeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(delegate != null)
                    delegate.didCloseButtonPressed();
            }
        });
        songName = (TextView) v.findViewById(R.id.song_name);
        songName.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(delegate != null)
                    delegate.didOpenPlayerRequested();
            }
        });
        playbackProgress = (ProgressBar) v.findViewById(R.id.playback_progress);
    }
    public void setDelegate(MiniPlayerDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    public void setProgressBarMax(int max)
    {
        playbackProgress.setMax(max);
    }
    public void updateProgressBar(int progress)
    {
        playbackProgress.setProgress(progress);
    }
    public void updatePlayButton(boolean isPlaying)
    {
        if(isPlaying)
            playButton.setImageResource(R.drawable.ic_pausepl);
        else
            playButton.setImageResource(R.drawable.ic_playpl);
    }
    public void updateSongData(String name, String performer)
    {
        SpannableString str = new SpannableString(String.format("%s — %s", name, performer));
        str.setSpan(boldText, 0, name.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        songName.setText(str);
    }
}
