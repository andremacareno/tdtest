package com.andremacareno.tdtest.views.actionviews;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.PorterDuff;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by Andrew on 13.05.2015.
 */
public abstract class SharedMediaActionView extends ActionView {
    public View defaultModeContainer;
    public ImageButton backButton;
    public TextView mediaTypeTextView;
    public View mediaTypeMenu;

    public View selectionModeContainer;
    public ImageButton disableButton;
    public TextView mediaCountTextView;
    public ImageButton forwardButton;
    public ImageButton delButton;
    private final AccelerateInterpolator accInterpolator = new AccelerateInterpolator(1.7f);
    private final DecelerateInterpolator decInterpolator = new DecelerateInterpolator(1.3f);

    public SharedMediaActionView(Context context) {
        super(context);
    }
    @Override
    protected void initView()
    {
        View v = inflate(getContext(), R.layout.action_view_shared_media, this);
        defaultModeContainer = v.findViewById(R.id.sharedmedia_default_action_view);
        backButton = (ImageButton) defaultModeContainer.findViewById(R.id.back);
        mediaTypeTextView = (TextView) defaultModeContainer.findViewById(R.id.sharedmedia_type);
        mediaTypeMenu = defaultModeContainer.findViewById(R.id.passcode_type_caret);
        mediaTypeMenu.getBackground().setColorFilter(0xff818181, PorterDuff.Mode.MULTIPLY);

        selectionModeContainer = v.findViewById(R.id.sharedmedia_selectionmode_action_view);
        disableButton = (ImageButton) selectionModeContainer.findViewById(R.id.disable_multiselection);
        mediaCountTextView = (TextView) selectionModeContainer.findViewById(R.id.selection_count);
        forwardButton = (ImageButton) selectionModeContainer.findViewById(R.id.fwd_selection);
        delButton = (ImageButton) selectionModeContainer.findViewById(R.id.del_selection);
    }
    public void setMode(boolean selectionMode, boolean animated)
    {
        if(selectionMode) {
            if(animated)
                slideDownSelectionBar();
            else
                selectionModeContainer.setTranslationY(0);
        }
        else
        {
            if(animated)
                slideUpSelectionBar();
            else
                selectionModeContainer.setTranslationY(-AndroidUtilities.dp(56));
        }
    }

    private void slideDownSelectionBar()
    {
        ObjectAnimator animator = ObjectAnimator.ofFloat(selectionModeContainer, "translationY", -AndroidUtilities.dp(56), 0).setDuration(180);
        animator.setInterpolator(decInterpolator);
        animator.start();
        selectionModeContainer.setVisibility(View.VISIBLE);
    }
    private void slideUpSelectionBar()
    {
        ObjectAnimator animator = ObjectAnimator.ofFloat(selectionModeContainer, "translationY", 0, -AndroidUtilities.dp(56)).setDuration(180);
        animator.setInterpolator(accInterpolator);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                //selectionModeContainer.setVisibility(View.INVISIBLE);
            }
        });
        animator.start();
        animator.setInterpolator(accInterpolator);
        animator.start();
    }

}
