package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.viewcontrollers.ChatController;
import com.andremacareno.tdtest.viewcontrollers.ViewControllable;
import com.andremacareno.tdtest.viewcontrollers.ViewController;

/**
 * Created by andremacareno on 04/04/15.
 */
public class BaseChatListView extends SwipeMenuRecyclerView implements ViewControllable {
    private ChatController controller;
    private LayoutManager lm;
    private boolean is_scrollable = true;
    public BaseChatListView(Context context) {
        super(context);
        init();
    }

    public BaseChatListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BaseChatListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    @Override
    protected void init()
    {
        super.init();
        if(isInEditMode())
            return;
        if(lm == null) {
            lm = new LinearLayoutManager(TelegramApplication.sharedApplication().getApplicationContext());
            ((LinearLayoutManager) lm).setStackFromEnd(true);
        }
        setLayoutManager(lm);
        setHasFixedSize(false);
        /*((LinearLayoutManager) lm).setReverseLayout(true);
        ((LinearLayoutManager) lm).setStackFromEnd(true);*/
        setItemAnimator(new DefaultItemAnimator());
    }
    @Override
    public void setController(ViewController vc)
    {
        this.controller = (ChatController) vc;
    }

    @Override
    public ViewController getController() {
        return controller;
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev)
    {
        forceSuperOnInterceptTouchEvent = controller != null && controller.isInSelectionMode();
        return super.onInterceptTouchEvent(ev);
    }
    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
