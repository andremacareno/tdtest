package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by Andrew on 12.05.2015.
 */
public class CountryListItem extends FrameLayout {
    private TextView country;
    private TextView code;
    public CountryListItem(Context context) {
        super(context);
        init();
    }

    public CountryListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CountryListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.country_list_layout, this);
        country = (TextView) v.findViewById(R.id.country_name);
        code = (TextView) v.findViewById(R.id.country_code);
    }
    public void setCountryName(String country)
    {
        this.country.setText(country);
    }
    public void setCountryCode(String code)
    {
        this.code.setText(code);
    }
}
