package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by andremacareno on 20/08/15.
 */
public class SharedMediaButton extends RelativeLayout {
    private TextView sharedMediaCount;
    private SharedMediaLightListView previewList;
    public SharedMediaButton(Context context) {
        super(context);
        init();
    }

    public SharedMediaButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.shared_media_button, this);
        sharedMediaCount = (TextView) v.findViewById(R.id.media_count);
        previewList = (SharedMediaLightListView) v.findViewById(R.id.previews);
    }
    public void setMediaCount(int count)
    {
        if(count == 0)
            previewList.setVisibility(View.GONE);
        this.sharedMediaCount.setText(String.valueOf(count));
    }
    public SharedMediaLightListView getPreviewList() { return this.previewList; }
}
