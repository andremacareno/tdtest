package com.andremacareno.tdtest.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.utils.AndroidUtilities;

public class RevealingCheckView extends View {
    //private Paint ringPaint;
    private Paint circlePaint;

    private final float radius = AndroidUtilities.dp(11);
    private final float center = radius + AndroidUtilities.dp(1);
    private boolean reverse = false;

    private int time = 0;
    private int fillDuration = 160;
    private double result = 0.0;
    private boolean isAnimating = false;
    private Drawable checkDrawable;
    private int checkAnimTime = 0;
    private Rect checkRect;
    private int checkLevel = 0;
    private boolean skipAnimation = false;
    //private boolean showRing = false;
    public RevealingCheckView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public RevealingCheckView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public RevealingCheckView(Context context) {
        super(context); init();
    }
    /*public void setShowRing(boolean showRing)
    {
        this.showRing = showRing;
    }*/
    private void init()
    {
        //checkDrawable = new ClipDrawable(new ScaleDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_check), 0, AndroidUtilities.dp(18), AndroidUtilities.dp(18)), Gravity.LEFT, ClipDrawable.HORIZONTAL);
        checkDrawable = new ClipDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_check), Gravity.LEFT, ClipDrawable.HORIZONTAL);
        /*ringPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        ringPaint.setStyle(Paint.Style.STROKE);
        ringPaint.setStrokeWidth(AndroidUtilities.dp(2));
        ringPaint.setColor(ContextCompat.getColor(getContext(), R.color.sharedaudio_selection));*/
        checkRect = new Rect();
        circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        circlePaint.setStyle(Paint.Style.FILL);
        circlePaint.setColor(Color.rgb(131, 208, 140));
    }
    public void uncheck(boolean animate)
    {
        setVisibility(View.VISIBLE);
        skipAnimation = !animate;
        reverse = true;
        isAnimating = true;
        time = checkAnimTime = fillDuration;
        checkLevel = animate ? 10000 : 0;
        postInvalidate();
    }
    public void check(boolean animate)
    {
        setVisibility(View.VISIBLE);
        skipAnimation = !animate;
        isAnimating = true;
        reverse = false;
        checkLevel = animate ? 0 : 10000;
        time = checkAnimTime = 0;
        postInvalidate();
    }
    @Override
    protected void onDraw(Canvas canvas) {
        /*if(showRing)
            canvas.drawCircle(center, center, radius, ringPaint);*/
        canvas.getClipBounds(checkRect);
        checkRect.left += AndroidUtilities.dp(1);
        checkRect.right -= AndroidUtilities.dp(1);
        checkDrawable.setBounds(checkRect);
        if(isAnimating && !skipAnimation) {
            canvas.drawCircle(center, center, getCircleRadius(), circlePaint);
            checkDrawable.setLevel(checkLevel);
            checkDrawable.draw(canvas);
            if(!reverse && checkLevel < 10000)
            {
                if(time > fillDuration && checkAnimTime > fillDuration) {
                    checkAnimTime = 0;
                    time = 0;
                    result = 0.0;
                    checkLevel = 10000;
                    isAnimating = false;
                }
                else
                    postInvalidateDelayed(16);
            }
            else if(reverse)
            {
                if(time < 0 && checkAnimTime < 0) {
                    time = 0;
                    checkAnimTime = 0;
                    result = 0.0;
                    checkLevel = 0;
                    isAnimating = false;
                } else
                    postInvalidateDelayed(16);
            }
        }
        else if(isAnimating && checkLevel > 0 && !reverse)
        {
            canvas.drawCircle(center, center, radius, circlePaint);
            checkDrawable.setLevel(checkLevel);
            checkDrawable.draw(canvas);
        }
        if(reverse && (!isAnimating || skipAnimation) && getVisibility() != GONE) //&& !showRing
            setVisibility(View.GONE);
    }

    @Override
    public void onMeasure(int measureWidthSpec, int measureHeightSpec)
    {
        setMeasuredDimension(AndroidUtilities.dp(24), AndroidUtilities.dp(24));
    }

    private float getCircleRadius()
    {
        //result = radius * Math.pow(2, 10 * (((double) time)/fillDuration - 1.0) );
        if(!reverse)
        {
            if(checkAnimTime != 0 || time >= 112)
            {
                checkLevel = Math.round(10000.0f * (float)Math.sqrt(1.0f - ((float)checkAnimTime/fillDuration-1.0f)*((float)checkAnimTime/fillDuration-1.0f)));
                checkAnimTime += 16;
            }
            if(time <= fillDuration) {
                result = radius * Math.sqrt(1.0 - ((double) time / fillDuration - 1.0) * ((double) time / fillDuration - 1.0));
                time += 16;
            }
            return (float) result;
        }
        else
        {
            if(checkAnimTime >= 0)
            {
                checkLevel = Math.round(10000.0f * (float)Math.sqrt(1.0f - ((float)checkAnimTime/fillDuration-1.0f)*((float)checkAnimTime/fillDuration-1.0f)));
                checkAnimTime -= 16;
            }
            if(time >= 0 && checkAnimTime <= 48) {
                result = radius * Math.sqrt(1.0 - ((double) time / fillDuration - 1.0) * ((double) time / fillDuration - 1.0));
                time -= 16;
            }
            else
                result = radius;
            return (float) result;
        }
    }
}
