package com.andremacareno.tdtest.views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.ChatParticipantModel;

/**
 * Created by andremacareno on 05/05/15.
 */
public class ChooseAdminParticipantListItem extends FrameLayout {
    public AvatarImageView avatar;
    public TextView contactName;
    public TextView status;
    public AppCompatCheckBox checkbox;
    protected ColorStateList defaultTextColor;
    protected ColorStateList defaultCheckboxColor, inactiveCheckboxColor;


    public ChooseAdminParticipantListItem(Context context) {
        super(context);
        init();
    }
    public ChooseAdminParticipantListItem(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    protected void init()
    {
        View itemView = inflate(getContext(), R.layout.choose_admin_list_item, this);
        this.avatar = (AvatarImageView) itemView.findViewById(R.id.avatar);
        this.contactName = (TextView) itemView.findViewById(R.id.contact_name);
        this.status = (TextView) itemView.findViewById(R.id.status);
        this.checkbox = (AppCompatCheckBox) itemView.findViewById(R.id.checkbox);
        setBackgroundResource(R.drawable.profile_list_selector);
        defaultTextColor = this.status.getTextColors();
        inactiveCheckboxColor = new ColorStateList(
                new int[][]{
                        new int[]{-android.R.attr.state_checked},
                        new int[]{android.R.attr.state_checked}
                },
                new int[]{
                        Color.rgb(129, 129, 129)
                        , Color.rgb(129, 129, 129),
                });
        defaultCheckboxColor = new ColorStateList(
                new int[][]{
                        new int[]{-android.R.attr.state_checked},
                        new int[]{android.R.attr.state_checked}
                },
                new int[]{
                        Color.rgb(129, 129, 129)
                        , ContextCompat.getColor(getContext(), R.color.header_color)
                });
    }
    public AvatarImageView getAvatarImageView() { return this.avatar; }
    public void fillLayout(ChatParticipantModel itm)
    {
        contactName.setText(itm.getDisplayName());
        if(!itm.isBot())
            status.setText(itm.getStringStatus());
        else
            status.setText(itm.canBotReadMessages() ? R.string.has_access_to_messages : R.string.has_no_access_to_messages);
        if(itm.isOnline() && !itm.isBot())
            status.setTextColor(ContextCompat.getColor(TelegramApplication.sharedApplication().getApplicationContext(), R.color.vk_unread_text));
        else
            status.setTextColor(defaultTextColor);

        if(itm.getCurrentRole().getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor()) {
            checkbox.setSupportButtonTintList(inactiveCheckboxColor);
            checkbox.setChecked(true);
        }
        else
        {
            checkbox.setSupportButtonTintList(defaultCheckboxColor);
            checkbox.setChecked(itm.getCurrentRole().getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor());
        }
    }
}
