package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.GroupCache;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 22/06/15.
 *
 */
public class GroupChatView extends FrameLayout {

    public AvatarImageView avatar;
    public View editModeAvatar;
    public EditText newGroupName;
    private final int avatarSize = isInEditMode() ? 168 : TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size);
    private TextView chat_name;
    private TextView members_info;
    private TdApi.Group boundChat;
    private TdApi.Chat chatRef;

    public GroupChatView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.group_chat_view, this);
        editModeAvatar = v.findViewById(R.id.avatar_edit_mode);
        newGroupName = (EditText) v.findViewById(R.id.edit_mode_group_name);
        avatar = (AvatarImageView) v.findViewById(R.id.avatar);
        chat_name = (TextView) v.findViewById(R.id.name);
        members_info = (TextView) v.findViewById(R.id.status);
    }
    private void fillLayout()
    {
        if(boundChat == null || chatRef == null)
            return;
        chat_name.setText(chatRef.title);
        members_info.setText(CommonTools.generateChatDescription(boundChat.id, false));
        String initials = chatRef.title.substring(0, 1).toUpperCase();
        FileModel avatarFileModel = FileCache.getInstance().getFileModel(CommonTools.isLowDPIScreen() ? chatRef.photo.small : chatRef.photo.big);
        if(avatarFileModel == null || avatarFileModel.getFileId() == 0) {
            String placeholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, boundChat.id);
            TelegramImageLoader.getInstance().loadPlaceholder(boundChat.id, initials, placeholderKey, avatar, false);
        }
        else {
            String avatarKey = CommonTools.makeFileKey(avatarFileModel.getFileId());
            TelegramImageLoader.getInstance().loadImage(avatarFileModel, avatarKey, avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, avatarSize, avatarSize, false);
        }
    }
    public void setEditMode(boolean editMode)
    {
        if(editMode)
        {
            chat_name.setVisibility(View.GONE);
            members_info.setVisibility(View.GONE);
            editModeAvatar.setVisibility(View.VISIBLE);
            newGroupName.setVisibility(View.VISIBLE);
            AndroidUtilities.showKeyboard(newGroupName);
        }
        else
        {
            closeKeyboard();
            if(!newGroupName.getText().toString().isEmpty())
                chat_name.setText(newGroupName.getText());
            chat_name.setVisibility(View.VISIBLE);
            members_info.setVisibility(View.VISIBLE);
            editModeAvatar.setVisibility(View.GONE);
            newGroupName.setVisibility(View.GONE);
        }
    }
    public void closeKeyboard()
    {
        AndroidUtilities.hideKeyboard(newGroupName);
    }
    public void updateOnlineCount()
    {
        members_info.setText(CommonTools.generateChatDescription(boundChat.id, false));
    }
    public String getNewTitle()
    {
        return newGroupName.getText().toString();
    }
    public void removePhoto()
    {
        avatar.setImageDrawable(null);
    }
    public void updateTitle(String newTitle)
    {
        chat_name.setText(newTitle);
    }
    public void updatePhoto(TdApi.File newPhoto)
    {
        if(newPhoto.id == 0) {
            String initials = chatRef.title.substring(0, 1).toUpperCase();
            String placeholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, boundChat.id);
            TelegramImageLoader.getInstance().loadPlaceholder(boundChat.id, initials, placeholderKey, avatar, false);
        }
        else {
            String avatarKey = CommonTools.makeFileKey(newPhoto.id);
            TelegramImageLoader.getInstance().loadImage(FileCache.getInstance().getFileModel(newPhoto), avatarKey, avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, avatarSize, avatarSize, false);
        }
    }
    public void bindChat(int groupId)
    {
        this.boundChat = GroupCache.getInstance().getGroup(groupId, false);
        this.chatRef = ChatCache.getInstance().getChatByGroupId(groupId);
        fillLayout();
    }
}
