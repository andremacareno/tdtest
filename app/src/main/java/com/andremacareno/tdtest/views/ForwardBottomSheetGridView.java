package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.util.AttributeSet;

import com.andremacareno.tdtest.viewcontrollers.ViewControllable;
import com.andremacareno.tdtest.viewcontrollers.ViewController;

/**
 * Created by andremacareno on 04/04/15.
 */
public class ForwardBottomSheetGridView extends BaseListView implements ViewControllable {
    private ViewController controller;
    private LayoutManager lm;
    public ForwardBottomSheetGridView(Context context) {
        super(context);
        init();
    }

    public ForwardBottomSheetGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ForwardBottomSheetGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init()
    {
        if(isInEditMode())
            return;
        if(lm == null) {
            lm = new GridLayoutManager(getContext(), 4);
        }
        setLayoutManager(lm);
        setHasFixedSize(false);
        setItemAnimator(new DefaultItemAnimator());
    }
    @Override
    public void setController(ViewController vc)
    {
        this.controller = vc;
    }

    @Override
    public ViewController getController() {
        return controller;
    }
    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
