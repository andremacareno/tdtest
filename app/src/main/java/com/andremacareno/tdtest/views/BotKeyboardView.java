package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.BotKeyboardModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 13/08/15.
 */
public class BotKeyboardView extends RecyclerView{
    private static final int SPAN_COUNT = 1200;
    private GridLayoutManager lm;
    private volatile int approximateHeight = 0;
    public BotKeyboardView(Context context) {
        super(context);
        init();
    }

    public BotKeyboardView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.kbd_background));
        if(lm == null)
            lm = new GridLayoutManager(getContext(), 1);
        lm.setSpanCount(SPAN_COUNT);
        setLayoutManager(lm);
    }
    public void updateGridLayout(final BotKeyboardModel kbd)
    {
        approximateHeight = AndroidUtilities.dp(60) * kbd.getRowCount();
        lm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int row = kbd.rowOfButton(position);
                return SPAN_COUNT / kbd.getRowSize(row);
            }
        });
    }
    public int getApproximateHeight() { return this.approximateHeight; }
}
