package com.andremacareno.tdtest.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 01/08/15.
 */
public class PasscodePinButton extends View {
    private final int padding = AndroidUtilities.dp(4);
    private int width = 0;
    private int height = 0;
    private int digitPositionY;
    private int letterPositionY;
    private TextPaint letterPaint;
    private TextPaint digitPaint;
    private final Drawable backspaceDrawable = getResources().getDrawable(R.drawable.ic_passcode_delete);
    private boolean showBackspace;
    private String digit;
    private String letters;

    public PasscodePinButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        //process attributes
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.PinPadAttributes,
                0, 0);

        try {
            showBackspace = a.getBoolean(R.styleable.PinPadAttributes_showBackspace, false);
            digit = a.getString(R.styleable.PinPadAttributes_digit);
            letters = a.getString(R.styleable.PinPadAttributes_letters);
        } finally {
            a.recycle();
        }
        init();
    }
    private void init()
    {
        setBackgroundResource(R.drawable.pin_keyboard);
        setClickable(true);
        if(showBackspace)
        {
            if(isInEditMode())
                backspaceDrawable.setBounds(63, 72, 135, 132);
            else
                backspaceDrawable.setBounds(AndroidUtilities.dp(21), AndroidUtilities.dp(26), AndroidUtilities.dp(45), AndroidUtilities.dp(44));
        }
        else
        {
            digitPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
            digitPaint.setTextAlign(Paint.Align.CENTER);
            digitPaint.setColor(ContextCompat.getColor(getContext(), R.color.white));
            digitPaint.setStrokeWidth(1.0f);
            digitPaint.setTypeface(AndroidUtilities.getTypeface("Roboto-Light.ttf"));
            letterPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
            letterPaint.setColor(ContextCompat.getColor(getContext(), R.color.passcode_letters));
            letterPaint.setTextAlign(Paint.Align.CENTER);
            letterPaint.setStrokeWidth(1.0f);
        }
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(getMeasuredWidth() == 0 || getMeasuredHeight() == 0)
                    return;
                if(Build.VERSION.SDK_INT >= 16)
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                else
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                didLayoutFinished();
            }
        });
    }
    private void didLayoutFinished()
    {
        width = getWidth() / 3;
        height = getHeight() / 4;
        int diff = (Math.max(width, height) - Math.min(width, height)) / 2;
        getLayoutParams().height = Math.min(width, height);
        getLayoutParams().width = Math.min(width, height);
        ((GridLayout.LayoutParams) getLayoutParams()).setMargins(diff, 0, diff, 0);
        width = height = getLayoutParams().height;
        if(BuildConfig.DEBUG)
            Log.d("PasscodePinButton", String.format("width = %d, height = %d", width, height));
        if(showBackspace)
        {
            backspaceDrawable.setBounds((width / 2) - AndroidUtilities.dp(12), (height / 2) - AndroidUtilities.dp(9), (height / 2) + AndroidUtilities.dp(12), (height / 2) + AndroidUtilities.dp(9));
        }
        else
        {
            int digitTextSize = 2 * (height-3*padding) / 3;
            digitPositionY = digitTextSize + padding;
            digitPaint.setTextSize(digitTextSize);
            letterPositionY = 2*padding + digitPositionY + digitTextSize / 3;
            letterPaint.setTextSize(digitTextSize / 3);
        }
        requestLayout();
        postInvalidate();
    }
    @Override
    public void onDraw(Canvas canvas)
    {
        if(width == 0 || height == 0)
            return;
        if(showBackspace)
            backspaceDrawable.draw(canvas);
        else
        {
            canvas.drawText(digit, width / 2, digitPositionY, digitPaint);
            canvas.drawText(letters, width / 2, letterPositionY, letterPaint);
        }
    }
}
