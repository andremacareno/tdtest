package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.Emoji;
import com.andremacareno.tdtest.images.RecyclingImageView;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.utils.AndroidUtilities;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 10/08/15.
 */
public class StickerEmojiView extends FrameLayout {

    private RecyclingImageView imageView;
    private TdApi.Sticker attachedSticker;
    private TextView emojiTextView;

    public StickerEmojiView(Context context) {
        super(context);

        imageView = new StickerImageView(context);
        addView(imageView, new LayoutParams(AndroidUtilities.dp(66), AndroidUtilities.dp(66), Gravity.CENTER));

        emojiTextView = new TextView(context);
        emojiTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
        addView(emojiTextView, new LayoutParams(AndroidUtilities.dp(28), AndroidUtilities.dp(28), Gravity.BOTTOM | Gravity.RIGHT));
    }

    @Override
    public void setPressed(boolean pressed) {
        if (imageView.isPressed() != pressed) {
            imageView.setPressed(pressed);
            imageView.invalidate();
        }
        super.setPressed(pressed);
    }

    public TdApi.Sticker getAttachedSticker() {
        return attachedSticker;
    }

    public void setSticker(TdApi.Sticker sticker, boolean showEmoji) {
        if (sticker == null)
            return;
        attachedSticker = sticker;
        TelegramImageLoader.getInstance().loadSticker(sticker, TelegramImageLoader.CMD_RETURN_STICKER_THUMB, imageView);
        if (showEmoji) {
            boolean set = sticker.emoji != null && sticker.emoji.length() > 0;
            if(set) {
                emojiTextView.setText(Emoji.replaceEmoji(sticker.emoji, emojiTextView.getPaint().getFontMetricsInt(), AndroidUtilities.dp(16), false));
                emojiTextView.setVisibility(VISIBLE);
            }
            else
                emojiTextView.setVisibility(INVISIBLE);
        } else {
            emojiTextView.setVisibility(INVISIBLE);
        }
    }
}
