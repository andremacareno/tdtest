package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.images.RecyclingImageView;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.WebPageMessageModel;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 05/05/15.
 */
public class WebPagePreview extends FrameLayout {
    private TextView siteName;
    private TextView title;
    private TextView author;
    private TextView description;
    private WebPageArticleImageView articlePreview;
    private RecyclingImageView photoImageView;


    public WebPagePreview(Context context) {
        super(context);
        init();
    }
    public WebPagePreview(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    private void init()
    {
        View itemView = inflate(getContext(), R.layout.webpage_preview, this);
        this.siteName = (TextView) itemView.findViewById(R.id.preview_site_name);
        this.title = (TextView) itemView.findViewById(R.id.preview_site_title);
        this.author = (TextView) itemView.findViewById(R.id.preview_site_author);
        this.description = (TextView) itemView.findViewById(R.id.preview_site_description);
        this.articlePreview = (WebPageArticleImageView) itemView.findViewById(R.id.preview_article_img);
        this.photoImageView = (RecyclingImageView) itemView.findViewById(R.id.preview_photo);
    }
    public void attachWebPage(TdApi.WebPage wp)
    {
        if(wp.siteName.length() == 0)
            siteName.setVisibility(View.GONE);
        else
        {
            siteName.setVisibility(View.VISIBLE);
            siteName.setText(wp.siteName);
        }

        if(wp.title.length() == 0)
            title.setVisibility(View.GONE);
        else
        {
            title.setVisibility(View.VISIBLE);
            title.setText(wp.title);
        }

        if(wp.author.length() == 0)
            author.setVisibility(View.GONE);
        else
        {
            author.setVisibility(View.VISIBLE);
            author.setText(wp.author);
        }

        if(wp.description.length() == 0)
            description.setVisibility(View.GONE);
        else
        {
            description.setVisibility(View.VISIBLE);
            description.setText(wp.description);
        }
    }
    public void setPhoto(WebPageMessageModel msgModel)
    {
        if(msgModel.getPageContainer().type.equals("article") || msgModel.getPageContainer().type.equals("app"))
        {
            photoImageView.setVisibility(View.GONE);
            if(msgModel.getPhoto() == null || msgModel.getPhoto().getFileId() == 0)
                articlePreview.setVisibility(View.GONE);
            else
            {
                articlePreview.setVisibility(View.VISIBLE);
                TelegramImageLoader.getInstance().loadImage(msgModel.getPhoto(), CommonTools.makeFileKey(msgModel.getPhoto().getFileId()), articlePreview, TelegramImageLoader.POST_PROCESSING_NOTHING, getResources().getDimensionPixelSize(R.dimen.avatar_size), getResources().getDimensionPixelSize(R.dimen.avatar_size), false);
            }
        }
        else
        {
            articlePreview.setVisibility(View.GONE);
            if(msgModel.getPhoto() == null || msgModel.getPhoto().getFileId() == 0)
                photoImageView.setVisibility(View.GONE);
            else
            {
                photoImageView.setVisibility(View.VISIBLE);
                TelegramImageLoader.getInstance().loadImage(msgModel.getPhoto(), CommonTools.makeFileKey(msgModel.getPhoto().getFileId()), photoImageView, TelegramImageLoader.POST_PROCESSING_NOTHING, msgModel.getPhotoWidth(), msgModel.getPhotoHeight(), false);
            }
        }
    }
    public void setPhotoClickListener(OnClickListener listener)
    {
        photoImageView.setOnClickListener(listener);
        articlePreview.setOnClickListener(listener);
    }
}
