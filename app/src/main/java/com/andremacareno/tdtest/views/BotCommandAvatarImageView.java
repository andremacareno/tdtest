package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;

import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by Andrew on 25.04.2015.
 */
public class BotCommandAvatarImageView extends AvatarImageView {
    public BotCommandAvatarImageView(Context context) {
        super(context);
    }

    public BotCommandAvatarImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    @Override
    public void onMeasure(int measureWidthSpec, int heightMeasureSpec)
    {
        if(isInEditMode())
            setMeasuredDimension(84, 84);
        else
           setMeasuredDimension(AndroidUtilities.dp(28), AndroidUtilities.dp(28));
    }
}
