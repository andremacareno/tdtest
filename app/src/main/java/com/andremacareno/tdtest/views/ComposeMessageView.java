package com.andremacareno.tdtest.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.UserCache;
import com.andremacareno.tdtest.models.InlineBotShortModel;
import com.andremacareno.tdtest.models.InlineQueryResultWrap;
import com.andremacareno.tdtest.utils.AndroidUtilities;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Arrays;
import java.util.List;

/**
 * Created by andremacareno on 10/04/15.
 */
public class ComposeMessageView extends FrameLayout {
    public static final int IC_SMILE = 0;
    public static final int IC_KEYBOARD = 1;
    private volatile boolean lockSendMessage;
    private EditText messageEditText;
    private TextView inlineHelp;
    private ImageButton smilesButton;
    private ImageButton attachButton;
    private ImageButton micButton;
    private ImageButton slashButton;
    private ImageButton kbdButton;
    private ImageButton sendMessageButton;
    private AnimatorSet runningAnimation;
    private AnimatorSet runningAnimation2;
    private int runningAnimationType;
    private long lastTypingRequestTimestamp = 0;
    private boolean showSlashButton = false;
    private boolean showKeyboardButton = false;
    private Client.ResultHandler inlineSearchHandler;
    private EmojiView.EmojiKeyboardCallback emojiKeyboardCallback = null;
    private ComposeViewDelegate delegate;

    private TdApi.User currentInlineBot = null;
    private volatile boolean fromLoadMore = false;
    private String inlineBotQuery = "";
    private volatile String nextOffset = "";
    public static Handler inlineQueryHandler = new Handler(Looper.getMainLooper());
    public final TdApi.GetInlineQueryResults inlineSearchRequest = new TdApi.GetInlineQueryResults();
    public Client.ResultHandler inlineQueryResultHandler;
    public ComposeMessageView(Context context) {
        super(context);
        init();
    }
    public ComposeMessageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public ComposeMessageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init()
    {
        View v = inflate(getContext(), R.layout.compose_message, this);
        messageEditText = (EditText) v.findViewById(R.id.messageEditText);
        inlineHelp = (TextView) v.findViewById(R.id.inlineBotPlaceholder);
        smilesButton = (ImageButton) v.findViewById(R.id.smiles_btn);
        smilesButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                emojiKeyboardCallback.toggleEmojiKeyboard();
            }
        });
        sendMessageButton = (ImageButton) v.findViewById(R.id.send_btn);
        attachButton = (ImageButton) v.findViewById(R.id.attach_btn);
        micButton = (ImageButton) v.findViewById(R.id.mic_btn);
        slashButton = (ImageButton) v.findViewById(R.id.bot_slash_btn);
        kbdButton = (ImageButton) v.findViewById(R.id.bot_kbd_btn);
        inlineQueryResultHandler = new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(object == null || object.getConstructor() != TdApi.InlineQueryResults.CONSTRUCTOR)
                    return;
                if(BuildConfig.DEBUG) {
                    Log.d("ComposeMessage", object.toString());
                }
                final TdApi.InlineQueryResults results = (TdApi.InlineQueryResults) object;
                nextOffset = results.nextOffset;
                final InlineQueryResultWrap[] wrappedResults = new InlineQueryResultWrap[results.results.length];
                for(int i = 0; i < results.results.length; i++)
                {
                    InlineQueryResultWrap wrap = new InlineQueryResultWrap(results.inlineQueryId, results.results[i]);
                    wrappedResults[i] = wrap;
                }
                TelegramApplication.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(delegate != null) {
                            List<InlineQueryResultWrap> asList = Arrays.asList(wrappedResults);
                            boolean vertical = !asList.isEmpty() && (results.results[0].getConstructor() == TdApi.InlineQueryResultArticle.CONSTRUCTOR || results.results[0].getConstructor() == TdApi.InlineQueryResultVideo.CONSTRUCTOR);
                            delegate.onNewInlineQueryResults(asList, vertical, fromLoadMore);
                            delegate.toggleInline(!asList.isEmpty());
                        }
                    }
                });
            }
        };
        inlineSearchHandler = new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(object == null || object.getConstructor() != TdApi.User.CONSTRUCTOR)
                    return;
                final TdApi.User u = (TdApi.User) object;
                if(u.type.getConstructor() == TdApi.UserTypeBot.CONSTRUCTOR && ((TdApi.UserTypeBot) u.type).isInline)
                {
                    post(new Runnable() {
                        @Override
                        public void run() {
                            currentInlineBot = u;
                            UserCache.getInstance().cacheUser(u.id);
                            enableInlineMode((TdApi.UserTypeBot) u.type);
                        }
                    });
                }
            }
        };
        messageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String txt = s.toString();

                if(!txt.startsWith("/") && delegate != null)
                    delegate.didHideCommandListRequested();
                if(!txt.startsWith("@") && delegate != null)
                    delegate.didHideRecentInlineListRequested();

                if(txt.startsWith("/") && delegate != null)
                    delegate.didBotCommandListRequested();
                else if(txt.startsWith("@") && txt.indexOf(' ') == -1 && delegate != null) {
                    if(BuildConfig.DEBUG)
                        Log.d("ComposeView", "show inline");
                    delegate.didShowRecentInlineListRequested();
                }
                if(currentInlineBot != null && txt.startsWith("@".concat(currentInlineBot.username).concat(" ")))
                {
                    try {
                        inlineBotQuery = txt.substring(currentInlineBot.username.length() + 2).trim();
                        inlineHelp.setVisibility(inlineBotQuery.isEmpty() ? VISIBLE : GONE);
                        if(currentInlineBot != null && (((TdApi.UserTypeBot)currentInlineBot.type).inlineQueryPlaceholder.isEmpty() || !inlineBotQuery.isEmpty()))
                        {
                            fromLoadMore = false;
                            inlineQueryHandler.removeCallbacksAndMessages(null);
                            inlineQueryHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        inlineSearchRequest.offset = nextOffset;
                                        inlineSearchRequest.botUserId = currentInlineBot.id;
                                        inlineSearchRequest.query = inlineBotQuery;
                                        TG.getClientInstance().send(inlineSearchRequest, inlineQueryResultHandler);
                                    }
                                    catch(Exception ignored) {}
                                }
                            }, 600);
                        }
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                    }
                }
                else if(txt.startsWith("@") && txt.endsWith(" ") && delegate != null)
                {
                    try {
                        String username = txt.trim().substring(1);
                        TdApi.SearchUser searchUser = new TdApi.SearchUser(username);
                        TG.getClientInstance().send(searchUser, inlineSearchHandler);
                    }
                    catch(Exception ignored) {}
                }
                else if(txt.isEmpty())
                    disableInlineMode();
                else
                    inlineHelp.setVisibility(View.GONE);
                checkSendButton();
                if(s.length() > 0)
                {
                    long currentTimestamp = System.currentTimeMillis();
                    if(currentTimestamp - lastTypingRequestTimestamp > 5000 && delegate != null)
                    {
                        lastTypingRequestTimestamp = currentTimestamp;
                        delegate.sendTypingRequested();
                    }
                }

            }
        });
        messageEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus && emojiKeyboardCallback.isEmojiKeyboardShown())
                    emojiKeyboardCallback.toggleEmojiKeyboard();
            }
        });
        sendMessageButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lockSendMessage == true)
                    return;
                if (delegate == null)
                    return;
                if (messageEditText.length() > 0 || (delegate != null && delegate.isAdditionalSendActionBound())) {
                    TdApi.InputMessageText content = new TdApi.InputMessageText(messageEditText.getText().toString(), true, null);
                    lockSendMessage = true;
                    delegate.didSendTextMsgRequested(content);
                }
            }
        });
        slashButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                messageEditText.setText("/");
                messageEditText.setSelection(1);
                AndroidUtilities.showKeyboard(messageEditText);
            }
        });
        kbdButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(delegate == null)
                    return;
                if(delegate.isBotKeyboardShown())
                {
                    kbdButton.setImageResource(R.drawable.ic_msg_panel_kb);
                    delegate.didHideBotKeyboardRequested();
                }
                else
                {
                    kbdButton.setImageResource(R.drawable.ic_command);
                    delegate.didShowBotKeyboardRequested();
                }
            }
        });
        attachButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(delegate != null)
                    delegate.didAttachButtonPressed();
            }
        });
    }
    public void clearTextMessageForm()
    {
        messageEditText.setText(null);
        lockSendMessage = false;
    }
    private void enableInlineMode(TdApi.UserTypeBot botInfo)
    {
        String txt = messageEditText.getText().toString();
        if(!(botInfo != null && txt.startsWith("@") && txt.endsWith(" ")))
        {
            if(BuildConfig.DEBUG)
                Log.d("ComposeMessage", "enableInlineMode: return");
            disableInlineMode();
            return;
        }
        Rect bounds = new Rect();
        Paint textPaint = messageEditText.getPaint();
        textPaint.getTextBounds(txt,0,txt.length(), bounds);
        inlineHelp.setVisibility(View.VISIBLE);
        inlineHelp.setText(" ".concat(botInfo.inlineQueryPlaceholder.isEmpty() ? getResources().getString(R.string.new_msg_placeholder) : botInfo.inlineQueryPlaceholder));
        inlineHelp.setPadding(AndroidUtilities.dp(56)+bounds.width(), inlineHelp.getPaddingTop(), inlineHelp.getPaddingRight(), inlineHelp.getPaddingBottom());
        if(delegate != null)
            delegate.toggleInline(false);
    }
    private void disableInlineMode()
    {
        currentInlineBot = null;
        inlineBotQuery = "";
        nextOffset = "";
        inlineHelp.setVisibility(View.GONE);
        if(delegate != null)
            delegate.toggleInline(false);
    }
    public EditText getMessageEditText() { return this.messageEditText; }
    public MicButton getMicButton() { return (MicButton) this.micButton; }
    public void setDelegate(ComposeViewDelegate callback)
    {
        this.delegate = callback;
        if(callback != null)
            checkSendButton();
    }
    public void setEmojiKeyboardCallback(EmojiView.EmojiKeyboardCallback callback) {
        this.emojiKeyboardCallback = callback;
    }
    public void changeSmileButtonIcon(int cmd)
    {
        if(cmd == IC_SMILE)
            smilesButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_smiles));
        else
            smilesButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_msg_panel_kb));
    }

    public void checkSendButton() {
        String message = messageEditText.getText().toString();
        if (message.length() > 0 || (delegate != null && delegate.isAdditionalSendActionBound())) {
            if (micButton.getVisibility() == View.VISIBLE || (delegate != null && delegate.isAdditionalSendActionBound())) {
                    if (runningAnimationType == 1) {
                        return;
                    }
                    if (runningAnimation != null) {
                        runningAnimation.cancel();
                        runningAnimation = null;
                    }
                    if (runningAnimation2 != null) {
                        runningAnimation2.cancel();
                        runningAnimation2 = null;
                    }

                    if (attachButton != null) {
                        runningAnimation2 = new AnimatorSet();
                        runningAnimation2.playTogether(
                                ObjectAnimator.ofFloat(attachButton, "alpha", 0.0f),
                                ObjectAnimator.ofFloat(attachButton, "scaleX", 0.0f)
                        );
                        runningAnimation2.setDuration(100);
                        runningAnimation2.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                if (runningAnimation2.equals(animation)) {
                                    attachButton.setVisibility(View.GONE);
                                    attachButton.clearAnimation();
                                }
                            }
                        });
                        runningAnimation2.start();

                        updateFieldRight(0);

                    }

                    sendMessageButton.setVisibility(View.VISIBLE);
                    runningAnimation = new AnimatorSet();
                    runningAnimationType = 1;
                    if(showSlashButton)
                        runningAnimation.playTogether(
                                ObjectAnimator.ofFloat(slashButton, "scaleX", 0.1f),
                                ObjectAnimator.ofFloat(slashButton, "scaleY", 0.1f),
                                ObjectAnimator.ofFloat(slashButton, "alpha", 0.0f),
                                ObjectAnimator.ofFloat(micButton, "scaleX", 0.1f),
                                ObjectAnimator.ofFloat(micButton, "scaleY", 0.1f),
                                ObjectAnimator.ofFloat(micButton, "alpha", 0.0f),
                                ObjectAnimator.ofFloat(sendMessageButton, "scaleX", 1.0f),
                                ObjectAnimator.ofFloat(sendMessageButton, "scaleY", 1.0f),
                                ObjectAnimator.ofFloat(sendMessageButton, "alpha", 1.0f)
                        );
                    else if(showKeyboardButton)
                        runningAnimation.playTogether(
                                ObjectAnimator.ofFloat(kbdButton, "scaleX", 0.1f),
                                ObjectAnimator.ofFloat(kbdButton, "scaleY", 0.1f),
                                ObjectAnimator.ofFloat(kbdButton, "alpha", 0.0f),
                                ObjectAnimator.ofFloat(micButton, "scaleX", 0.1f),
                                ObjectAnimator.ofFloat(micButton, "scaleY", 0.1f),
                                ObjectAnimator.ofFloat(micButton, "alpha", 0.0f),
                                ObjectAnimator.ofFloat(sendMessageButton, "scaleX", 1.0f),
                                ObjectAnimator.ofFloat(sendMessageButton, "scaleY", 1.0f),
                                ObjectAnimator.ofFloat(sendMessageButton, "alpha", 1.0f)
                        );
                    else
                        runningAnimation.playTogether(
                                ObjectAnimator.ofFloat(micButton, "scaleX", 0.1f),
                                ObjectAnimator.ofFloat(micButton, "scaleY", 0.1f),
                                ObjectAnimator.ofFloat(micButton, "alpha", 0.0f),
                                ObjectAnimator.ofFloat(sendMessageButton, "scaleX", 1.0f),
                                ObjectAnimator.ofFloat(sendMessageButton, "scaleY", 1.0f),
                                ObjectAnimator.ofFloat(sendMessageButton, "alpha", 1.0f)
                        );

                    runningAnimation.setDuration(150);
                    runningAnimation.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            if (runningAnimation.equals(animation)) {
                                sendMessageButton.setVisibility(View.VISIBLE);
                                micButton.setVisibility(View.GONE);
                                if(showSlashButton)
                                {
                                    slashButton.setVisibility(View.GONE);
                                    slashButton.clearAnimation();
                                }
                                else if(showKeyboardButton)
                                {
                                    kbdButton.setVisibility(View.GONE);
                                    kbdButton.clearAnimation();
                                }
                                micButton.clearAnimation();
                                runningAnimation = null;
                                runningAnimationType = 0;
                            }
                        }
                    });
                    runningAnimation.start();
            }
        } else if (sendMessageButton.getVisibility() == View.VISIBLE) {
            if (runningAnimationType == 2) {
                return;
            }

            if (runningAnimation != null) {
                runningAnimation.cancel();
                runningAnimation = null;
            }
            if (runningAnimation2 != null) {
                runningAnimation2.cancel();
                runningAnimation2 = null;
            }

            if (attachButton != null) {
                attachButton.setVisibility(View.VISIBLE);
                runningAnimation2 = new AnimatorSet();
                runningAnimation2.playTogether(
                        ObjectAnimator.ofFloat(attachButton, "alpha", 1.0f),
                        ObjectAnimator.ofFloat(attachButton, "scaleX", 1.0f)
                );
                runningAnimation2.setDuration(100);
                runningAnimation2.start();

                updateFieldRight(1);
            }

            micButton.setVisibility(View.VISIBLE);
            runningAnimation = new AnimatorSet();
            runningAnimationType = 2;
            if(showSlashButton)
                runningAnimation.playTogether(
                        ObjectAnimator.ofFloat(sendMessageButton, "scaleX", 0.1f),
                        ObjectAnimator.ofFloat(sendMessageButton, "scaleY", 0.1f),
                        ObjectAnimator.ofFloat(sendMessageButton, "alpha", 0.0f),
                        ObjectAnimator.ofFloat(micButton, "scaleX", 1.0f),
                        ObjectAnimator.ofFloat(micButton, "scaleY", 1.0f),
                        ObjectAnimator.ofFloat(micButton, "alpha", 1.0f),
                        ObjectAnimator.ofFloat(slashButton, "scaleX", 1.0f),
                        ObjectAnimator.ofFloat(slashButton, "scaleY", 1.0f),
                        ObjectAnimator.ofFloat(slashButton, "alpha", 1.0f)
                );
            else if(showKeyboardButton)
                runningAnimation.playTogether(
                        ObjectAnimator.ofFloat(sendMessageButton, "scaleX", 0.1f),
                        ObjectAnimator.ofFloat(sendMessageButton, "scaleY", 0.1f),
                        ObjectAnimator.ofFloat(sendMessageButton, "alpha", 0.0f),
                        ObjectAnimator.ofFloat(micButton, "scaleX", 1.0f),
                        ObjectAnimator.ofFloat(micButton, "scaleY", 1.0f),
                        ObjectAnimator.ofFloat(micButton, "alpha", 1.0f),
                        ObjectAnimator.ofFloat(kbdButton, "scaleX", 1.0f),
                        ObjectAnimator.ofFloat(kbdButton, "scaleY", 1.0f),
                        ObjectAnimator.ofFloat(kbdButton, "alpha", 1.0f)
                );
            else
                runningAnimation.playTogether(
                        ObjectAnimator.ofFloat(sendMessageButton, "scaleX", 0.1f),
                        ObjectAnimator.ofFloat(sendMessageButton, "scaleY", 0.1f),
                        ObjectAnimator.ofFloat(sendMessageButton, "alpha", 0.0f),
                        ObjectAnimator.ofFloat(micButton, "scaleX", 1.0f),
                        ObjectAnimator.ofFloat(micButton, "scaleY", 1.0f),
                        ObjectAnimator.ofFloat(micButton, "alpha", 1.0f)
                );

            runningAnimation.setDuration(150);
            runningAnimation.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    if (runningAnimation.equals(animation)) {
                        sendMessageButton.setVisibility(View.GONE);
                        sendMessageButton.clearAnimation();
                        if(showSlashButton)
                            slashButton.setVisibility(View.VISIBLE);
                        else if(showKeyboardButton)
                            kbdButton.setVisibility(View.VISIBLE);
                        micButton.setVisibility(View.VISIBLE);
                        runningAnimation = null;
                        runningAnimationType = 0;
                    }
                }
            });
            runningAnimation.start();
        }
    }
    private void updateFieldRight(int attachVisible) {
        if (messageEditText == null) {
            return;
        }
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) messageEditText.getLayoutParams();
        if (attachVisible == 1) {
            if (slashButton != null && slashButton.getVisibility() == VISIBLE) {
                layoutParams.rightMargin = AndroidUtilities.dp(98);
            } else {
                layoutParams.rightMargin = AndroidUtilities.dp(50);
            }
        } else if (attachVisible == 2) {
            if (layoutParams.rightMargin != AndroidUtilities.dp(2)) {
                if (slashButton != null && slashButton.getVisibility() == VISIBLE) {
                    layoutParams.rightMargin = AndroidUtilities.dp(98);
                } else {
                    layoutParams.rightMargin = AndroidUtilities.dp(50);
                }
            }
        } else {
            layoutParams.rightMargin = AndroidUtilities.dp(2);
        }
        messageEditText.setLayoutParams(layoutParams);
    }

    public void setSlashButtonVisibility(boolean visible)
    {
        this.showSlashButton = visible;
        if(showSlashButton) {
            slashButton.setVisibility(View.VISIBLE);
            kbdButton.setVisibility(View.GONE);
        }
        else
            slashButton.setVisibility(View.GONE);
    }
    public void setKeyboardButtonVisibility(boolean visible, boolean drawKeyboardIcon)
    {
        this.showKeyboardButton = visible;
        if(showKeyboardButton) {
            kbdButton.setVisibility(View.VISIBLE);
            setSlashButtonVisibility(false);
        }
        else
            kbdButton.setVisibility(View.GONE);
        if(drawKeyboardIcon)
            kbdButton.setImageResource(R.drawable.ic_msg_panel_kb);
        else
            kbdButton.setImageResource(R.drawable.ic_command);
    }

    public void restoreTextMessage(long chatId)
    {
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.COMPOSEVIEW_PREFS, Activity.MODE_PRIVATE);
        String key = String.format("msg%d", chatId);
        String msg = preferences.getString(key, null);
        if(msg == null)
            return;
        messageEditText.setText(msg);
    }
    public void saveTextMessage(long chatId)
    {
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.COMPOSEVIEW_PREFS, Activity.MODE_PRIVATE);
        SharedPreferences.Editor prefEdit = preferences.edit();
        String key = String.format("msg%d", chatId);
        String msg = messageEditText.getText().toString();
        if(msg == null || msg.length() == 0 || msg.startsWith("/"))
            prefEdit.remove(key);
        else
            prefEdit.putString(key, msg);
        prefEdit.apply();
    }
    public void loadMoreInlineResults()
    {
        if(currentInlineBot == null || nextOffset.isEmpty())
            return;
        fromLoadMore = true;
        inlineQueryHandler.removeCallbacksAndMessages(null);
        inlineQueryHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    inlineSearchRequest.offset = nextOffset;
                    inlineSearchRequest.botUserId = currentInlineBot.id;
                    inlineSearchRequest.query = inlineBotQuery;
                    TG.getClientInstance().send(inlineSearchRequest, inlineQueryResultHandler);
                } catch (Exception ignored) {
                }
            }
        }, 600);
    }
    public void setInlineBot(InlineBotShortModel user)
    {
        messageEditText.setText("");
        currentInlineBot = user.getUserObj();
        messageEditText.setText(user.getBotUsername().concat(" "));
        messageEditText.setSelection(user.getBotUsername().length() + 1);
    }

    public interface ComposeViewDelegate
    {
        public void didSendTextMsgRequested(TdApi.InputMessageText text);
        public void didAttachButtonPressed();
        public void didBotCommandListRequested();
        public void didHideCommandListRequested();
        public void didShowRecentInlineListRequested();
        public void didHideRecentInlineListRequested();
        public void didShowBotKeyboardRequested();
        public void didHideBotKeyboardRequested();
        public boolean isBotKeyboardShown();
        public void sendTypingRequested();
        public boolean isAdditionalSendActionBound();
        public void onNewInlineQueryResults(List<InlineQueryResultWrap> results, boolean showVertical, boolean fromLoadMore);
        public void toggleInline(boolean show);
        public void inlineBotSelected(InlineBotShortModel cmd);
    }
}
