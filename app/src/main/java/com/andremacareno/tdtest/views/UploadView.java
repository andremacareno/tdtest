package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.andremacareno.tdtest.models.FileModel;

/**
 * Created by andremacareno on 17/08/15.
 */
public abstract class UploadView extends FrameLayout {
    protected FileModel attachedFile;
    private int attachedMsgId;
    public UploadView(Context context) {
        super(context);
        init();
    }

    public UploadView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public void attachDelegateToFile(FileModel file)
    {
        if(attachedFile != null)
            attachedFile.removeDelegate(getDelegate());
        if(file == null)
            return;
        this.attachedFile = file;
        attachedFile.addDelegate(getDelegate());
        fillLayout();
    }
    public void setAttachedMsgId(int msgId)
    {
        this.attachedMsgId = msgId;
    }
    public int getAttachedMsgId() { return this.attachedMsgId; }
    protected String generateDelegateKey()
    {
        return String.format("file%d_%d_upload_delegate", getAttachedMsgId(), attachedFile == null ? 0 : attachedFile.getFileId());
    }
    protected abstract void didUploadStarted();
    protected abstract void didUploadCancelled();
    protected abstract void fillLayout();
    protected abstract FileModel.FileModelDelegate getDelegate();
    protected abstract void init();
}
