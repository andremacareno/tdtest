package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;

import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by Andrew on 25.04.2015.
 */
public class ProfileAvatarImageView extends AvatarImageView {
    public ProfileAvatarImageView(Context context) {
        super(context);
    }

    public ProfileAvatarImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    @Override
    public void onMeasure(int measureWidthSpec, int heightMeasureSpec)
    {
        if(!isInEditMode())
            setMeasuredDimension(AndroidUtilities.dp(60), AndroidUtilities.dp(60));
        else
            setMeasuredDimension(180, 180);
    }
}
