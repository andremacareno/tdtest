package com.andremacareno.tdtest.views;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.AdditionalActionForward;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.FwdChatsController;
import com.andremacareno.tdtest.viewcontrollers.ViewController;

import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 16/08/15.
 */
public class ForwardBottomSheetView extends LinearLayout implements BottomSheet {

    public interface ForwardMenuDelegate
    {
        public void onBottomSheetClose(boolean dismissed);
        public AdditionalActionForward getForwardInfo();
        public long getCurrentChatId();
        public void performSelfForward();
        public void performSelfComment(TdApi.InputMessageText comment);
        public void closeBottomSheet();
    }
    private float yFraction;
    private ViewTreeObserver.OnPreDrawListener preDrawListener;
    public View clearSearch;
    public EditText searchEditText;
    public ForwardBottomSheetGridView chatGrid;
    public AdditionalSendView fwdInfo;
    public EditText msgEditText;
    public View sendMsg;
    private ForwardMenuDelegate delegate;
    private Runnable commentWindowModeRunnable;
    private Runnable searchWindowModeRunnable;
    private Runnable onCloseActivityRunnable;

    public ForwardBottomSheetView(Context context) {
        super(context);
        init();
    }

    public ForwardBottomSheetView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        setOrientation(VERTICAL);
        //setGravity(Gravity.BOTTOM);
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
        View v = inflate(getContext(), R.layout.forward_bottom_sheet, this);
        clearSearch = v.findViewById(R.id.fwd_clear_search_btn);
        searchEditText = (EditText) v.findViewById(R.id.fwd_search_edittext);
        chatGrid = (ForwardBottomSheetGridView) v.findViewById(R.id.fwdChats);
        fwdInfo = (AdditionalSendView) v.findViewById(R.id.fwd_info);
        msgEditText = (EditText) v.findViewById(R.id.fwdMessageEditText);
        sendMsg = v.findViewById(R.id.fwd_btn);

        msgEditText.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP && commentWindowModeRunnable != null)
                    commentWindowModeRunnable.run();
                return false;
            }
        });
        searchEditText.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP && searchWindowModeRunnable != null)
                    searchWindowModeRunnable.run();
                return false;
            }
        });
        clearSearch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                searchEditText.setText("");
            }
        });
        if(getHeight() != 0)
            adjustGridHeight();
        else
        {
            getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    if (Build.VERSION.SDK_INT >= 16) {
                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                    if(getHeight() != 0)
                        adjustGridHeight();
                    postInvalidate();
                }
            });
        }
        sendMsg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewController controller = chatGrid.getController();
                if(controller == null || !(controller instanceof FwdChatsController) || delegate == null)
                    return;
                final AdditionalActionForward fwdInfoObj = delegate.getForwardInfo();
                FwdChatsController cast = (FwdChatsController) controller;
                boolean selfForward = cast.forwardToSelf();
                final long[] selection = cast.getSelection();
                if(selfForward)
                {
                    String comment = msgEditText.getText().toString().trim();
                    if(!comment.isEmpty())
                        delegate.performSelfComment(new TdApi.InputMessageText(comment, true, null));
                    delegate.performSelfForward();
                }
                if(selection != null && selection.length > 0)
                {
                    final String comment = msgEditText.getText().toString().trim();
                    final TdApi.InputMessageText commentObj = comment.isEmpty() ? null : new TdApi.InputMessageText(comment, true, null);
                    final long sourceChat = fwdInfoObj.getSourceChatId();
                    final int[] msgs = fwdInfoObj.getMessageIds();
                    Runnable forwardRunnable = new Runnable() {
                        @Override
                        public void run() {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(500);  //let other threads hiding bottom sheet, performing self forwarding etc
                                        for(long chat : selection)
                                        {
                                            TdApi.Chat chatObj = ChatCache.getInstance().getChatById(chat);
                                            if(chatObj == null)
                                                continue;
                                            boolean fromChannel = chatObj.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR && !((TdApi.ChannelChatInfo) chatObj.type).channel.isSupergroup;
                                            if(commentObj != null)
                                            {
                                                TdApi.SendMessage commentReq = new TdApi.SendMessage(chat, 0, fromChannel, false, false, null, commentObj);
                                                TG.getClientInstance().send(commentReq, TelegramApplication.dummyResultHandler);
                                            }
                                            TdApi.ForwardMessages fwdReq = new TdApi.ForwardMessages(chat, sourceChat, msgs, fromChannel, false, false);
                                            TG.getClientInstance().send(fwdReq, TelegramApplication.dummyResultHandler);
                                        }
                                    }
                                    catch(Exception e) { e.printStackTrace(); }
                                }
                            }).start();
                        }
                    };
                    TelegramApplication.applicationHandler.post(forwardRunnable);
                }
                delegate.closeBottomSheet();
            }
        });
    }
    public void helpAdjustScreen(Runnable commentWindowModeRunnable, Runnable searchWindowModeRunnable, Runnable onCloseActivityRunnable)
    {
        this.commentWindowModeRunnable = commentWindowModeRunnable;
        this.searchWindowModeRunnable = searchWindowModeRunnable;
        this.onCloseActivityRunnable = onCloseActivityRunnable;
    }
    public void setYFraction(float fraction) {

        this.yFraction = fraction;
        if (getHeight() == 0) {
            if (preDrawListener == null) {
                preDrawListener = new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        getViewTreeObserver().removeOnPreDrawListener(preDrawListener);
                        setYFraction(yFraction);
                        return true;
                    }
                };
                getViewTreeObserver().addOnPreDrawListener(preDrawListener);
            }
            return;
        }
        float translationY = getHeight() * fraction;
        setTranslationY(translationY);
    }
    public float getYFraction() {
        return this.yFraction;
    }
    public void setDelegate(ForwardMenuDelegate delegateRef)
    {
        this.delegate = delegateRef;
        AdditionalActionForward fwdInfoObj = delegate.getForwardInfo();
        fwdInfo.attachAction(fwdInfoObj);
        ViewController controller = chatGrid.getController();
        if(controller != null && controller instanceof FwdChatsController)
        {
            FwdChatsController cast = (FwdChatsController) controller;
            cast.setCurrentChat(delegate.getCurrentChatId());
        }
    }
    @Override
    public void onClose(boolean dismissed) {
        try {
            if(delegate != null)
                delegate.onBottomSheetClose(dismissed);
            if(onCloseActivityRunnable != null)
                onCloseActivityRunnable.run();
            searchEditText.setText("");
            msgEditText.setText("");
            ((InputMethodManager) searchEditText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            ViewController controller = chatGrid.getController();
            if(controller != null && controller instanceof FwdChatsController)
            {
                FwdChatsController cast = (FwdChatsController) controller;
                cast.reset();
            }
        }
        catch(Exception ignored) {}
    }

    private void adjustGridHeight()
    {
        if(chatGrid != null)
        {
            // display size in pixels
            int height = getResources().getDisplayMetrics().heightPixels - AndroidUtilities.getStatusBarHeight();
            ViewGroup.LayoutParams lp = chatGrid.getLayoutParams();
            lp.height = lp.height == 0 ? height / 2 : Math.min(height / 2, AndroidUtilities.dp(256));
            chatGrid.setLayoutParams(lp);
        }
    }
}
