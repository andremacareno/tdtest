package com.andremacareno.tdtest.views;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by andremacareno on 25/08/15.
 */
public class VoiceRecordingBackground extends FrameLayout {
    private final Handler timerHandler = new Handler();
    private TextView timer;
    private TextView slideToCancel;
    private long recStarted;
    private Animation.AnimationListener animEndListener;
    private final Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            updateTimer();
        }
    };
    public VoiceRecordingBackground(Context context) {
        super(context);
        init();
    }

    public VoiceRecordingBackground(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.voice_recording_bg, this);
        timer = (TextView) v.findViewById(R.id.recording_timer);
        timer.setText("0:00");
        slideToCancel = (TextView) v.findViewById(R.id.slide_to_cancel);
        animEndListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                slideToCancel.setAlpha(1.0f);
                slideToCancel.setTranslationX(0);
                setVisibility(View.GONE);
                timerHandler.removeCallbacks(timerRunnable);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        };
    }

    public void voiceRecordingEnabled()
    {
        Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.bottom_up);
        anim.setDuration(300);
        timer.setText("0:00");
        recStarted = System.currentTimeMillis();
        setVisibility(View.VISIBLE);
        startAnimation(anim);
        timerHandler.postDelayed(timerRunnable, 1000);
    }

    public void voiceRecordingDisabled()
    {
        Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.bottom_down);
        anim.setAnimationListener(animEndListener);
        anim.setDuration(300);
        startAnimation(anim);
    }

    private void updateTimer()
    {
        long now = System.currentTimeMillis();
        int seconds = (int) ((now - recStarted) / 1000);
        String duration = String.format("%d:%02d", seconds / 60, seconds % 60);
        timer.setText(duration);
        timerHandler.postDelayed(timerRunnable, 1000);
    }
    public void moveSlideToCancel(float micTranslation, float threshold)
    {
        float progress = 0.8f * Math.abs(micTranslation / threshold);
        slideToCancel.setTranslationX(micTranslation * 0.4f);
        slideToCancel.setAlpha(1.0f - progress);
    }
}
