package com.andremacareno.tdtest.views.actionviews;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.views.ProfileAvatarImageView;

/**
 * Created by andremacareno on 12/05/15.
 */
public class NewGroupFinalStageActionView extends ActionView {
    private static final String TAG = "EnterTitleActionView";
    public ProfileAvatarImageView profilePhoto;
    public EditText titleEditText;
    public ImageButton backButton;

    public NewGroupFinalStageActionView(Context context) {
        super(context);
    }


    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void initView() {
        View v = inflate(getContext(), R.layout.action_view_new_group_title, this);
        profilePhoto = (ProfileAvatarImageView) v.findViewById(R.id.group_avatar);
        titleEditText = (EditText) v.findViewById(R.id.group_name);
        backButton = (ImageButton) v.findViewById(R.id.chat_back_btn);
    }
    public void setBackButtonClickListener(OnClickListener listener)
    {
        backButton.setOnClickListener(listener);
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), AndroidUtilities.dp(148));
    }

}
