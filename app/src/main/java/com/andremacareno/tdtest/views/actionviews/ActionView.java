package com.andremacareno.tdtest.views.actionviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;

/**
 * Created by Andrew on 12.05.2015.
 */
public abstract class ActionView extends FrameLayout {
    private ActionViewController viewController;
    public ActionView(Context context) {
        super(context);
        initView();
    }

    public ActionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public ActionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }
    public ActionViewController getController() {
        return this.viewController;
    }
    public abstract Object getTag();
    protected abstract void initView();
    public void setController(ActionViewController controller)
    {
        if(controller == null)
            return;
        this.viewController = controller;
        controller.setActionView(this);
    }
    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
