package com.andremacareno.tdtest.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by Andrew on 23.04.2015.
 */
public class ComposeMessagePlaceholder extends View {
    public enum Reason { NO_REASON, DELETED_ACCOUNT, KICKED_FROM_CHAT, BOT_NOT_INITIALISED, CHANNEL_NOT_ADMIN, CHANNEL_NOT_FOLLOWING }
    private Reason placeholderReason = null;
    private String start_txt, kicked_txt, deleted_acc_txt, follow_txt;
    private Paint paint, followPaint;
    private float textOriginX = 0;
    private float textOriginY = 0;
    private Drawable plusDrawable;

    public ComposeMessagePlaceholder(Context context) {
        super(context);
        init();
    }

    public ComposeMessagePlaceholder(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init()
    {
        plusDrawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_add_white);
        follow_txt = getResources().getString(R.string.follow);
        start_txt = getResources().getString(R.string.start);
        kicked_txt = getResources().getString(R.string.delete_this_group);
        deleted_acc_txt = getResources().getString(R.string.delete_this_chat);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(ContextCompat.getColor(getContext(), R.color.compose_msg_placeholder_text));
        paint.setTextSize(AndroidUtilities.dp(14));
        followPaint = new Paint(paint);
        followPaint.setTextSize(AndroidUtilities.dp(18));
        followPaint.setTypeface(Typeface.DEFAULT_BOLD);
        followPaint.setColor(ContextCompat.getColor(getContext(), R.color.white));

        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if(getWidth() == 0 || getHeight() == 0)
                    return;
                if (Build.VERSION.SDK_INT >= 16) {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                textOriginX = getWidth() / 2.0f;
                textOriginY = getHeight() / 2.0f + AndroidUtilities.dp(3);
                postInvalidate();
            }
        });
    }
    public void explainReason(Reason reason)
    {
        this.placeholderReason = reason;
        if(reason == Reason.CHANNEL_NOT_FOLLOWING)
            setBackgroundResource(R.drawable.follow_button_background);
        else
            setBackgroundResource(R.drawable.list_selector);
        postInvalidate();
    }
    public Reason getReason() { return this.placeholderReason; }
    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        if(placeholderReason != null && textOriginX != 0 && textOriginY != 0) {
            String textToDraw = placeholderReason == Reason.DELETED_ACCOUNT ? deleted_acc_txt : placeholderReason == Reason.KICKED_FROM_CHAT ? kicked_txt : placeholderReason == Reason.CHANNEL_NOT_FOLLOWING ? follow_txt : start_txt;
            if(placeholderReason == Reason.CHANNEL_NOT_FOLLOWING)
            {
                plusDrawable.setBounds((int) textOriginX - plusDrawable.getIntrinsicWidth() - AndroidUtilities.dp(12), (int)textOriginY-plusDrawable.getIntrinsicHeight() + AndroidUtilities.dp(8), (int)textOriginX - AndroidUtilities.dp(12), (int)textOriginY + AndroidUtilities.dp(8));
                plusDrawable.draw(canvas);
                canvas.drawText(textToDraw, textOriginX + plusDrawable.getIntrinsicWidth()/2 + AndroidUtilities.dp(16), textOriginY + AndroidUtilities.dp(3), followPaint);
            }
            else
                canvas.drawText(textToDraw, textOriginX, textOriginY, paint);
        }
    }
}
