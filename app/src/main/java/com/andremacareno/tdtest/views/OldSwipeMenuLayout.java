package com.andremacareno.tdtest.views;
/*
	Original: https://github.com/TUBB/SwipeMenuRecyclerView/blob/master/library/src/main/java/com/tubb/smrv/SwipeMenuLayout.java
	Modified by Andre Macareno to support bidirectional swipe
 */

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ScrollerCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.R;

public class OldSwipeMenuLayout extends FrameLayout {

	private static final int STATE_CLOSE = 0;
	private static final int STATE_OPEN = 1;
	private int mSwipeDirection;
	private View mContentView;
	private View mMenuView;
	private int mDownX;
	private boolean opening = false;
	private int state = STATE_CLOSE;
	private GestureDetectorCompat mGestureDetector;
	private GestureDetector.OnGestureListener mGestureListener;
	private boolean isFling;
	private ScrollerCompat mOpenScroller;
	private ScrollerCompat mCloseScroller;
	private int mBaseX;
	private Interpolator mCloseInterpolator;
	private Interpolator mOpenInterpolator;
	private ViewConfiguration mViewConfiguration;
	private boolean swipeEnable = true;
	private int animDuration = 300;

	public OldSwipeMenuLayout(Context context) {
		super(context);
		init();
	}
	private void init()
	{
		mOpenInterpolator = new DecelerateInterpolator(1.5f);
		mCloseInterpolator = new AccelerateDecelerateInterpolator(getContext(), null);
		mCloseScroller = ScrollerCompat.create(getContext(), mCloseInterpolator);
		mOpenScroller = ScrollerCompat.create(getContext(), mOpenInterpolator);
		mGestureListener = new GestureDetector.SimpleOnGestureListener() {
			@Override
			public boolean onDown(MotionEvent e) {
				isFling = false;
				return true;
			}
			@Override
			public void onLongPress(MotionEvent e)
			{
				isFling = false;
			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2,
								   float velocityX, float velocityY) {
				if(mViewConfiguration == null)
					return false;
				if (velocityX > mViewConfiguration.getScaledMinimumFlingVelocity() || velocityY > mViewConfiguration.getScaledMinimumFlingVelocity())
					isFling = true;
				return isFling;
			}
		};
		mGestureDetector = new GestureDetectorCompat(getContext(),
				mGestureListener);
	}
	public void didViewComplete() {
		setClickable(true);
		mContentView = findViewById(R.id.smContentView);
		if (mContentView == null) {
			return;
		}
		mMenuView = findViewById(R.id.smRightMenuView);
		if (mMenuView == null) {
			return;
		}
		mViewConfiguration = ViewConfiguration.get(getContext());
	}

	public void setSwipeDirection(int swipeDirection) {
		mSwipeDirection = swipeDirection;
	}

	/*public void setCloseInterpolator(Interpolator closeInterpolator) {
		mCloseInterpolator = closeInterpolator;
		if (mCloseInterpolator != null) {
			mCloseScroller = ScrollerCompat.create(getContext(),
					mCloseInterpolator);
		}
	}

	public void setOpenInterpolator(Interpolator openInterpolator) {
		mOpenInterpolator = openInterpolator;
		if (mOpenInterpolator != null) {
			mOpenScroller = ScrollerCompat.create(getContext(),
					mOpenInterpolator);
		}
	}*/

	public boolean onSwipe(MotionEvent event) {
		mGestureDetector.onTouchEvent(event);
		if(mMenuView == null || mContentView == null)
			return false;
		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				mDownX = (int) event.getRawX();
				isFling = false;
				return false;
			case MotionEvent.ACTION_MOVE:
				int dis = (int) (mDownX - event.getRawX());
				if(dis >= 10 || dis <= mMenuView.getWidth() - 10) {
					if(BuildConfig.DEBUG)
						Log.d("SwipeMenuLayout", "opening");
					opening = true;
				}
				if (state == STATE_OPEN) {
					dis += mMenuView.getWidth() * mSwipeDirection;
				}
				swipe(dis);
				break;
			case MotionEvent.ACTION_UP:
				opening = false;
				if ((isFling || Math.abs(mDownX - event.getRawX()) > (mMenuView.getWidth() / 3)) &&
						Math.signum(mDownX - event.getRawX()) == mSwipeDirection) {
					smoothOpenMenu();
				} else {
					smoothCloseMenu();
					return false;
				}
				break;
		}
		return true;
	}

	public boolean isOpen() {
		return state == STATE_OPEN;
	}

	private void swipe(int dis) {
		if(mMenuView == null || mContentView == null)
			return;
		if (Math.signum(dis) != mSwipeDirection) {
			dis = 0;
		} else if (Math.abs(dis) > mMenuView.getWidth()) {
			dis = mMenuView.getWidth() * mSwipeDirection;
			state = STATE_OPEN;
		}

		LayoutParams lp = (LayoutParams) mContentView.getLayoutParams();
		int lGap = getPaddingLeft() + lp.leftMargin;
		mContentView.layout(lGap - dis,
				mContentView.getTop(),
				lGap + ViewCompat.getMeasuredWidthAndState(mContentView) - dis,
				mContentView.getBottom());

		if (mSwipeDirection == SwipeMenuRecyclerView.DIRECTION_LEFT) {
			mMenuView.layout(getMeasuredWidth() - dis, mMenuView.getTop(),
					getMeasuredWidth() + ViewCompat.getMeasuredWidthAndState(mMenuView) - dis,
					mMenuView.getBottom());
		} else {
			mMenuView.layout(-ViewCompat.getMeasuredWidthAndState(mMenuView) - dis, mMenuView.getTop(),
					-dis, mMenuView.getBottom());
		}
	}
	public boolean isOpening() { return opening; }
	@Override
	public void computeScroll() {
		if (state == STATE_OPEN) {
			if (mOpenScroller.computeScrollOffset()) {
				swipe(mOpenScroller.getCurrX() * mSwipeDirection);
				postInvalidate();
			}
		} else {
			if (mCloseScroller.computeScrollOffset()) {
				swipe((mBaseX - mCloseScroller.getCurrX()) * mSwipeDirection);
				postInvalidate();
			}
		}
	}

	public void smoothCloseMenu() {
		closeOpenedMenu();
	}

	public void closeOpenedMenu() {
		state = STATE_CLOSE;
		if (mSwipeDirection == SwipeMenuRecyclerView.DIRECTION_LEFT) {
			mBaseX = -mContentView.getLeft();
			mCloseScroller.startScroll(0, 0, mMenuView.getWidth(), 0, animDuration);
		} else {
			mBaseX = mMenuView.getRight();
			mCloseScroller.startScroll(0, 0, mMenuView.getWidth(), 0, animDuration);
		}
		postInvalidate();
	}

	public void smoothOpenMenu() {
		state = STATE_OPEN;
		if (mSwipeDirection == SwipeMenuRecyclerView.DIRECTION_LEFT) {
			mOpenScroller.startScroll(-mContentView.getLeft(), 0, mMenuView.getWidth(), 0, animDuration);
		} else {
			mOpenScroller.startScroll(mContentView.getLeft(), 0, mMenuView.getWidth(), 0, animDuration);
		}
		postInvalidate();
	}

	public void closeMenu() {
		if (mCloseScroller.computeScrollOffset()) {
			mCloseScroller.abortAnimation();
		}
		if (state == STATE_OPEN) {
			state = STATE_CLOSE;
			swipe(0);
		}
	}

	public void openMenu() {
		if (state == STATE_CLOSE) {
			state = STATE_OPEN;
			swipe(mMenuView.getWidth() * mSwipeDirection);
		}
	}

	public View getMenuView() {
		return mMenuView;
	}

	public View getContentView() {
		return mContentView;
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		if(mContentView == null)
			return;
		LayoutParams lp = (LayoutParams) mContentView.getLayoutParams();
		int lGap = getPaddingLeft() + lp.leftMargin;
		int tGap = getPaddingTop() + lp.topMargin;
		mContentView.layout(lGap,
				tGap,
				lGap + ViewCompat.getMeasuredWidthAndState(mContentView),
				tGap + ViewCompat.getMeasuredHeightAndState(mContentView));
		lp = (LayoutParams) mMenuView.getLayoutParams();
		tGap = getPaddingTop() + lp.topMargin;
		if (mSwipeDirection == SwipeMenuRecyclerView.DIRECTION_LEFT) {
			mMenuView.layout(getMeasuredWidth(), tGap,
					getMeasuredWidth() + ViewCompat.getMeasuredWidthAndState(mMenuView),
					tGap + ViewCompat.getMeasuredHeightAndState(mMenuView));
		} else {
			mMenuView.layout(-ViewCompat.getMeasuredWidthAndState(mMenuView), tGap,
					0, tGap + ViewCompat.getMeasuredHeightAndState(mMenuView));
		}
	}

	public void setSwipeEnable(boolean swipeEnable) {
		this.swipeEnable = swipeEnable;
	}

	public boolean isSwipeEnable() {
		return swipeEnable;
	}

}