package com.andremacareno.tdtest.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 22/06/15.
 */
public class VideoDownloadButton extends DownloadButton {
    private int DOWNLOAD_BTN_SIZE;
    private Drawable downloadDrawable;
    private Drawable pauseDownloadDrawable;
    private Drawable playDrawable;

    private Paint blackCirclePaint;

    public VideoDownloadButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    @Override
    protected void init()
    {
        DOWNLOAD_BTN_SIZE = getResources().getDimensionPixelSize(R.dimen.download_btn_size);
        downloadDrawable = getResources().getDrawable(R.drawable.ic_download);
        pauseDownloadDrawable = getResources().getDrawable(R.drawable.ic_pause);
        playDrawable = getResources().getDrawable(R.drawable.ic_play);
        if(isInEditMode())
            downloadDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-18, DOWNLOAD_BTN_SIZE/2-27, DOWNLOAD_BTN_SIZE/2+18, DOWNLOAD_BTN_SIZE/2+27);
        else
        {
            final int eighteenDp = AndroidUtilities.dp(18)/2;
            final int twelveDp = AndroidUtilities.dp(12)/2;
            final int fourteenDp = AndroidUtilities.dp(14)/2;
            final int thirteenDp = AndroidUtilities.dp(13)/2;
            final int fifteenDp = AndroidUtilities.dp(15)/2;
            downloadDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-twelveDp, DOWNLOAD_BTN_SIZE/2-eighteenDp, DOWNLOAD_BTN_SIZE/2+twelveDp, DOWNLOAD_BTN_SIZE/2+eighteenDp);
            pauseDownloadDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-twelveDp, DOWNLOAD_BTN_SIZE/2-fourteenDp, DOWNLOAD_BTN_SIZE/2+twelveDp, DOWNLOAD_BTN_SIZE/2+fourteenDp);
            playDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-thirteenDp, DOWNLOAD_BTN_SIZE/2-fifteenDp, DOWNLOAD_BTN_SIZE/2+thirteenDp, DOWNLOAD_BTN_SIZE/2+fifteenDp);
        }
        blackCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        blackCirclePaint.setColor(ContextCompat.getColor(getContext(), R.color.img_download_btn_bg));
    }
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(DOWNLOAD_BTN_SIZE, DOWNLOAD_BTN_SIZE);
    }
    @Override
    public void onDraw(Canvas canvas)
    {
        canvas.drawCircle(DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, blackCirclePaint);
        if(attachedFile.getDownloadState() == FileModel.DownloadState.LOADED)
            playDrawable.draw(canvas);
        else if(attachedFile.getDownloadState() == FileModel.DownloadState.EMPTY)
            downloadDrawable.draw(canvas);
        else if(attachedFile.getDownloadState() == FileModel.DownloadState.LOADING)
            pauseDownloadDrawable.draw(canvas);
    }
}
