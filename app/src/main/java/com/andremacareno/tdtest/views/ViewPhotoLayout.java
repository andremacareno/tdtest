package com.andremacareno.tdtest.views;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.tdtest.AdvancedDoubleTapListener;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.images.RecyclingBitmapDrawable;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.tasks.CreatePhotoDrawableFromPathTask;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by andremacareno on 13/12/15.
 */
public class ViewPhotoLayout extends FrameLayout {
    private static final int minAlpha = 80;
    private static final int maxAlpha = 255;
    private AccelerateInterpolator interp;
    private static final SimpleDateFormat photoDateFormat = new SimpleDateFormat("dd.MM.yy H:mm", Locale.US);
    private static final String delegateKeyFormat = "viewphoto_%d_delegate";
    private PhotoView photoImageView;
    private View photoInfo;
    private RadialProgressView radialProgress;
    private ImageButton backButton;
    private FileModel photo;
    private RecyclingBitmapDrawable photoDrawable;
    private TextView photoFrom, photoDate;
    private Runnable closePhotoRunnable;
    private OnTouchListener photoViewTouchListener;
    private boolean swipeToDismissMode = false;
    private int bgColor;
    private float touchY;
    private float translationCancelThreshold;
    private boolean thresholdReached = false;
    private volatile float progress = 0;
    private boolean panelsHidden = false;
    private volatile boolean photoViewModeEnabled = false;
    private AnimatorSet showPanels, hidePanels;
    private AnimatorSet.AnimatorListener showListener, hideListener;
    private AdvancedDoubleTapListener doubleTapListener;
    public ViewPhotoLayout(Context context) {
        super(context);
        init();
    }

    public ViewPhotoLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ViewPhotoLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init()
    {
        interp = new AccelerateInterpolator(1.4f);
        showListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                backButton.setVisibility(View.VISIBLE);
                photoInfo.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        };
        hideListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                backButton.setVisibility(View.GONE);
                photoInfo.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        };
        translationCancelThreshold = ((float) TelegramApplication.sharedApplication().getResources().getDisplayMetrics().heightPixels) * 0.2f;
        bgColor = Color.argb(255, 0, 0, 0);
        setBackgroundColor(bgColor);
        View v = inflate(getContext(), R.layout.view_photo, this);
        photoInfo = v.findViewById(R.id.photo_info);
        photoFrom = (TextView) photoInfo.findViewById(R.id.photo_from);
        photoDate = (TextView) photoInfo.findViewById(R.id.photo_date);
        photoImageView = (PhotoView) v.findViewById(R.id.photo_view);
        radialProgress = (RadialProgressView) v.findViewById(R.id.radial_progress);
        backButton = (ImageButton) v.findViewById(R.id.photo_back_btn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePhotoRunnable.run();
            }
        });
        int centerX = getResources().getDisplayMetrics().widthPixels / 2;
        int centerY = getResources().getDisplayMetrics().heightPixels / 2;
        int halfRadialProgressSize = getResources().getDimensionPixelSize(R.dimen.radialprogress_size) / 2;
        radialProgress.setProgressRect(centerX - halfRadialProgressSize, centerY - halfRadialProgressSize, centerX + halfRadialProgressSize, centerY + halfRadialProgressSize);
        radialProgress.setBackground(ContextCompat.getDrawable(getContext(), R.color.transparent), true, true);
        radialProgress.setProgress(progress, false);
        photoViewTouchListener = new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                boolean correctIPhotoViewImplementation = photoImageView.getIPhotoViewImplementation() instanceof PhotoViewAttacher;
                if(photo == null || !photo.isDownloaded())
                    return true;
                if(motionEvent.getPointerCount() > 1 && correctIPhotoViewImplementation) {
                    if(swipeToDismissMode)
                        return true;
                    return ((PhotoViewAttacher) photoImageView.getIPhotoViewImplementation()).onTouch(view, motionEvent);
                }
                float scale = photoImageView.getIPhotoViewImplementation().getScale();
                if(scale != 1.0f && correctIPhotoViewImplementation)
                    return ((PhotoViewAttacher) photoImageView.getIPhotoViewImplementation()).onTouch(view, motionEvent);
                int mask = motionEvent.getAction() & MotionEvent.ACTION_MASK;
                if(mask == MotionEvent.ACTION_DOWN)
                {
                    touchY = motionEvent.getRawY();
                    if(!swipeToDismissMode && correctIPhotoViewImplementation)
                        return ((PhotoViewAttacher) photoImageView.getIPhotoViewImplementation()).onTouch(view, motionEvent);
                    thresholdReached = false;
                    return true;
                }
                else if(mask == MotionEvent.ACTION_UP)
                {
                    //dismiss
                    if(!swipeToDismissMode && correctIPhotoViewImplementation)
                        return ((PhotoViewAttacher) photoImageView.getIPhotoViewImplementation()).onTouch(view, motionEvent);
                    resetTranslation();
                    if(swipeToDismissMode && thresholdReached)
                    {
                        closePhotoRunnable.run();
                        swipeToDismissMode = false;
                        thresholdReached = false;
                        return true;
                    }
                    swipeToDismissMode = false;
                    thresholdReached = false;
                }
                else if(mask == MotionEvent.ACTION_MOVE)
                {
                    float translation = motionEvent.getRawY() - touchY;
                    if(!swipeToDismissMode && Math.abs(translation) > 30.0f) {
                        swipeToDismissMode = true;
                        hidePanels();
                    }
                    else if(!swipeToDismissMode && correctIPhotoViewImplementation)
                        return ((PhotoViewAttacher) photoImageView.getIPhotoViewImplementation()).onTouch(view, motionEvent);
                    float translationToThreshold = Math.abs(translation / translationCancelThreshold);
                    int alpha = translationToThreshold >= 1.0f ? minAlpha : (int) (maxAlpha - (maxAlpha - minAlpha) * translationToThreshold);
                    if(BuildConfig.DEBUG)
                        Log.d("ViewPhotoLayout", String.format("alpha = %d", alpha));
                    bgColor = Color.argb(alpha, 0, 0, 0);
                    setBackgroundColor(bgColor);
                    if(Math.abs(translation) >= translationCancelThreshold)
                        thresholdReached = true;
                    else
                        thresholdReached = false;
                    photoImageView.setTranslationY(translation);
                    return true;
                }
                return true;
            }
        };
        photoImageView.setOnTouchListener(photoViewTouchListener);
        if(photoImageView.getIPhotoViewImplementation() instanceof PhotoViewAttacher)
        {
            doubleTapListener = new AdvancedDoubleTapListener((PhotoViewAttacher)photoImageView.getIPhotoViewImplementation()) {
                @Override
                public void didSingleTapConfirmed() {
                    if(panelsHidden)
                        showPanels();
                    else
                        hidePanels();
                }
            };
            ((PhotoViewAttacher)photoImageView.getIPhotoViewImplementation()).setOnDoubleTapListener(doubleTapListener);
        }
    }
    public void setPhoto(FileModel ph, String name, int dateInSeconds)
    {
        radialProgress.setProgressColor(0xFFFFFFFF);
        this.photo = ph;
        long dateInMilliseconds = ((long) dateInSeconds) * 1000;
        photoFrom.setText(name);
        photoDate.setText(photoDateFormat.format(new Date(dateInMilliseconds)));
        photoViewModeEnabled = true;
        final String delegateKey = String.format(delegateKeyFormat, photo.getFileId());
        if(photo != null && !photo.isDownloaded())
        {
            if(!photo.getDelegates().containsKey(delegateKey))
            {
                FileModel.FileModelDelegate delegate = new FileModel.FileModelDelegate() {
                    @Override
                    public String getDelegateKey() {
                        return delegateKey;
                    }

                    @Override
                    public void didFileDownloadStarted() {
                        progress = 0;
                        if(radialProgress != null)
                            radialProgress.setProgress(progress, false);
                    }

                    @Override
                    public void didFileProgressUpdated() {
                        if(!photoViewModeEnabled)
                            return;
                        if(radialProgress != null && photo != null) {
                            progress = (float) photo.getDownloadedSize() / (float) photo.getFullSize();
                            radialProgress.setProgress(progress, true);
                        }
                    }

                    @Override
                    public void didFileDownloaded() {
                        if(photo != null)
                            photo.removeDelegate(this);
                        didPhotoDownloaded();
                    }

                    @Override
                    public void didFileDownloadCancelled() {
                    }
                };
                photo.addDelegate(delegate);
            }
            FileCache.getInstance().downloadFile(photo);
        }
        else
            didPhotoDownloaded();
        /*else if(photoDrawable != null)
        {
            radialProgress.setVisibility(View.GONE);
            photoImageView.setVisibility(View.VISIBLE);
            photoImageView.setImageDrawable(photoDrawable);
        }*/
    }

    public void didPhotoViewClosed()
    {
        panelsHidden = false;
        bgColor = Color.argb(255, 0, 0, 0);
        setBackgroundColor(bgColor);
        radialProgress.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
        photoInfo.setVisibility(View.VISIBLE);
        backButton.setAlpha(1.0f);
        photoInfo.setAlpha(1.0f);
        if(photoDrawable != null) {
            photoImageView.setImageDrawable(null);
            photoDrawable.setIsDisplayed(false);
            photoDrawable = null;
            photoViewModeEnabled = false;
        }
    }
    public void setCloseRunnable(Runnable closeRunnable)
    {
        this.closePhotoRunnable = closeRunnable;
    }
    private void didPhotoDownloaded()
    {
        /*if(!photoViewModeEnabled)
            return;*/
        radialProgress.setProgress(1, true);
        //radialProgress.setBackground(null, false, false);
        CreatePhotoDrawableFromPathTask t = new CreatePhotoDrawableFromPathTask(photo.getFile().path);
        t.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(AsyncTaskManTask task) {
                if(!photoViewModeEnabled)
                    return;
                photoDrawable = ((CreatePhotoDrawableFromPathTask) task).getDrawable();
                radialProgress.post(new Runnable() {
                    @Override
                    public void run() {
                        radialProgress.setVisibility(View.GONE);
                    }
                });
                photoImageView.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            photoDrawable.setIsDisplayed(true);
                            photoImageView.setImageDrawable(photoDrawable);
                            photoImageView.setVisibility(View.VISIBLE);
                        }
                        catch(Exception ignored) {}
                    }
                });
            }
        });
        t.addToQueue();
    }
    private void resetTranslation()
    {
        photoImageView.setTranslationY(0.0f);
        bgColor = Color.argb(255, 0, 0, 0);
        setBackgroundColor(bgColor);
    }
    private void hidePanels()
    {
        if(panelsHidden)
            return;
        panelsHidden = true;
        hidePanels = new AnimatorSet();
        hidePanels.setDuration(180);
        hidePanels.addListener(hideListener);
        hidePanels.playTogether(
                ObjectAnimator.ofFloat(backButton, "alpha", 1f, 0f),
                ObjectAnimator.ofFloat(photoInfo, "alpha", 1f, 0f)
        );
        hidePanels.start();
    }
    private void showPanels()
    {
        if(!panelsHidden)
            return;
        panelsHidden = false;
        showPanels = new AnimatorSet();
        showPanels.setDuration(180);
        showPanels.addListener(showListener);
        showPanels.playTogether(
                ObjectAnimator.ofFloat(backButton, "alpha", 0f, 1f),
                ObjectAnimator.ofFloat(photoInfo, "alpha", 0f, 1f)
        );
        showPanels.start();
    }
}
