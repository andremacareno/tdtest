/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andremacareno.tdtest.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.images.RecyclingImageView;
import com.andremacareno.tdtest.models.AlbumCoverDrawable;
import com.andremacareno.tdtest.views.player.PlayerView;

public class AlbumArtImageView extends RecyclingImageView {
    private PlayerView.PlayerDelegate playerDelegate;
    public AlbumArtImageView(Context context) {
        super(context);
    }

    public AlbumArtImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        if(drawable == null)
        {
            setScaleType(ScaleType.CENTER_INSIDE);
            super.setImageDrawable(getResources().getDrawable(R.drawable.ic_nocover));
            if(playerDelegate != null)
                playerDelegate.didCoverDarknessDetected(false);
            return;
        }
        if(drawable instanceof AlbumCoverDrawable)
        {
            try {
                AlbumCoverDrawable cast = (AlbumCoverDrawable) drawable;
                if(!cast.hasCover())
                {
                    setScaleType(ScaleType.CENTER_INSIDE);
                    super.setImageDrawable(getResources().getDrawable(R.drawable.ic_nocover));
                    if(playerDelegate != null)
                        playerDelegate.didCoverDarknessDetected(false);
                }
                else
                {
                    setScaleType(ScaleType.CENTER_CROP);
                    super.setImageDrawable(drawable);
                    if(playerDelegate != null)
                        playerDelegate.didCoverDarknessDetected(cast.isDarkCover());
                }
            }
            catch(Exception e) { e.printStackTrace(); }
        }
        else {
            setScaleType(ScaleType.CENTER_CROP);
            super.setImageDrawable(drawable);
        }
    }
    public void setPlayerDelegate(PlayerView.PlayerDelegate delegate)
    {
        this.playerDelegate = delegate;
    }
}
