package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.andremacareno.tdtest.AudioPlaybackService;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.AudioTrackModel;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 15/08/15.
 */
public class SharedAudioItem extends DownloadView {
    private AudioDownloadButton downloadButton;
    private RadialProgressView radialProgress;
    private ImageView selectionRing;
    private TextView trackName;
    private TextView bandName;

    private String trackNameString;
    private String bandNameString;
    private FileModel.FileModelDelegate delegate;
    private AudioPlaybackService.SimplePlaybackDelegate audioDelegate;

    public SharedAudioItem(Context context) {
        super(context);
    }

    @Override
    protected void didDownloadStarted() {
        radialProgress.setVisibility(VISIBLE);
        radialProgress.setProgress((float) attachedFile.getDownloadedSize() / attachedFile.getFullSize(), false);
    }

    @Override
    protected void didDownloadCancelled() {
        radialProgress.setVisibility(GONE);
    }

    @Override
    public DownloadButton getDownloadButton() {
        return this.downloadButton;
    }

    @Override
    protected void fillLayout() {
        if(attachedFile == null)
            return;
        FileModel.DownloadState state = attachedFile.getDownloadState();
        this.trackName.setText(trackNameString);
        this.bandName.setText(bandNameString);
        if(state == FileModel.DownloadState.LOADING)
        {
            radialProgress.setVisibility(View.VISIBLE);
            radialProgress.setProgress((float) attachedFile.getDownloadedSize() / attachedFile.getFullSize(), false);
        }
        else {
            radialProgress.setVisibility(View.GONE);
        }
    }

    @Override
    protected void init()
    {
        int DOWNLOAD_BTN_SIZE = getResources().getDimensionPixelSize(R.dimen.download_btn_size);
        View v = inflate(getContext(), R.layout.shared_audio, this);
        downloadButton = (AudioDownloadButton) v.findViewById(R.id.download_btn);
        selectionRing = (ImageView) v.findViewById(R.id.selection_mark);
        trackName = (TextView) v.findViewById(R.id.track_name);
        bandName = (TextView) v.findViewById(R.id.band_name);
        radialProgress = (RadialProgressView) v.findViewById(R.id.radial_progress);
        radialProgress.setProgressColor(ContextCompat.getColor(getContext(), R.color.dark_blue_bg));
        int oneDp = isInEditMode() ? 3 : AndroidUtilities.dp(1);
        radialProgress.setProgressRect(oneDp, oneDp, DOWNLOAD_BTN_SIZE - oneDp, DOWNLOAD_BTN_SIZE - oneDp);
        radialProgress.setProgress(0, false);
        radialProgress.setBackground(getResources().getDrawable(R.color.transparent), true, true);
        radialProgress.setVisibility(View.GONE);
    }

    public void check(boolean animated)
    {
        //TODO revealingimageview + animate
        setBackgroundResource(R.color.sharedaudio_selection);
        selectionRing.setImageDrawable(getResources().getDrawable(R.drawable.ic_attach_check));
    }
    public void uncheck(boolean animated)
    {
        setBackgroundResource(R.drawable.list_selector);
        selectionRing.setImageDrawable(getResources().getDrawable(R.drawable.sharedmedia_audio_selection_ring));
    }
    public void setSelectionMode(boolean enabled)
    {
        if(enabled)
        {
            setBackgroundResource(R.color.sharedaudio_selection);
            downloadButton.setVisibility(View.GONE);
            radialProgress.setVisibility(View.GONE);
            selectionRing.setVisibility(View.VISIBLE);
        }
        else {
            setBackgroundResource(R.drawable.list_selector);
            downloadButton.setVisibility(View.VISIBLE);
            radialProgress.setVisibility(View.GONE);
            selectionRing.setVisibility(View.GONE);
        }
    }
    public void setTrackName(String str) { this.trackNameString = str; }
    public void setBandName(String str) { this.bandNameString = str; }
    public FileModel.FileModelDelegate getDelegate() {
        if(delegate == null)
            delegate = new FileModel.FileModelDelegate() {
                @Override
                public String getDelegateKey() {
                    return generateDelegateKey();
                }

                @Override
                public void didFileProgressUpdated() {
                    if(radialProgress.getVisibility() != View.VISIBLE)
                        radialProgress.setVisibility(View.GONE);
                    radialProgress.setProgress((float) attachedFile.getDownloadedSize() / attachedFile.getFullSize(), true);
                }

                @Override
                public void didFileDownloaded() {
                    radialProgress.setProgress(1.0f, false);
                    radialProgress.setVisibility(GONE);
                    downloadButton.invalidate();
                }

                @Override
                public void didFileDownloadCancelled() {
                    didDownloadCancelled();
                    getDownloadButton().invalidate();
                }
                @Override
                public void didFileDownloadStarted() {
                    didDownloadStarted();
                    getDownloadButton().invalidate();
                }
            };
        return delegate;
    }
    @Override
    public void attachDelegateToFile(final FileModel file)
    {
        super.attachDelegateToFile(file);
        AudioPlaybackService playbackService = TelegramApplication.sharedApplication().getAudioPlaybackService();
        if(playbackService != null)
        {
            if(file == null && audioDelegate != null)
            {
                playbackService.removeSimpleDelegate(audioDelegate);
                return;
            }
            if(audioDelegate == null)
                audioDelegate = new AudioPlaybackService.SimplePlaybackDelegate() {
                    @Override
                    public void didPlaybackStarted(FileModel track) {
                        if(attachedFile.getFileId() == track.getFileId())
                            downloadButton.setPlaybackState(true);
                    }

                    @Override
                    public void didPlaybackPaused(FileModel track) {
                        if(attachedFile.getFileId() == track.getFileId())
                            downloadButton.setPlaybackState(false);
                    }

                    @Override
                    public void didPlaybackStopped() {
                        downloadButton.setPlaybackState(false);
                    }

                    @Override
                    public String getDelegateKey() {
                        return String.format("audiodelegate%d_%d", getAttachedMsgId(), file.getFileId());
                    }
                };
            playbackService.addSimpleDelegate(audioDelegate);
            AudioTrackModel nowPlaying = playbackService.nowPlaying();
            if(nowPlaying == null)
                downloadButton.setPlaybackState(false);
            else
            {
                if(!playbackService.isAudioPlaying())
                    downloadButton.setPlaybackState(false);
                else if(nowPlaying.getTrack().getFileId() == file.getFileId())
                    downloadButton.setPlaybackState(true);
                else
                    downloadButton.setPlaybackState(false);
            }
        }
    }
}
