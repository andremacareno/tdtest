package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.Emoji;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 05/05/15.
 */
public class FwdChatGridItem extends LinearLayout {
    private SelectableAvatarImageView avatar;
    private TextView dialogName;
    private RevealingCheckView check;
    private int avatarSize;

    public FwdChatGridItem(Context context) {
        super(context);
        init();
    }
    public FwdChatGridItem(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    private void init()
    {
        setOrientation(VERTICAL);

        View itemView = inflate(getContext(), R.layout.fwd_chatlist_item, this);
        setClickable(true);
        setBackgroundResource(R.drawable.list_selector);
        this.avatar = (SelectableAvatarImageView) itemView.findViewById(R.id.chat_avatar);
        this.dialogName = (TextView) itemView.findViewById(R.id.chat_name);
        this.check = (RevealingCheckView) itemView.findViewById(R.id.fwd_check);
        this.avatarSize = getResources().getDimensionPixelSize(R.dimen.avatar_size);
    }
    public SelectableAvatarImageView getAvatarImageView() { return this.avatar; }
    public void fillLayout(TdApi.Chat chat) {
        dialogName.setText(Emoji.replaceEmoji(chat.title, dialogName.getPaint().getFontMetricsInt(), AndroidUtilities.dp(16), false));
    }
    public void loadAvatar(TdApi.Chat chat)
    {
        FileModel photo = FileCache.getInstance().getFileModel(CommonTools.isLowDPIScreen() ? chat.photo.small : chat.photo.big);
        String newKey = CommonTools.makeFileKey(photo.getFileId());
        TelegramImageLoader.getInstance().loadImage(photo, newKey, avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, avatarSize, avatarSize, false);
    }
    public void loadPlaceholder(TdApi.Chat chat)
    {
        String initials = CommonTools.getInitialsByChatInfo(chat);
        int peer = chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR ? ((TdApi.ChannelChatInfo) chat.type).channel.id : chat.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR ? ((TdApi.PrivateChatInfo) chat.type).user.id : ((TdApi.GroupChatInfo) chat.type).group.id;
        String key = String.format(Constants.AVATAR_PLACEHOLDER_FMT, peer);
        TelegramImageLoader.getInstance().loadPlaceholder(peer, initials, key, avatar, false);
    }
    public void check(boolean animate)
    {
        this.avatar.select(animate);
        this.check.check(animate);
    }
    public void uncheck(boolean animate)
    {
        this.avatar.deselect(animate);
        this.check.uncheck(animate);
    }
    @Override
    public void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
    }
}
