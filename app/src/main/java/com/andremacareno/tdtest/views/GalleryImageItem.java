package com.andremacareno.tdtest.views;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.images.RecyclingImageView;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 16/08/15.
 */
public class GalleryImageItem extends FrameLayout {
    private RecyclingImageView img;
    private FrameLayout checkContainer;
    private RevealingCheckView checkIcon;
    public GalleryImageItem(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        img = new RecyclingImageView(getContext());
        checkContainer = new FrameLayout(getContext());
        checkContainer.setBackgroundResource(R.drawable.sharedmedia_img_selection_ring);
        checkIcon = new RevealingCheckView(getContext());
        FrameLayout.LayoutParams imgLayoutParams = new LayoutParams(AndroidUtilities.dp(100), AndroidUtilities.dp(100), Gravity.CENTER);
        FrameLayout.LayoutParams checkLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        FrameLayout.LayoutParams selectionMarkLayoutParams = new LayoutParams(AndroidUtilities.dp(24), AndroidUtilities.dp(24), Gravity.TOP | Gravity.LEFT);
        int selectionMarkPadding = AndroidUtilities.dp(4);
        selectionMarkLayoutParams.setMargins(selectionMarkPadding, selectionMarkPadding, selectionMarkPadding, selectionMarkPadding);
        img.setLayoutParams(imgLayoutParams);
        checkContainer.setLayoutParams(selectionMarkLayoutParams);
        checkIcon.setLayoutParams(checkLP);
        img.setPadding(selectionMarkPadding, selectionMarkPadding, selectionMarkPadding, selectionMarkPadding);
        img.setScaleType(ImageView.ScaleType.CENTER_CROP);
        img.setVisibility(View.VISIBLE);
        checkContainer.setVisibility(View.VISIBLE);

        addView(img);
        addView(checkContainer);
        checkContainer.addView(checkIcon);
    }
    public RecyclingImageView getImageView() { return this.img; }
    public void markSelected(boolean animated)
    {
        checkIcon.check(animated);
    }
    public void markUnselected(boolean animated)
    {
        checkIcon.uncheck(animated);
    }
}
