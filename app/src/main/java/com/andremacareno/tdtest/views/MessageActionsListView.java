package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewTreeObserver;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.listadapters.ExactlyHeightLayoutManager;
import com.andremacareno.tdtest.models.MessageModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.ChatAttachListController;
import com.andremacareno.tdtest.viewcontrollers.ViewControllable;
import com.andremacareno.tdtest.viewcontrollers.ViewController;

/**
 * Created by andremacareno on 04/04/15.
 */
public class MessageActionsListView extends BaseListView implements ViewControllable, BottomSheet {
    public interface MessageActionsDelegate
    {
        public void onBottomSheetClose(boolean dismissed);
        public boolean canSendMessage();
        public boolean canDeleteMessage(MessageModel msg);
        public boolean canCopyMessage(MessageModel msg);
        public void onReplyRequested(MessageModel msg);
        public void onShareRequested(MessageModel msg);
        public void onCopyRequested(MessageModel msg);
        public void onSelectRequested(MessageModel msg);
        public void onDeleteRequested(MessageModel msg);
        public void onActionChosen();
    }
    private ChatAttachListController controller;
    private ExactlyHeightLayoutManager lm;
    private float yFraction;
    private MessageActionsDelegate delegate;
    private ViewTreeObserver.OnPreDrawListener preDrawListener;
    public MessageActionsListView(Context context) {
        super(context);
        init();
    }

    public MessageActionsListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MessageActionsListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init()
    {
        if(isInEditMode())
            return;
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
        if(lm == null)
            lm = new ExactlyHeightLayoutManager(TelegramApplication.sharedApplication().getApplicationContext(), AndroidUtilities.dp(156));
        setLayoutManager(lm);
        setHasFixedSize(true);
    }
    @Override
    public void setController(ViewController vc)
    {
        this.controller = (ChatAttachListController) vc;
    }

    @Override
    public ViewController getController() {
        return controller;
    }
    public void setNewElementCount(int count)
    {
        ((ExactlyHeightLayoutManager) lm).setSpecifiedHeight(count * AndroidUtilities.dp(52));
    }

    @Override
    public void onClose(boolean dismissed) {
        if(delegate != null)
            delegate.onBottomSheetClose(dismissed);
    }
    public void setDelegate(MessageActionsDelegate delegate)
    {
        this.delegate = delegate;
    }
    @Override
    public void setYFraction(float fraction) {

        this.yFraction = fraction;
        int height = Math.max(lm.getExactlyHeight(), getHeight());
        if(BuildConfig.DEBUG)
            Log.d("MessageActions", String.format("height = %d", height));
        if (height <= 0) {
            if (preDrawListener == null) {
                preDrawListener = new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        getViewTreeObserver().removeOnPreDrawListener(preDrawListener);
                        setYFraction(yFraction);
                        return true;
                    }
                };
                getViewTreeObserver().addOnPreDrawListener(preDrawListener);
            }
            return;
        }
        float translationY = height * fraction;
        setTranslationY(translationY);
    }
    @Override
    public float getYFraction() {
        return this.yFraction;
    }
}
