package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;

import com.andremacareno.tdtest.R;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 16/08/15.
 */
public class AttachMenuView extends RelativeLayout implements BottomSheet {

    public interface AttachMenuDelegate
    {
        public void requestPerformQuickSend();
        public void didQuickSendSelectionCountUpdated(int newCount);
        public void sendGalleryImage(TdApi.InputMessageContent img);
        public void shareContactRequested();
        public void onBottomSheetClose(boolean dismissed);
        public void onActionChoose();
    }
    private float yFraction;
    private ViewTreeObserver.OnPreDrawListener preDrawListener;
    public View attachListDivider;
    public ChatAttachListView attachActionsListView;
    public RecentFromGalleryListView recentFromGallery;
    private final DecelerateInterpolator slideUpInterpolator = new DecelerateInterpolator(1.7f);
    private final AccelerateInterpolator slideDownInterpolator = new AccelerateInterpolator(1.7f);
    private AttachMenuDelegate delegate;

    public AttachMenuView(Context context) {
        super(context);
        init();
    }

    public AttachMenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.attach_menu, this);
        attachListDivider = v.findViewById(R.id.item_divider);
        attachActionsListView = (ChatAttachListView) v.findViewById(R.id.attach_actions);
        recentFromGallery = (RecentFromGalleryListView) v.findViewById(R.id.recent_from_gallery);
    }
    public void setYFraction(float fraction) {

        this.yFraction = fraction;
        if (getHeight() == 0) {
            if (preDrawListener == null) {
                preDrawListener = new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        getViewTreeObserver().removeOnPreDrawListener(preDrawListener);
                        setYFraction(yFraction);
                        return true;
                    }
                };
                getViewTreeObserver().addOnPreDrawListener(preDrawListener);
            }
            return;
        }
        float translationY = getHeight() * fraction;
        setTranslationY(translationY);
    }
    public float getYFraction() {
        return this.yFraction;
    }
    public void setDelegate(AttachMenuDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    @Override
    public void onClose(boolean dismissed) {
        if(delegate != null)
            delegate.onBottomSheetClose(dismissed);
    }
}
