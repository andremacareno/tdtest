package com.andremacareno.tdtest.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by andremacareno on 17/08/15.
 */
public class AudioDownloadButton extends DownloadButton {
    private int DOWNLOAD_BTN_SIZE;
    private Drawable downloadDrawable;
    private Drawable pauseDownloadDrawable;
    private Drawable playDrawable;
    private Drawable pausePlaybackDrawable;
    private Paint lightBlueCirclePaint;
    private Paint darkBlueCirclePaint;
    private boolean isPlaying = false;

    public AudioDownloadButton(Context context) {
        super(context);
    }
    public AudioDownloadButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(DOWNLOAD_BTN_SIZE, DOWNLOAD_BTN_SIZE);
    }


    @Override
    protected void init()
    {
        DOWNLOAD_BTN_SIZE = getResources().getDimensionPixelSize(R.dimen.download_btn_size);
        downloadDrawable = getResources().getDrawable(R.drawable.ic_download_blue);
        pauseDownloadDrawable = getResources().getDrawable(R.drawable.ic_file_pause_blue);
        playDrawable = getResources().getDrawable(R.drawable.ic_file_play);
        pausePlaybackDrawable = getResources().getDrawable(R.drawable.ic_pause);
        if(isInEditMode())
            playDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-18, DOWNLOAD_BTN_SIZE/2-22, DOWNLOAD_BTN_SIZE/2+22, DOWNLOAD_BTN_SIZE/2+22);
        else
        {
            final int eighteenDp = AndroidUtilities.dp(18)/2;
            final int twelveDp = AndroidUtilities.dp(12)/2;
            final int fourteenDp = AndroidUtilities.dp(14)/2;
            final int fifteenDp = AndroidUtilities.dp(15)/2;
            downloadDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-twelveDp, DOWNLOAD_BTN_SIZE/2-eighteenDp, DOWNLOAD_BTN_SIZE/2+twelveDp, DOWNLOAD_BTN_SIZE/2+eighteenDp);
            pauseDownloadDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-twelveDp, DOWNLOAD_BTN_SIZE/2-fourteenDp, DOWNLOAD_BTN_SIZE/2+twelveDp, DOWNLOAD_BTN_SIZE/2+fourteenDp);
            playDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-twelveDp, DOWNLOAD_BTN_SIZE/2-fifteenDp, DOWNLOAD_BTN_SIZE/2+fourteenDp, DOWNLOAD_BTN_SIZE/2+fifteenDp);
            pausePlaybackDrawable.setBounds(DOWNLOAD_BTN_SIZE/2-twelveDp, DOWNLOAD_BTN_SIZE/2-fourteenDp, DOWNLOAD_BTN_SIZE/2+twelveDp, DOWNLOAD_BTN_SIZE/2+fourteenDp);
        }
        lightBlueCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        lightBlueCirclePaint.setColor(ContextCompat.getColor(getContext(), R.color.light_blue_bg));
        darkBlueCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        darkBlueCirclePaint.setColor(ContextCompat.getColor(getContext(), R.color.dark_blue_bg));
    }
    @Override
    public void onDraw(Canvas canvas)
    {
        if(isInEditMode())
        {
            canvas.drawCircle(DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, darkBlueCirclePaint);
            playDrawable.draw(canvas);
            return;
        }
        if(attachedFile == null)
            return;
        if(attachedFile.getDownloadState() == FileModel.DownloadState.LOADED)
        {
            canvas.drawCircle(DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, darkBlueCirclePaint);
            if(!isPlaying)
                playDrawable.draw(canvas);
            else
                pausePlaybackDrawable.draw(canvas);
        }
        else
        {
            canvas.drawCircle(DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, DOWNLOAD_BTN_SIZE / 2, lightBlueCirclePaint);
            if(attachedFile.getDownloadState() == FileModel.DownloadState.EMPTY)
                downloadDrawable.draw(canvas);
            else if(attachedFile.getDownloadState() == FileModel.DownloadState.LOADING)
                pauseDownloadDrawable.draw(canvas);
        }
    }
    public void setPlaybackState(boolean playing)
    {
        this.isPlaying = playing;
        invalidate();
    }
}
