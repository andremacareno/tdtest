package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.design.widget.NavigationView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.CurrentUserModel;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.UserModel;

/**
 * Created by Andrew on 10.05.2015.
 */
public class NavDrawerView extends NavigationView {
    private View layout;
    private AvatarImageView avatar;
    public NavDrawerView(Context context) {
        super(context);
        init();
    }

    public NavDrawerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NavDrawerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    private void init()
    {
        layout = inflateHeaderView(R.layout.navdrawer_header);
    }
    public void initHeader()
    {
        TextView username = (TextView) layout.findViewById(R.id.appdrawer_username);
        avatar = (AvatarImageView) layout.findViewById(R.id.appdrawer_avatar);
        TextView phoneNumber = (TextView) layout.findViewById(R.id.appdrawer_phone);

        UserModel itm = CurrentUserModel.getInstance().getUserModel();
        FileModel photo = itm.getPhoto();
        int fileId = photo.getFileId();
        if(fileId != 0)
            TelegramImageLoader.getInstance().loadImage(photo, itm.getPhotoKey().concat("_highres"), avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.big_avatar_size), TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.big_avatar_size), false);
        else
            TelegramImageLoader.getInstance().loadPlaceholder(itm.getId(), itm.getInitials(), itm.getPhotoPlaceholderKey(), avatar, false);

        username.setText(itm.getDisplayName());
        phoneNumber.setText(itm.getFormattedPhoneNumber());
    }
    public void refreshAvatar()
    {
        if(CurrentUserModel.getInstance() == null)
            return;
        UserModel itm = CurrentUserModel.getInstance().getUserModel();
        if(itm == null || avatar == null)
            return;
        FileModel photo = itm.getPhoto();
        int fileId = photo.getFileId();
        if(fileId != 0)
            TelegramImageLoader.getInstance().loadImage(photo, itm.getPhotoKey().concat("_highres"), avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.big_avatar_size), TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.big_avatar_size), false);
        else
            TelegramImageLoader.getInstance().loadPlaceholder(itm.getId(), itm.getInitials(), itm.getPhotoPlaceholderKey(), avatar, false);
    }
    public void refreshInfo()
    {
        try {
            TextView username = (TextView) layout.findViewById(R.id.appdrawer_username);
            TextView phoneNumber = (TextView) layout.findViewById(R.id.appdrawer_phone);
            UserModel itm = CurrentUserModel.getInstance().getUserModel();
            username.setText(itm.getDisplayName());
            phoneNumber.setText(itm.getFormattedPhoneNumber());
        }
        catch(Exception ignored) {}

    }
}
