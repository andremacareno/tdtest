package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.MessageModel;

/**
 * Created by andremacareno on 20/05/15.
 */
public class MessageStatusView extends View {
    public interface UpdateChatReadDelegate
    {
        public void processUpdateChatRead(int chatReadOutbox);
    }
    public static final int STATUS_SENDING = 0;
    public static final int STATUS_UNREAD = 1;
    public static final int STATUS_ERROR = 2;
    public static final int STATUS_READ = 3;            //nice stairs

    private MessageModel attachedMsg;
    private UpdateChatReadDelegate delegate;
    private int SENDING_DRAWABLE, UNREAD_DRAWABLE, ERROR_DRAWABLE;
    public MessageStatusView(Context context) {
        super(context);
        init();
    }

    public MessageStatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MessageStatusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init()
    {
        SENDING_DRAWABLE = R.drawable.ic_clock;
        UNREAD_DRAWABLE = R.drawable.unread_status_circle;
        ERROR_DRAWABLE = R.drawable.ic_error;
        delegate = new UpdateChatReadDelegate() {
            @Override
            public void processUpdateChatRead(int chatReadOutbox) {
                int status;
                if(BuildConfig.DEBUG)
                    Log.d("MessageStatusView", String.format("chatReadOutbox = %d; msgId = %d", chatReadOutbox, attachedMsg == null ? 0 : attachedMsg.getMessageId()));
                if(attachedMsg == null)
                    status = MessageStatusView.STATUS_READ;
                else if(attachedMsg.isSending())
                    status = MessageStatusView.STATUS_SENDING;
                else if(attachedMsg.getIntegerDate() == 0)
                    status = MessageStatusView.STATUS_ERROR;
                else if(attachedMsg.isOut() && attachedMsg.getMessageId() > chatReadOutbox)
                    status = MessageStatusView.STATUS_UNREAD;
                else
                    status = MessageStatusView.STATUS_READ;
                setVisibility(status == STATUS_READ ? View.GONE : View.VISIBLE);
                if(status != STATUS_READ)
                {
                    int drawableToSet;
                    if(status == STATUS_SENDING)
                        drawableToSet = SENDING_DRAWABLE;
                    else if(status == STATUS_UNREAD)
                        drawableToSet = UNREAD_DRAWABLE;
                    else if(status == STATUS_ERROR)
                        drawableToSet = ERROR_DRAWABLE;
                    else {
                        setVisibility(View.GONE);
                        return;
                    }
                    setBackgroundResource(drawableToSet);
                }
            }
        };
    }
    public UpdateChatReadDelegate getDelegate() { return this.delegate; }
    public void attachMessage(MessageModel msg)
    {
        this.attachedMsg = msg;
    }
}
