package com.andremacareno.tdtest.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by andremacareno on 13/08/15.
 */
public class RecentFromGalleryListView extends RecyclerView{
    private LinearLayoutManager lm;
    public RecentFromGalleryListView(Context context) {
        super(context);
        init();
    }

    public RecentFromGalleryListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        if(lm == null)
            lm = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        setLayoutManager(lm);
    }
}
