package com.andremacareno.tdtest.views.actionviews;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.andremacareno.tdtest.R;

/**
 * Created by andremacareno on 12/05/15.
 */
public class NewChatActionView extends ActionView {
    private static final String TAG = "NewChatActionView";
    public TextView secondLine;
    public ImageButton backButton;
    public ImageButton doneButton;

    public NewChatActionView(Context context) {
        super(context);
    }


    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void initView() {
        View v = inflate(getContext(), R.layout.action_view_new_chat, this);
        FrameLayout middle = (FrameLayout) v.findViewById(R.id.chat_info);
        secondLine = (TextView) middle.findViewById(R.id.chat_subtitle);
        backButton = (ImageButton) v.findViewById(R.id.new_chat_back_btn);
        doneButton = (ImageButton) v.findViewById(R.id.new_chat_done);
    }
    public void setBackButtonClickListener(OnClickListener listener)
    {
        backButton.setOnClickListener(listener);
    }
    public void setDoneButtonClickListener(OnClickListener listener)
    {
        doneButton.setOnClickListener(listener);
    }
    public void updateSecondLine(String newString)
    {
        secondLine.setText(newString);
    }
}
