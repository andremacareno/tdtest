package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.UserModel;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 22/06/15.
 */
public class UserDetailsView extends LinearLayout {
    public interface UserDetailsDelegate
    {
        public void didUsernameViewClicked();
        public void didPhoneViewClicked();
    }
    private UserNameView user_name;
    private UserPhoneView user_phone;
    private BotAboutView bot_about;
    private UserModel boundUser;
    private UserDetailsDelegate delegate;
    private boolean dontHideUsernameField = false;
    public UserDetailsView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.user_details, this);
        setOrientation(VERTICAL);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        user_name = (UserNameView) v.findViewById(R.id.username);
        user_phone = (UserPhoneView) v.findViewById(R.id.phone);
        bot_about = (BotAboutView) v.findViewById(R.id.bot_info);
        user_name.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (delegate != null)
                    delegate.didUsernameViewClicked();
            }
        });
        user_phone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (delegate != null)
                    delegate.didPhoneViewClicked();
            }
        });
    }
    public void setDontHideUsernameField(boolean dontHide) { this.dontHideUsernameField = dontHide; }
    private void fillLayout()
    {
        if(boundUser == null)
            return;
        String formattedPhoneNumber = boundUser.getFormattedPhoneNumber();
        String displayUsername = boundUser.getDisplayUsername();
        if(displayUsername.length() <= 1 && !dontHideUsernameField)
            user_name.setVisibility(View.GONE);
        else {
            user_name.setVisibility(View.VISIBLE);
            user_name.setUserName(displayUsername.length() > 1 ? displayUsername : getResources().getString(R.string.unspecified));
        }
        if(boundUser.isBot())
        {
            user_phone.setVisibility(View.GONE);
            bot_about.setVisibility(View.VISIBLE);
            bot_about.setAbout(((TdApi.BotInfoGeneral)boundUser.getBotInfo()).shareText);
        }
        else
        {
            bot_about.setVisibility(View.GONE);
            user_phone.setVisibility(View.VISIBLE);
            user_phone.setPhone(formattedPhoneNumber);
        }
    }
    public void bindUser(UserModel user)
    {
        if(user == null)
            return;
        this.boundUser = user;
        fillLayout();
    }
    public void updateUserPhone(String newPhone)
    {
        try {
            user_phone.setPhone(CommonTools.formatPhoneNumber(newPhone));
        }
        catch(Exception ignored) {}
    }
    public void updateUsername(String newUsername)
    {
        if(newUsername.isEmpty()) {
            if(!dontHideUsernameField)
                user_name.setVisibility(View.GONE);
            return;
        }
        user_name.setVisibility(VISIBLE);
        if(newUsername.isEmpty())
            user_name.setUserName(getResources().getString(R.string.unspecified));
        else
            user_name.setUserName("@".concat(newUsername));
    }
    public void setDelegate(UserDetailsDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
}
