package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;

import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.listadapters.ExactlyHeightLayoutManager;
import com.andremacareno.tdtest.viewcontrollers.BotCmdListController;

/**
 * Created by andremacareno on 04/04/15.
 */
public class BotCmdListView extends BaseListView{
    private BotCmdListController controller;
    private LayoutManager lm;
    public BotCmdListView(Context context) {
        super(context);
        init();
    }

    public BotCmdListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BotCmdListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init()
    {
        if(lm == null)
            lm = new ExactlyHeightLayoutManager(TelegramApplication.sharedApplication().getApplicationContext(), true);
        setLayoutManager(lm);
        //setItemAnimator(new DefaultItemAnimator());
    }
}
