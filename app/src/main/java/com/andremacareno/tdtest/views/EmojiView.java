package com.andremacareno.tdtest.views;

/*
 * Based on source code of Telegram for Android
 * It is licensed under GNU GPL v. 2 or later.
 *
 * Copyright Nikolai Kudashov, 2013.
 */

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.graphics.Point;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.HapticFeedbackConstants;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Emoji;
import com.andremacareno.tdtest.FlowLayoutManager;
import com.andremacareno.tdtest.GifKeyboardCache;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.StickersCache;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.images.AnimationView;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.utils.AndroidUtilities;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by andremacareno on 09/08/15.
 */
public class EmojiView extends FrameLayout {

    public interface Listener {
        boolean onBackspace();
        void onEmojiSelected(String emoji);
        void onStickerSelected(TdApi.Sticker sticker);
        void onGifSelected(TdApi.Animation anim);
    }

    private ArrayList<EmojiGridAdapter> adapters = new ArrayList<>();
    private HashMap<Integer, Integer> stickersUseHistory = new HashMap<>();
    private ArrayList<TdApi.Sticker> recentStickers = new ArrayList<>();
    private ArrayList<TdApi.StickerSet> stickerSets = new ArrayList<>();
    private NotificationObserver stickersLoadObserver, stickersUpdateObserver, gifKeyboardUpdateObserver, gifKeyboardLoadObserver;
    private Point displaySize = new Point();
    private int[] icons = {
            R.drawable.ic_emoji_recent,
            R.drawable.ic_emoji_smile,
            R.drawable.ic_emoji_flower,
            R.drawable.ic_emoji_bell,
            R.drawable.ic_emoji_car,
            R.drawable.ic_emoji_symbol,
            R.drawable.ic_emoji_sticker};

    private Listener listener;
    private ViewPager pager;
    private FrameLayout recentsWrap;
    private FrameLayout stickersWrap;
    private ArrayList<GridView> views = new ArrayList<>();
    private ImageView backspaceButton;
    private StickersGridAdapter stickersGridAdapter;
    private LinearLayout pagerSlidingTabStripContainer;
    private ScrollSlidingTabStrip scrollSlidingTabStrip;
    private GridView stickersGridView;
    private BaseListView gifsGridView;
    private TextView stickersEmptyView;
    private FlowLayoutManager flowLayoutManager;
    private GifsAdapter gifsAdapter;

    private int stickersTabOffset;
    private int recentTabBum = -2;
    private int gifTabBum = -2;
    private boolean switchToGifTab;

    private int oldWidth;
    private int lastNotifyWidth;

    private boolean backspacePressed;
    private boolean backspaceOnce;
    private boolean showStickers;
    private boolean showGifs;

    public EmojiView(boolean needStickers, boolean needGif, final Context context) {
        super(context);
        WindowManager manager = (WindowManager) TelegramApplication.sharedApplication().getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        if (manager != null) {
            Display display = manager.getDefaultDisplay();
            if (display != null) {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                display.getMetrics(displayMetrics);
                if (android.os.Build.VERSION.SDK_INT < 13) {
                    displaySize.set(display.getWidth(), display.getHeight());
                } else {
                    display.getSize(displaySize);
                }
            }
        }
        stickersLoadObserver = new NotificationObserver(NotificationCenter.didStickersLoaded) {
            @Override
            public void didNotificationReceived(Notification notification) {
                post(new Runnable() {
                    @Override
                    public void run() {
                        updateStickerTabs();
                        stickersGridAdapter.notifyDataSetChanged();
                    }
                });
            }
        };
        stickersUpdateObserver = new NotificationObserver(NotificationCenter.didStickersUpdated) {
            @Override
            public void didNotificationReceived(Notification notification) {
                try {
                    post(new Runnable() {
                        @Override
                        public void run() {
                            updateStickerTabs();
                            stickersGridAdapter.notifyDataSetChanged();
                        }
                    });
                }
                catch(Exception ignored) {}
            }
        };
        gifKeyboardLoadObserver = new NotificationObserver(NotificationCenter.didSavedAnimationsLoaded) {
            @Override
            public void didNotificationReceived(Notification notification) {
                post(new Runnable() {
                    @Override
                    public void run() {
                        updateStickerTabs();
                        gifsAdapter.notifyDataSetChanged();
                    }
                });
            }
        };
        gifKeyboardUpdateObserver = new NotificationObserver(NotificationCenter.didSavedAnimationsUpdated) {
            @Override
            public void didNotificationReceived(Notification notification) {
                post(new Runnable() {
                    @Override
                    public void run() {
                        updateStickerTabs();
                        gifsAdapter.notifyDataSetChanged();
                    }
                });
            }
        };
        showStickers = needStickers;
        showGifs = needGif;
        for (int i = 0; i < Emoji.data.length; i++) {
            GridView gridView = new GridView(context);
            gridView.setColumnWidth(AndroidUtilities.dp(45));
            gridView.setNumColumns(-1);
            views.add(gridView);

            EmojiGridAdapter emojiGridAdapter = new EmojiGridAdapter(Emoji.data[i]);
            gridView.setAdapter(emojiGridAdapter);
            //AndroidUtilities.setListViewEdgeEffectColor(gridView, 0xfff5f6f7);
            adapters.add(emojiGridAdapter);
        }

        if (showStickers) {
            StickersCache.checkStickers();
            stickersGridView = new GridView(context) {
                @Override
                public void setVisibility(int visibility) {
                    if (gifsGridView != null && gifsGridView.getVisibility() == VISIBLE) {
                        super.setVisibility(GONE);
                        return;
                    }
                    super.setVisibility(visibility);
                }
            };
            stickersGridView.setColumnWidth(AndroidUtilities.dp(72));
            stickersGridView.setNumColumns(-1);
            stickersGridView.setPadding(0, AndroidUtilities.dp(4), 0, 0);
            stickersGridView.setClipToPadding(false);
            views.add(stickersGridView);
            stickersGridAdapter = new StickersGridAdapter(context);
            stickersGridView.setAdapter(stickersGridAdapter);
            //AndroidUtilities.setListViewEdgeEffectColor(gridView, 0xfff5f6f7);

            stickersWrap = new FrameLayout(context);
            stickersWrap.addView(stickersGridView);

            if (needGif) {
                GifKeyboardCache.checkGifs();
                gifsGridView = new BaseListView(context);
                gifsGridView.setLayoutManager(flowLayoutManager = new FlowLayoutManager() {

                    private Size size = new Size(0, 0);

                    @Override
                    protected Size getSizeForItem(int i) {
                        try {
                                ArrayList<TdApi.Animation> animations = GifKeyboardCache.getAnimations();
                                synchronized (animations) {
                                    TdApi.Animation anim = animations.get(i);
                                    if (anim.width != 0 && anim.height != 0)
                                        size.upd(anim.width, anim.height);
                                    else if(anim.thumb.width != 0 && anim.thumb.height != 0)
                                        size.upd(anim.thumb.photo.id != 0 ? anim.thumb.width : 100, anim.thumb.photo.id != 0 ? anim.thumb.height : 100);
                                    else
                                        size.upd(100, 100);
                                }
                        }
                        catch(Exception e) { size.upd(100, 100); }
                        if(BuildConfig.DEBUG)
                            Log.d("EmojiView", String.format("size: %d %d", size.getWidth(), size.getHeight()));
                        return size;
                    }
                });
                gifsGridView.setMotionEventSplittingEnabled(false);
                if (Build.VERSION.SDK_INT >= 9) {
                    gifsGridView.setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);
                }
                gifsGridView.setAdapter(gifsAdapter = new GifsAdapter(context));
                gifsGridView.setVisibility(GONE);
                stickersWrap.addView(gifsGridView);
            }

            stickersEmptyView = new TextView(context);
            stickersEmptyView.setText(R.string.no_stickers);
            stickersEmptyView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            stickersEmptyView.setTextColor(0xff888888);
            stickersWrap.addView(stickersEmptyView, new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.CENTER));
            stickersGridView.setEmptyView(stickersEmptyView);

            scrollSlidingTabStrip = new ScrollSlidingTabStrip(context) {

                boolean startedScroll;
                float lastX;
                float lastTranslateX;
                boolean first = true;

                @Override
                public boolean onInterceptTouchEvent(MotionEvent ev) {
                    if (getParent() != null) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                    }
                    return super.onInterceptTouchEvent(ev);
                }

                @Override
                public boolean onTouchEvent(MotionEvent ev) {
                    if (Build.VERSION.SDK_INT >= 11) {
                        if (first) {
                            first = false;
                            lastX = ev.getX();
                        }
                        float newTranslationX = scrollSlidingTabStrip.getTranslationX();
                        if (scrollSlidingTabStrip.getScrollX() == 0 && newTranslationX == 0) {
                            if (!startedScroll && lastX - ev.getX() < 0) {
                                if (pager.beginFakeDrag()) {
                                    startedScroll = true;
                                    lastTranslateX = scrollSlidingTabStrip.getTranslationX();
                                }
                            } else if (startedScroll && lastX - ev.getX() > 0) {
                                if (pager.isFakeDragging()) {
                                    pager.endFakeDrag();
                                    startedScroll = false;
                                }
                            }
                        }
                        if (startedScroll) {
                            int dx = (int) (ev.getX() - lastX + newTranslationX - lastTranslateX);
                            try {
                                pager.fakeDragBy(dx);
                                lastTranslateX = newTranslationX;
                            } catch (Exception e) {
                                try {
                                    pager.endFakeDrag();
                                } catch (Exception e2) {
                                    //don't promt
                                }
                                startedScroll = false;
                            }
                        }
                        lastX = ev.getX();
                        if (ev.getAction() == MotionEvent.ACTION_CANCEL || ev.getAction() == MotionEvent.ACTION_UP) {
                            first = true;
                            if (startedScroll) {
                                pager.endFakeDrag();
                                startedScroll = false;
                            }
                        }
                        return startedScroll || super.onTouchEvent(ev);
                    }
                    return super.onTouchEvent(ev);
                }
            };
            scrollSlidingTabStrip.setUnderlineHeight(AndroidUtilities.dp(1));
            scrollSlidingTabStrip.setIndicatorColor(0xffe2e5e7);
            scrollSlidingTabStrip.setUnderlineColor(0xffe2e5e7);
            scrollSlidingTabStrip.setVisibility(INVISIBLE);
            addView(scrollSlidingTabStrip, new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, AndroidUtilities.dp(48), Gravity.LEFT | Gravity.TOP));
            scrollSlidingTabStrip.setTranslationX(displaySize.x);
            updateStickerTabs();
            scrollSlidingTabStrip.setDelegate(new ScrollSlidingTabStrip.ScrollSlidingTabStripDelegate() {
                @Override
                public void onPageSelected(int page) {
                    if (gifsGridView != null) {
                        if (page == gifTabBum + 1) {
                            if (gifsGridView.getVisibility() != VISIBLE) {
                                showGifTab();
                            }
                        } else {
                            if (gifsGridView.getVisibility() == VISIBLE) {
                                gifsGridView.setVisibility(GONE);
                                stickersGridView.setVisibility(VISIBLE);
                                stickersEmptyView.setVisibility(stickersGridAdapter.getCount() != 0 ? GONE : VISIBLE);
                            }
                        }
                    }
                    if (page == 0) {
                        pager.setCurrentItem(0);
                        return;
                    } else {
                        if (page == gifTabBum + 1) {
                            return;
                        } else {
                            if (page == recentTabBum + 1) {
                                views.get(6).setSelection(0);
                                return;
                            }
                        }
                    }
                    int index = page - 1 - stickersTabOffset;
                    if (index == stickerSets.size())
                        return;
                    if (index >= stickerSets.size())
                        index = stickerSets.size() - 1;
                    views.get(6).setSelection(stickersGridAdapter.getPositionForPack(stickerSets.get(index)));
                }
            });

            stickersGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    checkStickersScroll(firstVisibleItem);
                }
            });

            stickersGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    int count = view.getChildCount();
                    for (int a = 0; a < count; a++) {
                        View child = view.getChildAt(a);
                        if (child.getHeight() + child.getTop() < AndroidUtilities.dp(5)) {
                            firstVisibleItem++;
                        } else {
                            break;
                        }
                    }
                    scrollSlidingTabStrip.onPageScrolled(stickersGridAdapter.getTabForPosition(firstVisibleItem) + 1, 0);
                }
            });
        }

        setBackgroundColor(0xfff5f6f7);

        pager = new ViewPager(context) {
            @Override
            public boolean onInterceptTouchEvent(MotionEvent ev) {
                if (getParent() != null) {
                    getParent().requestDisallowInterceptTouchEvent(true);
                }
                return super.onInterceptTouchEvent(ev);
            }
        };
        pager.setAdapter(new EmojiPagesAdapter());

        pagerSlidingTabStripContainer = new LinearLayout(context) {
            @Override
            public boolean onInterceptTouchEvent(MotionEvent ev) {
                if (getParent() != null) {
                    getParent().requestDisallowInterceptTouchEvent(true);
                }
                return super.onInterceptTouchEvent(ev);
            }
        };
        pagerSlidingTabStripContainer.setOrientation(LinearLayout.HORIZONTAL);
        pagerSlidingTabStripContainer.setBackgroundColor(0xfff5f6f7);
        addView(pagerSlidingTabStripContainer, new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, AndroidUtilities.dp(48)));

        PagerSlidingTabStrip pagerSlidingTabStrip = new PagerSlidingTabStrip(context);
        pagerSlidingTabStrip.setViewPager(pager);
        pagerSlidingTabStrip.setShouldExpand(true);
        pagerSlidingTabStrip.setIndicatorHeight(AndroidUtilities.dp(2));
        pagerSlidingTabStrip.setUnderlineHeight(AndroidUtilities.dp(1));
        pagerSlidingTabStrip.setIndicatorColor(0xff2b96e2);
        pagerSlidingTabStrip.setUnderlineColor(0xffe2e5e7);
        pagerSlidingTabStripContainer.addView(pagerSlidingTabStrip, new LinearLayout.LayoutParams(0, AndroidUtilities.dp(48), 1.0f));
        pagerSlidingTabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                EmojiView.this.onPageScrolled(position, getMeasuredWidth(), positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        FrameLayout frameLayout = new FrameLayout(context);
        pagerSlidingTabStripContainer.addView(frameLayout, new LinearLayout.LayoutParams(AndroidUtilities.dp(52), AndroidUtilities.dp(48)));

        backspaceButton = new ImageView(context) {
            @Override
            public boolean onTouchEvent(MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    backspacePressed = true;
                    backspaceOnce = false;
                    postBackspaceRunnable(350);
                } else if (event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_UP) {
                    backspacePressed = false;
                    if (!backspaceOnce) {
                        if (EmojiView.this.listener != null && EmojiView.this.listener.onBackspace()) {
                            backspaceButton.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                        }
                    }
                }
                super.onTouchEvent(event);
                return true;
            }
        };
        backspaceButton.setClickable(true);
        backspaceButton.setScaleType(ImageView.ScaleType.CENTER);
        backspaceButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_emoji_backspace));
        frameLayout.addView(backspaceButton, new FrameLayout.LayoutParams(AndroidUtilities.dp(52), AndroidUtilities.dp(48)));

        View view = new View(context);
        view.setBackgroundColor(0xffe2e5e7);
        frameLayout.addView(view, new FrameLayout.LayoutParams(AndroidUtilities.dp(52), AndroidUtilities.dp(1), Gravity.LEFT | Gravity.BOTTOM));

        recentsWrap = new FrameLayout(context);
        recentsWrap.addView(views.get(0));

        TextView textView = new TextView(context);
        textView.setText(R.string.no_recent);
        textView.setTextSize(18);
        textView.setTextColor(0xff888888);
        textView.setGravity(Gravity.CENTER);
        recentsWrap.addView(textView);
        views.get(0).setEmptyView(textView);

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, Gravity.LEFT | Gravity.TOP);
        lp.setMargins(AndroidUtilities.dp(0), AndroidUtilities.dp(48), AndroidUtilities.dp(0), AndroidUtilities.dp(0));
        addView(pager, lp);

        loadRecents();

        if (Emoji.data[0] == null || Emoji.data[0].length == 0) {
            pager.setCurrentItem(1);
        }
    }
    private void checkStickersScroll(int firstVisibleItem) {
        if (stickersGridView == null) {
            return;
        }
        if (stickersGridView.getVisibility() != VISIBLE) {
            scrollSlidingTabStrip.onPageScrolled(gifTabBum + 1, (recentTabBum > 0 ? recentTabBum : stickersTabOffset) + 1);
            return;
        }
        int count = stickersGridView.getChildCount();
        for (int a = 0; a < count; a++) {
            View child = stickersGridView.getChildAt(a);
            if (child.getHeight() + child.getTop() < AndroidUtilities.dp(5)) {
                firstVisibleItem++;
            } else {
                break;
            }
        }
        scrollSlidingTabStrip.onPageScrolled(stickersGridAdapter.getTabForPosition(firstVisibleItem) + 1, (recentTabBum > 0 ? recentTabBum : stickersTabOffset) + 1);
    }
    private void showGifTab() {
        gifsGridView.setVisibility(VISIBLE);
        stickersGridView.setVisibility(GONE);
        stickersEmptyView.setVisibility(GONE);
        scrollSlidingTabStrip.onPageScrolled(gifTabBum + 1, (recentTabBum > 0 ? recentTabBum : stickersTabOffset) + 1);
    }
    private void onPageScrolled(int position, int width, int positionOffsetPixels) {
        if (scrollSlidingTabStrip == null) {
            return;
        }

        if (width == 0) {
            width = displaySize.x;
        }

        int margin = 0;
        if (position == 5) {
            margin = -positionOffsetPixels;
        } else if (position == 6) {
            margin = -width;
        }

        if (pagerSlidingTabStripContainer.getTranslationX() != margin) {
            pagerSlidingTabStripContainer.setTranslationX(margin);
            scrollSlidingTabStrip.setTranslationX(width + margin);
            scrollSlidingTabStrip.setVisibility(margin < 0 ? VISIBLE : INVISIBLE);
        }
    }

    private void postBackspaceRunnable(final int time) {
        postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!backspacePressed) {
                    return;
                }
                if (EmojiView.this.listener != null && EmojiView.this.listener.onBackspace()) {
                    backspaceButton.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                }
                backspaceOnce = true;
                postBackspaceRunnable(Math.max(50, time - 100));
            }
        }, time);
    }

    private void addToRecent(long code) {
        if (pager.getCurrentItem() == 0) {
            return;
        }
        ArrayList<Long> recent = new ArrayList<>();
        long[] currentRecent = Emoji.data[0];
        boolean was = false;
        for (long aCurrentRecent : currentRecent) {
            if (code == aCurrentRecent) {
                recent.add(0, code);
                was = true;
            } else {
                recent.add(aCurrentRecent);
            }
        }
        if (!was) {
            recent.add(0, code);
        }
        Emoji.data[0] = new long[Math.min(recent.size(), 50)];
        for (int q = 0; q < Emoji.data[0].length; q++) {
            Emoji.data[0][q] = recent.get(q);
        }
        adapters.get(0).data = Emoji.data[0];
        adapters.get(0).notifyDataSetChanged();
        saveRecents();
    }

    private String convert(long paramLong) {
        String str = "";
        for (int i = 0; ; i++) {
            if (i >= 4) {
                return str;
            }
            int j = (int) (0xFFFF & paramLong >> 16 * (3 - i));
            if (j != 0) {
                str = str + (char) j;
            }
        }
    }

    private void saveRecents() {
        ArrayList<Long> arrayList = new ArrayList<>(Emoji.data[0].length);
        for (int j = 0; j < Emoji.data[0].length; j++) {
            arrayList.add(Emoji.data[0][j]);
        }
        getContext().getSharedPreferences("emoji", 0).edit().putString("recents", TextUtils.join(",", arrayList)).commit();
    }

    private void saveRecentStickers() {
        SharedPreferences preferences = getContext().getSharedPreferences("emoji", Activity.MODE_PRIVATE);
        StringBuilder stringBuilder = new StringBuilder();
        for (HashMap.Entry<Integer, Integer> entry : stickersUseHistory.entrySet()) {
            if (stringBuilder.length() != 0) {
                stringBuilder.append(",");
            }
            stringBuilder.append(entry.getKey());
            stringBuilder.append("=");
            stringBuilder.append(entry.getValue());
        }
        preferences.edit().putString("stickers", stringBuilder.toString()).commit();
    }

    private void sortStickers() {
        if (StickersCache.getStickerSets().isEmpty()) {
            recentStickers.clear();
            return;
        }
        recentStickers.clear();
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        for (HashMap.Entry<Integer, Integer> entry : stickersUseHistory.entrySet()) {
            TdApi.Sticker sticker = StickersCache.getStickerByFileId(entry.getKey());
            if (sticker != null) {
                recentStickers.add(sticker);
                hashMap.put(sticker.sticker.id, entry.getValue());
            }
        }
        if (!stickersUseHistory.isEmpty()) {
            stickersUseHistory = hashMap;
            saveRecents();
        } else {
            stickersUseHistory = hashMap;
        }
        Collections.sort(recentStickers, new Comparator<TdApi.Sticker>() {
            @Override
            public int compare(TdApi.Sticker lhs, TdApi.Sticker rhs) {
                Integer count1 = stickersUseHistory.get(lhs.sticker.id);
                Integer count2 = stickersUseHistory.get(rhs.sticker.id);
                if (count1 == null) {
                    count1 = 0;
                }
                if (count2 == null) {
                    count2 = 0;
                }
                if (count1 > count2) {
                    return -1;
                } else if (count1 < count2) {
                    return 1;
                }
                return 0;
            }
        });
        while (recentStickers.size() > 20) {
            recentStickers.remove(recentStickers.size() - 1);
        }
    }
    private void updateStickerTabs() {
        recentTabBum = -2;
        gifTabBum = -2;

        stickersTabOffset = 0;
        int lastPosition = scrollSlidingTabStrip.getCurrentPosition();
        scrollSlidingTabStrip.removeTabs();
        scrollSlidingTabStrip.addIconTab(R.drawable.ic_smiles_smile);
        ArrayList<TdApi.Animation> anims = GifKeyboardCache.getAnimations();
        synchronized(anims)
        {
            if (showGifs && !anims.isEmpty()) {
                scrollSlidingTabStrip.addIconTab(R.drawable.ic_gif_gray);
                gifTabBum = stickersTabOffset;
                stickersTabOffset++;
            }
        }

        if (!recentStickers.isEmpty()) {
            recentTabBum = stickersTabOffset;
            stickersTabOffset++;
            scrollSlidingTabStrip.addIconTab(R.drawable.ic_smiles_recent);
        }

        stickerSets.clear();
        stickerSets.addAll(StickersCache.getStickerSets());
        for (int a = 0; a < stickerSets.size(); a++) {
            TdApi.StickerSet set = stickerSets.get(a);
            /*if ((pack.set.flags & 2) != 0 || pack.documents == null || pack.documents.isEmpty()) {
                continue;
            }*/
            if(set == null || set.stickers.length == 0)
                continue;
            scrollSlidingTabStrip.addStickerTab(set.stickers[0]);
        }
        scrollSlidingTabStrip.updateTabStyles();
        if (lastPosition != 0) {
            scrollSlidingTabStrip.onPageScrolled(lastPosition, lastPosition);
        }
        if (switchToGifTab) {
            if (gifTabBum >= 0 && gifsGridView.getVisibility() != VISIBLE) {
                showGifTab();
            }
            switchToGifTab = false;
        }
        if (gifTabBum == -2 && gifsGridView != null && gifsGridView.getVisibility() == VISIBLE) {
            gifsGridView.setVisibility(GONE);
            stickersGridView.setVisibility(VISIBLE);
            stickersEmptyView.setVisibility(stickersGridAdapter.getCount() != 0 ? GONE : VISIBLE);
        } else if (gifTabBum != -2) {
            if (gifsGridView != null && gifsGridView.getVisibility() == VISIBLE) {
                scrollSlidingTabStrip.onPageScrolled(gifTabBum + 1, (recentTabBum > 0 ? recentTabBum : stickersTabOffset) + 1);
            } else {
                scrollSlidingTabStrip.onPageScrolled(stickersGridAdapter.getTabForPosition(stickersGridView.getFirstVisiblePosition()) + 1, (recentTabBum > 0 ? recentTabBum : stickersTabOffset) + 1);
            }
        }
    }

    public void loadRecents() {
        SharedPreferences preferences = getContext().getSharedPreferences("emoji", Activity.MODE_PRIVATE);
        String str = preferences.getString("recents", "");
        try {
            if (str != null && str.length() > 0) {
                String[] args = str.split(",");
                Emoji.data[0] = new long[args.length];
                for (int i = 0; i < args.length; i++) {
                    Emoji.data[0][i] = Long.parseLong(args[i]);
                }
            } else {
                Emoji.data[0] = new long[]{0x00000000D83DDE02L, 0x00000000D83DDE18L, 0x0000000000002764L, 0x00000000D83DDE0DL, 0x00000000D83DDE0AL, 0x00000000D83DDE01L,
                        0x00000000D83DDC4DL, 0x000000000000263AL, 0x00000000D83DDE14L, 0x00000000D83DDE04L, 0x00000000D83DDE2DL, 0x00000000D83DDC8BL,
                        0x00000000D83DDE12L, 0x00000000D83DDE33L, 0x00000000D83DDE1CL, 0x00000000D83DDE48L, 0x00000000D83DDE09L, 0x00000000D83DDE03L,
                        0x00000000D83DDE22L, 0x00000000D83DDE1DL, 0x00000000D83DDE31L, 0x00000000D83DDE21L, 0x00000000D83DDE0FL, 0x00000000D83DDE1EL,
                        0x00000000D83DDE05L, 0x00000000D83DDE1AL, 0x00000000D83DDE4AL, 0x00000000D83DDE0CL, 0x00000000D83DDE00L, 0x00000000D83DDE0BL,
                        0x00000000D83DDE06L, 0x00000000D83DDC4CL, 0x00000000D83DDE10L, 0x00000000D83DDE15L};
            }
            adapters.get(0).data = Emoji.data[0];
            adapters.get(0).notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (showStickers) {
            try {
                stickersUseHistory.clear();
                str = preferences.getString("stickers", "");
                if (str != null && str.length() > 0) {
                    String[] args = str.split(",");
                    for (String arg : args) {
                        String[] args2 = arg.split("=");
                        stickersUseHistory.put(Integer.parseInt(args2[0]), Integer.parseInt(args2[1]));
                    }
                }
                sortStickers();
                updateStickerTabs();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) pagerSlidingTabStripContainer.getLayoutParams();
        FrameLayout.LayoutParams layoutParams1 = null;
        layoutParams.width = View.MeasureSpec.getSize(widthMeasureSpec);
        if (scrollSlidingTabStrip != null) {
            layoutParams1 = (FrameLayout.LayoutParams) scrollSlidingTabStrip.getLayoutParams();
            if (layoutParams1 != null) {
                layoutParams1.width = layoutParams.width;
            }
        }
        if (layoutParams.width != oldWidth) {
            if (scrollSlidingTabStrip != null && layoutParams1 != null) {
                onPageScrolled(pager.getCurrentItem(), layoutParams.width, 0);
                scrollSlidingTabStrip.setLayoutParams(layoutParams1);
            }
            pagerSlidingTabStripContainer.setLayoutParams(layoutParams);
            oldWidth = layoutParams.width;
        }
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(layoutParams.width, MeasureSpec.EXACTLY), View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(heightMeasureSpec), MeasureSpec.EXACTLY));
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if (lastNotifyWidth != right - left) {
            lastNotifyWidth = right - left;
            if (stickersGridAdapter != null) {
                stickersGridAdapter.notifyDataSetChanged();
            }
        }
        super.onLayout(changed, left, top, right, bottom);
    }

    public void setListener(Listener value) {
        listener = value;
    }

    public void invalidateViews() {
        for (GridView gridView : views) {
            if (gridView != null) {
                gridView.invalidateViews();
            }
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (stickersGridAdapter != null) {
            NotificationCenter.getInstance().addObserver(stickersLoadObserver);
            NotificationCenter.getInstance().addObserver(stickersUpdateObserver);
            NotificationCenter.getInstance().addObserver(gifKeyboardLoadObserver);
            NotificationCenter.getInstance().addObserver(gifKeyboardUpdateObserver);
        }
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility != GONE && stickersGridAdapter != null) {
            NotificationCenter.getInstance().addObserver(stickersLoadObserver);
            NotificationCenter.getInstance().addObserver(stickersUpdateObserver);
            NotificationCenter.getInstance().addObserver(gifKeyboardLoadObserver);
            NotificationCenter.getInstance().addObserver(gifKeyboardUpdateObserver);
            sortStickers();
            updateStickerTabs();
            stickersGridAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (stickersGridAdapter != null) {
            NotificationCenter.getInstance().removeObserver(stickersLoadObserver);
            NotificationCenter.getInstance().removeObserver(stickersUpdateObserver);
            NotificationCenter.getInstance().removeObserver(gifKeyboardLoadObserver);
            NotificationCenter.getInstance().removeObserver(gifKeyboardUpdateObserver);
        }
    }

    private class StickersGridAdapter extends BaseAdapter {

        private Context context;
        private int stickersPerRow;
        private HashMap<Integer, TdApi.StickerSet> rowStartPack = new HashMap<>();
        private HashMap<TdApi.StickerSet, Integer> packStartRow = new HashMap<>();
        private HashMap<Integer, TdApi.Sticker> cache = new HashMap<>();
        private int totalItems;

        public StickersGridAdapter(Context context) {
            this.context = context;
        }

        public int getCount() {
            return totalItems != 0 ? totalItems + 1 : 0;
        }

        public Object getItem(int i) {
            return cache.get(i);
        }

        public long getItemId(int i) {
            return NO_ID;
        }

        public int getPositionForPack(TdApi.StickerSet stickerSet) {
            return packStartRow.get(stickerSet) * stickersPerRow;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return cache.get(position) != null;
        }

        @Override
        public int getItemViewType(int position) {
            if (cache.get(position) != null) {
                return 0;
            }
            return 1;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        public int getTabForPosition(int position) {
            if (stickersPerRow == 0) {
                int width = getMeasuredWidth();
                if (width == 0) {
                    width = displaySize.x;
                }
                stickersPerRow = width / AndroidUtilities.dp(72);
            }
            int row = position / stickersPerRow;
            TdApi.StickerSet pack = rowStartPack.get(row);
            if (pack == null) {
                return recentTabBum;
            }
            return stickerSets.indexOf(pack) + stickersTabOffset;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            TdApi.Sticker sticker = cache.get(i);
            if (sticker != null) {
                if (view == null) {
                    view = new StickerEmojiView(context) {
                        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
                            super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(AndroidUtilities.dp(82), MeasureSpec.EXACTLY));
                        }
                    };
                    view.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (listener != null) {
                                if(BuildConfig.DEBUG)
                                    Log.d("EmojiView", "sending sticker");
                                TdApi.Sticker sticker = ((StickerEmojiView) v).getAttachedSticker();
                                Integer count = stickersUseHistory.get(sticker.sticker.id);
                                if (count == null) {
                                    count = 0;
                                }
                                if (count == 0 && stickersUseHistory.size() > 19) {
                                    for (int a = recentStickers.size() - 1; a >= 0; a--) {
                                        TdApi.Sticker recentSticker = recentStickers.get(a);
                                        stickersUseHistory.remove(recentSticker.sticker.id);
                                        recentStickers.remove(a);
                                        if (stickersUseHistory.size() <= 19) {
                                            break;
                                        }
                                    }
                                }
                                stickersUseHistory.put(sticker.sticker.id, ++count);
                                saveRecentStickers();
                                listener.onStickerSelected(sticker);
                                StickersCache.didStickerSent(sticker);
                            }
                        }
                    });
                }
                ((StickerEmojiView) view).setSticker(sticker, false);
            } else {
                if (view == null) {
                    view = new DummyFrameLayout(context);
                }
                if (i == totalItems) {
                    int row = (i - 1) / stickersPerRow;
                    TdApi.StickerSet pack = rowStartPack.get(row);
                    if (pack == null) {
                        ((DummyFrameLayout) view).setHeight(1);
                    } else {
                        int height = pager.getHeight() - (int) Math.ceil(pack.stickers.length / (float) stickersPerRow) * AndroidUtilities.dp(82);
                        ((DummyFrameLayout) view).setHeight(height > 0 ? height : 1);
                    }
                } else {
                    ((DummyFrameLayout) view).setHeight(AndroidUtilities.dp(82));
                }
            }
            return view;
        }

        @Override
        public void notifyDataSetChanged() {
            int width = getMeasuredWidth();
            if (width == 0) {
                width = displaySize.x;
            }
            stickersPerRow = width / AndroidUtilities.dp(72);
            rowStartPack.clear();
            packStartRow.clear();
            cache.clear();
            totalItems = 0;
            ArrayList<TdApi.StickerSet> packs = stickerSets;
            for (int a = -1; a < packs.size(); a++) {
                ArrayList<TdApi.Sticker> stickers;
                TdApi.StickerSet set = null;
                int startRow = totalItems / stickersPerRow;
                if (a == -1) {
                    stickers = recentStickers;
                } else {
                    set = packs.get(a);
                    stickers = new ArrayList<TdApi.Sticker>(Arrays.asList(set.stickers));
                    packStartRow.put(set, startRow);
                }
                if (stickers.isEmpty()) {
                    continue;
                }
                int count = (int) Math.ceil(stickers.size() / (float) stickersPerRow);
                for (int b = 0; b < stickers.size(); b++) {
                    cache.put(b + totalItems, stickers.get(b));
                }
                totalItems += count * stickersPerRow;
                for (int b = 0; b < count; b++) {
                    rowStartPack.put(startRow + b, set);
                }
            }
            super.notifyDataSetChanged();
        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (observer != null) {
                super.unregisterDataSetObserver(observer);
            }
        }
    }

    private class EmojiGridAdapter extends BaseAdapter {
        long[] data;

        public EmojiGridAdapter(long[] arg2) {
            this.data = arg2;
        }

        public int getCount() {
            return data.length;
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return data[i];
        }

        public View getView(int i, View view, ViewGroup paramViewGroup) {
            ImageView imageView = (ImageView)view;
            if (imageView == null) {
                imageView = new ImageView(EmojiView.this.getContext()) {
                    public void onMeasure(int paramAnonymousInt1, int paramAnonymousInt2) {
                        setMeasuredDimension(View.MeasureSpec.getSize(paramAnonymousInt1), View.MeasureSpec.getSize(paramAnonymousInt1));
                    }
                };
                imageView.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (EmojiView.this.listener != null) {
                            EmojiView.this.listener.onEmojiSelected(EmojiView.this.convert((Long)view.getTag()));
                        }
                        EmojiView.this.addToRecent((Long)view.getTag());
                    }
                });
                imageView.setBackgroundResource(R.drawable.list_selector);
                imageView.setScaleType(ImageView.ScaleType.CENTER);
            }
            imageView.setImageDrawable(Emoji.getEmojiBigDrawable(data[i]));
            imageView.setTag(data[i]);
            return imageView;
        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (observer != null) {
                super.unregisterDataSetObserver(observer);
            }
        }
    }


    private class EmojiPagesAdapter extends PagerAdapter implements PagerSlidingTabStrip.IconTabProvider {

        public void destroyItem(ViewGroup viewGroup, int position, Object object) {
            View view;
            if (position == 0) {
                view = recentsWrap;
            } else if (position == 6) {
                view = stickersWrap;
            } else {
                view = views.get(position);
            }
            viewGroup.removeView(view);
        }

        public int getCount() {
            return views.size();
        }

        public int getPageIconResId(int paramInt) {
            return icons[paramInt];
        }

        public Object instantiateItem(ViewGroup viewGroup, int position) {
            View view;
            if (position == 0) {
                view = recentsWrap;
            } else if (position == 6) {
                view = stickersWrap;
            } else {
                view = views.get(position);
            }
            viewGroup.addView(view);
            return view;
        }

        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (observer != null) {
                super.unregisterDataSetObserver(observer);
            }
        }
    }

    public interface EmojiKeyboardCallback
    {
        public void toggleEmojiKeyboard();
        public void closeKeyboard();
        public boolean isEmojiKeyboardShown();
    }

    private class GifsAdapter extends RecyclerView.Adapter {

        private class Holder extends RecyclerView.ViewHolder {

            public Holder(View itemView) {
                super(itemView);
                itemView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (getAdapterPosition() < 0)
                            return;
                        try {
                            ArrayList<TdApi.Animation> anims = GifKeyboardCache.getAnimations();
                            synchronized (anims) {
                                TdApi.Animation anim = anims.get(getAdapterPosition());
                                listener.onGifSelected(anim);
                            }
                        } catch (Exception ignored) {
                        }
                    }
                });
                itemView.setOnLongClickListener(new OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        if(getAdapterPosition() < 0)
                            return false;
                        try {
                            ArrayList<TdApi.Animation> anims = GifKeyboardCache.getAnimations();
                            synchronized (anims) {
                                final TdApi.Animation anim = anims.get(getAdapterPosition());
                                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                                builder.setTitle(R.string.action_confirmation);
                                builder.setMessage(R.string.delete_gif);
                                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        GifKeyboardCache.deleteSavedAnimation(anim);
                                    }
                                });
                                builder.setNegativeButton(R.string.no, null);
                                builder.show();
                            }
                        } catch (Exception ignored) {
                        }
                        return true;
                    }
                });
            }
        }

        private Context mContext;

        public GifsAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getItemCount() {
            ArrayList<TdApi.Animation> anims = GifKeyboardCache.getAnimations();
            synchronized(anims)
            {
                return anims.size();
            }
        }
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            AnimationView view = new AnimationView(mContext);
            try {
                ArrayList<TdApi.Animation> anims = GifKeyboardCache.getAnimations();
                synchronized(anims)
                {
                    TdApi.Animation anim = anims.get(i);
                    view.setLayoutParams(new RecyclerView.LayoutParams(anim.width, anim.height));
                }
            }
            catch(Exception ignored) {}
            return new Holder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
            try {
                ArrayList<TdApi.Animation> anims = GifKeyboardCache.getAnimations();
                synchronized(anims)
                {
                    TdApi.Animation anim = anims.get(i);
                    String key = CommonTools.makeFileKey(anim.animation.id);
                    TelegramImageLoader.getInstance().loadAnimation(anim, key, (AnimationView) viewHolder.itemView);
                }
            }
            catch(Exception e) { e.printStackTrace(); }
        }
    }
}
