package com.andremacareno.tdtest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.images.RecyclingImageView;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.AdditionalActionForward;
import com.andremacareno.tdtest.models.AdditionalActionForwardChatMessages;
import com.andremacareno.tdtest.models.AdditionalActionForwardSharedMedia;
import com.andremacareno.tdtest.models.AdditionalActionReply;
import com.andremacareno.tdtest.models.AdditionalActionSendContact;
import com.andremacareno.tdtest.models.AdditionalSendAction;
import com.andremacareno.tdtest.models.AudioTrackModel;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.MessageModel;
import com.andremacareno.tdtest.models.PhotoMessageModel;
import com.andremacareno.tdtest.models.SharedImageModel;
import com.andremacareno.tdtest.models.SharedMedia;
import com.andremacareno.tdtest.models.StickerMessageModel;
import com.andremacareno.tdtest.models.VideoMessageModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 23/08/15.
 */
public class AdditionalSendView extends LinearLayout {
    private TextView title;
    private TextView subtitle;
    private ImageButton cancelButton;
    private RecyclingImageView thumb;
    public AdditionalSendView(Context context) {
        super(context);
        init();
    }

    public AdditionalSendView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.shared_attachment, this);
        setOrientation(HORIZONTAL);
        thumb = (RecyclingImageView) v.findViewById(R.id.reply_thumb);
        title = (TextView) v.findViewById(R.id.title);
        subtitle = (TextView) v.findViewById(R.id.subtitle);
        cancelButton = (ImageButton) v.findViewById(R.id.detach);
    }
    public void attachAction(AdditionalSendAction act)
    {
        if(act == null)
            return;
        if(act.getDataType() == AdditionalSendAction.DataType.FWD)
        {
            int fwdCount = ((AdditionalActionForward) act).getForwardedCount();
            title.setVisibility(View.VISIBLE);
            subtitle.setVisibility(View.VISIBLE);
            thumb.setVisibility(View.GONE);
            cancelButton.setVisibility(View.GONE);
            String strTitle;
            String secondLine = "";
            if(((AdditionalActionForward) act).fromSharedMedia()) {
                strTitle = getResources().getString(R.string.shared_media);
                if(fwdCount == 1)
                {
                    SharedMedia media = ((AdditionalActionForwardSharedMedia) act).getMessages().get(0);
                    if(!media.isDummy()){
                        if(media instanceof AudioTrackModel)
                            secondLine = getResources().getString(R.string.audio);
                        else
                            secondLine = ((SharedImageModel) media).isVideo() ? getResources().getString(R.string.video) : getResources().getString(R.string.photo);
                    }
                    else
                        secondLine = "";
                }
            }
            else
            {
                AdditionalActionForwardChatMessages cast = (AdditionalActionForwardChatMessages) act;
                strTitle = cast.getFwdInfoTitle();
                if(fwdCount == 1)
                    secondLine = cast.getMessages().get(0).getTextRepresentation();
            }
            title.setText(strTitle);
            if(fwdCount == 1)
                subtitle.setText(secondLine);
            else
                subtitle.setText(String.format(getResources().getString(R.string.n_forwarded_messages), fwdCount));
        }
        else if(act.getDataType() == AdditionalSendAction.DataType.SEND_CONTACT)
        {
            TdApi.InputMessageContact contact = ((AdditionalActionSendContact) act).getShareableContact();
            String name = contact.lastName == null ? contact.firstName : String.format("%s %s", contact.firstName, contact.lastName);
            title.setVisibility(View.VISIBLE);
            subtitle.setVisibility(View.VISIBLE);
            thumb.setVisibility(View.GONE);
            cancelButton.setVisibility(View.VISIBLE);
            title.setText(R.string.contact);
            subtitle.setText(name);
        }
        else if(act.getDataType() == AdditionalSendAction.DataType.REPLY)
        {
            MessageModel msg = ((AdditionalActionReply) act).getReplyTo();
            FileModel thumbFile;
            if(msg.getSuitableConstructor() == TdApi.MessagePhoto.CONSTRUCTOR)
                thumbFile = ((PhotoMessageModel) msg).getPreview().getPhoto();
            else if(msg.getSuitableConstructor() == TdApi.MessageVideo.CONSTRUCTOR)
                thumbFile = ((VideoMessageModel) msg).getThumb().getPhoto();
            else if(msg.getSuitableConstructor() == TdApi.MessageSticker.CONSTRUCTOR)
                thumbFile = FileCache.getInstance().getFileModel(((StickerMessageModel) msg).getSticker().thumb.photo);
            else
                thumbFile = null;
            if(thumbFile != null)
            {
                thumb.setVisibility(View.VISIBLE);
                TelegramImageLoader.getInstance().loadImage(thumbFile, String.format("reply_msg_%d_file_%d_preview", msg.getMessageId(), thumbFile.getFileId()), thumb, TelegramImageLoader.POST_PROCESSING_SCALE, AndroidUtilities.dp(38), AndroidUtilities.dp(38), false);
            }
            else
                thumb.setVisibility(View.GONE);
            title.setVisibility(View.VISIBLE);
            subtitle.setVisibility(View.VISIBLE);
            cancelButton.setVisibility(View.VISIBLE);
            title.setText(msg.getFromUsername());
            subtitle.setText(msg.getTextRepresentation());
        }
    }
    public ImageButton getCancelButton() { return this.cancelButton; }

}
