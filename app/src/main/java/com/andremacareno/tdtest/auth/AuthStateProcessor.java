package com.andremacareno.tdtest.auth;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.andremacareno.tdtest.LocalConfig;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.models.CurrentUserModel;
import com.andremacareno.tdtest.models.UserModel;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 03/04/15.
 */
public class AuthStateProcessor {
    private static final int RECEIVING_AUTH_STATE = 0;
    private static final int RECEIVING_CURRENT_USER_INIT_NOTIFICATION = 1;
    private static final int FIRST_GETAUTHSTATE_TIMEOUT = 250;
    public interface AuthStateDelegate
    {
        public void onLoggingOut(TdApi.AuthStateLoggingOut state);
        public void didSetPhoneRequested(TdApi.AuthStateWaitPhoneNumber state);
        public void didSetCodeRequested(TdApi.AuthStateWaitCode state);
        public void didSetNameRequested(TdApi.AuthStateWaitName state);
        public void didSetPasswordRequested(TdApi.AuthStateWaitPassword state);
        public void didSuccessfullyAuthorized(TdApi.AuthStateOk state);            //at this moment CurrentUserModel are ready to use.
        public void didErrorOccurred();                                            //returned Error
    }
    public interface SecondaryAuthDelegate
    {
        public String getPhoneNumber();
        public void processError();
    }
    private AuthStateDelegate delegate;
    private PauseHandler updBridge;
    private static Handler getAuthStateHandler = new Handler(Looper.getMainLooper());
    private long getAuthStateTimeout = FIRST_GETAUTHSTATE_TIMEOUT;

    private static volatile AuthStateProcessor _instance;
    public static AuthStateProcessor getInstance()
    {
        if(_instance == null)
        {
            synchronized(AuthStateProcessor.class)
            {
                if(_instance == null)
                    _instance = new AuthStateProcessor();
            }
        }
        return _instance;
    }
    public void setDelegate(AuthStateDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    private AuthStateProcessor() {
        updBridge = new UpdateBridge(this);
    }
    public void pauseHandler()
    {
        updBridge.pause();
    }
    public void resumeHandler()
    {
        updBridge.resume();
    }
    public void process(TdApi.AuthState authState)
    {
        if(delegate == null)
            return;
        if(authState.getConstructor() != TdApi.AuthStateLoggingOut.CONSTRUCTOR) {
            getAuthStateHandler.removeCallbacksAndMessages(null);
            getAuthStateTimeout = FIRST_GETAUTHSTATE_TIMEOUT;
        }
        if(authState.getConstructor() == TdApi.AuthStateWaitPhoneNumber.CONSTRUCTOR)
            delegate.didSetPhoneRequested((TdApi.AuthStateWaitPhoneNumber) authState);
        else if(authState.getConstructor() == TdApi.AuthStateWaitName.CONSTRUCTOR)
            delegate.didSetNameRequested((TdApi.AuthStateWaitName) authState);
        else if(authState.getConstructor() == TdApi.AuthStateWaitCode.CONSTRUCTOR)
            delegate.didSetCodeRequested((TdApi.AuthStateWaitCode) authState);
        else if(authState.getConstructor() == TdApi.AuthStateWaitPassword.CONSTRUCTOR)
            delegate.didSetPasswordRequested((TdApi.AuthStateWaitPassword) authState);
        else if(authState.getConstructor() == TdApi.AuthStateOk.CONSTRUCTOR) {
            TdApi.GetNotificationSettings requestGlobalNotificationSettings = new TdApi.GetNotificationSettings(new TdApi.NotificationSettingsForAllChats());
            TG.getClientInstance().send(requestGlobalNotificationSettings, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    if(object != null && object.getConstructor() == TdApi.NotificationSettings.CONSTRUCTOR)
                    {
                        TdApi.NotificationSettings globalSettings = (TdApi.NotificationSettings) object;
                        LocalConfig.getInstance().setChatsMutedByDefault(globalSettings.muteFor > 0);
                    }
                    TdApi.GetMe getMe = new TdApi.GetMe();
                    TG.getClientInstance().send(getMe, new Client.ResultHandler() {
                        @Override
                        public void onResult(TdApi.TLObject object) {
                            if (object.getConstructor() != TdApi.User.CONSTRUCTOR)
                                return;
                            UserModel me = new UserModel((TdApi.User) object);
                            CurrentUserModel.createInstance(me);
                            Message msg = Message.obtain(updBridge);
                            msg.what = RECEIVING_CURRENT_USER_INIT_NOTIFICATION;
                            msg.sendToTarget();
                        }
                    });
                }});
        }
        else if(authState.getConstructor() == TdApi.AuthStateLoggingOut.CONSTRUCTOR)
        {
            if(getAuthStateTimeout == FIRST_GETAUTHSTATE_TIMEOUT)
                delegate.onLoggingOut((TdApi.AuthStateLoggingOut) authState);
            getAuthStateHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    getAuthState();
                }
            }, getAuthStateTimeout);
            if(getAuthStateTimeout < 8000)
                getAuthStateTimeout *= 2;
        }
    }
    public void changeAuthState(TdApi.TLFunction changeStateReq)
    {
        TG.getClientInstance().send(changeStateReq, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                Message msg = Message.obtain(updBridge);
                msg.obj = object;
                msg.what = RECEIVING_AUTH_STATE;
                msg.sendToTarget();
            }
        });
    }
    public void getAuthState()
    {
        TG.getClientInstance().send(new TdApi.GetAuthState(), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                Message msg = Message.obtain(updBridge);
                msg.obj = object;
                msg.what = RECEIVING_AUTH_STATE;
                msg.sendToTarget();
            }
        });
    }
    private static class UpdateBridge extends PauseHandler
    {
        private WeakReference<AuthStateProcessor> processorRef;
        public UpdateBridge(AuthStateProcessor processor)
        {
            this.processorRef = new WeakReference<AuthStateProcessor>(processor);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }
        @Override
        protected boolean storeOnlyLastMessage() {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(processorRef.get() != null) {
                if(message.what == RECEIVING_AUTH_STATE)
                {
                    if(message.obj instanceof TdApi.Error)
                        processorRef.get().delegate.didErrorOccurred();
                    else
                        processorRef.get().process((TdApi.AuthState) message.obj);
                }
                else if(message.what == RECEIVING_CURRENT_USER_INIT_NOTIFICATION)
                    processorRef.get().delegate.didSuccessfullyAuthorized(new TdApi.AuthStateOk());
            }
        }
    }
}
