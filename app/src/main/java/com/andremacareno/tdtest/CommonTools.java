package com.andremacareno.tdtest;

import android.util.DisplayMetrics;
import android.util.Log;

import com.andremacareno.tdtest.models.CurrentUserModel;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.utils.phoneformat.PhoneFormat;
import com.andremacareno.tdtest.viewcontrollers.passcode.PasscodeGestureController;
import com.andremacareno.tdtest.viewcontrollers.passcode.PasscodePasswordController;
import com.andremacareno.tdtest.viewcontrollers.passcode.PasscodePatternController;
import com.andremacareno.tdtest.viewcontrollers.passcode.PasscodePinController;
import com.andremacareno.tdtest.viewcontrollers.passcode.PasscodeViewController;
import com.andremacareno.tdtest.views.PasscodeView;

import org.drinkless.td.libcore.telegram.TdApi;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by andremacareno on 04/04/15.
 */
public class CommonTools {
    private static final SimpleDateFormat hhmm = new SimpleDateFormat("h:mm a", Locale.US);
    private static final SimpleDateFormat dmmm = new SimpleDateFormat("d MMM", Locale.US);
    private static final SimpleDateFormat dmmmyyyy = new SimpleDateFormat("d MMM yyyy", Locale.US);
    private static final SimpleDateFormat mmmmd = new SimpleDateFormat("MMMM d", Locale.US);
    private static final SimpleDateFormat mmmmyyyy = new SimpleDateFormat("MMMM yyyy", Locale.US);
    private static Boolean lowDpi = null;
    private static String[] statusText = new String[] {
            TelegramApplication.sharedApplication().getResources().getString(R.string.online),
            TelegramApplication.sharedApplication().getResources().getString(R.string.offline),
            TelegramApplication.sharedApplication().getResources().getString(R.string.offline_unknown)
    };
    public static String dateToString(long seconds, boolean showYear)
    {
        //returns HH:MM, "yesterday" or DD MMM
        long millis = seconds * 1000;
        Calendar today = Calendar.getInstance(); // today
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(millis);
        if(today.get(Calendar.YEAR) == date.get(Calendar.YEAR) && today.get(Calendar.DAY_OF_YEAR) == date.get(Calendar.DAY_OF_YEAR))
            return hhmm.format(new Date(millis));
        today.add(Calendar.DAY_OF_YEAR, -1); // yesterday
        if (today.get(Calendar.YEAR) == date.get(Calendar.YEAR)
                && today.get(Calendar.DAY_OF_YEAR) == date.get(Calendar.DAY_OF_YEAR)) {
            return TelegramApplication.sharedApplication().getResources().getString(R.string.yesterday);
        }
        if(showYear == false)
            return dmmm.format(new Date(millis));
        return dmmmyyyy.format(new Date(millis));
    }
    public static String dateToTimeString(long seconds)
    {
        return hhmm.format(new Date(seconds*1000));
    }
    public static boolean isLowDPIScreen() {
        if(lowDpi != null)
            return lowDpi;
        DisplayMetrics metrics = TelegramApplication.sharedApplication().getResources().getDisplayMetrics();
        lowDpi = metrics.densityDpi == DisplayMetrics.DENSITY_LOW || metrics.densityDpi == DisplayMetrics.DENSITY_MEDIUM || metrics.densityDpi == DisplayMetrics.DENSITY_HIGH;
        return lowDpi;
    }


    public static boolean isDeletedUser(TdApi.User user)
    {
        //deleted user have only id
        if(user.type.getConstructor() == TdApi.UserTypeUnknown.CONSTRUCTOR || user.type.getConstructor() == TdApi.UserTypeDeleted.CONSTRUCTOR)
            return true;
        if(user.firstName == null || user.firstName.length() == 0)
            return true;
        return false;
    }
    public static boolean isChatServiceMessage(int constructor)
    {
        if(constructor == TdApi.MessageGroupChatCreate.CONSTRUCTOR)
            return true;
        if(constructor == TdApi.MessageChatJoinByLink.CONSTRUCTOR)
            return true;
        if(constructor == TdApi.MessageChatAddParticipants.CONSTRUCTOR)
            return true;
        if(constructor == TdApi.MessageChatChangePhoto.CONSTRUCTOR)
            return true;
        if(constructor == TdApi.MessageChatChangeTitle.CONSTRUCTOR)
            return true;
        if(constructor == TdApi.MessageChatDeleteParticipant.CONSTRUCTOR)
            return true;
        if(constructor == TdApi.MessageChatDeletePhoto.CONSTRUCTOR)
            return true;
        if(constructor == TdApi.MessageChannelChatCreate.CONSTRUCTOR)
            return true;
        if(constructor == TdApi.MessageChatMigrateFrom.CONSTRUCTOR)
            return true;
        if(constructor == TdApi.MessageChatMigrateTo.CONSTRUCTOR)
            return true;
        return false;
    }
    public static boolean isNonDownloadedFile(TdApi.File file)
    {
        return file.path == null || file.path.length() == 0;
    }
    public static String getInitialsByChatInfo(TdApi.Chat chat)
    {
        switch(chat.type.getConstructor())
        {
            case TdApi.PrivateChatInfo.CONSTRUCTOR:
                TdApi.User u = ((TdApi.PrivateChatInfo) chat.type).user;
                if(u.firstName.length() == 0 && u.lastName.length() == 0)
                    return " ";
                else if(u.firstName.length() > 0 && u.lastName.length() == 0)
                    return u.firstName.substring(0, 1).toUpperCase();
                else if(u.lastName.length() > 0 && u.firstName.length() == 0)
                    return u.lastName.substring(0, 1).toUpperCase();
                return String.format("%s%s", u.firstName.substring(0, 1).toUpperCase(), u.lastName.substring(0, 1).toUpperCase());
            case TdApi.GroupChatInfo.CONSTRUCTOR:
            case TdApi.ChannelChatInfo.CONSTRUCTOR:
                return chat.title.substring(0, 1).toUpperCase();
            default:
                return "U";
        }
    }
    public static String getInitialsFromUserName(String firstName, String lastName)
    {
        if(firstName.length() == 0 && lastName.length() == 0)
            return "NN";
        else if(firstName.length() > 0 && lastName.length() == 0)
            return firstName.substring(0, 1).toUpperCase();
        else if(firstName.length() == 0 && lastName.length() > 0)
            return lastName.substring(0, 1).toUpperCase();
        return String.format("%s%s", firstName.substring(0, 1).toUpperCase(), lastName.substring(0, 1).toUpperCase());
    }
    public static boolean needToProcessMessageInBackground(TdApi.Message msg, boolean fromGroupChat)
    {
        //additional check to prevent app crash on updating popular chats
        boolean userCached;
        if(msg != null && msg.fromId == 0)
            return false;
        switch(msg.content.getConstructor())
        {
            case TdApi.MessageText.CONSTRUCTOR:
            case TdApi.MessageAudio.CONSTRUCTOR:
            case TdApi.MessageVoice.CONSTRUCTOR:
            case TdApi.MessageDocument.CONSTRUCTOR:
            case TdApi.MessagePhoto.CONSTRUCTOR:
            case TdApi.MessageLocation.CONSTRUCTOR:
            case TdApi.MessageContact.CONSTRUCTOR:
            case TdApi.MessageDeleted.CONSTRUCTOR:
            case TdApi.MessageVideo.CONSTRUCTOR:
            case TdApi.MessageAnimation.CONSTRUCTOR:
            case TdApi.MessageSticker.CONSTRUCTOR:
            case TdApi.MessageWebPage.CONSTRUCTOR:
            case TdApi.MessageVenue.CONSTRUCTOR:
                if(fromGroupChat)
                    return !UserCache.getInstance().isCached(msg.fromId);
                return false;
            case TdApi.MessageGroupChatCreate.CONSTRUCTOR:
                boolean chatCreatorCached = UserCache.getInstance().isCached(msg.fromId);
                return !chatCreatorCached;
            case TdApi.MessageChatAddParticipants.CONSTRUCTOR:
                boolean inviterCached = UserCache.getInstance().isCached(msg.fromId);
                boolean invitedCached = UserCache.getInstance().isCached(((TdApi.MessageChatAddParticipants)msg.content).participants);
                return !(inviterCached && invitedCached);
            case TdApi.MessageChatChangePhoto.CONSTRUCTOR:
                userCached = UserCache.getInstance().isCached(msg.fromId);
                return !userCached;
            case TdApi.MessageChatChangeTitle.CONSTRUCTOR:
                userCached = UserCache.getInstance().isCached(msg.fromId);
                return !userCached;
            case TdApi.MessageChatDeleteParticipant.CONSTRUCTOR:
                boolean kickerCached = UserCache.getInstance().isCached(msg.fromId);
                boolean kickedCached = UserCache.getInstance().isCached(((TdApi.MessageChatDeleteParticipant) msg.content).user.id);
                return !(kickerCached && kickedCached);
            case TdApi.MessageChatDeletePhoto.CONSTRUCTOR:
                userCached = UserCache.getInstance().isCached(msg.fromId);
                return !userCached;
            case TdApi.MessageChatJoinByLink.CONSTRUCTOR:
                userCached = UserCache.getInstance().isCached(msg.fromId);
                return !userCached;
            case TdApi.MessageChatMigrateFrom.CONSTRUCTOR:
            case TdApi.MessageChatMigrateTo.CONSTRUCTOR:
                return false;
            default:
                if(fromGroupChat)
                    return !UserCache.getInstance().isCached(msg.fromId);
                return false;
        }
    }
    public static String messageToText(TdApi.Message msg)
    {
        if(BuildConfig.DEBUG)
            Log.d("CommonTools", "messageToText");
        String fmt;
        UserModel u;
        String username;
        TdApi.Chat chat = ChatCache.getInstance().getChatById(msg.chatId);
        switch(msg.content.getConstructor())
        {
            case TdApi.MessageText.CONSTRUCTOR:
                //text message
                return ((TdApi.MessageText) msg.content).text;
            case TdApi.MessageAudio.CONSTRUCTOR:
                //audio file
                return TelegramApplication.sharedApplication().getResources().getString(R.string.audio);
            case TdApi.MessageVoice.CONSTRUCTOR:
                return TelegramApplication.sharedApplication().getResources().getString(R.string.voice);
            case TdApi.MessageDocument.CONSTRUCTOR:
                //document
                return TelegramApplication.sharedApplication().getResources().getString(R.string.document);
            case TdApi.MessagePhoto.CONSTRUCTOR:
                //photo
                return TelegramApplication.sharedApplication().getResources().getString(R.string.photo);
            case TdApi.MessageLocation.CONSTRUCTOR:
                //geopoint
                return TelegramApplication.sharedApplication().getResources().getString(R.string.geopoint);
            case TdApi.MessageContact.CONSTRUCTOR:
                //contact
                return TelegramApplication.sharedApplication().getResources().getString(R.string.contact);
            case TdApi.MessageDeleted.CONSTRUCTOR:
                //deleted
                return TelegramApplication.sharedApplication().getResources().getString(R.string.deleted_msg);
            case TdApi.MessageAnimation.CONSTRUCTOR:
                return TelegramApplication.sharedApplication().getString(R.string.animation);
            case TdApi.MessageVideo.CONSTRUCTOR:
                return TelegramApplication.sharedApplication().getResources().getString(R.string.video);
            case TdApi.MessageSticker.CONSTRUCTOR:
                return TelegramApplication.sharedApplication().getResources().getString(R.string.sticker);
            case TdApi.MessageGroupChatCreate.CONSTRUCTOR:
                UserModel chatCreator = UserCache.getInstance().getUserById(msg.fromId);
                fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_created_group_extended);
                return String.format(fmt, getDisplayNameForChat(chatCreator, 0), ((TdApi.MessageGroupChatCreate) msg.content).title);
            case TdApi.MessageChatAddParticipants.CONSTRUCTOR:
                UserModel inviter = UserCache.getInstance().getUserById(msg.fromId);
                if(((TdApi.MessageChatAddParticipants)msg.content).participants.length == 1 && inviter.getId() == ((TdApi.MessageChatAddParticipants)msg.content).participants[0].id)
                {
                    u = new UserModel(((TdApi.MessageChatAddParticipants) msg.content).participants[0]);
                    fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_returned);
                    username = getDisplayNameForChat(u, 0);
                    return String.format(fmt, username);
                }
                String inviterDisplayName = getDisplayNameForChat(inviter, 0);
                String invitedString = "";
                fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_new_participant_extended);
                for(TdApi.User currentUser : ((TdApi.MessageChatAddParticipants)msg.content).participants)
                {
                    u = new UserModel(currentUser);
                    String name = getDisplayNameForChat(u, 1);
                    if(invitedString.isEmpty())
                        invitedString = name;
                    else
                        invitedString = invitedString.concat(", ").concat(name);
                }
                return String.format(fmt, inviterDisplayName, invitedString);
            case TdApi.MessageChatChangePhoto.CONSTRUCTOR:
                if(chat != null && chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR)
                    return TelegramApplication.sharedApplication().getResources().getString(R.string.channel_photo_changed);
                u = UserCache.getInstance().getUserById(msg.fromId);;
                fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_changed_photo_extended);
                return String.format(fmt, getDisplayNameForChat(u, 0));
            case TdApi.MessageChatChangeTitle.CONSTRUCTOR:
                if(chat != null && chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR)
                    return String.format(TelegramApplication.sharedApplication().getResources().getString(R.string.channel_renamed), ((TdApi.MessageChatChangeTitle) msg.content).title);
                u = UserCache.getInstance().getUserById(msg.fromId);
                fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_changed_title);
                return String.format(fmt, getDisplayNameForChat(u, 0), ((TdApi.MessageChatChangeTitle)msg.content).title);
            case TdApi.MessageChatDeleteParticipant.CONSTRUCTOR:
                UserModel kicker = UserCache.getInstance().getUserById(msg.fromId);
                u = new UserModel(((TdApi.MessageChatDeleteParticipant) msg.content).user);
                if(kicker.getId() == u.getId())
                {
                    fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_left);
                    username = getDisplayNameForChat(u, 0);
                    return String.format(fmt, username);
                }
                else
                {
                    fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_removed_participant_extended);
                    username = getDisplayNameForChat(u, 1);
                    return String.format(fmt, getDisplayNameForChat(kicker, 0), username);
                }
            case TdApi.MessageChatDeletePhoto.CONSTRUCTOR:
                if(chat != null && chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR)
                    return TelegramApplication.sharedApplication().getResources().getString(R.string.channel_photo_removed);
                u = UserCache.getInstance().getUserById(msg.fromId);
                fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_removed_photo_extended);
                return String.format(fmt, getDisplayNameForChat(u, 0));
            case TdApi.MessageChatJoinByLink.CONSTRUCTOR:
                u = UserCache.getInstance().getUserById(msg.fromId);
                fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_invited_by_link);
                return String.format(fmt, getDisplayNameForChat(u, 0));
            case TdApi.MessageWebPage.CONSTRUCTOR:
                return ((TdApi.MessageWebPage) msg.content).text;
            case TdApi.MessageVenue.CONSTRUCTOR:
                return TelegramApplication.sharedApplication().getResources().getString(R.string.geopoint);
            case TdApi.MessageChatMigrateFrom.CONSTRUCTOR:
                return TelegramApplication.sharedApplication().getResources().getString(R.string.supergroup_created);
            case TdApi.MessageChatMigrateTo.CONSTRUCTOR:
                return TelegramApplication.sharedApplication().getResources().getString(R.string.upgraded_to_supergroup);
            case TdApi.MessageChannelChatCreate.CONSTRUCTOR:
                return TelegramApplication.sharedApplication().getString(R.string.channel_created);
            default:
                return TelegramApplication.sharedApplication().getResources().getString(R.string.unknown_msg);
        }
    }
    public static int indexOfSuitablePreview(TdApi.PhotoSize[] photos)
    {
        DisplayMetrics metrics = TelegramApplication.sharedApplication().getResources().getDisplayMetrics();
        int maxDimension = (int) (Math.min(metrics.widthPixels, metrics.heightPixels)*0.8);
        int suitable = 0;
        for(int i = 0; i < photos.length; i++)
        {
            if(photos[i].width <= maxDimension && photos[i].width > photos[suitable].width)
                suitable = i;
        }
        return suitable;
    }
    public static int indexOfSharedMediaPreview(TdApi.PhotoSize[] photos)
    {
        int maxDimension = AndroidUtilities.dp(160);
        int suitable = 0;
        for(int i = 0; i < photos.length; i++)
        {
            if(photos[i].width <= maxDimension && photos[i].width > photos[suitable].width)
                suitable = i;
        }
        return suitable;
    }
    public static int indexOfMaxResPhoto(TdApi.PhotoSize[] photos)
    {
        long maxSquare = -1;
        int index = 0;
        for(int i = 0; i < photos.length; i++)
        {
            long sq = photos[i].height * photos[i].width;
            if(sq > maxSquare) {
                maxSquare = sq;
                index = i;
            }
        }
        return index;
    }

    public static String sizeToString(int bytes)
    {
        double kb = ((double) bytes) / 1000.0;
        double mb = kb / 1000.0;
        double gb = mb / 1000.0;

        if(gb > 1.0)
            return String.format(TelegramApplication.sharedApplication().getResources().getString(R.string.gb_format), gb);
        else if(mb > 1.0)
            return String.format(TelegramApplication.sharedApplication().getResources().getString(R.string.mb_format), mb);
        else if(kb > 1.0)
            return String.format(TelegramApplication.sharedApplication().getResources().getString(R.string.kb_format), kb);
        return String.format(TelegramApplication.sharedApplication().getResources().getString(R.string.b_format), bytes);
    }

    public static String dateToDayAndMonth(long seconds)
    {
        return mmmmd.format(new Date(seconds * 1000));
    }

    public static String makeFileKey(int id)
    {
        if(id != 0)
            return "file".concat(String.valueOf(id));
        return null;
    }

    public static String formatPhoneNumber(String phone)
    {
        if(phone == null || phone.length() == 0)
            return TelegramApplication.sharedApplication().getResources().getString(R.string.unknown);
        String ph = !phone.substring(0, 1).equals("+") ? "+".concat(phone) : phone;
        return PhoneFormat.getInstance().format(ph);
    }
    public static String statusToText(TdApi.UserStatus st)
    {
        if(st.getConstructor() == TdApi.UserStatusOnline.CONSTRUCTOR || st.getConstructor() == TdApi.UserStatusRecently.CONSTRUCTOR)
            return statusText[0];
        else
        {
            if(st.getConstructor() == TdApi.UserStatusOffline.CONSTRUCTOR)
                return String.format(statusText[1], CommonTools.dateToString(((TdApi.UserStatusOffline) st).wasOnline, false));
            return statusText[2];
        }
    }

    public static PasscodeViewController initPasscodeViewController(PasscodeView viewRef, PasscodeViewController.PasscodeType type, PasscodeViewController.Mode mode)
    {
        if(type == PasscodeViewController.PasscodeType.PIN)
            return new PasscodePinController(viewRef, mode);
        else if(type == PasscodeViewController.PasscodeType.PATTERN)
            return new PasscodePatternController(viewRef, mode);
        else if(type == PasscodeViewController.PasscodeType.GESTURE)
            return new PasscodeGestureController(viewRef, mode);
        return new PasscodePasswordController(viewRef, mode);
    }
    public static String getDisplayNameForChat(UserModel user, int position)
    {
        if(user == null)
            return "";
        if(user.getId() == CurrentUserModel.getInstance().getUserModel().getId())
        {
            if(position == 0)
                return TelegramApplication.sharedApplication().getResources().getString(R.string.you);
            return TelegramApplication.sharedApplication().getResources().getString(R.string.you_lowercase);
        }
        if(user.getLastName() == null || user.getLastName().length() == 0)
            return user.getFirstName();
        return String.format("%s %s", user.getFirstName(), user.getLastName());
    }
    public static String generateChannelDescription(int channelId)
    {
        TdApi.ChannelFull ch = ChannelCache.getInstance().getChannelFull(channelId);
        if(ch == null || ch.participantsCount <= 0)
            return TelegramApplication.sharedApplication().getString(R.string.private_channel);
        else if(ch.participantsCount == 1)
            return TelegramApplication.sharedApplication().getString(R.string.one_follower);
        int thousands = ch.participantsCount / 1000;
        int millions = ch.participantsCount / 1000000;
        if(ch.channel.isSupergroup)
            return String.format(TelegramApplication.sharedApplication().getString(R.string.supergroup_members), ch.participantsCount);
        else
        {
            if(thousands <= 1)
                return String.format(TelegramApplication.sharedApplication().getString(R.string.multiple_followers), ch.participantsCount);
            else if(millions <= 0)
                return String.format(TelegramApplication.sharedApplication().getString(R.string.thousands_followers), thousands);
            return String.format(TelegramApplication.sharedApplication().getString(R.string.millions_followers), millions);
        }
    }

    public static String generateChatDescription(int groupId, boolean supergroup)
    {
        if(!supergroup)
        {
            TdApi.GroupFull groupFull = GroupCache.getInstance().getGroupFull(groupId, false);
            if(groupFull.participants.length == 1)
                return TelegramApplication.sharedApplication().getResources().getString(R.string.chat_info_one_member);
            int online = getGroupOnlineCount(groupFull.participants);
            if(online < 2)
                return String.format(TelegramApplication.sharedApplication().getResources().getString(R.string.chat_info_participants_count), groupFull.participants.length);
            return String.format(TelegramApplication.sharedApplication().getResources().getString(R.string.chat_info_participants_count_extended), groupFull.participants.length, online);
        }
        else
        {
            TdApi.ChannelFull ch = ChannelCache.getInstance().getChannelFull(groupId);
            if(ch == null || ch.participantsCount == 0)
                return TelegramApplication.sharedApplication().getString(R.string.supergroup);
            return String.format(TelegramApplication.sharedApplication().getResources().getString(R.string.chat_info_participants_count), ch.participantsCount);
        }
    }
    public static int getGroupOnlineCount(TdApi.ChatParticipant[] participants)
    {
        int onlineCount = 0;
        for(TdApi.ChatParticipant p : participants)
        {
            if(p.botInfo.getConstructor() == TdApi.BotInfoGeneral.CONSTRUCTOR)
                continue;
            if(UserModel.isOnlineStatus(p.user.status))
                onlineCount++;
        }
        return onlineCount;
    }
    public static boolean forwardFromChannel(TdApi.MessageForwardInfo info) { return info != null && info.getConstructor() == TdApi.MessageForwardedFromChannel.CONSTRUCTOR; }
    public static String dateToMonthAndYear(Date d)
    {
        return mmmmyyyy.format(d);
    }
    public static String generateLocationKey(TdApi.Location loc, boolean isVenue) { return String.format("%s_%f_%f", isVenue ? "venue" : "location", loc.latitude, loc.longitude); }

    public static boolean isFullObjectPreloaded(int peer, PeerType type)
    {
        if(type == PeerType.USER)
            return UserCache.getInstance().isCachedFull(peer);
        else if(type == PeerType.GROUP)
            return GroupCache.getInstance().isCachedFull(peer);
        else if(type == PeerType.CHANNEL)
            return ChannelCache.getInstance().isCachedFull(peer);
        return false;
    }
    public static boolean checkUsername(String name)
    {
        if(name.length() == 0)
            return false;
        if (name != null) {
            if (name.startsWith("_") || name.endsWith("_")) {
                return false;
            }
            for (int a = 0; a < name.length(); a++) {
                char ch = name.charAt(a);
                if (a == 0 && ch >= '0' && ch <= '9') {
                    return false;
                }
                if (!(ch >= '0' && ch <= '9' || ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z' || ch == '_')) {
                    return false;
                }
            }
        }
        if (name == null || name.length() < 5) {
            return false;
        }
        if (name.length() > 32) {
            return false;
        }
        return true;
    }
}
