package com.andremacareno.tdtest.models;

/**
 * Created by andremacareno on 22/08/15.
 */
public abstract class AdditionalActionForward implements AdditionalSendAction {
    protected int[] messageIds;
    @Override
    public DataType getDataType() {
        return DataType.FWD;
    }
    public abstract long getSourceChatId();
    public abstract boolean fromSharedMedia();
    public abstract int getForwardedCount();
    public int[] getMessageIds() { return this.messageIds; }
}
