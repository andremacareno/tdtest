package com.andremacareno.tdtest.models;

import android.text.Spannable;
import android.util.Patterns;
import android.view.View;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.NonUnderlinedRegularClickableSpan;
import com.andremacareno.tdtest.fragments.BaseChatFragment;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Andrew on 22.04.2015.
 */
public class BotHeaderMessageModel extends PseudoMessageModel {
    private Spannable short_description;
    private BaseChatFragment.ChatDelegate delegate;
    public BotHeaderMessageModel(TdApi.Message msg) {
        super(msg);
    }
    public BotHeaderMessageModel(TdApi.BotInfoGeneral info)
    {
        super(null);
        if(info == null)
            return;
        setBotInfo(info);
    }
    @Override
    public int getMessageId()
    {
        return Constants.MODEL_BOT_CHAT_HEADER;
    }
    @Override
    protected void processMessage(TdApi.Message msg) {
    }

    @Override
    public int getSuitableConstructor() {
        return Constants.MODEL_BOT_CHAT_HEADER;
    }
    public void setDelegate(BaseChatFragment.ChatDelegate delegate)
    {
        this.delegate = delegate;
    }
    public Spannable getDescription() { return this.short_description; }
    public void setBotInfo(TdApi.BotInfoGeneral info)
    {
        short_description = Spannable.Factory.getInstance().newSpannable(info.description);
        Pattern urlPattern = Patterns.WEB_URL;
        Matcher matcher = urlPattern.matcher(info.description);
        while (matcher.find()) {
            final String str = matcher.group();
            NonUnderlinedRegularClickableSpan span = new NonUnderlinedRegularClickableSpan() {
                @Override
                public void onClick(View view) {
                    if(delegate != null)
                        delegate.didUrlClicked(str, false);
                }
            };
            short_description.setSpan(span, matcher.start(), matcher.end(), 0);
        }

        Pattern botCmdPattern = Pattern.compile("/([A-Za-z0-9_-]{1,32})");
        matcher = botCmdPattern.matcher(info.description);
        while (matcher.find()) {
            String found = matcher.group();
            final String str = found.startsWith(" ") ? found.substring(1) : found;
            NonUnderlinedRegularClickableSpan span = new NonUnderlinedRegularClickableSpan() {
                @Override
                public void onClick(View view) {
                    if(delegate != null)
                        delegate.didCommandClicked(str);
                }
            };
            int startPosition = found.startsWith(" ") ? matcher.start()+1 : matcher.start();
            short_description.setSpan(span, startPosition, matcher.end(), 0);
        }
    }
}
