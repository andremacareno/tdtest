package com.andremacareno.tdtest.models;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.view.View;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.NonUnderlinedClickableSpan;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class ChatMigrateToMessageModel extends TextMessageModel {
    public ChatMigrateToMessageModel(TdApi.Message msg) {
        super(msg);
    }

    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        final int channelId = ((TdApi.MessageChatMigrateTo) msgContent).channelId;
        String newGroupName = ChatCache.getInstance().getChatByChannelId(channelId).title;
        String fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.upgraded_to_supergroup_extended);
        this.text = new SpannableString(String.format(fmt, newGroupName));
        ClickableSpan clickableSpan = new NonUnderlinedClickableSpan() {
            @Override
            public void onClick(View textView) {
                NotificationCenter.getInstance().postNotification(NotificationCenter.didSupergroupRequestedByLink, channelId);
            }
        };
        this.text.setSpan(clickableSpan, this.text.length() - 1 - newGroupName.length(), this.text.length() - 1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageChatMigrateTo.CONSTRUCTOR;
    }

    @Override
    public boolean isChatServiceMessage() {
        return true;
    }
}
