package com.andremacareno.tdtest.models;

import android.text.SpannableString;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class ChatMigrateFromMessageModel extends TextMessageModel {
    public ChatMigrateFromMessageModel(TdApi.Message msg) {
        super(msg);
    }

    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        this.text = new SpannableString(TelegramApplication.sharedApplication().getResources().getString(R.string.supergroup_created));
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageChatMigrateFrom.CONSTRUCTOR;
    }

    @Override
    public boolean isChatServiceMessage() {
        return true;
    }
}
