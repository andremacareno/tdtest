package com.andremacareno.tdtest.models;

import android.content.res.Resources;
import android.graphics.Bitmap;

import com.andremacareno.tdtest.images.RecyclingBitmapDrawable;

/**
 * Created by andremacareno on 28/08/15.
 */
public class AlbumCoverDrawable extends RecyclingBitmapDrawable {
    private boolean has_cover = false;
    private boolean darkCover = false;
    public AlbumCoverDrawable(Resources res, Bitmap bitmap) {
        super(res, bitmap);
    }
    public void setHasCover(boolean has) { this.has_cover = has; }
    public void setHasDarkCover(boolean has) { this.darkCover = has; }

    public boolean hasCover() { return this.has_cover; }
    public boolean isDarkCover() { return this.darkCover; }
}
