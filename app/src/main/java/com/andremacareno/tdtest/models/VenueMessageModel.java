package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.CommonTools;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class VenueMessageModel extends MessageModel {
    private TdApi.Location location;
    private String title;
    private String address;
    private String locationKey;
    public VenueMessageModel(TdApi.Message msg) {
        super(msg);
    }

    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        this.location = ((TdApi.MessageVenue) msgContent).location;
        this.locationKey = CommonTools.generateLocationKey(this.location, true);
        this.title = ((TdApi.MessageVenue) msgContent).title;
        this.address = ((TdApi.MessageVenue) msgContent).address;
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageVenue.CONSTRUCTOR;
    }

    public TdApi.Location getLocation() { return this.location; }
    public String getLocationKey() { return this.locationKey; }
    public String getTitle() { return this.title; }
    public String getAddress() { return this.address; }
}
