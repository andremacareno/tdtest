package com.andremacareno.tdtest.models;

import android.text.SpannableString;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class ChannelChatCreateMessageModel extends TextMessageModel {
    public ChannelChatCreateMessageModel(TdApi.Message msg) {
        super(msg);
    }

    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        this.text = new SpannableString(TelegramApplication.sharedApplication().getString(R.string.channel_created));
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageChannelChatCreate.CONSTRUCTOR;
    }

    @Override
    public boolean isChatServiceMessage() {
        return true;
    }
}
