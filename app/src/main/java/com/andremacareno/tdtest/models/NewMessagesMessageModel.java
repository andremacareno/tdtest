package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 22.04.2015.
 */
public class NewMessagesMessageModel extends PseudoMessageModel {
    private String headerString;
    public NewMessagesMessageModel(int count) {
        super(null);
        if(count == 1)
            this.headerString = TelegramApplication.sharedApplication().getResources().getString(R.string.one_new_msg);
        else
            this.headerString = String.format(TelegramApplication.sharedApplication().getResources().getString(R.string.n_new_messages), count);
    }
    @Override
    public int getMessageId()
    {
        return -1;
    }
    @Override
    protected void processMessage(TdApi.Message msg) {
    }

    @Override
    public int getSuitableConstructor() {
        return Constants.MODEL_UNREAD_SEPARATOR;
    }

    public String getHeaderString() { return this.headerString; }

}
