package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.files.FileCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 06/05/15.
 */
public class PhotoSizeModel {
    private String type;
    private FileModel photo;
    private int width;
    private int height;
    public PhotoSizeModel(TdApi.PhotoSize ph)
    {
        this.type = ph.type;
        this.photo = FileCache.getInstance().getFileModel(ph.photo);
        this.width = ph.width;
        this.height = ph.height;
    }
    public String getType() { return this.type; }
    public FileModel getPhoto() { return this.photo; }
    public int getWidth() { return this.width; }
    public int getHeight() { return this.height; }
}
