package com.andremacareno.tdtest.models;

import android.text.Spannable;

import com.andremacareno.tdtest.fragments.BaseChatFragment;
import com.andremacareno.tdtest.markup.TelegramMarkupFactory;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class TextMessageModel extends MessageModel {
    public interface TextMessageLinkDelegate
    {
        public BaseChatFragment.ChatDelegate getChatDelegate();
    }
    protected Spannable text;
    private BaseChatFragment.ChatDelegate delegate;
    private TextMessageLinkDelegate linkDelegate;
    public TextMessageModel(TdApi.Message msg) {
        super(msg);
    }
    @Override
    protected void beforeProcessMessageContent()
    {
        linkDelegate = new TextMessageLinkDelegate() {
            @Override
            public BaseChatFragment.ChatDelegate getChatDelegate() {
                return delegate;
            }
        };
    }


    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        text = TelegramMarkupFactory.addSpan(((TdApi.MessageText) msgContent).text, ((TdApi.MessageText) msgContent).entities, linkDelegate);
        /*Pattern botCmdPattern = Pattern.compile("/([A-Za-z0-9_-]{1,32})");
        Pattern mentionPattern = Pattern.compile("@([A-Za-z0-9_]{5,32})");
        final Pattern urlPattern = Patterns.WEB_URL;

        Matcher matcher = urlPattern.matcher(text);
        while (matcher.find()) {
            final String str = matcher.group();
            NonUnderlinedRegularClickableSpan span = new NonUnderlinedRegularClickableSpan() {
                @Override
                public void onClick(View view) {
                    if(delegate != null)
                        delegate.didUrlClicked(str);
                }
            };
            text.setSpan(span, matcher.start(), matcher.end(), 0);
        }
        matcher = botCmdPattern.matcher(text);
        while (matcher.find()) {
            String found = matcher.group();
            final String str = found.startsWith(" ") ? found.substring(1) : found;
            NonUnderlinedRegularClickableSpan span = new NonUnderlinedRegularClickableSpan() {
                @Override
                public void onClick(View view) {
                    if(delegate != null)
                        delegate.didCommandClicked(str);
                }
            };
            int startPosition = found.startsWith(" ") ? matcher.start()+1 : matcher.start();
            text.setSpan(span, startPosition, matcher.end(), 0);
        }
        matcher = mentionPattern.matcher(text);
        while (matcher.find()) {
            String found = matcher.group();
            final String str = found.startsWith(" ") ? found.substring(1) : found;
            NonUnderlinedRegularClickableSpan span = new NonUnderlinedRegularClickableSpan() {
                @Override
                public void onClick(View view) {
                    if(delegate != null)
                        delegate.didUsernameClicked(str);
                }
            };
            int startPosition = found.startsWith(" ") ? matcher.start()+1 : matcher.start();
            text.setSpan(span, startPosition, matcher.end(), 0);
        }*/


    }
    public void rerunProcessMessageContent(TdApi.MessageContent msgContent)
    {
        processMessageContent(msgContent);
    }

    public void setDelegate(BaseChatFragment.ChatDelegate delegate)
    {
        this.delegate = delegate;
    }
    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageText.CONSTRUCTOR;
    }

    public Spannable getText() { return text; }


}
