package com.andremacareno.tdtest.models;

import android.text.Spannable;
import android.util.Log;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.fragments.BaseChatFragment;
import com.andremacareno.tdtest.markup.TelegramMarkupFactory;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class WebPageMessageModel extends MessageModel {
    private Spannable text;
    private TdApi.WebPage webPageContainer;
    private BaseChatFragment.ChatDelegate delegate;
    private FileModel photo;
    private int bigPhotoWidth;
    private int bigPhotoHeight;
    private FileModel original;
    private TextMessageModel.TextMessageLinkDelegate linkDelegate;
    public WebPageMessageModel(TdApi.Message msg) {
        super(msg);
    }
    @Override
    protected void beforeProcessMessageContent()
    {
        linkDelegate = new TextMessageModel.TextMessageLinkDelegate() {
            @Override
            public BaseChatFragment.ChatDelegate getChatDelegate() {
                return delegate;
            }
        };
    }


    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        text = TelegramMarkupFactory.addSpan(((TdApi.MessageWebPage) msgContent).text, ((TdApi.MessageWebPage) msgContent).entities, linkDelegate);
        this.webPageContainer = ((TdApi.MessageWebPage) msgContent).webPage;
        if(webPageContainer.type.equals("article") || webPageContainer.type.equals("app"))
        {
            if(webPageContainer.photo.photos.length > 0)
                photo = FileCache.getInstance().getFileModel(webPageContainer.photo.photos[CommonTools.indexOfMaxResPhoto(webPageContainer.photo.photos)].photo);
            else
                photo = null;
        }
        else if(webPageContainer.photo.photos.length > 0) {
            TdApi.PhotoSize phSize = webPageContainer.photo.photos[CommonTools.indexOfSuitablePreview(webPageContainer.photo.photos)];
            int screenWidth = TelegramApplication.sharedApplication().getResources().getDisplayMetrics().widthPixels;
            int screenHeight = TelegramApplication.sharedApplication().getResources().getDisplayMetrics().heightPixels;
            float thumbAspectRatio = (float) phSize.width / phSize.height;
            if(phSize.width < Math.min(screenWidth, screenHeight)*0.6f)
            {
                bigPhotoWidth = Math.round((float) Math.min(screenWidth, screenHeight) * 0.6f);
                bigPhotoHeight = Math.round((float) bigPhotoWidth / thumbAspectRatio);
            }
            else
            {
                bigPhotoHeight = phSize.height;
                bigPhotoWidth = phSize.width;
            }
            try
            {
                if(BuildConfig.DEBUG)
                    Log.d("WebPage", String.format("setting original; index = %d", CommonTools.indexOfMaxResPhoto(webPageContainer.photo.photos)));
                this.original = FileCache.getInstance().getFileModel(webPageContainer.photo.photos[CommonTools.indexOfMaxResPhoto(webPageContainer.photo.photos)].photo);
                photo = FileCache.getInstance().getFileModel(webPageContainer.photo.photos[CommonTools.indexOfSuitablePreview(webPageContainer.photo.photos)].photo);
            }
            catch(Exception ignored) {ignored.printStackTrace();}
        }
        else {
            photo = null;
            original = null;
        }
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageWebPage.CONSTRUCTOR;
    }

    public void setDelegate(BaseChatFragment.ChatDelegate delegate)
    {
        this.delegate = delegate;
    }
    public Spannable getText() { return this.text; }
    public TdApi.WebPage getPageContainer() { return this.webPageContainer; }
    public FileModel getPhoto() { return this.photo; }
    public FileModel getOriginal() {
        if(BuildConfig.DEBUG)
            Log.d("WebPage", String.format("original = %s", original == null ? "null" : original.toString()));
        return this.original;
    }
    public int getPhotoWidth() { return this.bigPhotoWidth; }
    public int getPhotoHeight() { return this.bigPhotoHeight; }
}
