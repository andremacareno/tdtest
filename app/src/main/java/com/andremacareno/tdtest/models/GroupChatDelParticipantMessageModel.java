package com.andremacareno.tdtest.models;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.view.View;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.NonUnderlinedClickableSpan;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class GroupChatDelParticipantMessageModel extends TextMessageModel {

    public GroupChatDelParticipantMessageModel(TdApi.Message msg) {
        super(msg);
    }

    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        if(getFromUsername() == null)
            return;
        final TdApi.User u = ((TdApi.MessageChatDeleteParticipant)msgContent).user;
        int myId = CurrentUserModel.getInstance().getUserModel().getId();
        if(u.id == getFromId())
        {
            String fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_left);
            String username = u.id == CurrentUserModel.getInstance().getUserModel().getId() ? TelegramApplication.sharedApplication().getResources().getString(R.string.you) : String.format("%s %s", u.firstName, u.lastName);
            this.text = new SpannableString(String.format(fmt, username));
            if(u.id != myId)
            {
                ClickableSpan clickableSpan = new NonUnderlinedClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        NotificationCenter.getInstance().postNotification(NotificationCenter.didViewProfileByLinkRequested, u.id);
                    }
                };
                this.text.setSpan(clickableSpan, 0, username.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }
        }
        else
        {
            String fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_removed_participant_extended);
            String username = getFromId() == CurrentUserModel.getInstance().getUserModel().getId() ? TelegramApplication.sharedApplication().getResources().getString(R.string.you) : getFromUsername();
            String username2 = u.id == CurrentUserModel.getInstance().getUserModel().getId() ? TelegramApplication.sharedApplication().getResources().getString(R.string.you_lowercase) : String.format("%s %s", u.firstName, u.lastName);
            this.text = new SpannableString(String.format(fmt, username, username2));
            if(getFromId() != myId)
            {
                ClickableSpan clickableSpan = new NonUnderlinedClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        NotificationCenter.getInstance().postNotification(NotificationCenter.didViewProfileByLinkRequested, getFromId());
                    }
                };
                this.text.setSpan(clickableSpan, 0, username.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }
            if(u.id != myId)
            {
                ClickableSpan clickableSpan = new NonUnderlinedClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        NotificationCenter.getInstance().postNotification(NotificationCenter.didViewProfileByLinkRequested, u.id);
                    }
                };
                this.text.setSpan(clickableSpan, text.length()-username2.length(), text.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }
        }
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageChatDeleteParticipant.CONSTRUCTOR;
    }

    @Override
    public boolean isChatServiceMessage() {
        return true;
    }
}
