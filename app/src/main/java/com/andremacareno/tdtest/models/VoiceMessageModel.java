package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.files.FileCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class VoiceMessageModel extends MessageModel {
    private FileModel fileRef;
    private String duration;
    private float startFromMark = 0;
    private long totalPcmDuration;
    private int intDuration;
    private String voiceMsgDelegateKey;
    private boolean isPlaying = false;

    public VoiceMessageModel(TdApi.Message msg) {
        super(msg);
    }

    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        TdApi.Voice audio = ((TdApi.MessageVoice) msgContent).voice;
        fileRef = FileCache.getInstance().getFileModel(audio.voice);
        intDuration = audio.duration;
        long minutes = audio.duration / 60;
        long seconds = audio.duration % 60;
        duration = String.format("%d:%02d", minutes, seconds);
        voiceMsgDelegateKey = String.format("voice%d_%d_delegate", getMessageId(), audio.voice.id);
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageVoice.CONSTRUCTOR;
    }

    public FileModel getAudioFileRef() { return this.fileRef; }
    public String getDuration() { return this.duration; }
    public int getIntegerDuration() { return this.intDuration; }
    public void startPlaybackFrom(float progress) {
        startFromMark = progress;
    }
    public float getStartFrom() { return this.startFromMark; }
    public void resetStartMark() { this.startFromMark = 0; }
    public long getTotalPcmDuration() { return this.totalPcmDuration; }
    public void setTotalPcmDuration(long duration) {
        this.totalPcmDuration = duration;
    }
    public String getDelegateKey() { return this.voiceMsgDelegateKey; }
    public void updateProgress(float progress)
    {
        this.startFromMark = progress;
    }
    public boolean getPlaybackState() { return this.isPlaying; }
    public void setPlaybackState(boolean newState) { this.isPlaying = newState; }
}
