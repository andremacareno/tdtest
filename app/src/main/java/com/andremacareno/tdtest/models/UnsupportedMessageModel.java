package com.andremacareno.tdtest.models;

import android.text.SpannableString;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class UnsupportedMessageModel extends TextMessageModel {

    public UnsupportedMessageModel(TdApi.Message msg) {
        super(msg);
    }

    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        this.text = new SpannableString(TelegramApplication.sharedApplication().getResources().getString(R.string.unknown_msg));
        //this.text = new SpannableString(msgContent.toString());
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageUnsupported.CONSTRUCTOR;
    }
}
