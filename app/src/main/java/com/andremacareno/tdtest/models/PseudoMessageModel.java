package com.andremacareno.tdtest.models;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 22.04.2015.
 * MessageModel class for design elements such as date and "N new messages" sharedmedia_section headers
 */
public abstract class PseudoMessageModel extends MessageModel{
    public PseudoMessageModel(TdApi.Message msg)
    {
        super(null);
        processMessage(msg);
    }
    protected abstract void processMessage(TdApi.Message msg);
    @Override
    public boolean isPseudoMessage() { return true; }
    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {}
}
