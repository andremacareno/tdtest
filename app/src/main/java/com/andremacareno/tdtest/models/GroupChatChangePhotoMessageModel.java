package com.andremacareno.tdtest.models;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.view.View;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.NonUnderlinedClickableSpan;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.files.FileCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class GroupChatChangePhotoMessageModel extends TextMessageModel {
    private FileModel newChatPhoto;
    private String newChatPhotoKey;
    public GroupChatChangePhotoMessageModel(TdApi.Message msg) {
        super(msg);
    }

    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        TdApi.MessageChatChangePhoto cast = (TdApi.MessageChatChangePhoto) msgContent;
        try
        {
            newChatPhoto = FileCache.getInstance().getFileModel(cast.photo.photos[CommonTools.indexOfSuitablePreview(cast.photo.photos)].photo);
            newChatPhotoKey = CommonTools.makeFileKey(newChatPhoto.getFileId());
        }
        catch(Exception e) { e.printStackTrace(); newChatPhoto = null; newChatPhotoKey = null; }
        if(getFromUsername() == null) {
            if(messageInChannelChat())
                this.text = new SpannableString(TelegramApplication.sharedApplication().getString(R.string.channel_photo_changed));
            return;
        }
        String fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_changed_photo_extended);
        if(CurrentUserModel.getInstance().getUserModel().getId() == getFromId())
            this.text = new SpannableString(String.format(fmt, TelegramApplication.sharedApplication().getResources().getString(R.string.you)));
        else {
            this.text = new SpannableString(String.format(fmt, getFromUsername()));
            ClickableSpan clickableSpan = new NonUnderlinedClickableSpan() {
                @Override
                public void onClick(View textView) {
                    NotificationCenter.getInstance().postNotification(NotificationCenter.didViewProfileByLinkRequested, getFromId());
                }
            };
            this.text.setSpan(clickableSpan, 0, getFromUsername().length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageChatChangePhoto.CONSTRUCTOR;
    }
    @Override
    public boolean isChatServiceMessage() {
        return true;
    }
    public FileModel getNewChatPhoto() { return this.newChatPhoto; }
    public String getNewChatPhotoKey() { return this.newChatPhotoKey; }
}
