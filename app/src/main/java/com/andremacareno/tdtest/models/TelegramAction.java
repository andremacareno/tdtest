package com.andremacareno.tdtest.models;

/**
 * Created by Andrew on 01.05.2015.
 */
public class TelegramAction {
    private static int totalEvents = 1;
    public static final int ATTACH_CHOOSE_FROM_GALLERY = totalEvents++;
    public static final int ATTACH_TAKE_A_PHOTO = totalEvents++;
    public static final int ATTACH_CONTACT = totalEvents++;
    public static final int ATTACH_DO_NOTHING = totalEvents++;
    public static final int ATTACH_AUDIO = totalEvents++;

    public static final int PROFILE_PHONE = totalEvents++;
    public static final int PROFILE_USERNAME = totalEvents++;
    public static final int PROFILE_NOTIFICATIONS = totalEvents++;
    public static final int PROFILE_SHARE = totalEvents++;
    public static final int PROFILE_BLOCK = totalEvents++;

    public static final int NAVDRAWER_NEW_GROUP = totalEvents++;
    public static final int NAVDRAWER_CONTACTS = totalEvents++;
    public static final int NAVDRAWER_SETTINGS = totalEvents++;
    public static final int NAVDRAWER_LOG_OUT = totalEvents++;
    public static final int NAVDRAWER_NEW_CHANNEL = totalEvents++;

    public static final int SETTINGS_PASSCODE_LOCK = totalEvents++;

    public static final int MESSAGE_REPLY = totalEvents++;
    public static final int MESSAGE_SHARE = totalEvents++;
    public static final int MESSAGE_COPY = totalEvents++;
    public static final int MESSAGE_SELECT = totalEvents++;
    public static final int MESSAGE_DELETE = totalEvents++;

    private int action;
    private String text;
    private int imgRes;
    public TelegramAction(int act, String txt, int res)
    {
        this.action = act;
        this.text = txt;
        this.imgRes = res;
    }
    public void setText(String txt) { this.text = txt; }
    public int getAction() { return this.action; }
    public String getText() { return this.text; }
    public int getImageResource() { return this.imgRes; }
}
