package com.andremacareno.tdtest.models;

/**
 * Created by Andrew on 21.04.2015.
 */
public interface MessageModelChangeObserver {
    //messageId: id of message corresponding to changed model
    public void didMessageModelChanged(int messageId);

}
