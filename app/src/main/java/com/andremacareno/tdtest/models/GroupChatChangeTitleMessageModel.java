package com.andremacareno.tdtest.models;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.view.View;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.NonUnderlinedClickableSpan;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class GroupChatChangeTitleMessageModel extends TextMessageModel {

    public GroupChatChangeTitleMessageModel(TdApi.Message msg) {
        super(msg);
    }

    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        if(getFromUsername() == null) {
            if(messageInChannelChat())
                this.text = new SpannableString(TelegramApplication.sharedApplication().getResources().getString(R.string.channel_renamed_short));
            return;
        }
        TdApi.MessageChatChangeTitle changeTitle = (TdApi.MessageChatChangeTitle) msgContent;
        String fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_changed_title);
        if(CurrentUserModel.getInstance().getUserModel().getId() == getFromId())
            this.text = new SpannableString(String.format(fmt, TelegramApplication.sharedApplication().getResources().getString(R.string.you), changeTitle.title));
        else {
            this.text = new SpannableString(String.format(fmt, this.getFromUsername(), changeTitle.title));
            ClickableSpan clickableSpan = new NonUnderlinedClickableSpan() {
                @Override
                public void onClick(View textView) {
                    NotificationCenter.getInstance().postNotification(NotificationCenter.didViewProfileByLinkRequested, getFromId());
                }
            };
            this.text.setSpan(clickableSpan, 0, getFromUsername().length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageChatChangeTitle.CONSTRUCTOR;
    }

    @Override
    public boolean isChatServiceMessage() {
        return true;
    }
}
