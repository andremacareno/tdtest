package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.TelegramApplication;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class PhotoMessageModel extends MessageModel {
    private PhotoSizeModel preview;
    private PhotoSizeModel original;

    private String preview_key;
    private String original_key;
    private int layoutParamsWidth, layoutParamsHeight;
    private String caption;
    public PhotoMessageModel(TdApi.Message msg) {
        super(msg);
    }


    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        TdApi.Photo photo = ((TdApi.MessagePhoto) msgContent).photo;
        if(photo.photos.length == 0) {
            this.preview = this.original = null;
            this.preview_key = this.original_key = null;
            this.layoutParamsWidth = this.layoutParamsHeight = 0;
            this.caption = "";
            return;
        }
        int previewIndex = CommonTools.indexOfSuitablePreview(photo.photos);
        int originalIndex = CommonTools.indexOfMaxResPhoto(photo.photos);
        if(previewIndex != originalIndex)
        {
            this.preview = new PhotoSizeModel(photo.photos[previewIndex]);
            this.original = new PhotoSizeModel(photo.photos[originalIndex]);
        }
        else
        {
            this.preview = new PhotoSizeModel(photo.photos[previewIndex]);
            this.original = this.preview;
        }
        caption = ((TdApi.MessagePhoto) msgContent).caption;
        preview_key = CommonTools.makeFileKey(this.preview.getPhoto().getFileId());
        original_key = CommonTools.makeFileKey(this.original.getPhoto().getFileId());
        int screenWidth = TelegramApplication.sharedApplication().getResources().getDisplayMetrics().widthPixels;
        int screenHeight = TelegramApplication.sharedApplication().getResources().getDisplayMetrics().heightPixels;
        float thumbAspectRatio = (float) preview.getWidth() / preview.getHeight();
        if(preview.getWidth() > Math.min(screenWidth, screenHeight)*0.6f)
        {
            layoutParamsWidth = Math.round((float) Math.min(screenWidth, screenHeight) * 0.6f);
            layoutParamsHeight = Math.round((float) layoutParamsWidth / thumbAspectRatio);
        }
        else
        {
            layoutParamsWidth = preview.getWidth();
            layoutParamsHeight = preview.getHeight();
        }
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessagePhoto.CONSTRUCTOR;
    }

    public PhotoSizeModel getPreview() { return this.preview; }
    public PhotoSizeModel getOriginal() { return this.original; }

    public String getPreviewKey() { return this.preview_key; }
    public String getOriginalKey() { return this.original_key; }
    public String getCaption() { return this.caption; }
    public int getLayoutParamsWidth() { return this.layoutParamsWidth; }
    public int getLayoutParamsHeight() { return this.layoutParamsHeight; }
}
