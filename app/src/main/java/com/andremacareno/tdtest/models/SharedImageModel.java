package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.CommonTools;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 19/08/15.
 */
public class SharedImageModel implements SharedMedia{
    private int msgId;
    private long date;
    protected PhotoSizeModel preview;
    protected PhotoSizeModel original;
    public SharedImageModel(TdApi.Message msg)
    {
        this.msgId = msg.id;
        this.date = ((long) msg.date) * 1000;
        if(msg.content.getConstructor() != TdApi.MessagePhoto.CONSTRUCTOR)
            return;
        TdApi.MessagePhoto photos = (TdApi.MessagePhoto) msg.content;
        preview = new PhotoSizeModel(photos.photo.photos[CommonTools.indexOfSharedMediaPreview(photos.photo.photos)]);
        original = new PhotoSizeModel(photos.photo.photos[CommonTools.indexOfMaxResPhoto(photos.photo.photos)]);

    }

    @Override
    public int getMessageId() {
        return msgId;
    }

    @Override
    public long getDate() {
        return date;
    }

    public boolean isVideo() {
        return false;
    }
    public PhotoSizeModel getPreview() { return this.preview; }
    public PhotoSizeModel getOriginal() { return this.original; }
    @Override
    public boolean isDummy() {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SharedImageModel that = (SharedImageModel) o;

        if (msgId != that.msgId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return msgId;
    }
}
