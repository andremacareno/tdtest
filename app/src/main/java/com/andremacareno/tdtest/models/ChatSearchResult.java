package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 20/02/16.
 */
public class ChatSearchResult implements GlobalSearchResult {
    private TdApi.Chat chat;
    private int peer;
    private String initials;
    private String placeholderKey;
    private String username;
    private ResultType resultType;
    public ChatSearchResult(TdApi.Chat ch, ResultType type)
    {
        this.chat = ch;
        this.resultType = type;
        this.initials = CommonTools.getInitialsByChatInfo(ch);
        this.peer = ch.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR ? ((TdApi.ChannelChatInfo) ch.type).channel.id : ch.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR ? ((TdApi.PrivateChatInfo) ch.type).user.id : ((TdApi.GroupChatInfo) ch.type).group.id;
        this.placeholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, peer);
        this.username = ch.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR ? "@".concat(((TdApi.ChannelChatInfo) ch.type).channel.username) : ch.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR ? "@".concat(((TdApi.PrivateChatInfo) ch.type).user.username) : "";
    }
    @Override
    public ResultType getResultType() {
        return this.resultType;
    }

    public TdApi.Chat getChatObj() { return this.chat; }
    public int getPeer() { return this.peer; }
    public String getInitials() { return this.initials; }
    public String getPlaceholderKey() { return this.placeholderKey; }
    public String getUsername() { return this.username; }
}
