package com.andremacareno.tdtest.models;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.View;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.NonUnderlinedClickableSpan;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class GroupChatAddParticipantMessageModel extends TextMessageModel {

    public GroupChatAddParticipantMessageModel(TdApi.Message msg) {
        super(msg);
    }

    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        if(getFromUsername() == null)
            return;
        int myId = CurrentUserModel.getInstance().getUserModel().getId();
        if(((TdApi.MessageChatAddParticipants)msgContent).participants.length == 0)
            return;
        else if(((TdApi.MessageChatAddParticipants)msgContent).participants.length == 1)
        {
            final TdApi.User u = ((TdApi.MessageChatAddParticipants)msgContent).participants[0];
            if(u.id == getFromId())
            {
                String fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_returned);
                String username = u.id == myId ? TelegramApplication.sharedApplication().getResources().getString(R.string.you) : String.format("%s %s", u.firstName, u.lastName);
                this.text = new SpannableString(String.format(fmt, username));
                if(u.id != myId)
                {
                    ClickableSpan clickableSpan = new NonUnderlinedClickableSpan() {
                        @Override
                        public void onClick(View textView) {
                            NotificationCenter.getInstance().postNotification(NotificationCenter.didViewProfileByLinkRequested, u.id);
                        }
                    };
                    this.text.setSpan(clickableSpan, 0, username.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                }

            }
            else
            {
                String fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_new_participant_extended);
                String username = getFromId() == myId ? TelegramApplication.sharedApplication().getResources().getString(R.string.you) : getFromUsername();
                String username2 = u.id == myId ? TelegramApplication.sharedApplication().getResources().getString(R.string.you_lowercase) : String.format("%s %s", u.firstName, u.lastName);
                this.text = new SpannableString(String.format(fmt, username, username2));
                if(getFromId() != myId)
                {
                    ClickableSpan clickableSpan = new NonUnderlinedClickableSpan() {
                        @Override
                        public void onClick(View textView) {
                            NotificationCenter.getInstance().postNotification(NotificationCenter.didViewProfileByLinkRequested, getFromId());
                        }
                    };
                    this.text.setSpan(clickableSpan, 0, username.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                }
                if(u.id != myId)
                {
                    ClickableSpan clickableSpan = new NonUnderlinedClickableSpan() {
                        @Override
                        public void onClick(View textView) {
                            NotificationCenter.getInstance().postNotification(NotificationCenter.didViewProfileByLinkRequested, u.id);
                        }
                    };
                    this.text.setSpan(clickableSpan, text.length()-username2.length(), text.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                }
            }
        }
        else
        {
            SpannableString secondPart = new SpannableString("");
            for(final TdApi.User u : ((TdApi.MessageChatAddParticipants)msgContent).participants)
            {
                String username = u.id == myId ? TelegramApplication.sharedApplication().getResources().getString(R.string.you) : String.format("%s %s", u.firstName, u.lastName);
                SpannableString txt = new SpannableString(username);
                if(secondPart.length() == 0)
                    secondPart = (SpannableString) TextUtils.concat(secondPart, txt);
                else
                    secondPart = (SpannableString) TextUtils.concat(secondPart, ", ", txt);
                        ClickableSpan clickableSpan = new NonUnderlinedClickableSpan() {
                            @Override
                            public void onClick(View textView) {
                                NotificationCenter.getInstance().postNotification(NotificationCenter.didViewProfileByLinkRequested, u.id);
                            }
                        };
                if(u.id != myId)
                    secondPart.setSpan(clickableSpan, secondPart.length()-txt.length(), secondPart.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }
            this.text = (SpannableString) TextUtils.concat(getFromId() == myId ? TelegramApplication.sharedApplication().getResources().getString(R.string.you) : getFromUsername());
            if(getFromId() != myId)
            {
                ClickableSpan clickableSpan = new NonUnderlinedClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        NotificationCenter.getInstance().postNotification(NotificationCenter.didViewProfileByLinkRequested, getFromId());
                    }
                };
                this.text.setSpan(clickableSpan, 0, getFromUsername().length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }
        }

    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageChatAddParticipants.CONSTRUCTOR;
    }

    @Override
    public boolean isChatServiceMessage() {
        return true;
    }
}
