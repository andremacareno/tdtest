package com.andremacareno.tdtest.models;

import android.util.Log;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.TelegramApplication;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class AnimationMessageModel extends MessageModel {
    private TdApi.Animation animation;

    private String anim_key;
    private int lpWidth, lpHeight;
    public AnimationMessageModel(TdApi.Message msg) {
        super(msg);
    }


    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        this.animation = ((TdApi.MessageAnimation) msgContent).animation;
        this.anim_key = CommonTools.makeFileKey(this.animation.animation.id);

        int screenWidth = TelegramApplication.sharedApplication().getResources().getDisplayMetrics().widthPixels;
        int screenHeight = TelegramApplication.sharedApplication().getResources().getDisplayMetrics().heightPixels;
        float thumbAspectRatio = (float) animation.width / animation.height;
        if(animation.width > Math.min(screenWidth, screenHeight)*0.6f)
        {
            lpWidth = Math.round((float) Math.min(screenWidth, screenHeight) * 0.6f);
            lpHeight = Math.round((float) lpWidth / thumbAspectRatio);
        }
        else
        {
            lpWidth = animation.width;
            lpHeight = animation.height;
        }
        if(BuildConfig.DEBUG)
            Log.d("MessageAnimation", String.format("lpWidth = %d; lpHeight = %d", lpWidth, lpHeight));
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageAnimation.CONSTRUCTOR;
    }
    public TdApi.Animation getAnimation() { return this.animation; }

    public String getKey() { return this.anim_key; }
    public int getLayoutParamsWidth() { return this.lpWidth; }
    public int getLayoutParamsHeight() { return this.lpHeight; }
}
