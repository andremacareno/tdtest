package com.andremacareno.tdtest.models;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 12/05/15.
 */
public class ChatParticipantModel extends UserModel {
    private int inviterId;
    private TdApi.ChatParticipantRole role;
    public ChatParticipantModel(UserModel u, TdApi.ChatParticipantRole role)
    {
        super(u);
        this.role = role;
        this.inviterId = 0;
    }
    public ChatParticipantModel(TdApi.ChatParticipant u) {
        super(u.user);
        this.role = u.role;
        this.inviterId = u.inviterId;
        this.botInfo = u.botInfo;
    }
    public TdApi.ChatParticipantRole getCurrentRole() { return this.role; }
    public void setRole(TdApi.ChatParticipantRole role) { this.role = role; }
    public int getInviterId() { return this.inviterId; }
}
