package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.CommonTools;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class GeoPointMessageModel extends MessageModel {
    private TdApi.Location location;
    private String locationKey;
    public GeoPointMessageModel(TdApi.Message msg) {
        super(msg);
    }

    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        this.location = ((TdApi.MessageLocation) msgContent).location;
        this.locationKey = CommonTools.generateLocationKey(this.location, false);
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageLocation.CONSTRUCTOR;
    }

    public TdApi.Location getLocation() { return this.location; }
    public String getLocationKey() { return this.locationKey; }
}
