package com.andremacareno.tdtest.models;

/**
 * Created by andremacareno on 22/08/15.
 */
public class AdditionalActionInviteToChat implements AdditionalSendAction {
    private int userId;
    public AdditionalActionInviteToChat(int user)
    {
        this.userId = user;
    }

    @Override
    public DataType getDataType() {
        return DataType.INVITE_TO_CHAT;
    }
    public int getUserId() { return this.userId; }
}
