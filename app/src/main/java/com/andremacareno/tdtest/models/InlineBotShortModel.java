package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.files.FileCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 12/08/15.
 */
public class InlineBotShortModel {
    private TdApi.User boundUser;
    private String botName;
    private String botUsername;
    private String initials;
    private FileModel photo;
    public InlineBotShortModel(TdApi.User inlineBot)
    {
        this.boundUser = inlineBot;
        this.botName = String.format("%s %s", inlineBot.firstName, inlineBot.lastName);
        this.botUsername = "@".concat(inlineBot.username);
        this.initials = CommonTools.getInitialsFromUserName(inlineBot.firstName, inlineBot.lastName);
        TdApi.File file = CommonTools.isLowDPIScreen() ? inlineBot.profilePhoto.small : inlineBot.profilePhoto.big;
        photo = FileCache.getInstance().getFileModel(file);
    }

    public String getBotName() { return this.botName; }
    public String getBotUsername() { return this.botUsername; }
    public String getInitials() { return this.initials; }
    public FileModel getPhoto() { return this.photo; }
    public TdApi.User getUserObj() { return this.boundUser; }
}
