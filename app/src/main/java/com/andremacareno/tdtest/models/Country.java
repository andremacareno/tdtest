package com.andremacareno.tdtest.models;

/**
 * Created by Andrew on 23.04.2015.
 */
public class Country {
    private String code;
    private String fullName;
    private String codeWithPlus;

    public Country(String c, String fn)
    {
        this.code = c;
        this.codeWithPlus = "+".concat(c);
        this.fullName = fn;
    }

    public String getCodeWithPlus() { return this.codeWithPlus; }
    public String getCode() { return this.code; }
    public String getFullName() { return this.fullName; }
}
