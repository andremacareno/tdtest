package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.files.FileCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 15/08/15.
 */
public class AudioTrackModel implements SharedMedia {
    private String title;
    private String performer;
    private FileModel fileModel;
    private int duration;
    private int msgId;
    private long date;
    public AudioTrackModel(int msg_id, int date, TdApi.Audio track)
    {
        this.title = track.title == null || track.title.isEmpty() ? "Unnamed track" : track.title;
        this.date = ((long) date) * 1000;
        this.performer = track.performer == null || track.performer.isEmpty() ? "Unknown artist" : track.performer;
        this.duration = track.duration * 1000;
        this.fileModel = FileCache.getInstance().getFileModel(track.audio);
        this.msgId = msg_id;
    }

    public FileModel getTrack() { return this.fileModel; }
    public String getSongName() { return this.title; }
    public String getSongPerformer() { return this.performer; }
    public int getDuration() { return this.duration; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AudioTrackModel that = (AudioTrackModel) o;

        if (!fileModel.equals(that.fileModel)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return fileModel.hashCode();
    }

    @Override
    public int getMessageId() {
        return msgId;
    }

    @Override
    public long getDate() {
        return date;
    }

    @Override
    public boolean isDummy() {
        return false;
    }
    @Override
    public String toString() { return String.format("%s - %s", title, performer); }
}
