package com.andremacareno.tdtest.models;

import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.UserCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 18.04.2015.
 */
public class ChannelChatModel extends ChatModel{
    private boolean is_supergroup;
    private boolean left;
    public ChannelChatModel(TdApi.Chat ch)
    {
        super(ch);
        this.is_supergroup = ch.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR ? ((TdApi.ChannelChatInfo) ch.type).channel.isSupergroup : false;
        this.left = ((TdApi.ChannelChatInfo) ch.type).channel.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor() || ((TdApi.ChannelChatInfo) ch.type).channel.role.getConstructor() != Constants.PARTICIPANT_KICKED.getConstructor();
        if(isChatService() || !is_supergroup)
            return;
        String firstName = ch.topMessage.fromId == CurrentUserModel.getInstance().getUserModel().getId() ? TelegramApplication.sharedApplication().getResources().getString(R.string.you) : UserCache.getInstance().getUserById(ch.topMessage.fromId).getFirstName();
        this.top_message = new SpannableString(String.format("%s: %s", firstName, this.top_message));
        this.top_message.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TelegramApplication.sharedApplication().getApplicationContext(), R.color.list_of_chats_system_text)), 0, firstName.length()+1,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }
    @Override
    public void updateTopMessage(TdApi.Message msg)
    {
        super.updateTopMessage(msg);
        if(isChatService() || !is_supergroup)
            return;
        String firstName = msg.fromId == CurrentUserModel.getInstance().getUserModel().getId() ? TelegramApplication.sharedApplication().getResources().getString(R.string.you) : UserCache.getInstance().getUserById(msg.fromId).getFirstName();
        this.top_message = new SpannableString(String.format("%s: %s", firstName, this.top_message));
        this.top_message.setSpan(ContextCompat.getColor(TelegramApplication.sharedApplication().getApplicationContext(), R.color.list_of_chats_system_text), 0, firstName.length()+1,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }
    public void updateTopMessageContent(TdApi.MessageContent newContent)
    {
        this.is_special_message = newContent.getConstructor() != TdApi.MessageText.CONSTRUCTOR;
        if (isChatService() || !is_supergroup)
            this.top_message = new SpannableString(CommonTools.messageToText(this.msgRef));
        else {
            String firstName = msgRef.fromId == CurrentUserModel.getInstance().getUserModel().getId() ? TelegramApplication.sharedApplication().getResources().getString(R.string.you) : UserCache.getInstance().getUserById(msgRef.fromId).getFirstName();
            this.top_message = new SpannableString(String.format("%s: %s", firstName, CommonTools.messageToText(this.msgRef)));
            this.top_message.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TelegramApplication.sharedApplication().getApplicationContext(), R.color.list_of_chats_system_text)), 0, firstName.length() + 1,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }
    public void updateTitle(String newTitle)
    {
        this.display_name = newTitle;
        this.initials = newTitle.length() >= 1 ? newTitle.substring(0, 1).toUpperCase() : newTitle;
    }
    public boolean isLeft() { return this.left; }
    @Override
    public boolean isGroupChat() { return false; }
    @Override
    public boolean isSupergroup() { return this.is_supergroup; }
    @Override
    public boolean isChannel() { return true; }
}
