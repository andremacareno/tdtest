package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.Constants;

/**
 * Created by andremacareno on 16/08/15.
 */
public class GalleryImageEntry {
    private String path;
    private String cacheKey;
    private int id;
    public GalleryImageEntry(String path, int id)
    {
        this.path = path;
        this.cacheKey = String.format(Constants.GALLERY_PHOTO_KEY, id);
        this.id = id;
    }
    public String getPath() { return this.path; }
    public String getCacheKey() { return this.cacheKey; }
    public int getThumbId() { return this.id; }
}
