package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.files.FileCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class VideoMessageModel extends MessageModel {
    private int duration;
    private PhotoSizeModel thumb;
    private int layoutParamsWidth, layoutParamsHeight;
    private FileModel fileRef;
    private String thumbKey;
    public VideoMessageModel(TdApi.Message msg) {
        super(msg);
    }

    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        TdApi.Video video = ((TdApi.MessageVideo) msgContent).video;
        this.duration = video.duration;
        this.thumb = new PhotoSizeModel(video.thumb);
        int screenWidth = TelegramApplication.sharedApplication().getResources().getDisplayMetrics().widthPixels;
        int screenHeight = TelegramApplication.sharedApplication().getResources().getDisplayMetrics().heightPixels;
        float thumbAspectRatio = (float) thumb.getWidth() / thumb.getHeight();
        layoutParamsWidth = Math.round((float) Math.min(screenWidth, screenHeight) * 0.6f);
        layoutParamsHeight = Math.round((float) layoutParamsWidth / thumbAspectRatio);

        this.fileRef = FileCache.getInstance().getFileModel(video.video);
        this.thumbKey = CommonTools.makeFileKey(thumb.getPhoto().getFileId());
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageVideo.CONSTRUCTOR;
    }
    public int getLayoutParamsWidth() { return this.layoutParamsWidth; }
    public int getLayoutParamsHeight() { return this.layoutParamsHeight; }
    public PhotoSizeModel getThumb() { return this.thumb; }
    public String getThumbKey() { return this.thumbKey; }
    public int getDuration() { return this.duration; }
    public FileModel getVideoFileRef() { return this.fileRef; }
}
