package com.andremacareno.tdtest.models;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 22/08/15.
 */
public class AdditionalActionSendContact implements AdditionalSendAction {
    private TdApi.InputMessageContact contactToSend;
    public AdditionalActionSendContact(TdApi.InputMessageContact msg)
    {
        this.contactToSend = msg;
    }

    @Override
    public DataType getDataType() {
        return DataType.SEND_CONTACT;
    }
    public TdApi.InputMessageContact getShareableContact() { return this.contactToSend; }
}
