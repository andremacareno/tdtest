package com.andremacareno.tdtest.models;

import java.util.List;

/**
 * Created by andremacareno on 22/08/15.
 */
public class AdditionalActionForwardChatMessages extends AdditionalActionForward {
    private List<MessageModel> messages;
    private long sourceChatId;
    private String infoTitle;
    public AdditionalActionForwardChatMessages(List<MessageModel> messages, int[] msgIds, long src, String fwdInfoTitle)
    {
        this.messageIds = msgIds;
        this.messages = messages;
        this.sourceChatId = src;
        this.infoTitle = fwdInfoTitle;
    }

    @Override
    public long getSourceChatId() { return this.sourceChatId; }

    @Override
    public boolean fromSharedMedia() {
        return false;
    }
    public List<MessageModel> getMessages() { return this.messages; }
    @Override
    public int getForwardedCount() { return this.messages.size(); }
    public String getFwdInfoTitle() { return this.infoTitle; }
}
