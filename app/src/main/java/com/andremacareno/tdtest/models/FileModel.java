package com.andremacareno.tdtest.models;

import android.util.Log;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.MessageVoicePlayer;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.concurrent.ConcurrentHashMap;

public class FileModel {
    //this delegate is designed to be used in UI thread
    public interface FileModelDelegate
    {
        public String getDelegateKey();
        public void didFileDownloadStarted();
        public void didFileProgressUpdated();
        public void didFileDownloaded();
        public void didFileDownloadCancelled();
    }
    public enum DownloadState {EMPTY, LOADING, LOADED}
    private int id;
    private volatile DownloadState is_downloaded;
    private TdApi.File fileRef;
    private int fullSize;
    private int downloadedSize;
    private final ConcurrentHashMap<String, FileModelDelegate> downloadDelegates = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, MessageVoicePlayer.VoiceMessageDelegate> voiceMsgDelegates = new ConcurrentHashMap<>();
    private FileModel(TdApi.File f)
    {
        if(BuildConfig.DEBUG)
            Log.d("FileModel", f.toString());
        this.id = f.id;
        this.is_downloaded = CommonTools.isNonDownloadedFile(f) ? DownloadState.EMPTY : DownloadState.LOADED;
        this.fileRef = f;
        this.fullSize = f.size;
        this.downloadedSize = this.is_downloaded == DownloadState.EMPTY ? 0 : f.size;
    }
    public int getFileId() { return this.id; }
    public boolean isDownloaded() { return this.is_downloaded == DownloadState.LOADED; }
    public synchronized void markDownloaded(TdApi.File localFileInstance)
    {
        if(is_downloaded == DownloadState.LOADED)
            return;
        this.fileRef = localFileInstance;
        this.downloadedSize = this.fullSize;
        this.is_downloaded = DownloadState.LOADED;
    }
    public synchronized void markLoading()
    {
        this.is_downloaded = DownloadState.LOADING;
    }
    public synchronized void cancelLoading()
    {
        this.is_downloaded = DownloadState.EMPTY;
    }
    public synchronized void updateLoadedSize(int newSize)
    {
        this.downloadedSize = newSize;
    }
    public DownloadState getDownloadState() { return this.is_downloaded; }
    public TdApi.File getFile()
    {
        return this.fileRef;
    }
    public int getFullSize() { return this.fullSize; }
    public int getDownloadedSize() { return this.downloadedSize; }
    public static FileModel createInstance(TdApi.File f)
    {
        return new FileModel(f);
    }
    public void addDelegate(FileModelDelegate delegate)
    {
        synchronized(downloadDelegates)
        {
            downloadDelegates.put(delegate.getDelegateKey(), delegate);
        }
    }
    public void removeDelegate(FileModelDelegate delegate)
    {
        synchronized(downloadDelegates)
        {
            downloadDelegates.remove(delegate.getDelegateKey());
        }
    }
    public ConcurrentHashMap<String, FileModelDelegate> getDelegates() { return this.downloadDelegates; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileModel fileModel = (FileModel) o;

        if (id != fileModel.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public ConcurrentHashMap<String, MessageVoicePlayer.VoiceMessageDelegate> getVoiceMsgDelegates() { return this.voiceMsgDelegates; }
    public void removeVoiceMsgDelegate(String key) { this.voiceMsgDelegates.remove(key); }
    public void addDelegate(MessageVoicePlayer.VoiceMessageDelegate delegate) { this.voiceMsgDelegates.put(delegate.getDelegateKey(), delegate); }
}
