package com.andremacareno.tdtest.models;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 05/03/16.
 */
public class InlineQueryResultWrap {
    private long inlineQueryResultsId;
    private TdApi.InlineQueryResult result;
    public InlineQueryResultWrap(long resultsId, TdApi.InlineQueryResult result)
    {
        this.inlineQueryResultsId = resultsId;
        this.result = result;
    }
    public long getQueryResultsId() { return this.inlineQueryResultsId; }
    public TdApi.InlineQueryResult getResult() { return this.result; }
}
