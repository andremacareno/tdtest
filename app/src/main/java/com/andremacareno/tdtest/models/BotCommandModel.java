package com.andremacareno.tdtest.models;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 12/08/15.
 */
public class BotCommandModel {
    private UserModel botInfo;
    private TdApi.BotCommand cmd;
    private String displayCmd;
    public BotCommandModel(UserModel userRef, TdApi.BotCommand command)
    {
        this.botInfo = userRef;
        this.cmd = command;
        this.displayCmd = command.command.startsWith("/") ? command.command : "/".concat(command.command);
    }

    public UserModel getBotInfo() { return this.botInfo; }
    public String getDisplayCommand() { return this.displayCmd; }
    public String getCommand() { return this.cmd.command; }
    public String getCmdDescription() { return this.cmd.description; }
}
