package com.andremacareno.tdtest.models;

/**
 * Created by andremacareno on 19/08/15.
 */
public interface SharedMedia {
    public int getMessageId();
    public long getDate();
    public boolean isDummy();
}
