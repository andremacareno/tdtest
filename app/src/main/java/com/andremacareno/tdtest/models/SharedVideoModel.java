package com.andremacareno.tdtest.models;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 19/08/15.
 */
public class SharedVideoModel extends SharedImageModel{
    private String duration;
    public SharedVideoModel(TdApi.Message msg)
    {
        super(msg);
        if(msg.content.getConstructor() != TdApi.MessageVideo.CONSTRUCTOR)
            return;
        TdApi.MessageVideo videos = (TdApi.MessageVideo) msg.content;
        preview = new PhotoSizeModel(videos.video.thumb);
        long minutes = videos.video.duration / 60;
        long seconds = videos.video.duration % 60;
        duration = String.format("%d:%02d", minutes, seconds);
    }

    @Override
    public PhotoSizeModel getOriginal() {
        throw new UnsupportedOperationException("MessageVideo doesn't contain high-res preview");
    }
    public boolean isVideo() {
        return true;
    }
    public String getDuration() { return this.duration; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SharedVideoModel that = (SharedVideoModel) o;

        if (!duration.equals(that.duration)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + duration.hashCode();
        return result;
    }
}
