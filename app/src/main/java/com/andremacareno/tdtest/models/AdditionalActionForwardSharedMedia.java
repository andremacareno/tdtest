package com.andremacareno.tdtest.models;

import java.util.List;

/**
 * Created by andremacareno on 22/08/15.
 */
public class AdditionalActionForwardSharedMedia extends AdditionalActionForward {
    private List<SharedMedia> sharedMedia;
    private long sourceChatId;
    public AdditionalActionForwardSharedMedia(List<SharedMedia> media, int[] msgIds, long src)
    {
        this.sharedMedia = media;
        this.sourceChatId = src;
        this.messageIds = msgIds;
    }

    @Override
    public long getSourceChatId() { return this.sourceChatId; }
    @Override
    public boolean fromSharedMedia() {
        return true;
    }
    public List<SharedMedia> getMessages() { return this.sharedMedia; }
    @Override
    public int getForwardedCount() { return this.sharedMedia.size(); }
}
