package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.files.FileCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 18.04.2015.
 */
public class UserModel {
    private int id;
    private TdApi.LinkState myLink;
    private String display_name;
    private String first_name;
    private String last_name;
    private String txt_status;
    private String initials;
    private String phoneNumber;
    private String formattedPhoneNumber;
    private String username;

    private boolean is_my_contact;
    private boolean know_phone_number;
    private boolean has_my_contacts;
    private boolean online;
    private boolean blocked;
    private boolean can_read_msgs = false;
    private boolean can_join_chats = false;
    protected boolean is_bot;

    private FileModel photo;
    private String photoKey;
    private String photoPlaceholderKey;

    private long profilePhotoId;
    private long lastSeenDate;
    protected TdApi.BotInfo botInfo;
    private boolean deletedAcc;

    public UserModel(TdApi.User u)
    {
        convertUserObject(u, false);
    }
    public UserModel(TdApi.UserFull userFull)
    {
        TdApi.User u = userFull.user;
        convertUserObject(u, false);
        this.blocked = userFull.isBlocked;
        this.botInfo = userFull.botInfo;
    }

    public UserModel(UserModel u) {
        this.id = u.id;
        this.is_bot = u.is_bot;
        if(is_bot) {
            can_read_msgs = u.can_read_msgs;
            can_join_chats = u.can_join_chats;
        }
        this.myLink = u.myLink;
        this.first_name = u.first_name;
        this.last_name = u.last_name;
        this.display_name = u.display_name;
        this.initials = u.initials;
        this.txt_status = u.txt_status;
        this.online = u.online;
        this.lastSeenDate = u.lastSeenDate;
        this.blocked = false;
        this.botInfo = null;
        this.photo = u.photo;
        this.photoKey = u.photoKey;
        this.photoPlaceholderKey = u.photoPlaceholderKey;
        this.phoneNumber = u.phoneNumber;
        this.formattedPhoneNumber = u.formattedPhoneNumber;
        this.username = u.username;
        this.is_my_contact = u.is_my_contact;
        this.know_phone_number = u.know_phone_number;
        this.has_my_contacts = u.has_my_contacts;
        this.profilePhotoId = u.profilePhotoId;
        this.deletedAcc = u.deletedAcc;
    }

    public int getId() { return this.id; }
    public String getDisplayName() { return this.display_name; }
    public String getFirstName() { return this.first_name; }
    public String getLastName() { return this.last_name; }
    public String getStringStatus() { return this.txt_status; }
    public String getInitials() { return this.initials; }
    public boolean isOnline() {
        if(isBot())
            return false;
        return this.online;
    }
    public boolean isBlocked() { return this.blocked; }
    public String getPhotoKey() { return this.photoKey; }
    public FileModel getPhoto() { return this.photo; }
    public String getPhotoPlaceholderKey() { return this.photoPlaceholderKey; }
    public String getPhoneNumber() { return this.phoneNumber; }
    public String getFormattedPhoneNumber() { return this.formattedPhoneNumber; }
    public String getUsername() { return this.username; }
    public String getDisplayUsername() { return "@".concat(this.username); }
    public void addFullInfo(TdApi.UserFull u)
    {
        this.blocked = u.isBlocked;
        this.botInfo = u.botInfo;
    }
    public void setPhoto(TdApi.File f) { this.photo = FileCache.getInstance().getFileModel(f); }
    public void updateStatus(TdApi.UserStatus newStatus)
    {
        this.txt_status = CommonTools.statusToText(newStatus);
        this.online = isOnlineStatus(newStatus);
        this.lastSeenDate = computeLastSeen(newStatus);
    }
    public void updateUser(TdApi.User newData)
    {
        convertUserObject(newData, true);
    }
    public void didRemovedFromContacts()
    {
        this.is_my_contact = false;
    }
    public void updateUserBlocked(boolean newValue)
    {
        this.blocked = newValue;
    }
    public void convertUserObject(TdApi.User u, boolean update)
    {
        this.id = u.id;
        this.is_bot = u.type.getConstructor() == TdApi.UserTypeBot.CONSTRUCTOR;
        if(is_bot) {
            can_read_msgs = ((TdApi.UserTypeBot) u.type).canReadAllGroupChatMessages;
            can_join_chats = ((TdApi.UserTypeBot) u.type).canJoinGroupChats;
        }
        this.myLink = u.myLink;
        this.first_name = u.firstName;
        this.last_name = u.lastName;
        this.display_name = String.format("%s %s", this.first_name, this.last_name);
        this.initials = CommonTools.getInitialsFromUserName(this.first_name, this.last_name);
        this.txt_status = CommonTools.statusToText(u.status);
        this.online = isOnlineStatus(u.status);
        lastSeenDate = computeLastSeen(u.status);
        if(!update) {
            this.blocked = false;
            this.botInfo = null;
        }
        this.photo = FileCache.getInstance().getFileModel(CommonTools.isLowDPIScreen() ? u.profilePhoto.small : u.profilePhoto.big);
        this.photoKey = CommonTools.makeFileKey(this.photo.getFileId());
        this.photoPlaceholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, this.id);
        this.phoneNumber = u.phoneNumber;
        this.formattedPhoneNumber = CommonTools.formatPhoneNumber(u.phoneNumber);
        this.username = u.username;
        this.is_my_contact = u.myLink.getConstructor() == TdApi.LinkStateContact.CONSTRUCTOR;
        this.know_phone_number = u.myLink.getConstructor() == TdApi.LinkStateKnowsPhoneNumber.CONSTRUCTOR;
        this.has_my_contacts = u.foreignLink.getConstructor() == TdApi.LinkStateContact.CONSTRUCTOR;
        this.profilePhotoId = u.profilePhoto.id;
        this.deletedAcc = CommonTools.isDeletedUser(u);
    }

    public static boolean isOnlineStatus(TdApi.UserStatus st)
    {
        if(st.getConstructor() == TdApi.UserStatusOnline.CONSTRUCTOR || st.getConstructor() == TdApi.UserStatusRecently.CONSTRUCTOR)
            return true;
        return false;
    }
    public boolean isBot() { return this.is_bot; }
    public boolean canBotReadMessages() { return this.can_read_msgs; }
    public boolean canBotJoinChats() { return this.can_join_chats; }
    public boolean isMyContact() { return this.is_my_contact; }
    public boolean isPhoneNumberKnown() { return this.know_phone_number; }
    public boolean hasMyContacts() { return this.has_my_contacts; }
    public long getLastSeenDate() { return this.lastSeenDate; }
    public TdApi.LinkState getMyLinkState() { return this.myLink; }
    public TdApi.BotInfo getBotInfo() { return this.botInfo; }
    public long getProfilePhotoId() { return this.profilePhotoId; }
    public boolean isDeletedAccount() { return this.deletedAcc; }
    private long computeLastSeen(TdApi.UserStatus status)
    {
        if(isBot())
            return System.currentTimeMillis() - 2419200001L;
        if(status.getConstructor() == TdApi.UserStatusLastWeek.CONSTRUCTOR)
            return System.currentTimeMillis() - 604800000L;
        else if(status.getConstructor() == TdApi.UserStatusLastMonth.CONSTRUCTOR)
            return System.currentTimeMillis() - 2419200000L;
        else if(status.getConstructor() == TdApi.UserStatusOffline.CONSTRUCTOR)
            return ((long) ((TdApi.UserStatusOffline) status).wasOnline) * 1000L;
        return System.currentTimeMillis();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserModel userModel = (UserModel) o;

        if (id != userModel.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
