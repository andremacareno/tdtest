package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;

/**
 * Created by andremacareno on 20/02/16.
 */
public class MessageSearchResult implements GlobalSearchResult {
    private int id;
    private int peer;
    private long chatId;
    private String date;
    private String from;
    private String messageText;
    private FileModel photo;
    private String photoKey;
    private String initials;
    private String placeholderKey;
    private boolean isService;
    public MessageSearchResult(int messageId, int peer, long chatId, String date, String from, String messageText, boolean isService, FileModel photo, String initials)
    {
        this.id = messageId;
        this.chatId = chatId;
        this.date = date;
        this.from = from;
        this.messageText = messageText;
        this.photo = photo;
        this.isService = isService;
        this.photoKey = CommonTools.makeFileKey(photo.getFileId());
        this.initials = initials;
        this.peer = peer;
        this.placeholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, peer);
    }
    @Override
    public ResultType getResultType() {
        return ResultType.MESSAGE;
    }

    public String getDate() { return this.date; }
    public String getFrom() { return this.from; }
    public int getMessageId() { return this.id; }
    public int getPeer() { return this.peer; }
    public long getChatId() { return this.chatId; }
    public FileModel getPhoto() { return this.photo; }
    public String getPhotoKey() { return this.photoKey; }
    public String getText() { return this.messageText; }
    public String getInitials() { return this.initials; }
    public boolean isServiceMessage() { return this.isService; }
    public String getPlaceholderKey() { return this.placeholderKey; }
}
