package com.andremacareno.tdtest.models;

/**
 * Created by andremacareno on 19/08/15.
 */
public interface GlobalSearchResult {
    interface GlobalSearchDelegate
    {
        void didMessageSelected(MessageSearchResult result);
        void didChatSelected(ChatSearchResult result);
        void loadMoreMessages();
    }
    enum ResultType {CHAT_CONTACT, GLOBAL, MESSAGE, SECTION}
    ResultType getResultType();
}
