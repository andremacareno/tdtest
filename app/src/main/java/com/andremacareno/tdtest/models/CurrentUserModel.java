package com.andremacareno.tdtest.models;

/**
 * Created by Andrew on 11.05.2015.
 */
public class CurrentUserModel {
    private UserModel u;
    private static volatile CurrentUserModel _instance;
    private CurrentUserModel(UserModel u)
    {
        this.u = u;
    }
    public static void createInstance(UserModel u)
    {
        if(_instance == null)
        {
            synchronized(CurrentUserModel.class)
            {
                if(_instance == null)
                    _instance = new CurrentUserModel(u);
            }
        }
    }
    public static CurrentUserModel getInstance()
    {
        return _instance;
    }
    public UserModel getUserModel() { return this.u; }
    public void clearUserModelReference() {

        this.u = null;

    }
    public static void didLoggedOut() {
        synchronized(CurrentUserModel.class)
        {
            if(_instance != null)
                _instance.clearUserModelReference();
            _instance = null;
        }
    }
}
