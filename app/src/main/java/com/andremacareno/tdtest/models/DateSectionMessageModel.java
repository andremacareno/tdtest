package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 22.04.2015.
 */
public class DateSectionMessageModel extends PseudoMessageModel {
    private String date;
    public DateSectionMessageModel(TdApi.Message msg) {
        super(msg);
    }
    public DateSectionMessageModel(int date)
    {
        super(null);
        this.date = CommonTools.dateToDayAndMonth(date);
    }
    @Override
    public int getMessageId()
    {
        return -this.id;
    }
    @Override
    protected void processMessage(TdApi.Message msg) {
        if(msg != null)
            this.date = CommonTools.dateToDayAndMonth(msg.date);
    }

    @Override
    public int getSuitableConstructor() {
        return Constants.MODEL_DATE_SEPARATOR;
    }

    public String getHeaderDate() { return this.date; }

}
