package com.andremacareno.tdtest.models;

/**
 * Created by andremacareno on 22/08/15.
 */
public interface AdditionalSendAction {
    public enum DataType {FWD, REPLY, SEND_CONTACT, INVITE_TO_CHAT}
    public abstract DataType getDataType();
}
