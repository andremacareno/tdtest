package com.andremacareno.tdtest.models;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 15.05.2015.
 */
public class ChatModelFactory {
    public static ChatModel createInstance(TdApi.Chat ch)
    {
        if(ch.type.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR)
            return new GroupChatModel(ch);
        else if(ch.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR)
            return new ChannelChatModel(ch);
        return new ChatModel(ch);
    }
}
