package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.files.FileCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class ContactMessageModel extends MessageModel {
    private int userId;
    private String displayName;
    private String initials;
    private String phoneNumber;
    private FileModel photo = null;
    private String contactPhotoKey = null;

    public ContactMessageModel(TdApi.Message msg) {
        super(msg);
    }


    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        TdApi.MessageContact user = (TdApi.MessageContact) msgContent;
        this.userId = user.userId;
        this.displayName = String.format("%s %s", user.firstName, user.lastName);
        this.initials = CommonTools.getInitialsFromUserName(user.firstName, user.lastName);
        this.phoneNumber = CommonTools.formatPhoneNumber(user.phoneNumber);
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageContact.CONSTRUCTOR;
    }

    public int getContactUserId() { return this.userId; }
    public String getContactDisplayName() { return this.displayName; }
    public String getContactPhoneNumber() { return this.phoneNumber; }
    public String getContactInitials() { return this.initials; }
    public String getContactPhotoKey() { return this.contactPhotoKey; }
    public FileModel getContactPhoto() { return this.photo; }

    public void setContactPhoto(TdApi.File ph) {
        this.photo = FileCache.getInstance().getFileModel(ph);
        if(ph != null)
            this.contactPhotoKey = CommonTools.makeFileKey(this.photo.getFileId());
    }
}
