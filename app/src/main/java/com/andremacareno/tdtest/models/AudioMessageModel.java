package com.andremacareno.tdtest.models;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class AudioMessageModel extends MessageModel {
    private AudioTrackModel audio;

    public AudioMessageModel(TdApi.Message msg) {
        super(msg);
    }

    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        this.audio = new AudioTrackModel(getMessageId(), getIntegerDate(), ((TdApi.MessageAudio) msgContent).audio);
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageAudio.CONSTRUCTOR;
    }

    public FileModel getAudioFileRef() { return this.audio.getTrack(); }
    public AudioTrackModel getTrackModel() { return this.audio; }
    public String getTrackName() { return this.audio.getSongName(); }
    public String getBandName() {
        return this.audio.getSongPerformer();
    }
}
