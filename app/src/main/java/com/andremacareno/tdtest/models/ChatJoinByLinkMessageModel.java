package com.andremacareno.tdtest.models;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.view.View;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.NonUnderlinedClickableSpan;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class ChatJoinByLinkMessageModel extends TextMessageModel {
    public ChatJoinByLinkMessageModel(TdApi.Message msg) {
        super(msg);
    }

    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        if(getFromUsername() == null)
            return;
        String fmt = TelegramApplication.sharedApplication().getResources().getString(R.string.chat_invited_by_link);
        this.text = new SpannableString(String.format(fmt, getFromUsername()));
        if(getFromId() != CurrentUserModel.getInstance().getUserModel().getId())
        {
            ClickableSpan clickableSpan = new NonUnderlinedClickableSpan() {
                @Override
                public void onClick(View textView) {
                    NotificationCenter.getInstance().postNotification(NotificationCenter.didViewProfileByLinkRequested, getFromId());
                }
            };
            this.text.setSpan(clickableSpan, 0, getFromUsername().length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageChatJoinByLink.CONSTRUCTOR;
    }

    @Override
    public boolean isChatServiceMessage() {
        return true;
    }
}
