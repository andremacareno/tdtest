package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.CommonTools;

import java.util.Date;

/**
 * Created by andremacareno on 20/08/15.
 */
public class SharedMediaSectionModel implements SharedMedia {
    @Override
    public int getMessageId() {
        return -1;
    }

    @Override
    public long getDate() {
        return 0;
    }

    private String sectionLabel;
    public SharedMediaSectionModel(Date date)
    {
        this.sectionLabel = CommonTools.dateToMonthAndYear(date);
    }
    public String getSectionLabel() { return this.sectionLabel; }
    @Override
    public boolean isDummy() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SharedMediaSectionModel that = (SharedMediaSectionModel) o;

        if (!sectionLabel.equals(that.sectionLabel)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return sectionLabel.hashCode();
    }
}
