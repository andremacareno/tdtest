package com.andremacareno.tdtest.models;

import android.text.SpannableString;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.PeerType;
import com.andremacareno.tdtest.files.FileCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 18.04.2015.
 */
public class ChatModel {
    private long id;
    private int peer_id;
    private int unreadCount;
    private PeerType type;

    private volatile int lastReadInboxMessageId;
    private volatile int lastReadOutboxMessageId;

    public String display_name;
    private int topMessageId;
    private int topMessageDate;
    protected SpannableString top_message;
    private String top_message_date;
    private String unreadCount_str;
    public String initials;                //for placeholder
    private long offsetOrder;
    private boolean is_unknown_peer;
    protected boolean is_special_message;
    private boolean is_chat_service;
    private boolean is_out;
    private boolean is_bot;
    private FileModel photo;
    private String photoKey;
    private String placeholderKey;
    private int replyMarkupMsgId;
    private boolean muted;
    private long chatOrder;
    protected TdApi.Message msgRef;
    public ChatModel(TdApi.Chat ch)
    {
        this.muted = ch.notificationSettings.muteFor != 0;
        this.display_name = ch.title;
        this.initials = CommonTools.getInitialsByChatInfo(ch);
        this.id = ch.id;
        this.offsetOrder = ch.order;
        is_bot = (ch.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR && ((TdApi.PrivateChatInfo) ch.type).user.type.getConstructor() == TdApi.UserTypeBot.CONSTRUCTOR);
        if(ch.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR) {
            this.peer_id = ((TdApi.PrivateChatInfo) ch.type).user.id;
            this.type = PeerType.USER;
        }
        else if(ch.type.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR) {
            this.peer_id = ((TdApi.GroupChatInfo) ch.type).group.id;
            this.type = PeerType.GROUP;
        }
        else if(ch.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR) {
            this.peer_id = ((TdApi.ChannelChatInfo) ch.type).channel.id;
            this.type = PeerType.CHANNEL;
        }
        this.chatOrder = ch.order;
        this.unreadCount = ch.unreadCount;
        this.is_special_message = ch.topMessage.content.getConstructor() != TdApi.MessageText.CONSTRUCTOR && ch.topMessage.content.getConstructor() != TdApi.MessageWebPage.CONSTRUCTOR;
        this.is_chat_service = CommonTools.isChatServiceMessage(ch.topMessage.content.getConstructor());
        this.topMessageId = ch.topMessage.id;
        this.topMessageDate = ch.topMessage.date;
        this.top_message = new SpannableString(CommonTools.messageToText(ch.topMessage));
        this.top_message_date = CommonTools.dateToString(ch.topMessage.date, true);
        this.lastReadInboxMessageId = ch.lastReadInboxMessageId;
        this.lastReadOutboxMessageId = ch.lastReadOutboxMessageId;
        this.is_unknown_peer = ch.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR && CommonTools.isDeletedUser(((TdApi.PrivateChatInfo) ch.type).user);
        this.unreadCount_str = String.valueOf(unreadCount);
        this.photo = getFile(ch);
        this.photoKey = CommonTools.makeFileKey(photo.getFileId());
        this.placeholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, this.peer_id);
        this.is_out = CurrentUserModel.getInstance() != null && CurrentUserModel.getInstance().getUserModel().getId() == ch.topMessage.fromId;
        this.msgRef = ch.topMessage;
        this.replyMarkupMsgId = ch.replyMarkupMessageId;
    }

    public boolean isMuted() { return this.muted; }
    public String getDisplayName() { return this.display_name; }
    public String getInitials() { return this.initials; }
    public long getId() {
        return id;
    }
    public int getPeer() { return this.peer_id; }
    public String getKey() { return this.photoKey; }
    public int getUnreadCount() {
        return unreadCount;
    }
    public String getStringUnreadCount() {
        return unreadCount_str;
    }
    public int getLastReadInboxMessageId() {
        return lastReadInboxMessageId;
    }
    public int getLastReadOutboxMessageId() {
        return lastReadOutboxMessageId;
    }

    public int getTopMessageId() { return topMessageId; }
    public int getTopMessageDateInt() { return topMessageDate; }
    public SpannableString getTopMessage() {
        return top_message;
    }
    public String getTopMessageDate() { return this.top_message_date; }
    public boolean isOut() { return this.is_out; }
    public boolean isUnknownPeer() { return this.is_unknown_peer; }
    public boolean isGroupChat() { return false; }
    public boolean isChannel() { return false; }
    public boolean isSupergroup() { return false; }
    public boolean isServiceTopMessage() {
        return is_special_message;
    }
    public FileModel getPhoto() { return this.photo; }
    public String getPlaceholderKey() { return this.placeholderKey; }
    public int getReplyMarkupMsgId() { return this.replyMarkupMsgId; }
    public void updateChatLastReadInboxMessageId(int newLastReadInboxMessageId)
    {
        this.lastReadInboxMessageId = newLastReadInboxMessageId;
    }
    public void updateChatLastReadOutboxMessageId(int newLastReadOutboxMessageId)
    {
        this.lastReadOutboxMessageId = newLastReadOutboxMessageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChatModel that = (ChatModel) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    private static FileModel getFile(TdApi.Chat chat)
    {
        return FileCache.getInstance().getFileModel(CommonTools.isLowDPIScreen()? chat.photo.small : chat.photo.big);
    }

    public void updateTopMessage(TdApi.Message msg)
    {
        this.msgRef = msg;
        this.topMessageId = msg.id;
        this.topMessageDate = msg.date;
        this.top_message = new SpannableString(CommonTools.messageToText(msg));
        this.top_message_date = CommonTools.dateToString(msg.date, true);
        this.is_special_message = msg.content.getConstructor() != TdApi.MessageText.CONSTRUCTOR;
        this.is_out = CurrentUserModel.getInstance() != null && CurrentUserModel.getInstance().getUserModel().getId() == msg.fromId;
        if(!is_out && topMessageId > lastReadInboxMessageId) {
            unreadCount++;
            this.unreadCount_str = String.valueOf(unreadCount);
        }
    }
    public PeerType getPeerType() { return this.type; }
    public long getOffsetOrder() { return this.offsetOrder; }
    public void updateTopMessageId(int newMessageId)
    {
        this.topMessageId = newMessageId;
    }
    public void updateTopMessageContent(TdApi.MessageContent newContent)
    {
        this.msgRef.content = newContent;
        this.top_message = new SpannableString(CommonTools.messageToText(this.msgRef));
        this.is_special_message = newContent.getConstructor() != TdApi.MessageText.CONSTRUCTOR;
    }
    public void updateTopMessageDate(int newDate)
    {
        this.topMessageDate = newDate;
        if(newDate != 0)
            this.top_message_date = CommonTools.dateToString(newDate, true);
    }
    public void updateUnreadCount(int newCount)
    {
        this.unreadCount = newCount;
        this.unreadCount_str = String.format("%d", this.unreadCount);
    }
    public void updatePhoto(TdApi.UpdateChatPhoto upd)
    {
        this.photo = FileCache.getInstance().getFileModel(CommonTools.isLowDPIScreen() ? upd.photo.small : upd.photo.big);
        this.photoKey = CommonTools.makeFileKey(photo.getFileId());
    }
    public void updateChatOrder(long newOrder) { this.chatOrder = newOrder; }
    public long getChatOrder() { return this.chatOrder; }
    public boolean isBot() { return this.is_bot; }
    public void updateReplyMarkupMsgId(int newId)
    {
        this.replyMarkupMsgId = newId;
    }
    public boolean isChatService() { return this.is_chat_service; }
    public void updateMuteFlag(TdApi.NotificationSettings settings) {
        this.muted = settings.muteFor != 0;
    }
}
