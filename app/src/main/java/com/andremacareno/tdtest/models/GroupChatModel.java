package com.andremacareno.tdtest.models;

import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.UserCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 18.04.2015.
 */
public class GroupChatModel extends ChatModel{
    private boolean left;
    public GroupChatModel(TdApi.Chat ch)
    {
        super(ch);
        if(isChatService())
            return;
        String firstName;
        try
        {
            firstName = ch.topMessage.fromId == CurrentUserModel.getInstance().getUserModel().getId() ? TelegramApplication.sharedApplication().getResources().getString(R.string.you) : UserCache.getInstance().getUserById(ch.topMessage.fromId).getFirstName();
        }
        catch(Exception e) { e.printStackTrace(); firstName = ""; }
        this.top_message = new SpannableString(String.format("%s: %s", firstName, this.top_message));
        this.top_message.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TelegramApplication.sharedApplication().getApplicationContext(), R.color.list_of_chats_system_text)), 0, firstName.length()+1,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        this.left = ch.type.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR && ((TdApi.GroupChatInfo) ch.type).group.role.getConstructor() == Constants.PARTICIPANT_KICKED.getConstructor() || ((TdApi.GroupChatInfo) ch.type).group.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor();
    }
    @Override
    public void updateTopMessage(TdApi.Message msg)
    {
        super.updateTopMessage(msg);
        if(isChatService())
            return;
        try
        {
            String firstName = msg.content.getConstructor() == TdApi.MessageDeleted.CONSTRUCTOR ? "" : (msg.fromId == CurrentUserModel.getInstance().getUserModel().getId() ? TelegramApplication.sharedApplication().getResources().getString(R.string.you) : UserCache.getInstance().getUserById(msg.fromId).getFirstName());
            this.top_message = new SpannableString(String.format("%s: %s", firstName, CommonTools.messageToText(msg)));
            this.top_message.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TelegramApplication.sharedApplication().getApplicationContext(), R.color.list_of_chats_system_text)), 0, firstName.length()+1,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        catch(Exception ignored) {}
    }
    public void updateTopMessageContent(TdApi.MessageContent newContent) {
        this.msgRef.content = newContent;
        this.is_special_message = newContent.getConstructor() != TdApi.MessageText.CONSTRUCTOR;
        if (isChatService())
            this.top_message = new SpannableString(CommonTools.messageToText(this.msgRef));
        else {
            String firstName = msgRef.fromId == CurrentUserModel.getInstance().getUserModel().getId() ? TelegramApplication.sharedApplication().getResources().getString(R.string.you) : UserCache.getInstance().getUserById(msgRef.fromId).getFirstName();
            this.top_message = new SpannableString(String.format("%s: %s", firstName, CommonTools.messageToText(this.msgRef)));
            this.top_message.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TelegramApplication.sharedApplication().getApplicationContext(), R.color.list_of_chats_system_text)), 0, firstName.length() + 1,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    public void updateTitle(String newTitle)
    {
        this.display_name = newTitle;
        this.initials = newTitle.length() >= 1 ? newTitle.substring(0, 1).toUpperCase() : newTitle;
    }
    public void updateLeftState(int newParticipantsCount)
    {
        left = newParticipantsCount == 0;
    }
    public boolean isGroupChat() { return true; }
    public boolean isChatLeft() { return this.left; }
}
