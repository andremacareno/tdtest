package com.andremacareno.tdtest.models;

/**
 * Created by andremacareno on 20/02/16.
 */
public class SectionSearchResult implements GlobalSearchResult {
    private String text;
    public SectionSearchResult(String txt)
    {
        this.text = txt;
    }
    @Override
    public ResultType getResultType() {
        return ResultType.SECTION;
    }
    public String getText() { return this.text; }
}
