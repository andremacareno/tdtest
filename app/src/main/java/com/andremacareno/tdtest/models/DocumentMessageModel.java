package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.files.FileCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class DocumentMessageModel extends MessageModel {
    private FileModel fileRef;
    private String docName;
    private String mimeType;
    private PhotoSizeModel thumb;
    public DocumentMessageModel(TdApi.Message msg) {
        super(msg);
    }


    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        TdApi.MessageDocument doc = (TdApi.MessageDocument) msgContent;
        this.docName = doc.document.fileName == null || doc.document.fileName.length() == 0 ? TelegramApplication.sharedApplication().getResources().getString(R.string.document) : doc.document.fileName;
        this.fileRef = FileCache.getInstance().getFileModel(doc.document.document);
        this.thumb = new PhotoSizeModel(doc.document.thumb);
        this.mimeType = doc.document.mimeType;
    }

    @Override
    public int getSuitableConstructor() {
        return TdApi.MessageDocument.CONSTRUCTOR;
    }

    public FileModel getDocFileRef() { return this.fileRef; }
    public String getDocName() { return this.docName; }
    public PhotoSizeModel getThumbnail() { return this.thumb; }
    public String getMimeType() { return this.mimeType; }
    public boolean hasThumb() { return this.thumb.getPhoto().getFileId() != 0; }
}
