package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.TelegramApplication;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class StickerMessageModel extends MessageModel {
    private TdApi.Sticker stickerObj;
    public StickerMessageModel(TdApi.Message msg) {
        super(msg);
    }


    @Override
    protected void processMessageContent(TdApi.MessageContent msgContent) {
        TdApi.MessageSticker st = (TdApi.MessageSticker) msgContent;
        this.stickerObj = st.sticker;
        int screenWidth = TelegramApplication.sharedApplication().getResources().getDisplayMetrics().widthPixels;
        int screenHeight = TelegramApplication.sharedApplication().getResources().getDisplayMetrics().heightPixels;
        float thumbAspectRatio = (float) stickerObj.width / stickerObj.height;
        if(stickerObj.width >= Math.min(screenWidth, screenHeight))
        {
            stickerObj.width = Math.round((float) Math.min(screenWidth, screenHeight) * 0.6f);
            stickerObj.height = Math.round((float) stickerObj.width / thumbAspectRatio);
        }
    }

    @Override
    public int getSuitableConstructor() { return TdApi.MessageSticker.CONSTRUCTOR; }
    public TdApi.Sticker getSticker() { return this.stickerObj; }
}
