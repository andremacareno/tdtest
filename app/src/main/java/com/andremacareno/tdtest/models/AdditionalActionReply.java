package com.andremacareno.tdtest.models;

/**
 * Created by andremacareno on 22/08/15.
 */
public class AdditionalActionReply implements AdditionalSendAction {
    private MessageModel replyTo;
    public AdditionalActionReply(MessageModel msg)
    {
        this.replyTo = msg;
    }

    @Override
    public DataType getDataType() {
        return DataType.REPLY;
    }
    public MessageModel getReplyTo() { return this.replyTo; }
}
