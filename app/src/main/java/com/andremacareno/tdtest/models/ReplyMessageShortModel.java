package com.andremacareno.tdtest.models;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.UserCache;
import com.andremacareno.tdtest.files.FileCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 24/02/16.
 */
public class ReplyMessageShortModel {
    private FileModel thumb;
    private String username;
    private String textContent;
    private int repliedToMsgId;
    public ReplyMessageShortModel(MessageModel repliedMessage)
    {
        this.repliedToMsgId = repliedMessage.id;
        this.username = repliedMessage.getFromUsername();
        try
        {
            this.textContent = repliedMessage.getTextRepresentation();
        }
        catch(Exception e) { e.printStackTrace(); this.textContent = ""; }
        if(repliedMessage.getSuitableConstructor() == TdApi.MessagePhoto.CONSTRUCTOR)
            thumb = ((PhotoMessageModel) repliedMessage).getPreview().getPhoto();
        else if(repliedMessage.getSuitableConstructor() == TdApi.MessageVideo.CONSTRUCTOR)
            thumb = ((VideoMessageModel) repliedMessage).getThumb().getPhoto();
        else if(repliedMessage.getSuitableConstructor() == TdApi.MessageSticker.CONSTRUCTOR)
            thumb = FileCache.getInstance().getFileModel(((StickerMessageModel) repliedMessage).getSticker().thumb.photo);
        else
            thumb = null;
    }
    public ReplyMessageShortModel(TdApi.Message repliedMessageFull)
    {
        this.repliedToMsgId = repliedMessageFull.id;
        try
        {
            this.username = UserCache.getInstance().getUserById(repliedMessageFull.fromId).getDisplayName();
        }
        catch(Exception e) { this.username = ""; e.printStackTrace(); }
        try
        {
            this.textContent = CommonTools.messageToText(repliedMessageFull);
        }
        catch(Exception e) { e.printStackTrace(); this.textContent = ""; }
        if(repliedMessageFull.content.getConstructor() == TdApi.MessagePhoto.CONSTRUCTOR)
            thumb = FileCache.getInstance().getFileModel(((TdApi.MessagePhoto) repliedMessageFull.content).photo.photos[CommonTools.indexOfSuitablePreview(((TdApi.MessagePhoto) repliedMessageFull.content).photo.photos)].photo);
        else if(repliedMessageFull.content.getConstructor() == TdApi.MessageVideo.CONSTRUCTOR)
            thumb = FileCache.getInstance().getFileModel(((TdApi.MessageVideo) repliedMessageFull.content).video.thumb.photo);
        else if(repliedMessageFull.content.getConstructor() == TdApi.MessageSticker.CONSTRUCTOR)
            thumb = FileCache.getInstance().getFileModel(((TdApi.MessageSticker) repliedMessageFull.content).sticker.thumb.photo);
        else
            thumb = null;
    }
    public FileModel getThumb() { return this.thumb; }
    public String getUsername() { return this.username; }
    public String getText() { return this.textContent; }
    public int getReplyToMessageId() { return this.repliedToMsgId; }
}
