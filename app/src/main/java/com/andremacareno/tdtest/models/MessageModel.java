package com.andremacareno.tdtest.models;

import android.util.Log;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.files.FileCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 17.04.2015.
 */
public abstract class MessageModel {
    protected int id;                             //identifier of this message
    private boolean inChannelChat;
    private boolean inSupergroup;
    private boolean fwdFromChannel;
    private int forwardFromId;                  //if non-zero, id of user who originally sent the message
    private String forwardDate;
    private int fromId;
    private long chatId;
    private int int_date;
    private int int_fwd_date;
    private String date;                        //date (human-readable)

    private String fromUsername;
    private FileModel fromPhoto;
    private String fromInitials;
    private String fromPhotoKey;
    private String fromPlaceholderKey;
    private String asText;

    private String forwardFromUsername;
    private FileModel forwardFromPhoto;
    private String forwardFromInitials;
    private String forwardFromPhotoKey;
    private String forwardFromPlaceholderKey;
    private boolean is_fwd;
    private boolean sentFromThisClient;
    private TdApi.MessageSendState sendState;
    private ReplyMessageShortModel replyTo;
    private String viewsCount;
    private String via_username = null;
    private int via_userId = 0;
    public MessageModel(TdApi.Message msg)
    {
        if(msg == null) {
            this.id = this.forwardFromId = fromId = 0;
            this.chatId = 0;
            return;
        }
        this.id = msg.id;
        this.fwdFromChannel = CommonTools.forwardFromChannel(msg.forwardInfo);
        TdApi.ChatInfo chatInfo = ChatCache.getInstance().getChatById(msg.chatId).type;
        this.inChannelChat = chatInfo.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR;
        if(inChannelChat)
            inSupergroup = ((TdApi.ChannelChatInfo) chatInfo).channel.isSupergroup;
        else
            inSupergroup = false;
        this.forwardFromId = msg.forwardInfo.getConstructor() == TdApi.MessageNotForwarded.CONSTRUCTOR ? 0 : fwdFromChannel ? ((TdApi.MessageForwardedFromChannel) msg.forwardInfo).channelId : ((TdApi.MessageForwardedFromUser) msg.forwardInfo).userId;
        this.forwardFromPlaceholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, this.forwardFromId);
        this.is_fwd = forwardFromId != 0;
        this.int_date = msg.date;
        this.int_fwd_date = msg.forwardInfo.getConstructor() == TdApi.MessageNotForwarded.CONSTRUCTOR ? 0 : fwdFromChannel ? ((TdApi.MessageForwardedFromChannel) msg.forwardInfo).date : ((TdApi.MessageForwardedFromUser) msg.forwardInfo).date;
        if(int_fwd_date != 0)
            this.forwardDate = CommonTools.dateToTimeString(int_fwd_date);
        else
            this.forwardDate = null;
        this.fromId = msg.fromId;
        this.fromPlaceholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, this.fromId);
        this.chatId = msg.chatId;
        this.date = CommonTools.dateToTimeString(msg.date);
        this.sentFromThisClient = false;
        this.sendState = msg.sendState;
        if(inChannelChat || fwdFromChannel)
        {
            generateViewsCountString(msg.views);
        }
        beforeProcessMessageContent();
        processMessageContent(msg.content);
    }

    private void generateViewsCountString(int views) {
        int views_thousands = views / 1000;
        int views_millions = views / 1000000;
        if(views_millions > 1)
            viewsCount = String.format("%dM", views_millions);
        else if(views_thousands >= 1)
            viewsCount = String.format("%dk", views_thousands);
        else
            viewsCount = String.valueOf(views);
    }

    //abstract methods
    protected abstract void processMessageContent(TdApi.MessageContent msgContent);
    public abstract int getSuitableConstructor();
    //setter methods (for updateMessage* updates)
    public void updateId(int id)
    {
        if(BuildConfig.DEBUG)
            Log.d("Message", String.format("updating id from %d to %d", this.id, id));
        this.id = id;
        this.sendState = new TdApi.MessageIsSuccessfullySent();
    }
    public void updateContent(TdApi.MessageContent newContent)
    {
        if(newContent.getConstructor() != getSuitableConstructor())
            return;
        processMessageContent(newContent);
    }
    public void updateViews(TdApi.UpdateMessageViews upd)
    {
        generateViewsCountString(upd.views);
    }
    public boolean isForwarded() { return this.is_fwd; }
    public void markSentFromThisClient() { this.sentFromThisClient = true; }
    public void updateDate(int newDate)
    {
        this.date = CommonTools.dateToTimeString(newDate);
    }
    public void addFromInfo(String fr_u, TdApi.File fr_ph, String initials) {
        if(fr_u == null)
            this.fromUsername = "null";
        else
            this.fromUsername = fr_u;
        this.fromPhoto = FileCache.getInstance().getFileModel(fr_ph);
        this.fromInitials = initials;
        this.fromPhotoKey = CommonTools.makeFileKey(fromPhoto.getFileId());
    }
    public void addForwardFromInfo(String fwd_fr_u, TdApi.File fwd_fr_ph, String initials) {
        this.forwardFromUsername = fwd_fr_u;
        this.forwardFromPhoto = FileCache.getInstance().getFileModel(fwd_fr_ph);
        this.forwardFromInitials = initials;
        this.forwardFromPhotoKey = CommonTools.makeFileKey(forwardFromPhoto.getFileId());
    }
    public void markSending() {
        this.sendState = new TdApi.MessageIsBeingSent();
    }
    public void markSent() {
        this.sendState = new TdApi.MessageIsSuccessfullySent();
    }
    //getter methods
    public int getMessageId() {
        return id;
    }

    public int getForwardFromId() {
        return forwardFromId;
    }

    public String getForwardDate() {
        return forwardDate;
    }

    public int getFromId() {
        return fromId;
    }

    public long getChatId() {
        return chatId;
    }

    public String getDate() {
        return date;
    }
    public String getViewsCount() { return this.viewsCount; }
    public int getIntegerDate() { return this.int_date; }
    public int getIntegerForwardDate() { return this.int_fwd_date; }
    public boolean isSentFromThisClient() { return this.sentFromThisClient; }
    public boolean isChatServiceMessage() {
        //override in derived classes
        return false;
    }
    @Override
    public String toString()
    {
        return String.format("MessageModel: id = %d", this.id);
    }
    public boolean isOut() { return this.fromId == CurrentUserModel.getInstance().getUserModel().getId(); }
    public boolean isPseudoMessage() {
        return false;
    }
    public String getFromUsername() { return this.fromUsername; }
    public FileModel getFromPhoto() { return this.fromPhoto; }
    public String getFromInitials() { return this.fromInitials; }
    public String getFromPhotoKey() { return this.fromPhotoKey; }
    public String getFromPlaceholderKey() { return this.fromPlaceholderKey; }
    public String getForwardFromUsername() { return this.forwardFromUsername; }
    public FileModel getForwardFromPhoto() { return this.forwardFromPhoto; }
    public String getForwardFromInitials() { return this.forwardFromInitials; }
    public String getForwardFromPhotoKey() { return this.forwardFromPhotoKey; }
    public String getForwardFromPlaceholderKey() { return this.forwardFromPlaceholderKey; }
    public boolean forwardedFromChannel() { return this.fwdFromChannel; }
    public void nullForwardInfo() {
        this.fwdFromChannel = false;
        this.is_fwd = false;
        this.int_fwd_date = 0;
        this.forwardDate = this.forwardFromInitials = this.forwardFromPhotoKey = this.forwardFromPlaceholderKey = null;
        this.forwardFromId = 0;
    }
    public void addStringRepresentation(String str) { this.asText = str; }
    public boolean messageInSupergroup() { return this.inSupergroup; }
    public boolean messageInChannelChat() { return this.inChannelChat; }
    public boolean isSending() { return this.sendState.getConstructor() == TdApi.MessageIsBeingSent.CONSTRUCTOR; }
    public int getViaId() { return this.via_userId; }
    public String getViaUsername() { return this.via_username; }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageModel that = (MessageModel) o;

        if (id != that.id) return false;

        return true;
    }
    public String getTextRepresentation() { return this.asText; }
    public void addViaInfo(String username, int user)
    {
        this.via_userId = user;
        this.via_username = username;
    }
    public void addReplyInfo(ReplyMessageShortModel replyInfo) { this.replyTo = replyInfo; }
    public ReplyMessageShortModel getReplyInfo() { return this.replyTo; }
    @Override
    public int hashCode() {
        return id;
    }
    protected void beforeProcessMessageContent() {}
}
