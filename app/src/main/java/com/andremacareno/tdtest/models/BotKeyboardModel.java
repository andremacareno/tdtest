package com.andremacareno.tdtest.models;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

import gnu.trove.map.hash.TIntIntHashMap;

/**
 * Created by andremacareno on 13/08/15.
 */
public class BotKeyboardModel {
    private boolean oneTimeKbd;
    private int rowCount;
    private ArrayList<String> buttons;
    private final TIntIntHashMap rowSizeMap = new TIntIntHashMap();
    private final TIntIntHashMap indexToRow = new TIntIntHashMap();
    public BotKeyboardModel(TdApi.ReplyMarkupShowKeyboard kbd)
    {
        translateKeyboardToModel(kbd);
    }
    public void translateKeyboardToModel(TdApi.ReplyMarkupShowKeyboard kbd)
    {
        this.oneTimeKbd = kbd.oneTime;
        this.rowCount = kbd.rows.length;
        this.buttons = new ArrayList<String>();
        int index = 0;
        for(int i = 0; i < kbd.rows.length; i++) {
            String[] arr = kbd.rows[i];
            for(String str : arr)
            {
                buttons.add(str);
                indexToRow.put(index, i);
                index++;
            }
            rowSizeMap.put(i, arr.length);
        }
    }
    public void updateModel(BotKeyboardModel model)
    {
        this.rowCount = model.getRowCount();
        this.oneTimeKbd = model.isOneTimeKeyboard();
        this.buttons.clear();
        this.buttons.addAll(model.getButtons());
        this.rowSizeMap.clear();
        this.rowSizeMap.putAll(model.getRowSizeMap());
        this.indexToRow.clear();
        this.indexToRow.putAll(model.getIndexToRowMap());
    }
    public int getRowCount() { return this.rowCount; }
    public boolean isOneTimeKeyboard() { return this.oneTimeKbd; }
    public ArrayList<String> getButtons() { return this.buttons; }
    public TIntIntHashMap getRowSizeMap() { return this.rowSizeMap; }
    public TIntIntHashMap getIndexToRowMap() { return this.indexToRow; }
    public int rowOfButton(int position) { return indexToRow.get(position); }
    public int getRowSize(int row) { return this.rowSizeMap.get(row); }
}
