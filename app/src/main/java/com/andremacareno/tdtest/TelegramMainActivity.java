package com.andremacareno.tdtest;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.andremacareno.asynccore.AsyncTaskManager;
import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.auth.AuthStateProcessor;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.fragments.BaseFragment;
import com.andremacareno.tdtest.fragments.EnterCodeFragment;
import com.andremacareno.tdtest.fragments.EnterPasswordFragment;
import com.andremacareno.tdtest.fragments.EnterPhoneFragment;
import com.andremacareno.tdtest.fragments.PasscodeSetFragment;
import com.andremacareno.tdtest.fragments.PlayerFragment;
import com.andremacareno.tdtest.fragments.RecoverAuthPasswordFragment;
import com.andremacareno.tdtest.fragments.RetainFragment;
import com.andremacareno.tdtest.fragments.RosterFragment;
import com.andremacareno.tdtest.fragments.SignUpFragment;
import com.andremacareno.tdtest.images.ImageCache;
import com.andremacareno.tdtest.models.AudioTrackModel;
import com.andremacareno.tdtest.models.CurrentUserModel;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.TelegramAction;
import com.andremacareno.tdtest.viewcontrollers.FwdChatsController;
import com.andremacareno.tdtest.viewcontrollers.passcode.PasscodeViewController;
import com.andremacareno.tdtest.views.BottomSheet;
import com.andremacareno.tdtest.views.ForwardBottomSheetView;
import com.andremacareno.tdtest.views.NavDrawerListener;
import com.andremacareno.tdtest.views.NavDrawerView;
import com.andremacareno.tdtest.views.PasscodeView;
import com.andremacareno.tdtest.views.ViewPhotoLayout;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.player.MiniPlayerView;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import org.drinkless.td.libcore.telegram.TdApi;


public class TelegramMainActivity extends AppCompatActivity implements LogOutDelegate, MiniPlayerView.MiniPlayerDelegate {

    private final String TAG = "MainActivity";
    private AuthStateProcessor.AuthStateDelegate authStateDelegate;
    private AuthStateProcessor.SecondaryAuthDelegate secondaryAuthDelegate;
    private BackPressListener backPressInterceptor;
    private boolean clearTillMainFragment = false;
    private PasscodeViewController passcodeViewController;
    private FrameLayout.LayoutParams lp;
    private NotificationObserver validPasscodeObserver;

    private ActionView actionView = null;
    private ActionView previousActionView = null;
    NavDrawerListener selectionListener;
    NavDrawerView navDrawerView;
    private ImageButton actionButton;
    private DrawerLayout drawerLayout;
    private ViewGroup mainScreen;
    private FrameLayout actionViewPlaceholder;
    private View actionViewShadow;
    private FrameLayout container;
    private FrameLayout passcodeContainer;
    private boolean receivedPhoneStateDueToLogOut = false;
    private PasscodeView passcodeView;
    private MiniPlayerView miniPlayer;
    private ViewPhotoLayout viewPhotoLayout;
    private boolean passcodeWasVisible = false;
    private boolean viewPhotoModeEnabled = false;
    private boolean disableMenuButton = false;
    private boolean searchMode = false;
    private ProgressDialog loggingOutDialog = null;
    private final int bottomSheetColorFrom = Color.argb(0, 0, 0, 0);
    private final int bottomSheetColorTo = Color.argb(190, 0, 0, 0);
    private AudioPlaybackService.PlaybackDelegate miniPlayerDelegate;
    //private Animation actionViewFwdOpen, actionViewFwdExit, actionViewBackOpen, actionViewBackExit;
    private Animation.AnimationListener actionViewAnimListener;
    private boolean actionView_useExitAnimations = false;
    private Runnable removeViewRunnable, closePhotoRunnable;

    //viewed photo info
    private int viewedPhotoFileId = 0;
    private String viewedPhotoUserName = null;
    private int viewedPhotoDateInSeconds = 0;

    //new chat floating action menu
    private FloatingActionMenu fam;
    private FloatingActionButton newChat, newGroup, newChannel;
    private boolean showFam;

    //bottom sheet
    private boolean bottomSheetDismissed = false;
    private final FrameLayout.LayoutParams bottomSheetLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.BOTTOM);
    private final DecelerateInterpolator slideUpInterpolator = new DecelerateInterpolator(1.7f);
    private final AccelerateInterpolator slideDownInterpolator = new AccelerateInterpolator(1.7f);
    private final Animator.AnimatorListener slideDownListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animator) {
        }

        @Override
        public void onAnimationEnd(Animator animator) {
            if(attachedBottomSheet != null) {
                final BottomSheet finalSheet = attachedBottomSheet;
                attachedBottomSheet = null;
                bottomSheetContainer.removeView((View) finalSheet);
                bottomSheetContainer.setVisibility(View.GONE);
                finalSheet.onClose(bottomSheetDismissed);
            }
            bottomSheetDismissed = false;
        }

        @Override
        public void onAnimationCancel(Animator animator) {
        }

        @Override
        public void onAnimationRepeat(Animator animator) {

        }
    };
    private BottomSheet attachedBottomSheet = null;
    private FrameLayout bottomSheetContainer;

    //forwarding bottom sheet
    private ForwardBottomSheetView fwdBottomSheet = null;
    private FwdChatsController fwdController = null;
    private TextWatcher fwdSearchListener = null;
    private boolean restoreFwdBottomSheet = false;
    private ForwardBottomSheetView.ForwardMenuDelegate fwdDelegate = null;
    private Runnable normalWindowMode = new Runnable() {
        @Override
        public void run() {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
    };
    private Runnable specialWindowMode = new Runnable() {
        @Override
        public void run() {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putInt(Constants.ARG_VIEW_PHOTO_DATE, viewedPhotoDateInSeconds);
        if(viewedPhotoUserName != null)
            outState.putString(Constants.ARG_VIEW_PHOTO_USER, viewedPhotoUserName);
        outState.putInt(Constants.ARG_VIEW_PHOTO_FILE_ID, viewedPhotoFileId);
        outState.putBoolean(Constants.ARG_VIEW_PHOTO_MODE_ENABLED, viewPhotoModeEnabled);
        outState.putBoolean(Constants.ARG_MINI_PLAYER_VISIBILITY, miniPlayer != null && miniPlayer.getVisibility() != View.GONE);
        outState.putBoolean(Constants.ARG_PASSCODE_LOCK_WAS_VISIBLE, passcodeContainer != null && passcodeContainer.getVisibility() != View.GONE);
        outState.putBoolean(Constants.ARG_SHOW_CHAT_FAM, showFam);
        outState.putBoolean(Constants.ARG_RESTORE_FWD_BOTTOM_SHEET, restoreFwdBottomSheet);
        super.onSaveInstanceState(outState);
    }
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TelegramApplication.sharedApplication().readTDLibReloadingRecord();
        if(TelegramApplication.sharedApplication().isTDLibReloaded())
        {
            NotificationCenter.stop();
        }
        setContentView(R.layout.activity_main);
        loggingOutDialog = new ProgressDialog(TelegramMainActivity.this);
        loggingOutDialog.setMessage(getResources().getString(R.string.logging_out));
        loggingOutDialog.setIndeterminate(true);
        loggingOutDialog.setCancelable(false);
        miniPlayerDelegate = new AudioPlaybackService.PlaybackDelegate() {
            @Override
            public void didPlaybackStarted(AudioTrackModel nowPlaying) {
                miniPlayer.updateSongData(nowPlaying.getSongName(), nowPlaying.getSongPerformer());
                miniPlayer.setProgressBarMax(nowPlaying.getDuration());
                miniPlayer.updateProgressBar(0);
                miniPlayer.updatePlayButton(true);
            }

            @Override
            public void didPlaybackStopped() {
                try
                {
                    Fragment fr = getSupportFragmentManager().findFragmentByTag(Constants.PLAYLIST_FRAGMENT);
                    if(fr != null && fr.isVisible())
                        clearTillMainFragmentNextTime();
                    hideMiniPlayer();
                }
                catch(Exception e) { e.printStackTrace(); }
            }

            @Override
            public void didPlaybackPaused() {
                miniPlayer.updatePlayButton(false);
            }

            @Override
            public void didPlaybackUnpaused() {
                miniPlayer.updatePlayButton(true);
            }

            @Override
            public void didTrackPositionChanged(int currentPosition) {
                miniPlayer.updateProgressBar(currentPosition);
            }

            @Override
            public void didTrackDownloadFinished() {
            }

            @Override
            public int getDelegateId() { return Constants.MINI_PLAYER_DELEGATE_ID; }
        };
        authStateDelegate = new AuthStateProcessor.AuthStateDelegate() {
            @Override
            public void onLoggingOut(TdApi.AuthStateLoggingOut state) {
                if(loggingOutDialog != null && !loggingOutDialog.isShowing())
                    loggingOutDialog.show();
            }

            @Override
            public void didSetPhoneRequested(TdApi.AuthStateWaitPhoneNumber state) {
                if(loggingOutDialog != null && loggingOutDialog.isShowing())
                    loggingOutDialog.dismiss();
                loadFragment(state);
            }

            @Override
            public void didSetCodeRequested(TdApi.AuthStateWaitCode state) {
                EnterPhoneFragment fr = (EnterPhoneFragment) getSupportFragmentManager().findFragmentByTag(Constants.ENTER_PHONE_FRAGMENT);
                if(fr != null)
                    fr.unlockSetPhoneOnResume();
                loadFragment(state);
            }

            @Override
            public void didSetNameRequested(TdApi.AuthStateWaitName state) {
                loadFragment(state);
            }

            @Override
            public void didSetPasswordRequested(TdApi.AuthStateWaitPassword state) {
                loadFragment(state);
            }

            @Override
            public void didSuccessfullyAuthorized(TdApi.AuthStateOk state) {
                FileCache.getInstance().attachObservers();
                UserCache.getInstance().attachObserver();
                ChatCache.getInstance().attachObserver();
                ChannelCache.getInstance().attachObserver();
                GroupCache.getInstance().attachObserver();
                loadFragment(state);
                if(navDrawerView != null)
                    navDrawerView.initHeader();
            }


            @Override
            public void didErrorOccurred() {
                if(secondaryAuthDelegate != null)
                    secondaryAuthDelegate.processError();
            }
        };

        this.actionButton = (ImageButton) findViewById(R.id.action_button);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mainScreen = (ViewGroup) findViewById(R.id.main_screen);
        bottomSheetContainer = (FrameLayout) mainScreen.findViewById(R.id.bottom_sheet_frame);
        bottomSheetContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDismissed = true;
                slideDownBottomSheet();
            }
        });
        miniPlayer = (MiniPlayerView) mainScreen.findViewById(R.id.mini_player);
        miniPlayer.setDelegate(this);
        if(savedInstanceState != null)
        {
            restoreFwdBottomSheet = savedInstanceState.getBoolean(Constants.ARG_RESTORE_FWD_BOTTOM_SHEET, false);
            showFam = savedInstanceState.getBoolean(Constants.ARG_SHOW_CHAT_FAM, false);
            disableMenuButton = savedInstanceState.getBoolean(Constants.ARG_DISABLE_MENU_BUTTON, false);
            viewedPhotoFileId = savedInstanceState.getInt(Constants.ARG_VIEW_PHOTO_FILE_ID, 0);
            viewedPhotoDateInSeconds = savedInstanceState.getInt(Constants.ARG_VIEW_PHOTO_DATE, 0);
            viewedPhotoUserName = savedInstanceState.getString(Constants.ARG_VIEW_PHOTO_USER, null);
            viewPhotoModeEnabled = savedInstanceState.getBoolean(Constants.ARG_VIEW_PHOTO_MODE_ENABLED, false);
            passcodeWasVisible = savedInstanceState.getBoolean(Constants.ARG_PASSCODE_LOCK_WAS_VISIBLE, false);
            boolean miniPlayerVisibility = savedInstanceState.getBoolean(Constants.ARG_MINI_PLAYER_VISIBILITY, false);
            if(miniPlayerVisibility == true)
                showMiniPlayer(false);
            else
                hideMiniPlayer();
        }
        fam = (FloatingActionMenu) mainScreen.findViewById(R.id.new_chat_menu);
        fam.setMenuButtonColorNormalResId(R.color.header_color);
        fam.setMenuButtonColorPressedResId(R.color.header_color_pressed);
        newChat = new FloatingActionButton(getApplicationContext());
        newChat.setId(R.id.fab_new_private);
        newChat.setButtonSize(FloatingActionButton.SIZE_MINI);
        newChat.setLabelText(getResources().getString(R.string.new_chat));
        newChat.setColorNormalResId(R.color.fab_new_chat);
        newChat.setColorPressedResId(R.color.fab_new_chat);
        newChat.setImageResource(R.drawable.ic_new_private_chat);
        fam.addMenuButton(newChat);

        newGroup = new FloatingActionButton(getApplicationContext());
        newGroup.setId(R.id.fab_new_group);
        newGroup.setButtonSize(FloatingActionButton.SIZE_MINI);
        newGroup.setLabelText(getResources().getString(R.string.new_group));
        newGroup.setColorNormalResId(R.color.fab_new_group);
        newGroup.setColorPressedResId(R.color.fab_new_group);
        newGroup.setImageResource(R.drawable.ic_new_group_chat);
        fam.addMenuButton(newGroup);

        newChannel = new FloatingActionButton(getApplicationContext());
        newChannel.setId(R.id.fab_new_channel);
        newChannel.setButtonSize(FloatingActionButton.SIZE_MINI);
        newChannel.setLabelText(getResources().getString(R.string.new_channel));
        newChannel.setColorNormalResId(R.color.fab_new_channel);
        newChannel.setColorPressedResId(R.color.fab_new_channel);
        newChannel.setImageResource(R.drawable.ic_new_channel);
        fam.addMenuButton(newChannel);
        final View.OnClickListener famClickOutside = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fam.close(true);
                fam.setOnClickListener(null);
                fam.setClickable(false);
            }
        };
        fam.setOnMenuButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(passcodeContainer != null && passcodeContainer.getVisibility() == View.VISIBLE)
                    return;
                if(!fam.isOpened()) {
                    fam.open(true);
                    fam.setOnClickListener(famClickOutside);
                }
                else
                {
                    fam.close(true);
                    fam.setOnClickListener(null);
                    fam.setClickable(false);
                }
            }
        });

        View.OnClickListener famClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int actionToSend;
                if(view.getId() == R.id.fab_new_private)
                    actionToSend = TelegramAction.NAVDRAWER_CONTACTS;
                else if(view.getId() == R.id.fab_new_group)
                    actionToSend = TelegramAction.NAVDRAWER_NEW_GROUP;
                else if(view.getId() == R.id.fab_new_channel)
                {
                    actionToSend = TelegramAction.NAVDRAWER_NEW_CHANNEL;
                }
                else
                    return;
                fam.close(true);
                fam.setOnClickListener(null);
                fam.setClickable(false);
                NotificationCenter.getInstance().postNotificationDelayed(NotificationCenter.didNavDrawerClosed, actionToSend, 200);
            }
        };
        newChat.setOnClickListener(famClickListener);
        newGroup.setOnClickListener(famClickListener);
        newChannel.setOnClickListener(famClickListener);
        if(showFam)
            fam.showMenuButton(false);
        else
            fam.hideMenuButton(false);
        viewPhotoLayout = (ViewPhotoLayout) mainScreen.findViewById(R.id.view_photo);
        actionViewPlaceholder = (FrameLayout) mainScreen.findViewById(R.id.action_bar);
        actionViewShadow = mainScreen.findViewById(R.id.action_bar_shadow);
        container = (FrameLayout) mainScreen.findViewById(R.id.container);
        lp = new FrameLayout.LayoutParams(getResources().getDisplayMetrics().widthPixels, getResources().getDimensionPixelSize(R.dimen.actionview_size));
        lp.gravity = Gravity.TOP;
        navDrawerView = (NavDrawerView) findViewById(R.id.navigation);
        navDrawerView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem menuItem) {
                //drawerLayout.closeDrawer(GravityCompat.START);
                int actionToSend;
                switch(menuItem.getItemId())
                {
                    case R.id.drawer_settings:
                        actionToSend = TelegramAction.NAVDRAWER_SETTINGS;
                        break;
                    case R.id.drawer_logout:
                        actionToSend = TelegramAction.NAVDRAWER_LOG_OUT;
                        break;
                    default:
                        return true;
                }
                NotificationCenter.getInstance().postNotification(NotificationCenter.didNavDrawerActionSelected, actionToSend);
                return true;
            }
        });
        selectionListener = new NavDrawerListener(new Runnable() {
            @Override
            public void run() {
                closeNavDrawer();
            }
        }, new Runnable() {
            @Override
            public void run() {
                if(navDrawerView != null)
                    navDrawerView.refreshAvatar();
            }
        });
        validPasscodeObserver = new NotificationObserver(NotificationCenter.didPasscodeLockPassed) {
            @Override
            public void didNotificationReceived(Notification notification) {
                passcodeCorrect();
            }
        };
        drawerLayout.setDrawerListener(selectionListener);
        NotificationCenter.getInstance().addObserver(validPasscodeObserver);
        NotificationCenter.getInstance().addObserver(selectionListener);



        removeViewRunnable = new Runnable() {
            @Override
            public void run() {
                if(previousActionView != null) {
                    actionViewPlaceholder.removeView(previousActionView);
                    previousActionView = null;
                }
            }
        };
        closePhotoRunnable = new Runnable() {
            @Override
            public void run() {
                closePhotoView();
            }
        };
        actionViewAnimListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(previousActionView != null)
                {
                    try
                    {
                        if(previousActionView.getController() != null)
                            previousActionView.getController().onActionViewRemoved();
                        actionViewPlaceholder.post(removeViewRunnable);
                    }
                    catch(Exception e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
        if(viewPhotoModeEnabled && viewPhotoLayout != null && viewedPhotoFileId > 0) {
            FileModel photo = FileCache.getInstance().getFileModelById(viewedPhotoFileId);
            if(photo != null)
                enableViewPhotoMode(photo, viewedPhotoUserName, viewedPhotoDateInSeconds);
        }
        if(restoreFwdBottomSheet)
            showFwdBottomSheet(RetainFragment.findOrCreateRetainFragment(getSupportFragmentManager()).fwdDelegate);
        /*Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        if(Build.VERSION.SDK_INT >= 21) {
            ((AppBarLayout.LayoutParams) findViewById(R.id.main_collapsing).getLayoutParams()).height += AndroidUtilities.getStatusBarHeight();
            mActionBarToolbar.setPadding(0, AndroidUtilities.getStatusBarHeight(), 0, 0);
        }
        setSupportActionBar(mActionBarToolbar);
        mActionBarToolbar.setNavigationIcon(null);*/
    }
    private void checkNeedShowPasscodeLockScreen()
    {
        if(passcodeContainer != null && passcodeContainer.getVisibility() == View.VISIBLE)
            return;
        long currentTimestamp = System.currentTimeMillis();
        long backgroundTimestamp = TelegramApplication.sharedApplication().getBackgroundTimestamp();
        long diff = currentTimestamp - backgroundTimestamp;
        long autoLockTimeout = TelegramApplication.sharedApplication().sharedPreferences().getInt(Constants.PREFS_AUTOLOCK_TIMEOUT, PasscodeSetFragment.AUTOLOCK_DISABLED);
        boolean passcodeEnabled = TelegramApplication.sharedApplication().sharedPreferences().getBoolean(Constants.PREFS_PASSCODE_LOCK_ENABLED, false);
        if(autoLockTimeout == PasscodeSetFragment.AUTOLOCK_DISABLED && passcodeEnabled && TelegramApplication.sharedApplication().sharedPreferences().getBoolean(Constants.PREFS_LOCK_APP_NEXT_TIME, false))
        {
            if(diff >= 700 || passcodeWasVisible)
                showPasscodeLockScreen();
        }
        else if(passcodeEnabled)
        {
            long reqTimeout;
            if(autoLockTimeout == PasscodeSetFragment.AUTOLOCK_1M)
                reqTimeout = 60000;
            else if(autoLockTimeout == PasscodeSetFragment.AUTOLOCK_5M)
                reqTimeout = 300000;
            else if(autoLockTimeout == PasscodeSetFragment.AUTOLOCK_1H)
                reqTimeout = 3600000;
            else
                reqTimeout = 18000000;
            if(diff >= reqTimeout)
                showPasscodeLockScreen();
        }
    }
    public void showPasscodeLockScreen()
    {
        enableChatFam(false);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initPasscodeView();
        passcodeContainer.addView(passcodeView);
        if(drawerLayout != null) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
        container.setVisibility(View.GONE);
        actionViewPlaceholder.setVisibility(View.GONE);
        passcodeContainer.setVisibility(View.VISIBLE);
        NotificationCenter.getInstance().postNotification(NotificationCenter.didPasscodeLockScreenShown, null);
    }
    public void passcodeCorrect()
    {
        passcodeContainer.setVisibility(View.GONE);
        passcodeContainer.removeView(passcodeView);
        container.setVisibility(View.VISIBLE);
        actionViewPlaceholder.setVisibility(View.VISIBLE);
        SharedPreferences.Editor prefEdit = TelegramApplication.sharedApplication().sharedPreferences().edit();
        prefEdit.putBoolean(Constants.PREFS_LOCK_APP_NEXT_TIME, false);
        prefEdit.apply();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
        Fragment fr = getSupportFragmentManager().findFragmentByTag(Constants.ROSTER_FRAGMENT);
        if(fr != null && fr.isVisible())
            enableNavDrawer();
    }
    @Override
    public void onAttachFragment(Fragment fr)
    {
        super.onAttachFragment(fr);
        if(drawerLayout != null) {
            if (!fr.getTag().equals(Constants.ROSTER_FRAGMENT))
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        FileCache.getInstance().didUiThreadPaused();
        TelegramApplication.sharedApplication().notifyAppBecameBackground();
        AuthStateProcessor.getInstance().pauseHandler();
        TelegramApplication.sharedApplication().stopConnectionChangeBroadcasting();
    }
    @Override
    protected void onStart()
    {
        super.onStart();
        checkNeedShowPasscodeLockScreen();
    }
    @Override
    protected void onResume()
    {
        super.onResume();
        if(TelegramApplication.sharedApplication().isTDLibReloaded())
        {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "activity knows that tdlib was reloaded");
            didTDLibReloaded();
        }
        FileCache.getInstance().didUiThreadResumed();
        TelegramApplication.sharedApplication().startConnectionChangeBroadcasting();
        AuthStateProcessor.getInstance().resumeHandler();
        NotificationObserver serviceConnectedObserver = new NotificationObserver(NotificationCenter.didUpdateServiceStarted) {
            @Override
            public void didNotificationReceived(Notification notification) {
                NotificationCenter.getInstance().removeObserver(this);
                AuthStateProcessor.getInstance().setDelegate(authStateDelegate);
                AuthStateProcessor.getInstance().getAuthState();
            }
        };
        NotificationCenter.getInstance().addObserver(serviceConnectedObserver);
        TelegramApplication.sharedApplication().startUpdateService();
        restoreMiniPlayerState();
        restoreMiniPlayerVisibility();
    }
    @Override

    protected void onRestart()
    {
        super.onRestart();
        TelegramApplication.sharedApplication().confirmTDLibNotReloading();
        checkNeedShowPasscodeLockScreen();
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if(loggingOutDialog.isShowing())
            loggingOutDialog.dismiss();
        if(selectionListener != null)
            NotificationCenter.getInstance().removeObserver(selectionListener);
        if(bottomSheetContainer != null)
            bottomSheetContainer.removeAllViews();
        TelegramApplication.sharedApplication().confirmTDLibNotReloading();
        FileCache.getInstance().removeObservers();
        AudioPlaybackService audioPlaybackService = AudioPlaybackService.getInstance();
        if(audioPlaybackService != null) {
            audioPlaybackService.removePlaybackDelegate(miniPlayerDelegate);
        }
        if(drawerLayout != null)
            drawerLayout.setDrawerListener(null);
        NotificationCenter.getInstance().removeObserver(validPasscodeObserver);
        if(actionView != null) {
            actionViewPlaceholder.removeView(actionView);
            actionView = null;
        }
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        TelegramApplication.sharedApplication().writeThatTDLibMightBeReloaded();
    }
    private void loadFragment(TdApi.AuthState state) {
        int constructor = state.getConstructor();
        String tag;
        if(constructor == TdApi.AuthStateOk.CONSTRUCTOR) {
            tag = Constants.ROSTER_FRAGMENT;
            SharedPreferences.Editor prefEdit = TelegramApplication.sharedApplication().sharedPreferences().edit();
            prefEdit.remove(Constants.PREFS_PHONE);
            prefEdit.apply();
        }
        else if(constructor == TdApi.AuthStateWaitCode.CONSTRUCTOR)
            tag = Constants.ENTER_CODE_FRAGMENT;
        else if(constructor == TdApi.AuthStateWaitName.CONSTRUCTOR)
            tag = Constants.SIGN_UP_FRAGMENT;
        else if(constructor == TdApi.AuthStateWaitPassword.CONSTRUCTOR) {
            if(((TdApi.AuthStateWaitPassword) state).emailUnconfirmedPattern == null || ((TdApi.AuthStateWaitPassword) state).emailUnconfirmedPattern.length() == 0)
                tag = Constants.ENTER_PASSWORD_FRAGMENT;
            else
                tag = Constants.RECOVER_AUTH_PASSWORD_FRAGMENT;
        }
        else
            tag = Constants.ENTER_PHONE_FRAGMENT;
        if(BuildConfig.DEBUG)
            Log.d(TAG, "loadFragment");
        Fragment check = getSupportFragmentManager().findFragmentByTag(tag);
        if(check != null) {
            if(!TelegramApplication.sharedApplication().isTDLibReloaded())
                return;
        }
        Fragment fr;
        if(constructor == TdApi.AuthStateOk.CONSTRUCTOR)
            fr = new RosterFragment();
        else if(constructor == TdApi.AuthStateWaitCode.CONSTRUCTOR) {
            fr = new EnterCodeFragment();
            Bundle args = new Bundle();
            args.putString(Constants.ARG_PHONE_NUMBER, getPhoneNumber(secondaryAuthDelegate));
            fr.setArguments(args);
        }
        else if(constructor == TdApi.AuthStateWaitName.CONSTRUCTOR) {
            fr = new SignUpFragment();
            Bundle args = new Bundle();
            args.putString(Constants.ARG_PHONE_NUMBER, getPhoneNumber(secondaryAuthDelegate));
            fr.setArguments(args);
        }
        else if(constructor == TdApi.AuthStateWaitPassword.CONSTRUCTOR) {
            Bundle args = new Bundle();
            TdApi.AuthStateWaitPassword authState = (TdApi.AuthStateWaitPassword) state;
            if(authState.emailUnconfirmedPattern == null || authState.emailUnconfirmedPattern.length() == 0)
            {
                args.putBoolean(Constants.ARG_HAS_RECOVERY_EMAIL, authState.hasRecovery);
                args.putString(Constants.ARG_PASSWORD_HINT, authState.hint);
                fr = new EnterPasswordFragment();
                fr.setArguments(args);
            }
            else
            {
                args.putString(Constants.ARG_EMAIL_PATTERN, authState.emailUnconfirmedPattern);
                fr = new RecoverAuthPasswordFragment();
                fr.setArguments(args);
            }
        }
        else if(constructor == TdApi.AuthStateWaitPhoneNumber.CONSTRUCTOR)
            fr = new EnterPhoneFragment();
        else
            return;
        fr.setRetainInstance(true);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction tr = fm.beginTransaction();
        if(constructor == TdApi.AuthStateWaitPhoneNumber.CONSTRUCTOR)
        {
            if(receivedPhoneStateDueToLogOut)
                tr.setCustomAnimations(R.anim.fragment_close_enter, R.anim.fragment_close_exit);
        }
        else
            tr.setCustomAnimations(R.anim.fragment_open_enter, R.anim.fragment_open_exit, R.anim.fragment_close_enter, R.anim.fragment_close_exit);
        if(constructor == TdApi.AuthStateOk.CONSTRUCTOR || (constructor == TdApi.AuthStateWaitPhoneNumber.CONSTRUCTOR && receivedPhoneStateDueToLogOut))
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        else if(constructor == TdApi.AuthStateWaitPassword.CONSTRUCTOR && (((TdApi.AuthStateWaitPassword) state).emailUnconfirmedPattern == null || ((TdApi.AuthStateWaitPassword) state).emailUnconfirmedPattern.length() == 0))
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if(constructor == TdApi.AuthStateWaitPhoneNumber.CONSTRUCTOR && receivedPhoneStateDueToLogOut == false)
        {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "adding fragment");
            tr.add(R.id.container, fr, tag).commitAllowingStateLoss();
        }
        else {
            if((!TelegramApplication.sharedApplication().isTDLibReloaded()) && (!receivedPhoneStateDueToLogOut && constructor != TdApi.AuthStateOk.CONSTRUCTOR))
                tr.addToBackStack(null);
            tr.replace(R.id.container, fr, tag).commitAllowingStateLoss();
            if(TelegramApplication.sharedApplication().isTDLibReloaded())
                TelegramApplication.sharedApplication().tdlibReloadingProcessed();
        }
        if(constructor == TdApi.AuthStateOk.CONSTRUCTOR) {
            secondaryAuthDelegate = null;
        }
        receivedPhoneStateDueToLogOut = false;
        Log.d(TAG, "finished restoring");
    }
    private String getPhoneNumber(AuthStateProcessor.SecondaryAuthDelegate delegate)
    {
        if(delegate != null)
        {
            if(delegate.getPhoneNumber() != null && delegate.getPhoneNumber().length() > 0)
                return delegate.getPhoneNumber();
        }
        return TelegramApplication.sharedApplication().sharedPreferences().getString(Constants.PREFS_PHONE, "");
    }
    public View getActionViewPlaceholder()
    {
        return this.actionViewPlaceholder;
    }

    @Override
    public void onBackPressed()
    {
        if(passcodeContainer != null && passcodeContainer.getVisibility() == View.VISIBLE)
            return;
        if(drawerLayout != null && drawerLayout.isDrawerOpen(navDrawerView)) {
            drawerLayout.closeDrawers();
            return;
        }
        if(attachedBottomSheet != null)
        {
            bottomSheetDismissed = true;
            slideDownBottomSheet();
            return;
        }
        if(fam != null && fam.isOpened())
        {
            fam.close(true);
            fam.setOnClickListener(null);
            fam.setClickable(false);
            return;
        }
        else if(backPressInterceptor != null)
            backPressInterceptor.onBackPressed();
        else if(getSupportFragmentManager().getBackStackEntryCount() > 0)
            processBackButton();
        else
            super.onBackPressed();
    }
    public void performGoBack() {
        if(backPressInterceptor != null)
            backPressInterceptor.onBackPressed();
        else
            processBackButton();
    }
    private void processBackButton()
    {
        if(viewPhotoModeEnabled)
        {
            closePhotoView();
            return;
        }
        actionView_useExitAnimations = true;
        FragmentManager fm = getSupportFragmentManager();
        if(clearTillMainFragment)
        {
            for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
            clearTillMainFragment = false;
        }
        else
            fm.popBackStack();
    }
    public void removeBackPressInterceptor()
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "removing back press interceptor");
        this.backPressInterceptor = null;
    }
    public void removeBackPressInterceptor(BackPressListener listener)
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "removing back press interceptor");
        if(listener == null)
            return;
        if(listener == this.backPressInterceptor)
            this.backPressInterceptor = null;
    }
    public void interceptBackPress(BackPressListener interceptor) {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "added back press interceptor");
        this.backPressInterceptor = interceptor;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if(!disableMenuButton && (passcodeContainer == null || passcodeContainer.getVisibility() != View.VISIBLE))
        {
            final int keycode = event.getKeyCode();
            final int action = event.getAction();
            if (keycode == KeyEvent.KEYCODE_MENU && action == KeyEvent.ACTION_UP) {
                if(navDrawerView != null && drawerLayout.getDrawerLockMode(navDrawerView) != DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                {
                    if(drawerLayout.isDrawerOpen(navDrawerView))
                        drawerLayout.closeDrawer(navDrawerView);
                    else
                        drawerLayout.openDrawer(navDrawerView);
                }
                return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }
    public void disableMenuButtonProcessing(boolean disable) { disableMenuButton = disable; }
    public void openNavigationDrawer()
    {
        drawerLayout.openDrawer(navDrawerView);
    }
    @Override
    public void didLogOutRequested()
    {
        receivedPhoneStateDueToLogOut = true;
        getSharedPreferences(Constants.COMPOSEVIEW_PREFS, Activity.MODE_PRIVATE).edit().clear().apply();
        TelegramApplication.sharedApplication().sharedPreferences().edit().clear().apply();
        CurrentUserModel.didLoggedOut();
        AudioPlaybackService audioPlaybackService = AudioPlaybackService.getInstance();
        if(audioPlaybackService != null) {
            audioPlaybackService.stopPlayback(true);
            hideMiniPlayer();
        }

        NotificationCenter.getInstance().removeAllObservers();
        FileCache.getInstance().clear();
        UserCache.getInstance().clear();
        MessageCache.getInstance().clear();
        ChatCache.getInstance().clear();
        GroupCache.getInstance().clear();
        ChannelCache.getInstance().clear();
        ImageCache.sharedInstance().evictAll();
        StickersCache.invalidate();
        GifKeyboardCache.invalidate();
        TdApi.ResetAuth resetFunc = new TdApi.ResetAuth(false);
        AuthStateProcessor.getInstance().changeAuthState(resetFunc);
        if(navDrawerView != null) {
            if(selectionListener != null)
                NotificationCenter.getInstance().addObserver(selectionListener);
        }
    }

    public void clearTillMainFragmentNextTime()
    {
        clearTillMainFragment = true;
    }
    public void unsetClearTillMainFragmentFlag() { clearTillMainFragment = false; }
    public void enableNavDrawer()
    {
        if(passcodeContainer != null && passcodeContainer.getVisibility() == View.VISIBLE)
            return;
        if(drawerLayout != null)
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }
    public void disableNavDrawer()
    {
        if(drawerLayout != null)
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }
    public void closeNavDrawer()
    {
        if(drawerLayout != null)
            drawerLayout.closeDrawers();
    }
    public FragmentTransaction prepareAnimatedTransaction() {
        return getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.fragment_open_enter, R.anim.fragment_open_exit, R.anim.fragment_close_enter, R.anim.fragment_close_exit);
    }
    public void setActionView(ActionView v)
    {
        try
        {
            if(BuildConfig.DEBUG)
                Log.d(TAG, String.format("setActionView: %s", v.toString()));
            if(v == previousActionView)
            {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, String.format("v == previousActionView; actionView = %s", actionView.toString()));
                v.clearAnimation();
                actionView.clearAnimation();
                v.setLayoutParams(lp);
                actionViewPlaceholder.removeAllViews();
                this.actionView.getController().onActionViewRemoved();
                this.actionView = v;
                actionViewPlaceholder.addView(v);
                return;
            }
            v.setLayoutParams(lp);
            actionViewPlaceholder.addView(v);
            //stop open animation if still drawing
            if(previousActionView != null) {
                previousActionView.clearAnimation();
                actionViewPlaceholder.removeView(previousActionView);
            }
            if(actionView != null && actionView.getAnimation() != null) {
                actionView.clearAnimation();
                if(previousActionView != null)
                    previousActionView.setEnabled(false);
                actionViewPlaceholder.removeAllViews();
                this.actionView.getController().onActionViewRemoved();
                this.actionView = v;
                actionViewPlaceholder.addView(v);
                return;
            }
            this.previousActionView = this.actionView;
            this.actionView = v;
            if(previousActionView != null)
            {
                if(actionView_useExitAnimations)
                {
                    Animation actionViewBackOpen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fragment_close_enter);
                    Animation actionViewBackExit = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fragment_close_exit);
                    actionViewBackOpen.setAnimationListener(actionViewAnimListener);
                    previousActionView.setAnimation(actionViewBackExit);
                    actionView.setAnimation(actionViewBackOpen);
                    actionView_useExitAnimations = false;
                }
                else
                {
                    Animation actionViewFwdOpen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fragment_open_enter);
                    Animation actionViewFwdExit = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fragment_open_exit);
                    actionViewFwdOpen.setAnimationListener(actionViewAnimListener);
                    previousActionView.setAnimation(actionViewFwdExit);
                    actionView.setAnimation(actionViewFwdOpen);
                }
            }
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    public void setActionButtonDelegate(final BaseFragment.ActionButtonDelegate delegate)
    {
        if(delegate == null) {
            this.actionButton.setVisibility(View.GONE);
            return;
        }
        this.actionButton.setVisibility(View.VISIBLE);
        this.actionButton.setImageResource(delegate.getActionButtonImageResource());
        this.actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delegate.onActionButtonClick();
            }
        });
    }
    public void hideActionViewShadow()
    {
        actionViewShadow.setVisibility(View.INVISIBLE);
    }
    public void showActionViewShadow()
    {
        actionViewShadow.setVisibility(View.VISIBLE);
    }
    public void hideActionView()
    {
        if(actionViewPlaceholder != null) {
            actionViewPlaceholder.setVisibility(View.GONE);
            actionViewShadow.setVisibility(View.GONE);
        }
    }
    public void showActionView()
    {
        if(actionViewPlaceholder != null) {
            actionViewPlaceholder.setVisibility(View.VISIBLE);
            actionViewShadow.setVisibility(View.VISIBLE);
        }
    }
    private void initPasscodeView()
    {
        passcodeContainer = (FrameLayout) drawerLayout.findViewById(R.id.passcode_container);
        if(BuildConfig.DEBUG)
            Log.d(TAG, "initialising passcode view");
        SharedPreferences prefs = TelegramApplication.sharedApplication().sharedPreferences();
        PasscodeViewController.PasscodeType passcodeType;
        int passcodePrefType = prefs.getInt(Constants.PREFS_PASSCODE_LOCK_TYPE, Constants.PASSCODE_TYPE_PIN);
        if(passcodePrefType == Constants.PASSCODE_TYPE_PIN)
            passcodeType = PasscodeViewController.PasscodeType.PIN;
        else if(passcodePrefType == Constants.PASSCODE_TYPE_PASSWORD)
            passcodeType = PasscodeViewController.PasscodeType.PASSWORD;
        else if(passcodePrefType == Constants.PASSCODE_TYPE_PATTERN)
            passcodeType = PasscodeViewController.PasscodeType.PATTERN;
        else
            passcodeType = PasscodeViewController.PasscodeType.GESTURE;
        passcodeView = new PasscodeView(this, passcodeType);
        passcodeView.setLayoutParams(passcodeContainer.getLayoutParams());
        passcodeViewController = CommonTools.initPasscodeViewController(passcodeView, passcodeType, PasscodeViewController.Mode.LOCK);
    }

    public void setSecondaryAuthDelegate(AuthStateProcessor.SecondaryAuthDelegate delegate)
    {
        this.secondaryAuthDelegate = delegate;
    }

    @Override
    public void didPlayButtonPressed() {
        AudioPlaybackService audioPlaybackService = AudioPlaybackService.getInstance();
        if(audioPlaybackService == null)
            return;
        audioPlaybackService.togglePlayback();
    }

    @Override
    public void didCloseButtonPressed() {
        miniPlayer.setVisibility(View.GONE);
        AudioPlaybackService audioPlaybackService = AudioPlaybackService.getInstance();
        if(audioPlaybackService == null)
            return;
        audioPlaybackService.stopPlayback(true);
    }

    @Override
    public void didOpenPlayerRequested() {
        hideMiniPlayer();
        PlayerFragment fr = new PlayerFragment();
        fr.setRetainInstance(true);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction tr = prepareAnimatedTransaction();
        tr.replace(R.id.container, fr, Constants.PLAYER_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }

    public void showChatFam(boolean show)
    {
        if(fam == null)
            return;
        if(show)
            fam.showMenuButton(true);
        else
            fam.hideMenuButton(true);
    }
    public void enableChatFam(boolean enable)
    {
        enableChatFam(enable, false);
    }
    public void enableChatFam(boolean enable, boolean animate)
    {
        if(fam == null)
            return;
        this.showFam = enable;
        if(enable)
            fam.showMenuButton(animate);
        else
            fam.hideMenuButton(animate);
    }
    public void showMiniPlayer(boolean sendStartPlaybackMessage)
    {
        AudioPlaybackService audioPlaybackService = AudioPlaybackService.getInstance();
        if(audioPlaybackService == null)
            return;
        audioPlaybackService.setPlaybackDelegate(miniPlayerDelegate);
        if(sendStartPlaybackMessage)
            audioPlaybackService.playSong(true);
        restoreMiniPlayerState();
        miniPlayer.setVisibility(View.VISIBLE);
    }
    private void restoreMiniPlayerState()
    {
        AudioPlaybackService audioPlaybackService = AudioPlaybackService.getInstance();
        if(audioPlaybackService == null)
            return;
        if(audioPlaybackService.isAudioPlaying()) {
            miniPlayer.updatePlayButton(true);
        }
        AudioTrackModel track = audioPlaybackService.nowPlaying();
        if(track == null)
            return;
        miniPlayer.setProgressBarMax(track.getDuration());
        miniPlayer.updateSongData(track.getSongName(), track.getSongPerformer());
    }
    private void restoreMiniPlayerVisibility()
    {
        AudioPlaybackService audioPlaybackService = AudioPlaybackService.getInstance();
        if(audioPlaybackService == null)
            return;
        if(audioPlaybackService.isAudioPlaying())
            miniPlayer.setVisibility(View.VISIBLE);
        else
            miniPlayer.setVisibility(View.GONE);
    }
    public void hideMiniPlayer()
    {
        miniPlayer.setVisibility(View.GONE);
    }
    public void refreshNavDrawerInfo() {
        if(navDrawerView != null)
            navDrawerView.refreshInfo();
    }
    public void didTDLibReloaded()
    {
        try
        {
            FragmentManager fm = getSupportFragmentManager();
            if(fm.getFragments() == null)
                return;
            AsyncTaskManager.stopManager();
            TelegramApplication.sharedApplication().reInitTDLibClient();
            ImageCache.sharedInstance().evictAll();
            FileCache.getInstance().clear();
            UserCache.getInstance().clear();
            MessageCache.getInstance().clear();
            ChatCache.getInstance().clear();
            GroupCache.getInstance().clear();
            StickersCache.invalidate();
            GifKeyboardCache.invalidate();
            System.gc();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            for(Fragment fr : fm.getFragments())
            {
                if(fr != null)
                {
                    if(BuildConfig.DEBUG)
                        Log.d(TAG, String.format("removing fragment %s", fr.getTag()));
                    fm.beginTransaction().remove(fr).commitAllowingStateLoss();
                    fm.executePendingTransactions();
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    public void enableViewPhotoMode(FileModel photo, String user, int dateInSeconds)
    {
        if(viewPhotoLayout == null)
            return;
        viewPhotoModeEnabled = true;
        viewedPhotoFileId = photo.getFileId();
        viewedPhotoUserName = user;
        viewedPhotoDateInSeconds = dateInSeconds;
        viewPhotoLayout.setPhoto(photo, user, dateInSeconds);
        viewPhotoLayout.setCloseRunnable(closePhotoRunnable);
        viewPhotoLayout.setVisibility(View.VISIBLE);
    }
    private void closePhotoView()
    {
        viewPhotoLayout.setVisibility(View.GONE);
        if(viewPhotoLayout != null)
            viewPhotoLayout.didPhotoViewClosed();
        viewPhotoModeEnabled = false;
    }
    public void showBottomSheet(BottomSheet bottomSheet)
    {
        if(bottomSheet == null)
            return;
        if(attachedBottomSheet != null && bottomSheetContainer.getChildCount() > 0)
            return;
        this.attachedBottomSheet = bottomSheet;
        bottomSheetContainer.setVisibility(View.VISIBLE);
        ((View) bottomSheet).setLayoutParams(bottomSheetLP);
        bottomSheetContainer.addView((View) attachedBottomSheet);
        slideUpBottomSheet();
    }
    private void slideUpBottomSheet()
    {
        if(attachedBottomSheet == null)
            return;
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), bottomSheetColorFrom, bottomSheetColorTo);
        ObjectAnimator animator = ObjectAnimator.ofFloat(attachedBottomSheet, "yFraction", 1.0f, 0).setDuration(250);
        animator.setInterpolator(slideUpInterpolator);
        colorAnimation.setInterpolator(slideUpInterpolator);
        colorAnimation.setDuration(250);
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                if (bottomSheetContainer != null)
                    bottomSheetContainer.setBackgroundColor((int) animator.getAnimatedValue());
            }
        });
        colorAnimation.start();
        animator.start();
    }
    public void slideDownBottomSheet()
    {
        if(attachedBottomSheet == null)
            return;
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), bottomSheetColorTo, bottomSheetColorFrom);
        ObjectAnimator animator = ObjectAnimator.ofFloat(attachedBottomSheet, "yFraction", 0f, 1.0f).setDuration(250);
        animator.setInterpolator(slideUpInterpolator);
        colorAnimation.setInterpolator(slideUpInterpolator);
        colorAnimation.setDuration(250);
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                if (bottomSheetContainer != null)
                    bottomSheetContainer.setBackgroundColor((int) animator.getAnimatedValue());
            }
        });
        colorAnimation.start();
        animator.addListener(slideDownListener);
        animator.setInterpolator(slideDownInterpolator);
        animator.start();
    }
    public void showFwdBottomSheet(ForwardBottomSheetView.ForwardMenuDelegate delegate)
    {
        if(fwdBottomSheet == null)
            fwdBottomSheet = new ForwardBottomSheetView(this);
        fwdBottomSheet.helpAdjustScreen(specialWindowMode, normalWindowMode, new Runnable() {
            @Override
            public void run() {
                onFwdBottomSheetHide();
            }
        });
        fwdBottomSheet.setDelegate(delegate);
        this.fwdDelegate = delegate;
        RetainFragment retainFragment =
                RetainFragment.findOrCreateRetainFragment(getSupportFragmentManager());
        if(fwdController == null) {
            if(retainFragment.fwdChatsController != null)
            {
                fwdController = retainFragment.fwdChatsController;
                fwdController.replaceView(fwdBottomSheet.chatGrid);
            }
            else
            {
                fwdController = new FwdChatsController(fwdBottomSheet.chatGrid);
                fwdBottomSheet.chatGrid.setController(fwdController);
            }
        }
        else
            fwdController.replaceView(fwdBottomSheet.chatGrid);
        if(fwdSearchListener == null)
            fwdSearchListener = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String s = editable.toString();
                    if(fwdController != null)
                        fwdController.searchChats(s);
                }
            };
        fwdBottomSheet.searchEditText.addTextChangedListener(fwdSearchListener);
        RetainFragment fr = RetainFragment.findOrCreateRetainFragment(getSupportFragmentManager());
        fr.fwdChatsController = fwdController;
        fr.fwdDelegate = fwdDelegate;
        try
        {
            restoreFwdBottomSheet = true;
            showBottomSheet(fwdBottomSheet);
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    public void onFwdBottomSheetHide()
    {
        restoreFwdBottomSheet = false;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

}
