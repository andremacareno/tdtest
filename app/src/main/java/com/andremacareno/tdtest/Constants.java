package com.andremacareno.tdtest;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 08/04/15.
 */
public class Constants {
    public static final String GENERAL_NEW_MESSAGE_OBSERVER = "general_new_message_observer";
    public static final String GENERAL_USER_ACTION_OBSERVER = "general_user_action_observer";
    public static final String GENERAL_UPDATE_NOTIFICATIONS_OBSERVER = "general_notifications_observer";
    public static final String CONTACTLIST_USER_STATUS_OBSERVER = "contactlist_user_status_observer";
    public static final String CONTACTLIST_USER_DATA_OBSERVER = "contactlist_user_data_observer";
    public static final String USERCACHE_USER_STATUS_OBSERVER = "usercache_user_status_observer";
    public static final String USERCACHE_USER_BLOCK_OBSERVER = "usercache_user_block_observer";
    public static final String USERCACHE_USER_DATA_OBSERVER = "usercache_user_data_observer";
    public static final String CHATCACHE_CHAT_TITLE_OBSERVER = "chatcache_chat_title_observer";
    public static final String CHATCACHE_CHAT_PARTICIPANTS_COUNT_OBSERVER = "chatcache_chat_participants_count_observer";
    public static final String GENERAL_CHAT_READ_INBOX_OBSERVER = "general_chat_read_inbox_observer";
    public static final String GENERAL_CHAT_READ_OUTBOX_OBSERVER = "general_chat_read_outbox_observer";
    public static final String CHAT_ACTION_VIEW_STATUS_OBSERVER = "chat_action_view_status_observer";
    public static final String PROFILE_ACTION_VIEW_STATUS_OBSERVER = "profile_action_view_status_observer";
    public static final String CHAT_NEW_MESSAGE_OBSERVER = "chat%d_new_message_observer";
    public static final String CHAT_UPDATE_MESSAGE_OBSERVER = "chat_update_message_observer";
    public static final String CHAT_USER_STATUS_OBSERVER = "chatscreen%d_user_status_updates_observer";
    public static final String CHAT_USER_DATA_OBSERVER = "chatscreen%d_user_data_updates_observer";
    public static final String VIEWCHAT_USER_STATUS_OBSERVER = "chat%d_user_status_updates_observer";
    public static final String VIEWCHAT_USER_DATA_OBSERVER = "chat%d_user_data_updates_observer";
    public static final String VIEWCHAT_UPDATE_TITLE_OBSERVER = "chat%d_title_updates_observer";
    public static final String VIEWCHAT_UPDATE_PARTICIPANTS_COUNT_OBSERVER = "chat%d_participants_count_updates_observer";
    public static final String VIEWCHAT_UPDATE_NOTIFICATIONS_OBSERVER = "viewchat%d_notifications_observer";
    public static final String CHATSCREEN_UPDATE_TITLE_OBSERVER = "chatscreen%d_title_updates_observer";
    public static final String CHATSCREEN_UPDATE_PHOTO_OBSERVER = "chatscreen%d_photo_updates_observer";
    public static final String CHATSCREEN_UPDATE_PARTICIPANTS_COUNT_OBSERVER = "chatscreen%d_participants_count_updates_observer";
    public static final String VIEWPROFILE_USER_STATUS_OBSERVER = "user%d_user_status_updates_observer";
    public static final String VIEWPROFILE_USER_DATA_OBSERVER = "user%d_user_data_updates_observer";
    public static final String VIEWPROFILE_USER_BLOCK_OBSERVER = "user%d_user_blocked_updates_observer";
    public static final String CHATSCREEN_REPLY_MARKUP_OBSERVER = "chat%d_reply_markup_observer";
    public static final String CHATSCREEN_GENERAL_READ_INBOX_OBSERVER = "chat%d_general_read_inbox_observer";
    public static final String CHATSCREEN_GENERAL_READ_OUTBOX_OBSERVER = "chat%d_general_read_outbox_observer";
    public static final String CHATSCREEN_UPDATE_NOTIFICATION_SETTINGS_OBSERVER = "chatscreen%d_update_notification_settings_observer";
    public static final String CHATLIST_UPDATE_TITLE_OBSERVER = "chatlist_update_title_observer";
    public static final String CHATLIST_UPDATE_PHOTO_OBSERVER = "chatlist_update_photo_observer";
    public static final String CHATLIST_UPDATE_READ_INBOX_OBSERVER = "chatlist_update_read_inbox_observer";
    public static final String CHATLIST_UPDATE_READ_OUTBOX_OBSERVER = "chatlist_update_read_outbox_observer";
    public static final String CHATLIST_UPDATE_PARTICIPANTS_COUNT_OBSERVER = "chatlist_update_participants_count_observer";
    public static final String CHATLIST_UPDATE_REPLY_MARKUP_OBSERVER = "chatlist_update_reply_markup_observer";
    public static final String CHATLIST_UPDATE_MESSAGE_ID_OBSERVER = "chatlist_update_message_id_observer";
    public static final String CHATLIST_UPDATE_MESSAGE_SEND_FAIL_OBSERVER = "chatlist_update_message_send_fail_observer";
    public static final String CHATLIST_UPDATE_MESSAGE_DATE_OBSERVER = "chatlist_update_message_date_observer";
    public static final String CHATLIST_UPDATE_MESSAGE_CONTENT_OBSERVER = "chatlist_update_message_content_observer";
    public static final String CHATLIST_UPDATE_NEW_MSG_OBSERVER = "chatlist_update_new_message_observer";
    public static final String CHATLIST_UPDATE_MESSAGE_DEL_OBSERVER = "chatlist_update_message_del_observer";
    public static final String CHATLIST_UPDATE_CHAT_OBSERVER = "chatlist_update_chat_observer";
    public static final String CHANNEL_CACHE_UPDATE_OBSERVER = "channel_cache_update_observer";
    public static final String FILECACHE_UPDATE_FILE_OBSERVER = "filecache_update_file_observer";
    public static final String FILECACHE_UPDATE_FILE_PROGRESS_OBSERVER = "filecache_update_file_progress_observer";
    public static final String SHAREDMEDIA_DELETE_MESSAGES_OBSERVER = "shared_media_delete_messages_observer";
    public static final String SHAREDMEDIA_NEW_MESSAGE_OBSERVER = "shared_media_new_message_observer";
    public static final String PLAYER_DELETE_MESSAGES_OBSERVER = "player_delete_messages_observer";
    public static final String PLAYER_NEW_MESSAGE_OBSERVER = "player_new_message_observer";
    public static final String CHATCACHE_UPDATE_CHAT_OBSERVER = "chatcache_update_chat_observer";
    public static final String CHATCACHE_UPDATE_PHOTO_OBSERVER = "chatcache_update_photo_observer";
    public static final String CHANNEL_CHAT_UPDATE_CHANNEL_OBSERVER = "channel%d_chat_update_channel_observer";
    public static final String VIEWCHANNEL_UPDATE_CHAT_TITLE_OBSERVER = "viewchannel_chat%d_update_title_observer";
    public static final String VIEWCHANNEL_UPDATE_CHAT_PHOTO_OBSERVER = "viewchannel_chat%d_update_photo_observer";
    public static final String VIEWCHANNEL_UPDATE_CHANNEL_OBSERVER = "viewchannel_channel%d_update_observer";

    public static final String SIGN_UP_FRAGMENT = "sign_up_fragment";
    public static final String ENTER_CODE_FRAGMENT = "enter_code_fragment";
    public static final String ENTER_PASSWORD_FRAGMENT = "enter_password_fragment";
    public static final String RECOVER_AUTH_PASSWORD_FRAGMENT = "recover_auth_password_fragment";
    public static final String ROSTER_FRAGMENT = "roster_fragment";
    public static final String CHAT_FRAGMENT = "chat_fragment";
    public static final String SELECTED_CHAT_FRAGMENT = "selected_chat_fragment";
    public static final String ENTER_PHONE_FRAGMENT = "enter_phone_fragment";
    public static final String CHOOSE_COUNTRY_FRAGMENT = "choose_country_fragment";
    public static final String CONTACTS_FRAGMENT = "contacts_fragment";
    public static final String NEW_CHAT_FRAGMENT = "new_chat_fragment";
    public static final String NEW_CHANNEL_FRAGMENT = "new_channel_fragment";
    public static final String ENTER_CHAT_TITLE_FRAGMENT = "enter_chat_title_fragment";
    public static final String CHOOSE_CHANNEL_PARTICIPANTS_FRAGMENT = "choose_chat_participants_fragment";
    public static final String CHOOSE_CHANNEL_TYPE_FRAGMENT = "choose_channel_type_fragment";
    public static final String VIEW_PHOTO_FRAGMENT = "view_photo_fragment";
    public static final String VIEW_PROFILE_FRAGMENT = "view_profile_fragment";
    public static final String VIEW_CHAT_FRAGMENT = "view_chat_fragment";
    public static final String SETTINGS_FRAGMENT = "settings_fragment";
    public static final String SETTINGS_EDIT_NAME_FRAGMENT = "settings_edit_name_fragment";
    public static final String SETTINGS_EDIT_USERNAME_FRAGMENT = "settings_edit_username_fragment";
    public static final String SETTINGS_PASSCODE_LOCK_FRAGMENT = "settings_passcode_lock_fragment";
    public static final String SETTINGS_PASSCODE_SET_FRAGMENT = "settings_passcode_set_fragment";
    public static final String ADD_MEMBER_FRAGMENT = "add_member_fragment";
    public static final String EDIT_CONTACT_FRAGMENT = "edit_contact_fragment";
    public static final String EDIT_CHAT_TITLE_FRAGMENT = "edit_chat_title_fragment";
    public static final String PLAYER_FRAGMENT = "player_fragment";
    public static final String SHARED_MEDIA_FRAGMENT = "shared_media_fragment";
    public static final String PLAYLIST_FRAGMENT = "playlist_fragment";
    public static final String SELECT_CHAT_FRAGMENT = "select_chat_fragment";
    public static final String EDIT_ADMINS_FRAGMENT = "edit_admins";
    public static final String EDIT_CHANNEL_FRAGMENT = "edit_channel";


    public static final int MAX_CHAT_PARTICIPANTS = 200;
    public static final String APP_PREFS = "app_prefs";
    public static final String COMPOSEVIEW_PREFS = "compose_view_prefs";
    public static final String ARG_RESTORE_FWD_BOTTOM_SHEET = "restore_fwd_bottom_sheet";
    public static final String ARG_LAST_MSG_ID = "last_message_id";
    public static final String ARG_SHOW_AUDIO = "show_audio";
    public static final String ARG_CHANNEL_ID = "channel_id";
    public static final String ARG_GROUP_ID = "group_id";
    public static final String ARG_HAS_RECOVERY_EMAIL = "has_recovery_email";
    public static final String ARG_PASSWORD_HINT = "password_hint";
    public static final String ARG_EMAIL_PATTERN = "email_pattern";
    public static final String ARG_PASSCODE_LOCK_WAS_VISIBLE = "passcode_lock_was_visible";
    public static final String ARG_CHAT_INVITE_LINK = "chat_invite_link";
    public static final String ARG_USERNAME = "current_username";
    public static final String ARG_START_MESSAGE = "start_message";
    public static final String ARG_DELETED_ACCOUNT = "deleted_account";
    public static final String ARG_BOT = "bot";
    public static final String ARG_CHAT_ID = "chat_id";
    public static final String ARG_USER_ID = "user_id";
    public static final String ARG_SUPERGROUP_ID = "supergroup_id";
    public static final String ARG_CHAT_MUTED = "chat_muted";
    public static final String ARG_REPLY_MARKUP_MSG = "reply_markup_message_id";
    public static final String ARG_LAST_READ_INBOX = "last_read_inbox";
    public static final String ARG_LAST_READ_OUTBOX = "last_read_outbox";
    public static final String ARG_UNREAD_COUNT = "unread_count";
    public static final String ARG_IGNORE_IDS = "ignore_ids";
    public static final String ARG_ADD_CHANNEL_ADMIN = "add_channel_admin";
    public static final String ARG_INVITE_CHANNEL_MEMBER = "invite_channel_member";
    public static final String ARG_INVITE_CHANNEL_ADMIN = "invite_channel_admin";
    public static final String ARG_CHAT_LEFT = "left";
    public static final String ARG_CHAT_PEER = "chat_peer";
    public static final String ARG_CHAT_TITLE = "chat_title";
    public static final String ARG_FROM_SEARCH = "from_search";
    public static final String ARG_FIRST_NAME = "first_name";
    public static final String ARG_LAST_NAME = "last_name";
    public static final String ARG_CHAT_REFERER = "chat_referer";
    public static final String ARG_CLEAR_BACK_STACK_ON_EXIT = "clear_back_stack_on_exit";
    public static final String ARG_MINI_PLAYER_VISIBILITY = "mini_player_visibility";
    public static final String ARG_LOW_MEMORY_DETECTED = "low_memory_detected";
    public static final String ARG_VIEW_PHOTO_MODE_ENABLED = "view_photo_mode_enabled";
    public static final String ARG_VIEW_PHOTO_FILE_ID = "view_photo_file_id";
    public static final String ARG_VIEW_PHOTO_USER = "view_photo_user";
    public static final String ARG_VIEW_PHOTO_DATE = "view_photo_date";
    public static final String ARG_DISABLE_MENU_BUTTON = "disable_menu_button";
    public static final String ARG_SHOW_CHAT_FAM = "show_chat_fam";
    public static final String ARG_CHOOSE_MODE = "choose_mode";

    public static final int MODEL_DATE_SEPARATOR = 1;
    public static final int MODEL_UNREAD_SEPARATOR = 2;
    public static final int MODEL_BOT_CHAT_HEADER = 3;
    public static final String AVATAR_PLACEHOLDER_FMT = "avatar_placeholder_%d";
    public static final String ARG_PHONE_NUMBER = "phone_number";
    public static final String GIF_CACHE_KEY_FORMAT = "gif%d";

    public static final String OPTION_NETWORK_UNREACHABLE = "network_unreachable";

    public static final String PREFS_PASSCODE_LOCK_ENABLED = "passcode_lock_enabled";
    public static final String PREFS_PASSCODE_LOCK_TYPE = "passcode_lock_type";
    public static final String PREFS_PASSCODE_LOCK_PIN = "passcode_lock_pin";
    public static final String PREFS_PASSCODE_LOCK_PASSWORD = "passcode_lock_password";
    public static final String PREFS_PASSCODE_LOCK_PATTERN = "passcode_lock_pattern";
    public static final String PREFS_PHONE = "phone_number";
    public static final String GESTURE_LIBRARY = "tmp_gesture";
    public static final String PREFS_LOCK_APP_NEXT_TIME = "lock_app_next_time";
    public static final String PREFS_AFK_TIMESTAMP = "afk_timestamp";
    public static final String PREFS_AUTOLOCK_TIMEOUT = "autolock_timeout";
    public static final String PREFS_TDLIB_RELOADED = "tdlib_reloaded";

    public static final String AVATAR_PREVIEW_FORMAT = "avatar_preview_%s";

    public static final int PASSCODE_TYPE_PIN = 0;
    public static final int PASSCODE_TYPE_PASSWORD = 1;
    public static final int PASSCODE_TYPE_PATTERN = 2;
    public static final int PASSCODE_TYPE_GESTURE = 3;

    public static final String STICKER_THUMB_KEY = "sticker%d_%d_thumb";
    public static final String STICKER_FULL_KEY = "sticker%d_%d_full";


    //for inputMessageVenue (in near future)
    public static final String FOURSQUARE_CLIENT_ID = "EQFTZHSMDHALVXJJ5YCX03L1IOIHN2QOHLNLDT20VEILF03K";
    public static final String FOURSQUARE_CLIENT_SECRET = "2HBDUOCPSYFIIWT3NFQLSYNA4DT3L0U4DSTYUDDXBTAHAM3Q";
    public static final String FOURSQUARE_API_VERSION = "20150326";

    public static final String STATIC_MAP_URL_FORMAT = "http://maps.google.com/maps/api/staticmap?center=%f,%f&zoom=15&size=%dx%d&sensor=false";

    public static final int MINI_PLAYER_DELEGATE_ID = 0;
    public static final int PLAYER_DELEGATE_ID = 1;
    public static final String GALLERY_PHOTO_KEY = "gallery_%d_thumb";
    public static final String TELEGRAM_PREFIX = "telegram.me/";
    public static final String LOCATIONS_FOLDER = "/locations";
    public static final String INLINE_THUMBS_FOLDER = "/inline_thumbs";
    private static int keyCount = 0;
    public static final int GROUP_CHAT_FRAGMENT_MODEL_CHANGE_LISTENER = keyCount++;
    public static final int VIEW_GROUP_CHAT_FRAGMENT_MODEL_CHANGE_LISTENER = keyCount++;
    public static final TdApi.ChatParticipantRole PARTICIPANT_ADMIN = new TdApi.ChatParticipantRoleAdmin();
    public static final TdApi.ChatParticipantRole PARTICIPANT_EDITOR = new TdApi.ChatParticipantRoleEditor();
    public static final TdApi.ChatParticipantRole PARTICIPANT_MODERATOR = new TdApi.ChatParticipantRoleModerator();
    public static final TdApi.ChatParticipantRole PARTICIPANT_KICKED = new TdApi.ChatParticipantRoleKicked();
    public static final TdApi.ChatParticipantRole PARTICIPANT_LEFT = new TdApi.ChatParticipantRoleLeft();
    public static final TdApi.ChatParticipantRole PARTICIPANT_GENERAL = new TdApi.ChatParticipantRoleGeneral();

    public static final TdApi.ChannelParticipantsFilter CHANNEL_GET_ADMINS = new TdApi.ChannelParticipantsAdmins();
    public static final TdApi.ChannelParticipantsFilter CHANNEL_GET_BOTS = new TdApi.ChannelParticipantsBots();
    public static final TdApi.ChannelParticipantsFilter CHANNEL_GET_RECENT = new TdApi.ChannelParticipantsRecent();
}
