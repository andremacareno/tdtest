package com.andremacareno.tdtest;

import java.nio.ByteBuffer;

/**
 * Created by andremacareno on 01/03/16.
 */
public class VideoUtils {
    public native static int convertVideoFrame(ByteBuffer src, ByteBuffer dest, int destFormat, int width, int height, int padding, int swap);
}
