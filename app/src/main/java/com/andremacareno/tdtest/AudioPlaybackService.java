package com.andremacareno.tdtest;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.util.Log;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.models.AudioMessageModel;
import com.andremacareno.tdtest.models.AudioTrackModel;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.tasks.CreatePlaylistTask;
import com.andremacareno.tdtest.updatelisteners.UpdateMessageProcessor;

import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import gnu.trove.set.hash.TIntHashSet;

/**
 * Created by andremacareno on 15/08/15.
 */
public class AudioPlaybackService implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener {
    public interface PlaybackDelegate
    {
        public void didPlaybackStarted(AudioTrackModel nowPlaying);
        public void didPlaybackStopped();
        public void didPlaybackPaused();
        public void didPlaybackUnpaused();
        public void didTrackPositionChanged(int currentPosition);
        public void didTrackDownloadFinished();
        public int getDelegateId();
    }
    public interface SimplePlaybackDelegate
    {
        public void didPlaybackStarted(FileModel track);
        public void didPlaybackPaused(FileModel track);
        public void didPlaybackStopped();
        public String getDelegateKey();
    }
    private volatile boolean enableRepeat = false;
    private volatile boolean enableShuffle = false;
    private volatile long chatId;
    private volatile boolean cancelAsyncPlaySong = false;
    private final Handler handler = new Handler(Looper.getMainLooper());
    private final IBinder musicBind = new AudioBinder();
    private final CopyOnWriteArrayList<AudioTrackModel> previousTracks = new CopyOnWriteArrayList<>();
    private final CopyOnWriteArrayList<AudioTrackModel> playlist = new CopyOnWriteArrayList<>();
    private final CopyOnWriteArrayList<AudioTrackModel> shuffledPlaylist = new CopyOnWriteArrayList<>();
    private final ConcurrentHashMap<Integer, AudioTrackModel> msgToTrack = new ConcurrentHashMap<>();
    private final TIntHashSet writtenAudio = new TIntHashSet();
    private PlaybackDelegate delegate = null;
    private MediaPlayer mediaPlayer = null;
    private CreatePlaylistTask currentlyLoadingPlaylist = null;
    private final HashMap<String, SimplePlaybackDelegate> simpleDelegateMap = new HashMap<>();
    private UpdateMessageProcessor updateMessageProcessor;
    private volatile boolean nowPlayingWasRemoved = false;
    private UpdateMessageProcessor.UpdateMessageDelegate updateMsgUiThreadDelegate;
    private AudioTrackModel nowPlaying = null;

    private static volatile AudioPlaybackService _instance;

    private AudioPlaybackService()
    {
        initMediaPlayer();
    }
    public static AudioPlaybackService getInstance()
    {
        if(_instance == null)
        {
            synchronized(AudioPlaybackService.class)
            {
                if(_instance == null)
                    _instance = new AudioPlaybackService();
            }
        }
        return _instance;
    }
    public static void stop()
    {
        if(_instance != null)
        {
            synchronized(AudioPlaybackService.class)
            {
                if(_instance != null)
                {
                    _instance.finishHim();
                    _instance = null;
                }
            }
        }
    }
    private void finishHim()
    {
        if(mediaPlayer != null)
        {
            try
            {
                mediaPlayer.stop();
                mediaPlayer.release();
            }
            finally
            {
                if(updateMessageProcessor != null)
                    updateMessageProcessor.removeObservers();
            }
        }
    }
    /** Called when MediaPlayer is ready */
    public void onPrepared(MediaPlayer player) {
        player.start();
        if(delegate != null)
            delegate.didPlaybackStarted(nowPlaying);
        notifySubscribersAboutPlaybackStart();
        startPlayProgressUpdater();
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        stopPlayProgressUpdater();
        if(delegate != null)
            delegate.didPlaybackPaused();
        notifySubscribersAboutPlaybackPause();
        playNextTrack();
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        stopPlayback(true);
        return true;
    }
    private void initMediaPlayer()
    {
        if(mediaPlayer == null)
            mediaPlayer = new MediaPlayer();
        mediaPlayer.setWakeMode(TelegramApplication.sharedApplication().getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnErrorListener(this);
    }
    private void initObservers()
    {
        if(updateMessageProcessor == null)
        {
            updateMsgUiThreadDelegate = new UpdateMessageProcessor.UpdateMessageDelegate() {
                @Override
                public void updateMessageContent(TdApi.UpdateMessageContent upd) {
                }

                @Override
                public void updateMessageDate(TdApi.UpdateMessageDate upd) {
                }

                @Override
                public void updateMessageId(TdApi.UpdateMessageId upd) {
                }

                @Override
                public void updateMessageSendFailed(TdApi.UpdateMessageSendFailed upd) {
                }

                @Override
                public void updateNewMessage(TdApi.UpdateNewMessage upd) {
                    if(BuildConfig.DEBUG)
                        Log.d("AudioPlaybackService", "Processing updateNewMessage");
                    if(upd.message.chatId != chatId)
                        return;
                    if(upd.message.content.getConstructor() != TdApi.MessageAudio.CONSTRUCTOR)
                        return;
                    synchronized(writtenAudio)
                    {
                        if(writtenAudio.contains(((TdApi.MessageAudio) upd.message.content).audio.audio.id))
                            return;
                    }
                    AudioTrackModel track = new AudioTrackModel(upd.message.id, upd.message.date, ((TdApi.MessageAudio) upd.message.content).audio);
                    synchronized(writtenAudio)
                    {
                        if(writtenAudio.contains(track.getTrack().getFileId()))
                            return;
                        writtenAudio.add(track.getTrack().getFileId());
                    }
                    if(BuildConfig.DEBUG)
                        Log.d("AudioPlaybackService", "adding to playlist");
                    msgToTrack.put(track.getMessageId(), track);
                    playlist.add(track);
                }

                @Override
                public void updateDelMessages(TdApi.UpdateDeleteMessages upd) {
                    for(int msg : upd.messageIds)
                        removeFromPlaylist(msg, upd.chatId);
                }

                @Override
                public void updateViewsCount(TdApi.UpdateMessageViews upd) {

                }
            };
            updateMessageProcessor = new UpdateMessageProcessor(null, updateMsgUiThreadDelegate) {
                @Override
                protected String getKeyByUpdateConstructor(int constructor) {
                    if(constructor == TdApi.UpdateDeleteMessages.CONSTRUCTOR)
                        return Constants.PLAYER_DELETE_MESSAGES_OBSERVER;
                    else if(constructor == TdApi.UpdateNewMessage.CONSTRUCTOR)
                        return Constants.PLAYER_NEW_MESSAGE_OBSERVER;
                    return null;
                }

                @Override
                protected boolean shouldObserveUpdateMsgContent() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateMsgDate() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateMsgId() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateMsgSendFailed() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateNewMsg() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateDeleteMessages() {
                    return true;
                }

                @Override
                protected boolean shouldObserveViewsCount() {
                    return false;
                }
            };
        }
    }
    public class AudioBinder extends Binder {
        AudioPlaybackService getService() {
            return AudioPlaybackService.this;
        }
    }
    public void playSong(boolean trackLoaded)
    {
        if(!enableShuffle && playlist.isEmpty())
            return;
        if(enableShuffle && shuffledPlaylist.isEmpty())
            return;
        if(mediaPlayer == null)
            return;
        AudioTrackModel track = enableShuffle ? shuffledPlaylist.get(0) : playlist.get(0);
        mediaPlayer.reset();
        nowPlaying = track;
        if(trackLoaded)
        {
            String path = track.getTrack().getFile().path;
            if(path == null || path.length() == 0)
                return;
            File fileObj = new File(path);
            FileInputStream fis;
            try {
                fis = new FileInputStream(fileObj);
                FileDescriptor fd = fis.getFD();
                mediaPlayer.setDataSource(fd);
                mediaPlayer.prepareAsync();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            if(delegate != null)
                delegate.didPlaybackStarted(nowPlaying);
            notifySubscribersAboutPlaybackStart();
        }
    }
    public void addToPlaylist(AudioMessageModel msg)
    {
        if(playlist.isEmpty() || chatId != msg.getChatId())
            return;
        synchronized(writtenAudio)
        {
            if(writtenAudio.contains(msg.getTrackModel().getTrack().getFileId()))
                return;
            writtenAudio.add(msg.getTrackModel().getTrack().getFileId());
            playlist.add(msg.getTrackModel());
            msgToTrack.put(msg.getMessageId(), msg.getTrackModel());
        }
    }
    public void removeFromPlaylist(int msgId, long fromChatId)
    {
        if(mediaPlayer == null || chatId != fromChatId)
            return;
        AudioTrackModel track = msgToTrack.get(msgId);
        if(track == null) {
            return;
        }
        if(track.equals(nowPlaying)) {
            nowPlayingWasRemoved = true;
            stopPlayProgressUpdater();
            notifySubscribersAboutPlaybackStop();
            mediaPlayer.reset();
        }
        int index = playlist.indexOf(track);
        if(index >= 0)
            playlist.remove(index);
        index = previousTracks.indexOf(track);
        if(index >= 0)
            previousTracks.remove(index);
        msgToTrack.remove(msgId);
        synchronized(writtenAudio)
        {
            if(!writtenAudio.contains(track.getTrack().getFileId()))
                return;
            writtenAudio.remove(track.getTrack().getFileId());
        }
    }
    public void addToPlaylist(ArrayList<AudioTrackModel> tracks)
    {
        for(AudioTrackModel track : tracks)
            msgToTrack.put(track.getMessageId(), track);
        playlist.addAll(tracks);
    }
    public void initializePlaylist(AudioTrackModel track, long chat_id)
    {
        if(enableRepeat)
            enableRepeat = false;
        if(enableShuffle)
            enableShuffle = false;
        if(playlist.isEmpty())
        {
            playlist.add(track);
            msgToTrack.put(track.getMessageId(), track);
            startUpdatingPlaylist();
            this.chatId = chat_id;
            return;
        }
        if(chat_id != this.chatId)
        {
            stopPlayback(true);
            msgToTrack.put(track.getMessageId(), track);
            playlist.add(track);
        }
        else
        {
            int index = playlist.indexOf(track);
            stopPlayback(false);
            if(index >= 0) {
                for(int i = 0; i < playlist.size(); i++) {
                    AudioTrackModel removedTrack = playlist.remove(i);
                    previousTracks.add(0, removedTrack);
                }
            }
            playlist.add(0, track);
        }
        this.chatId = chat_id;
        startUpdatingPlaylist();
    }
    private void startUpdatingPlaylist()
    {
        initObservers();
        TelegramUpdatesHandler updateHandler = TelegramApplication.sharedApplication().getUpdateService();
        if(updateHandler != null && updateMessageProcessor != null)
            updateMessageProcessor.attachObservers(updateHandler);
    }
    public void playNextTrack()
    {
        if(BuildConfig.DEBUG)
            Log.d("PlaybackService", "next track requested");
        if(enableShuffle) {
            if(enableRepeat && shuffledPlaylist.isEmpty())
                return;
            else if(shuffledPlaylist.size() <= 1)
                return;
        }
        else
        {
            if(enableRepeat && playlist.isEmpty())
                return;
            else if(playlist.size() <= 1)
                return;
        }
        if(!enableRepeat)
        {
            AudioTrackModel nowPlayed = enableShuffle ? shuffledPlaylist.remove(0) : playlist.remove(0);
            if(nowPlayed.getTrack().getDownloadState() != FileModel.DownloadState.LOADED)
                FileCache.getInstance().cancelDownload(nowPlayed.getTrack());
            previousTracks.add(0, nowPlayed);
        }
        final AudioTrackModel nextTrack = enableShuffle ? shuffledPlaylist.get(0) : playlist.get(0);
        if(nextTrack.getTrack().getDownloadState() == FileModel.DownloadState.LOADED) {
            playSong(true);
            return;
        }
        downloadAndPlay(nextTrack);
    }
    private void downloadAndPlay(final AudioTrackModel track)
    {
        if(BuildConfig.DEBUG)
            Log.d("PlaybackService", "downloading track...");
        FileModel.FileModelDelegate trackDownloadDelegate = new FileModel.FileModelDelegate() {
            @Override
            public String getDelegateKey() {
                return String.format("chat%d_track%d_delegate", chatId, track.getTrack().getFileId());
            }

            @Override
            public void didFileDownloadStarted() {
            }

            @Override
            public void didFileProgressUpdated() {
            }

            @Override
            public void didFileDownloaded() {
                track.getTrack().removeDelegate(this);
                if(!cancelAsyncPlaySong) {
                    if(delegate != null)
                        delegate.didTrackDownloadFinished();
                    playSong(true);
                }
                cancelAsyncPlaySong = false;
            }

            @Override
            public void didFileDownloadCancelled() {
            }
        };
        track.getTrack().addDelegate(trackDownloadDelegate);
        playSong(false);
        FileCache.getInstance().downloadFile(track.getTrack());
    }
    public void playPreviousTrack()
    {
        if(mediaPlayer.isPlaying())
        {
            if(mediaPlayer.getCurrentPosition() >= 5000)
            {
                mediaPlayer.seekTo(0);
                return;
            }
        }
        if(previousTracks.isEmpty())
            return;
        if(nowPlaying.getTrack().getDownloadState() != FileModel.DownloadState.LOADED)
            FileCache.getInstance().cancelDownload(nowPlaying.getTrack());
        AudioTrackModel trackToPlay = previousTracks.remove(0);
        if(enableShuffle)
            shuffledPlaylist.add(0, trackToPlay);
        else
            playlist.add(0, trackToPlay);
        if(trackToPlay.getTrack().getDownloadState() == FileModel.DownloadState.LOADED) {
            playSong(true);
            return;
        }
        downloadAndPlay(trackToPlay);
    }
    public void togglePlayback()
    {
        if(mediaPlayer.isPlaying())
            pausePlayback();
        else
            unpausePlayback();
    }
    public void pausePlayback()
    {
        if(!mediaPlayer.isPlaying())
            return;
        stopPlayProgressUpdater();
        mediaPlayer.pause();
        if(delegate != null)
            delegate.didPlaybackPaused();
        notifySubscribersAboutPlaybackPause();
    }
    public void unpausePlayback()
    {
        if(mediaPlayer.isPlaying())
            return;
        if(nowPlayingWasRemoved)
        {
            nowPlayingWasRemoved = false;
            playSong(true);
            return;
        }
        startPlayProgressUpdater();
        mediaPlayer.start();
        if(delegate != null)
            delegate.didPlaybackUnpaused();
        notifySubscribersAboutPlaybackStart();
    }
    public void stopPlayback(boolean clear)
    {
        stopPlayProgressUpdater();
        notifySubscribersAboutPlaybackStop();
        if(mediaPlayer.isPlaying())
            mediaPlayer.stop();
        mediaPlayer.reset();
        if(clear)
        {
            try {
                if(currentlyLoadingPlaylist != null) {
                    currentlyLoadingPlaylist.cancel();
                    currentlyLoadingPlaylist = null;
                }
            }
            catch(Exception e) { e.printStackTrace(); }
            chatId = 0;
            enableShuffle = false;
            enableRepeat = false;
            nowPlaying = null;
            previousTracks.clear();
            shuffledPlaylist.clear();
            playlist.clear();
            msgToTrack.clear();
            writtenAudio.clear();
        }
        else
        {
            AudioTrackModel playedNow = nowPlaying;
            if(playedNow != null && (previousTracks.isEmpty() || !previousTracks.get(0).equals(playedNow)))
                previousTracks.add(0, playedNow);
        }
        if(updateMessageProcessor != null)
            updateMessageProcessor.removeObservers();
    }
    public void requestSeekTo(int position)
    {
        mediaPlayer.seekTo(position);
    }
    public AudioTrackModel nowPlaying()
    {
        return nowPlaying;
    }

    public void setPlaybackDelegate(PlaybackDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    public void removePlaybackDelegate(PlaybackDelegate which)
    {
        if(this.delegate == null)
            return;
        if(this.delegate.getDelegateId() == which.getDelegateId())
            this.delegate = null;
    }

    private void startPlayProgressUpdater() {
        Runnable notification = new Runnable() {
            public void run() {

                if(mediaPlayer.isPlaying() && delegate != null)
                    delegate.didTrackPositionChanged(mediaPlayer.getCurrentPosition());
                startPlayProgressUpdater();
            }
        };
        handler.postDelayed(notification,1000);
    }
    private void stopPlayProgressUpdater()
    {
        handler.removeCallbacksAndMessages(null);
    }
    public boolean isAudioPlaying() {
        if(mediaPlayer == null)
            return false;
        return mediaPlayer.isPlaying();
    }
    public void addSimpleDelegate(SimplePlaybackDelegate delegate)
    {
        simpleDelegateMap.put(delegate.getDelegateKey(), delegate);
    }
    public void removeSimpleDelegate(SimplePlaybackDelegate delegate)
    {
        simpleDelegateMap.remove(delegate.getDelegateKey());
    }
    private void notifySubscribersAboutPlaybackStop()
    {
        if(nowPlaying == null)
            return;
        if(delegate != null)
            delegate.didPlaybackStopped();
        for(String k : simpleDelegateMap.keySet())
            simpleDelegateMap.get(k).didPlaybackStopped();
    }
    private void notifySubscribersAboutPlaybackPause()
    {
        if(nowPlaying == null)
            return;
        if(delegate != null)
            delegate.didPlaybackPaused();
        for(String k : simpleDelegateMap.keySet())
            simpleDelegateMap.get(k).didPlaybackPaused(nowPlaying.getTrack());
    }
    private void notifySubscribersAboutPlaybackStart()
    {
        if(nowPlaying == null)
            return;
        MessageVoicePlayer.getInstance().stopPlayback();
        for(String k : simpleDelegateMap.keySet())
            simpleDelegateMap.get(k).didPlaybackStarted(nowPlaying.getTrack());
    }
    public void createPlaylistForChat(long chat_id, AudioTrackModel initTrack) {
        if(currentlyLoadingPlaylist != null)
        {
            if(currentlyLoadingPlaylist.getChatId() != chat_id)
                currentlyLoadingPlaylist.cancel();
            else
                return;
        }
        currentlyLoadingPlaylist = new CreatePlaylistTask(chat_id, initTrack, writtenAudio);
        currentlyLoadingPlaylist.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(AsyncTaskManTask task) {
                CreatePlaylistTask finishedTask = (CreatePlaylistTask) task;
                if(finishedTask.isCancelled()) {
                    cancelAsyncPlaySong = true;
                    return;
                }
                addToPlaylist(finishedTask.getPlaylist());
            }
        });
        currentlyLoadingPlaylist.addToQueue();
    }
    public void toggleRepeat()
    {
        this.enableRepeat = !enableRepeat;
    }
    public boolean isRepeatEnabled() { return enableRepeat; }
    public long getPlaylistChatId() { return this.chatId; }
    public void toggleShuffle() {
        this.enableShuffle = !enableShuffle;
        if(enableShuffle)
            shufflePlaylist();
    }
    public boolean isShuffleEnabled() { return this.enableShuffle; }

    private void shufflePlaylist()
    {
        if(BuildConfig.DEBUG)
        {
            Log.d("AudioPlaybackService", playlist.toString());
        }
        shuffledPlaylist.clear();
        shuffledPlaylist.addAll(playlist);
        Collections.shuffle(shuffledPlaylist, new Random(System.currentTimeMillis()));
    }
}
