package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.viewcontrollers.NewChatContactsListViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.views.ContactsListView;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.NewChatActionView;

/**
 * Created by andremacareno on 13/04/15.
 */
public class NewGroupFragment extends BaseFragment {
    private final String TAG = "NewChatFragment";
    private ContactsListView roster;
    protected NewChatContactsListViewController listController;
    private NewChatActionViewController actionViewController;
    private boolean disabledDoneButton = true;
    private int chatParticipantsCount = 0;
    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new NewChatActionView(getActivity());
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new NewChatActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        if(listController == null) {
            listController = new NewChatContactsListViewController(roster);
            listController.setFragmentReference(this);
            roster.setController(listController);
        }
        else
        {
            roster.setController(listController);
            listController.replaceView(roster);
        }
        listController.onUpdateServiceConnected(service);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        roster = new ContactsListView(inflater.getContext());
        roster.setLayoutParams(new ViewGroup.LayoutParams(container.getLayoutParams()));
        roster.setVisibility(View.VISIBLE);
        return roster;
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(listController != null)
            listController.removeObservers();
    }

    public void enableDoneButton()
    {
        disabledDoneButton = false;
    }
    public void disableDoneButton()
    {
        disabledDoneButton = true;
    }

    public void goNext()
    {
        Fragment fr = new NewChatEnterTitleFragment();
        Bundle b = new Bundle();
        b.putIntArray("participant_ids", listController.getParticipants());
        fr.setArguments(b);
        String tag = Constants.ENTER_CHAT_TITLE_FRAGMENT;
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.container, fr, tag).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }

    public void updateActionViewSubtitle(int newCount) {
        this.chatParticipantsCount = newCount;
        String str = String.format(TelegramApplication.sharedApplication().getResources().getString(R.string.participants_count), chatParticipantsCount, Constants.MAX_CHAT_PARTICIPANTS);
        ((NewChatActionView)actionViewController.getActionView()).updateSecondLine(str);
    }
    private class NewChatActionViewController extends ActionViewController
    {
        private View.OnClickListener backButtonListener;
        private View.OnClickListener doneButtonListener;
        @Override
        public void onFragmentPaused() {
            ((NewChatActionView) getActionView()).setBackButtonClickListener(null);
            ((NewChatActionView) getActionView()).setDoneButtonClickListener(null);
        }

        @Override
        public void onFragmentResumed() {
            if(backButtonListener == null)
                backButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goBack();
                    }
                };
            if(doneButtonListener == null)
                doneButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!disabledDoneButton)
                            goNext();
                    }
                };
            updateActionViewSubtitle(chatParticipantsCount);
            ((NewChatActionView) getActionView()).setBackButtonClickListener(backButtonListener);
            ((NewChatActionView) getActionView()).setDoneButtonClickListener(doneButtonListener);
        }

        @Override
        public void onActionViewRemoved() {

        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
}
