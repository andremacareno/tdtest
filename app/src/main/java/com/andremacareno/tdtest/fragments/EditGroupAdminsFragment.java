package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.GroupCache;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.viewcontrollers.ChooseAdminsListController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SimpleActionViewController;
import com.andremacareno.tdtest.views.ChatParticipantsListView;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 03/04/15.
 */
public class EditGroupAdminsFragment extends BaseFragment {
    private final String actionViewTag = "EditGroupAdminsAV";
    private long chatId;
    private int groupId;
    private View allAreAdminsView;
    private TextView allAreAdminsHelp;
    private SwitchCompat allAreAdminsSwitch;
    private View.OnClickListener allAreAdminsClickListener;
    private ActionViewController actionViewController;
    private ChatParticipantsListView participantsListView;
    private ChooseAdminsListController listController;
    private boolean allAreAdmins = false;
    private String allCanAdd, onlyAdminsCanAdd;
    public boolean updating = false;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getArguments() == null)
            return;
        this.chatId = getArguments().getLong(Constants.ARG_CHAT_ID);
        this.groupId = getArguments().getInt(Constants.ARG_GROUP_ID);

        TdApi.Group group = GroupCache.getInstance().getGroup(groupId, false);
        allAreAdmins = group.anyoneCanEdit;
        allCanAdd = String.format(getResources().getString(R.string.admins_help), getResources().getString(R.string.all_members));
        onlyAdminsCanAdd = String.format(getResources().getString(R.string.admins_help), getResources().getString(R.string.only_admins));
        final Runnable refreshRunnable = new Runnable() {
            @Override
            public void run() {
                refresh();
                updating = false;
            }
        };
        final Client.ResultHandler resultHandler = new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(object == null || object.getConstructor() != TdApi.Group.CONSTRUCTOR)
                    return;
                TdApi.Group group = (TdApi.Group) object;
                allAreAdmins = group.anyoneCanEdit;
                TelegramApplication.applicationHandler.post(refreshRunnable);
            }
        };
        allAreAdminsClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(updating)
                    return;
                updating = true;
                TdApi.ToggleGroupEditors toggle = new TdApi.ToggleGroupEditors(groupId, !allAreAdmins);
                TG.getClientInstance().send(toggle, resultHandler);
            }
        };
    }
    @Override
    public void onResume()
    {
        super.onResume();
        if(listController == null) {
            listController = new ChooseAdminsListController(participantsListView, groupId, chatId, allAreAdmins, this);
            participantsListView.setController(listController);
        }
        else
        {
            participantsListView.setController(listController);
            listController.replaceView(participantsListView);
        }
        refresh();
    }
    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new SimpleActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new EditChatNameActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.group_admins_settings, container, false);
        participantsListView = (ChatParticipantsListView) v.findViewById(R.id.participants_list);
        allAreAdminsView = v.findViewById(R.id.all_admins_toggle);
        allAreAdminsHelp = (TextView) v.findViewById(R.id.all_admins_help);
        allAreAdminsSwitch = (SwitchCompat) allAreAdminsView.findViewById(R.id.all_admins_switch);
        allAreAdminsSwitch.setClickable(false);
        allAreAdminsView.setOnClickListener(allAreAdminsClickListener);
        return v;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    private void refresh()
    {
        if(allAreAdmins)
        {
            allAreAdminsSwitch.setChecked(true);
            allAreAdminsHelp.setText(allCanAdd);
        }
        else
        {
            allAreAdminsSwitch.setChecked(false);
            allAreAdminsHelp.setText(onlyAdminsCanAdd);
        }
        if(listController != null)
            listController.notifyAllAdminsFlagChanged(allAreAdmins);
    }

    private class EditChatNameActionViewController extends SimpleActionViewController {
        private View.OnClickListener doneButtonListener;

        @Override
        public boolean havePopup() {
            return false;
        }

        @Override
        public boolean showBackButton() {
            return true;
        }

        @Override
        public boolean showDoneButton() {
            return false;
        }

        @Override
        public String getTitle() {
            return getResources().getString(R.string.administrators);
        }

        @Override
        public View.OnClickListener getBackButtonListener() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goBack();
                }
            };
        }

        @Override
        public View.OnClickListener getDoneButtonListener() {
            return null;
        }

        @Override
        public int getMenuResource() {
            return 0;
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
}
