package com.andremacareno.tdtest.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.OnTaskFailureListener;
import com.andremacareno.tdtest.BackPressListener;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.GroupCache;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.CurrentUserModel;
import com.andremacareno.tdtest.models.SharedImageModel;
import com.andremacareno.tdtest.models.SharedMedia;
import com.andremacareno.tdtest.models.TelegramAction;
import com.andremacareno.tdtest.tasks.CountAdminsTask;
import com.andremacareno.tdtest.tasks.CreateFileForCameraTask;
import com.andremacareno.tdtest.tasks.GalleryImagePathTask;
import com.andremacareno.tdtest.updatelisteners.UpdateChatProcessor;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.ChatParticipantsListController;
import com.andremacareno.tdtest.viewcontrollers.SharedMediaLightListViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ChatInfoActionViewController;
import com.andremacareno.tdtest.views.ChatAdminsCountButton;
import com.andremacareno.tdtest.views.ChatParticipantsListView;
import com.andremacareno.tdtest.views.SharedMediaButton;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.GroupChatActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.lang.ref.WeakReference;

/**
 * Created by Andrew on 09.05.2015.
 */
public class GroupChatProfileFragment extends BaseFragment {
    public interface ParticipantListDelegate
    {
        public void goToParticipantProfile(int id, long chatId);
        public void removeParticipant(int id);
    }
    public interface ProfileDelegate
    {
        public void refreshAdminsCount();
        public void groupNameDoneButtonPressed();
        public void editPhotoAlertRequested();
    }
    private SharedMediaLightListViewController.SharedMediaLightListDelegate mediaListDelegate;
    private long chatId;
    private int groupId = 0;
    private ScrollView container;
    private BackPressListener editModeBackPress;
    private GroupChatActionView actionView;
    private ChatInfoActionViewController actionViewController;
    private ChatAdminsCountButton adminsButton;
    private View adminsButtonDivider;
    private SharedMediaLightListViewController previewListController;
    private View addMemberButton;
    private TextView addMemberCaption, supergroupConvertDescription;
    private SharedMediaButton sharedMediaView;
    private ChatParticipantsListView listView;
    private ChatParticipantsListController listController;
    private GalleryImagePathTask galleryImagePathTask;
    private CameraFileReceiver cameraFileReceiver;
    private String photo_path = null;
    private ParticipantListDelegate participantListDelegate;
    private View.OnClickListener adminsButtonClickListener;
    private ProfileDelegate profileDelegate;
    private ActionButtonDelegate actionButtonDelegate;
    private boolean editMode = false;
    private boolean removeChatPhoto = false;
    private boolean updating = false;
    private ProgressDialog processingDialog;
    private UpdateChatProcessor updateChatProcessor;
    //private final int supergroupThreshold = TelegramApplication.testDc ? 5 : 200;
    private final int supergroupThreshold = 5;
    private final View.OnClickListener addMemberClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            addMember();
        }
    };
    private final View.OnClickListener upgradeToSupergroupClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            convertToSupergroup();
        }
    };
    @Override
    public void onPause()
    {
        super.onPause();
        if(processingDialog.isShowing())
            processingDialog.dismiss();
        if(previewListController != null)
            previewListController.onFragmentPaused();
        if(updateChatProcessor != null)
            updateChatProcessor.performPauseObservers();
    }
    @Override
    public void onResume()
    {
        super.onResume();
        if(previewListController != null)
            previewListController.onFragmentResumed();
        if(updateChatProcessor != null)
            updateChatProcessor.performResumeObservers();
        processingDialog= new ProgressDialog(getActivity());
        processingDialog.setMessage(getResources().getString(R.string.updating));
        processingDialog.setIndeterminate(true);
        processingDialog.setCancelable(false);
        if(updating)
            processingDialog.show();
    }
    @Override
    public void onPasscodeLockShown()
    {
        if(actionView != null)
            actionView.chatView.closeKeyboard();
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getArguments() == null)
            return;
        editModeBackPress = new BackPressListener() {
            @Override
            public void onBackPressed() {
                if(editMode) {
                    ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(this);
                    TdApi.Group group = GroupCache.getInstance().getGroup(groupId, false);
                    if(group != null && group.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor()) {
                        adminsButton.setVisibility(View.VISIBLE);
                        adminsButtonDivider.setVisibility(View.VISIBLE);
                    }
                    sharedMediaView.setVisibility(View.VISIBLE);
                    actionViewController.setEditMode(false);
                    editMode = false;
                    invalidateActionButton();
                }
            }
        };
        profileDelegate = new ProfileDelegate() {
            @Override
            public void refreshAdminsCount() {
                countAdmins();
            }

            @Override
            public void groupNameDoneButtonPressed() {
                finishEdit();
            }

            @Override
            public void editPhotoAlertRequested() {
                if(groupId != 0)
                {
                    TdApi.Chat chat = ChatCache.getInstance().getChatById(chatId);
                    TdApi.File chatPhoto = CommonTools.isLowDPIScreen() ? chat.photo.small : chat.photo.big;
                    CharSequence[] items = chatPhoto.id == 0 ? new CharSequence[]{getResources().getString(R.string.from_camera), getResources().getString(R.string.from_gallery)} : new CharSequence[]{getResources().getString(R.string.from_camera), getResources().getString(R.string.from_gallery), getResources().getString(R.string.delete_photo)};
                    AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
                    adb.setItems(items, new AlertDialog.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (i == 0)
                                openCameraForNewPhoto();
                            else if(i == 1)
                                openGalleryForNewPhoto();
                            else if(i == 2)
                                removePhoto();
                        }
                    });
                    adb.create().show();
                }
            }
        };
        adminsButtonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAdminSettings();
            }
        };
        if(participantListDelegate == null)
            participantListDelegate = new ParticipantListDelegate() {
                @Override
                public void goToParticipantProfile(int id, long chatId) {
                    if(BuildConfig.DEBUG)
                        Log.d("ViewGroup", "open profile requested");
                    openProfile(id, chatId);
                }

                @Override
                public void removeParticipant(int id) {
                    createKickFromGroupDialog(id);
                }
            };
        mediaListDelegate = new SharedMediaLightListViewController.SharedMediaLightListDelegate() {
            @Override
            public void didMediaCountReceived(int count) {
                if(sharedMediaView != null)
                    sharedMediaView.setMediaCount(count);
            }

            @Override
            public void didItemClicked(SharedMedia item) {
                openPhotoFragment((SharedImageModel) item);
            }
        };
        this.chatId = getArguments().getLong(Constants.ARG_CHAT_ID);
        this.groupId = getArguments().getInt(Constants.ARG_GROUP_ID, 0);
        galleryImagePathTask = new GalleryImagePathTask();
        this.cameraFileReceiver = new CameraFileReceiver(this);
    }

    @Override
    public ActionView getActionView()
    {
        if(actionView == null)
        {
            actionView = new GroupChatActionView(getActivity());
            actionView.setBackButtonClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goBack();
                }
            });
        }
        return actionView;
    }
    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null) {
            actionViewController = new ChatInfoActionViewController(groupId, this.chatId);
            actionViewController.setProfileDelegate(profileDelegate);
            initUpdateProcessor();
        }
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        if(listController != null)
            listController.onUpdateServiceConnected(service);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(actionView != null)
            actionView.chatView.closeKeyboard();
        this.container = (ScrollView) inflater.inflate(R.layout.chat_info, container, false);
        addMemberButton = this.container.findViewById(R.id.add_member_button);
        addMemberCaption = (TextView) addMemberButton.findViewById(R.id.add_member_caption);
        addMemberButton.setOnClickListener(addMemberClickListener);
        supergroupConvertDescription = (TextView) this.container.findViewById(R.id.supergroup_help);
        listView = (ChatParticipantsListView) this.container.findViewById(R.id.participants_list);
        if(groupId != 0) {
            if(listController == null) {
                listController = new ChatParticipantsListController(listView, groupId, this.chatId);
                listView.setController(listController);
            }
            else
            {
                listView.setController(listController);
                listController.replaceView(listView);
            }
            if(listController != null)
                listController.addDelegate(participantListDelegate);
        }
        adminsButton = (ChatAdminsCountButton) this.container.findViewById(R.id.chat_admins_count);
        adminsButtonDivider = this.container.findViewById(R.id.chat_admins_count_divider);
        sharedMediaView = (SharedMediaButton) this.container.findViewById(R.id.shared_media_button);
        sharedMediaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSharedMedia();
            }
        });
        return this.container;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null && editMode)
            didEditModeButtonClicked(true);
        PopupMenu.OnMenuItemClickListener menuListener = new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if(menuItem.getItemId() == R.id.action_ragequit)
                    performRageQuit();
                return true;
            }
        };
        TdApi.Group group = GroupCache.getInstance().getGroup(groupId, false);
        if(group != null)
        {
            if(group.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor()) {
                if(group.participantsCount >= supergroupThreshold) {
                    addMemberCaption.setText(R.string.upgrade_to_supergroup);
                    addMemberButton.setOnClickListener(upgradeToSupergroupClickListener);
                    supergroupConvertDescription.setVisibility(View.VISIBLE);
                }
                else
                {
                    addMemberButton.setOnClickListener(addMemberClickListener);
                    addMemberCaption.setText(R.string.add_member);
                    supergroupConvertDescription.setVisibility(View.GONE);
                }
                addMemberButton.setVisibility(View.VISIBLE);
                adminsButton.setOnClickListener(adminsButtonClickListener);
                adminsButton.setVisibility(editMode ? View.GONE : View.VISIBLE);
                if(group.anyoneCanEdit)
                    adminsButton.setAllAdmins();
                else
                    countAdmins();
            }
            else {
                addMemberButton.setOnClickListener(addMemberClickListener);
                addMemberButton.setVisibility(group.anyoneCanEdit ? View.VISIBLE : View.GONE);
                adminsButton.setVisibility(View.GONE);
                addMemberCaption.setText(R.string.add_member);
                supergroupConvertDescription.setVisibility(View.GONE);
            }
            adminsButtonDivider.setVisibility(adminsButton.getVisibility());
        }
        if(actionViewController != null)
            actionViewController.setMenuListener(menuListener);
        if(previewListController == null && getArguments() != null && getArguments().containsKey(Constants.ARG_CHAT_ID)) {
            previewListController = new SharedMediaLightListViewController(sharedMediaView.getPreviewList(), getArguments().getLong(Constants.ARG_CHAT_ID));
            previewListController.setListDelegate(mediaListDelegate);
        }
        else if(previewListController != null) {
            previewListController.replaceView(sharedMediaView.getPreviewList());
            previewListController.setListDelegate(mediaListDelegate);
        }
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        if(groupId == 0)
            return null;
        TdApi.Group group = GroupCache.getInstance().getGroup(groupId, false);
        if(group == null)
            return null;
        if(!group.anyoneCanEdit && group.role.getConstructor() != Constants.PARTICIPANT_ADMIN.getConstructor())
            return null;
        if(group.anyoneCanEdit && (group.role.getConstructor() != Constants.PARTICIPANT_ADMIN.getConstructor() && group.role.getConstructor() != Constants.PARTICIPANT_EDITOR.getConstructor() && group.role.getConstructor() != Constants.PARTICIPANT_GENERAL.getConstructor()))
            return null;
        if(actionButtonDelegate == null)
            actionButtonDelegate = new ActionButtonDelegate() {
                @Override
                public void onActionButtonClick() {
                    if(updating)
                        return;
                    didEditModeButtonClicked(false);
                    editMode = !editMode;
                    invalidateActionButton();
                }

                @Override
                public int getActionButtonImageResource() {
                    return editMode ? R.drawable.ic_check_gray : R.drawable.ic_edit;
                }
            };
        return actionButtonDelegate;
    }
    private void didEditModeButtonClicked(boolean restore)
    {
        if((!editMode && !restore) || (editMode && restore))
        {
            ((TelegramMainActivity) getActivity()).interceptBackPress(editModeBackPress);
            actionViewController.setEditMode(true);
            adminsButton.setVisibility(View.GONE);
            sharedMediaView.setVisibility(View.GONE);
        }
        else if(editMode) {
            ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(editModeBackPress);
            TdApi.Group group = GroupCache.getInstance().getGroup(groupId, false);
            if(group != null && group.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor())
                adminsButton.setVisibility(View.VISIBLE);
            sharedMediaView.setVisibility(View.VISIBLE);
            finishEdit();
            actionViewController.setEditMode(false);
        }
        adminsButtonDivider.setVisibility(adminsButton.getVisibility());
    }
    private void finishEdit()
    {
        if(!editMode || actionView == null)
            return;
        updating = true;
        if(processingDialog != null)
            processingDialog.show();
        String newGroupName = actionView.chatView.getNewTitle();
        if(newGroupName != null && !newGroupName.isEmpty())
        {
            TdApi.ChangeChatTitle changeTitleReq = new TdApi.ChangeChatTitle(chatId, newGroupName);
            TG.getClientInstance().send(changeTitleReq, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    performChangePhoto();
                }
            });
        }
        else
            performChangePhoto();
    }
    private void performChangePhoto()
    {
        if(removeChatPhoto || photo_path != null)
        {
            TdApi.ChangeChatPhoto changePhotoReq = new TdApi.ChangeChatPhoto(chatId, removeChatPhoto ? new TdApi.InputFileId(0) : new TdApi.InputFileLocal(photo_path), null);
            TG.getClientInstance().send(changePhotoReq, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    TelegramApplication.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            updating = false;
                            if (processingDialog != null)
                                processingDialog.dismiss();
                        }
                    });
                }
            });
        }
        else {
            updating = false;
            TelegramApplication.applicationHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (processingDialog != null)
                        processingDialog.dismiss();
                }
            });
        }
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(listController != null)
            listController.removeObservers();
        if(updateChatProcessor != null)
            updateChatProcessor.removeObservers();
        actionViewController = null;
    }
    protected void openProfile(int user, long chatId)
    {
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_CHAT_ID, chatId);
        args.putLong(Constants.ARG_CHAT_REFERER, this.chatId);
        args.putInt(Constants.ARG_USER_ID, user);
        ProfileFragment fr = new ProfileFragment();
        fr.setArguments(args);
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction tr = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        tr.replace(R.id.container, fr, Constants.VIEW_PROFILE_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    protected void convertToSupergroup()
    {
        updating = true;
        if(processingDialog != null)
            processingDialog.show();
        TdApi.MigrateGroupChatToChannelChat upgradeReq = new TdApi.MigrateGroupChatToChannelChat(chatId);
        TG.getClientInstance().send(upgradeReq, new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                TelegramApplication.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        updating = false;
                        if (processingDialog != null)
                            processingDialog.dismiss();
                        try {
                            ((TelegramMainActivity) getActivity()).clearTillMainFragmentNextTime();
                            if (object.getConstructor() == TdApi.Chat.CONSTRUCTOR) {
                                openSupergroup((TdApi.Chat) object);
                            } else {
                                goBack();
                            }
                        } catch (Exception ignored) {
                        }
                    }
                });
            }
        });
    }
    protected void addMember()
    {
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_CHAT_ID, chatId);
        TdApi.GroupFull groupFull = GroupCache.getInstance().getGroupFull(groupId, false);
        if(groupFull.inviteLink != null && !groupFull.inviteLink.isEmpty())
            args.putString(Constants.ARG_CHAT_INVITE_LINK, groupFull.inviteLink);
        if(listController != null)
            args.putIntArray(Constants.ARG_IGNORE_IDS, listController.getParticipantIds());
        AddMemberFragment fr = new AddMemberFragment();
        fr.setRetainInstance(true);
        fr.setArguments(args);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction tr = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        tr.replace(R.id.container, fr, Constants.ADD_MEMBER_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    public void createKickFromGroupDialog(final int userId)
    {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setItems(new CharSequence[]{getResources().getString(R.string.remove_from_group)}, new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0) {
                    TdApi.ChangeChatParticipantRole kickRequest = new TdApi.ChangeChatParticipantRole(chatId, userId, Constants.PARTICIPANT_KICKED);
                    TG.getClientInstance().send(kickRequest, TelegramApplication.dummyResultHandler);
                }
            }
        });
        adb.create().show();
    }
    private void performRageQuit()
    {
        ((TelegramMainActivity) getActivity()).clearTillMainFragmentNextTime();
        TdApi.DeleteChatHistory rage = new TdApi.DeleteChatHistory(this.chatId);
        TdApi.ChangeChatParticipantRole quit = new TdApi.ChangeChatParticipantRole(this.chatId, CurrentUserModel.getInstance().getUserModel().getId(), Constants.PARTICIPANT_LEFT);
        TG.getClientInstance().send(rage, TelegramApplication.dummyResultHandler);
        TG.getClientInstance().send(quit, TelegramApplication.dummyResultHandler);
        goBack();
    }

    private void openCameraForNewPhoto()
    {
        if(!AndroidUtilities.haveCamera())
        {
            Toast.makeText(TelegramApplication.sharedApplication().getApplicationContext(), R.string.no_camera, Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Activity activity = getActivity();
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            CreateFileForCameraTask task = new CreateFileForCameraTask();
            task.onComplete(new OnTaskCompleteListener() {
                @Override
                public void taskComplete(AsyncTaskManTask task) {
                    CreateFileForCameraTask t = (CreateFileForCameraTask) task;
                    photo_path = t.getPhotoPath();
                    if(photo_path != null)
                        removeChatPhoto = false;
                    Message msg = Message.obtain(cameraFileReceiver);
                    msg.obj = t.getPhotoUri();
                    msg.sendToTarget();
                }
            });
            task.onFailure(new OnTaskFailureListener() {
                @Override
                public void taskFailed(AsyncTaskManTask task) {
                    //Toast.makeText((fragmentRef.get().getActivity()), R.string.error_taking_photo, Toast.LENGTH_SHORT).show();
                }
            });
            task.addToQueue();
        }
    }
    private void openSupergroup(TdApi.Chat chat)
    {
        if(chat == null || chat.type.getConstructor() != TdApi.ChannelChatInfo.CONSTRUCTOR)
            return;
        try
        {
            Bundle args = new Bundle();
            args.putLong(Constants.ARG_CHAT_ID, chat.id);
            args.putInt(Constants.ARG_LAST_MSG_ID, chat.topMessage.id);
            args.putBoolean(Constants.ARG_CHAT_MUTED, chat.notificationSettings.muteFor > 0);
            args.putBoolean(Constants.ARG_BOT, false);
            args.putInt(Constants.ARG_REPLY_MARKUP_MSG, 0);
            args.putInt(Constants.ARG_CHAT_PEER, ((TdApi.ChannelChatInfo) chat.type).channel.id);
            args.putBoolean(Constants.ARG_DELETED_ACCOUNT, false);
            args.putInt(Constants.ARG_LAST_READ_INBOX, chat.lastReadInboxMessageId);
            args.putInt(Constants.ARG_LAST_READ_OUTBOX, 2000000000);
            args.putInt(Constants.ARG_UNREAD_COUNT, chat.unreadCount);
            args.putInt(Constants.ARG_START_MESSAGE, chat.topMessage.id);
            args.putString(Constants.ARG_CHAT_TITLE, chat.title);
            Fragment fr;
            args.putInt(Constants.ARG_SUPERGROUP_ID, ((TdApi.ChannelChatInfo) chat.type).channel.id);
            args.putBoolean(Constants.ARG_CHAT_LEFT, false);
            fr = new GroupChatFragment();
            fr.setRetainInstance(true);
            fr.setArguments(args);
            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
            ft.replace(R.id.container, fr, Constants.CHAT_FRAGMENT);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
            fm.executePendingTransactions();
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    private void openGalleryForNewPhoto()
    {
        if (Build.VERSION.SDK_INT <19){
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(galleryIntent, TelegramAction.ATTACH_CHOOSE_FROM_GALLERY);
        } else {
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(galleryIntent, TelegramAction.ATTACH_CHOOSE_FROM_GALLERY);
        }
    }
    private void removePhoto()
    {
        removeChatPhoto = true;
        photo_path = null;
        if(actionView != null)
            actionView.chatView.removePhoto();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == Activity.RESULT_OK)
        {
            if(requestCode == TelegramAction.ATTACH_TAKE_A_PHOTO)
            {
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                if(actionView != null && photo_path != null) {
                    TelegramApplication.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            TelegramImageLoader.getInstance().loadAvatarPreview(photo_path, actionView.chatView.avatar);
                        }
                    });
                }
                File f = new File("file:".concat(photo_path));
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                getActivity().sendBroadcast(mediaScanIntent);
            }
            else if(requestCode == TelegramAction.ATTACH_CHOOSE_FROM_GALLERY)
            {
                Uri uri = data.getData();
                if(uri == null)
                    Toast.makeText(getActivity(), R.string.error_picking_from_gallery, Toast.LENGTH_LONG).show();
                else
                {
                    if(Build.VERSION.SDK_INT >= 19)
                    {
                        final int takeFlags = data.getFlags()
                                & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        // Check for the freshest data.
                        //noinspection ResourceType
                        TelegramApplication.sharedApplication().getContentResolver().takePersistableUriPermission(uri, takeFlags);
                    }
                    galleryImagePathTask.setLink(uri);
                    galleryImagePathTask.onFailure(new OnTaskFailureListener() {
                        @Override
                        public void taskFailed(AsyncTaskManTask task) {
                            try
                            {
                                TelegramApplication.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), R.string.error_picking_from_gallery, Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            catch(Exception ignored) {}
                        }
                    });
                    galleryImagePathTask.onComplete(new OnTaskCompleteListener() {
                        @Override
                        public void taskComplete(AsyncTaskManTask task) {
                            photo_path = ((GalleryImagePathTask) task).getImagePath();
                            if(actionView != null && photo_path != null) {
                                TelegramApplication.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        TelegramImageLoader.getInstance().loadAvatarPreview(photo_path, actionView.chatView.avatar);
                                    }
                                });
                            }
                            if(photo_path != null)
                                removeChatPhoto = false;
                        }
                    });
                    galleryImagePathTask.addToQueue();
                }
            }
        }
    }
    private void openSharedMedia()
    {
        Fragment fr = new SharedMediaFragment();
        Bundle b = new Bundle();
        if(getArguments() == null || !getArguments().containsKey(Constants.ARG_CHAT_ID)) {
            Log.d("ProfileFragment", "missing chat id");
            return;
        }
        b.putLong(Constants.ARG_CHAT_ID, getArguments().getLong(Constants.ARG_CHAT_ID));
        fr.setArguments(b);
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        ft.replace(R.id.container, fr, Constants.SHARED_MEDIA_FRAGMENT);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    public void countAdmins()
    {
        if(chatId == 0)
            return;
        CountAdminsTask t = new CountAdminsTask(chatId);
        t.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(AsyncTaskManTask task) {
                final int count = ((CountAdminsTask) task).getResult();
                    TelegramApplication.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            adminsButton.setAdmins(count + 1);
                        }
                    });
            }
        });
        t.addToQueue();
    }
    public void openAdminSettings()
    {
        if(chatId == 0 || groupId == 0)
            return;
        Fragment fr = new EditGroupAdminsFragment();
        Bundle b = new Bundle();
        b.putLong(Constants.ARG_CHAT_ID, chatId);
        b.putInt(Constants.ARG_GROUP_ID, groupId);
        fr.setArguments(b);
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        ft.replace(R.id.container, fr, Constants.SHARED_MEDIA_FRAGMENT);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    public void openPhotoFragment(SharedImageModel model)
    {
        if(model.isVideo())
            return;
        ((TelegramMainActivity)getActivity()).enableViewPhotoMode(model.getOriginal().getPhoto(), "", (int) (model.getDate() / 1000));
        /*ViewPhotoFragment fr = new ViewPhotoFragment();
        fr.setOriginal(model.getOriginal());
        fr.setPreview(model.getPreview());
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction tr = fm.beginTransaction();
        tr.replace(R.id.container, fr, Constants.VIEW_PHOTO_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();*/
    }
    public static class CameraFileReceiver extends PauseHandler
    {
        private WeakReference<GroupChatProfileFragment> fragmentRef;
        public CameraFileReceiver(GroupChatProfileFragment c)
        {
            this.fragmentRef = new WeakReference<GroupChatProfileFragment>(c);
        }

        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(fragmentRef.get() == null)
                return;
            Uri capturedImageURI = (Uri) message.obj;
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    capturedImageURI);
            fragmentRef.get().startActivityForResult(takePictureIntent, TelegramAction.ATTACH_TAKE_A_PHOTO);
        }
    }
    private void initUpdateProcessor()
    {
        if(updateChatProcessor == null)
        {
            if(getArguments() == null)
                return;
            final long chatId = getArguments().getLong(Constants.ARG_CHAT_ID);
            this.updateChatProcessor = new UpdateChatProcessor() {
                @Override
                protected String getKeyByUpdateConstructor(int constructor) {
                    return String.format("group%d_%d_%d_profile_observer", chatId, groupId, constructor);
                }

                @Override
                protected boolean shouldObserveUpdateGroup() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateTitle() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateReplyMarkup() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateReadInbox() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateReadOutbox() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateChat() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateChatPhoto() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateChatOrder() {
                    return false;
                }

                @Override
                protected void processUpdateInBackground(TdApi.Update upd) {
                }

                @Override
                protected void processUpdateInUiThread(TdApi.Update upd) {
                    try
                    {
                        if(upd.getConstructor() == TdApi.UpdateChatTitle.CONSTRUCTOR && ((TdApi.UpdateChatTitle) upd).chatId == chatId)
                            actionView.chatView.updateTitle(((TdApi.UpdateChatTitle) upd).title);
                        else if(upd.getConstructor() == TdApi.UpdateChatPhoto.CONSTRUCTOR && ((TdApi.UpdateChatPhoto) upd).chatId == chatId)
                            actionView.chatView.updatePhoto(CommonTools.isLowDPIScreen() ? ((TdApi.UpdateChatPhoto) upd).photo.small : ((TdApi.UpdateChatPhoto) upd).photo.big);
                        else if(upd.getConstructor() == TdApi.UpdateGroup.CONSTRUCTOR && ((TdApi.UpdateGroup) upd).group.id == groupId)
                        {
                            TdApi.UpdateGroup cast = (TdApi.UpdateGroup) upd;
                            invalidateActionButton();
                        }
                    }
                    catch(Exception ignored) {}
                }
            };
            TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
            if(service != null) {
                updateChatProcessor.attachObservers(service);
            }
        }
    }
}
