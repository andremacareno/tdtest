package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SimpleActionViewController;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 03/04/15.
 */
public class EditUsernameSettingsFragment extends BaseFragment {
    private enum UsernameCheckStatus {CHECKING, SHORT, TAKEN, AVAILABLE, INVALID};
    private volatile UsernameCheckStatus checkStatus = UsernameCheckStatus.CHECKING;
    private final String actionViewTag = "EditUNameActionView";
    private EditText username;
    private TextView status;
    private TextView help;
    public static Handler checkUsernameHandler = new Handler(Looper.getMainLooper());
    private Client.ResultHandler statusCheckResultHandler;
    private ActionViewController actionViewController;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.statusCheckResultHandler = new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                try
                {
                    if(object instanceof TdApi.Error && !((TdApi.Error) object).text.equals("USERNAME_INVALID")) {
                        checkStatus = UsernameCheckStatus.AVAILABLE;
                    }
                    else {
                        if(object instanceof TdApi.Error)
                            checkStatus = UsernameCheckStatus.INVALID;
                        else
                            checkStatus = UsernameCheckStatus.TAKEN;
                        help.post(new Runnable() {
                            @Override
                            public void run() {
                                updateStatus();
                            }
                        });
                    }
                    help.post(new Runnable() {
                        @Override
                        public void run() {
                            updateStatus();
                        }
                    });
                }
                catch(Exception ignored) {}
            }
        };
    }
    @Override
    public void onResume()
    {
        super.onResume();
    }
    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new SimpleActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new EditNameSettingsActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.edit_username, container, false);
        username = (EditText) v.findViewById(R.id.first_name);
        status = (TextView) v.findViewById(R.id.username_check_status);
        help = (TextView) v.findViewById(R.id.help);
        username.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    if (username.length() > 0)
                        performEditName();
                    return true;
                }
                return false;
            }
        });
        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0 || s.toString().equals(getArguments().getString(Constants.ARG_USERNAME, "")))
                    status.setVisibility(View.GONE);
                else {
                    checkUsernameHandler.removeCallbacksAndMessages(null);
                    if (s.length() < 5) {
                        checkStatus = UsernameCheckStatus.SHORT;
                        updateStatus();
                        return;
                    }
                    checkStatus = UsernameCheckStatus.CHECKING;
                    updateStatus();
                    checkUsernameHandler.postDelayed(new CheckUsernameRequest(s.toString()), 600);
                }
            }
        });
        username.setText(getArguments().getString(Constants.ARG_USERNAME));
        help.setText(Html.fromHtml(getResources().getString(R.string.change_username_help)));
        return v;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    private void hideKeyboard()
    {
        if(AndroidUtilities.isKeyboardShown(username))
            AndroidUtilities.hideKeyboard(username);
    }
    private void performEditName()
    {
        if(checkStatus != UsernameCheckStatus.AVAILABLE && username.length() > 0)
            return;
        hideKeyboard();
        TdApi.ChangeUsername editNameReq = new TdApi.ChangeUsername(username.getText().toString());
        TG.getClientInstance().send(editNameReq, TelegramApplication.dummyResultHandler);
        goBack();
    }

    @Override
    protected void onPasscodeLockShown()
    {
        hideKeyboard();
    }

    private class EditNameSettingsActionViewController extends SimpleActionViewController {
        private View.OnClickListener doneButtonListener;

        @Override
        public boolean havePopup() {
            return false;
        }

        @Override
        public boolean showBackButton() {
            return true;
        }

        @Override
        public boolean showDoneButton() {
            return true;
        }

        @Override
        public String getTitle() {
            return getResources().getString(R.string.username);
        }

        @Override
        public View.OnClickListener getBackButtonListener() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    hideKeyboard();
                    goBack();
                }
            };
        }

        @Override
        public View.OnClickListener getDoneButtonListener() {
            if(this.doneButtonListener == null)
                this.doneButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try
                        {
                            if(username.length() > 0 || !getArguments().getString(Constants.ARG_USERNAME).isEmpty())
                                performEditName();
                        }
                        catch(Exception ignored) {}
                    }
                };
            return this.doneButtonListener;
        }

        @Override
        public int getMenuResource() {
            return 0;
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
    private void updateStatus()
    {
        if(checkStatus == UsernameCheckStatus.CHECKING)
        {
            status.setText(R.string.checking_username);
            status.setTextAppearance(status.getContext(), R.style.ChangeUsernameAvailability_Default);
        }
        else if(checkStatus == UsernameCheckStatus.AVAILABLE)
        {
            status.setText(String.format(getResources().getString(R.string.username_is_available), username.getText().toString()));
            status.setTextAppearance(status.getContext(), R.style.ChangeUsernameAvailability_Available);
        }
        else if(checkStatus == UsernameCheckStatus.TAKEN)
        {
            status.setText(R.string.username_is_taken);
            status.setTextAppearance(status.getContext(), R.style.ChangeUsernameAvailability_Error);
        }
        else if(checkStatus == UsernameCheckStatus.SHORT)
        {
            status.setText(R.string.too_short_username);
            status.setTextAppearance(status.getContext(), R.style.ChangeUsernameAvailability_Error);
        }
        else if(checkStatus == UsernameCheckStatus.INVALID)
        {
            status.setText(R.string.invalid_username);
            status.setTextAppearance(status.getContext(), R.style.ChangeUsernameAvailability_Error);
        }
        status.setVisibility(View.VISIBLE);
    }
    private class CheckUsernameRequest implements Runnable
    {
        private TdApi.SearchUser searchRequest;
        private TdApi.SearchChannel searchChannelReq;
        public CheckUsernameRequest(String username)
        {
            searchRequest = new TdApi.SearchUser(username);
            searchChannelReq = new TdApi.SearchChannel(username);
        }
        @Override
        public void run() {
            TG.getClientInstance().send(searchRequest, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    try
                    {
                        if(object instanceof TdApi.Error && !((TdApi.Error) object).text.equals("USERNAME_INVALID")) {
                            checkStatus = UsernameCheckStatus.AVAILABLE;
                            TG.getClientInstance().send(searchChannelReq, statusCheckResultHandler);
                        }
                        else {
                            if(object instanceof TdApi.Error)
                                checkStatus = UsernameCheckStatus.INVALID;
                            else
                                checkStatus = UsernameCheckStatus.TAKEN;
                            help.post(new Runnable() {
                                @Override
                                public void run() {
                                    updateStatus();
                                }
                            });
                        }
                    }
                    catch(Exception ignored) {}
                }
            });
        }
    }
}
