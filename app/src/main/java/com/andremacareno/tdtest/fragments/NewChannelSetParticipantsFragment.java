package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 13/04/15.
 */
public class NewChannelSetParticipantsFragment extends NewGroupFragment {
    @Override
    public void goNext()
    {
        if(getArguments() == null)
            return;
        long chatId = getArguments().getLong(Constants.ARG_CHAT_ID);
        final int channelId = getArguments().getInt(Constants.ARG_CHANNEL_ID);
        int[] participants = listController.getParticipants();
        if(participants == null || participants.length == 0)
            return;
        TdApi.AddChatParticipants addParticipants = new TdApi.AddChatParticipants(chatId, participants);
        TG.getClientInstance().send(addParticipants, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                final TdApi.Chat chat = ChatCache.getInstance().getChatByChannelId(channelId);
                TelegramApplication.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        continueLoading(chat);
                    }
                });
            }
        });
    }
    private void continueLoading(TdApi.Chat chat)
    {
        TdApi.Channel channel = ((TdApi.ChannelChatInfo) chat.type).channel;
        Fragment fr = new ChannelChatFragment();
        Bundle b = new Bundle();
        b.putLong(Constants.ARG_CHAT_ID, chat.id);
        b.putInt(Constants.ARG_CHANNEL_ID, channel.id);
        b.putInt(Constants.ARG_LAST_READ_INBOX, chat.lastReadInboxMessageId);
        b.putInt(Constants.ARG_LAST_READ_OUTBOX, chat.lastReadOutboxMessageId);
        b.putInt(Constants.ARG_UNREAD_COUNT, chat.unreadCount);
        b.putInt(Constants.ARG_START_MESSAGE, chat.topMessage.id);
        b.putString(Constants.ARG_CHAT_TITLE, chat.title);
        b.putInt(Constants.ARG_CHAT_PEER, channel.id);
        fr.setArguments(b);
        String tag = Constants.CHAT_FRAGMENT;
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction tr = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        tr.replace(R.id.container, fr, tag).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
        ((TelegramMainActivity) getActivity()).clearTillMainFragmentNextTime();
    }
}
