package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.auth.AuthStateProcessor;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.SubmitPasswordController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SimpleActionViewController;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 03/04/15.
 */
public class EnterPasswordFragment extends BaseFragment {
    private final String actionViewTag = "EnterCodeActionView";
    private ActionViewController actionViewController;
    private TextView helpTextView;
    private EditText passwordEditText;
    private TextView forgotPasswordTextView;
    SubmitPasswordController submitController;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new SimpleActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new EnterPasswordActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.enter_password, container, false);
        helpTextView = (TextView) v.findViewById(R.id.twostep_title);
        passwordEditText = (EditText) v.findViewById(R.id.passwordEditText);
        forgotPasswordTextView = (TextView) v.findViewById(R.id.forgot_password);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    if (submitController != null) {
                        hideKeyboard();
                        submitController.didSubmitPasswordButtonClicked();
                    }
                    return true;
                }
                return false;
            }
        });
        return v;
    }
    private void hideKeyboard()
    {
        if(AndroidUtilities.isKeyboardShown(passwordEditText))
            AndroidUtilities.hideKeyboard(passwordEditText);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String hint = getArguments().getString(Constants.ARG_PASSWORD_HINT, "");
        boolean haveRecoveryEmail = getArguments().getBoolean(Constants.ARG_HAS_RECOVERY_EMAIL, false);
        passwordEditText.setHint(hint);
        if(haveRecoveryEmail) {
            forgotPasswordTextView.setVisibility(View.VISIBLE);
            forgotPasswordTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AuthStateProcessor.getInstance().changeAuthState(new TdApi.RequestAuthPasswordRecovery());
                }
            });
        }
        else
            forgotPasswordTextView.setVisibility(View.GONE);
        if(submitController == null)
            submitController = new SubmitPasswordController(passwordEditText, helpTextView);
        else
            submitController.rebindToViews(passwordEditText, helpTextView);
        ((TelegramMainActivity) getActivity()).setSecondaryAuthDelegate(submitController.getSecondaryAuthDelegate());
    }
    private class EnterPasswordActionViewController extends SimpleActionViewController {
        private View.OnClickListener backButtonListener;

        @Override
        public boolean havePopup() {
            return false;
        }

        @Override
        public boolean showBackButton() {
            return true;
        }

        @Override
        public boolean showDoneButton() {
            return false;
        }

        @Override
        public String getTitle() {
            return getResources().getString(R.string.password);
        }

        @Override
        public View.OnClickListener getBackButtonListener() {
            if(this.backButtonListener == null)
                this.backButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goBack();
                    }
                };
            return this.backButtonListener;
        }
        @Override
        public View.OnClickListener getDoneButtonListener() {
            return null;
        }

        @Override
        public int getMenuResource() {
            return 0;
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
}
