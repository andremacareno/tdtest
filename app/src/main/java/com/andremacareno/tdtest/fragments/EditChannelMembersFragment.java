package com.andremacareno.tdtest.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.ChannelCache;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.UserCache;
import com.andremacareno.tdtest.models.ChatParticipantModel;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.viewcontrollers.ChannelAdminsListController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SimpleActionViewController;
import com.andremacareno.tdtest.views.ChatParticipantsListView;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 03/04/15.
 */
public class EditChannelMembersFragment extends BaseFragment {
    public interface ChannelAdminsDelegate
    {
        public boolean isCreator();
        public boolean hasModeratorRights();
        public void openAdminProfile(int id);
        public void didRemoveAlertRequested(int id);
    }
    private final String actionViewTag = "EditGroupAdminsAV";
    private long chatId;
    private int channelId;
    private NotificationObserver newMemberObserver;

    private View addMemberView;

    private View addAdminsView;
    private View.OnClickListener addMemberClick;
    private ActionViewController actionViewController;
    private ChatParticipantsListView participantsListView;
    private ChannelAdminsListController listController;
    private ChannelAdminsDelegate delegate;
    private TdApi.ChannelFull channelInfo;
    private boolean inviteAdmin;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getArguments() == null)
            return;
        inviteAdmin = getArguments().getBoolean(Constants.ARG_ADD_CHANNEL_ADMIN, false);
        newMemberObserver = new NotificationObserver(NotificationCenter.didNewChannelMemberRequested) {
            @Override
            public void didNotificationReceived(Notification notification) {
                try
                {
                    final int userId = (Integer) notification.getObject();
                    TdApi.AddChatParticipant addParticipant = new TdApi.AddChatParticipant(chatId, userId, 0);
                    TG.getClientInstance().send(addParticipant, new Client.ResultHandler() {
                        @Override
                        public void onResult(TdApi.TLObject object) {
                            TdApi.ChangeChatParticipantRole changeRole = new TdApi.ChangeChatParticipantRole(chatId, userId, inviteAdmin ? Constants.PARTICIPANT_EDITOR : Constants.PARTICIPANT_GENERAL);
                            TG.getClientInstance().send(changeRole, new Client.ResultHandler() {
                                @Override
                                public void onResult(TdApi.TLObject object) {
                                    TelegramApplication.applicationHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            UserModel u = UserCache.getInstance().getUserById(userId);
                                            ChatParticipantModel admin = new ChatParticipantModel(u, inviteAdmin ? Constants.PARTICIPANT_EDITOR : Constants.PARTICIPANT_GENERAL);
                                            if(listController != null)
                                                listController.onNewAdmin(admin);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
                finally
                {
                    NotificationCenter.getInstance().removeObserver(this);
                }
            }
        };
        this.chatId = getArguments().getLong(Constants.ARG_CHAT_ID);
        this.channelId = getArguments().getInt(Constants.ARG_CHANNEL_ID);
        channelInfo = ChannelCache.getInstance().getChannelFull(channelId);
        delegate = new ChannelAdminsDelegate() {
            @Override
            public boolean isCreator() {
                return channelInfo.channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor();
            }
            @Override
            public boolean hasModeratorRights()
            {
                return channelInfo.channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() || channelInfo.channel.role.getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor() || channelInfo.channel.role.getConstructor() == Constants.PARTICIPANT_MODERATOR.getConstructor();
            }

            @Override
            public void openAdminProfile(int id) {
                Bundle args = new Bundle();
                args.putLong(Constants.ARG_CHAT_ID, chatId);
                args.putInt(Constants.ARG_USER_ID, id);
                args.putLong(Constants.ARG_CHAT_REFERER, 0);
                ProfileFragment fr = new ProfileFragment();
                fr.setArguments(args);
                fr.setRetainInstance(true);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction tr = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
                tr.replace(R.id.container, fr, Constants.VIEW_PROFILE_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
                fm.executePendingTransactions();
            }

            @Override
            public void didRemoveAlertRequested(final int id) {
                AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
                adb.setItems(new CharSequence[]{getResources().getString(R.string.remove)}, new AlertDialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0) {
                            TdApi.ChangeChatParticipantRole kickRequest = new TdApi.ChangeChatParticipantRole(chatId, id, inviteAdmin ? Constants.PARTICIPANT_GENERAL : Constants.PARTICIPANT_KICKED);
                            TG.getClientInstance().send(kickRequest, new Client.ResultHandler() {
                                @Override
                                public void onResult(TdApi.TLObject object) {
                                    TelegramApplication.applicationHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            if(listController != null)
                                                listController.onAdminRemove(id);
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
                adb.create().show();
            }
        };
        addMemberClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(inviteAdmin)
                    selectAdmin();
                else
                    addMember();
            }
        };
    }
    @Override
    public void onResume()
    {
        super.onResume();
        if(listController == null) {
            listController = new ChannelAdminsListController(participantsListView, channelId, delegate, inviteAdmin);
            participantsListView.setController(listController);
        }
        else
        {
            participantsListView.setController(listController);
            listController.replaceView(participantsListView);
        }
    }
    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new SimpleActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new EditChatNameActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }
    private void selectAdmin()
    {
        NotificationCenter.getInstance().addObserver(newMemberObserver);
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_CHAT_ID, chatId);
        args.putBoolean(Constants.ARG_INVITE_CHANNEL_ADMIN, true);
        if(listController != null)
            args.putIntArray(Constants.ARG_IGNORE_IDS, listController.getParticipantIds());
        AddMemberFragment fr = new AddMemberFragment();
        fr.setRetainInstance(true);
        fr.setArguments(args);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction tr = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        tr.replace(R.id.container, fr, Constants.ADD_MEMBER_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void addMember()
    {
        NotificationCenter.getInstance().addObserver(newMemberObserver);
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_CHAT_ID, chatId);
        args.putBoolean(Constants.ARG_INVITE_CHANNEL_MEMBER, true);
        if(listController != null)
            args.putIntArray(Constants.ARG_IGNORE_IDS, listController.getParticipantIds());
        AddMemberFragment fr = new AddMemberFragment();
        fr.setRetainInstance(true);
        fr.setArguments(args);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction tr = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        tr.replace(R.id.container, fr, Constants.ADD_MEMBER_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.channel_members_settings, container, false);
        participantsListView = (ChatParticipantsListView) v.findViewById(R.id.participants_list);
        TextView help = (TextView) v.findViewById(R.id.channel_admins_help);
        addAdminsView = v.findViewById(R.id.add_admins_button);
        addMemberView = v.findViewById(R.id.add_member_button);
        if(inviteAdmin)
        {
            addMemberView.setVisibility(View.GONE);
            if(delegate != null && delegate.isCreator())
            {
                help.setVisibility(View.VISIBLE);
                addAdminsView.setVisibility(View.VISIBLE);
                addAdminsView.setOnClickListener(addMemberClick);
            }
            else
            {
                help.setVisibility(View.GONE);
                addAdminsView.setVisibility(View.GONE);
            }
        }
        else
        {
            help.setVisibility(View.GONE);
            addAdminsView.setVisibility(View.GONE);
            if(delegate != null && delegate.isCreator())
            {
                addMemberView.setVisibility(View.VISIBLE);
                addMemberView.setOnClickListener(addMemberClick);
            }
            else
            {
                addMemberView.setVisibility(View.GONE);
            }
        }
        return v;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private class EditChatNameActionViewController extends SimpleActionViewController {
        private View.OnClickListener doneButtonListener;

        @Override
        public boolean havePopup() {
            return false;
        }

        @Override
        public boolean showBackButton() {
            return true;
        }

        @Override
        public boolean showDoneButton() {
            return false;
        }

        @Override
        public String getTitle() {
            return inviteAdmin ? getResources().getString(R.string.administrators) : getResources().getString(R.string.members);
        }

        @Override
        public View.OnClickListener getBackButtonListener() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goBack();
                }
            };
        }

        @Override
        public View.OnClickListener getDoneButtonListener() {
            return null;
        }

        @Override
        public int getMenuResource() {
            return 0;
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
}
