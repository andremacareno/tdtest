package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.CreateChannelChooseTypeActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 15/04/15.
 */
public class NewChannelChooseTypeFragment extends BaseFragment {
    private final String TAG = "ChannelTypeFragment";
    private CreateChannelChooseTypeActionView actionView;
    private NewChannelChooseTypeActionViewController actionViewController;
    private ActionButtonDelegate actionButtonDelegate;

    private View choosePublicContainer;
    private View choosePrivateContainer;
    private View inviteLinkContainer;
    private AppCompatRadioButton publicChannelRadioButton;
    private AppCompatRadioButton privateChannelRadioButton;
    private TextView constantInviteLink;
    private EditText dynamicInviteLink;
    private TextView inviteLinkHelp;
    private String privateChannelInviteLink;
    private Boolean makePrivate = null;
    private View.OnClickListener makePublicClickListener, makePrivateClickListener;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        privateChannelInviteLink = getArguments() == null ? "" : getArguments().getString(Constants.ARG_CHAT_INVITE_LINK, "");
        makePublicClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(publicChannelRadioButton == null)
                    return;
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "make public");
                if(makePrivate == null)
                    makePrivate = false;
                privateChannelRadioButton.setChecked(false);
                publicChannelRadioButton.setChecked(true);
                constantInviteLink.setTextColor(ContextCompat.getColor(getContext(), R.color.settings_help));
                constantInviteLink.setText(Constants.TELEGRAM_PREFIX);
                dynamicInviteLink.setVisibility(View.VISIBLE);
                inviteLinkHelp.setText(R.string.public_channel_link_help);
            }
        };
        makePrivateClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(publicChannelRadioButton == null)
                    return;
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "make private");
                if(makePrivate == null)
                    makePrivate = true;
                publicChannelRadioButton.setChecked(false);
                privateChannelRadioButton.setChecked(true);
                constantInviteLink.setTextColor(ContextCompat.getColor(getContext(), R.color.chat_text));
                constantInviteLink.setText(privateChannelInviteLink);
                dynamicInviteLink.setVisibility(View.GONE);
                inviteLinkHelp.setText(R.string.private_channel_link_help);
            }
        };
    }
    @Override
    public void onResume()
    {
        super.onResume();
    }
    @Override
    public void onPause()
    {
        super.onPause();
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        if(actionButtonDelegate == null)
            actionButtonDelegate = new ActionButtonDelegate() {
                @Override
                public void onActionButtonClick() {
                    inviteParticipants();
                }

                @Override
                public int getActionButtonImageResource() {
                    return R.drawable.ic_arrow_forward_gray;
                }
            };
        return actionButtonDelegate;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.channel_choose_type, container, false);
        choosePublicContainer = v.findViewById(R.id.public_channel_block);
        choosePrivateContainer = v.findViewById(R.id.private_channel_block);
        inviteLinkContainer = v.findViewById(R.id.invite_link_block);

        publicChannelRadioButton = (AppCompatRadioButton) choosePublicContainer.findViewById(R.id.public_channel_checkbox);
        privateChannelRadioButton = (AppCompatRadioButton) choosePrivateContainer.findViewById(R.id.private_channel_checkbox);

        constantInviteLink = (TextView) inviteLinkContainer.findViewById(R.id.constant_invite_link);
        dynamicInviteLink = (EditText) inviteLinkContainer.findViewById(R.id.dynamic_invite_link);
        inviteLinkHelp = (TextView) inviteLinkContainer.findViewById(R.id.invite_link_help);
        publicChannelRadioButton.setOnClickListener(makePublicClickListener);
        privateChannelRadioButton.setOnClickListener(makePrivateClickListener);
        View publicLabel = choosePublicContainer.findViewById(R.id.public_channel_label);
        View privateLabel = choosePrivateContainer.findViewById(R.id.private_channel_label);

        publicLabel.setOnClickListener(makePublicClickListener);
        privateLabel.setOnClickListener(makePrivateClickListener);
        return v;
    }
    @Override
    public ActionView getActionView() {
        if(actionView == null)
            actionView = new CreateChannelChooseTypeActionView(getActivity());
        return actionView;
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new NewChannelChooseTypeActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }

    private void inviteParticipants()
    {
        if(getArguments() == null)
            return;
        if(makePrivate == null)
            return;
        if(!makePrivate)
        {
            final int channelId = getArguments().getInt(Constants.ARG_CHANNEL_ID, 0);
            final String username = dynamicInviteLink.getText().toString();
            if(!CommonTools.checkUsername(username))
            {
                Toast.makeText(getContext(), R.string.invalid_channel_link, Toast.LENGTH_SHORT).show();
                return;
            }
            TdApi.SearchChannel srch = new TdApi.SearchChannel(username);
            TG.getClientInstance().send(srch, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    if(object != null && object.getConstructor() == TdApi.Channel.CONSTRUCTOR)
                    {
                        TelegramApplication.applicationHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getContext(), R.string.channel_username_taken, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    else
                    {
                        TdApi.ChangeChannelUsername changeUsername = new TdApi.ChangeChannelUsername(channelId, username);
                        TG.getClientInstance().send(changeUsername, new Client.ResultHandler() {
                            @Override
                            public void onResult(TdApi.TLObject object) {
                                if(object != null && object.getConstructor() == TdApi.Channel.CONSTRUCTOR)
                                    TelegramApplication.applicationHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            continueInviteParticipants();
                                        }
                                    });
                            }
                        });
                    }
                }
            });
        }
        else
            continueInviteParticipants();
    }
    private void continueInviteParticipants()
    {
        hideKeyboard();
        Fragment fr = new NewChannelSetParticipantsFragment();
        Bundle b = new Bundle();
        b.putLong(Constants.ARG_CHAT_ID, getArguments() != null ? getArguments().getLong(Constants.ARG_CHAT_ID, 0) : 0);
        b.putInt(Constants.ARG_CHANNEL_ID, getArguments() != null ? getArguments().getInt(Constants.ARG_CHANNEL_ID, 0) : 0);
        fr.setArguments(b);
        String tag = Constants.CHOOSE_CHANNEL_PARTICIPANTS_FRAGMENT;
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.container, fr, tag).addToBackStack(null).commitAllowingStateLoss();
        ((TelegramMainActivity) getActivity()).clearTillMainFragmentNextTime();
        fm.executePendingTransactions();
    }
    private void hideKeyboard()
    {
        if(actionView == null)
            return;
        if(AndroidUtilities.isKeyboardShown(dynamicInviteLink))
            AndroidUtilities.hideKeyboard(dynamicInviteLink);
    }
    @Override
    protected void onPasscodeLockShown()
    {
        hideKeyboard();
    }
    private class NewChannelChooseTypeActionViewController extends ActionViewController {
        public NewChannelChooseTypeActionViewController()
        {
            super();
        }

        @Override
        public void onFragmentPaused() {

        }

        @Override
        public void onFragmentResumed() {
            ActionView av = getActionView();
            if(av == null || !(av instanceof CreateChannelChooseTypeActionView))
                return;
            CreateChannelChooseTypeActionView cast = (CreateChannelChooseTypeActionView) av;
            if(getArguments() == null)
                return;
            cast.channelView.bindChannel(getArguments().getInt(Constants.ARG_CHANNEL_ID, 0));
            cast.setBackButtonClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goBack();
                }
            });
        }

        @Override
        public void onActionViewRemoved() {

        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
}
