package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.models.Country;
import com.andremacareno.tdtest.tasks.CreateCountryListTask;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.utils.phoneformat.PhoneFormat;
import com.andremacareno.tdtest.viewcontrollers.SubmitPhoneButtonController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SimpleActionViewController;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * Created by andremacareno on 03/04/15.
 */
public class EnterPhoneFragment extends BaseFragment {
    private final String actionViewTag = "EnterPhoneActionView";
    private PauseHandler countryChooseHandler;
    private ArrayList<Country> countries = new ArrayList<Country>();
    private TIntObjectHashMap<Country> countryIndex = new TIntObjectHashMap<>();
    private NotificationObserver countryChooseListener;
    private EditText countryCode;
    private EditText phoneNumber;
    private TextView chooseCountryTextView;
    private TextView wrongPhoneTextView;
    private String chosenCountry;
    private boolean launchTaskOnce = false;
    private SubmitPhoneButtonController submitPhoneButtonController;
    private volatile PauseHandler countriesProcessedHandler;
    private boolean requestUnlockSetPhone = false;
    private ActionViewController actionViewController;
    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new SimpleActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new EnterPhoneActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        countryChooseHandler = new CountryChooseReceiver(this);
        countryChooseListener = new NotificationObserver(NotificationCenter.didCountrySelected) {
            @Override
            public void didNotificationReceived(Notification notification) {
                Country selectedCountry = (Country) notification.getObject();
                chosenCountry = selectedCountry.getFullName();
                Message msg = Message.obtain(countryChooseHandler);
                msg.obj = selectedCountry;
                msg.sendToTarget();
            }
        };
        NotificationCenter.getInstance().addObserver(countryChooseListener);
    }
    @Override
    public void onResume()
    {
        super.onResume();
        countryChooseHandler.resume();
    }
    @Override
    public void onPause()
    {
        super.onPause();
        countryChooseHandler.pause();
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(countryChooseListener != null)
            NotificationCenter.getInstance().removeObserver(countryChooseListener);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.auth_fragment, container, false);
        wrongPhoneTextView = (TextView) v.findViewById(R.id.wrong_phone);
        chooseCountryTextView = (TextView) v.findViewById(R.id.choose_country);
        countryCode = (EditText) v.findViewById(R.id.countryCodeEditText);
        countryCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 0) {
                    chooseCountryTextView.setText(getResources().getString(R.string.choose_a_country));
                    return;
                }
                if(s.length() > 0 && s.charAt(0) == '0')
                {
                    chooseCountryTextView.setText(getResources().getString(R.string.wrong_country));
                    return;
                }
                try {
                    String text = PhoneFormat.stripExceptNumbers(s.toString());
                    int code = Integer.parseInt(text);
                    Country c = countryIndex.get(code);
                    if(c != null)
                        chooseCountryTextView.setText(c.getFullName());
                    else
                        chooseCountryTextView.setText(getResources().getString(R.string.wrong_country));
                }
                catch(Exception e) {}
            }
        });
        phoneNumber = (EditText) v.findViewById(R.id.phoneNumberEditText);
        phoneNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    if(submitPhoneButtonController != null) {
                        hideKeyboard();
                        submitPhoneButtonController.didSetPhoneButtonClicked();
                    }
                    return true;
                }
                return false;
            }
        });
        chooseCountryTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                launchChooseCountryFragment();
            }
        });
        return v;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        countriesProcessedHandler = new CountriesHandler(this);
        if(launchTaskOnce == false)
        {
            launchTaskOnce = true;
            CreateCountryListTask t = new CreateCountryListTask(countries, countryIndex);
            t.onComplete(new OnTaskCompleteListener() {
                @Override
                public void taskComplete(AsyncTaskManTask task) {
                    Message msg = Message.obtain(countriesProcessedHandler);
                    msg.sendToTarget();
                }
            });
            t.addToQueue();
        }
        else
            init();
    }
    private void init()
    {
        if(chosenCountry == null)
            chooseCountryTextView.setText(R.string.choose_a_country);
        else
            chooseCountryTextView.setText(chosenCountry);
        if(submitPhoneButtonController == null)
            submitPhoneButtonController = new SubmitPhoneButtonController(countryCode, phoneNumber, wrongPhoneTextView);
        else {
            if(requestUnlockSetPhone) {
                submitPhoneButtonController.unlockSetPhone();
                requestUnlockSetPhone = false;
            }
            submitPhoneButtonController.rebindToViews(countryCode, phoneNumber, wrongPhoneTextView);
        }
        ((TelegramMainActivity) getActivity()).setSecondaryAuthDelegate(submitPhoneButtonController.getSecondaryAuthDelegate());
    }
    private void launchChooseCountryFragment()
    {
        TelegramMainActivity activity = (TelegramMainActivity) getActivity();
        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction tr = activity.prepareAnimatedTransaction();
        ChooseCountryFragment fr = new ChooseCountryFragment();
        fr.setDataReference(countries);
        fr.setRetainInstance(true);
        tr.replace(R.id.container, fr, Constants.CHOOSE_COUNTRY_FRAGMENT);
        tr.addToBackStack(null);
        tr.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private static class CountriesHandler extends PauseHandler
    {
        private WeakReference<EnterPhoneFragment> fragmentRef;
        public CountriesHandler(EnterPhoneFragment fr)
        {
            this.fragmentRef = new WeakReference<EnterPhoneFragment>(fr);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(fragmentRef.get() != null)
                fragmentRef.get().init();
        }
    }
    private void hideKeyboard()
    {
        if(AndroidUtilities.isKeyboardShown(countryCode))
            AndroidUtilities.hideKeyboard(countryCode);
        else if(AndroidUtilities.isKeyboardShown(phoneNumber))
            AndroidUtilities.hideKeyboard(phoneNumber);
    }
    public void unlockSetPhoneOnResume() { this.requestUnlockSetPhone = true; }
    private class EnterPhoneActionViewController extends SimpleActionViewController {
        private View.OnClickListener doneButtonListener;

        @Override
        public boolean havePopup() {
            return false;
        }

        @Override
        public boolean showBackButton() {
            return false;
        }

        @Override
        public boolean showDoneButton() {
            return true;
        }

        @Override
        public String getTitle() {
            return getResources().getString(R.string.phone_number);
        }

        @Override
        public View.OnClickListener getBackButtonListener() {
            return null;
        }

        @Override
        public View.OnClickListener getDoneButtonListener() {
            if(this.doneButtonListener == null)
                this.doneButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(submitPhoneButtonController != null) {
                            hideKeyboard();
                            submitPhoneButtonController.didSetPhoneButtonClicked();
                        }
                    }
                };
            return this.doneButtonListener;
        }

        @Override
        public int getMenuResource() {
            return 0;
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }

    private static class CountryChooseReceiver extends PauseHandler
    {
        private WeakReference<EnterPhoneFragment> fragmentRef;
        public CountryChooseReceiver(EnterPhoneFragment fragment)
        {
            this.fragmentRef = new WeakReference<EnterPhoneFragment>(fragment);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(fragmentRef.get() == null)
                return;
            Country c = (Country) message.obj;
            if(c == null)
                return;
            if(fragmentRef.get().chooseCountryTextView != null)
                fragmentRef.get().chooseCountryTextView.setText(c.getFullName());
            if(fragmentRef.get().countryCode != null)
                fragmentRef.get().countryCode.setText(c.getCode());
        }
    }
}
