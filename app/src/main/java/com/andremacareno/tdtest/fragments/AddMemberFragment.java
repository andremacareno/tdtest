package com.andremacareno.tdtest.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.viewcontrollers.AddChannelAdminListViewController;
import com.andremacareno.tdtest.viewcontrollers.AddMemberListViewController;
import com.andremacareno.tdtest.viewcontrollers.ContactsListViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SimpleActionViewController;
import com.andremacareno.tdtest.views.ContactsListView;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

/**
 * Created by Andrew on 11.04.2015.
 */
public class AddMemberFragment extends BaseFragment {
    private final String actionViewTag = "AddMemberActionView";
    private ContactsListView roster;
    private ContactsListViewController listController;
    private ActionViewController actionViewController;
    private String inviteLink = null;
    private int[] ignoreIds;
    private boolean addChannelMember;
    private boolean addChannelAdmin;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addChannelMember = getArguments() != null ? getArguments().getBoolean(Constants.ARG_INVITE_CHANNEL_MEMBER, false) : false;
        addChannelAdmin = getArguments() != null ? getArguments().getBoolean(Constants.ARG_INVITE_CHANNEL_ADMIN, false) : false;
        if(getArguments() != null) {
            if(getArguments().containsKey(Constants.ARG_CHAT_INVITE_LINK))
                inviteLink = getArguments().getString(Constants.ARG_CHAT_INVITE_LINK);
            if(getArguments().containsKey(Constants.ARG_IGNORE_IDS))
                ignoreIds = getArguments().getIntArray(Constants.ARG_IGNORE_IDS);
        }
    }
    @Override
    public void onResume()
    {
        super.onResume();
        if(listController != null) {
            if(addChannelAdmin)
                ((AddChannelAdminListViewController)listController).setActivityRef(getActivity());
            else
                ((AddMemberListViewController)listController).setActivityRef(getActivity());
            listController.onFragmentResumed();
        }
    }
    @Override
    public void onPause()
    {
        super.onPause();
        if(listController != null)
            listController.onFragmentPaused();
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new SimpleActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new ContactsActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        if(listController == null) {
            if(BuildConfig.DEBUG)
                Log.d("AddMember", String.format("addAdmin = %s; addMember = %s", Boolean.toString(addChannelAdmin), Boolean.toString(addChannelMember)));
            if(!addChannelAdmin)
            {
                listController = new AddMemberListViewController(roster, ignoreIds, addChannelMember);
                ((AddMemberListViewController)listController).setActivityRef(getActivity());
                ((AddMemberListViewController)listController).setChatId(getArguments().getLong(Constants.ARG_CHAT_ID));
                ((AddMemberListViewController)listController).setAddMemberCallback(new Runnable() {
                    @Override
                    public void run() {
                        goBack();
                    }
                });
            }
            else
            {
                listController = new AddChannelAdminListViewController(roster, ignoreIds);
                ((AddChannelAdminListViewController)listController).setActivityRef(getActivity());
            }
            roster.setController(listController);
        }
        else
        {
            roster.setController(listController);
            listController.replaceView(roster);
        }
        listController.onUpdateServiceConnected(service);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        roster = new ContactsListView(inflater.getContext());
        roster.setLayoutParams(new ViewGroup.LayoutParams(container.getLayoutParams()));
        roster.setVisibility(View.VISIBLE);
        return roster;
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        listController.removeObservers();
    }

    private class ContactsActionViewController extends SimpleActionViewController {
        private View.OnClickListener backButtonListener;
        private PopupMenu.OnMenuItemClickListener menuButtonListener;
        @Override
        public boolean havePopup() {
            if(inviteLink == null || inviteLink.length() == 0)
                return false;
            return true;
        }

        @Override
        public boolean showBackButton() {
            return true;
        }

        @Override
        public boolean showDoneButton() {
            return false;
        }

        @Override
        public String getTitle() {
            return getResources().getString(R.string.select_contact);
        }

        @Override
        public View.OnClickListener getDoneButtonListener() {
            return null;
        }

        @Override
        public int getMenuResource() {
            if(inviteLink == null || inviteLink.length() == 0)
                return 0;
            return R.menu.add_member;
        }

        @Override
        public View.OnClickListener getBackButtonListener() {
            if(this.backButtonListener == null)
                this.backButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goBack();
                    }
                };
            return this.backButtonListener;
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        }
        @Override
        public PopupMenu.OnMenuItemClickListener getMenuItemClickListener() {
            if(inviteLink == null || inviteLink.length() == 0)
                return null;
            if(this.menuButtonListener == null)
                this.menuButtonListener = new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if(menuItem.getItemId() == R.id.action_copy_link)
                        {
                            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData clip = ClipData.newPlainText("telegramMessage", inviteLink);
                            clipboard.setPrimaryClip(clip);
                            Toast.makeText(getActivity(), R.string.link_copied_to_clipboard, Toast.LENGTH_SHORT).show();
                            return true;
                        }
                        return false;
                    }
                };
            return this.menuButtonListener;
        }
    }
}
