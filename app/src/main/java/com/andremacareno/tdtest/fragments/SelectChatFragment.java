package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.models.AdditionalActionInviteToChat;
import com.andremacareno.tdtest.models.AdditionalSendAction;
import com.andremacareno.tdtest.models.ChatModel;
import com.andremacareno.tdtest.models.GlobalSearchResult;
import com.andremacareno.tdtest.models.GroupChatModel;
import com.andremacareno.tdtest.viewcontrollers.ConversationsListViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SimpleActionViewController;
import com.andremacareno.tdtest.views.ConversationsListView;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 11.04.2015.
 */
public class SelectChatFragment extends BaseFragment {
    private final String actionViewTag = "SelectChatActionView";
    private ConversationsListView roster;
    private ConversationsListViewController listController;
    private ActionViewController actionViewController;
    private boolean dontRestore = false;
    private Runnable selectionCallback = null;
    private AdditionalSendAction boundAction = null;
    private RosterFragment.RosterFragmentDelegate delegate;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        delegate = new RosterFragment.RosterFragmentDelegate() {
            @Override
            public void didChatSelected(ChatModel chat) {
                launchChatFragment(chat);
            }

            @Override
            public void didSearchResultSelected(GlobalSearchResult result) {

            }

            @Override
            public void notifySearchResultsCount(int count) {

            }
        };
    }
    @Override
    public void onResume()
    {
        super.onResume();
        if(dontRestore)
            return;
        if(listController != null)
            listController.onFragmentResumed();

    }
    @Override
    public void onPause()
    {
        super.onPause();
        if(dontRestore)
            return;
        if(listController != null)
            listController.onFragmentPaused();
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new SimpleActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }
    @Override
    public ActionViewController getActionViewController()
    {
        if(actionViewController == null)
            actionViewController = new SelectChatActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        if(listController == null) {
            if(boundAction != null)
                listController = new ConversationsListViewController(roster, true, boundAction.getDataType() == AdditionalSendAction.DataType.INVITE_TO_CHAT, boundAction.getDataType() == AdditionalSendAction.DataType.FWD);
        }
        else
        {
            roster.setController(listController);
            listController.replaceView(roster);
        }
        if(listController != null)
        {
            listController.onUpdateServiceConnected(service);
            listController.setFragmentDelegate(delegate);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        roster = new ConversationsListView(inflater.getContext());
        roster.setLayoutParams(new ViewGroup.LayoutParams(container.getLayoutParams()));
        roster.setVisibility(View.VISIBLE);
        return roster;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        didFragmentBecameUseless();
    }
    private void didFragmentBecameUseless()
    {
        if(dontRestore)
            return;
        dontRestore = true;
        if(listController != null)
            listController.removeObservers();
    }
    private void launchChatFragment(ChatModel model)
    {
        if(selectionCallback != null)
            selectionCallback.run();
        if(boundAction != null && boundAction.getDataType() == AdditionalSendAction.DataType.INVITE_TO_CHAT)
            TG.getClientInstance().send(new TdApi.AddChatParticipant(model.getId(), ((AdditionalActionInviteToChat) boundAction).getUserId(), 50), TelegramApplication.dummyResultHandler);
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_CHAT_ID, model.getId());
        args.putInt(Constants.ARG_REPLY_MARKUP_MSG, model.getReplyMarkupMsgId());
        args.putInt(Constants.ARG_CHAT_PEER, model.getPeer());
        args.putInt(Constants.ARG_LAST_READ_INBOX, model.getLastReadInboxMessageId());
        args.putInt(Constants.ARG_LAST_READ_OUTBOX, model.getLastReadOutboxMessageId());
        args.putInt(Constants.ARG_UNREAD_COUNT, model.getUnreadCount());
        args.putInt(Constants.ARG_START_MESSAGE, model.getTopMessageId());
        args.putString(Constants.ARG_CHAT_TITLE, model.getDisplayName());
        args.putBoolean(Constants.ARG_CLEAR_BACK_STACK_ON_EXIT, true);
        BaseChatFragment fr;
        if(!model.isGroupChat() && !model.isChannel()) {
            fr = new PrivateChatFragment();
            args.putInt(Constants.ARG_USER_ID, model.getPeer());
        }
        else if(!model.isChannel())
        {
            args.putInt(Constants.ARG_GROUP_ID, model.getPeer());
            args.putBoolean(Constants.ARG_CHAT_LEFT, ((GroupChatModel) model).isChatLeft());
            fr = new GroupChatFragment();
        }
        else
        {
            args.putInt(Constants.ARG_CHANNEL_ID, model.getPeer());
            fr = new ChannelChatFragment();
        }
        fr.bindAdditionalSendAction(this.boundAction);
        fr.setRetainInstance(true);
        fr.setArguments(args);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        ft.replace(R.id.container, fr, Constants.SELECTED_CHAT_FRAGMENT);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
        fm.executePendingTransactions();
        didFragmentBecameUseless();
    }
    public void setSelectionCallback(Runnable r)
    {
        this.selectionCallback = r;
    }
    public void bindAction(AdditionalSendAction act)
    {
        this.boundAction = act;
    }
    private class SelectChatActionViewController extends SimpleActionViewController {
        private View.OnClickListener backButtonListener;

        @Override
        public boolean havePopup() {
            return false;
        }

        @Override
        public boolean showBackButton() {
            return true;
        }

        @Override
        public boolean showDoneButton() {
            return false;
        }

        @Override
        public String getTitle() {
            return boundAction == null || boundAction.getDataType() != AdditionalSendAction.DataType.INVITE_TO_CHAT ? getResources().getString(R.string.select_chat) : getResources().getString(R.string.select_group);
        }

        @Override
        public View.OnClickListener getDoneButtonListener() {
            return null;
        }

        @Override
        public int getMenuResource() {
            return 0;
        }

        @Override
        public View.OnClickListener getBackButtonListener() {
            if(this.backButtonListener == null)
                this.backButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goBack();
                    }
                };
            return this.backButtonListener;
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
}
