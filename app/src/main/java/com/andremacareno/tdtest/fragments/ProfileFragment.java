package com.andremacareno.tdtest.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.Toast;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.UserCache;
import com.andremacareno.tdtest.models.AdditionalActionInviteToChat;
import com.andremacareno.tdtest.models.AdditionalActionSendContact;
import com.andremacareno.tdtest.models.AdditionalSendAction;
import com.andremacareno.tdtest.models.CurrentUserModel;
import com.andremacareno.tdtest.models.SharedImageModel;
import com.andremacareno.tdtest.models.SharedMedia;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.updatelisteners.UpdateUserProcessor;
import com.andremacareno.tdtest.viewcontrollers.SharedMediaLightListViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ProfileActionViewController;
import com.andremacareno.tdtest.views.SharedMediaButton;
import com.andremacareno.tdtest.views.UserDetailsView;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.ProfileActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 09.05.2015.
 */
public class ProfileFragment extends BaseFragment {
    private SharedMediaLightListViewController.SharedMediaLightListDelegate mediaListDelegate;
    private ProfileActionViewController actionViewController;
    private SharedMediaLightListViewController previewListController;
    private UserModel userData;
    private UserDetailsView userDetailsView;
    private View addBotToGroupButton;
    private SharedMediaButton sharedMediaView;
    private ActionButtonDelegate actionButtonDelegate;
    private ScrollView container;
    private AdditionalSendAction contactData = null;
    private AdditionalSendAction botData = null;
    private View.OnClickListener botButtonClickListener = null;
    private UpdateUserProcessor updateUserProcessor;
    private UserDetailsView.UserDetailsDelegate userDetailsDelegate;
    @Override
    public void onPause()
    {
        super.onPause();
        if(previewListController != null)
            previewListController.onFragmentPaused();
        if(updateUserProcessor != null)
            updateUserProcessor.performPauseObservers();
    }
    @Override
    public void onResume()
    {
        super.onResume();
        if(previewListController != null)
            previewListController.onFragmentResumed();
        if(updateUserProcessor != null)
            updateUserProcessor.performResumeObservers();
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(updateUserProcessor != null)
            updateUserProcessor.removeObservers();
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getArguments() == null || !getArguments().containsKey(Constants.ARG_USER_ID))
            return;
        userData = UserCache.getInstance().getFullUserById(getArguments().getInt(Constants.ARG_USER_ID));
        if(userData == null)
            return;
        userDetailsDelegate = new UserDetailsView.UserDetailsDelegate() {
            @Override
            public void didUsernameViewClicked() {
                if(userData == null || userData.getUsername().isEmpty())
                    return;
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("telegramUsername", String.format("http://telegram.me/%s", userData.getUsername()));
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getActivity(), R.string.chat_link_copied_to_clipboard, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void didPhoneViewClicked() {

            }
        };
        if(userData.isMyContact() || userData.isPhoneNumberKnown())
            contactData = new AdditionalActionSendContact(new TdApi.InputMessageContact(userData.getPhoneNumber(), userData.getFirstName(), userData.getLastName(), userData.getId()));
        if(userData.isBot())
            botData = new AdditionalActionInviteToChat(userData.getId());
        botButtonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addBotToGroup();
            }
        };
        mediaListDelegate = new SharedMediaLightListViewController.SharedMediaLightListDelegate() {
            @Override
            public void didMediaCountReceived(int count) {
                if(sharedMediaView != null) {
                    sharedMediaView.setMediaCount(count);
                }
            }

            @Override
            public void didItemClicked(SharedMedia item) {
                openPhotoFragment((SharedImageModel) item);
            }
        };
    }

    @Override
    public ActionView getActionView()
    {
        ProfileActionView v = new ProfileActionView(getActivity());
        v.setBackButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        return v;
    }
    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null) {
            if(userData == null)
                return null;
            if(getArguments() != null && getArguments().containsKey(Constants.ARG_CHAT_ID))
                actionViewController = new ProfileActionViewController(userData, getArguments().getLong(Constants.ARG_CHAT_ID));
            else
                actionViewController = new ProfileActionViewController(userData);
            initUpdateProcessor();
        }
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        //((TelegramMainActivity) getActivity()).getToolbarController().notifyFragmentChanged(this);
        this.container = (ScrollView) inflater.inflate(R.layout.profile, container, false);
        userDetailsView = (UserDetailsView) this.container.findViewById(R.id.user_details);
        userDetailsView.setDelegate(userDetailsDelegate);
        addBotToGroupButton = this.container.findViewById(R.id.add_bot_button);
        sharedMediaView = (SharedMediaButton) this.container.findViewById(R.id.shared_media_button);
        sharedMediaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSharedMedia();
            }
        });
        if(userData == null || userData.getId() == CurrentUserModel.getInstance().getUserModel().getId())
            sharedMediaView.setVisibility(View.GONE);
        return this.container;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if(userData == null)
            return;
        if(!userData.isBot() || !userData.canBotJoinChats())
            addBotToGroupButton.setVisibility(View.GONE);
        else
            addBotToGroupButton.setOnClickListener(botButtonClickListener);
        userDetailsView.bindUser(userData);
        PopupMenu.OnMenuItemClickListener menuListener = new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if(actionViewController == null)
                    return false;
                if(menuItem.getItemId() == R.id.action_block)
                    actionViewController.blockUser();
                else if(menuItem.getItemId() == R.id.action_unblock)
                    actionViewController.unblockUser();
                else if(menuItem.getItemId() == R.id.action_delete)
                    actionViewController.removeUserFromContacts();
                else if(menuItem.getItemId() == R.id.action_edit || menuItem.getItemId() == R.id.action_add)
                    editContact();
                else if(menuItem.getItemId() == R.id.action_share)
                    shareContact();
                return true;
            }
        };
        if(actionViewController != null)
        {
            actionViewController.setMenuListener(menuListener);
        }
        if(previewListController == null && getArguments() != null && getArguments().containsKey(Constants.ARG_CHAT_ID)) {
            previewListController = new SharedMediaLightListViewController(sharedMediaView.getPreviewList(), getArguments().getLong(Constants.ARG_CHAT_ID));
            previewListController.setListDelegate(mediaListDelegate);
        }
        else if(previewListController != null) {
            previewListController.replaceView(sharedMediaView.getPreviewList());
            previewListController.setListDelegate(mediaListDelegate);
        }
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        if(userData == null || userData.isDeletedAccount() || userData.getId() == CurrentUserModel.getInstance().getUserModel().getId())
            return null;
        if(actionButtonDelegate == null)
            actionButtonDelegate = new ActionButtonDelegate() {
                @Override
                public void onActionButtonClick() {
                    if(getArguments() != null && getArguments().containsKey(Constants.ARG_CHAT_ID) && getArguments().containsKey(Constants.ARG_CHAT_REFERER)) {
                        long fromChat = getArguments().getLong(Constants.ARG_CHAT_REFERER);
                        long thisChat = getArguments().getLong(Constants.ARG_CHAT_ID);
                        if(fromChat != thisChat) {
                            createPrivateChatWithThisUser();
                            return;
                        }
                    }
                    goBack();
                }

                @Override
                public int getActionButtonImageResource() {
                    return R.drawable.ic_message;
                }
            };
        return actionButtonDelegate;
    }
    private void addBotToGroup()
    {
        SelectChatFragment fr = new SelectChatFragment();
        fr.bindAction(botData);
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        ft.replace(R.id.container, fr, Constants.SELECT_CHAT_FRAGMENT);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void editContact()
    {
        EditContactNameFragment fr = new EditContactNameFragment();
        fr.bindUser(userData);
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction tr = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        tr.replace(R.id.container, fr, Constants.EDIT_CONTACT_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void createPrivateChatWithThisUser()
    {
        TdApi.CreatePrivateChat getChatFunc = new TdApi.CreatePrivateChat(userData.getId());
        TG.getClientInstance().send(getChatFunc, new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                if(object.getConstructor() != TdApi.Chat.CONSTRUCTOR)
                    return;
                TelegramApplication.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        loadChatFragment((TdApi.Chat) object);
                    }
                });
            }
        });
    }
    private void loadChatFragment(TdApi.Chat chat)
    {
        TdApi.PrivateChatInfo privateChat = (TdApi.PrivateChatInfo) chat.type;
        Fragment fr;
        Bundle b = new Bundle();
        b.putLong(Constants.ARG_CHAT_ID, chat.id);
        b.putBoolean(Constants.ARG_BOT, userData.isBot());
        b.putInt(Constants.ARG_LAST_READ_INBOX, chat.lastReadInboxMessageId);
        b.putInt(Constants.ARG_LAST_READ_OUTBOX, chat.lastReadOutboxMessageId);
        b.putInt(Constants.ARG_UNREAD_COUNT, chat.unreadCount);
        b.putInt(Constants.ARG_START_MESSAGE, chat.topMessage.id);
        b.putString(Constants.ARG_CHAT_TITLE, String.format("%s %s", privateChat.user.firstName, privateChat.user.lastName));
        b.putInt(Constants.ARG_CHAT_PEER, privateChat.user.id);
        b.putBoolean(Constants.ARG_CLEAR_BACK_STACK_ON_EXIT, true);
        if(chat.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR) {
            fr = new PrivateChatFragment();
            b.putInt(Constants.ARG_USER_ID, ((TdApi.PrivateChatInfo) chat.type).user.id);
        }
        else
        {
            b.putBoolean(Constants.ARG_CHAT_LEFT, ((TdApi.GroupChatInfo) chat.type).group.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor() || ((TdApi.GroupChatInfo) chat.type).group.role.getConstructor() == Constants.PARTICIPANT_KICKED.getConstructor());
            fr = new GroupChatFragment();
        }
        fr.setArguments(b);
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        ft.replace(R.id.container, fr, Constants.CHAT_FRAGMENT);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void openSharedMedia()
    {
        Fragment fr = new SharedMediaFragment();
        Bundle b = new Bundle();
        if(getArguments() == null || !getArguments().containsKey(Constants.ARG_CHAT_ID)) {
            Log.d("ProfileFragment", "missing chat id");
            return;
        }
        b.putLong(Constants.ARG_CHAT_ID, getArguments().getLong(Constants.ARG_CHAT_ID));
        fr.setArguments(b);
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        ft.replace(R.id.container, fr, Constants.SHARED_MEDIA_FRAGMENT);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void shareContact()
    {
        if(contactData == null)
            return;
        SelectChatFragment fr = new SelectChatFragment();
        fr.bindAction(contactData);
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        ft.replace(R.id.container, fr, Constants.SELECT_CHAT_FRAGMENT);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    public void openPhotoFragment(SharedImageModel model)
    {
        if(model.isVideo())
            return;
        ((TelegramMainActivity)getActivity()).enableViewPhotoMode(model.getOriginal().getPhoto(), "", (int) (model.getDate() / 1000));
        /*ViewPhotoFragment fr = new ViewPhotoFragment();
        fr.setOriginal(model.getOriginal());
        fr.setPreview(model.getPreview());
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction tr = fm.beginTransaction();
        tr.replace(R.id.container, fr, Constants.VIEW_PHOTO_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();*/
    }
    private void initUpdateProcessor() {
        if (updateUserProcessor == null) {
            if (getArguments() == null || userData == null)
                return;
            updateUserProcessor = new UpdateUserProcessor() {
                @Override
                protected String getKeyByUpdateConstructor(int constructor) {
                    return String.format("viewprofile_user%d_observer_%d", userData.getId(), constructor);
                }

                @Override
                protected boolean shouldObserveUpdateUserStatus() {
                    return userData.getId() != 333000 && userData.getId() != 777000;
                }

                @Override
                protected boolean shouldObserveUpdateUserBlocked() {
                    if (CurrentUserModel.getInstance() != null && CurrentUserModel.getInstance().getUserModel().getId() == userData.getId())
                        return false;
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateUser() {
                    return userData.getId() != 333000 && userData.getId() != 777000;
                }

                @Override
                protected void processUpdateInBackground(TdApi.Update upd) {
                    try
                    {
                        if (upd.getConstructor() == TdApi.UpdateUserStatus.CONSTRUCTOR && ((TdApi.UpdateUserStatus) upd).userId == userData.getId())
                            userData.updateStatus(((TdApi.UpdateUserStatus) upd).status);
                        else if (upd.getConstructor() == TdApi.UpdateUserBlocked.CONSTRUCTOR && ((TdApi.UpdateUserBlocked) upd).userId == userData.getId()) {
                            userData.updateUserBlocked(((TdApi.UpdateUserBlocked) upd).isBlocked);
                        } else if (upd.getConstructor() == TdApi.UpdateUser.CONSTRUCTOR && ((TdApi.UpdateUser) upd).user.id == userData.getId()) {
                            TdApi.User u = ((TdApi.UpdateUser) upd).user;
                            userData.updateUser(u);
                        }
                    }
                    catch(Exception e) { e.printStackTrace(); }
                }

                @Override
                protected void processUpdateInUiThread(TdApi.Update upd) {
                    try {
                        if (upd.getConstructor() == TdApi.UpdateUserStatus.CONSTRUCTOR && ((TdApi.UpdateUserStatus) upd).userId == userData.getId())
                            actionViewController.updateStatus(((TdApi.UpdateUserStatus) upd).status);
                        else if (upd.getConstructor() == TdApi.UpdateUserBlocked.CONSTRUCTOR && ((TdApi.UpdateUserBlocked) upd).userId == userData.getId())
                            actionViewController.initMenu();
                        else if (upd.getConstructor() == TdApi.UpdateUser.CONSTRUCTOR && ((TdApi.UpdateUser) upd).user.id == userData.getId()) {
                            TdApi.User u = ((TdApi.UpdateUser) upd).user;
                            actionViewController.updateDisplayName(String.format("%s %s", u.firstName, u.lastName));
                            actionViewController.initMenu();
                            actionViewController.updatePhoto(CommonTools.isLowDPIScreen() ? u.profilePhoto.small : u.profilePhoto.big);
                            userDetailsView.updateUserPhone(u.phoneNumber);
                            userDetailsView.updateUsername(u.username);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
            if (service != null) {
                updateUserProcessor.attachObservers(service);
            }
        }
    }
}
