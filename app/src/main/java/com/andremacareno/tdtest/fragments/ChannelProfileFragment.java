package com.andremacareno.tdtest.fragments;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.Toast;

import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.ChannelCache;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.models.CurrentUserModel;
import com.andremacareno.tdtest.models.SharedImageModel;
import com.andremacareno.tdtest.models.SharedMedia;
import com.andremacareno.tdtest.updatelisteners.UpdateChannelProcessor;
import com.andremacareno.tdtest.updatelisteners.UpdateChatProcessor;
import com.andremacareno.tdtest.viewcontrollers.SharedMediaLightListViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ChannelProfileActionViewController;
import com.andremacareno.tdtest.views.ChannelDetailsView;
import com.andremacareno.tdtest.views.ChannelMembersCountButton;
import com.andremacareno.tdtest.views.ChatAdminsCountButton;
import com.andremacareno.tdtest.views.SharedMediaButton;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.ChannelActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;

/**
 * Created by Andrew on 09.05.2015.
 */
public class ChannelProfileFragment extends BaseFragment {
    private SharedMediaLightListViewController.SharedMediaLightListDelegate mediaListDelegate;
    private ChannelProfileActionViewController actionViewController;
    private SharedMediaLightListViewController previewListController;
    private ChannelDetailsView channelDetailsView;
    private SharedMediaButton sharedMediaView;
    private View addMembersButton;
    private ActionButtonDelegate actionButtonDelegate;
    private ChatAdminsCountButton adminsCount;
    private ChannelMembersCountButton membersCount;
    private View adminInfoDivider;
    private ScrollView container;
    private int channelId;
    private NotificationObserver updateDescriptionObserver;
    private View.OnClickListener adminsButtonClick, membersButtonClick;
    private TdApi.ChannelFull channel;
    private UpdateChatProcessor updateChatProcessor;
    private UpdateChannelProcessor updateChannelProcessor;
    private PauseHandler updateDescriptionHandler;
    private ProgressDialog processingDialog;
    private boolean updating = false;
    private ChannelDetailsView.ChannelDetailsDelegate channelDetailsDelegate;
    @Override
    public void onPause()
    {
        super.onPause();
        if(processingDialog.isShowing())
            processingDialog.dismiss();
        if(updateDescriptionHandler != null)
            updateDescriptionHandler.pause();
        if(previewListController != null)
            previewListController.onFragmentPaused();
        if(updateChatProcessor != null)
            updateChatProcessor.performPauseObservers();
        if(updateChannelProcessor != null)
            updateChannelProcessor.performPauseObservers();
    }
    @Override
    public void onResume()
    {
        super.onResume();
        processingDialog= new ProgressDialog(getActivity());
        processingDialog.setMessage(getResources().getString(R.string.updating));
        processingDialog.setIndeterminate(true);
        processingDialog.setCancelable(false);
        if(updating)
            processingDialog.show();
        if(updateDescriptionHandler != null)
            updateDescriptionHandler.resume();
        if(previewListController != null)
            previewListController.onFragmentResumed();
        if(updateChatProcessor != null)
            updateChatProcessor.performResumeObservers();
        if(updateChannelProcessor != null)
            updateChannelProcessor.performResumeObservers();
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getArguments() == null)
            return;
        channelDetailsDelegate = new ChannelDetailsView.ChannelDetailsDelegate() {
            @Override
            public void didChannelLinkClicked() {
                try
                {
                    TdApi.Channel channel = ChannelCache.getInstance().getChannel(getArguments().getInt(Constants.ARG_CHANNEL_ID));
                    if(channel == null || channel.username.isEmpty())
                        return;
                    ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("telegramChannelLink", String.format("http://telegram.me/%s", channel.username));
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(getActivity(), R.string.chat_link_copied_to_clipboard, Toast.LENGTH_SHORT).show();
                }
                catch(Exception e) { e.printStackTrace(); }
            }

            @Override
            public void didChannelDescriptionClicked() {

            }
        };
        updateDescriptionHandler = new ChannelDescriptionHandler(this);
        this.channelId = getArguments().getInt(Constants.ARG_CHANNEL_ID, 0);
        this.channel = ChannelCache.getInstance().getChannelFull(channelId);
        mediaListDelegate = new SharedMediaLightListViewController.SharedMediaLightListDelegate() {
            @Override
            public void didMediaCountReceived(int count) {
                if(sharedMediaView != null) {
                    sharedMediaView.setMediaCount(count);
                }
            }

            @Override
            public void didItemClicked(SharedMedia item) {
                openPhotoFragment((SharedImageModel) item);
            }
        };
        updateDescriptionObserver = new NotificationObserver(NotificationCenter.didChannelDescriptionUpdated) {
            @Override
            public void didNotificationReceived(Notification notification) {
                try
                {
                    String newDescription = (String) notification.getObject();
                    Message msg = Message.obtain(updateDescriptionHandler);
                    msg.obj = newDescription;
                    msg.sendToTarget();
                }
                catch(Exception e) { e.printStackTrace(); }
            }
        };
        NotificationCenter.getInstance().addObserver(updateDescriptionObserver);
        adminsButtonClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAdmins();
            }
        };
        membersButtonClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewChannelMembers();
            }
        };
    }

    @Override
    public ActionView getActionView()
    {
        ChannelActionView v = new ChannelActionView(getActivity());
        v.setBackButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        return v;
    }
    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null) {
            if(channelId == 0)
                return null;
            actionViewController = new ChannelProfileActionViewController(channelId);
            initUpdateProcessor();
        }
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        //((TelegramMainActivity) getActivity()).getToolbarController().notifyFragmentChanged(this);
        this.container = (ScrollView) inflater.inflate(R.layout.channel_profile, container, false);
        addMembersButton = this.container.findViewById(R.id.add_member_button);
        addMembersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMember();
            }
        });
        channelDetailsView = (ChannelDetailsView) this.container.findViewById(R.id.channel_details);
        sharedMediaView = (SharedMediaButton) this.container.findViewById(R.id.shared_media_button);
        sharedMediaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSharedMedia();
            }
        });
        adminsCount = (ChatAdminsCountButton) this.container.findViewById(R.id.chat_admins_count);
        membersCount = (ChannelMembersCountButton) this.container.findViewById(R.id.channel_members_count);
        adminInfoDivider = this.container.findViewById(R.id.channel_admin_info_separator);
        channelDetailsView.setDelegate(channelDetailsDelegate);
        //channel.channel.role.getConstructor() != Constants.PARTICIPANT_ADMIN.getConstructor()
        if(channel.channel.isSupergroup && channel.channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() || channel.channel.role.getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor())
            addMembersButton.setVisibility(View.VISIBLE);
        else
            addMembersButton.setVisibility(View.GONE);
        if(!channel.canGetParticipants)
        {
            adminsCount.setVisibility(View.GONE);
            membersCount.setVisibility(View.GONE);
            adminInfoDivider.setVisibility(View.GONE);
        }
        else
        {
            if(channel.channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() || channel.channel.role.getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor())
            {
                adminsCount.setVisibility(View.VISIBLE);
                adminInfoDivider.setVisibility(View.VISIBLE);
            }
            else
            {
                adminsCount.setVisibility(View.GONE);
                adminInfoDivider.setVisibility(View.GONE);
            }
            membersCount.setVisibility(View.VISIBLE);
            adminsCount.setAdmins(channel.adminsCount);
            membersCount.setMembersCount(channel.participantsCount);
            adminsCount.setOnClickListener(adminsButtonClick);
            membersCount.setOnClickListener(membersButtonClick);
        }
        return this.container;
    }
    private void openSharedMedia()
    {
        Fragment fr = new SharedMediaFragment();
        Bundle b = new Bundle();
        if(getArguments() == null || !getArguments().containsKey(Constants.ARG_CHAT_ID)) {
            Log.d("ProfileFragment", "missing chat id");
            return;
        }
        b.putLong(Constants.ARG_CHAT_ID, getArguments().getLong(Constants.ARG_CHAT_ID));
        fr.setArguments(b);
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        ft.replace(R.id.container, fr, Constants.SHARED_MEDIA_FRAGMENT);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if(channelId == 0)
            return;
        channelDetailsView.bindChannel(channelId);
        PopupMenu.OnMenuItemClickListener menuListener = new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if(actionViewController == null)
                    return false;
                if(menuItem.getItemId() == R.id.action_share)
                    shareChannel();
                else if(menuItem.getItemId() == R.id.action_leave)
                {
                    final boolean isAdmin = channel != null && channel.channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor();
                    int res = isAdmin ? R.string.cannot_be_undone : R.string.srsly_leave_channel;
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.action_confirmation)
                            .setMessage(res)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    leaveChannel();
                                }

                            })
                            .setNegativeButton(R.string.no, null)
                            .setCancelable(true)
                            .show();
                }
                return true;
            }
        };
        if(actionViewController != null)
            actionViewController.setMenuListener(menuListener);
        if(previewListController == null && getArguments() != null && getArguments().containsKey(Constants.ARG_CHAT_ID)) {
            previewListController = new SharedMediaLightListViewController(sharedMediaView.getPreviewList(), getArguments().getLong(Constants.ARG_CHAT_ID));
            previewListController.setListDelegate(mediaListDelegate);
        }
        else if(previewListController != null) {
            previewListController.replaceView(sharedMediaView.getPreviewList());
            previewListController.setListDelegate(mediaListDelegate);
        }
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(updateChatProcessor != null)
            updateChatProcessor.removeObservers();
        if(updateChannelProcessor != null)
            updateChannelProcessor.removeObservers();
        NotificationCenter.getInstance().removeObserver(updateDescriptionObserver);
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        TdApi.Channel channel = ((TdApi.ChannelChatInfo) ChatCache.getInstance().getChatByChannelId(channelId).type).channel;
        if((channel.isSupergroup && (channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() || channel.role.getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor()) || (!channel.isSupergroup && channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor())))
        {
            if(actionButtonDelegate == null)
                actionButtonDelegate = new ActionButtonDelegate() {
                    @Override
                    public void onActionButtonClick() {
                        editChannel();
                    }

                    @Override
                    public int getActionButtonImageResource() {
                        return R.drawable.ic_edit;
                    }
                };
            return actionButtonDelegate;
        }
        else if(channel.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor())
        {
            if(actionButtonDelegate == null)
                actionButtonDelegate = new ActionButtonDelegate() {
                    @Override
                    public void onActionButtonClick() {
                        try
                        {
                            TdApi.AddChatParticipant follow = new TdApi.AddChatParticipant(getArguments().getLong(Constants.ARG_CHAT_ID), CurrentUserModel.getInstance().getUserModel().getId(), 50);
                            TG.getClientInstance().send(follow, TelegramApplication.dummyResultHandler);
                        }
                        catch(Exception ignored) {}
                    }

                    @Override
                    public int getActionButtonImageResource() {
                        return R.drawable.ic_add_gray;
                    }
                };
        }
        else
            return null;
        return actionButtonDelegate;
    }

    protected void addMember()
    {
        if(getArguments() == null || channel == null || !channel.channel.isSupergroup)
            return;
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_CHAT_ID, getArguments().getLong(Constants.ARG_CHAT_ID));
        args.putBoolean(Constants.ARG_INVITE_CHANNEL_MEMBER, true);
        AddMemberFragment fr = new AddMemberFragment();
        fr.setRetainInstance(true);
        fr.setArguments(args);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction tr = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        tr.replace(R.id.container, fr, Constants.ADD_MEMBER_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void addAdmins()
    {
        EditChannelMembersFragment fr = new EditChannelMembersFragment();
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_CHAT_ID, getArguments().getLong(Constants.ARG_CHAT_ID));
        args.putBoolean(Constants.ARG_ADD_CHANNEL_ADMIN, true);
        args.putInt(Constants.ARG_CHANNEL_ID, channelId);
        fr.setArguments(args);
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        ft.replace(R.id.container, fr, Constants.EDIT_ADMINS_FRAGMENT);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void viewChannelMembers()
    {
        EditChannelMembersFragment fr = new EditChannelMembersFragment();
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_CHAT_ID, getArguments().getLong(Constants.ARG_CHAT_ID));
        args.putInt(Constants.ARG_CHANNEL_ID, channelId);
        fr.setArguments(args);
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        ft.replace(R.id.container, fr, Constants.EDIT_ADMINS_FRAGMENT);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void editChannel()
    {
        EditChannelInfoFragment fr = new EditChannelInfoFragment();
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_CHAT_ID, getArguments().getLong(Constants.ARG_CHAT_ID));
        args.putInt(Constants.ARG_CHANNEL_ID, channelId);
        fr.setArguments(args);
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        ft.replace(R.id.container, fr, Constants.EDIT_CHANNEL_FRAGMENT);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void shareChannel()
    {
        if(channel.inviteLink.isEmpty() && channel.channel.role.getConstructor() != Constants.PARTICIPANT_ADMIN.getConstructor())
            return;
        if(!channel.inviteLink.isEmpty())
        {
            try
            {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("telegramChannelLink", channel.inviteLink);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getActivity(), R.string.link_copied_to_clipboard, Toast.LENGTH_SHORT).show();
            }
            catch(Exception e) { e.printStackTrace(); }
        }
        else
        {
            try
            {
                TdApi.ExportChatInviteLink inviteLinkReq = new TdApi.ExportChatInviteLink(getArguments().getLong(Constants.ARG_CHAT_ID));
                TG.getClientInstance().send(inviteLinkReq, new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        if(object == null || object.getConstructor() != TdApi.ChatInviteLink.CONSTRUCTOR)
                            return;
                        final TdApi.ChatInviteLink cast = (TdApi.ChatInviteLink) object;
                        try
                        {
                            TelegramApplication.applicationHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    try
                                    {
                                        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                                        ClipData clip = ClipData.newPlainText("telegramChannelLink", cast.inviteLink);
                                        clipboard.setPrimaryClip(clip);
                                        Toast.makeText(getActivity(), R.string.link_copied_to_clipboard, Toast.LENGTH_SHORT).show();
                                        channel.inviteLink = cast.inviteLink;
                                    }
                                    catch(Exception e) { e.printStackTrace(); }
                                }
                            });
                        }
                        catch(Exception ignored) {}
                    }
                });
            }
            catch(Exception ignored) {}
        }
    }
    public void openPhotoFragment(SharedImageModel model)
    {
        if(model.isVideo())
            return;
        ((TelegramMainActivity)getActivity()).enableViewPhotoMode(model.getOriginal().getPhoto(), "", (int) (model.getDate() / 1000));
    }
    public void leaveChannel()
    {
        try
        {
            final boolean isAdmin = channel != null && channel.channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor();
            updating = true;
            if(processingDialog != null)
                processingDialog.show();
            TdApi.TLFunction function = isAdmin ? new TdApi.DeleteChannel(channelId) : new TdApi.ChangeChatParticipantRole(getArguments().getLong(Constants.ARG_CHAT_ID), CurrentUserModel.getInstance().getUserModel().getId(), Constants.PARTICIPANT_LEFT);
            TG.getClientInstance().send(function, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    try
                    {
                        TelegramApplication.applicationHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                updating = false;
                                if(processingDialog != null)
                                    processingDialog.dismiss();
                                ((TelegramMainActivity) getActivity()).clearTillMainFragmentNextTime();
                                goBack();
                            }
                        });
                    }
                    catch(Exception ignored) {}
                }
            });
        }
        catch(Exception ignored) {}
    }
    private void initUpdateProcessor()
    {
        if(updateChatProcessor == null)
        {
            if(getArguments() == null)
                return;
            final long chatId = getArguments().getLong(Constants.ARG_CHAT_ID);
            this.updateChatProcessor = new UpdateChatProcessor() {
                @Override
                protected String getKeyByUpdateConstructor(int constructor) {
                    return String.format("channel%d_%d_%d_observer", chatId, channelId, constructor);
                }

                @Override
                protected boolean shouldObserveUpdateGroup() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateTitle() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateReplyMarkup() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateReadInbox() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateReadOutbox() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateChat() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateChatPhoto() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateChatOrder() {
                    return false;
                }

                @Override
                protected void processUpdateInBackground(TdApi.Update upd) {
                }

                @Override
                protected void processUpdateInUiThread(TdApi.Update upd) {
                    if(upd.getConstructor() == TdApi.UpdateChatTitle.CONSTRUCTOR && ((TdApi.UpdateChatTitle) upd).chatId == chatId)
                        actionViewController.updateTitle(((TdApi.UpdateChatTitle) upd).title);
                    else if(upd.getConstructor() == TdApi.UpdateChatPhoto.CONSTRUCTOR && ((TdApi.UpdateChatPhoto) upd).chatId == chatId)
                        actionViewController.updatePhoto(CommonTools.isLowDPIScreen() ? ((TdApi.UpdateChatPhoto) upd).photo.small : ((TdApi.UpdateChatPhoto) upd).photo.big);
                }
            };
            this.updateChannelProcessor = new UpdateChannelProcessor() {
                @Override
                protected String getKeyByUpdateConstructor(int constructor) {
                    return String.format("channel%d_profile_observer_%d", channelId, constructor);
                }

                @Override
                protected void processUpdateInBackground(TdApi.Update upd) {

                }

                @Override
                protected void processUpdateInUiThread(TdApi.Update upd) {
                    if(upd == null || upd.getConstructor() != TdApi.UpdateChannel.CONSTRUCTOR)
                        return;
                    TdApi.UpdateChannel cast = (TdApi.UpdateChannel) upd;
                    if(cast.channel.id != channelId)
                        return;
                    channel.channel = cast.channel;
                    channelDetailsView.updateChannelLink(cast.channel.username);
                    invalidateActionButton();
                }
            };
            TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
            if(service != null) {
                updateChatProcessor.attachObservers(service);
                updateChannelProcessor.attachObservers(service);
            }
        }
    }
    static class ChannelDescriptionHandler extends PauseHandler
    {
        protected WeakReference<ChannelProfileFragment> fragmentRef;
        public ChannelDescriptionHandler(ChannelProfileFragment fragment)
        {
            this.fragmentRef = new WeakReference<ChannelProfileFragment>(fragment);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            try
            {
                String newDescr = (String) message.obj;
                fragmentRef.get().channel.about = newDescr;
                fragmentRef.get().channelDetailsView.updateChannelDescription(newDescr);
            }
            catch(Exception ignored) {}
        }
    }
}
