package com.andremacareno.tdtest.fragments;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.passcode.PasscodePasswordController;
import com.andremacareno.tdtest.viewcontrollers.passcode.PasscodeViewController;
import com.andremacareno.tdtest.views.PasscodeView;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.ChangePasscodeActionView;

/**
 * Created by andremacareno on 03/04/15.
 */
public class ChangePasscodeFragment extends BaseFragment {
    private static final String TAG = "ChangePasscodeFragment";
    private final String actionViewTag = "ChangePasscodeActionView";
    private ChangePasscodeActionViewController actionViewController;
    private PasscodeView passcodeView;
    private PasscodeViewController passcodeViewController;
    private PasscodeViewController.PasscodeType passcodeType = PasscodeViewController.PasscodeType.PIN;
    private NotificationObserver passcodeSetObserver;
    private NotificationObserver confirmationModeObserver;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        passcodeSetObserver = new NotificationObserver(NotificationCenter.didPasscodeSet) {
            @Override
            public void didNotificationReceived(Notification notification) {
                goBack();
            }
        };
        confirmationModeObserver = new NotificationObserver(NotificationCenter.didEnteredInConfirmationMode) {
            @Override
            public void didNotificationReceived(Notification notification) {
                actionViewController.didEnteredInConfirmationMode();
            }
        };
        NotificationCenter.getInstance().addObserver(passcodeSetObserver);
        NotificationCenter.getInstance().addObserver(confirmationModeObserver);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
    }
    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        ChangePasscodeActionView v = new ChangePasscodeActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
        v.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        return v;
    }
    @Override
    public ActionViewController getActionViewController()
    {
        if(actionViewController == null)
            actionViewController = new ChangePasscodeActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        passcodeView = new PasscodeView(container.getContext(), passcodeType);
        passcodeView.setLayoutParams(container.getLayoutParams());
        return passcodeView;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if(passcodeViewController == null) {
            if(passcodeView != null && passcodeType != null)
                passcodeViewController = CommonTools.initPasscodeViewController(passcodeView, passcodeType, PasscodeViewController.Mode.SET);
        }
        else
            passcodeViewController.reattachToView(passcodeView);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        NotificationCenter.getInstance().removeObserver(passcodeSetObserver);
        NotificationCenter.getInstance().removeObserver(confirmationModeObserver);
        ((TelegramMainActivity) getActivity()).showActionViewShadow();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
    }
    @Override
    public void onResume()
    {
        super.onResume();
        ((TelegramMainActivity) getActivity()).hideActionViewShadow();
    }
    @Override
    protected void onPasscodeLockShown()
    {
        try
        {
            if(passcodeType == PasscodeViewController.PasscodeType.PASSWORD && (passcodeViewController != null && passcodeViewController instanceof PasscodePasswordController))
            {
                ((PasscodePasswordController) passcodeViewController).hideKeyboard();
            }
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    private class ChangePasscodeActionViewController extends ActionViewController
    {
        private PopupMenu.OnMenuItemClickListener menuItemClickListener;
        @Override
        public void onFragmentPaused() {

        }

        @Override
        public void onFragmentResumed() {
            //TODO menu item click listener
            ChangePasscodeActionView actionView = (ChangePasscodeActionView) getActionView();
            final String passcodeTypeString;
            if(passcodeType == PasscodeViewController.PasscodeType.PIN)
                passcodeTypeString = getResources().getString(R.string.pin);
            else if(passcodeType == PasscodeViewController.PasscodeType.PASSWORD)
                passcodeTypeString = getResources().getString(R.string.password);
            else if(passcodeType == PasscodeViewController.PasscodeType.PATTERN)
                passcodeTypeString = getResources().getString(R.string.pattern);
            else
                passcodeTypeString = getResources().getString(R.string.gesture);
            actionView.passcodeTypeTextView.setText(passcodeTypeString);
            final PopupMenu popup = new PopupMenu(actionView.getContext(), actionView.passcodeTypeMenu);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.passcode_type, popup.getMenu());
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popup.show();
                }
            };
            actionView.passcodeTypeMenu.setOnClickListener(clickListener);
            actionView.passcodeTypeTextView.setOnClickListener(clickListener);
            if(menuItemClickListener == null)
            {
                menuItemClickListener = new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        PasscodeViewController.PasscodeType newPasscodeType;
                        if(menuItem.getItemId() == R.id.action_pin)
                            newPasscodeType = PasscodeViewController.PasscodeType.PIN;
                        else if(menuItem.getItemId() == R.id.action_password)
                            newPasscodeType = PasscodeViewController.PasscodeType.PASSWORD;
                        else if(menuItem.getItemId() == R.id.action_gesture)
                            newPasscodeType = PasscodeViewController.PasscodeType.GESTURE;
                        else
                            newPasscodeType = PasscodeViewController.PasscodeType.PATTERN;
                        if(passcodeType != newPasscodeType)
                        {
                            int delay = 0;
                            if(passcodeType == PasscodeViewController.PasscodeType.PASSWORD && passcodeViewController != null) {
                                ((PasscodePasswordController) passcodeViewController).hideKeyboard();
                                delay = 200;
                            }
                            passcodeType = newPasscodeType;
                            try
                            {
                                TelegramApplication.applicationHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        passcodeView.passcodeTypeChanged(passcodeType);
                                        passcodeViewController = CommonTools.initPasscodeViewController(passcodeView, passcodeType, PasscodeViewController.Mode.SET);
                                        onFragmentResumed();
                                    }
                                }, delay);
                            }
                            catch(Exception e) { e.printStackTrace(); }
                        }
                        return true;
                    }
                };
            }
            popup.setOnMenuItemClickListener(menuItemClickListener);
        }

        @Override
        public void onActionViewRemoved() {

        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
        public void didEnteredInConfirmationMode()
        {
            ChangePasscodeActionView actionView = (ChangePasscodeActionView) getActionView();
            actionView.passcodeTypeTextView.setOnClickListener(null);
            actionView.passcodeTypeMenu.setVisibility(View.GONE);
        }
    }
}
