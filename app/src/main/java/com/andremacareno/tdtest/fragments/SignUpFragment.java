package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.auth.AuthStateProcessor;
import com.andremacareno.tdtest.viewcontrollers.SignUpButtonController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SimpleActionViewController;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

/**
 * Created by andremacareno on 03/04/15.
 */
public class SignUpFragment extends BaseFragment {
    private final String actionViewTag = "SignUpActionView";
    private ActionViewController actionViewController;
    private EditText firstName;
    private EditText lastName;
    private SignUpButtonController signUpController;
    AuthStateProcessor.SecondaryAuthDelegate secondaryDelegate;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        secondaryDelegate = new AuthStateProcessor.SecondaryAuthDelegate() {
            @Override
            public String getPhoneNumber() {
                return getArguments().getString(Constants.ARG_PHONE_NUMBER, "");
            }

            @Override
            public void processError() {
            }
        };
    }
    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new SimpleActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new SignUpActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.signup, container, false);
        firstName = (EditText) v.findViewById(R.id.firstNameEditText);
        lastName = (EditText) v.findViewById(R.id.lastNameEditText);
        return v;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(signUpController == null)
            signUpController = new SignUpButtonController(firstName, lastName);
        else
            signUpController.rebindToViews(firstName, lastName);
        ((TelegramMainActivity) getActivity()).setSecondaryAuthDelegate(secondaryDelegate);
    }
    private class SignUpActionViewController extends SimpleActionViewController {
        private View.OnClickListener backButtonListener;
        private View.OnClickListener doneButtonListener;

        @Override
        public boolean havePopup() {
            return false;
        }

        @Override
        public boolean showBackButton() {
            return true;
        }

        @Override
        public boolean showDoneButton() {
            return true;
        }

        @Override
        public String getTitle() {
            return getResources().getString(R.string.sign_up);
        }

        @Override
        public View.OnClickListener getBackButtonListener() {
            if(this.backButtonListener == null)
                this.backButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goBack();
                    }
                };
            return this.backButtonListener;
        }
        @Override
        public View.OnClickListener getDoneButtonListener() {
            if(this.doneButtonListener == null)
                this.doneButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(signUpController != null)
                            signUpController.didSignUpButtonClicked();
                    }
                };
            return this.doneButtonListener;
        }

        @Override
        public int getMenuResource() {
            return 0;
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
}
