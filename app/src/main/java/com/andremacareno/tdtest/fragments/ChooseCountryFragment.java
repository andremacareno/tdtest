package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.listadapters.CountriesAdapter;
import com.andremacareno.tdtest.models.Country;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SimpleActionViewController;
import com.andremacareno.tdtest.views.BaseListView;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

import java.util.ArrayList;

/**
 * Created by Andrew on 23.04.2015.
 */
public class ChooseCountryFragment extends BaseFragment {
    private final String actionViewTag = "ChooseCountryActionView";
    private ActionViewController actionViewController;
    private CountriesAdapter adapter;
    private ArrayList<Country> data;


    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public ActionView getActionView() {
        return new SimpleActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new ChooseCountryActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.choose_country, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    public void setDataReference(ArrayList<Country> ref)
    {
        this.data = ref;
    }
    private void init()
    {
        BaseListView v = (BaseListView) getView().findViewById(R.id.countries);
        RecyclerView.LayoutManager lm = new LinearLayoutManager(TelegramApplication.sharedApplication().getApplicationContext());
        v.setLayoutManager(lm);
        v.setHasFixedSize(true);
        v.setItemAnimator(new DefaultItemAnimator());
        adapter = new CountriesAdapter(data, new CountriesAdapter.CountryListDelegate() {
            @Override
            public void didCountrySelected(Country country) {
                goBack();
                NotificationCenter.getInstance().postNotification(NotificationCenter.didCountrySelected, country);
            }
        });
        v.setAdapter(adapter);
    }
    private class ChooseCountryActionViewController extends SimpleActionViewController {
        private View.OnClickListener backButtonListener;

        @Override
        public boolean havePopup() {
            return false;
        }

        @Override
        public boolean showBackButton() {
            return true;
        }

        @Override
        public boolean showDoneButton() {
            return false;
        }

        @Override
        public String getTitle() {
            return getResources().getString(R.string.country);
        }

        @Override
        public View.OnClickListener getBackButtonListener() {
            if(this.backButtonListener == null)
                this.backButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goBack();
                    }
                };
            return this.backButtonListener;
        }
        @Override
        public View.OnClickListener getDoneButtonListener() {
            return null;
        }

        @Override
        public int getMenuResource() {
            return 0;
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
}
