package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.viewcontrollers.ContactsListViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SimpleActionViewController;
import com.andremacareno.tdtest.views.ContactsListView;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 11.04.2015.
 */
public class ContactsFragment extends BaseFragment {
    public interface ContactListDelegate
    {
        public void onContactChoose(UserModel contact);
    }
    private final String actionViewTag = "ContactsActionView";
    private ContactsListView roster;
    private ContactsListViewController listController;
    private ContactListDelegate delegate;
    private ActionViewController actionViewController;
    private boolean chooseMode = false;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getArguments() != null)
            chooseMode = getArguments().getBoolean(Constants.ARG_CHOOSE_MODE, false);
        delegate = new ContactListDelegate() {
            @Override
            public void onContactChoose(UserModel contact) {
                if(chooseMode) {
                    NotificationCenter.getInstance().postNotification(NotificationCenter.didContactsListViewItemClicked, contact);
                    goBack();
                }
                else
                {
                    TdApi.CreatePrivateChat getChatFunc = new TdApi.CreatePrivateChat(contact.getId());
                    TG.getClientInstance().send(getChatFunc, new Client.ResultHandler() {
                        @Override
                        public void onResult(final TdApi.TLObject object) {
                            if(object.getConstructor() != TdApi.Chat.CONSTRUCTOR)
                                return;
                            TelegramApplication.applicationHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    loadChatFragment((TdApi.Chat) object);
                                }
                            });
                        }
                    });
                }
            }
        };
    }
    @Override
    public void onResume()
    {
        super.onResume();
        if(listController != null)
            listController.onFragmentResumed();
    }
    @Override
    public void onPause()
    {
        super.onPause();
        if(listController != null)
            listController.onFragmentPaused();
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new SimpleActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new ContactsActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        if(listController == null) {
            listController = new ContactsListViewController(roster);
            listController.setContactListDelegate(delegate);
            roster.setController(listController);
        }
        else
        {
            roster.setController(listController);
            listController.replaceView(roster);
        }
        listController.onUpdateServiceConnected(service);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        roster = new ContactsListView(inflater.getContext());
        roster.setLayoutParams(new ViewGroup.LayoutParams(container.getLayoutParams()));
        roster.setVisibility(View.VISIBLE);
        return roster;
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(listController != null) {
            listController.removeObservers();
            listController = null;
        }
    }
    private void loadChatFragment(TdApi.Chat chat)
    {
        TdApi.PrivateChatInfo privateChat = (TdApi.PrivateChatInfo) chat.type;
        Fragment fr = new PrivateChatFragment();
        Bundle b = new Bundle();
        b.putLong(Constants.ARG_CHAT_ID, chat.id);
        b.putInt(Constants.ARG_USER_ID, privateChat.user.id);
        b.putInt(Constants.ARG_LAST_READ_INBOX, chat.lastReadInboxMessageId);
        b.putInt(Constants.ARG_LAST_READ_OUTBOX, chat.lastReadOutboxMessageId);
        b.putInt(Constants.ARG_UNREAD_COUNT, chat.unreadCount);
        b.putInt(Constants.ARG_START_MESSAGE, chat.topMessage.id);
        b.putString(Constants.ARG_CHAT_TITLE, String.format("%s %s", privateChat.user.firstName, privateChat.user.lastName));
        b.putInt(Constants.ARG_CHAT_PEER, privateChat.user.id);
        b.putBoolean(Constants.ARG_CLEAR_BACK_STACK_ON_EXIT, true);
        fr.setArguments(b);
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        ft.replace(R.id.container, fr, Constants.CHAT_FRAGMENT);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private class ContactsActionViewController extends SimpleActionViewController {
        private View.OnClickListener backButtonListener;

        @Override
        public boolean havePopup() {
            return false;
        }

        @Override
        public boolean showBackButton() {
            return true;
        }

        @Override
        public boolean showDoneButton() {
            return false;
        }

        @Override
        public String getTitle() {
            return getResources().getString(R.string.contacts);
        }

        @Override
        public View.OnClickListener getDoneButtonListener() {
            return null;
        }

        @Override
        public int getMenuResource() {
            return 0;
        }

        @Override
        public View.OnClickListener getBackButtonListener() {
            if(this.backButtonListener == null)
                this.backButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goBack();
                    }
                };
            return this.backButtonListener;
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
}
