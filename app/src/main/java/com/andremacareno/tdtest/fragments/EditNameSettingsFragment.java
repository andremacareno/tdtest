package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SimpleActionViewController;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 03/04/15.
 */
public class EditNameSettingsFragment extends BaseFragment {
    private final String actionViewTag = "EditNameSettingsActionView";
    private EditText firstName;
    private EditText lastName;
    private ActionViewController actionViewController;
    @Override
    public void onResume()
    {
        super.onResume();
    }
    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new SimpleActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new EditNameSettingsActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.edit_name, container, false);
        firstName = (EditText) v.findViewById(R.id.first_name);
        lastName = (EditText) v.findViewById(R.id.last_name);
        lastName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    if(firstName.length() > 0) {
                        hideKeyboard();
                        performEditName();
                    }
                    return true;
                }
                return false;
            }
        });
        firstName.setText(getArguments().getString(Constants.ARG_FIRST_NAME));
        lastName.setText(getArguments().getString(Constants.ARG_LAST_NAME));
        return v;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    private void hideKeyboard()
    {
        if(AndroidUtilities.isKeyboardShown(firstName))
            AndroidUtilities.hideKeyboard(firstName);
        if(AndroidUtilities.isKeyboardShown(lastName))
            AndroidUtilities.hideKeyboard(lastName);
    }
    private void performEditName()
    {
        TdApi.ChangeName editNameReq = new TdApi.ChangeName(firstName.getText().toString(), lastName.getText().toString());
        TG.getClientInstance().send(editNameReq, TelegramApplication.dummyResultHandler);
        goBack();
    }
    @Override
    protected void onPasscodeLockShown()
    {
        hideKeyboard();
    }

    private class EditNameSettingsActionViewController extends SimpleActionViewController {
        private View.OnClickListener doneButtonListener;

        @Override
        public boolean havePopup() {
            return false;
        }

        @Override
        public boolean showBackButton() {
            return true;
        }

        @Override
        public boolean showDoneButton() {
            return true;
        }

        @Override
        public String getTitle() {
            return getResources().getString(R.string.edit_name);
        }

        @Override
        public View.OnClickListener getBackButtonListener() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    hideKeyboard();
                    goBack();
                }
            };
        }

        @Override
        public View.OnClickListener getDoneButtonListener() {
            if(this.doneButtonListener == null)
                this.doneButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(firstName.length() > 0)
                        {
                            hideKeyboard();
                            performEditName();
                        }
                    }
                };
            return this.doneButtonListener;
        }

        @Override
        public int getMenuResource() {
            return 0;
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
}
