package com.andremacareno.tdtest.fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SimpleActionViewController;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

/**
 * Created by andremacareno on 03/04/15.
 */
public class PasscodeSetFragment extends BaseFragment {
    public static final int AUTOLOCK_DISABLED = 0;
    public static final int AUTOLOCK_1M = 1;
    public static final int AUTOLOCK_5M = 2;
    public static final int AUTOLOCK_1H = 3;
    public static final int AUTOLOCK_5H = 4;
    private static final String TAG = "PasscodeSetFragment";
    private final String actionViewTag = "PasscodeSetActionView";
    private SimpleActionViewController actionViewController;
    private View passcodeToggle;
    private View autolockButton;
    private View changePasscodeLink;
    private SwitchCompat passcodeToggleSwitch;
    private TextView changePasscodeTextView;
    private TextView autoLockTimeoutTextView;
    private TextView autoLockHelp;
    private boolean passcodeEnabled;
    private AnimatorSet autoLockViewHide = new AnimatorSet();
    View.OnClickListener goToPasscodeSetScreenListener;
    View.OnClickListener passcodeToggleClickListener;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new SimpleActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }
    @Override
    public ActionViewController getActionViewController()
    {
        if(actionViewController == null)
        {
            actionViewController = new PasscodeActionViewController();
        }
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.settings_passcode, container, false);
        passcodeToggle = v.findViewById(R.id.passcode_toggle);
        autolockButton = v.findViewById(R.id.passcode_autolock_button);
        changePasscodeLink = v.findViewById(R.id.change_passcode);
        passcodeToggleSwitch = (SwitchCompat) passcodeToggle.findViewById(R.id.passcode_lock_switch);
        passcodeToggleSwitch.setClickable(false);
        changePasscodeTextView = (TextView) changePasscodeLink.findViewById(R.id.change_passcode_text_view);
        autoLockTimeoutTextView = (TextView) autolockButton.findViewById(R.id.passcode_autolock_timeout);
        autoLockHelp = (TextView) v.findViewById(R.id.autolock_help);
        autolockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseAutoLockTimeout();
            }
        });
        goToPasscodeSetScreenListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToPasscodeSetScreen();
            }
        };
        passcodeToggleClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(passcodeEnabled)
                {
                    disablePasscode();
                    refreshList(true);
                }
                else
                    goToPasscodeSetScreen();
            }
        };
        refreshList(false);
        return v;
    }
    private void refreshList(boolean animateAutoLock)
    {
        passcodeEnabled = TelegramApplication.sharedApplication().sharedPreferences().getBoolean(Constants.PREFS_PASSCODE_LOCK_ENABLED, false);
        if(passcodeEnabled)
        {
            passcodeToggleSwitch.setChecked(true);
            changePasscodeLink.setClickable(true);
            if(Build.VERSION.SDK_INT >= 23)
                changePasscodeTextView.setTextAppearance(R.style.Settings_PrimaryText);
            else
                changePasscodeTextView.setTextAppearance(getActivity().getApplicationContext(), R.style.Settings_PrimaryText);
            changePasscodeLink.setOnClickListener(goToPasscodeSetScreenListener);
            refreshAutolockButton();
            autoLockHelp.setVisibility(View.VISIBLE);
            autolockButton.setVisibility(View.VISIBLE);
        }
        else
        {
            passcodeToggleSwitch.setChecked(false);
            changePasscodeLink.setClickable(false);
            if(Build.VERSION.SDK_INT >= 23)
                changePasscodeTextView.setTextAppearance(R.style.Settings_PrimaryText_Disabled);
            else
                changePasscodeTextView.setTextAppearance(getActivity().getApplicationContext(), R.style.Settings_PrimaryText_Disabled);
            if(animateAutoLock)
                hideAutoLock();
            else
            {
                autoLockHelp.setVisibility(View.GONE);
                autolockButton.setVisibility(View.GONE);
            }
        }
        passcodeToggle.setOnClickListener(passcodeToggleClickListener);
    }
    private void refreshAutolockButton()
    {
        int autoLockValue = TelegramApplication.sharedApplication().sharedPreferences().getInt(Constants.PREFS_AUTOLOCK_TIMEOUT, AUTOLOCK_DISABLED);
        String autoLockTimeout;
        switch(autoLockValue)
        {
            case AUTOLOCK_1M:
                autoLockTimeout = getResources().getString(R.string.autolock_1m);
                break;
            case AUTOLOCK_5M:
                autoLockTimeout = getResources().getString(R.string.autolock_5m);
                break;
            case AUTOLOCK_1H:
                autoLockTimeout = getResources().getString(R.string.autolock_1h);
                break;
            case AUTOLOCK_5H:
                autoLockTimeout = getResources().getString(R.string.autolock_5h);
                break;
            case AUTOLOCK_DISABLED:
            default:
                autoLockTimeout = getResources().getString(R.string.autolock_disable);
                break;
        }
        autoLockTimeoutTextView.setText(autoLockTimeout);
    }
    private void disablePasscode()
    {
        SharedPreferences prefs = TelegramApplication.sharedApplication().sharedPreferences();
        SharedPreferences.Editor prefEdit = prefs.edit();
        int passcodeType = prefs.getInt(Constants.PREFS_PASSCODE_LOCK_TYPE, Constants.PASSCODE_TYPE_PIN);
        if(passcodeType == Constants.PASSCODE_TYPE_PIN)
            prefEdit.remove(Constants.PREFS_PASSCODE_LOCK_PIN);
        else if(passcodeType == Constants.PASSCODE_TYPE_PASSWORD)
            prefEdit.remove(Constants.PREFS_PASSCODE_LOCK_PASSWORD);
        else if(passcodeType == Constants.PASSCODE_TYPE_PATTERN)
            prefEdit.remove(Constants.PREFS_PASSCODE_LOCK_PATTERN);
        prefEdit.remove(Constants.PREFS_PASSCODE_LOCK_TYPE);
        prefEdit.putBoolean(Constants.PREFS_PASSCODE_LOCK_ENABLED, false);
        prefEdit.putBoolean(Constants.PREFS_LOCK_APP_NEXT_TIME, false);
        prefEdit.apply();
        NotificationCenter.getInstance().postNotification(NotificationCenter.didPasscodeUnset, null);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }
    private void goToPasscodeSetScreen()
    {
        Fragment fr = new ChangePasscodeFragment();
        String tag = Constants.SETTINGS_PASSCODE_SET_FRAGMENT;
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction().replace(R.id.container, fr, tag).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
    @Override
    public void onResume()
    {
        super.onResume();
        refreshList(false);
    }
    private class PasscodeActionViewController extends SimpleActionViewController {
        private View.OnClickListener backButtonListener;

        @Override
        public boolean havePopup() {
            return false;
        }

        @Override
        public boolean showBackButton() {
            return true;
        }

        @Override
        public boolean showDoneButton() {
            return false;
        }

        @Override
        public String getTitle() {
            return getResources().getString(R.string.passcode_lock);
        }

        @Override
        public View.OnClickListener getBackButtonListener() {
            if(this.backButtonListener == null)
                this.backButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goBack();
                    }
                };
            return this.backButtonListener;
        }

        @Override
        public View.OnClickListener getDoneButtonListener() {
            return null;
        }

        @Override
        public int getMenuResource() {
            return 0;
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
    public void chooseAutoLockTimeout()
    {
        int autoLockValue = TelegramApplication.sharedApplication().sharedPreferences().getInt(Constants.PREFS_AUTOLOCK_TIMEOUT, AUTOLOCK_DISABLED);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.auto_lock);
        final NumberPicker np = new NumberPicker(getActivity());
        np.setFocusable(false);
        np.setFocusableInTouchMode(false);
        np.setMinValue(AUTOLOCK_DISABLED);
        np.setMaxValue(AUTOLOCK_5H);
        np.setValue(autoLockValue);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        np.setDisplayedValues(new String[] {getResources().getString(R.string.autolock_disable), getResources().getString(R.string.autolock_1m), getResources().getString(R.string.autolock_5m), getResources().getString(R.string.autolock_1h), getResources().getString(R.string.autolock_5h)});
        builder.setView(np);
        builder.setNegativeButton(R.string.done, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor prefEdit = TelegramApplication.sharedApplication().sharedPreferences().edit();
                prefEdit.putInt(Constants.PREFS_AUTOLOCK_TIMEOUT, np.getValue());
                prefEdit.apply();
                refreshAutolockButton();
            }
        });
        builder.create().show();
    }

    private void hideAutoLock() {
        autoLockViewHide.playTogether(
                ObjectAnimator.ofFloat(autolockButton, View.ALPHA, 1.0f, 0),
                ObjectAnimator.ofFloat(autoLockHelp, View.ALPHA, 1.0f, 0)
        );
        autoLockViewHide.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                autolockButton.setVisibility(View.GONE);
                autoLockHelp.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        autoLockViewHide.setDuration(270);
        autoLockViewHide.setInterpolator(new AccelerateInterpolator(1.8f));
        autoLockViewHide.start();
    }

}
