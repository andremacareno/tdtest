package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SimpleActionViewController;
import com.andremacareno.tdtest.views.AvatarImageView;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 03/04/15.
 */
public class EditContactNameFragment extends BaseFragment {
    private final String actionViewTag = "EditNameSettingsActionView";
    private UserModel boundUser;
    private EditText firstName;
    private EditText lastName;

    private AvatarImageView avatar;
    private TextView phoneNumber;
    private TextView status;
    private boolean dontChangeEditText = false;

    private ActionViewController actionViewController;
    @Override
    public void onResume()
    {
        super.onResume();
    }
    @Override
    public void onPause()
    {
        super.onPause();
        dontChangeEditText = true;
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new SimpleActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new EditContactNameActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.edit_contact_name, container, false);

        avatar = (AvatarImageView) v.findViewById(R.id.avatar);
        phoneNumber = (TextView) v.findViewById(R.id.phone);
        status = (TextView) v.findViewById(R.id.status);

        firstName = (EditText) v.findViewById(R.id.first_name);
        lastName = (EditText) v.findViewById(R.id.last_name);
        lastName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    if(firstName.length() > 0) {
                        hideKeyboard();
                        performImport();
                    }
                    return true;
                }
                return false;
            }
        });

        return v;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        phoneNumber.setText(boundUser.getFormattedPhoneNumber());
        phoneNumber.setTypeface(AndroidUtilities.getTypeface("rmedium.ttf"));
        status.setText(boundUser.getStringStatus());
        TelegramImageLoader.getInstance().loadImage(boundUser.getPhoto(), CommonTools.makeFileKey(boundUser.getPhoto().getFileId()), avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size), false);
        if(!dontChangeEditText)
        {
            firstName.setText(boundUser.getFirstName());
            lastName.setText(boundUser.getLastName());
        }
    }
    private void hideKeyboard()
    {
        if(AndroidUtilities.isKeyboardShown(firstName))
            AndroidUtilities.hideKeyboard(firstName);
        else if(AndroidUtilities.isKeyboardShown(lastName))
            AndroidUtilities.hideKeyboard(lastName);
    }
    private void performImport()
    {
        TdApi.InputContact inputContact = new TdApi.InputContact(boundUser.getPhoneNumber(), firstName.getText().toString(), lastName.getText().toString());
        TdApi.ImportContacts importReq = new TdApi.ImportContacts(new TdApi.InputContact[] {inputContact});
        TG.getClientInstance().send(importReq, TelegramApplication.dummyResultHandler);
        goBack();
    }
    @Override
    protected void onPasscodeLockShown()
    {
        hideKeyboard();
    }
    public void bindUser(UserModel user)
    {
        this.boundUser = user;
    }
    private class EditContactNameActionViewController extends SimpleActionViewController {
        private View.OnClickListener doneButtonListener;

        @Override
        public boolean havePopup() {
            return false;
        }

        @Override
        public boolean showBackButton() {
            return true;
        }

        @Override
        public boolean showDoneButton() {
            return true;
        }

        @Override
        public String getTitle() {
            return getResources().getString(R.string.edit_name);
        }

        @Override
        public View.OnClickListener getBackButtonListener() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    hideKeyboard();
                    goBack();
                }
            };
        }

        @Override
        public View.OnClickListener getDoneButtonListener() {
            if(this.doneButtonListener == null)
                this.doneButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(firstName.length() > 0)
                        {
                            hideKeyboard();
                            performImport();
                        }
                    }
                };
            return this.doneButtonListener;
        }

        @Override
        public int getMenuResource() {
            return 0;
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
}
