package com.andremacareno.tdtest.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.OnTaskFailureListener;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.TelegramAction;
import com.andremacareno.tdtest.tasks.CreateFileForCameraTask;
import com.andremacareno.tdtest.tasks.GalleryImagePathTask;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.NewGroupFinalStageActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 15/04/15.
 */
public class NewChannelFragment extends BaseFragment {
    private final String TAG = "NewChannelFragment";
    private NewGroupFinalStageActionView actionView;
    private EnterTitleActionViewController actionViewController;
    private ActionButtonDelegate actionButtonDelegate;
    private String photo_path;
    private GalleryImagePathTask galleryImagePathTask;
    private EditText channelDescriptionEditText;
    private PauseHandler cameraFileReceiver;
    private ColorFilter infoIconColorFilter;
    private ImageView infoIcon;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        infoIconColorFilter = new PorterDuffColorFilter(ContextCompat.getColor(getContext(), R.color.new_channel_description_focus), PorterDuff.Mode.SRC_ATOP);
        if(cameraFileReceiver == null) {
            cameraFileReceiver = new CameraFileReceiver(this);
            galleryImagePathTask = new GalleryImagePathTask();
        }
    }
    @Override
    public void onResume()
    {
        super.onResume();
        if(cameraFileReceiver != null) {
            cameraFileReceiver.resume();
        }
        if(actionView != null && photo_path != null)
            TelegramImageLoader.getInstance().loadAvatarPreview(photo_path, actionView.profilePhoto);
    }
    @Override
    public void onPause()
    {
        super.onPause();
        if(cameraFileReceiver != null) {
            cameraFileReceiver.pause();
        }
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        if(actionButtonDelegate == null)
            actionButtonDelegate = new ActionButtonDelegate() {
                @Override
                public void onActionButtonClick() {
                    createChat();
                }

                @Override
                public int getActionButtonImageResource() {
                    return R.drawable.ic_arrow_forward_gray;
                }
            };
        return actionButtonDelegate;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.channel_enter_description, container, false);
        channelDescriptionEditText = (EditText) v.findViewById(R.id.channel_description);
        infoIcon = (ImageView) v.findViewById(R.id.info_icon);
        channelDescriptionEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    infoIcon.setColorFilter(infoIconColorFilter);
                }
                else
                    infoIcon.clearColorFilter();
            }
        });
        return v;
    }
    @Override
    public ActionView getActionView() {
        if(actionView == null)
            actionView = new NewGroupFinalStageActionView(getActivity());
        return actionView;
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new EnterTitleActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
    }
    private void createChat()
    {
        if(actionView == null)
            return;
        String title = actionView.titleEditText.getText().toString();
        String about = channelDescriptionEditText.getText().toString();
        if(title.length() <= 0)
            return;
        TdApi.CreateNewChannelChat newChannel = new TdApi.CreateNewChannelChat(title, true, false, about);
        TG.getClientInstance().send(newChannel, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(object == null || object.getConstructor() != TdApi.Chat.CONSTRUCTOR)
                    return;
                final TdApi.Chat chat = (TdApi.Chat) object;
                TdApi.ExportChatInviteLink getInviteLink = new TdApi.ExportChatInviteLink(chat.id);
                TG.getClientInstance().send(getInviteLink, new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        if(object == null || object.getConstructor() != TdApi.ChatInviteLink.CONSTRUCTOR)
                            return;
                        final String inviteLink = ((TdApi.ChatInviteLink) object).inviteLink;
                        if(photo_path != null)
                        {
                            TdApi.ChangeChatPhoto setPhoto = new TdApi.ChangeChatPhoto(chat.id, new TdApi.InputFileLocal(photo_path), null);
                            TG.getClientInstance().send(setPhoto, new Client.ResultHandler() {
                                @Override
                                public void onResult(TdApi.TLObject object) {
                                    if(object != null)
                                        Log.d(TAG, object.toString());
                                    TelegramApplication.applicationHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            goToChooseType(chat, inviteLink);
                                        }
                                    });
                                }
                            });
                        }
                        else
                            TelegramApplication.applicationHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    goToChooseType(chat, inviteLink);
                                }
                            });
                    }
                });
            }
        });
    }
    private void goToChooseType(TdApi.Chat chat, String inviteLink)
    {
        TdApi.Channel channel = ((TdApi.ChannelChatInfo) chat.type).channel;
        Fragment fr = new NewChannelChooseTypeFragment();
        Bundle b = new Bundle();
        b.putLong(Constants.ARG_CHAT_ID, chat.id);
        b.putInt(Constants.ARG_CHANNEL_ID, channel.id);
        b.putString(Constants.ARG_CHAT_INVITE_LINK, inviteLink);
        fr.setArguments(b);
        String tag = Constants.CHOOSE_CHANNEL_TYPE_FRAGMENT;
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.container, fr, tag).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }

    private void hideKeyboard()
    {
        if(actionView == null)
            return;
        if(AndroidUtilities.isKeyboardShown(actionView.titleEditText))
            AndroidUtilities.hideKeyboard(actionView.titleEditText);
        if(AndroidUtilities.isKeyboardShown(channelDescriptionEditText))
            AndroidUtilities.hideKeyboard(channelDescriptionEditText);
    }
    @Override
    protected void onPasscodeLockShown()
    {
        hideKeyboard();
    }
    private void removePhoto()
    {
        photo_path = null;
        if(actionView != null)
            actionView.profilePhoto.setImageDrawable(null);
    }
    private void openGalleryForNewPhoto()
    {
        if (Build.VERSION.SDK_INT <19){
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(galleryIntent, TelegramAction.ATTACH_CHOOSE_FROM_GALLERY);
        } else {
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(galleryIntent, TelegramAction.ATTACH_CHOOSE_FROM_GALLERY);
        }
    }
    private void openCameraForNewPhoto()
    {
        if(!AndroidUtilities.haveCamera())
        {
            Toast.makeText(TelegramApplication.sharedApplication().getApplicationContext(), R.string.no_camera, Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Activity activity = getActivity();
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            CreateFileForCameraTask task = new CreateFileForCameraTask();
            task.onComplete(new OnTaskCompleteListener() {
                @Override
                public void taskComplete(AsyncTaskManTask task) {
                    CreateFileForCameraTask t = (CreateFileForCameraTask) task;
                    photo_path = t.getPhotoPath();
                    Message msg = Message.obtain(cameraFileReceiver);
                    msg.obj = t.getPhotoUri();
                    msg.sendToTarget();
                }
            });
            task.onFailure(new OnTaskFailureListener() {
                @Override
                public void taskFailed(AsyncTaskManTask task) {
                    //Toast.makeText((fragmentRef.get().getActivity()), R.string.error_taking_photo, Toast.LENGTH_SHORT).show();
                }
            });
            task.addToQueue();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == Activity.RESULT_OK)
        {
            if(requestCode == TelegramAction.ATTACH_TAKE_A_PHOTO)
            {
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                File f = new File("file:".concat(photo_path));
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                getActivity().sendBroadcast(mediaScanIntent);
                if(photo_path != null && actionView != null)
                    TelegramImageLoader.getInstance().loadAvatarPreview(photo_path, actionView.profilePhoto);

            }
            else if(requestCode == TelegramAction.ATTACH_CHOOSE_FROM_GALLERY)
            {
                Uri uri = data.getData();
                if(uri == null)
                    Toast.makeText(getActivity(), R.string.error_picking_from_gallery, Toast.LENGTH_LONG).show();
                else
                {
                    if(Build.VERSION.SDK_INT >= 19)
                    {
                        final int takeFlags = data.getFlags()
                                & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        // Check for the freshest data.
                        //noinspection ResourceType
                        TelegramApplication.sharedApplication().getContentResolver().takePersistableUriPermission(uri, takeFlags);
                    }
                    galleryImagePathTask.setLink(uri);
                    galleryImagePathTask.onFailure(new OnTaskFailureListener() {
                        @Override
                        public void taskFailed(AsyncTaskManTask task) {
                            try
                            {
                                TelegramApplication.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), R.string.error_picking_from_gallery, Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            catch(Exception ignored) {}
                        }
                    });
                    galleryImagePathTask.onComplete(new OnTaskCompleteListener() {
                        @Override
                        public void taskComplete(AsyncTaskManTask task) {
                            photo_path = ((GalleryImagePathTask) task).getImagePath();
                            if (photo_path != null) {
                                TelegramApplication.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        TelegramImageLoader.getInstance().loadAvatarPreview(photo_path, actionView.profilePhoto);
                                    }
                                });
                            }
                        }
                    });
                    galleryImagePathTask.addToQueue();
                }

            }
        }
    }
    private void avatarAlert()
    {
        hideKeyboard();
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        CharSequence[] arr = photo_path != null ? new CharSequence[]{getResources().getString(R.string.from_camera), getResources().getString(R.string.from_gallery), getResources().getString(R.string.delete_photo)} : new CharSequence[]{getResources().getString(R.string.from_camera), getResources().getString(R.string.from_gallery)};
        adb.setItems(arr, new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0)
                    openCameraForNewPhoto();
                else if(i == 1)
                    openGalleryForNewPhoto();
                else if(i == 2)
                    removePhoto();
            }
        });
        adb.create().show();
    }
    private class EnterTitleActionViewController extends ActionViewController {
        private TextView.OnEditorActionListener onDoneListener;
        private View.OnClickListener avatarClickListener;
        public EnterTitleActionViewController()
        {
            super();
            init();
        }
        private void init()
        {
            onDoneListener = new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    if(i == EditorInfo.IME_ACTION_NEXT)
                    {
                        if(channelDescriptionEditText != null)
                            channelDescriptionEditText.requestFocus();
                        return true;
                    }
                    return false;
                }
            };
            avatarClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    avatarAlert();
                }
            };
        }
        @Override
        public void onFragmentPaused() {

        }

        @Override
        public void onFragmentResumed() {
            ActionView av = getActionView();
            if(av == null || !(av instanceof NewGroupFinalStageActionView))
                return;
            NewGroupFinalStageActionView cast = (NewGroupFinalStageActionView) av;
            cast.titleEditText.setHint(R.string.channel_name);
            cast.titleEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
            cast.titleEditText.setOnEditorActionListener(onDoneListener);
            cast.profilePhoto.setOnClickListener(avatarClickListener);
            cast.setBackButtonClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goBack();
                }
            });
        }

        @Override
        public void onActionViewRemoved() {

        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
    public static class CameraFileReceiver extends PauseHandler
    {
        private WeakReference<NewChannelFragment> fragmentRef;
        public CameraFileReceiver(NewChannelFragment c)
        {
            this.fragmentRef = new WeakReference<NewChannelFragment>(c);
        }

        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(fragmentRef.get() == null)
                return;
            Uri capturedImageURI = (Uri) message.obj;
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    capturedImageURI);
            fragmentRef.get().startActivityForResult(takePictureIntent, TelegramAction.ATTACH_TAKE_A_PHOTO);
        }
    }
}
