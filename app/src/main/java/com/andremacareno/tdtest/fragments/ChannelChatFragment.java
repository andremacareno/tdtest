package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.ChannelCache;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.updatelisteners.UpdateChannelProcessor;
import com.andremacareno.tdtest.updatelisteners.UpdateChatProcessor;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ChannelChatActionViewController;
import com.andremacareno.tdtest.views.ComposeMessagePlaceholder;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.ChatActionView;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 17/04/15.
 */
public class ChannelChatFragment extends BaseChatFragment {
    private PopupMenu contextMenu = null;
    private ChatActionView actionView = null;
    private ChannelChatActionViewController actionViewController;
    private UpdateChatProcessor updateChatProcessor;
    private UpdateChannelProcessor updateChannelProcessor;
    private int channelId;
    private long chatId;
    private TdApi.Channel channelRef = null;
    //private PauseHandler fillHeaderTaskHandler;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.channelId = getArguments() != null && getArguments().containsKey(Constants.ARG_CHANNEL_ID) ? getArguments().getInt(Constants.ARG_CHANNEL_ID, 0) : 0;
        this.chatId = getArguments() != null && getArguments().containsKey(Constants.ARG_CHAT_ID) ? getArguments().getLong(Constants.ARG_CHAT_ID, 0L) : 0L;
        channelRef = ChannelCache.getInstance().getChannel(channelId);
    }
    @Override
    public ComposeMessagePlaceholder.Reason displayComposeMessageView() {
        if(channelId == 0 || chatId == 0)
            return ComposeMessagePlaceholder.Reason.CHANNEL_NOT_ADMIN;
        TdApi.Chat chat = ChatCache.getInstance().getChatById(chatId);
        if(channelRef != null && chat != null) {
            if(!channelRef.isBroadcast)
                return ComposeMessagePlaceholder.Reason.NO_REASON;
            if(channelRef.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() || channelRef.role.getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor())
                return ComposeMessagePlaceholder.Reason.NO_REASON;
            if(channelRef.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor())
                return ComposeMessagePlaceholder.Reason.CHANNEL_NOT_FOLLOWING;
        }
        return ComposeMessagePlaceholder.Reason.CHANNEL_NOT_ADMIN;
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        if(actionView == null)
        {
            actionView = new ChatActionView(getActivity());
            actionView.setBackButtonClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    closeKeyboard();
                    goBack();
                }
            });
            actionView.setMiddlePartClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (BuildConfig.DEBUG)
                        Log.d("ChannelChatFragment", String.format("channelId = %d", channelId));
                    if (channelId != 0)
                        openChannelProfile(channelId, getArguments().getLong(Constants.ARG_CHAT_ID));
                }
            });
            actionView.setClearSelectionClickListener(clearSelection);
            actionView.setDelButtonClickListener(delSelection);
            actionView.setShareButtonClickListener(fwdSelection);
        }
        return actionView;
    }
    @Override
    protected void disableSelectionMode() {
        if(actionView != null)
            actionView.setSelectionMode(false, true);
    }

    @Override
    protected void onSelectionCountUpdate(int newCount) {
        if(actionView != null && newCount >= 1)
        {
            actionView.changeSelectionCount(newCount);
            actionView.setSelectionMode(true, !selectionMode);
        }
    }
    @Override
    public void onResume()
    {
        super.onResume();
        if(updateChatProcessor != null)
            updateChatProcessor.performResumeObservers();
        if(updateChannelProcessor != null)
            updateChannelProcessor.performResumeObservers();
    }
    @Override
    public void onPause()
    {
        super.onPause();
        if(updateChatProcessor != null)
            updateChatProcessor.performPauseObservers();
        if(updateChannelProcessor != null)
            updateChannelProcessor.performPauseObservers();
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(updateChatProcessor != null)
            updateChatProcessor.removeObservers();
        if(updateChannelProcessor != null)
            updateChannelProcessor.removeObservers();
        ChannelCache.getInstance().removeChannelFull(channelId);
    }
    @Override
    protected void changeDeleteIconVisibilityInSelectionBar(boolean canDelete) {
        if(actionView != null)
            actionView.setDelButtonVisibility(canDelete);
    }
    @Override
    protected void enableSearchMode()
    {
        if(actionView == null)
            return;
        searchMode = true;
        actionView.setDelegate(chatSearchDelegate);
        actionView.setSearchMode(true);
        ((TelegramMainActivity) getActivity()).interceptBackPress(searchModeBackPress);
    }
    @Override
    protected void disableSearchMode()
    {
        if(actionView == null)
            return;
        searchMode = false;
        actionView.setSearchMode(false);
        ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(searchModeBackPress);
    }
    @Override
    protected void searchHashtag(String hashtag) {
        enableSearchMode();
        actionView.searchEditText.setText(hashtag);
        if(hashtag.length() > 1)
            chatSearchDelegate.didQuerySubmit(hashtag);
    }
    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null) {
            actionViewController = new ChannelChatActionViewController();
            actionViewController.bindChatInfo(chatId);
            initUpdateProcessor();
            updateComposeMessageViewVisibility();
        }
        return actionViewController;
    }
    private void initUpdateProcessor()
    {
        if(updateChatProcessor == null)
        {
            this.updateChatProcessor = new UpdateChatProcessor() {
                private final String updateTitle = String.format(Constants.CHATSCREEN_UPDATE_TITLE_OBSERVER, chatId);
                private final String updatePhoto = String.format(Constants.CHATSCREEN_UPDATE_PHOTO_OBSERVER, chatId);
                @Override
                protected String getKeyByUpdateConstructor(int constructor) {
                    if(constructor == TdApi.UpdateChatPhoto.CONSTRUCTOR)
                        return updatePhoto;
                    return updateTitle;
                }

                @Override
                protected boolean shouldObserveUpdateGroup() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateTitle() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateReplyMarkup() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateReadInbox() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateReadOutbox() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateChat() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateChatPhoto() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateChatOrder() {
                    return false;
                }

                @Override
                protected void processUpdateInBackground(TdApi.Update upd) {
                }

                @Override
                protected void processUpdateInUiThread(TdApi.Update upd) {
                    if(upd.getConstructor() == TdApi.UpdateChatTitle.CONSTRUCTOR && ((TdApi.UpdateChatTitle) upd).chatId == chatId)
                        actionViewController.updateTitle(((TdApi.UpdateChatTitle) upd).title);
                    else if(upd.getConstructor() == TdApi.UpdateChatPhoto.CONSTRUCTOR && ((TdApi.UpdateChatPhoto) upd).chatId == chatId)
                        actionViewController.updatePhoto(CommonTools.isLowDPIScreen() ? ((TdApi.UpdateChatPhoto) upd).photo.small : ((TdApi.UpdateChatPhoto) upd).photo.big);
                }
            };
            this.updateChannelProcessor = new UpdateChannelProcessor() {
                @Override
                protected String getKeyByUpdateConstructor(int constructor) {
                    return String.format(Constants.CHANNEL_CHAT_UPDATE_CHANNEL_OBSERVER, channelId);
                }

                @Override
                protected void processUpdateInBackground(TdApi.Update upd) {

                }

                @Override
                protected void processUpdateInUiThread(TdApi.Update upd) {
                    if(upd == null || upd.getConstructor() != TdApi.UpdateChannel.CONSTRUCTOR)
                        return;
                    TdApi.UpdateChannel cast = (TdApi.UpdateChannel) upd;
                    if(cast.channel.id != channelId)
                        return;
                    channelRef = cast.channel;
                    updateComposeMessageViewVisibility();
                }
            };
            TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
            if(service != null) {
                updateChatProcessor.attachObservers(service);
                updateChannelProcessor.attachObservers(service);
            }
        }
    }

    @Override
    protected void initBotCmdListController() {
    }

    @Override
    protected boolean isPrivateChat() {
        return false;
    }


    @Override
    protected boolean shouldSendTyping() {
        return false;
    }
    @Override
    protected void updateContextMenu()
    {
        if(actionView == null)
            return;
        if(contextMenu == null) {
            contextMenu = new PopupMenu(getActivity(), actionView.menuButton);
            contextMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    if(chatDelegate != null) {
                        chatDelegate.didContextMenuItemClicked(menuItem.getItemId());
                        return true;
                    }
                    return false;
                }
            });
        }
        else if(contextMenu.getMenu() != null)
            contextMenu.getMenu().clear();
        MenuInflater inflater = contextMenu.getMenuInflater();
        try
        {
            inflater.inflate(muted ? R.menu.channel_chat_muted : R.menu.channel_chat_unmuted, contextMenu.getMenu());
        }
        catch(Exception ignored) {}
        actionView.setMenu(contextMenu);
    }

    @Override
    protected boolean canDeleteIncomingMessages() {
        if(channelRef != null)
            return channelRef.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() || channelRef.role.getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor() || channelRef.role.getConstructor() == Constants.PARTICIPANT_MODERATOR.getConstructor();
        return false;
    }
}
