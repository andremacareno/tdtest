package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.views.actionviews.ActionView;

/**
 * Created by Andrew on 12.05.2015.
 */
public abstract class BaseFragment extends Fragment {
    private ActionView actionView;
    private NotificationObserver passcodeLockShownObserver, passcodeLockPassedObserver;
    protected boolean passcodeLockShown = false;
    public interface ActionButtonDelegate
    {
        public void onActionButtonClick();
        public int getActionButtonImageResource();
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }
    @Override
    public void onResume()
    {
        super.onResume();
        try
        {
            if(passcodeLockShownObserver == null)
            {
                passcodeLockShownObserver = new NotificationObserver(NotificationCenter.didPasscodeLockScreenShown) {
                    @Override
                    public void didNotificationReceived(Notification notification) {
                        passcodeLockShown = true;
                        onPasscodeLockShown();
                    }
                };
                NotificationCenter.getInstance().addObserver(passcodeLockShownObserver);
                passcodeLockPassedObserver = new NotificationObserver(NotificationCenter.didPasscodeLockPassed) {
                    @Override
                    public void didNotificationReceived(Notification notification) {
                        passcodeLockShown = false;
                        onPasscodeLockPassed();
                    }
                };
                NotificationCenter.getInstance().addObserver(passcodeLockPassedObserver);
            }
            if(actionView != null)
                actionView.getController().onFragmentResumed();
        }
        catch(Exception ignored) {}
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        /*if(memoryController == null)
        {
            memoryController = new ComponentCallbacks2() {
                @Override
                public void onTrimMemory(int level) {
                    if (level >= TRIM_MEMORY_MODERATE)
                    {
                        if(BuildConfig.DEBUG)
                            Log.d("BaseFragment", "low memory condition detected");
                        lowMemoryDetected = true;
                    }
                }

                @Override
                public void onConfigurationChanged(Configuration configuration) {

                }

                @Override
                public void onLowMemory() {
                    lowMemoryDetected = true;
                }
            };
            getActivity().registerComponentCallbacks(memoryController);
        }*/
        ((TelegramMainActivity) getActivity()).setActionButtonDelegate(getActionButtonDelegate());
        actionView = getActionView();
        if(actionView != null)
        {
            actionView.setController(getActionViewController());
            ((TelegramMainActivity) getActivity()).setActionView(actionView);
        }
        if(TelegramApplication.sharedApplication().getUpdateService() == null)
        {
            NotificationObserver updateServiceObserver = new NotificationObserver(NotificationCenter.didUpdateServiceStarted) {
                @Override
                public void didNotificationReceived(Notification notification) {
                    NotificationCenter.getInstance().removeObserver(this);
                    TelegramUpdatesHandler service = (TelegramUpdatesHandler) notification.getObject();
                    onUpdateServiceConnected(service);
                    ActionViewController actionViewController = getActionViewController();
                    if(actionViewController != null)
                        actionViewController.onUpdateServiceConnected(service);
                }
            };
            NotificationCenter.getInstance().addObserver(updateServiceObserver);
        }
        else {
            onUpdateServiceConnected(TelegramApplication.sharedApplication().getUpdateService());
            ActionViewController actionViewController = getActionViewController();
            if(actionViewController != null)
                actionViewController.onUpdateServiceConnected(TelegramApplication.sharedApplication().getUpdateService());
        }
    }
    @Override
    public void onPause()
    {
        super.onPause();
        try
        {
            if(actionView != null)
            {
                actionView.getController().onFragmentPaused();
                actionView = null;
            }
        }
        catch(Exception ignored) {}
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        try
        {
            if(passcodeLockShownObserver != null) {
                NotificationCenter.getInstance().removeObserver(passcodeLockShownObserver);
                NotificationCenter.getInstance().removeObserver(passcodeLockPassedObserver);
            }
            ((ViewGroup) ((TelegramMainActivity) getActivity()).getActionViewPlaceholder()).removeView(actionView);
        }
        catch(Exception ignored) {}
    }
    protected void onPasscodeLockShown() {
        //if fragment need to do something on passcode lock show (e.g. hide keyboard), just override this method.
    }
    protected void onPasscodeLockPassed() {
        //if fragment need to do something on passcode lock show (e.g. hide keyboard), just override this method.
    }
    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim)
    {
        if(nextAnim == 0)
            return super.onCreateAnimation(transit, enter, nextAnim);
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);

        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                didFragmentAnimationStarted();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                didFragmentAnimationFinished();
            }
        });
        return anim;
    }
    protected void invalidateActionButton()
    {
        ((TelegramMainActivity) getActivity()).setActionButtonDelegate(getActionButtonDelegate());
    }
    protected void goBack()
    {
        try
        {
            ((TelegramMainActivity) getActivity()).performGoBack();
        }
        catch(Exception ignored) {}
    }
    protected void didFragmentAnimationStarted() {}
    protected void didFragmentAnimationFinished() {}
    public abstract ActionButtonDelegate getActionButtonDelegate();             //return null if don't need to show action button
    public abstract ActionView getActionView();
    public abstract ActionViewController getActionViewController();
    public abstract void onUpdateServiceConnected(TelegramUpdatesHandler service);
}
