package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.viewcontrollers.SubmitCodeButtonController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SimpleActionViewController;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 03/04/15.
 */
public class EnterCodeFragment extends BaseFragment {
    private final String actionViewTag = "EnterCodeActionView";
    private ActionViewController actionViewController;
    private EditText smsCode;
    private TextView wrongCode;
    private TextView smsSentText;
    SubmitCodeButtonController submitController;
    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new SimpleActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new EnterCodeActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.enter_code, container, false);
        String enteredPhone = getArguments().getString(Constants.ARG_PHONE_NUMBER, "");
        smsSentText = (TextView) v.findViewById(R.id.sms_sent_notification);
        smsSentText.setText(String.format(getResources().getString(R.string.sms_sent_notification), enteredPhone));
        wrongCode = (TextView) v.findViewById(R.id.wrong_code);
        smsCode = (EditText) v.findViewById(R.id.smsCodeEditText);
        return v;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(submitController == null)
            submitController = new SubmitCodeButtonController(smsCode, wrongCode);
        else
            submitController.rebindToViews(smsCode, wrongCode);
        ((TelegramMainActivity) getActivity()).setSecondaryAuthDelegate(submitController.getSecondaryAuthDelegate());
        smsCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == getResources().getInteger(R.integer.auth_code_length))
                {
                    //submit code
                    if(submitController != null)
                        submitController.didEnterCodeButtonClicked();
                }
            }
        });
    }
    private class EnterCodeActionViewController extends SimpleActionViewController {
        private View.OnClickListener backButtonListener;

        @Override
        public boolean havePopup() {
            return false;
        }

        @Override
        public boolean showBackButton() {
            return true;
        }

        @Override
        public boolean showDoneButton() {
            return false;
        }

        @Override
        public String getTitle() {
            return getResources().getString(R.string.enter_code);
        }

        @Override
        public View.OnClickListener getBackButtonListener() {
            if(this.backButtonListener == null)
                this.backButtonListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TG.getClientInstance().send(new TdApi.ResetAuth(true), TelegramApplication.dummyResultHandler);     //need to reset setCode state, so force=true is applicable
                        goBack();
                    }
                };
            return this.backButtonListener;
        }
        @Override
        public View.OnClickListener getDoneButtonListener() {
            return null;
        }

        @Override
        public int getMenuResource() {
            return 0;
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
}
