package com.andremacareno.tdtest.fragments;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.OnTaskFailureListener;
import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.AudioPlaybackService;
import com.andremacareno.tdtest.BackPressListener;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.ChannelCache;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.Emoji;
import com.andremacareno.tdtest.MessageCache;
import com.andremacareno.tdtest.MessageVoicePlayer;
import com.andremacareno.tdtest.PeerType;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.models.AdditionalActionForward;
import com.andremacareno.tdtest.models.AdditionalActionForwardChatMessages;
import com.andremacareno.tdtest.models.AdditionalActionReply;
import com.andremacareno.tdtest.models.AdditionalActionSendContact;
import com.andremacareno.tdtest.models.AdditionalSendAction;
import com.andremacareno.tdtest.models.AudioTrackModel;
import com.andremacareno.tdtest.models.BotCommandModel;
import com.andremacareno.tdtest.models.CurrentUserModel;
import com.andremacareno.tdtest.models.InlineBotShortModel;
import com.andremacareno.tdtest.models.InlineQueryResultWrap;
import com.andremacareno.tdtest.models.MessageModel;
import com.andremacareno.tdtest.models.PhotoMessageModel;
import com.andremacareno.tdtest.models.TelegramAction;
import com.andremacareno.tdtest.models.TextMessageModel;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.models.WebPageMessageModel;
import com.andremacareno.tdtest.tasks.AudioPathTask;
import com.andremacareno.tdtest.tasks.GalleryImagePathTask;
import com.andremacareno.tdtest.tasks.GenerateMessageModelsTask;
import com.andremacareno.tdtest.tasks.GetFullPeerTask;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.BotCmdListController;
import com.andremacareno.tdtest.viewcontrollers.ChatAttachListController;
import com.andremacareno.tdtest.viewcontrollers.ChatController;
import com.andremacareno.tdtest.viewcontrollers.ChatSearchController;
import com.andremacareno.tdtest.viewcontrollers.GalleryRecentPhotosController;
import com.andremacareno.tdtest.viewcontrollers.InlineResultListController;
import com.andremacareno.tdtest.viewcontrollers.MessageMenuListController;
import com.andremacareno.tdtest.viewcontrollers.RecentInlineBotsListController;
import com.andremacareno.tdtest.views.AdditionalSendView;
import com.andremacareno.tdtest.views.AttachMenuView;
import com.andremacareno.tdtest.views.BaseChatListView;
import com.andremacareno.tdtest.views.BigMicButton;
import com.andremacareno.tdtest.views.BotCmdListView;
import com.andremacareno.tdtest.views.BotKeyboardView;
import com.andremacareno.tdtest.views.ComposeMessagePlaceholder;
import com.andremacareno.tdtest.views.ComposeMessageView;
import com.andremacareno.tdtest.views.EmojiView;
import com.andremacareno.tdtest.views.EmptyListPlaceholder;
import com.andremacareno.tdtest.views.ForwardBottomSheetView;
import com.andremacareno.tdtest.views.InlineResultsListView;
import com.andremacareno.tdtest.views.MessageActionsListView;
import com.andremacareno.tdtest.views.ScrollButtonDelegate;
import com.andremacareno.tdtest.views.VoiceRecordingBackground;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.ChatActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by andremacareno on 17/04/15.
 */
public abstract class BaseChatFragment extends BaseFragment {
    private final AccelerateInterpolator accInterpolator = new AccelerateInterpolator(1.6f);
    private final DecelerateInterpolator decInterpolator = new DecelerateInterpolator(1.6f);
    private final RelativeLayout.LayoutParams hiddenKeyboard = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 0);
    private final RelativeLayout.LayoutParams shownKeyboard = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(260));
    private final RelativeLayout.LayoutParams shownSmallKeyboard = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
    private BaseChatListView dialogList;
    private AdditionalSendView additionalSendView;
    private View additionalSendViewDivider;
    protected ChatController listController;
    private EmptyListPlaceholder noMessagesPlaceholder;
    private ChatAttachListController attachListController;
    private GalleryRecentPhotosController recentPhotosController;
    private BigMicButton micButton;
    private ComposeMessagePlaceholder composeMsgPlaceholder;
    private VoiceRecordingBackground voiceRecBackground;
    protected ComposeMessageView composeMsgView;
    protected boolean muted = false;
    protected boolean searchMode = false;
    protected boolean noMessagesConfirmed = false;
    private boolean scrollButtonHidden = true;
    private boolean restoreAttachMenu = false;
    private boolean restoreMessageMenu = false;
    protected BackPressListener closeMenuDelegate, closeEmojiKeyboardDelegate, searchModeBackPress, selectionModeBackPress;
    protected BotCmdListView botCmdListView, recentInlineListView;
    protected FrameLayout botCmdListViewWrap;
    private AttachMenuView attachMenu;
    private FrameLayout keyboardFrameLayout;
    private EmojiView emojiView;
    protected BotKeyboardView botKeyboard;
    private EmojiView.EmojiKeyboardCallback emojiKeyboardCallback;
    private GalleryImagePathTask galleryImagePathTask;
    private AudioPathTask audioPathTask;
    private View scrollButton;
    private ComposeMessageView.ComposeViewDelegate composeViewDelegate;
    private ScrollButtonDelegate scrollButtonDelegate;
    private AttachMenuView.AttachMenuDelegate attachMenuDelegate;
    protected ChatDelegate chatDelegate;
    private boolean dontSendMessages = false;
    private boolean isEmojiKeyboardShown = false;
    private boolean forceReplyTo = false;
    private boolean botKeyboardHidden = true;
    private static volatile Handler searchUserHandler;
    protected volatile int replyMarkupMessageId;
    protected BotCmdListController botCommandListController;
    protected BotCmdListController.BotCmdListControllerDelegate cmdListControllerDelegate;
    protected RecentInlineBotsListController recentInlineListController;
    private BigMicButton.BigMicButtonDelegate micButtonDelegate;
    private TdApi.SendChatAction sendTypingRequest;
    private AdditionalSendAction additionalSendAction = null;
    private Runnable forwardRunnable, clearComposeFormRunnable;
    private View.OnClickListener textMessagePlainObserver;
    private View.OnClickListener composeMessagePlaceholderClickListener;
    private boolean disableOpenProfile = false;
    protected ChatActionView.ChatSearchViewDelegate chatSearchDelegate;
    private MessageActionsListView messageActionsBottomSheet;
    private MessageMenuListController messageActionsController;
    private MessageActionsListView.MessageActionsDelegate msgActionsDelegate;
    private NotificationObserver contactListObserver;
    private int chatTopMsgId = 0;
    protected boolean selectionMode = false;
    private ProgressDialog processingDialog;
    private boolean searchingByUsername = false;
    private ForwardBottomSheetView.ForwardMenuDelegate fwdMenuDelegate;

    protected final View.OnClickListener clearSelection = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            performClearSelection();
        }
    };
    protected final View.OnClickListener delSelection = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(listController != null)
                listController.delSelectedMessages();
        }
    };
    protected final View.OnClickListener fwdSelection = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try
            {
                ((TelegramMainActivity) getActivity()).showFwdBottomSheet(fwdMenuDelegate);
            }
            catch(Exception ignored) {}
        }
    };

    private void performClearSelection() {
        if(listController != null && chatDelegate != null)
        {
            listController.clearSelection(true);
            chatDelegate.didSelectionCountChanged(0);
        }
        try
        {
            ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(selectionModeBackPress);
        }
        catch(Exception ignored) {}
    }

    //inline bots
    private FrameLayout inlineListWrap;
    private InlineResultsListView inlineList;
    private InlineResultListController inlineListController;
    private InlineResultListController.InlineResultListDelegate inlineDelegate;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        searchMode = false;
        if(getArguments() != null)
            chatTopMsgId = getArguments().getInt(Constants.ARG_LAST_MSG_ID, getArguments().containsKey(Constants.ARG_START_MESSAGE) ? getArguments().getInt(Constants.ARG_START_MESSAGE) : 0);
        contactListObserver = new NotificationObserver(NotificationCenter.didContactsListViewItemClicked) {
            @Override
            public void didNotificationReceived(Notification notification) {
                if(notification.getObject() != null && notification.getObject() instanceof UserModel)
                {
                    UserModel contact = (UserModel) notification.getObject();
                    NotificationCenter.getInstance().removeObserver(contactListObserver);
                    TdApi.InputMessageContact sendContact = new TdApi.InputMessageContact(contact.getPhoneNumber(), contact.getFirstName(), contact.getLastName(), contact.getId());
                    performSendMessage(sendContact);
                }
            }
        };
        chatSearchDelegate = new ChatActionView.ChatSearchViewDelegate() {
            @Override
            public void searchAboveRequested() {
                if(listController != null)
                {
                    ChatSearchController searchController = listController.getSearchController();
                    if(searchController != null)
                        searchController.searchAbove();
                }
            }

            @Override
            public void searchBelowRequested() {
                if(listController != null)
                {
                    ChatSearchController searchController = listController.getSearchController();
                    if(searchController != null)
                        searchController.searchBelow();
                }
            }

            @Override
            public void didQuerySubmit(String query) {
                if(listController != null)
                {
                    ChatSearchController searchController = listController.getSearchController();
                    if(searchController != null)
                        searchController.submit(query);
                }
            }
        };
        inlineDelegate = new InlineResultListController.InlineResultListDelegate() {
            @Override
            public void didResultSelected(long queryId, String resultId) {
                if(dontSendMessages)
                    return;
                if(BuildConfig.DEBUG)
                    Log.d("BaseChatFragment", "sending message");
                if(additionalSendAction != null && additionalSendAction.getDataType() == AdditionalSendAction.DataType.REPLY) {
                    listController.sendMessage(queryId, resultId, ((AdditionalActionReply) additionalSendAction).getReplyTo().getMessageId());
                }
                else
                    listController.sendMessage(queryId, resultId);
                additionalSendAction = null;
                checkAdditionalSendViewVisibility();
            }

            @Override
            public void loadMore() {
                if(composeMsgView != null)
                    composeMsgView.loadMoreInlineResults();
            }
        };
        searchModeBackPress = new BackPressListener() {
            @Override
            public void onBackPressed() {
                disableSearchMode();
            }
        };
        selectionModeBackPress = new BackPressListener() {
            @Override
            public void onBackPressed() {
                performClearSelection();
            }
        };
        if(getArguments() != null && getArguments().getLong(Constants.ARG_CHAT_ID, 0) != 0)
        {
            TdApi.OpenChat openChatRequest = new TdApi.OpenChat(getArguments().getLong(Constants.ARG_CHAT_ID));
            TG.getClientInstance().send(openChatRequest, TelegramApplication.dummyResultHandler);
        }
        fwdMenuDelegate = new ForwardBottomSheetView.ForwardMenuDelegate()
        {
            private AdditionalActionForwardChatMessages fwdInfo;
            @Override
            public void onBottomSheetClose(boolean dismissed) {
                fwdInfo = null;
                if(!dismissed)
                    performClearSelection();
            }

            @Override
            public AdditionalActionForward getForwardInfo() {
                if(listController == null)
                    return null;
                if(fwdInfo == null)
                    fwdInfo = new AdditionalActionForwardChatMessages(listController.getSelection(), listController.getSelectionIds(), getArguments().getLong(Constants.ARG_CHAT_ID), listController.getFwdTitle());
                return fwdInfo;
            }

            @Override
            public long getCurrentChatId() {
                return getArguments().getLong(Constants.ARG_CHAT_ID);
            }

            @Override
            public void performSelfForward() {
                final AdditionalActionForward actualFwdInfoRef = fwdInfo;
                if(listController != null && actualFwdInfoRef != null)
                    listController.forwardMessages(actualFwdInfoRef.getSourceChatId(), actualFwdInfoRef.getMessageIds());
            }

            @Override
            public void performSelfComment(TdApi.InputMessageText comment) {
                performSendMessage(comment);
            }

            @Override
            public void closeBottomSheet() {
                try
                {
                    ((TelegramMainActivity) getActivity()).slideDownBottomSheet();
                }
                catch(Exception ignored) {}
            }
        };
        this.muted = getArguments() != null && getArguments().getBoolean(Constants.ARG_CHAT_MUTED, false);
        clearComposeFormRunnable = new Runnable() {
            @Override
            public void run() {
            if(composeMsgView != null)
                composeMsgView.clearTextMessageForm();
            }
        };
        composeMessagePlaceholderClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ComposeMessagePlaceholder.Reason reason = composeMsgPlaceholder.getReason();
                if(reason == null)
                    return;
                if(isPrivateChat() && reason == ComposeMessagePlaceholder.Reason.BOT_NOT_INITIALISED)
                    ((PrivateChatFragment) BaseChatFragment.this).initChatWithBot();
                else if(reason == ComposeMessagePlaceholder.Reason.KICKED_FROM_CHAT || reason == ComposeMessagePlaceholder.Reason.DELETED_ACCOUNT)
                {
                    if(getArguments() == null || !getArguments().containsKey(Constants.ARG_CHAT_ID))
                        return;
                    long chatId = getArguments().getLong(Constants.ARG_CHAT_ID);
                    ((TelegramMainActivity) getActivity()).clearTillMainFragmentNextTime();
                    TdApi.DeleteChatHistory delChat = new TdApi.DeleteChatHistory(chatId);
                    TG.getClientInstance().send(delChat, TelegramApplication.dummyResultHandler);
                    goBack();
                }
                else if(reason == ComposeMessagePlaceholder.Reason.CHANNEL_NOT_FOLLOWING)
                {
                    try
                    {
                        TdApi.AddChatParticipant follow = new TdApi.AddChatParticipant(getArguments().getLong(Constants.ARG_CHAT_ID), CurrentUserModel.getInstance().getUserModel().getId(), 50);
                        TG.getClientInstance().send(follow, TelegramApplication.dummyResultHandler);
                    }
                    catch(Exception ignored) {}
                }
            }
        };
        textMessagePlainObserver = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isEmojiKeyboardShown && emojiKeyboardCallback != null)
                    emojiKeyboardCallback.closeKeyboard();
                if(!botKeyboardHidden)
                    hideBotKeyboard(false);
                if(composeMsgView != null)
                    composeMsgView.getMessageEditText().setOnClickListener(null);
            }
        };
        micButtonDelegate = new BigMicButton.BigMicButtonDelegate() {
            @Override
            public void didRecordStarted() {
                micButton.voiceRecordingEnabled();
                voiceRecBackground.voiceRecordingEnabled();
            }

            @Override
            public void didRecordStopped() {
                micButton.voiceRecordingDisabled();
                voiceRecBackground.voiceRecordingDisabled();
            }

            @Override
            public void didVolumeLevelReceived(float percent) {
                micButton.updateVolume(percent);
            }

            @Override
            public void didMicButtonMoved(float translation, float threshold) {
                micButton.setTranslationX(translation);
                voiceRecBackground.moveSlideToCancel(translation, threshold);
            }
        };
        forwardRunnable = new Runnable() {
            @Override
            public void run() {
                didFragmentBecameUseless();
            }
        };
        this.sendTypingRequest = new TdApi.SendChatAction(getArguments().getLong(Constants.ARG_CHAT_ID), new TdApi.SendMessageTypingAction());
        this.replyMarkupMessageId = getArguments().getInt(Constants.ARG_REPLY_MARKUP_MSG);
        galleryImagePathTask = new GalleryImagePathTask();
        audioPathTask = new AudioPathTask();
        hiddenKeyboard.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        shownKeyboard.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        shownSmallKeyboard.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        if(chatDelegate == null)
            chatDelegate = new ChatDelegate() {
                @Override
                public void didUrlClicked(final String url, boolean confirm) {
                    if(listController != null && listController.isInSelectionMode())
                        return;
                    if(confirm)
                    {
                        new AlertDialog.Builder(getActivity())
                                .setTitle(R.string.action_confirmation)
                                .setMessage(String.format(getResources().getString(R.string.open_link_confirmation), url))
                                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        openLink(url);
                                    }

                                })
                                .setNegativeButton(R.string.no, null)
                                .setCancelable(true)
                                .show();
                    }
                    else
                        openLink(url);
                }

                @Override
                public void didCommandClicked(String cmd) {
                    if(listController != null && listController.isInSelectionMode())
                        return;
                    performSendMessage(new TdApi.InputMessageText(cmd, true, null));
                }

                @Override
                public void didUsernameClicked(String username) {
                    if(listController != null && listController.isInSelectionMode())
                        return;
                    final String uname = username.startsWith("@") ? username.substring(1) : username;
                    searchingByUsername = true;
                    if(processingDialog != null)
                        processingDialog.show();
                    TdApi.SearchUser searchUserReq = new TdApi.SearchUser(uname);
                    Client.ResultHandler handler = new Client.ResultHandler() {
                        @Override
                        public void onResult(final TdApi.TLObject object) {
                            try
                            {
                                if (object == null || object.getConstructor() != TdApi.User.CONSTRUCTOR) {
                                    TdApi.SearchChannel searchChannel = new TdApi.SearchChannel(uname);
                                    TG.getClientInstance().send(searchChannel, new Client.ResultHandler() {
                                        @Override
                                        public void onResult(TdApi.TLObject object) {
                                            try
                                            {
                                                if(object == null || object.getConstructor() != TdApi.Channel.CONSTRUCTOR)
                                                {
                                                    if (searchUserHandler != null)
                                                        searchUserHandler.post(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                searchingByUsername = false;
                                                                if(processingDialog != null)
                                                                    processingDialog.dismiss();
                                                                Toast.makeText(TelegramApplication.sharedApplication().getApplicationContext(), R.string.chat_not_found, Toast.LENGTH_SHORT).show();
                                                            }
                                                        });
                                                }
                                                else {
                                                    final int peer = ((TdApi.Channel) object).id;
                                                    GetFullPeerTask getFullPeerTask = new GetFullPeerTask(peer, PeerType.CHANNEL);
                                                    getFullPeerTask.onComplete(new OnTaskCompleteListener() {
                                                        @Override
                                                        public void taskComplete(AsyncTaskManTask task) {
                                                            final long chatId = ((GetFullPeerTask) task).getChatId();
                                                            if (searchUserHandler != null) {
                                                                searchUserHandler.post(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        searchingByUsername = false;
                                                                        if(processingDialog != null)
                                                                            processingDialog.dismiss();
                                                                        TdApi.Channel channel = ChannelCache.getInstance().getChannel(peer);
                                                                        openChannelChat(channel, chatId);
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                    getFullPeerTask.addToQueue();
                                                }
                                            }
                                            catch(Exception e) { e.printStackTrace(); }
                                        }
                                    });
                                } else {
                                    final int peer = ((TdApi.User) object).id;
                                    GetFullPeerTask getFullPeerTask = new GetFullPeerTask(peer, PeerType.USER);
                                    getFullPeerTask.onComplete(new OnTaskCompleteListener() {
                                        @Override
                                        public void taskComplete(AsyncTaskManTask task) {
                                            final long chatId = ((GetFullPeerTask) task).getChatId();
                                            if (searchUserHandler != null) {
                                                searchUserHandler.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        searchingByUsername = false;
                                                        if(processingDialog != null)
                                                            processingDialog.dismiss();
                                                        //UserModel user = UserCache.getInstance().getUserById(peer);
                                                        openProfile(peer, chatId);
                                                        //openPrivateChat(user, chatId);
                                                    }
                                                });
                                            }
                                        }
                                    });
                                    getFullPeerTask.addToQueue();
                                }
                            }
                            catch(Exception e) { e.printStackTrace(); }
                        }
                    };
                    TG.getClientInstance().send(searchUserReq, handler);
                }

                @Override
                public void didHashtagClicked(String hashtag) {
                    if(listController != null && listController.isInSelectionMode())
                        return;
                    searchHashtag(hashtag);
                    //Toast.makeText(getContext(), String.format("hashtag: %s", hashtag), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void didEmailClicked(String email) {
                    sendEmail(email);
                }

                @Override
                public void didPhotoClicked(PhotoMessageModel photo) {
                    if(listController != null && listController.isInSelectionMode())
                        listController.selectMessage(photo.getMessageId());
                    else
                        openPhotoView(photo);
                }
                @Override
                public void didPhotoClicked(WebPageMessageModel photo) {
                    if(listController != null && listController.isInSelectionMode())
                        listController.selectMessage(photo.getMessageId());
                    else
                        openPhotoView(photo);
                }

                @Override
                public void didPlayerLaunchRequested(AudioTrackModel msg) {
                    if(listController != null && listController.isInSelectionMode())
                        listController.selectMessage(msg.getMessageId());
                    else
                        openPlayer(msg);
                }

                @Override
                public void didOpenProfileRequested(int user, long chatId) {
                    openProfile(user, chatId);
                }

                @Override
                public void didOpenChannelProfileRequested(int channelId, long chatId) {
                    openChannelProfile(channelId, chatId);
                }

                @Override
                public void didVoiceMessageRecorded(TdApi.InputMessageVoice inputMsg) {
                    performSendMessage(inputMsg);
                }

                @Override
                public void didNoMessagesInChatConfirmed() {
                    noMessagesConfirmed = true;
                    if(getArguments() != null && isPrivateChat() && getArguments().getBoolean(Constants.ARG_BOT, false))
                        updateComposeMessageViewVisibility();
                    else
                        noMessagesPlaceholder.setVisibility(View.VISIBLE);
                }

                @Override
                public void didChatHasMessagesConfirmed() {
                    noMessagesConfirmed = false;
                    if(getArguments() != null && isPrivateChat() && getArguments().getBoolean(Constants.ARG_BOT, false))
                        updateComposeMessageViewVisibility();
                    else
                        noMessagesPlaceholder.setVisibility(View.INVISIBLE);
                }

                @Override
                public void didChatItemClicked(final MessageModel model) {
                    createMessageActionsView();
                    messageActionsController.attachMessage(model);
                    try
                    {
                        ((TelegramMainActivity) getActivity()).showBottomSheet(messageActionsBottomSheet);
                        restoreMessageMenu = true;
                    }
                    catch(Exception ignored) {}
                }

                @Override
                public void didFileOpenRequested(String path, String mimeType) {
                    if(BuildConfig.DEBUG)
                        Log.d("BaseChatFragment", String.format("mimeType = %s", mimeType));
                    Uri uriPath = Uri.fromFile(new File(path));
                    Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                    fileIntent.setDataAndType(uriPath, mimeType);
                    List<ResolveInfo> intents = getActivity().getPackageManager().queryIntentActivities(fileIntent, PackageManager.MATCH_DEFAULT_ONLY);
                    if(intents.isEmpty())
                        Toast.makeText(getActivity().getApplicationContext(), R.string.no_app_found, Toast.LENGTH_SHORT).show();
                    else
                        startActivity(fileIntent);
                }

                @Override
                public void didNotificationSettingsUpdated(boolean nowMuted, TdApi.NotificationSettingsScope scope) {
                    if(scope.getConstructor() == TdApi.NotificationSettingsForGroupChats.CONSTRUCTOR && isPrivateChat())
                        return;
                    if(scope.getConstructor() == TdApi.NotificationSettingsForPrivateChats.CONSTRUCTOR && !isPrivateChat())
                        return;
                    if(scope.getConstructor() == TdApi.NotificationSettingsForChat.CONSTRUCTOR)
                    {
                        long thisChatId = getArguments() == null || !getArguments().containsKey(Constants.ARG_CHAT_ID) ? 0 : getArguments().getLong(Constants.ARG_CHAT_ID);
                        long settingsChatId = ((TdApi.NotificationSettingsForChat) scope).chatId;
                        if(thisChatId != settingsChatId)
                            return;
                    }
                    if(muted != nowMuted)
                    {
                        muted = nowMuted;
                        updateContextMenu();
                    }
                }

                @Override
                public void didContextMenuItemClicked(final int menuItemId) {
                    if(getArguments() == null || !getArguments().containsKey(Constants.ARG_CHAT_ID))
                        return;
                    long chatId = getArguments().getLong(Constants.ARG_CHAT_ID);
                    if(menuItemId == R.id.search && !searchMode)
                    {
                        enableSearchMode();
                    }
                    else if(menuItemId == R.id.mute || menuItemId == R.id.unmute)
                    {
                        final TdApi.NotificationSettingsScope scope = new TdApi.NotificationSettingsForChat(chatId);
                        TdApi.GetNotificationSettings currentSettingsReq = new TdApi.GetNotificationSettings(scope);
                        TG.getClientInstance().send(currentSettingsReq, new Client.ResultHandler() {
                            @Override
                            public void onResult(TdApi.TLObject object) {
                                if (object == null || object.getConstructor() != TdApi.NotificationSettings.CONSTRUCTOR)
                                    return;
                                TdApi.NotificationSettings settings = (TdApi.NotificationSettings) object;
                                settings.muteFor = menuItemId == R.id.mute ? Integer.MAX_VALUE : 0;
                                TdApi.SetNotificationSettings setReq = new TdApi.SetNotificationSettings(scope, settings);
                                TG.getClientInstance().send(setReq, new Client.ResultHandler() {
                                    @Override
                                    public void onResult(TdApi.TLObject object) {
                                        try {
                                            if (object == null || object.getConstructor() != TdApi.Ok.CONSTRUCTOR)
                                                return;
                                            TelegramApplication.applicationHandler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    muted = menuItemId == R.id.mute;
                                                    updateContextMenu();
                                                }
                                            });
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        });
                    }
                    else if(menuItemId == R.id.clear_history)
                    {
                        TdApi.DeleteChatHistory delHistoryReq = new TdApi.DeleteChatHistory(chatId);
                        TG.getClientInstance().send(delHistoryReq, TelegramApplication.dummyResultHandler);
                        if(listController != null)
                            listController.clearChatHistory();
                    }
                    else if(menuItemId == R.id.leave_group && !isPrivateChat())
                    {
                        TdApi.ChangeChatParticipantRole quit = new TdApi.ChangeChatParticipantRole(chatId, CurrentUserModel.getInstance().getUserModel().getId(), Constants.PARTICIPANT_LEFT);
                        TG.getClientInstance().send(quit, TelegramApplication.dummyResultHandler);
                        ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(closeEmojiKeyboardDelegate);
                        ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(closeMenuDelegate);
                        goBack();
                    }
                    /*else if(menuItemId == R.id.disband)
                    {
                        try
                        {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle(R.string.disband_confirm_title)
                                    .setMessage(R.string.disband_confirm)
                                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            ((GroupChatFragment)BaseChatFragment.this).disbandNaVi();
                                        }

                                    })
                                    .setNegativeButton(R.string.no, null)
                                    .setCancelable(false)
                                    .show();
                        }
                        catch(Exception e) { e.printStackTrace(); }
                    }*/
                }

                @Override
                public void didLocationClicked(TdApi.Location location) {
                    if(listController != null && listController.isInSelectionMode())
                        return;
                    String uri = String.format(Locale.ENGLISH, "geo:%f,%f", location.latitude, location.longitude);
                    Intent geoIntent  = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    List<ResolveInfo> intents = getActivity().getPackageManager().queryIntentActivities(geoIntent, PackageManager.MATCH_DEFAULT_ONLY);
                    if(intents.isEmpty())
                        Toast.makeText(getActivity().getApplicationContext(), R.string.maps_app_not_found, Toast.LENGTH_SHORT).show();
                    else
                        startActivity(geoIntent);
                }

                @Override
                public void didClearComposeMessageFormRequested() {
                    try
                    {
                        TelegramApplication.applicationHandler.post(clearComposeFormRunnable);
                    }
                    catch(Exception e) { e.printStackTrace(); }
                }

                @Override
                public void didQuickReplyRequested(MessageModel msg) {
                    additionalSendAction = new AdditionalActionReply(msg);
                    checkAdditionalSendViewVisibility();
                    composeMsgView.checkSendButton();
                    AndroidUtilities.showKeyboard(composeMsgView.getMessageEditText());
                }

                @Override
                public void didQuickShareRequested(MessageModel msg) {
                    if(BuildConfig.DEBUG)
                        Log.d("ChatFragment", "quick share requested");
                    if(msgActionsDelegate != null)
                        msgActionsDelegate.onShareRequested(msg);
                }

                @Override
                public boolean canSendMessage() {
                    return displayComposeMessageView() == ComposeMessagePlaceholder.Reason.NO_REASON;
                }

                @Override
                public void didViaBotClicked(String botUsername) {
                    if(composeMsgView != null)
                    {
                        composeMsgView.getMessageEditText().setText("");
                        composeMsgView.getMessageEditText().setText(botUsername.concat(" "));
                        composeMsgView.getMessageEditText().setSelection(botUsername.length() + 1);
                        AndroidUtilities.showKeyboard(composeMsgView.getMessageEditText());
                    }
                }

                @Override
                public void didSelectionCountChanged(int selectionCount) {
                    if(selectionCount == 0 && selectionMode) {
                        selectionMode = false;
                        disableSelectionMode();
                        try
                        {
                            ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(selectionModeBackPress);
                        }
                        catch(Exception ignored) {}
                    }
                    else {
                        onSelectionCountUpdate(selectionCount);
                        if(selectionCount >= 1) {
                            selectionMode = true;
                        }
                    }
                    try
                    {
                        if(selectionCount >= 1)
                            ((TelegramMainActivity) getActivity()).interceptBackPress(selectionModeBackPress);
                        else
                            ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(selectionModeBackPress);
                    }
                    catch(Exception ignored) {}
                }

                @Override
                public void didSelectionDelButtonVisibilityChanged(boolean show) {
                    changeDeleteIconVisibilityInSelectionBar(show);
                }

                @Override
                public boolean canDeleteMessage(MessageModel msg) {
                    if(msgActionsDelegate != null)
                        return msgActionsDelegate.canDeleteMessage(msg);
                    return false;
                }

                @Override
                public void didChatSearchReachedEndAbove() {
                    Toast.makeText(getContext(), R.string.end_of_search_above, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void didChatSearchReachedEndBelow() {
                    Toast.makeText(getContext(), R.string.end_of_search_below, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void didChatSearchHaveNoResults() {
                    Toast.makeText(getContext(), R.string.no_results, Toast.LENGTH_SHORT).show();
                }
            };
        msgActionsDelegate = new MessageActionsListView.MessageActionsDelegate() {
            @Override
            public void onBottomSheetClose(boolean dismissed) {
                if(!dismissed && messageActionsController != null)
                    messageActionsController.continueProcessing();
                restoreMessageMenu = false;
            }
            @Override
            public void onActionChosen()
            {
                try
                {
                    ((TelegramMainActivity) getActivity()).slideDownBottomSheet();
                }
                catch(Exception ignored) {}
            }
            @Override
            public boolean canSendMessage() {
                if(chatDelegate != null)
                    return chatDelegate.canSendMessage();
                return false;
            }

            @Override
            public boolean canDeleteMessage(MessageModel msg) {
                if(msg == null)
                    return false;
                if(CurrentUserModel.getInstance() != null && msg.getFromId() == CurrentUserModel.getInstance().getUserModel().getId())
                    return true;
                return canDeleteIncomingMessages();
            }

            @Override
            public boolean canCopyMessage(MessageModel msg) {
                return msg != null && (msg.getSuitableConstructor() == TdApi.MessageText.CONSTRUCTOR || msg.getSuitableConstructor() == TdApi.MessageWebPage.CONSTRUCTOR);
            }

            @Override
            public void onReplyRequested(MessageModel msg) {
                additionalSendAction = new AdditionalActionReply(msg);
                checkAdditionalSendViewVisibility();
                composeMsgView.checkSendButton();
                AndroidUtilities.showKeyboard(composeMsgView.getMessageEditText());
            }

            @Override
            public void onShareRequested(final MessageModel msg) {
                if(BuildConfig.DEBUG)
                    Log.d("ChatFragment", "share requested");
                final AdditionalActionForward fwdInfo = new AdditionalActionForwardChatMessages(Arrays.asList(msg), new int[] {msg.getMessageId()}, msg.getChatId(), msg.getFromUsername());
                ForwardBottomSheetView.ForwardMenuDelegate lightFwdMenuDelegate = new ForwardBottomSheetView.ForwardMenuDelegate() {
                    @Override
                    public void onBottomSheetClose(boolean dismissed) {
                        if(!dismissed)
                            performClearSelection();
                    }

                    @Override
                    public AdditionalActionForward getForwardInfo() {
                        if(listController == null)
                            return null;
                        return fwdInfo;
                    }

                    @Override
                    public long getCurrentChatId() {
                        return getArguments().getLong(Constants.ARG_CHAT_ID);
                    }

                    @Override
                    public void performSelfForward() {
                        if(listController != null && fwdInfo != null)
                            listController.forwardMessages(fwdInfo.getSourceChatId(), fwdInfo.getMessageIds());
                    }

                    @Override
                    public void performSelfComment(TdApi.InputMessageText comment) {
                        performSendMessage(comment);
                    }

                    @Override
                    public void closeBottomSheet() {
                        try
                        {
                            ((TelegramMainActivity) getActivity()).slideDownBottomSheet();
                        }
                        catch(Exception ignored) {}
                    }
                };
                ((TelegramMainActivity) getActivity()).showFwdBottomSheet(lightFwdMenuDelegate);
            }

            @Override
            public void onCopyRequested(MessageModel msg) {
                if(msg.getSuitableConstructor() != TdApi.MessageText.CONSTRUCTOR && msg.getSuitableConstructor() != TdApi.MessageWebPage.CONSTRUCTOR)
                    return;
                try
                {
                    ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("telegramMessage", msg.getSuitableConstructor() == TdApi.MessageText.CONSTRUCTOR ?  ((TextMessageModel) msg).getText() : ((WebPageMessageModel) msg).getText());
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(getActivity(), R.string.copied_to_clipboard, Toast.LENGTH_SHORT).show();
                }
                catch(Exception e) { e.printStackTrace(); }
            }

            @Override
            public void onSelectRequested(MessageModel msg) {
                if(listController != null)
                    listController.selectMessage(msg.getMessageId());
            }

            @Override
            public void onDeleteRequested(MessageModel msg) {
                listController.deleteMessage(msg.getMessageId());
            }
        };
        composeViewDelegate = new ComposeMessageView.ComposeViewDelegate() {
            @Override
            public void didSendTextMsgRequested(TdApi.InputMessageText text) {
                String trimmed = text.text == null ? "" : text.text.trim();
                if(additionalSendAction != null)
                {
                    if(additionalSendAction.getDataType() == AdditionalSendAction.DataType.REPLY && trimmed.length() == 0)
                        return;
                    if(additionalSendAction.getDataType() == AdditionalSendAction.DataType.REPLY) {
                        performSendMessage(text, ((AdditionalActionReply) additionalSendAction).getReplyTo().getMessageId());
                    }
                    else if(additionalSendAction.getDataType() == AdditionalSendAction.DataType.SEND_CONTACT)
                    {
                        if(trimmed.length() > 0)
                            performSendMessage(text);
                        performSendMessage(((AdditionalActionSendContact) additionalSendAction).getShareableContact());
                    }
                    else if(additionalSendAction.getDataType() == AdditionalSendAction.DataType.FWD)
                    {
                        if(trimmed.length() > 0)
                            performSendMessage(text);
                        if(!dontSendMessages)
                            listController.forwardMessages(((AdditionalActionForward) additionalSendAction).getSourceChatId(), ((AdditionalActionForward) additionalSendAction).getMessageIds());
                    }
                    additionalSendAction = null;
                    checkAdditionalSendViewVisibility();
                    if(trimmed.length() == 0)
                        composeMsgView.checkSendButton();
                }
                else if(trimmed.length() > 0)
                    performSendMessage(text);
            }

            @Override
            public void didAttachButtonPressed() {
                if(isEmojiKeyboardShown)
                    emojiKeyboardCallback.toggleEmojiKeyboard();
                ((TelegramMainActivity) getActivity()).showBottomSheet(attachMenu);
                restoreAttachMenu = true;
            }

            @Override
            public void didBotCommandListRequested() {
                if(botCommandListController == null || botCommandListController.getCmdListSize() == 0)
                    return;
                botCmdListViewWrap.setVisibility(View.VISIBLE);
                botCmdListView.setVisibility(View.VISIBLE);
                recentInlineListView.setVisibility(View.GONE);
            }

            @Override
            public void didHideCommandListRequested() {
                if(botCommandListController == null || botCommandListController.getCmdListSize() == 0)
                    return;
                botCmdListViewWrap.setVisibility(View.GONE);
            }

            @Override
            public void didShowRecentInlineListRequested() {
                if(recentInlineListController == null || recentInlineListController.getCmdListSize() == 0)
                    return;
                botCmdListViewWrap.setVisibility(View.VISIBLE);
                botCmdListView.setVisibility(View.GONE);
                recentInlineListView.setVisibility(View.VISIBLE);
            }

            @Override
            public void didHideRecentInlineListRequested() {
                if(recentInlineListController == null || recentInlineListController.getCmdListSize() == 0)
                    return;
                botCmdListViewWrap.setVisibility(View.GONE);
            }

            @Override
            public void didShowBotKeyboardRequested() {
                showBotKeyboard();
            }

            @Override
            public void didHideBotKeyboardRequested() {
                hideBotKeyboard(true);
            }

            @Override
            public boolean isBotKeyboardShown() {
                return botKeyboard.getVisibility() == View.VISIBLE;
            }

            @Override
            public void sendTypingRequested() {
                if(shouldSendTyping()) {
                    if(BuildConfig.DEBUG)
                        Log.d("BaseChatFragment", "sending typing notification");
                    TG.getClientInstance().send(sendTypingRequest, TelegramApplication.dummyResultHandler);
                }
            }

            @Override
            public boolean isAdditionalSendActionBound() {
                return additionalSendAction != null;
            }

            @Override
            public void onNewInlineQueryResults(List<InlineQueryResultWrap> results, boolean vertical, boolean fromLoadMore) {
                if(inlineListController != null) {
                    inlineList.changeOrientation(vertical);
                    inlineListController.newQueryResultsReceived(results, fromLoadMore);
                }
            }

            @Override
            public void toggleInline(boolean show) {
                inlineListWrap.setVisibility(show ? View.VISIBLE : View.GONE);
            }

            @Override
            public void inlineBotSelected(InlineBotShortModel cmd) {
                composeMsgView.setInlineBot(cmd);
            }
        };
        attachMenuDelegate = new AttachMenuView.AttachMenuDelegate() {
            @Override
            public void onBottomSheetClose(boolean dismissed) {
                restoreAttachMenu = false;
                if(dismissed && recentPhotosController != null)
                    recentPhotosController.getAdapter().clearSelection();
                attachListController.continueStartIntent();
            }

            @Override
            public void onActionChoose() {
                try
                {
                    ((TelegramMainActivity) getActivity()).slideDownBottomSheet();
                }
                catch(Exception ignored) {}
            }

            @Override
            public void requestPerformQuickSend() {
                recentPhotosController.sendSelection();
                try
                {
                    ((TelegramMainActivity) getActivity()).slideDownBottomSheet();
                }
                catch(Exception ignored) {}
            }


            @Override
            public void didQuickSendSelectionCountUpdated(int newCount) {
                attachListController.getAdapter().updateQuickSendSelectionCount(newCount);
            }

            @Override
            public void sendGalleryImage(TdApi.InputMessageContent img) {
                performSendMessage(img);
            }

            @Override
            public void shareContactRequested() {
                NotificationCenter.getInstance().addObserver(contactListObserver);
                openContacts();
            }
        };
        emojiKeyboardCallback = new EmojiView.EmojiKeyboardCallback() {
            @Override
            public void toggleEmojiKeyboard()
            {
                if(isEmojiKeyboardShown)
                {
                    closeKeyboard();
                }
                else
                {
                    if(composeMsgView != null)
                        composeMsgView.getMessageEditText().setOnClickListener(textMessagePlainObserver);
                    AndroidUtilities.hideKeyboard(composeMsgView.getMessageEditText());
                    composeMsgView.changeSmileButtonIcon(ComposeMessageView.IC_KEYBOARD);
                    if(!botKeyboardHidden) {
                        hideBotKeyboard(false);
                    }
                    emojiView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ((TelegramMainActivity) getActivity()).interceptBackPress(closeMenuDelegate);
                            keyboardFrameLayout.setLayoutParams(shownKeyboard);
                            emojiView.setVisibility(View.VISIBLE);
                            isEmojiKeyboardShown = true;
                        }
                    }, 150);
                }
            }

            @Override
            public void closeKeyboard() {
                emojiView.setVisibility(View.GONE);
                keyboardFrameLayout.setLayoutParams(hiddenKeyboard);
                composeMsgView.changeSmileButtonIcon(ComposeMessageView.IC_SMILE);
                emojiView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(botKeyboardHidden)
                            AndroidUtilities.showKeyboard(composeMsgView.getMessageEditText());
                        isEmojiKeyboardShown = false;
                    }
                }, 150);
                ((TelegramMainActivity) getActivity()).removeBackPressInterceptor();
            }

            @Override
            public boolean isEmojiKeyboardShown() {
                return isEmojiKeyboardShown;
            }
        };
        cmdListControllerDelegate = new BotCmdListController.BotCmdListControllerDelegate() {
            @Override
            public void didBotCommandListUpdated() {
                if(BuildConfig.DEBUG)
                    Log.d("BaseChatFragment", String.format("command list updated: size = %d", botCommandListController.getCmdListSize()));
                composeMsgView.setSlashButtonVisibility(botCommandListController.getCmdListSize() > 0);
            }

            @Override
            public void didCommandSelected(BotCommandModel cmd) {
                composeMsgView.getMessageEditText().setText("");
                String msgToSend = cmd.getCommand().startsWith("/") ? cmd.getCommand() : "/".concat(cmd.getCommand());
                if(!isPrivateChat())
                {
                    msgToSend = msgToSend.concat(cmd.getBotInfo().getDisplayUsername());
                    performSendMessage(new TdApi.InputMessageText(msgToSend, true, null), replyMarkupMessageId);
                }
                else
                    performSendMessage(new TdApi.InputMessageText(msgToSend, true, null));
            }

            @Override
            public void didKeyboardButtonPressed(String msgToSend, boolean oneTimeKeyboard) {
                if(isPrivateChat())
                    performSendMessage(new TdApi.InputMessageText(msgToSend, true, null));
                else
                    performSendMessage(new TdApi.InputMessageText(msgToSend, true, null), replyMarkupMessageId);
                if(oneTimeKeyboard)
                    deleteChatReplyMarkup();
            }

            @Override
            public void didNonKbdReplyMarkupReceived(TdApi.ReplyMarkup replyMarkup) {
                /*
                    received replyMarkupNone -> do nothing
                    received replyMarkupHideKeyboard -> hide botKeyboard
                    received replyMarkupForceReply -> raise forceReply flag
                 */
                if(replyMarkup.getConstructor() == TdApi.ReplyMarkupHideKeyboard.CONSTRUCTOR)
                    hideBotKeyboard(false);
                else if(replyMarkup.getConstructor() == TdApi.ReplyMarkupForceReply.CONSTRUCTOR) {
                    hideBotKeyboard(false);
                    forceReplyTo = true;
                    if(additionalSendAction != null && ((TdApi.ReplyMarkupForceReply) replyMarkup).personal)
                    {
                        additionalSendAction = null;
                        checkAdditionalSendViewVisibility();
                    }
                    showUiReplyMarkup();
                }
            }

            @Override
            public void didKeyboardReceived(boolean personal) {
                if(personal) {
                    showBotKeyboard();
                }
                else
                    hideBotKeyboard(false);
            }

            @Override
            public void didReplyMarkupMessageIdUpdated(int newMessageId) {
                replyMarkupMessageId = newMessageId;
                if(replyMarkupMessageId == 0) {
                    hideBotKeyboard(false);
                    composeMsgView.setKeyboardButtonVisibility(false, false);
                    if(botCommandListController != null && botCommandListController.getCmdListSize() > 0)
                        composeMsgView.setSlashButtonVisibility(true);
                }
            }
        };
    }

    private void createMessageActionsView() {
        if(messageActionsBottomSheet == null)
        {
            messageActionsBottomSheet = new MessageActionsListView(getContext());
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            messageActionsBottomSheet.setLayoutParams(lp);
            if(messageActionsController != null)
                messageActionsController.replaceView(messageActionsBottomSheet);
            else
            {
                messageActionsController = new MessageMenuListController(messageActionsBottomSheet);
                messageActionsController.setDelegate(msgActionsDelegate);
            }
            messageActionsBottomSheet.setDelegate(msgActionsDelegate);
        }
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        if(attachListController == null)
        {
            attachListController = new ChatAttachListController(attachMenu.attachActionsListView);
            attachMenu.attachActionsListView.setController(attachListController);
        }
        else
        {
            attachMenu.attachActionsListView.setController(attachListController);
            attachListController.replaceView(attachMenu.attachActionsListView);
        }
        attachListController.setDelegate(attachMenuDelegate);
        if(recentPhotosController == null)
            recentPhotosController = new GalleryRecentPhotosController(attachMenu.recentFromGallery);
        else
            recentPhotosController.replaceView(attachMenu.recentFromGallery);
        recentPhotosController.setDelegate(attachMenuDelegate);
        attachListController.onUpdateServiceConnected(service);
        attachListController.setFragmentReference(this);
        if(listController == null) {
            long chatId = getArguments().getLong(Constants.ARG_CHAT_ID);
            int lastReadInbox = getArguments().getInt(Constants.ARG_LAST_READ_INBOX, 0);
            int lastReadOutbox = getArguments().getInt(Constants.ARG_LAST_READ_OUTBOX, 0);
            int unreadCount = getArguments().getInt(Constants.ARG_UNREAD_COUNT);
            int startMessage = getArguments().getInt(Constants.ARG_START_MESSAGE, Integer.MAX_VALUE);
            boolean isChatWithBot = getArguments().getBoolean(Constants.ARG_BOT, false);
            boolean fromSearch = getArguments().getBoolean(Constants.ARG_FROM_SEARCH, false);
            listController = new ChatController(isChatWithBot, chatId, lastReadInbox, lastReadOutbox, startMessage, unreadCount, getChatListView(), fromSearch, chatTopMsgId);
            inlineListController = new InlineResultListController(inlineList, inlineDelegate);
            recentInlineListController = new RecentInlineBotsListController(recentInlineListView, composeViewDelegate);
            dialogList.setController(listController);
            listController.setChatDelegate(chatDelegate);
        }
        else
        {
            recentInlineListController.replaceView(recentInlineListView);
            dialogList.setController(listController);
            listController.replaceView(dialogList);
            inlineListController.replaceView(inlineList);
        }
        listController.onUpdateServiceConnected(service);
    }
    @Override
    public void onResume()
    {
        super.onResume();
        processingDialog= new ProgressDialog(getActivity());
        processingDialog.setMessage(getResources().getString(R.string.loading));
        processingDialog.setIndeterminate(true);
        processingDialog.setCancelable(false);
        if(searchingByUsername)
            processingDialog.show();
        searchUserHandler = new Handler(Looper.getMainLooper());
        //if(getArguments() != null && getArguments().getBoolean(Constants.ARG_CLEAR_BACK_STACK_ON_EXIT, false))
        ((TelegramMainActivity) getActivity()).clearTillMainFragmentNextTime();     //TODO check
        if(listController != null) {
            listController.onFragmentResumed();
            listController.setScrollButtonDelegate(scrollButtonDelegate);
            listController.getAdapter().setChatDelegate(chatDelegate);
        }
        if(attachListController != null)
            attachListController.onFragmentResumed();
        if(botCommandListController != null) {
            botCommandListController.replaceView(botKeyboard, botCmdListView);
            botCommandListController.onFragmentResumed();
        }
        if(searchMode)
            enableSearchMode();
    }
    protected void onLateBotCmdListControllerInit()
    {
        if(botCommandListController != null) {
            botCommandListController.replaceView(botKeyboard, botCmdListView);
            botCommandListController.onFragmentResumed();
        }
    }
    @Override
    public void onPause()
    {
        super.onPause();
        if(processingDialog.isShowing())
            processingDialog.dismiss();
        searchUserHandler = null;
        ((TelegramMainActivity) getActivity()).unsetClearTillMainFragmentFlag();
        if(listController != null)
            listController.onFragmentPaused();
        if(attachListController != null)
            attachListController.onFragmentPaused();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.chat_layout, container, false);
        dialogList = (BaseChatListView) v.findViewById(R.id.msg_list);
        composeMsgPlaceholder = (ComposeMessagePlaceholder) v.findViewById(R.id.compose_msg_placeholder);
        additionalSendView = (AdditionalSendView) v.findViewById(R.id.additional_send);
        additionalSendViewDivider = v.findViewById(R.id.additional_send_divider);
        micButton = (BigMicButton) v.findViewById(R.id.big_mic);
        voiceRecBackground = (VoiceRecordingBackground) v.findViewById(R.id.voice_rec_bg);
        composeMsgView = (ComposeMessageView) v.findViewById(R.id.bottom_panel);
        if(listController == null && getArguments() != null && getArguments().containsKey(Constants.ARG_CHAT_ID))
            composeMsgView.restoreTextMessage(getArguments().getLong(Constants.ARG_CHAT_ID));
        inlineListWrap = (FrameLayout) v.findViewById(R.id.inline_results_wrap);
        inlineList = (InlineResultsListView) v.findViewById(R.id.inline_results);
        botCmdListViewWrap = (FrameLayout) v.findViewById(R.id.bot_cmd_list_wrap);
        botCmdListView = (BotCmdListView) botCmdListViewWrap.findViewById(R.id.bot_cmd_list);
        recentInlineListView = (BotCmdListView) botCmdListViewWrap.findViewById(R.id.recent_inline_list);
        scrollButton = v.findViewById(R.id.scroll_down);
        noMessagesPlaceholder = (EmptyListPlaceholder) v.findViewById(R.id.no_msgs_placeholder);
        if(scrollButtonHidden)
            scrollButton.setTranslationY(AndroidUtilities.dp(100));
        else
            scrollButton.setTranslationY(0);
        scrollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listController != null) {
                    TdApi.Chat chatObj = getArguments() == null ? null : ChatCache.getInstance().getChatById(getArguments().getLong(Constants.ARG_CHAT_ID, 0));
                    if(chatObj == null)
                        listController.scrollToBottom();
                    else
                        listController.jumpTo(chatObj.topMessage.id);
                }
            }
        });
        scrollButtonDelegate = new ScrollButtonDelegate() {
            @Override
            public void onChatScroll(int itemsBelow, int itemsTotal) {
                if(BuildConfig.DEBUG)
                    Log.d("BaseChatFragment", String.format("ScrollButtonDelegate: itemsBelow = %d, itemsTotal = %d", itemsBelow, itemsTotal));
                if(itemsTotal < 15)
                    hideScrollButton(true);
                else
                {
                    if(itemsBelow >= 10) {
                        showScrollButton(true);
                    }
                    else if(itemsBelow < 10)
                        hideScrollButton(true);
                }
            }
        };
        keyboardFrameLayout = (FrameLayout) v.findViewById(R.id.custom_kbd_container);
        botKeyboard = (BotKeyboardView) keyboardFrameLayout.findViewById(R.id.bot_keyboard);
        emojiView = new EmojiView(true, true, keyboardFrameLayout.getContext());
        emojiView.setVisibility(View.GONE);
        emojiView.setListener(new EmojiView.Listener() {
            public boolean onBackspace() {
                if (composeMsgView.getMessageEditText().length() == 0) {
                    return false;
                }
                composeMsgView.getMessageEditText().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL));
                return true;
            }

            public void onEmojiSelected(String symbol) {
                int i = composeMsgView.getMessageEditText().getSelectionEnd();
                if (i < 0) {
                    i = 0;
                }
                try {
                    CharSequence localCharSequence = Emoji.replaceEmoji(symbol, composeMsgView.getMessageEditText().getPaint().getFontMetricsInt(), AndroidUtilities.dp(20), false);
                    composeMsgView.getMessageEditText().setText(composeMsgView.getMessageEditText().getText().insert(i, localCharSequence));
                    int j = i + localCharSequence.length();
                    composeMsgView.getMessageEditText().setSelection(j, j);
                } catch (Exception e) {
                }
            }

            public void onStickerSelected(TdApi.Sticker sticker) {
                performSendMessage(new TdApi.InputMessageSticker(new TdApi.InputFileId(sticker.sticker.id)));
            }

            @Override
            public void onGifSelected(TdApi.Animation anim) {
                performSendMessage(new TdApi.InputMessageAnimation(new TdApi.InputFileId(anim.animation.id), anim.width, anim.height, null));
            }
        });
        additionalSendView.getCancelButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                additionalSendAction = null;
                checkAdditionalSendViewVisibility();
                composeMsgView.checkSendButton();
            }
        });
        attachMenu = new AttachMenuView(container.getContext());
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        attachMenu.setLayoutParams(lp);
        checkAdditionalSendViewVisibility();
        attachMenu.setDelegate(attachMenuDelegate);
        keyboardFrameLayout.addView(emojiView);
        composeMsgView.getMicButton().setBigButtonDelegate(micButtonDelegate);
        composeMsgView.setDelegate(composeViewDelegate);
        composeMsgView.setEmojiKeyboardCallback(emojiKeyboardCallback);
        return v;
    }
    protected void deleteSelectedMessages()
    {

    }
    private void showUiReplyMarkup()
    {
        if(additionalSendAction == null && replyMarkupMessageId != 0 && getArguments() != null)
        {
            TdApi.Message msg = MessageCache.getInstance().getMessageById(getArguments().getLong(Constants.ARG_CHAT_ID), replyMarkupMessageId);
            if(msg == null || (msg.replyMarkup.getConstructor() != TdApi.ReplyMarkupForceReply.CONSTRUCTOR && isPrivateChat()))
                return;
            GenerateMessageModelsTask convertReply = new GenerateMessageModelsTask(msg);
            convertReply.onComplete(new OnTaskCompleteListener() {
                @Override
                public void taskComplete(AsyncTaskManTask task) {
                    try
                    {
                        GenerateMessageModelsTask cast = (GenerateMessageModelsTask) task;
                        final MessageModel msg = cast.getMessages().get(0);
                        if(msg.getMessageId() != replyMarkupMessageId)
                            return;
                        TelegramApplication.applicationHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if(additionalSendAction == null)
                                {
                                    additionalSendAction = new AdditionalActionReply(msg);
                                    checkAdditionalSendViewVisibility();
                                }
                            }
                        });
                    }
                    catch(Exception ignored) {}
                }
            });
            convertReply.addToQueue();
        }
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        showUiReplyMarkup();
        updateContextMenu();
        updateComposeMessageViewVisibility();
        if (closeMenuDelegate == null)
            closeMenuDelegate = new BackPressListener() {
                @Override
                public void onBackPressed() {
                    if(isEmojiKeyboardShown)
                    {
                        emojiKeyboardCallback.closeKeyboard();
                    }
                    if(!botKeyboardHidden)
                        hideBotKeyboard(false);
                }
            };
        if(closeEmojiKeyboardDelegate == null)
        {
            closeEmojiKeyboardDelegate = new BackPressListener() {
                @Override
                public void onBackPressed() {
                    if(isEmojiKeyboardShown)
                        emojiKeyboardCallback.toggleEmojiKeyboard();
                }
            };
        }
        if(isEmojiKeyboardShown || !botKeyboardHidden)
            ((TelegramMainActivity) getActivity()).interceptBackPress(closeMenuDelegate);
        else
            ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(closeMenuDelegate);
        composeMsgView.getMicButton().setChatDelegate(this.chatDelegate);
        if(restoreAttachMenu)
        {
            ((TelegramMainActivity) getActivity()).showBottomSheet(attachMenu);
        }
        else if(restoreMessageMenu)
        {
            createMessageActionsView();
            ((TelegramMainActivity) getActivity()).showBottomSheet(messageActionsBottomSheet);
        }
    }
    protected void updateComposeMessageViewVisibility()
    {
        ComposeMessagePlaceholder.Reason reason = displayComposeMessageView();
        TdApi.Chat chatObj = getArguments() == null ? null : ChatCache.getInstance().getChatById(getArguments().getLong(Constants.ARG_CHAT_ID, 0));
        if(chatObj != null && chatObj.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR)
        {
            if(reason == ComposeMessagePlaceholder.Reason.NO_REASON)
            {
                dontSendMessages = false;
                composeMsgPlaceholder.setVisibility(View.GONE);
                composeMsgView.setVisibility(View.VISIBLE);
            }
            else
            {
                dontSendMessages = true;
                if(reason == ComposeMessagePlaceholder.Reason.CHANNEL_NOT_FOLLOWING)
                {
                    composeMsgPlaceholder.explainReason(reason);
                    composeMsgPlaceholder.setVisibility(View.VISIBLE);
                    composeMsgPlaceholder.setOnClickListener(composeMessagePlaceholderClickListener);
                    composeMsgView.setVisibility(View.INVISIBLE);
                }
                else
                {
                    composeMsgPlaceholder.setVisibility(View.GONE);
                    composeMsgView.setVisibility(View.GONE);
                }
            }
        }
        else
        {
            if(reason == ComposeMessagePlaceholder.Reason.NO_REASON) {
                dontSendMessages = false;
                composeMsgPlaceholder.setVisibility(View.GONE);
                composeMsgView.setVisibility(View.VISIBLE);
            }
            else {
                dontSendMessages = reason != ComposeMessagePlaceholder.Reason.BOT_NOT_INITIALISED;
                composeMsgPlaceholder.explainReason(reason);
                composeMsgPlaceholder.setVisibility(View.VISIBLE);
                composeMsgPlaceholder.setOnClickListener(composeMessagePlaceholderClickListener);
                composeMsgView.setVisibility(View.INVISIBLE);
            }
        }
    }
    private void performSendMessage(TdApi.InputMessageContent content)
    {
        if(dontSendMessages)
            return;
        if(forceReplyTo)
        {
            listController.sendMessage(content, replyMarkupMessageId);
            forceReplyTo = false;
            deleteChatReplyMarkup();
        }
        else
            listController.sendMessage(content);
    }
    private void performSendMessage(TdApi.InputMessageContent content, int replyTo)
    {
        if(dontSendMessages)
            return;
        listController.sendMessage(content, replyTo);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == Activity.RESULT_OK)
        {
            if(requestCode == TelegramAction.ATTACH_TAKE_A_PHOTO)
            {
                attachListController.addPhotoToGallery();
                TdApi.InputMessageContent content = new TdApi.InputMessagePhoto(new TdApi.InputFileLocal(attachListController.getPhotoPath()), "");
                performSendMessage(content);
            }
            else if(requestCode == TelegramAction.ATTACH_CHOOSE_FROM_GALLERY)
            {
                Uri uri = data.getData();
                if(uri == null)
                    Toast.makeText(getActivity(), R.string.error_picking_from_gallery, Toast.LENGTH_LONG).show();
                else
                {
                    if(Build.VERSION.SDK_INT >= 19)
                    {
                        final int takeFlags = data.getFlags()
                                & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        // Check for the freshest data.
                        //noinspection ResourceType
                        TelegramApplication.sharedApplication().getContentResolver().takePersistableUriPermission(uri, takeFlags);
                    }
                    galleryImagePathTask.setLink(uri);
                    galleryImagePathTask.onFailure(new OnTaskFailureListener() {
                        @Override
                        public void taskFailed(AsyncTaskManTask task) {
                            try
                            {
                                TelegramApplication.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), R.string.error_picking_from_gallery, Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            catch(Exception ignored) {}
                        }
                    });
                    galleryImagePathTask.onComplete(new OnTaskCompleteListener() {
                        @Override
                        public void taskComplete(AsyncTaskManTask task) {
                            String path = ((GalleryImagePathTask) task).getImagePath();
                            if(path != null)
                            {
                                TdApi.InputMessageContent content = new TdApi.InputMessagePhoto(new TdApi.InputFileLocal(path), "");
                                performSendMessage(content);
                            }
                        }
                    });
                    galleryImagePathTask.addToQueue();
                }

            }
            else if(requestCode == TelegramAction.ATTACH_AUDIO)
            {
                Uri uri = data.getData();
                if(uri == null)
                    Toast.makeText(getActivity(), R.string.error_picking_audio, Toast.LENGTH_LONG).show();
                else
                {
                    if(Build.VERSION.SDK_INT >= 19)
                    {
                        final int takeFlags = data.getFlags()
                                & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        // Check for the freshest data.
                        //noinspection ResourceType
                        TelegramApplication.sharedApplication().getContentResolver().takePersistableUriPermission(uri, takeFlags);
                    }
                    audioPathTask.setLink(uri);
                    audioPathTask.onFailure(new OnTaskFailureListener() {
                        @Override
                        public void taskFailed(AsyncTaskManTask task) {
                            try
                            {
                                TelegramApplication.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), R.string.error_picking_audio, Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            catch(Exception ignored) {}
                        }
                    });
                    audioPathTask.onComplete(new OnTaskCompleteListener() {
                        @Override
                        public void taskComplete(AsyncTaskManTask task) {
                            String path = ((AudioPathTask) task).getImagePath();
                            if(path != null)
                            {
                                TdApi.InputMessageContent content = new TdApi.InputMessageAudio(new TdApi.InputFileLocal(path), 0, "", "");
                                performSendMessage(content);
                            }
                        }
                    });
                    audioPathTask.addToQueue();
                }

            }
        }
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        messageActionsBottomSheet = null;
        if(composeMsgView != null && getArguments() != null && getArguments().containsKey(Constants.ARG_CHAT_ID))
            composeMsgView.saveTextMessage(getArguments().getLong(Constants.ARG_CHAT_ID));
        try {
            ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(closeEmojiKeyboardDelegate);
            ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(closeMenuDelegate);
        }
        catch(Exception e) { e.printStackTrace(); }
        TdApi.CloseChat closeChatRequest = new TdApi.CloseChat(getArguments().getLong(Constants.ARG_CHAT_ID));
        TG.getClientInstance().send(closeChatRequest, TelegramApplication.dummyResultHandler);
        didFragmentBecameUseless();
    }

    private void didFragmentBecameUseless()
    {
        if(listController != null) {
            listController.removeObservers();
            listController.stopLoadingThread();
            listController = null;
        }
        if(botCommandListController != null)
            botCommandListController.removeObservers();
        if(recentPhotosController != null)
            recentPhotosController.removeObservers();
        MessageVoicePlayer.getInstance().stopPlayback();
    }
    private void openPhotoView(PhotoMessageModel msg)
    {
        AndroidUtilities.hideKeyboard(composeMsgView.getMessageEditText());
        ((TelegramMainActivity)getActivity()).enableViewPhotoMode(msg.getOriginal().getPhoto(), msg.getForwardFromId() == 0 ? msg.getFromUsername() : msg.getForwardFromUsername(), msg.getForwardFromId() == 0 ? msg.getIntegerDate() : msg.getIntegerForwardDate());
        /*ViewPhotoFragment fr = new ViewPhotoFragment();
        fr.setOriginal(msg.getOriginal());
        fr.setPreview(msg.getPreview());
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction tr = fm.beginTransaction();
        tr.replace(R.id.container, fr, Constants.VIEW_PHOTO_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();*/
    }
    private void openPhotoView(WebPageMessageModel msg)
    {

        if(msg.getOriginal() == null || msg.getOriginal().getFileId() == 0) {
            if(BuildConfig.DEBUG)
                Log.d("BaseFragment", "null");
            return;
        }
        AndroidUtilities.hideKeyboard(composeMsgView.getMessageEditText());
        ((TelegramMainActivity)getActivity()).enableViewPhotoMode(msg.getOriginal(), msg.getForwardFromId() == 0 ? msg.getFromUsername() : msg.getForwardFromUsername(), msg.getForwardFromId() == 0 ? msg.getIntegerDate() : msg.getIntegerForwardDate());
        /*ViewPhotoFragment fr = new ViewPhotoFragment();
        fr.setOriginal(msg.getOriginal());
        fr.setPreview(msg.getPreview());
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction tr = fm.beginTransaction();
        tr.replace(R.id.container, fr, Constants.VIEW_PHOTO_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();*/
    }
    private void closeKeyboardAndRemoveBackPressListeners()
    {
        try
        {
            ActionView actionView = getActionView();
            if(searchMode && actionView != null && actionView instanceof ChatActionView)
                ((ChatActionView) actionView).clear(true);
            ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(searchModeBackPress);
            ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(closeEmojiKeyboardDelegate);
            ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(closeMenuDelegate);
        }
        catch(Exception ignored) {}
    }
    private void openPrivateChat(UserModel user, long chatId)
    {
        if(getArguments() != null && getArguments().getLong(Constants.ARG_CHAT_ID) == chatId)
            return;
        if(disableOpenProfile)
            return;
        if(user == null)
            return;
        try
        {
            TdApi.Chat chat = ChatCache.getInstance().getChatById(chatId);
            if(chat == null)
                return;
            closeKeyboardAndRemoveBackPressListeners();
            disableOpenProfile = true;
            AndroidUtilities.hideKeyboard(composeMsgView.getMessageEditText());
            Bundle args = new Bundle();
            args.putLong(Constants.ARG_CHAT_ID, chatId);
            args.putInt(Constants.ARG_LAST_MSG_ID, chat.topMessage.id);
            args.putBoolean(Constants.ARG_CHAT_MUTED, chat.notificationSettings.muteFor > 0);
            args.putBoolean(Constants.ARG_BOT, user.isBot());
            args.putInt(Constants.ARG_REPLY_MARKUP_MSG, chat.replyMarkupMessageId);
            args.putInt(Constants.ARG_CHAT_PEER, user.getId());
            args.putBoolean(Constants.ARG_DELETED_ACCOUNT, user.isDeletedAccount());
            args.putInt(Constants.ARG_LAST_READ_INBOX, chat.lastReadInboxMessageId);
            args.putInt(Constants.ARG_LAST_READ_OUTBOX, chat.lastReadOutboxMessageId);
            args.putInt(Constants.ARG_UNREAD_COUNT, chat.unreadCount);
            args.putInt(Constants.ARG_START_MESSAGE, chat.topMessage.id);
            args.putString(Constants.ARG_CHAT_TITLE, chat.title);
            Fragment fr = new PrivateChatFragment();;
            args.putInt(Constants.ARG_USER_ID, user.getId());
            fr.setRetainInstance(true);
            fr.setArguments(args);
            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
            ((TelegramMainActivity) getActivity()).clearTillMainFragmentNextTime();
            ft.replace(R.id.container, fr, Constants.CHAT_FRAGMENT);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
            fm.executePendingTransactions();
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    private void openChannelChat(TdApi.Channel channel, long chatId)
    {
        if(getArguments() != null && getArguments().getLong(Constants.ARG_CHAT_ID) == chatId)
            return;
        if(disableOpenProfile)
            return;
        if(channel == null)
            return;
        try
        {
            TdApi.Chat chat = ChatCache.getInstance().getChatById(chatId);
            if(chat == null)
                return;
            closeKeyboardAndRemoveBackPressListeners();
            disableOpenProfile = true;
            AndroidUtilities.hideKeyboard(composeMsgView.getMessageEditText());
            Bundle args = new Bundle();
            args.putLong(Constants.ARG_CHAT_ID, chatId);
            args.putInt(Constants.ARG_LAST_MSG_ID, chat.topMessage.id);
            args.putBoolean(Constants.ARG_CHAT_MUTED, chat.notificationSettings.muteFor > 0);
            args.putBoolean(Constants.ARG_BOT, false);
            args.putInt(Constants.ARG_REPLY_MARKUP_MSG, chat.replyMarkupMessageId);
            args.putInt(Constants.ARG_CHAT_PEER, channel.id);
            args.putBoolean(Constants.ARG_DELETED_ACCOUNT, false);
            args.putInt(Constants.ARG_LAST_READ_INBOX, chat.lastReadInboxMessageId);
            if(channel.isSupergroup)
                args.putInt(Constants.ARG_LAST_READ_OUTBOX, 2000000000);
            else
                args.putInt(Constants.ARG_LAST_READ_OUTBOX, chat.lastReadOutboxMessageId);
            args.putInt(Constants.ARG_UNREAD_COUNT, chat.unreadCount);
            args.putInt(Constants.ARG_START_MESSAGE, chat.topMessage.id);
            args.putString(Constants.ARG_CHAT_TITLE, chat.title);
            Fragment fr;
            if(!channel.isSupergroup)
            {
                args.putInt(Constants.ARG_CHANNEL_ID, channel.id);
                fr = new ChannelChatFragment();
            }
            else
            {
                args.putInt(Constants.ARG_SUPERGROUP_ID, channel.id);
                args.putBoolean(Constants.ARG_CHAT_LEFT, channel.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor() || channel.role.getConstructor() == Constants.PARTICIPANT_KICKED.getConstructor());
                fr = new GroupChatFragment();
            }

            fr.setRetainInstance(true);
            fr.setArguments(args);
            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
            ((TelegramMainActivity) getActivity()).clearTillMainFragmentNextTime();
            ft.replace(R.id.container, fr, Constants.CHAT_FRAGMENT);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
            fm.executePendingTransactions();
        }
        catch(Exception e) { e.printStackTrace(); }
    }

    protected void openChannelProfile(int channelId, long channelChatId)
    {
        try
        {
            if(BuildConfig.DEBUG)
                Log.d("ChatFragment", "openChannelProfile");
            if(disableOpenProfile)
                return;
            closeKeyboardAndRemoveBackPressListeners();
            disableOpenProfile = true;
            AndroidUtilities.hideKeyboard(composeMsgView.getMessageEditText());
            Bundle args = new Bundle();
            args.putLong(Constants.ARG_CHAT_ID, channelChatId);
            args.putInt(Constants.ARG_CHANNEL_ID, channelId);
            if(getArguments() != null && getArguments().containsKey(Constants.ARG_CHAT_ID))
                args.putLong(Constants.ARG_CHAT_REFERER, getArguments().getLong(Constants.ARG_CHAT_ID));
            ChannelProfileFragment fr = new ChannelProfileFragment();
            fr.setArguments(args);
            fr.setRetainInstance(true);
            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction tr = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
            tr.replace(R.id.container, fr, Constants.VIEW_PROFILE_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
            fm.executePendingTransactions();
        }
        catch(Exception ignored) {}
    }
    protected void openProfile(int user, long chatId)
    {
        try
        {
            if(BuildConfig.DEBUG)
                Log.d("ChatFragment", "openProfile");
            if(disableOpenProfile)
                return;
            closeKeyboardAndRemoveBackPressListeners();
            disableOpenProfile = true;
            AndroidUtilities.hideKeyboard(composeMsgView.getMessageEditText());
            Bundle args = new Bundle();
            args.putLong(Constants.ARG_CHAT_ID, chatId);
            args.putInt(Constants.ARG_USER_ID, user);
            if(getArguments() != null && getArguments().containsKey(Constants.ARG_CHAT_ID))
                args.putLong(Constants.ARG_CHAT_REFERER, getArguments().getLong(Constants.ARG_CHAT_ID));
            ProfileFragment fr = new ProfileFragment();
            fr.setArguments(args);
            fr.setRetainInstance(true);
            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction tr = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
            tr.replace(R.id.container, fr, Constants.VIEW_PROFILE_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
            fm.executePendingTransactions();
        }
        catch(Exception ignored) {}
    }
    protected void sendEmail(String email)
    {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",email, null));
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        startActivity(Intent.createChooser(emailIntent, getResources().getString(R.string.send_email)));
    }
    protected void openLink(String link)
    {
        String lnk = link.startsWith("http://") || link.startsWith("https://") ? link : "http://".concat(link);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(lnk));
        startActivity(i);
    }
    protected void openGroupChatInfo(int groupId)
    {
        closeKeyboardAndRemoveBackPressListeners();
        AndroidUtilities.hideKeyboard(composeMsgView.getMessageEditText());
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_CHAT_ID, getArguments().getLong(Constants.ARG_CHAT_ID));
        args.putInt(Constants.ARG_GROUP_ID, groupId);
        GroupChatProfileFragment fr = new GroupChatProfileFragment();
        fr.setArguments(args);
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction tr = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        tr.replace(R.id.container, fr, Constants.VIEW_CHAT_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    protected void openPlayer(AudioTrackModel track)
    {
        AudioPlaybackService playbackService = TelegramApplication.sharedApplication().getAudioPlaybackService();
        if(playbackService == null)
            return;
        if(track.equals(playbackService.nowPlaying()))
        {
            if(playbackService.isAudioPlaying())
                playbackService.pausePlayback();
            else
                playbackService.unpausePlayback();
            return;
        }
        playbackService.initializePlaylist(track, getArguments().getLong(Constants.ARG_CHAT_ID));
        playbackService.createPlaylistForChat(getArguments().getLong(Constants.ARG_CHAT_ID), track);
        ((TelegramMainActivity) getActivity()).showMiniPlayer(true);
    }
    private void openContacts()
    {
        Fragment fr = new ContactsFragment();
        String tag = Constants.CONTACTS_FRAGMENT;
        fr.setRetainInstance(true);
        Bundle args = new Bundle();
        args.putBoolean(Constants.ARG_CHOOSE_MODE, true);
        fr.setArguments(args);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction().replace(R.id.container, fr, tag).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    protected abstract void disableSelectionMode();
    protected abstract void onSelectionCountUpdate(int newCount);
    protected abstract void enableSearchMode();
    protected abstract void searchHashtag(String hashtag);
    protected abstract void disableSearchMode();
    protected abstract void changeDeleteIconVisibilityInSelectionBar(boolean canDelete);
    protected BaseChatListView getChatListView() { return this.dialogList; }
    protected void closeKeyboard()
    {
        try
        {
            EditText msgEditText = composeMsgView.getMessageEditText();
            if(AndroidUtilities.isKeyboardShown(msgEditText))
                AndroidUtilities.hideKeyboard(msgEditText);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    protected void hideBotKeyboard(boolean showKeyboard)
    {
        botKeyboardHidden = true;
        botKeyboard.setVisibility(View.GONE);
        keyboardFrameLayout.setLayoutParams(hiddenKeyboard);
        composeMsgView.setKeyboardButtonVisibility(true, false);
        if(showKeyboard)
            AndroidUtilities.showKeyboard(composeMsgView.getMessageEditText());
        ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(closeMenuDelegate);
    }
    protected void showBotKeyboard()
    {
        try
        {
            botKeyboardHidden = false;
            if(isEmojiKeyboardShown)
                emojiKeyboardCallback.closeKeyboard();
            ((TelegramMainActivity) getActivity()).interceptBackPress(closeMenuDelegate);
            if(composeMsgView != null)
                composeMsgView.getMessageEditText().setOnClickListener(textMessagePlainObserver);
            AndroidUtilities.hideKeyboard(composeMsgView.getMessageEditText());
            botKeyboard.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try
                    {
                        botKeyboard.setVisibility(View.VISIBLE);
                        if (botKeyboard.getApproximateHeight() < AndroidUtilities.dp(260)) {
                            shownSmallKeyboard.height = botKeyboard.getApproximateHeight();
                            keyboardFrameLayout.setLayoutParams(shownSmallKeyboard);
                            botKeyboard.getLayoutParams().height = shownSmallKeyboard.height;
                        } else {
                            keyboardFrameLayout.setLayoutParams(shownKeyboard);

                        }
                        composeMsgView.setKeyboardButtonVisibility(true, true);
                    }
                    catch(Exception ignored) {}
                }
            }, 200);
        }
        catch(Exception ignored) {}
    }
    @Override
    protected void didFragmentAnimationFinished()
    {
        disableOpenProfile = false;
    }
    protected void deleteChatReplyMarkup()
    {
        if(BuildConfig.DEBUG)
            Log.d("BaseChatFragment", "delete chat reply markup");
        TdApi.DeleteChatReplyMarkup delReplyMarkupReq = new TdApi.DeleteChatReplyMarkup(getArguments().getLong(Constants.ARG_CHAT_ID), replyMarkupMessageId);
        TG.getClientInstance().send(delReplyMarkupReq, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (BuildConfig.DEBUG)
                    Log.d("BaseChatFragment", String.format("deleteChatReplyMarkup: %s", object.toString()));
            }
        });
    }
    public void showScrollButton(boolean animated)
    {
        if(!scrollButtonHidden)
            return;
        if(!animated)
        {
            scrollButton.setTranslationY(0);
            scrollButtonHidden = false;
            return;
        }
        scrollButtonHidden = false;
        ObjectAnimator animator = ObjectAnimator.ofFloat(scrollButton, "translationY", AndroidUtilities.dp(100), 0).setDuration(300);
        animator.setInterpolator(decInterpolator);
        scrollButton.setClickable(true);
        animator.start();
    }
    public void hideScrollButton(boolean animated)
    {
        if(scrollButtonHidden)
            return;
        if(!animated)
        {
            scrollButton.setTranslationY(AndroidUtilities.dp(100));
            scrollButtonHidden = true;
            return;
        }
        scrollButtonHidden = true;
        ObjectAnimator animator = ObjectAnimator.ofFloat(scrollButton, "translationY", 0, AndroidUtilities.dp(100)).setDuration(300);
        animator.setInterpolator(accInterpolator);
        scrollButton.setClickable(false);
        animator.start();
    }
    public void bindAdditionalSendAction(AdditionalSendAction action)
    {
        this.additionalSendAction = action;
    }
    private void checkAdditionalSendViewVisibility()
    {
        //TODO animated hide
        if(additionalSendAction == null || additionalSendAction.getDataType() == AdditionalSendAction.DataType.INVITE_TO_CHAT)
        {
            additionalSendViewDivider.setVisibility(View.GONE);
            additionalSendView.setVisibility(View.GONE);
        }
        else
        {
            additionalSendViewDivider.setVisibility(View.VISIBLE);
            additionalSendView.setVisibility(View.VISIBLE);
            additionalSendView.attachAction(additionalSendAction);
        }
    }
    @Override
    protected void onPasscodeLockShown()
    {
        if(composeMsgView != null)
            AndroidUtilities.hideKeyboard(composeMsgView.getMessageEditText());
    }
    protected abstract void initBotCmdListController();
    protected abstract boolean isPrivateChat();
    protected abstract boolean shouldSendTyping();
    protected abstract ComposeMessagePlaceholder.Reason displayComposeMessageView();
    protected abstract void updateContextMenu();
    protected abstract boolean canDeleteIncomingMessages();
    public interface ChatDelegate
    {
        public void didUrlClicked(String url, boolean confirm);
        public void didCommandClicked(String cmd);
        public void didUsernameClicked(String username);
        public void didHashtagClicked(String hashtag);
        public void didEmailClicked(String email);
        public void didPhotoClicked(PhotoMessageModel photo);
        public void didPhotoClicked(WebPageMessageModel photo);
        public void didPlayerLaunchRequested(AudioTrackModel msg);
        public void didOpenProfileRequested(int user, long chatId);
        public void didOpenChannelProfileRequested(int channelId, long chatId);
        public void didVoiceMessageRecorded(TdApi.InputMessageVoice inputMsg);
        public void didNoMessagesInChatConfirmed();
        public void didChatHasMessagesConfirmed();
        public void didChatItemClicked(MessageModel model);
        public void didFileOpenRequested(String path, String mimeType);
        public void didNotificationSettingsUpdated(boolean muted, TdApi.NotificationSettingsScope scope);
        public void didContextMenuItemClicked(int menuItemId);
        public void didLocationClicked(TdApi.Location location);
        public void didClearComposeMessageFormRequested();
        public void didQuickReplyRequested(MessageModel msg);
        public void didQuickShareRequested(MessageModel msg);
        public boolean canSendMessage();
        public void didViaBotClicked(String botUsername);
        public void didSelectionCountChanged(int selection);
        public void didSelectionDelButtonVisibilityChanged(boolean show);
        public boolean canDeleteMessage(MessageModel msg);
        void didChatSearchReachedEndAbove();
        void didChatSearchReachedEndBelow();
        void didChatSearchHaveNoResults();
    }
}
