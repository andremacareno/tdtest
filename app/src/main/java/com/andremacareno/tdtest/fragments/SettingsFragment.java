package com.andremacareno.tdtest.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.OnTaskFailureListener;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.models.CurrentUserModel;
import com.andremacareno.tdtest.models.TelegramAction;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.tasks.CreateFileForCameraTask;
import com.andremacareno.tdtest.tasks.GalleryImagePathTask;
import com.andremacareno.tdtest.updatelisteners.UpdateUserProcessor;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.SettingsListController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.SettingsActionViewController;
import com.andremacareno.tdtest.views.SettingsListView;
import com.andremacareno.tdtest.views.UserDetailsView;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.ProfileActionView;

import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 03/04/15.
 */
public class SettingsFragment extends BaseFragment {
    private static final String TAG = "SettingsFragment";
    private final String actionViewTag = "SettingsActionView";
    private SettingsActionViewController actionViewController;
    private UserDetailsView userDetailsView;
    private SettingsListView settingsList;
    private SettingsListController listController;
    private ActionButtonDelegate actionButtonDelegate;
    private GalleryImagePathTask galleryImagePathTask;
    private String photo_path = null;
    private CameraFileReceiver cameraFileReceiver;
    private UpdateUserProcessor updateUserProcessor;
    private UserDetailsView.UserDetailsDelegate userDetailsDelegate;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        userDetailsDelegate = new UserDetailsView.UserDetailsDelegate() {
            @Override
            public void didUsernameViewClicked() {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "didUsernameViewClicked");
                editUsername();
            }

            @Override
            public void didPhoneViewClicked() {
            }
        };
        galleryImagePathTask = new GalleryImagePathTask();
        this.cameraFileReceiver = new CameraFileReceiver(this);
    }
    @Override
    public void onPause()
    {
        super.onPause();
        try
        {
            ((TelegramMainActivity) getActivity()).refreshNavDrawerInfo();
        }
        catch(Exception ignored) {}
        if(updateUserProcessor != null)
            updateUserProcessor.performPauseObservers();
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        if(actionButtonDelegate == null)
            actionButtonDelegate = new ActionButtonDelegate() {
                @Override
                public void onActionButtonClick() {
                    AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
                    adb.setItems(new CharSequence[]{getResources().getString(R.string.from_camera), getResources().getString(R.string.from_gallery), getResources().getString(R.string.delete_photo)}, new AlertDialog.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (i == 0)
                                openCameraForNewPhoto();
                            else if(i == 1)
                                openGalleryForNewPhoto();
                            else if(i == 2)
                                removePhoto();
                        }
                    });
                    adb.create().show();
                }

                @Override
                public int getActionButtonImageResource() {
                    return R.drawable.ic_camera;
                }
            };
        return actionButtonDelegate;
    }

    @Override
    public ActionView getActionView() {
        ProfileActionView v = new ProfileActionView(getActivity()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
        v.setBackButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        return v;
    }
    @Override
    public ActionViewController getActionViewController()
    {
        if(actionViewController == null)
        {
            UserModel currentUser = (CurrentUserModel.getInstance() != null && CurrentUserModel.getInstance().getUserModel() != null) ? CurrentUserModel.getInstance().getUserModel() : null;
            if(currentUser == null)
                return null;
            actionViewController = new SettingsActionViewController(currentUser);
            initUpdateProcessor();
        }
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.settings, container, false);
        userDetailsView = (UserDetailsView) v.findViewById(R.id.user_details);
        userDetailsView.setDontHideUsernameField(true);
        userDetailsView.setDelegate(userDetailsDelegate);
        settingsList = (SettingsListView) v.findViewById(R.id.settings_list);
        if(listController == null)
        {
            listController = new SettingsListController(settingsList);
            settingsList.setController(listController);
        }
        else
        {
            settingsList.setController(listController);
            listController.replaceView(settingsList);
        }
        listController.setFragmentReference(this);
        return v;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        UserModel currentUser = (CurrentUserModel.getInstance() != null && CurrentUserModel.getInstance().getUserModel() != null) ? CurrentUserModel.getInstance().getUserModel() : null;
        userDetailsView.bindUser(currentUser);
        PopupMenu.OnMenuItemClickListener menuListener = new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if(menuItem.getItemId() == R.id.action_edit_name)
                    editName();
                else if(menuItem.getItemId() == R.id.action_log_out)
                    performLogOut();
                return true;
            }
        };
        if(actionViewController != null)
        {
            actionViewController.setMenuListener(menuListener);
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(updateUserProcessor != null)
            updateUserProcessor.removeObservers();
    }
    @Override
    public void onResume()
    {
        super.onResume();
        if(updateUserProcessor != null)
            updateUserProcessor.performResumeObservers();
    }
    private void performLogOut()
    {
        TelegramApplication.sharedApplication().getUpdateService().didLogOutRequested();
        ((TelegramMainActivity) getActivity()).didLogOutRequested();
    }
    private void editName()
    {
        Bundle args = new Bundle();
        args.putString(Constants.ARG_FIRST_NAME, CurrentUserModel.getInstance().getUserModel().getFirstName());
        args.putString(Constants.ARG_LAST_NAME, CurrentUserModel.getInstance().getUserModel().getLastName());
        Fragment fr = new EditNameSettingsFragment();
        fr.setArguments(args);
        String tag = Constants.SETTINGS_EDIT_NAME_FRAGMENT;
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction().replace(R.id.container, fr, tag).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void editUsername()
    {
        Bundle args = new Bundle();
        args.putString(Constants.ARG_USERNAME, CurrentUserModel.getInstance().getUserModel().getUsername());
        Fragment fr = new EditUsernameSettingsFragment();
        fr.setArguments(args);
        String tag = Constants.SETTINGS_EDIT_USERNAME_FRAGMENT;
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction().replace(R.id.container, fr, tag).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    public void processAction(TelegramAction action)
    {
        if(action.getAction() == TelegramAction.SETTINGS_PASSCODE_LOCK)
            goToPasscodeLock();
    }
    private void goToPasscodeLock()
    {
        boolean passcodeEnabled = TelegramApplication.sharedApplication().sharedPreferences().getBoolean(Constants.PREFS_PASSCODE_LOCK_ENABLED, false);
        if(passcodeEnabled)
            ((TelegramMainActivity) getActivity()).showPasscodeLockScreen();
        Fragment fr = new PasscodeSetFragment();
        String tag = Constants.SETTINGS_PASSCODE_LOCK_FRAGMENT;
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction().replace(R.id.container, fr, tag).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void openCameraForNewPhoto()
    {
        if(!AndroidUtilities.haveCamera())
        {
            Toast.makeText(TelegramApplication.sharedApplication().getApplicationContext(), R.string.no_camera, Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Activity activity = getActivity();
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            CreateFileForCameraTask task = new CreateFileForCameraTask();
            task.onComplete(new OnTaskCompleteListener() {
                @Override
                public void taskComplete(AsyncTaskManTask task) {
                    CreateFileForCameraTask t = (CreateFileForCameraTask) task;
                    photo_path = t.getPhotoPath();
                    Message msg = Message.obtain(cameraFileReceiver);
                    msg.obj = t.getPhotoUri();
                    msg.sendToTarget();
                }
            });
            task.onFailure(new OnTaskFailureListener() {
                @Override
                public void taskFailed(AsyncTaskManTask task) {
                    //Toast.makeText((fragmentRef.get().getActivity()), R.string.error_taking_photo, Toast.LENGTH_SHORT).show();
                }
            });
            task.addToQueue();
        }
    }
    private void openGalleryForNewPhoto()
    {
        if (Build.VERSION.SDK_INT <19){
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(galleryIntent, TelegramAction.ATTACH_CHOOSE_FROM_GALLERY);
        } else {
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(galleryIntent, TelegramAction.ATTACH_CHOOSE_FROM_GALLERY);
        }
    }
    private void removePhoto()
    {
        TdApi.DeleteProfilePhoto delReq = new TdApi.DeleteProfilePhoto(CurrentUserModel.getInstance().getUserModel().getProfilePhotoId());
        TG.getClientInstance().send(delReq, TelegramApplication.dummyResultHandler);
    }
    private void performChangePhoto(String filePath)
    {
        TdApi.SetProfilePhoto setPhotoReq = new TdApi.SetProfilePhoto(filePath, null);
        TG.getClientInstance().send(setPhotoReq, TelegramApplication.dummyResultHandler);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == Activity.RESULT_OK)
        {
            if(requestCode == TelegramAction.ATTACH_TAKE_A_PHOTO)
            {
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                File f = new File("file:".concat(photo_path));
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                getActivity().sendBroadcast(mediaScanIntent);
                performChangePhoto(photo_path);
            }
            else if(requestCode == TelegramAction.ATTACH_CHOOSE_FROM_GALLERY)
            {
                Uri uri = data.getData();
                if(uri == null)
                    Toast.makeText(getActivity(), R.string.error_picking_from_gallery, Toast.LENGTH_LONG).show();
                else
                {
                    if(Build.VERSION.SDK_INT >= 19)
                    {
                        final int takeFlags = data.getFlags()
                                & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        // Check for the freshest data.
                        //noinspection ResourceType
                        TelegramApplication.sharedApplication().getContentResolver().takePersistableUriPermission(uri, takeFlags);
                    }
                    galleryImagePathTask.setLink(uri);
                    galleryImagePathTask.onFailure(new OnTaskFailureListener() {
                        @Override
                        public void taskFailed(AsyncTaskManTask task) {
                            try
                            {
                                TelegramApplication.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), R.string.error_picking_from_gallery, Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            catch(Exception ignored) {}
                        }
                    });
                    galleryImagePathTask.onComplete(new OnTaskCompleteListener() {
                        @Override
                        public void taskComplete(AsyncTaskManTask task) {
                            String path = ((GalleryImagePathTask) task).getImagePath();
                            if(path != null)
                                performChangePhoto(path);
                        }
                    });
                    galleryImagePathTask.addToQueue();
                }

            }
        }
    }
    private void initUpdateProcessor() {
        if (updateUserProcessor == null) {
            final UserModel userData = CurrentUserModel.getInstance() == null ? null : CurrentUserModel.getInstance().getUserModel();
            if (userData == null)
                return;
            if(BuildConfig.DEBUG)
                Log.d(TAG, String.format("currentUser == %d", userData.getId()));
            updateUserProcessor = new UpdateUserProcessor() {
                @Override
                protected String getKeyByUpdateConstructor(int constructor) {
                    return String.format("settings_user%d_observer_%d", userData.getId(), constructor);
                }

                @Override
                protected boolean shouldObserveUpdateUserStatus() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateUserBlocked() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateUser() {
                    return true;
                }

                @Override
                protected void processUpdateInBackground(TdApi.Update upd) {
                    try
                    {
                        if (upd.getConstructor() == TdApi.UpdateUserStatus.CONSTRUCTOR && ((TdApi.UpdateUserStatus) upd).userId == userData.getId())
                            userData.updateStatus(((TdApi.UpdateUserStatus) upd).status);
                        else if (upd.getConstructor() == TdApi.UpdateUser.CONSTRUCTOR && ((TdApi.UpdateUser) upd).user.id == userData.getId()) {
                            TdApi.User u = ((TdApi.UpdateUser) upd).user;
                            userData.updateUser(u);
                        }
                    }
                    catch(Exception e) { e.printStackTrace(); }
                }

                @Override
                protected void processUpdateInUiThread(TdApi.Update upd) {
                    try {
                        if (upd.getConstructor() == TdApi.UpdateUserStatus.CONSTRUCTOR && ((TdApi.UpdateUserStatus) upd).userId == userData.getId())
                            actionViewController.updateStatus(((TdApi.UpdateUserStatus) upd).status);
                        else if (upd.getConstructor() == TdApi.UpdateUserBlocked.CONSTRUCTOR && ((TdApi.UpdateUserBlocked) upd).userId == userData.getId())
                            actionViewController.initMenu();
                        else if (upd.getConstructor() == TdApi.UpdateUser.CONSTRUCTOR && ((TdApi.UpdateUser) upd).user.id == userData.getId()) {
                            if(BuildConfig.DEBUG)
                                Log.d(TAG, "processing updateUser");
                            TdApi.User u = ((TdApi.UpdateUser) upd).user;
                            actionViewController.updateDisplayName(String.format("%s %s", u.firstName, u.lastName));
                            actionViewController.initMenu();
                            actionViewController.updatePhoto(CommonTools.isLowDPIScreen() ? u.profilePhoto.small : u.profilePhoto.big);
                            userDetailsView.updateUserPhone(u.phoneNumber);
                            userDetailsView.updateUsername(u.username);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
            if (service != null) {
                updateUserProcessor.attachObservers(service);
            }
        }
    }

    public static class CameraFileReceiver extends PauseHandler
    {
        private WeakReference<SettingsFragment> fragmentRef;
        public CameraFileReceiver(SettingsFragment c)
        {
            this.fragmentRef = new WeakReference<SettingsFragment>(c);
        }

        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(fragmentRef.get() == null)
                return;
            Uri capturedImageURI = (Uri) message.obj;
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    capturedImageURI);
            fragmentRef.get().startActivityForResult(takePictureIntent, TelegramAction.ATTACH_TAKE_A_PHOTO);
        }
    }
}
