package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.support.v7.widget.ListPopupWindow;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;

import com.andremacareno.tdtest.AudioPlaybackService;
import com.andremacareno.tdtest.BackPressListener;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.listadapters.menu.MenuAdapter;
import com.andremacareno.tdtest.listadapters.menu.MenuItem;
import com.andremacareno.tdtest.models.AdditionalActionForward;
import com.andremacareno.tdtest.models.AdditionalActionForwardSharedMedia;
import com.andremacareno.tdtest.models.AudioTrackModel;
import com.andremacareno.tdtest.models.SharedImageModel;
import com.andremacareno.tdtest.models.SharedMedia;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.SharedMediaViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.views.ForwardBottomSheetView;
import com.andremacareno.tdtest.views.PopupListView;
import com.andremacareno.tdtest.views.SharedMediaListView;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.SharedMediaActionView;

import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.List;

/**
 * Created by Andrew on 11.04.2015.
 */
public class SharedMediaFragment extends BaseFragment {
    public interface SharedImageDelegate
    {
        public void openPhoto(SharedImageModel img);
        public void enableSelectionMode();
        public void didSelectionCountChanged(int newCount);
    }
    public interface SharedAudioDelegate
    {
        public void requestPlayTrack(AudioTrackModel track);
        public void enableSelectionMode();
        public void didSelectionCountChanged(int newCount);
    }
    public enum SharedMediaType {IMAGES_AND_VIDEOS, AUDIO}
    private SharedMediaType sharedMediaType;
    private final String actionViewTag = "SharedMediaActionView";
    private SharedMediaActionViewController actionViewController;
    private SharedMediaViewController listController;
    private SharedImageDelegate imgDelegate;
    private SharedAudioDelegate audioDelegate;
    private boolean selectionMode = false;
    private BackPressListener backButtonInterceptor;

    private SharedMediaListView list;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getArguments() != null && getArguments().containsKey(Constants.ARG_SHOW_AUDIO))
            sharedMediaType = SharedMediaType.AUDIO;
        else
            sharedMediaType = SharedMediaType.IMAGES_AND_VIDEOS;
        imgDelegate = new SharedImageDelegate() {
            @Override
            public void openPhoto(SharedImageModel img) {
                openPhotoFragment(img);
            }

            @Override
            public void enableSelectionMode() {
                if(selectionMode)
                    return;
                selectionMode = true;
                if(actionViewController != null)
                    actionViewController.enableMultiSelection(true);
                this.didSelectionCountChanged(1);
            }

            @Override
            public void didSelectionCountChanged(int newCount) {
                if(actionViewController != null)
                    actionViewController.changeSelectionCount(newCount);
            }
        };
        audioDelegate = new SharedAudioDelegate() {
            @Override
            public void requestPlayTrack(AudioTrackModel track) {
                AudioPlaybackService playbackService = TelegramApplication.sharedApplication().getAudioPlaybackService();
                if(playbackService == null)
                    return;
                if(track.equals(playbackService.nowPlaying()))
                {
                    if(playbackService.isAudioPlaying())
                        playbackService.pausePlayback();
                    else
                        playbackService.unpausePlayback();
                    return;
                }
                playbackService.initializePlaylist(track, getArguments().getLong(Constants.ARG_CHAT_ID));
                playbackService.createPlaylistForChat(getArguments().getLong(Constants.ARG_CHAT_ID), track);
                ((TelegramMainActivity) getActivity()).showMiniPlayer(true);
            }

            @Override
            public void enableSelectionMode() {
                if(selectionMode)
                    return;
                selectionMode = true;
                if(actionViewController != null)
                    actionViewController.enableMultiSelection(true);
                this.didSelectionCountChanged(1);
            }

            @Override
            public void didSelectionCountChanged(int newCount) {
                if(actionViewController != null)
                    actionViewController.changeSelectionCount(newCount);
            }
        };
        backButtonInterceptor = new BackPressListener() {
            @Override
            public void onBackPressed() {
                disableSelectionMode();
            }
        };
    }
    @Override
    public void onResume()
    {
        super.onResume();
        ((TelegramMainActivity) getActivity()).hideActionViewShadow();
        if(listController != null)
            listController.onFragmentResumed();
    }
    @Override
    public void onPause()
    {
        super.onPause();
        if(selectionMode)
            ((TelegramMainActivity) getActivity()).removeBackPressInterceptor();
        if(listController != null)
            listController.onFragmentPaused();
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return new SharedMediaActionView(TelegramApplication.sharedApplication().getApplicationContext()) {
            @Override
            public Object getTag() {
                return actionViewTag;
            }
        };
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null) {
            actionViewController = new SharedMediaActionViewController();
        }
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        if(listController != null)
            listController.onUpdateServiceConnected(service);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        list = new SharedMediaListView(inflater.getContext());
        list.setLayoutParams(new ViewGroup.LayoutParams(container.getLayoutParams()));
        list.setVisibility(View.VISIBLE);
        if(listController == null) {
            listController = new SharedMediaViewController(list, getArguments().getLong(Constants.ARG_CHAT_ID));
            list.setController(listController);
        }
        else
        {
            list.setController(listController);
            listController.replaceView(list);
        }
        list.setDisplayMode(sharedMediaType);
        listController.setImageAdapterDelegate(imgDelegate);
        listController.setAudioAdapterDelegate(audioDelegate);
        return list;
    }
    public void openPhotoFragment(SharedImageModel model)
    {
        if(model.isVideo())
            return;
        ((TelegramMainActivity)getActivity()).enableViewPhotoMode(model.getOriginal().getPhoto(), "", (int) (model.getDate() / 1000));
        /*ViewPhotoFragment fr = new ViewPhotoFragment();
        fr.setOriginal(model.getOriginal());
        fr.setPreview(model.getPreview());
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction tr = fm.beginTransaction();
        tr.replace(R.id.container, fr, Constants.VIEW_PHOTO_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();*/
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(actionViewController != null)
        {
            if(selectionMode) {
                actionViewController.enableMultiSelection(false);
                SharedMediaViewController.MediaListAdapter adapter = listController.getAdapter(sharedMediaType);
                actionViewController.changeSelectionCount(adapter.getSelectionCount());
            }
            else
                actionViewController.disableMultiSelection(false);
        }
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(listController != null)
            listController.removeObservers();
        ((TelegramMainActivity) getActivity()).showActionViewShadow();
        ((TelegramMainActivity) getActivity()).removeBackPressInterceptor();
    }
    public void disableSelectionMode()
    {
        if(selectionMode)
        {
            selectionMode = false;
            if(actionViewController != null)
                actionViewController.disableMultiSelection(true);
            ((TelegramMainActivity) getActivity()).removeBackPressInterceptor();
            if(listController != null)
            {
                SharedMediaViewController.MediaListAdapter adapter = listController.getAdapter(sharedMediaType);
                adapter.disableSelectionMode();
            }
        }
    }
    private void forwardSelection()
    {
        if(listController == null)
            return;
        final int[] msgIds = listController.getAdapter(sharedMediaType).getSelectedMessages();
        final List<SharedMedia> models = listController.getAdapter(sharedMediaType).getSelectedModels();
        if(msgIds.length == 0 || models == null)
            return;
        final AdditionalActionForward fwdInfo = new AdditionalActionForwardSharedMedia(models, msgIds, getArguments().getLong(Constants.ARG_CHAT_ID));
        ForwardBottomSheetView.ForwardMenuDelegate fwdMenuDelegate = new ForwardBottomSheetView.ForwardMenuDelegate()
        {
            @Override
            public void onBottomSheetClose(boolean dismissed) {
                if(!dismissed)
                    disableSelectionMode();
            }

            @Override
            public AdditionalActionForward getForwardInfo() {
                if(listController == null)
                    return null;
                return fwdInfo;
            }

            @Override
            public long getCurrentChatId() {
                return getArguments().getLong(Constants.ARG_CHAT_ID);
            }

            @Override
            public void performSelfForward() {
                try
                {
                    if(fwdInfo == null)
                        return;
                    final AdditionalActionForward actualFwdInfoRef = fwdInfo;
                    TdApi.Chat chat = ChatCache.getInstance().getChatById(getArguments().getLong(Constants.ARG_CHAT_ID));
                    if(chat == null)
                        return;
                    boolean fromChannel = chat.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR && (((TdApi.ChannelChatInfo) chat.type).channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() || ((TdApi.ChannelChatInfo) chat.type).channel.role.getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor());
                    TdApi.ForwardMessages fwd = new TdApi.ForwardMessages(chat.id, chat.id, actualFwdInfoRef.getMessageIds(), fromChannel, false, false);
                    TG.getClientInstance().send(fwd, TelegramApplication.dummyResultHandler);
                }
                catch(Exception e) { e.printStackTrace(); }
            }

            @Override
            public void performSelfComment(TdApi.InputMessageText comment) {
                try
                {
                    if(fwdInfo == null)
                        return;
                    final AdditionalActionForward actualFwdInfoRef = fwdInfo;
                    TdApi.Chat chat = ChatCache.getInstance().getChatById(getArguments().getLong(Constants.ARG_CHAT_ID));
                    if(chat == null)
                        return;
                    boolean fromChannel = chat.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR && (((TdApi.ChannelChatInfo) chat.type).channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() || ((TdApi.ChannelChatInfo) chat.type).channel.role.getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor());
                    TdApi.SendMessage fwd = new TdApi.SendMessage(chat.id, 0, fromChannel, false, false, null, comment);
                    TG.getClientInstance().send(fwd, TelegramApplication.dummyResultHandler);
                }
                catch(Exception e) { e.printStackTrace(); }
            }

            @Override
            public void closeBottomSheet() {
                try
                {
                    ((TelegramMainActivity) getActivity()).slideDownBottomSheet();
                }
                catch(Exception ignored) {}
            }
        };
        try
        {
            ((TelegramMainActivity) getActivity()).showFwdBottomSheet(fwdMenuDelegate);
        }
        catch(Exception ignored) {}
    }
    private void removeSelection()
    {
        if(listController == null)
            return;
        int[] msgIds = listController.getAdapter(sharedMediaType).getSelectedMessages();
        if(msgIds.length == 0)
            return;
        disableSelectionMode();
        for(int id : msgIds)
        {
            if(sharedMediaType == SharedMediaType.AUDIO)
                listController.removeAudio(id);
            else
                listController.removeImage(id);
        }
        TdApi.DeleteMessages delFunc = new TdApi.DeleteMessages(getArguments().getLong(Constants.ARG_CHAT_ID), msgIds);
        TG.getClientInstance().send(delFunc, TelegramApplication.dummyResultHandler);
    }

    private class SharedMediaActionViewController extends ActionViewController
    {
        private ListPopupWindow listPopupWindow;
        private ListAdapter adapter;
        public SharedMediaActionViewController()
        {
            adapter = new MenuAdapter(getActivity(), new MenuItem[] {new MenuItem(R.id.action_imgs_and_videos, getResources().getString(R.string.shared_media)), new MenuItem(R.id.action_audio, getResources().getString(R.string.audio_files))});
        }
        @Override
        public void onFragmentPaused() {

        }

        @Override
        public void onFragmentResumed() {
            final SharedMediaActionView actionView = (SharedMediaActionView) getActionView();
            final String passcodeTypeString;


            if(sharedMediaType == SharedMediaType.AUDIO)
                passcodeTypeString = getResources().getString(R.string.audio_files);
            else
                passcodeTypeString = getResources().getString(R.string.shared_media);
            actionView.mediaTypeTextView.setText(passcodeTypeString);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listPopupWindow != null)
                        listPopupWindow.show();
                }
            };
            actionView.backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goBack();
                }
            });
            if(getArguments() != null && getArguments().containsKey(Constants.ARG_SHOW_AUDIO))
                actionView.mediaTypeMenu.setVisibility(View.GONE);
            else
            {
                listPopupWindow = new PopupListView(getActivity()) {
                    @Override
                    protected void didItemClicked(MenuItem item) {
                        if(getArguments() != null && getArguments().containsKey(Constants.ARG_SHOW_AUDIO))
                            return;
                        SharedMediaType newMediaType;
                        if(item.getId() == R.id.action_imgs_and_videos)
                            newMediaType = SharedMediaType.IMAGES_AND_VIDEOS;
                        else
                            newMediaType = SharedMediaType.AUDIO;
                        if(sharedMediaType != newMediaType)
                        {
                            sharedMediaType = newMediaType;
                            actionView.mediaTypeTextView.setText(item.getValue());
                            if(list != null)
                                list.setDisplayMode(sharedMediaType);
                        }
                    }
                };
                listPopupWindow.setHorizontalOffset(AndroidUtilities.dp(56));
                listPopupWindow.setVerticalOffset(-AndroidUtilities.dp(52));
                listPopupWindow.setAdapter(adapter);
                listPopupWindow.setAnchorView(actionView.defaultModeContainer);
                actionView.mediaTypeMenu.setVisibility(View.VISIBLE);
                actionView.mediaTypeMenu.setOnClickListener(clickListener);
                actionView.mediaTypeTextView.setOnClickListener(clickListener);
            }
            actionView.disableButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    disableSelectionMode();
                }
            });
            actionView.forwardButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    forwardSelection();
                }
            });
            actionView.delButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeSelection();
                }
            });
        }

        @Override
        public void onActionViewRemoved() {
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
        public void enableMultiSelection(boolean animate)
        {
            ((TelegramMainActivity) getActivity()).interceptBackPress(backButtonInterceptor);
            if(getActionView() != null)
                ((SharedMediaActionView)getActionView()).setMode(true, animate);
        }
        public void disableMultiSelection(boolean animate)
        {
            if(getActionView() != null)
                ((SharedMediaActionView)getActionView()).setMode(false, animate);
        }
        public void changeSelectionCount(int newCount)
        {
            if(getActionView() != null)
                ((SharedMediaActionView) getActionView()).mediaCountTextView.setText(String.valueOf(newCount));
        }
    }
}
