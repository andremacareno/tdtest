package com.andremacareno.tdtest.fragments;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.andremacareno.tdtest.AudioPlaybackService;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.viewcontrollers.PlayerViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.player.PlayerView;

/**
 * Created by Andrew on 11.04.2015.
 */
public class PlayerFragment extends BaseFragment {
    public interface PlayerFragmentDelegate
    {
        public void goBack();
        public void openPlaylist(long chatId);
    }
    private PlayerView player;
    private PlayerViewController playerController;
    private PlayerFragmentDelegate playerFragmentDelegate;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        playerFragmentDelegate = new PlayerFragmentDelegate() {
            @Override
            public void goBack() {
                PlayerFragment.this.goBack();
            }

            @Override
            public void openPlaylist(long chatId) {
                Fragment fr = new SharedMediaFragment();
                Bundle b = new Bundle();
                if(chatId == 0) {
                    Log.d("PlayerFragment", "missing chat id");
                    return;
                }
                ((TelegramMainActivity) getActivity()).showActionView();
                ((TelegramMainActivity) getActivity()).showMiniPlayer(false);
                b.putLong(Constants.ARG_CHAT_ID, chatId);
                b.putBoolean(Constants.ARG_SHOW_AUDIO, true);
                fr.setArguments(b);
                fr.setRetainInstance(true);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
                ft.replace(R.id.container, fr, Constants.PLAYLIST_FRAGMENT);
                ft.addToBackStack(null);
                ft.commitAllowingStateLoss();
                fm.executePendingTransactions();
            }
        };
    }
    @Override
    public void onResume()
    {
        super.onResume();
        ((TelegramMainActivity) getActivity()).hideActionView();
        ((TelegramMainActivity) getActivity()).hideMiniPlayer();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        return null;
    }

    @Override
    public ActionViewController getActionViewController() {
        return null;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        player = new PlayerView(container.getContext());
        player.setLayoutParams(new RelativeLayout.LayoutParams(container.getLayoutParams()));
        player.setVisibility(View.VISIBLE);
        return player;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AudioPlaybackService playbackService = TelegramApplication.sharedApplication().getAudioPlaybackService();
        if(playbackService == null)
            return;
        if(playerController == null) {
            if(player != null)
                playerController = new PlayerViewController(player);
        }
        else if(player != null)
            playerController.refreshView(player);
        if(playerController != null)
        {
            playerController.setFragmentDelegate(playerFragmentDelegate);
            playbackService.setPlaybackDelegate(playerController);
        }
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();

        ((TelegramMainActivity) getActivity()).showActionView();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
        AudioPlaybackService playbackService = TelegramApplication.sharedApplication().getAudioPlaybackService();
        if(playbackService != null) {
            playbackService.removePlaybackDelegate(playerController);
            if(playbackService.isAudioPlaying())
                ((TelegramMainActivity) getActivity()).showMiniPlayer(false);
        }
    }
}
