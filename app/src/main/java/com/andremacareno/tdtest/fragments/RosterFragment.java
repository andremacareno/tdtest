package com.andremacareno.tdtest.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.BackPressListener;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.DrawerArrowDrawable;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.models.ChannelChatModel;
import com.andremacareno.tdtest.models.ChatModel;
import com.andremacareno.tdtest.models.ChatSearchResult;
import com.andremacareno.tdtest.models.GlobalSearchResult;
import com.andremacareno.tdtest.models.GroupChatModel;
import com.andremacareno.tdtest.models.MessageSearchResult;
import com.andremacareno.tdtest.models.TelegramAction;
import com.andremacareno.tdtest.tasks.GetFullPeerTask;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.ConversationsListViewController;
import com.andremacareno.tdtest.viewcontrollers.GlobalSearchListController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.views.ConversationsListView;
import com.andremacareno.tdtest.views.EmptyListPlaceholder;
import com.andremacareno.tdtest.views.SearchResultListView;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.ChatListActionView;

import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 03/04/15.
 */
public class RosterFragment extends BaseFragment {
    public interface RosterFragmentDelegate
    {
        public void didChatSelected(ChatModel chat);
        public void didSearchResultSelected(GlobalSearchResult result);
        public void notifySearchResultsCount(int count);
    }
    private static final String TAG = "RosterFragment";
    private ConversationsListView roster;
    private SearchResultListView searchResults;
    private ConversationsListViewController listController;
    private GlobalSearchListController searchListController;
    private NotificationObserver navDrawerSelectObserver, validPasscodeObserver, passcodeSetObserver, passcodeUnsetObserver;
    private ChatListActionView actionView;
    private ChatListActionViewController actionViewController;
    private DrawerArrowDrawable arrowDrawable;
    private EmptyListPlaceholder noResultsPlaceholder;
    private boolean shouldUpdateActionView = false;
    private RosterFragmentDelegate delegate;
    private boolean searchMode = false;
    private float parameter = 0.0f;
    private static Handler flipHandler = new Handler(Looper.getMainLooper());
    private static Handler searchReqHandler = new Handler(Looper.getMainLooper());
    private BackPressListener searchModeBackPressInterceptor;
    private Runnable flipRunnable = new Runnable() {
        @Override
        public void run() {
            if(parameter < 0.0f) {
                parameter = 0.0f;
                return;
            }
            else if(parameter > 1.0f)
            {
                parameter = 1.0f;
                return;
            }
            arrowDrawable.setFlip(!searchMode);
            parameter += searchMode ? 0.05 : -0.05;
            if(arrowDrawable != null)
                arrowDrawable.setParameter(parameter);
            if(actionView != null)
                actionView.openMenuButton.setImageDrawable(arrowDrawable);
            flipHandler.postDelayed(flipRunnable, 10);
        }
    };
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        searchMode = false;
        searchModeBackPressInterceptor = new BackPressListener() {
            @Override
            public void onBackPressed() {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "onBackPressed");
                disableSearchMode();
            }
        };
        arrowDrawable = new DrawerArrowDrawable(getResources());
        delegate = new RosterFragmentDelegate() {
            @Override
            public void didChatSelected(final ChatModel chat) {
                if(CommonTools.isFullObjectPreloaded(chat.getPeer(), chat.getPeerType()))
                    launchChatFragment(chat);
                else
                {
                    GetFullPeerTask task = new GetFullPeerTask(chat.getPeer(), chat.getPeerType());
                    task.onComplete(new OnTaskCompleteListener() {
                        @Override
                        public void taskComplete(AsyncTaskManTask task) {
                            TelegramApplication.applicationHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    launchChatFragment(chat);
                                }
                            });
                        }
                    });
                    task.addToQueue();
                }
            }

            @Override
            public void didSearchResultSelected(GlobalSearchResult result) {
                if(result.getResultType() == GlobalSearchResult.ResultType.MESSAGE)
                {
                    launchChatFragment((MessageSearchResult) result);
                }
                else if(result.getResultType() == GlobalSearchResult.ResultType.CHAT_CONTACT || result.getResultType() == GlobalSearchResult.ResultType.GLOBAL) {
                    TG.getClientInstance().send(new TdApi.AddRecentlyFoundChat(((ChatSearchResult) result).getChatObj().id), TelegramApplication.dummyResultHandler);
                    launchChatFragment((ChatSearchResult) result);
                }
            }

            @Override
            public void notifySearchResultsCount(int count) {
                if(count == 0 && actionView != null && actionView.searchEditText.length() > 0)
                    noResultsPlaceholder.setVisibility(View.VISIBLE);
                else
                    noResultsPlaceholder.setVisibility(View.GONE);
            }
        };
        this.navDrawerSelectObserver = new NotificationObserver(NotificationCenter.didNavDrawerClosed) {
            @Override
            public void didNotificationReceived(Notification notification) {
                int action = (int) notification.getObject();
                if (action == TelegramAction.NAVDRAWER_LOG_OUT) {
                    performLogOut();
                }
                else if(action == TelegramAction.NAVDRAWER_CONTACTS)
                    openContacts();
                else if(action == TelegramAction.NAVDRAWER_NEW_GROUP)
                    launchNewChatFragment();
                else if(action == TelegramAction.NAVDRAWER_NEW_CHANNEL)
                    launchNewChannelFragment();
                else if(action == TelegramAction.NAVDRAWER_SETTINGS)
                    openSettings();
            }
        };
        validPasscodeObserver = new NotificationObserver(NotificationCenter.didPasscodeLockPassed) {
            @Override
            public void didNotificationReceived(Notification notification) {
                if(actionViewController != null)
                    actionViewController.passcodeLockPassed();
            }
        };
        passcodeSetObserver = new NotificationObserver(NotificationCenter.didPasscodeSet) {
            @Override
            public void didNotificationReceived(Notification notification) {
                shouldUpdateActionView = true;
            }
        };
        passcodeUnsetObserver = new NotificationObserver(NotificationCenter.didPasscodeUnset) {
            @Override
            public void didNotificationReceived(Notification notification) {
                shouldUpdateActionView = true;
            }
        };
        NotificationCenter.getInstance().addObserver(passcodeSetObserver);
        NotificationCenter.getInstance().addObserver(passcodeUnsetObserver);
        NotificationCenter.getInstance().addObserver(validPasscodeObserver);
    }
    @Override
    public void onResume()
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "onResume");
        super.onResume();
        /*if(fabMenu != null && newChat != null)
            fabMenu.addMenuButton(newChat);*/
        if(actionViewController != null)
            actionViewController.restoreMenuButtonClickAbility();
        if(BuildConfig.DEBUG)
            Log.d(TAG, String.format("passcodeLockShown = %s", String.valueOf(passcodeLockShown)));
        ((TelegramMainActivity) getActivity()).enableChatFam(!passcodeLockShown);
        ((TelegramMainActivity) getActivity()).enableNavDrawer();
        if(listController != null) {
            listController.onFragmentResumed();
            listController.setFragmentDelegate(delegate);
            searchListController.onFragmentResumed();
            searchListController.setFragmentDelegate(delegate);
        }
        NotificationCenter.getInstance().addObserver(navDrawerSelectObserver);
        if(shouldUpdateActionView && actionViewController != null)
            actionViewController.didPasscodeLockSettingsUpdated();
        if(searchMode)
            enableSearchMode();
    }
    @Override
    public void onPause()
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "onPause");
        super.onPause();
        if(searchMode && actionView != null)
            AndroidUtilities.hideKeyboard(actionView.searchEditText);
        ((TelegramMainActivity) getActivity()).disableNavDrawer();
        ((TelegramMainActivity) getActivity()).enableChatFam(false);
        if(actionViewController != null)
            actionViewController.disableMenuButtonClick();
        if(listController != null) {
            listController.onFragmentPaused();
            searchListController.onFragmentPaused();
        }
        NotificationCenter.getInstance().removeObserver(navDrawerSelectObserver);
    }
    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        if(actionView == null)
            actionView = new ChatListActionView(getActivity());

        return actionView;
    }
    @Override
    public ActionViewController getActionViewController()
    {
        if(actionViewController == null)
            actionViewController = new ChatListActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        if(listController == null) {
            listController = new ConversationsListViewController(roster);
            searchListController = new GlobalSearchListController(searchResults);
        }
        else
        {
            roster.setController(listController);
            listController.replaceView(roster);

            searchResults.setController(listController);
            searchListController.replaceView(searchResults);
        }
        listController.onUpdateServiceConnected(service);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FrameLayout layout = (FrameLayout) inflater.inflate(R.layout.roster_fragment, container, false);
        roster = (ConversationsListView) layout.findViewById(R.id.conversations);
        roster.addOnScrollListener(new RecyclerView.OnScrollListener() {
            private boolean lastDirectionUp = false;
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                try
                {
                    if(Math.abs(dy) < 20)
                        return;
                    boolean directionUp = dy < 0;
                    if(lastDirectionUp != directionUp && directionUp)
                    {
                        lastDirectionUp = true;
                        ((TelegramMainActivity) getActivity()).enableChatFam(true, true);
                    }
                    else if(!directionUp && lastDirectionUp)
                    {
                        lastDirectionUp = false;
                        ((TelegramMainActivity) getActivity()).enableChatFam(false, true);
                    }
                }
                catch(Exception ignored) {}
            }
        });
        searchResults = (SearchResultListView) layout.findViewById(R.id.search_results);
        noResultsPlaceholder = (EmptyListPlaceholder) layout.findViewById(R.id.no_results_placeholder);

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        lp.bottomMargin = lp.rightMargin = AndroidUtilities.dp(16);

        return layout;
    }

    @Override
    public void onDestroy()
    {
        if(BuildConfig.DEBUG)
            Log.d("RosterFragment", "onDestroy");
        super.onDestroy();
        if(listController != null) {
            listController.removeObservers();
            searchListController.removeObservers();
        }
        listController = null;
        searchListController = null;
        NotificationCenter.getInstance().removeObserver(passcodeUnsetObserver);
        NotificationCenter.getInstance().removeObserver(passcodeSetObserver);
        NotificationCenter.getInstance().removeObserver(validPasscodeObserver);
    }
    private void performLogOut()
    {
        TelegramApplication.sharedApplication().getUpdateService().didLogOutRequested();
        ((TelegramMainActivity) getActivity()).didLogOutRequested();
    }
    private void openContacts()
    {
        Fragment fr = new ContactsFragment();
        String tag = Constants.CONTACTS_FRAGMENT;
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction().replace(R.id.container, fr, tag).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void openSettings()
    {
        Fragment fr = new SettingsFragment();
        String tag = Constants.SETTINGS_FRAGMENT;
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction().replace(R.id.container, fr, tag).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void launchNewChatFragment() {
        Fragment fr = new NewGroupFragment();
        String tag = Constants.NEW_CHAT_FRAGMENT;
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction().replace(R.id.container, fr, tag).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void launchNewChannelFragment() {
        Fragment fr = new NewChannelFragment();
        String tag = Constants.NEW_CHANNEL_FRAGMENT;
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction().replace(R.id.container, fr, tag).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    private void launchChatFragment(ChatModel model)
    {
        if(model == null)
            return;
        try
        {
            disableSearchMode();
            Bundle args = new Bundle();
            args.putLong(Constants.ARG_CHAT_ID, model.getId());
            args.putInt(Constants.ARG_LAST_MSG_ID, model.getTopMessageId());
            args.putBoolean(Constants.ARG_CHAT_MUTED, model.isMuted());
            args.putBoolean(Constants.ARG_BOT, model.isBot());
            args.putInt(Constants.ARG_REPLY_MARKUP_MSG, model.getReplyMarkupMsgId());
            args.putInt(Constants.ARG_CHAT_PEER, model.getPeer());
            args.putBoolean(Constants.ARG_DELETED_ACCOUNT, model.isUnknownPeer());
            args.putInt(Constants.ARG_LAST_READ_INBOX, model.getLastReadInboxMessageId());
            if(model.isSupergroup())
                args.putInt(Constants.ARG_LAST_READ_OUTBOX, 2000000000);
            else
                args.putInt(Constants.ARG_LAST_READ_OUTBOX, model.getLastReadOutboxMessageId());
            args.putInt(Constants.ARG_UNREAD_COUNT, model.getUnreadCount());
            args.putInt(Constants.ARG_START_MESSAGE, model.getTopMessageId());
            args.putString(Constants.ARG_CHAT_TITLE, model.getDisplayName());
            Fragment fr;
            if(!model.isGroupChat() && !model.isChannel()) {
                args.putInt(Constants.ARG_USER_ID, model.getPeer());
                fr = new PrivateChatFragment();
            }
            else if(model.isChannel() && !model.isSupergroup())
            {
                args.putInt(Constants.ARG_CHANNEL_ID, model.getPeer());
                fr = new ChannelChatFragment();
            }
            else if(model.isGroupChat())
            {
                args.putInt(Constants.ARG_GROUP_ID, model.getPeer());
                args.putBoolean(Constants.ARG_CHAT_LEFT, ((GroupChatModel) model).isChatLeft());
                fr = new GroupChatFragment();
            }
            else if(model.isSupergroup())
            {
                args.putInt(Constants.ARG_SUPERGROUP_ID, model.getPeer());
                args.putBoolean(Constants.ARG_CHAT_LEFT, ((ChannelChatModel) model).isLeft());
                fr = new GroupChatFragment();
            }
            else
                return;
            fr.setRetainInstance(true);
            fr.setArguments(args);
            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
            ft.replace(R.id.container, fr, Constants.CHAT_FRAGMENT);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
            fm.executePendingTransactions();
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    private void launchChatFragment(ChatSearchResult result)
    {
        if(result == null)
            return;
        try
        {
            ((TelegramMainActivity) getActivity()).removeBackPressInterceptor();
            TdApi.Chat chat = result.getChatObj();
            if(chat == null)
                return;
            Bundle args = new Bundle();
            args.putBoolean(Constants.ARG_FROM_SEARCH, true);
            if(chat != null)
                args.putInt(Constants.ARG_LAST_MSG_ID, chat.topMessage.id);
            args.putLong(Constants.ARG_CHAT_ID, chat.id);
            args.putBoolean(Constants.ARG_CHAT_MUTED, chat.notificationSettings.muteFor > 0);
            args.putBoolean(Constants.ARG_BOT, chat.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR ? ((TdApi.PrivateChatInfo) chat.type).user.type.getConstructor() == TdApi.UserTypeBot.CONSTRUCTOR : false);
            args.putInt(Constants.ARG_REPLY_MARKUP_MSG, chat.replyMarkupMessageId);
            args.putBoolean(Constants.ARG_DELETED_ACCOUNT, chat.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR && CommonTools.isDeletedUser(((TdApi.PrivateChatInfo) chat.type).user));
            args.putInt(Constants.ARG_LAST_READ_INBOX, chat.lastReadInboxMessageId);
            args.putInt(Constants.ARG_LAST_READ_OUTBOX, chat.lastReadOutboxMessageId);
            args.putInt(Constants.ARG_UNREAD_COUNT, chat.unreadCount);
            if(chat.topMessage != null)
                args.putInt(Constants.ARG_START_MESSAGE, chat.topMessage.id);
            args.putString(Constants.ARG_CHAT_TITLE, chat.title);
            Fragment fr;
            if(chat.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR) {
                TdApi.User user = ((TdApi.PrivateChatInfo) chat.type).user;
                args.putInt(Constants.ARG_USER_ID, user.id);
                fr = new PrivateChatFragment();
            }
            else if(chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR)
            {
                TdApi.Channel channel = ((TdApi.ChannelChatInfo) chat.type).channel;
                if(channel.isSupergroup)
                {
                    args.putInt(Constants.ARG_SUPERGROUP_ID, channel.id);
                    args.putBoolean(Constants.ARG_CHAT_LEFT, channel.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor() || channel.role.getConstructor() == Constants.PARTICIPANT_KICKED.getConstructor());
                    fr = new GroupChatFragment();
                }
                else
                {
                    args.putInt(Constants.ARG_CHANNEL_ID, channel.id);
                    fr = new ChannelChatFragment();
                }
            }
            else if(chat.type.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR)
            {
                TdApi.Group group = ((TdApi.GroupChatInfo) chat.type).group;
                args.putInt(Constants.ARG_GROUP_ID, group.id);
                args.putBoolean(Constants.ARG_CHAT_LEFT, group.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor() || group.role.getConstructor() == Constants.PARTICIPANT_KICKED.getConstructor());
                fr = new GroupChatFragment();
            }
            else
                return;
            fr.setRetainInstance(true);
            fr.setArguments(args);
            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
            ft.replace(R.id.container, fr, Constants.CHAT_FRAGMENT);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
            fm.executePendingTransactions();
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    private void launchChatFragment(MessageSearchResult result)
    {
        if(result == null)
            return;
        try
        {
            ((TelegramMainActivity) getActivity()).removeBackPressInterceptor();
            TdApi.Chat chat = ChatCache.getInstance().getChatById(result.getChatId());
            Bundle args = new Bundle();
            args.putBoolean(Constants.ARG_FROM_SEARCH, true);
            if(chat != null)
                args.putInt(Constants.ARG_LAST_MSG_ID, chat.topMessage.id);
            args.putLong(Constants.ARG_CHAT_ID, result.getChatId());
            args.putBoolean(Constants.ARG_CHAT_MUTED, chat.notificationSettings.muteFor > 0);
            args.putBoolean(Constants.ARG_BOT, chat.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR ? ((TdApi.PrivateChatInfo) chat.type).user.type.getConstructor() == TdApi.UserTypeBot.CONSTRUCTOR : false);
            args.putInt(Constants.ARG_REPLY_MARKUP_MSG, chat.replyMarkupMessageId);
            args.putBoolean(Constants.ARG_DELETED_ACCOUNT, chat.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR && CommonTools.isDeletedUser(((TdApi.PrivateChatInfo) chat.type).user));
            args.putInt(Constants.ARG_LAST_READ_INBOX, chat.lastReadInboxMessageId);
            args.putInt(Constants.ARG_LAST_READ_OUTBOX, chat.lastReadOutboxMessageId);
            args.putInt(Constants.ARG_UNREAD_COUNT, chat.unreadCount);
            args.putInt(Constants.ARG_START_MESSAGE, result.getMessageId());
            args.putString(Constants.ARG_CHAT_TITLE, chat.title);
            Fragment fr;
            if(chat.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR) {
                TdApi.User user = ((TdApi.PrivateChatInfo) chat.type).user;
                args.putInt(Constants.ARG_USER_ID, user.id);
                fr = new PrivateChatFragment();
            }
            else if(chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR)
            {
                TdApi.Channel channel = ((TdApi.ChannelChatInfo) chat.type).channel;
                if(channel.isSupergroup)
                {
                    args.putInt(Constants.ARG_SUPERGROUP_ID, channel.id);
                    args.putBoolean(Constants.ARG_CHAT_LEFT, channel.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor() || channel.role.getConstructor() == Constants.PARTICIPANT_KICKED.getConstructor());
                    fr = new GroupChatFragment();
                }
                else
                {
                    args.putInt(Constants.ARG_CHANNEL_ID, channel.id);
                    fr = new ChannelChatFragment();
                }
            }
            else if(chat.type.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR)
            {
                TdApi.Group group = ((TdApi.GroupChatInfo) chat.type).group;
                args.putInt(Constants.ARG_GROUP_ID, group.id);
                args.putBoolean(Constants.ARG_CHAT_LEFT, group.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor() || group.role.getConstructor() == Constants.PARTICIPANT_KICKED.getConstructor());
                fr = new GroupChatFragment();
            }
            else
                return;
            fr.setRetainInstance(true);
            fr.setArguments(args);
            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
            ft.replace(R.id.container, fr, Constants.CHAT_FRAGMENT);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
            fm.executePendingTransactions();
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    @Override
    protected void onPasscodeLockShown()
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "screen locked");
        try
        {
            ((TelegramMainActivity) getActivity()).enableChatFam(false);
        }
        catch(Exception ignored) {}
    }
    @Override
    protected void onPasscodeLockPassed()
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "passed");
        try
        {
            ((TelegramMainActivity) getActivity()).enableChatFam(true);
        }
        catch(Exception ignored) {}
    }
    private void enableSearchMode()
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "enable chatSearch mode");
        if(searchMode == false)
        {
            flipHandler.removeCallbacksAndMessages(null);
            parameter = 0.0f;
            flipHandler.post(flipRunnable);
        }
        searchMode = true;
        if(actionView == null) {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "return");
            return;
        }
        roster.setVisibility(View.GONE);
        searchResults.setVisibility(View.VISIBLE);
        actionView.lockButton.setVisibility(View.GONE);
        actionView.connectionStateText.setVisibility(View.GONE);
        actionView.searchButton.setVisibility(View.GONE);
        if(actionView.searchEditText.length() > 1)
            actionView.clearSearchButton.setVisibility(View.VISIBLE);
        actionView.searchEditText.setVisibility(View.VISIBLE);
        ((TelegramMainActivity) getActivity()).disableMenuButtonProcessing(true);
        ((TelegramMainActivity) getActivity()).enableChatFam(false);
        ((TelegramMainActivity) getActivity()).interceptBackPress(searchModeBackPressInterceptor);
        actionView.searchEditText.requestFocus();
        AndroidUtilities.showKeyboard(actionView.searchEditText);
        searchReqHandler.removeCallbacksAndMessages(null);
        searchReqHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                searchListController.newQuery(actionView.searchEditText.getText().toString());
            }
        }, 600);
    }
    private void disableSearchMode()
    {
        if(searchMode)
        {
            flipHandler.removeCallbacksAndMessages(null);
            parameter = 1.0f;
            flipHandler.post(flipRunnable);
        }
        searchMode = false;
        if(actionView == null || actionViewController == null) {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "return");
            return;
        }
        ((TelegramMainActivity) getActivity()).enableChatFam(true);
        AndroidUtilities.hideKeyboard(actionView.searchEditText);
        actionView.lockButton.setVisibility(actionViewController.passcodeEnabled ? View.VISIBLE : View.GONE);
        actionView.connectionStateText.setVisibility(View.VISIBLE);
        actionView.searchButton.setVisibility(View.VISIBLE);
        actionView.searchEditText.setVisibility(View.GONE);
        actionView.searchEditText.setText("");
        searchListController.clearData();
        actionView.clearSearchButton.setVisibility(View.GONE);
        roster.setVisibility(View.VISIBLE);
        searchResults.setVisibility(View.GONE);
        ((TelegramMainActivity) getActivity()).disableMenuButtonProcessing(false);
        ((TelegramMainActivity) getActivity()).removeBackPressInterceptor();
    }

    private class ChatListActionViewController extends ActionViewController {
        private NotificationObserver connectionLostObserver, connectionEstablishedObserver;
        private boolean hasNetworkConnection;
        public boolean passcodeEnabled;
        private boolean lockButtonState;
        private View.OnClickListener navDrawerButtonListener;
        private View.OnClickListener passcodeLockButtonClickListener;
        private View.OnClickListener searchButtonClickListener;
        private View.OnClickListener clearSearchClickListener;
        private TextWatcher searchQueryTextWatcher;

        public ChatListActionViewController() {
            init();
        }

        @Override
        public void onFragmentPaused() {
            NotificationCenter.getInstance().removeObserver(this.connectionEstablishedObserver);
            NotificationCenter.getInstance().removeObserver(this.connectionLostObserver);
        }

        @Override
        public void onFragmentResumed() {

            ChatListActionView av = (ChatListActionView) getActionView();
            if(av != null)
            {
                arrowDrawable.setStrokeColor(ContextCompat.getColor(getContext(), R.color.white));
                av.openMenuButton.setImageDrawable(arrowDrawable);
            }
            NotificationCenter.getInstance().addObserver(this.connectionEstablishedObserver);
            NotificationCenter.getInstance().addObserver(this.connectionLostObserver);
            av.openMenuButton.setOnClickListener(navDrawerButtonListener);
            av.lockButton.setOnClickListener(passcodeLockButtonClickListener);
            av.searchButton.setOnClickListener(searchButtonClickListener);
            av.clearSearchButton.setOnClickListener(clearSearchClickListener);
            av.searchEditText.addTextChangedListener(searchQueryTextWatcher);
            updateConnectionStatus();
            updatePasscodeLockIcon();

        }

        @Override
        public void onActionViewRemoved() {
            NotificationCenter.getInstance().removeObserver(this.connectionEstablishedObserver);
            NotificationCenter.getInstance().removeObserver(this.connectionLostObserver);
        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
        public void disableMenuButtonClick()
        {
            if(getActionView() != null) {
                ((ChatListActionView) getActionView()).openMenuButton.setClickable(false);
                ((ChatListActionView) getActionView()).openMenuButton.setOnClickListener(null);
            }
        }
        public void restoreMenuButtonClickAbility()
        {
            if(getActionView() != null) {
                ((ChatListActionView) getActionView()).openMenuButton.setClickable(true);
                ((ChatListActionView) getActionView()).openMenuButton.setOnClickListener(navDrawerButtonListener);
            }
        }
        private void init()
        {
            passcodeEnabled = TelegramApplication.sharedApplication().sharedPreferences().getBoolean(Constants.PREFS_PASSCODE_LOCK_ENABLED, false);
            lockButtonState = TelegramApplication.sharedApplication().sharedPreferences().getBoolean(Constants.PREFS_LOCK_APP_NEXT_TIME, false);
            hasNetworkConnection = TelegramApplication.sharedApplication().hasConnection();
            clearSearchClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(getActionView() != null)
                        ((ChatListActionView) getActionView()).searchEditText.setText("");
                }
            };
            searchQueryTextWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable query) {
                    if(!searchMode)
                        return;
                    if(getActionView() != null)
                        ((ChatListActionView) getActionView()).clearSearchButton.setVisibility(query.length() > 0 ? View.VISIBLE : View.GONE);
                    if(searchListController != null) {
                        String q = query.toString();
                        if(!q.startsWith(" ") && !q.endsWith(" "))
                            searchListController.newQuery(q);
                    }
                }
            };
            this.connectionLostObserver = new NotificationObserver(NotificationCenter.didNetworkConnectionLost) {
                @Override
                public void didNotificationReceived(Notification notification) {
                    hasNetworkConnection = false;
                    updateConnectionStatus();
                }
            };
            this.connectionEstablishedObserver = new NotificationObserver(NotificationCenter.didNetworkConnectionEstablished) {
                @Override
                public void didNotificationReceived(Notification notification) {
                    hasNetworkConnection = true;
                    updateConnectionStatus();
                }
            };
            this.navDrawerButtonListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(searchMode)
                        disableSearchMode();
                    else
                        ((TelegramMainActivity)getActivity()).openNavigationDrawer();
                }
            };
            this.passcodeLockButtonClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lockButtonState = !lockButtonState;
                    SharedPreferences prefs = TelegramApplication.sharedApplication().sharedPreferences();
                    SharedPreferences.Editor prefEdit = prefs.edit();
                    prefEdit.putBoolean(Constants.PREFS_LOCK_APP_NEXT_TIME, lockButtonState);
                    prefEdit.apply();
                    updatePasscodeLockIcon();
                }
            };
            this.searchButtonClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(BuildConfig.DEBUG)
                        Log.d(TAG, "enable chatSearch mode");
                    enableSearchMode();
                }
            };
        }
        private void updateConnectionStatus()
        {
            if(hasNetworkConnection)
                ((ChatListActionView) getActionView()).connectionStateText.setText(R.string.messages);
            else
                ((ChatListActionView) getActionView()).connectionStateText.setText(R.string.waiting_for_connection);
        }
        private void updatePasscodeLockIcon()
        {
            if(getActionView() == null)
                return;
            if(searchMode)
            {
                if(lockButtonState == true)
                    ((ChatListActionView)getActionView()).lockButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_close));
                else
                    ((ChatListActionView)getActionView()).lockButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_open));
                return;
            }
            if(!passcodeEnabled)
                ((ChatListActionView)getActionView()).lockButton.setVisibility(View.GONE);
            else
            {
                if(lockButtonState == true)
                    ((ChatListActionView)getActionView()).lockButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_close));
                else
                    ((ChatListActionView)getActionView()).lockButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_open));
                ((ChatListActionView)getActionView()).lockButton.setVisibility(View.VISIBLE);
            }
        }
        public void passcodeLockPassed()
        {
            lockButtonState = false;
            updatePasscodeLockIcon();
        }
        public void didPasscodeLockSettingsUpdated()
        {
            passcodeEnabled = TelegramApplication.sharedApplication().sharedPreferences().getBoolean(Constants.PREFS_PASSCODE_LOCK_ENABLED, false);
            lockButtonState = TelegramApplication.sharedApplication().sharedPreferences().getBoolean(Constants.PREFS_LOCK_APP_NEXT_TIME, false);
            updatePasscodeLockIcon();
        }
    }
}
