package com.andremacareno.tdtest.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.andremacareno.tdtest.ChannelCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.GroupCache;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.models.CurrentUserModel;
import com.andremacareno.tdtest.updatelisteners.UpdateChannelProcessor;
import com.andremacareno.tdtest.updatelisteners.UpdateChatProcessor;
import com.andremacareno.tdtest.viewcontrollers.BotCmdListController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ChannelChatActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.GroupChatActionViewController;
import com.andremacareno.tdtest.views.ComposeMessagePlaceholder;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.ChatActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 17/04/15.
 */
public class GroupChatFragment extends BaseChatFragment {
    private PopupMenu contextMenu = null;
    private ChatActionView actionView = null;
    private ActionViewController actionViewController;
    private int groupId = 0;
    private boolean isSupergroup = false;
    private TdApi.GroupFull fullChatInfo = null;
    private TdApi.ChannelFull fullSupergroupInfo = null;
    private UpdateChatProcessor updateChatProcessor;
    private UpdateChannelProcessor updateChannelProcessor;
    private PauseHandler supergroupBotListHandler;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getArguments() == null)
            return;
        supergroupBotListHandler = new SupergroupBotsHandler(this);
        if(getArguments().containsKey(Constants.ARG_SUPERGROUP_ID))
        {
            this.isSupergroup = true;
            this.groupId = getArguments().getInt(Constants.ARG_SUPERGROUP_ID, 0);
            this.fullSupergroupInfo = ChannelCache.getInstance().getChannelFull(groupId);
        }
        else
        {
            this.isSupergroup = false;
            this.groupId = getArguments().getInt(Constants.ARG_GROUP_ID, 0);
            this.fullChatInfo = GroupCache.getInstance().getGroupFull(groupId, false);
        }
    }
    @Override
    public ComposeMessagePlaceholder.Reason displayComposeMessageView() {
        if(groupId == 0 && getArguments() != null) {
            if(getArguments() != null && !getArguments().getBoolean(Constants.ARG_CHAT_LEFT, false))
                return ComposeMessagePlaceholder.Reason.NO_REASON;
        }
        else if(fullChatInfo != null) {
            if(fullChatInfo.group.role.getConstructor() != Constants.PARTICIPANT_LEFT.getConstructor() && fullChatInfo.group.role.getConstructor() != Constants.PARTICIPANT_KICKED.getConstructor())
                return ComposeMessagePlaceholder.Reason.NO_REASON;
        }
        else if(fullSupergroupInfo != null)
        {
            if(fullSupergroupInfo.channel.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor())
                return ComposeMessagePlaceholder.Reason.CHANNEL_NOT_FOLLOWING;
            if(fullSupergroupInfo.channel.role.getConstructor() != Constants.PARTICIPANT_KICKED.getConstructor())
                return ComposeMessagePlaceholder.Reason.NO_REASON;
        }
        return ComposeMessagePlaceholder.Reason.KICKED_FROM_CHAT;
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }

    @Override
    public ActionView getActionView() {
        if(actionView == null)
        {
            if(isSupergroup)
            {
                actionView = new ChatActionView(getActivity());
                actionView.setBackButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeKeyboard();
                        goBack();
                    }
                });
                actionView.setMiddlePartClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openChannelProfile(groupId, getArguments().getLong(Constants.ARG_CHAT_ID));
                    }
                });
            }
            else
            {
                actionView = new ChatActionView(getActivity());
                actionView.setBackButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeKeyboard();
                        goBack();
                    }
                });
                actionView.setMiddlePartClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (fullChatInfo != null && ((fullChatInfo.group.role.getConstructor() != Constants.PARTICIPANT_LEFT.getConstructor() && fullChatInfo.group.role.getConstructor() != Constants.PARTICIPANT_KICKED.getConstructor())))
                            openGroupChatInfo(groupId);
                    }
                });
            }
            actionView.setClearSelectionClickListener(clearSelection);
            actionView.setDelButtonClickListener(delSelection);
            actionView.setShareButtonClickListener(fwdSelection);
        }
        return actionView;
    }
    @Override
    public void onResume()
    {
        super.onResume();
        if(fullChatInfo == null && fullSupergroupInfo == null) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.action_confirmation)
                    .setMessage(R.string.error_receiving_chat_info)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            goBack();
                        }

                    })
                    .setCancelable(false)
                    .show();
        }
        if(updateChatProcessor != null)
            updateChatProcessor.performResumeObservers();
        if(updateChannelProcessor != null)
            updateChannelProcessor.performResumeObservers();
    }
    @Override
    public void onPause()
    {
        super.onPause();
        if(supergroupBotListHandler != null)
            supergroupBotListHandler.pause();
        if(updateChatProcessor != null)
            updateChatProcessor.performPauseObservers();
        if(updateChannelProcessor != null)
            updateChannelProcessor.performPauseObservers();
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(supergroupBotListHandler != null)
            supergroupBotListHandler.resume();
        if(updateChatProcessor != null)
            updateChatProcessor.removeObservers();
        if(updateChannelProcessor != null)
            updateChannelProcessor.removeObservers();
        if(isSupergroup && fullSupergroupInfo != null)
            ChannelCache.getInstance().removeChannelFull(fullSupergroupInfo.channel.id);
    }
    @Override
    protected void enableSearchMode()
    {
        if(actionView == null)
            return;
        searchMode = true;
        actionView.setDelegate(chatSearchDelegate);
        actionView.setSearchMode(true);
        ((TelegramMainActivity) getActivity()).interceptBackPress(searchModeBackPress);
    }
    @Override
    protected void searchHashtag(String hashtag) {
        enableSearchMode();
        actionView.searchEditText.setText(hashtag);
        if(hashtag.length() > 1)
            chatSearchDelegate.didQuerySubmit(hashtag);
    }
    @Override
    protected void disableSearchMode()
    {
        if(actionView == null)
            return;
        searchMode = false;
        actionView.setSearchMode(false);
        ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(searchModeBackPress);
    }
    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null && getArguments() != null) {
            if(isSupergroup)
            {
                actionViewController = new ChannelChatActionViewController();
                ((ChannelChatActionViewController) actionViewController).bindChatInfo(getArguments().getLong(Constants.ARG_CHAT_ID, 0));
            }
            else
            {
                actionViewController = new GroupChatActionViewController();
                ((GroupChatActionViewController) actionViewController).bindChatInfo(groupId);
            }
            initBotCmdListController();
            initUpdateProcessor();
            updateComposeMessageViewVisibility();
        }
        return actionViewController;
    }
    @Override
    protected void disableSelectionMode() {
        if(actionView != null)
            actionView.setSelectionMode(false, true);
    }

    @Override
    protected void onSelectionCountUpdate(int newCount) {
        if(actionView != null && newCount >= 1)
        {
            actionView.changeSelectionCount(newCount);
            actionView.setSelectionMode(true, !selectionMode);
        }
    }
    private void initUpdateProcessor()
    {
        if(updateChatProcessor == null)
        {
            if(getArguments() == null)
                return;
            this.updateChatProcessor = new UpdateChatProcessor() {
                @Override
                protected String getKeyByUpdateConstructor(int constructor) {
                    return String.format("groupchatscreen%d_constructor%d_observer", groupId, constructor);
                }

                @Override
                protected boolean shouldObserveUpdateGroup() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateTitle() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateReplyMarkup() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateReadInbox() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateReadOutbox() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateChat() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateChatPhoto() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateChatOrder() {
                    return false;
                }

                @Override
                protected void processUpdateInBackground(TdApi.Update upd) {
                }

                @Override
                protected void processUpdateInUiThread(TdApi.Update upd) {
                    try
                    {
                        if(!isSupergroup)
                        {
                            if(upd.getConstructor() == TdApi.UpdateGroup.CONSTRUCTOR && ((TdApi.UpdateGroup) upd).group.id == fullChatInfo.group.id)
                            {
                                fullChatInfo.group = ((TdApi.UpdateGroup) upd).group;
                                updateComposeMessageViewVisibility();
                                ((GroupChatActionViewController) actionViewController).updateMembersCount();
                            }
                        }
                        if(upd.getConstructor() == TdApi.UpdateChatPhoto.CONSTRUCTOR && ((TdApi.UpdateChatPhoto) upd).chatId == getArguments().getLong(Constants.ARG_CHAT_ID)) {
                            TdApi.UpdateChatPhoto castUpd = (TdApi.UpdateChatPhoto) upd;
                            if(isSupergroup)
                                ((ChannelChatActionViewController) actionViewController).updatePhoto(CommonTools.isLowDPIScreen() ? castUpd.photo.small : castUpd.photo.big);
                            else
                                ((GroupChatActionViewController) actionViewController).updatePhoto(CommonTools.isLowDPIScreen() ? castUpd.photo.small : castUpd.photo.big);
                        }
                        else if(upd.getConstructor() == TdApi.UpdateChatTitle.CONSTRUCTOR && ((TdApi.UpdateChatTitle) upd).chatId == getArguments().getLong(Constants.ARG_CHAT_ID)) {
                            TdApi.UpdateChatTitle castUpd = (TdApi.UpdateChatTitle) upd;
                            if(isSupergroup)
                                ((ChannelChatActionViewController) actionViewController).updateTitle(castUpd.title);
                            else
                                ((GroupChatActionViewController) actionViewController).updateTitle(castUpd.title);
                        }
                    }
                    catch(Exception ignored) {}
                }
            };
            TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
            if(service != null)
                updateChatProcessor.attachObservers(service);
            if(isSupergroup && fullSupergroupInfo != null)
            {
                this.updateChannelProcessor = new UpdateChannelProcessor() {
                    @Override
                    protected String getKeyByUpdateConstructor(int constructor) {
                        return String.format("supergroup_%d_%d_%d_observer", fullSupergroupInfo.channel.id, getArguments().getLong(Constants.ARG_CHAT_ID), constructor);
                    }

                    @Override
                    protected void processUpdateInBackground(TdApi.Update upd) {

                    }

                    @Override
                    protected void processUpdateInUiThread(TdApi.Update upd) {
                        try
                        {
                            if(upd == null || upd.getConstructor() != TdApi.UpdateChannel.CONSTRUCTOR)
                                return;
                            TdApi.UpdateChannel cast = (TdApi.UpdateChannel) upd;
                            if(cast.channel.id != fullSupergroupInfo.channel.id)
                                return;
                            fullSupergroupInfo.channel = cast.channel;
                            updateComposeMessageViewVisibility();
                        }
                        catch(Exception ignored) {}
                    }
                };
                if(service != null)
                    updateChannelProcessor.attachObservers(service);
            }
        }
    }

    @Override
    protected void initBotCmdListController() {
        if(!isSupergroup && fullChatInfo != null)
            this.botCommandListController = new BotCmdListController(botCmdListView, botKeyboard, fullChatInfo.participants, getArguments().getLong(Constants.ARG_CHAT_ID), replyMarkupMessageId, this.cmdListControllerDelegate);
        else if(isSupergroup && fullSupergroupInfo != null)
        {
            if(fullSupergroupInfo.canGetParticipants)
            {
                TdApi.GetChannelParticipants getBots = new TdApi.GetChannelParticipants(groupId, new TdApi.ChannelParticipantsBots(), 0, 200);
                TG.getClientInstance().send(getBots, new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        try
                        {
                            if(object.getConstructor() != TdApi.ChatParticipants.CONSTRUCTOR)
                                return;
                            Message msg = Message.obtain(supergroupBotListHandler);
                            msg.obj = object;
                            msg.sendToTarget();
                        }
                        catch(Exception ignored) {}
                    }
                });
            }
        }
    }

    @Override
    protected boolean isPrivateChat() {
        return false;
    }

    @Override
    protected void changeDeleteIconVisibilityInSelectionBar(boolean canDelete) {
        if(actionView != null)
            actionView.setDelButtonVisibility(canDelete);
    }

    @Override
    protected boolean shouldSendTyping() {
        return fullChatInfo != null && CommonTools.getGroupOnlineCount(fullChatInfo.participants) > 1;
    }
    @Override
    protected void updateContextMenu()
    {
        if(actionView == null)
            return;
        if(contextMenu == null) {
            contextMenu = new PopupMenu(getActivity(), actionView.menuButton);
            contextMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    if (chatDelegate != null) {
                        chatDelegate.didContextMenuItemClicked(menuItem.getItemId());
                        return true;
                    }
                    return false;
                }
            });
        }
        else if(contextMenu.getMenu() != null)
            contextMenu.getMenu().clear();
        MenuInflater inflater = contextMenu.getMenuInflater();
        try
        {
            boolean isAdmin = fullChatInfo != null && fullChatInfo.adminId == CurrentUserModel.getInstance().getUserModel().getId();
            if(!isAdmin)
                inflater.inflate(muted ? R.menu.group_chat_muted : R.menu.group_chat_unmuted, contextMenu.getMenu());
            else
                inflater.inflate(muted ? R.menu.group_chat_muted_admin : R.menu.group_chat_unmuted_admin, contextMenu.getMenu());
        }
        catch(Exception ignored) {}
        actionView.setMenu(contextMenu);
    }
    @Override
    protected boolean canDeleteIncomingMessages() {
        if(fullChatInfo != null || (getArguments() != null && getArguments().containsKey(Constants.ARG_GROUP_ID)))
            return true;
        else if(fullSupergroupInfo != null)
            return fullSupergroupInfo.channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() || fullSupergroupInfo.channel.role.getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor() || fullSupergroupInfo.channel.role.getConstructor() == Constants.PARTICIPANT_MODERATOR.getConstructor();
        return false;
    }

    static class SupergroupBotsHandler extends PauseHandler
    {
        protected WeakReference<GroupChatFragment> fragmentRef;
        public SupergroupBotsHandler(GroupChatFragment fragment)
        {
            this.fragmentRef = new WeakReference<GroupChatFragment>(fragment);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            try
            {
                TdApi.ChatParticipants participants = (TdApi.ChatParticipants) message.obj;
                fragmentRef.get().botCommandListController = new BotCmdListController(fragmentRef.get().botCmdListView, fragmentRef.get().botKeyboard, participants.participants, fragmentRef.get().getArguments().getLong(Constants.ARG_CHAT_ID), fragmentRef.get().replyMarkupMessageId, fragmentRef.get().cmdListControllerDelegate);
                fragmentRef.get().onLateBotCmdListControllerInit();
            }
            catch(Exception ignored) {}
        }
    }

}
