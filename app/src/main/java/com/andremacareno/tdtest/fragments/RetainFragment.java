package com.andremacareno.tdtest.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.andremacareno.tdtest.viewcontrollers.FwdChatsController;
import com.andremacareno.tdtest.views.ForwardBottomSheetView;

//caching fragment for some parts of TelegramMainActivity
public class RetainFragment extends Fragment {
    private static final String TAG = "RetainFragment";
    public FwdChatsController fwdChatsController = null;
    public ForwardBottomSheetView.ForwardMenuDelegate fwdDelegate = null;

    public RetainFragment() {}

    public static RetainFragment findOrCreateRetainFragment(FragmentManager fm) {
        RetainFragment fragment = (RetainFragment) fm.findFragmentByTag(TAG);
        if (fragment == null) {
            fragment = new RetainFragment();
            fm.beginTransaction().add(fragment, TAG).commit();
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}