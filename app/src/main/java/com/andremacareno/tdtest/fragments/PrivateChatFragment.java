package com.andremacareno.tdtest.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.UserCache;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.updatelisteners.UpdateChatProcessor;
import com.andremacareno.tdtest.updatelisteners.UpdateUserProcessor;
import com.andremacareno.tdtest.viewcontrollers.BotCmdListController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.viewcontrollers.actionview.PrivateChatActionViewController;
import com.andremacareno.tdtest.views.ComposeMessagePlaceholder;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.ChatActionView;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 17/04/15.
 */
public class PrivateChatFragment extends BaseChatFragment {
    private PrivateChatActionViewController actionViewController;
    private PopupMenu contextMenu = null;
    private UserModel fullUserInfo = null;
    private UpdateChatProcessor updateChatProcessor;
    private UpdateUserProcessor updateUserProcessor;
    private ChatActionView actionView = null;
    private long chatId;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getArguments() != null && getArguments().getInt(Constants.ARG_USER_ID, 0) != 0) {
            fullUserInfo = UserCache.getInstance().getFullUserById(getArguments().getInt(Constants.ARG_USER_ID));
            chatId = getArguments().getLong(Constants.ARG_CHAT_ID);
        }
    }
    @Override
    public ComposeMessagePlaceholder.Reason displayComposeMessageView() {
        if(fullUserInfo == null)
        {
            if(getArguments() != null && getArguments().getBoolean(Constants.ARG_BOT, false) && noMessagesConfirmed)
                return ComposeMessagePlaceholder.Reason.BOT_NOT_INITIALISED;
            if(getArguments() != null && !getArguments().getBoolean(Constants.ARG_DELETED_ACCOUNT))
                return ComposeMessagePlaceholder.Reason.NO_REASON;
        }
        else
        {
            if(fullUserInfo.isBot() && noMessagesConfirmed)
                return ComposeMessagePlaceholder.Reason.BOT_NOT_INITIALISED;
            if(!fullUserInfo.isDeletedAccount())
                return ComposeMessagePlaceholder.Reason.NO_REASON;
        }
        return ComposeMessagePlaceholder.Reason.DELETED_ACCOUNT;
    }

    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        return null;
    }
    @Override
    public void onResume()
    {
        super.onResume();
        if(fullUserInfo == null) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.action_confirmation)
                    .setMessage(R.string.error_receiving_chat_info)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            goBack();
                        }

                    })
                    .setCancelable(false)
                    .show();
            return;
        }
        if(updateChatProcessor != null)
            updateChatProcessor.performResumeObservers();
        if(updateUserProcessor != null)
            updateUserProcessor.performResumeObservers();
    }
    @Override
    public void onPause()
    {
        super.onPause();
        if(updateChatProcessor != null)
            updateChatProcessor.performPauseObservers();
        if(updateUserProcessor != null)
            updateUserProcessor.performPauseObservers();
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(updateChatProcessor != null)
            updateChatProcessor.removeObservers();
        if(updateUserProcessor != null)
            updateUserProcessor.removeObservers();
    }
    @Override
    public ActionView getActionView()
    {
        if(actionView == null)
        {
            actionView = new ChatActionView(getActivity());
            actionView.setBackButtonClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    closeKeyboard();
                    goBack();
                }
            });
            actionView.setMiddlePartClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (fullUserInfo != null)
                        openProfile(fullUserInfo.getId(), getArguments().getLong(Constants.ARG_CHAT_ID));
                }
            });
            actionView.setClearSelectionClickListener(clearSelection);
            actionView.setDelButtonClickListener(delSelection);
            actionView.setShareButtonClickListener(fwdSelection);
        }
        return actionView;
    }
    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null && fullUserInfo != null) {
            actionViewController = new PrivateChatActionViewController();
            actionViewController.bindUserInfo(fullUserInfo);
            initUpdateProcessor();
        }
        return actionViewController;
    }
    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        super.onUpdateServiceConnected(service);
        initBotCmdListController();
    }

    @Override
    protected void disableSelectionMode() {
        if(actionView != null)
            actionView.setSelectionMode(false, true);
    }

    @Override
    protected void onSelectionCountUpdate(int newCount) {
        if(actionView != null && newCount >= 1)
        {
            actionView.changeSelectionCount(newCount);
            actionView.setSelectionMode(true, !selectionMode);
        }
    }

    @Override
    protected void enableSearchMode()
    {
        if(actionView == null)
            return;
        searchMode = true;
        actionView.setDelegate(chatSearchDelegate);
        actionView.setSearchMode(true);
        ((TelegramMainActivity) getActivity()).interceptBackPress(searchModeBackPress);
    }

    @Override
    protected void searchHashtag(String hashtag) {
        enableSearchMode();
        actionView.searchEditText.setText(hashtag);
        if(hashtag.length() > 1)
            chatSearchDelegate.didQuerySubmit(hashtag);
    }

    @Override
    protected void disableSearchMode()
    {
        if(actionView == null)
            return;
        searchMode = false;
        actionView.setSearchMode(false);
        ((TelegramMainActivity) getActivity()).removeBackPressInterceptor(searchModeBackPress);
    }

    @Override
    protected void changeDeleteIconVisibilityInSelectionBar(boolean canDelete) {
        if(actionView != null)
            actionView.setDelButtonVisibility(canDelete);
    }

    @Override
    protected void initBotCmdListController() {
        this.botCommandListController = new BotCmdListController(botCmdListView, botKeyboard, fullUserInfo, getArguments().getLong(Constants.ARG_CHAT_ID), replyMarkupMessageId, this.cmdListControllerDelegate);
        if(fullUserInfo != null && fullUserInfo.isBot())
        {
            try
            {
                listController.updateBotInfo((TdApi.BotInfoGeneral) fullUserInfo.getBotInfo());
            }
            catch(Exception e) { e.printStackTrace(); }
        }
    }

    @Override
    protected boolean isPrivateChat() {
        return true;
    }

    @Override
    protected boolean shouldSendTyping() {
        if(fullUserInfo == null || fullUserInfo.isBot())
            return false;
        return fullUserInfo.isOnline();
    }
    @Override
    protected void updateContextMenu()
    {
        if(actionView == null)
            return;
        if(contextMenu == null) {
            contextMenu = new PopupMenu(getActivity(), actionView.menuButton);
            contextMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    if(chatDelegate != null) {
                        chatDelegate.didContextMenuItemClicked(menuItem.getItemId());
                        return true;
                    }
                    return false;
                }
            });
        }
        else if(contextMenu.getMenu() != null)
            contextMenu.getMenu().clear();
        MenuInflater inflater = contextMenu.getMenuInflater();
        inflater.inflate(muted ? R.menu.private_chat_muted : R.menu.private_chat_unmuted, contextMenu.getMenu());
        actionView.setMenu(contextMenu);
    }

    @Override
    protected boolean canDeleteIncomingMessages() {
        return true;
    }

    /*static class GenerateUserModelTaskHandler extends PauseHandler
    {
        protected WeakReference<PrivateChatActionViewController> controllerRef;
        protected WeakReference<PrivateChatFragment> fragmentRef;
        GenerateUserModelTaskHandler(PrivateChatActionViewController view, PrivateChatFragment fragment)
        {
            this.controllerRef = new WeakReference<PrivateChatActionViewController>(view);
            this.fragmentRef = new WeakReference<PrivateChatFragment>(fragment);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef.get() != null)
                controllerRef.get().bindUserInfo((UserModel) message.obj);
            if(fragmentRef.get() != null)
                fragmentRef.get().initBotCmdListController();
        }
    }*/
    protected int getUserId()
    {
        return fullUserInfo.getId();
    }
    public void initChatWithBot()
    {
        if(fullUserInfo == null || listController == null)
            return;
        if(fullUserInfo.isBot())
            listController.startChatWithBot(fullUserInfo.getId());
    }
    private void initUpdateProcessor() {
        if (updateChatProcessor == null) {
            if (getArguments() == null || fullUserInfo == null)
                return;
            this.updateChatProcessor = new UpdateChatProcessor() {
                @Override
                protected String getKeyByUpdateConstructor(int constructor) {
                    return String.format("private_chat_observer_%d_%d_%d", chatId, fullUserInfo.getId(), constructor);
                }

                @Override
                protected boolean shouldObserveUpdateGroup() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateTitle() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateReplyMarkup() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateReadInbox() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateReadOutbox() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateChat() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateChatPhoto() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateChatOrder() {
                    return false;
                }

                @Override
                protected void processUpdateInBackground(TdApi.Update upd) {
                }

                @Override
                protected void processUpdateInUiThread(TdApi.Update upd) {
                    if (upd.getConstructor() == TdApi.UpdateChatTitle.CONSTRUCTOR && ((TdApi.UpdateChatTitle) upd).chatId == chatId)
                        actionViewController.updateTitle(((TdApi.UpdateChatTitle) upd).title);
                    else if (upd.getConstructor() == TdApi.UpdateChatPhoto.CONSTRUCTOR && ((TdApi.UpdateChatPhoto) upd).chatId == chatId)
                        actionViewController.updatePhoto(CommonTools.isLowDPIScreen() ? ((TdApi.UpdateChatPhoto) upd).photo.small : ((TdApi.UpdateChatPhoto) upd).photo.big);
                }
            };

            updateUserProcessor = new UpdateUserProcessor() {
                @Override
                protected String getKeyByUpdateConstructor(int constructor) {
                    return String.format("chatscreen%d_%d_constructor%d_observer", chatId, fullUserInfo.getId(), constructor);
                }

                @Override
                protected boolean shouldObserveUpdateUserStatus() {
                    return !fullUserInfo.isBot() && fullUserInfo.getId() != 333000 && fullUserInfo.getId() != 777000;
                }

                @Override
                protected boolean shouldObserveUpdateUserBlocked() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateUser() {
                    return false;
                }

                @Override
                protected void processUpdateInBackground(TdApi.Update upd) {
                }

                @Override
                protected void processUpdateInUiThread(TdApi.Update upd) {
                    if (upd.getConstructor() == TdApi.UpdateUserStatus.CONSTRUCTOR && ((TdApi.UpdateUserStatus) upd).userId == fullUserInfo.getId())
                        actionViewController.updateStatus(((TdApi.UpdateUserStatus) upd).status);
                }
            };
            TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
            if (service != null) {
                updateChatProcessor.attachObservers(service);
                updateUserProcessor.attachObservers(service);
            }
        }
    }
}
