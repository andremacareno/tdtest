package com.andremacareno.tdtest.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.OnTaskFailureListener;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.ChannelCache;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.TelegramAction;
import com.andremacareno.tdtest.tasks.CreateFileForCameraTask;
import com.andremacareno.tdtest.tasks.GalleryImagePathTask;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.NewGroupFinalStageActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 03/04/15.
 */
public class EditChannelInfoFragment extends BaseFragment {
    private NewGroupFinalStageActionView actionView;
    private EnterTitleActionViewController actionViewController;
    private ActionButtonDelegate actionButtonDelegate;
    private String photo_path;
    private GalleryImagePathTask galleryImagePathTask;
    private EditText channelDescriptionEditText, channelUsernameEditText;
    private PauseHandler cameraFileReceiver;
    private int channelId;
    private long chatId;
    private TdApi.Chat chatInfo;
    private TdApi.ChannelFull channelInfo;
    private Boolean removeChannelPhoto = null;
    private ProgressDialog processingDialog;
    private boolean updating = false;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getArguments() == null)
            return;
        channelId = getArguments().getInt(Constants.ARG_CHANNEL_ID);
        chatId = getArguments().getLong(Constants.ARG_CHAT_ID);
        chatInfo = ChatCache.getInstance().getChatById(chatId);
        channelInfo = ChannelCache.getInstance().getChannelFull(channelId);
        if(cameraFileReceiver == null) {
            cameraFileReceiver = new CameraFileReceiver(this);
            galleryImagePathTask = new GalleryImagePathTask();
        }
    }
    @Override
    public void onResume()
    {
        super.onResume();
        processingDialog= new ProgressDialog(getActivity());
        processingDialog.setMessage(getResources().getString(R.string.updating));
        processingDialog.setIndeterminate(true);
        processingDialog.setCancelable(false);
        if(updating)
            processingDialog.show();
        if(cameraFileReceiver != null) {
            cameraFileReceiver.resume();
        }
        if(actionView != null && photo_path != null)
            TelegramImageLoader.getInstance().loadAvatarPreview(photo_path, actionView.profilePhoto);
        else if(actionView != null && removeChannelPhoto == null || !removeChannelPhoto) {
            FileModel file = FileCache.getInstance().getFileModel(CommonTools.isLowDPIScreen() ? chatInfo.photo.small : chatInfo.photo.big);
            String key = CommonTools.makeFileKey(file.getFileId());
            TelegramImageLoader.getInstance().loadImage(file, key, actionView.profilePhoto, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, AndroidUtilities.dp(60), AndroidUtilities.dp(60), false);
        }
    }
    @Override
    public void onPause()
    {
        super.onPause();
        if(processingDialog.isShowing())
            processingDialog.dismiss();
        if(cameraFileReceiver != null) {
            cameraFileReceiver.pause();
        }
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        if(actionButtonDelegate == null)
            actionButtonDelegate = new ActionButtonDelegate() {
                @Override
                public void onActionButtonClick() {
                    editChannel();
                }

                @Override
                public int getActionButtonImageResource() {
                    return R.drawable.ic_check_gray;
                }
            };
        return actionButtonDelegate;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.channel_edit_info, container, false);
        View channelUsernameContainer = v.findViewById(R.id.invite_link_block);
        channelDescriptionEditText = (EditText) v.findViewById(R.id.channel_description);
        channelUsernameEditText = (EditText) v.findViewById(R.id.dynamic_invite_link);
        if(channelInfo != null)
        {
            boolean canEditUsername = channelInfo.channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor();
            if(canEditUsername)
            {
                channelUsernameContainer.setVisibility(View.VISIBLE);
                channelUsernameEditText.setText(channelInfo.channel.username);
            }
            else
                channelUsernameContainer.setVisibility(View.GONE);
            channelDescriptionEditText.setText(channelInfo.about);
        }
        return v;
    }
    @Override
    public ActionView getActionView() {
        if(actionView == null) {
            actionView = new NewGroupFinalStageActionView(getActivity());
            if(channelInfo != null)
                actionView.titleEditText.setText(chatInfo.title);
        }
        return actionView;
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new EnterTitleActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
    }
    private void editChannel()
    {
        if(actionView == null)
            return;
        final String title = actionView.titleEditText.getText().toString();
        final String about = channelDescriptionEditText.getText().toString();
        final String username = channelUsernameEditText.getText().toString();
        if(title.length() <= 0)
            return;
        updating = true;
        if(processingDialog != null)
            processingDialog.show();
        if(channelInfo == null || channelInfo.channel.username.equals(username)) {
            continueEdit(title, about);
            return;
        }
        if(!username.isEmpty() && !CommonTools.checkUsername(username))
        {
            updating = false;
            if(processingDialog != null)
                processingDialog.dismiss();
            Toast.makeText(getContext(), R.string.invalid_channel_link, Toast.LENGTH_SHORT).show();
            return;
        }
        TdApi.SearchChannel srch = new TdApi.SearchChannel(username);
        TG.getClientInstance().send(srch, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(object != null && object.getConstructor() == TdApi.Channel.CONSTRUCTOR)
                {
                    TelegramApplication.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getContext(), R.string.channel_username_taken, Toast.LENGTH_LONG).show();
                        }
                    });
                }
                TdApi.SearchUser searchUser = new TdApi.SearchUser(username);
                TG.getClientInstance().send(searchUser, new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        if (object != null && object.getConstructor() == TdApi.User.CONSTRUCTOR) {
                            TelegramApplication.applicationHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    updating = false;
                                    if(processingDialog != null)
                                        processingDialog.dismiss();
                                    Toast.makeText(getContext(), R.string.channel_username_taken, Toast.LENGTH_LONG).show();
                                }
                            });
                        } else {
                            TdApi.ChangeChannelUsername changeUsername = new TdApi.ChangeChannelUsername(channelId, username);
                            TG.getClientInstance().send(changeUsername, new Client.ResultHandler() {
                                @Override
                                public void onResult(TdApi.TLObject object) {
                                    if (object != null && object.getConstructor() == TdApi.Channel.CONSTRUCTOR)
                                        TelegramApplication.applicationHandler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                continueEdit(title, about);
                                            }
                                        });
                                }
                            });
                        }
                    }
                });
            }
        });
    }
    private void changePhoto()
    {
        if(removeChannelPhoto != null)
        {
            TdApi.ChangeChatPhoto changePhoto = new TdApi.ChangeChatPhoto(chatId, removeChannelPhoto ? new TdApi.InputFileId(0) : new TdApi.InputFileLocal(photo_path), null);
            TG.getClientInstance().send(changePhoto, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    TelegramApplication.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            updating = false;
                            if(processingDialog != null)
                                processingDialog.dismiss();
                            goBack();
                        }
                    });
                }
            });
        }
        else {
            updating = false;
            if(processingDialog != null)
                processingDialog.dismiss();
            goBack();
        }
    }
    private void continueEdit(final String title, final String about)
    {
        final Client.ResultHandler onFinish = new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                changePhoto();
            }
        };
        TdApi.ChangeChannelAbout changeAbout = new TdApi.ChangeChannelAbout(channelId, about);
        final TdApi.ChangeChatTitle changeTitle = new TdApi.ChangeChatTitle(chatId, title);
        if(channelInfo != null && !channelInfo.about.equals(about))
        {
            TG.getClientInstance().send(changeAbout, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    if(!title.isEmpty())
                    {
                        NotificationCenter.getInstance().postNotification(NotificationCenter.didChannelDescriptionUpdated, about);
                        TG.getClientInstance().send(changeTitle, onFinish);
                    }
                }
            });
        }
        else if(!title.isEmpty() && (channelInfo == null || !chatInfo.title.equals(title)))
            TG.getClientInstance().send(changeTitle, onFinish);
        else
            changePhoto();
    }

    private void hideKeyboard()
    {
        if(actionView == null)
            return;
        if(AndroidUtilities.isKeyboardShown(actionView.titleEditText))
            AndroidUtilities.hideKeyboard(actionView.titleEditText);
        if(AndroidUtilities.isKeyboardShown(channelDescriptionEditText))
            AndroidUtilities.hideKeyboard(channelDescriptionEditText);
    }
    @Override
    protected void onPasscodeLockShown()
    {
        hideKeyboard();
    }
    private void removePhoto()
    {
        removeChannelPhoto = true;
        photo_path = null;
        if(actionView != null)
            actionView.profilePhoto.setImageDrawable(null);
    }
    private void openGalleryForNewPhoto()
    {
        if (Build.VERSION.SDK_INT <19){
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(galleryIntent, TelegramAction.ATTACH_CHOOSE_FROM_GALLERY);
        } else {
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(galleryIntent, TelegramAction.ATTACH_CHOOSE_FROM_GALLERY);
        }
    }
    private void openCameraForNewPhoto()
    {
        if(!AndroidUtilities.haveCamera())
        {
            Toast.makeText(TelegramApplication.sharedApplication().getApplicationContext(), R.string.no_camera, Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Activity activity = getActivity();
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            CreateFileForCameraTask task = new CreateFileForCameraTask();
            task.onComplete(new OnTaskCompleteListener() {
                @Override
                public void taskComplete(AsyncTaskManTask task) {
                    CreateFileForCameraTask t = (CreateFileForCameraTask) task;
                    photo_path = t.getPhotoPath();
                    removeChannelPhoto = false;
                    Message msg = Message.obtain(cameraFileReceiver);
                    msg.obj = t.getPhotoUri();
                    msg.sendToTarget();
                }
            });
            task.onFailure(new OnTaskFailureListener() {
                @Override
                public void taskFailed(AsyncTaskManTask task) {
                    //Toast.makeText((fragmentRef.get().getActivity()), R.string.error_taking_photo, Toast.LENGTH_SHORT).show();
                }
            });
            task.addToQueue();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == Activity.RESULT_OK)
        {
            if(requestCode == TelegramAction.ATTACH_TAKE_A_PHOTO)
            {
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                File f = new File("file:".concat(photo_path));
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                getActivity().sendBroadcast(mediaScanIntent);
                if(photo_path != null && actionView != null)
                    TelegramImageLoader.getInstance().loadAvatarPreview(photo_path, actionView.profilePhoto);

            }
            else if(requestCode == TelegramAction.ATTACH_CHOOSE_FROM_GALLERY)
            {
                Uri uri = data.getData();
                if(uri == null)
                    Toast.makeText(getActivity(), R.string.error_picking_from_gallery, Toast.LENGTH_LONG).show();
                else
                {
                    if(Build.VERSION.SDK_INT >= 19)
                    {
                        final int takeFlags = data.getFlags()
                                & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        // Check for the freshest data.
                        //noinspection ResourceType
                        TelegramApplication.sharedApplication().getContentResolver().takePersistableUriPermission(uri, takeFlags);
                    }
                    galleryImagePathTask.setLink(uri);
                    galleryImagePathTask.onFailure(new OnTaskFailureListener() {
                        @Override
                        public void taskFailed(AsyncTaskManTask task) {
                            Toast.makeText(getActivity(), R.string.error_picking_from_gallery, Toast.LENGTH_LONG).show();
                        }
                    });
                    galleryImagePathTask.onComplete(new OnTaskCompleteListener() {
                        @Override
                        public void taskComplete(AsyncTaskManTask task) {
                            photo_path = ((GalleryImagePathTask) task).getImagePath();
                            removeChannelPhoto = false;
                            if (photo_path != null) {
                                TelegramApplication.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        TelegramImageLoader.getInstance().loadAvatarPreview(photo_path, actionView.profilePhoto);
                                    }
                                });
                            }
                        }
                    });
                    galleryImagePathTask.addToQueue();
                }

            }
        }
    }
    private void avatarAlert()
    {
        hideKeyboard();
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        CharSequence[] arr = new CharSequence[]{getResources().getString(R.string.from_camera), getResources().getString(R.string.from_gallery), getResources().getString(R.string.delete_photo)};
        adb.setItems(arr, new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0)
                    openCameraForNewPhoto();
                else if(i == 1)
                    openGalleryForNewPhoto();
                else if(i == 2)
                    removePhoto();
            }
        });
        adb.create().show();
    }
    private class EnterTitleActionViewController extends ActionViewController {
        private TextView.OnEditorActionListener onDoneListener;
        private View.OnClickListener avatarClickListener;
        public EnterTitleActionViewController()
        {
            super();
            init();
        }
        private void init()
        {
            onDoneListener = new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    if(i == EditorInfo.IME_ACTION_NEXT)
                    {
                        if(channelDescriptionEditText != null)
                            channelDescriptionEditText.requestFocus();
                        return true;
                    }
                    return false;
                }
            };
            avatarClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    avatarAlert();
                }
            };
        }
        @Override
        public void onFragmentPaused() {

        }

        @Override
        public void onFragmentResumed() {
            ActionView av = getActionView();
            if(av == null || !(av instanceof NewGroupFinalStageActionView))
                return;
            NewGroupFinalStageActionView cast = (NewGroupFinalStageActionView) av;
            cast.titleEditText.setHint(R.string.channel_name);
            cast.titleEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
            cast.titleEditText.setOnEditorActionListener(onDoneListener);
            cast.profilePhoto.setOnClickListener(avatarClickListener);
            cast.setBackButtonClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goBack();
                }
            });
        }

        @Override
        public void onActionViewRemoved() {

        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
    public static class CameraFileReceiver extends PauseHandler
    {
        private WeakReference<EditChannelInfoFragment> fragmentRef;
        public CameraFileReceiver(EditChannelInfoFragment c)
        {
            this.fragmentRef = new WeakReference<EditChannelInfoFragment>(c);
        }

        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(fragmentRef.get() == null)
                return;
            Uri capturedImageURI = (Uri) message.obj;
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    capturedImageURI);
            fragmentRef.get().startActivityForResult(takePictureIntent, TelegramAction.ATTACH_TAKE_A_PHOTO);
        }
    }
}
