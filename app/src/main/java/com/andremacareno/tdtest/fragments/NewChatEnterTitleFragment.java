package com.andremacareno.tdtest.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.OnTaskFailureListener;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.TelegramAction;
import com.andremacareno.tdtest.tasks.CreateFileForCameraTask;
import com.andremacareno.tdtest.tasks.GalleryImagePathTask;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.viewcontrollers.NewChatParticipantsListController;
import com.andremacareno.tdtest.viewcontrollers.actionview.ActionViewController;
import com.andremacareno.tdtest.views.ContactsListView;
import com.andremacareno.tdtest.views.actionviews.ActionView;
import com.andremacareno.tdtest.views.actionviews.NewGroupFinalStageActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 15/04/15.
 */
public class NewChatEnterTitleFragment extends BaseFragment {
    private final String TAG = "NewChatEnterTitleFr";
    private NewGroupFinalStageActionView actionView;
    private EnterTitleActionViewController actionViewController;
    private NewChatParticipantsListController listController;
    private PauseHandler newChatHandler;
    private String photo_path = null;
    private PauseHandler cameraFileReceiver;
    private GalleryImagePathTask galleryImagePathTask;
    private ContactsListView participantsList;
    private ActionButtonDelegate actionButtonDelegate;
    //arguments
    private int[] participant_ids;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.participant_ids = getArguments() != null && getArguments().containsKey("participant_ids") ? getArguments().getIntArray("participant_ids") : null;
        if(newChatHandler == null) {
            newChatHandler = new NewChatCreatedHandler(this);
            cameraFileReceiver = new CameraFileReceiver(this);
            galleryImagePathTask = new GalleryImagePathTask();
        }

    }
    @Override
    public void onResume()
    {
        super.onResume();
        if(newChatHandler != null) {
            newChatHandler.resume();
            cameraFileReceiver.resume();
        }
        if(actionView != null && photo_path != null)
            TelegramImageLoader.getInstance().loadAvatarPreview(photo_path, actionView.profilePhoto);
        if(listController != null) {
            listController.onFragmentResumed();
            if(participantsList != null)
                listController.replaceView(participantsList);
        }
    }
    @Override
    public void onPause()
    {
        super.onPause();
        if(newChatHandler != null) {
            newChatHandler.pause();
            cameraFileReceiver.pause();
        }
        if(listController != null) {
            listController.onFragmentPaused();
        }
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(listController != null) {
            listController.removeObservers();
        }
        listController = null;
    }
    @Override
    public ActionButtonDelegate getActionButtonDelegate() {
        if(actionButtonDelegate == null)
            actionButtonDelegate = new ActionButtonDelegate() {
                @Override
                public void onActionButtonClick() {
                    createNewChat();
                }

                @Override
                public int getActionButtonImageResource() {
                    return R.drawable.ic_check_gray;
                }
            };
        return actionButtonDelegate;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        participantsList = new ContactsListView(container.getContext());
        participantsList.setMotionEventSplittingEnabled(false);
        participantsList.setLayoutParams(new ViewGroup.LayoutParams(container.getLayoutParams()));
        participantsList.setVisibility(View.VISIBLE);
        return participantsList;
    }
    @Override
    public ActionView getActionView() {
        if(actionView == null)
            actionView = new NewGroupFinalStageActionView(getActivity());
        return actionView;
    }

    @Override
    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new EnterTitleActionViewController();
        return actionViewController;
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        if(listController == null) {
            listController = new NewChatParticipantsListController(participantsList, participant_ids);
        }
    }

    private void createNewChat()
    {
        if(actionView == null)
            return;
        String title = actionView.titleEditText.getText().toString();
        if(title.length() <= 0)
            return;
        final TdApi.CreateNewGroupChat createChatFunc = new TdApi.CreateNewGroupChat(participant_ids, title);
        Client telegramClient = TG.getClientInstance();
        telegramClient.send(createChatFunc, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                TdApi.Chat ch = (TdApi.Chat) object;
                Message msg = Message.obtain(newChatHandler);
                msg.obj = ch;
                msg.sendToTarget();
            }
        });
    }
    private void continueGoToChat(TdApi.Chat newChat)
    {
        TdApi.Group groupChat = ((TdApi.GroupChatInfo)newChat.type).group;
        Fragment fr = new GroupChatFragment();
        Bundle b = new Bundle();
        b.putLong(Constants.ARG_CHAT_ID, newChat.id);
        b.putInt(Constants.ARG_GROUP_ID, groupChat.id);
        b.putInt(Constants.ARG_LAST_READ_INBOX, newChat.lastReadInboxMessageId);
        b.putInt(Constants.ARG_LAST_READ_OUTBOX, newChat.lastReadOutboxMessageId);
        b.putInt(Constants.ARG_UNREAD_COUNT, newChat.unreadCount);
        b.putInt(Constants.ARG_START_MESSAGE, newChat.topMessage.id);
        b.putString(Constants.ARG_CHAT_TITLE, newChat.title);
        b.putInt(Constants.ARG_CHAT_PEER, groupChat.id);
        b.putBoolean(Constants.ARG_CHAT_LEFT, groupChat.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor() || groupChat.role.getConstructor() == Constants.PARTICIPANT_KICKED.getConstructor());
        fr.setArguments(b);
        String tag = Constants.CHAT_FRAGMENT;
        fr.setRetainInstance(true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction tr = ((TelegramMainActivity) getActivity()).prepareAnimatedTransaction();
        tr.replace(R.id.container, fr, tag).addToBackStack(null).commitAllowingStateLoss();
        fm.executePendingTransactions();
        ((TelegramMainActivity) getActivity()).clearTillMainFragmentNextTime();
    }
    private void didChatCreated(final TdApi.Chat newChat)
    {
        if(newChat.type.getConstructor() != TdApi.GroupChatInfo.CONSTRUCTOR)
            return;
        hideKeyboard();
        if(photo_path != null)
        {
            TdApi.ChangeChatPhoto setPhotoReq = new TdApi.ChangeChatPhoto(newChat.id, new TdApi.InputFileLocal(photo_path), null);
            TG.getClientInstance().send(setPhotoReq, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    TelegramApplication.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            continueGoToChat(newChat);
                        }
                    });
                }
            });
        }
        else
            continueGoToChat(newChat);

    }
    private void hideKeyboard()
    {
        if(actionView == null)
            return;
        if(AndroidUtilities.isKeyboardShown(actionView.titleEditText))
            AndroidUtilities.hideKeyboard(actionView.titleEditText);
    }
    @Override
    protected void onPasscodeLockShown()
    {
        hideKeyboard();
    }
    static class NewChatCreatedHandler extends PauseHandler
    {
        private WeakReference<NewChatEnterTitleFragment> fragmentRef;
        public NewChatCreatedHandler(NewChatEnterTitleFragment fr)
        {
            this.fragmentRef = new WeakReference<NewChatEnterTitleFragment>(fr);
        }

        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            NewChatEnterTitleFragment fr = fragmentRef.get();
            if(fr == null)
                return;
            final TdApi.Chat finalChat = (TdApi.Chat) message.obj;
            int peer = ((TdApi.GroupChatInfo)finalChat.type).group.id;
            ChatCache.getInstance().setGroupLink(finalChat.id, peer);
            fragmentRef.get().didChatCreated(finalChat);
        }
    }
    private void removePhoto()
    {
        photo_path = null;
        if(actionView != null)
            actionView.profilePhoto.setImageDrawable(null);
    }
    private void openGalleryForNewPhoto()
    {
        if (Build.VERSION.SDK_INT <19){
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(galleryIntent, TelegramAction.ATTACH_CHOOSE_FROM_GALLERY);
        } else {
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(galleryIntent, TelegramAction.ATTACH_CHOOSE_FROM_GALLERY);
        }
    }
    private void openCameraForNewPhoto()
    {
        if(!AndroidUtilities.haveCamera())
        {
            Toast.makeText(TelegramApplication.sharedApplication().getApplicationContext(), R.string.no_camera, Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Activity activity = getActivity();
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            CreateFileForCameraTask task = new CreateFileForCameraTask();
            task.onComplete(new OnTaskCompleteListener() {
                @Override
                public void taskComplete(AsyncTaskManTask task) {
                    CreateFileForCameraTask t = (CreateFileForCameraTask) task;
                    photo_path = t.getPhotoPath();
                    Message msg = Message.obtain(cameraFileReceiver);
                    msg.obj = t.getPhotoUri();
                    msg.sendToTarget();
                }
            });
            task.onFailure(new OnTaskFailureListener() {
                @Override
                public void taskFailed(AsyncTaskManTask task) {
                    //Toast.makeText((fragmentRef.get().getActivity()), R.string.error_taking_photo, Toast.LENGTH_SHORT).show();
                }
            });
            task.addToQueue();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == Activity.RESULT_OK)
        {
            if(requestCode == TelegramAction.ATTACH_TAKE_A_PHOTO)
            {
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                File f = new File("file:".concat(photo_path));
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                getActivity().sendBroadcast(mediaScanIntent);
                if(photo_path != null && actionView != null)
                    TelegramImageLoader.getInstance().loadAvatarPreview(photo_path, actionView.profilePhoto);

            }
            else if(requestCode == TelegramAction.ATTACH_CHOOSE_FROM_GALLERY)
            {
                Uri uri = data.getData();
                if(uri == null)
                    Toast.makeText(getActivity(), R.string.error_picking_from_gallery, Toast.LENGTH_LONG).show();
                else
                {
                    if(Build.VERSION.SDK_INT >= 19)
                    {
                        final int takeFlags = data.getFlags()
                                & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        // Check for the freshest data.
                        //noinspection ResourceType
                        TelegramApplication.sharedApplication().getContentResolver().takePersistableUriPermission(uri, takeFlags);
                    }
                    galleryImagePathTask.setLink(uri);
                    galleryImagePathTask.onFailure(new OnTaskFailureListener() {
                        @Override
                        public void taskFailed(AsyncTaskManTask task) {
                            try
                            {
                                TelegramApplication.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), R.string.error_picking_from_gallery, Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            catch(Exception ignored) {}
                        }
                    });
                    galleryImagePathTask.onComplete(new OnTaskCompleteListener() {
                        @Override
                        public void taskComplete(AsyncTaskManTask task) {
                            photo_path = ((GalleryImagePathTask) task).getImagePath();
                            if (photo_path != null) {
                                TelegramApplication.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        TelegramImageLoader.getInstance().loadAvatarPreview(photo_path, actionView.profilePhoto);
                                    }
                                });
                            }
                        }
                    });
                    galleryImagePathTask.addToQueue();
                }

            }
        }
    }
    private void avatarAlert()
    {
        hideKeyboard();
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        CharSequence[] arr = photo_path != null ? new CharSequence[]{getResources().getString(R.string.from_camera), getResources().getString(R.string.from_gallery), getResources().getString(R.string.delete_photo)} : new CharSequence[]{getResources().getString(R.string.from_camera), getResources().getString(R.string.from_gallery)};
        adb.setItems(arr, new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0)
                    openCameraForNewPhoto();
                else if(i == 1)
                    openGalleryForNewPhoto();
                else if(i == 2)
                    removePhoto();
            }
        });
        adb.create().show();
    }
    private class EnterTitleActionViewController extends ActionViewController {
        private TextView.OnEditorActionListener onDoneListener;
        private View.OnClickListener avatarClickListener;
        public EnterTitleActionViewController()
        {
            super();
            init();
        }
        private void init()
        {
            onDoneListener = new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    if(i == EditorInfo.IME_ACTION_DONE)
                    {
                        if(textView.length() > 0)
                            createNewChat();
                        return true;
                    }
                    return false;
                }
            };
            avatarClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    avatarAlert();
                }
            };
        }
        @Override
        public void onFragmentPaused() {

        }

        @Override
        public void onFragmentResumed() {
            ActionView av = getActionView();
            if(av == null || !(av instanceof NewGroupFinalStageActionView))
                return;
            NewGroupFinalStageActionView cast = (NewGroupFinalStageActionView) av;
            cast.titleEditText.setOnEditorActionListener(onDoneListener);
            cast.profilePhoto.setOnClickListener(avatarClickListener);
            cast.setBackButtonClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goBack();
                }
            });
        }

        @Override
        public void onActionViewRemoved() {

        }

        @Override
        public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

        }
    }
    public static class CameraFileReceiver extends PauseHandler
    {
        private WeakReference<NewChatEnterTitleFragment> fragmentRef;
        public CameraFileReceiver(NewChatEnterTitleFragment c)
        {
            this.fragmentRef = new WeakReference<NewChatEnterTitleFragment>(c);
        }

        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(fragmentRef.get() == null)
                return;
            Uri capturedImageURI = (Uri) message.obj;
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    capturedImageURI);
            fragmentRef.get().startActivityForResult(takePictureIntent, TelegramAction.ATTACH_TAKE_A_PHOTO);
        }
    }
}
