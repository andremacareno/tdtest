package com.andremacareno.tdtest.updatelisteners;

import com.andremacareno.tdtest.Constants;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 09.04.2015.
 */
public abstract class UserActionListener extends TelegramUpdateListener {
    private Long observableDialogId;
    public UserActionListener()
    {
        this.observableDialogId = null;
    }
    public UserActionListener(Long obs_d)
    {
        this.observableDialogId = obs_d;
    }
    @Override
    public int getObservableConstructor() {
        return TdApi.UpdateUserAction.CONSTRUCTOR;
    }

    @Override
    public String getKey() {
        if(observableDialogId == null)
            return Constants.GENERAL_USER_ACTION_OBSERVER;
        return String.format("dialog%d_useraction_observer", observableDialogId);
    }

    @Override
    public void onResult(TdApi.TLObject object) {
        TdApi.UpdateUserAction act = (TdApi.UpdateUserAction) object;
        if(act.action.getConstructor() != observableUserActionConstructor())
            return;
        onUpdateReceived(act);
    }
    protected abstract int observableUserActionConstructor();
    protected abstract void onUpdateReceived(TdApi.UpdateUserAction act);
}
