package com.andremacareno.tdtest.updatelisteners;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 29.04.2015.
 */
public abstract class UpdateDeleteMessagesListener extends TelegramUpdateListener {
    private final String key;
    private long chatId;
    public UpdateDeleteMessagesListener(long chatId)
    {
        this.key = String.format("update_delete_messages_%d_observer", chatId);
        this.chatId = chatId;
    }
    @Override
    public int getObservableConstructor() {
        return TdApi.UpdateDeleteMessages.CONSTRUCTOR;
    }

    @Override
    public String getKey() {
        return this.key;
    }

    @Override
    public void onResult(TdApi.TLObject object) {
        if(object.getConstructor() != TdApi.UpdateDeleteMessages.CONSTRUCTOR)
            return;
        TdApi.UpdateDeleteMessages upd = (TdApi.UpdateDeleteMessages) object;
        if(upd.chatId == this.chatId)
            didUpdateReceived(upd);
    }
    protected abstract void didUpdateReceived(TdApi.UpdateDeleteMessages upd);
}
