package com.andremacareno.tdtest.updatelisteners;

import com.andremacareno.tdtest.Constants;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 29.04.2015.
 */
public abstract class UpdateChatReadOutboxListener extends TelegramUpdateListener{
    private long observableChatId;
    private boolean global;
    private final String key;
    public UpdateChatReadOutboxListener()
    {
        this.global = true;
        this.key = Constants.GENERAL_CHAT_READ_OUTBOX_OBSERVER;
    }
    public UpdateChatReadOutboxListener(long chatId)
    {
        this.global = false;
        this.observableChatId = chatId;
        this.key = String.format("chat%d_read_outbox_observer", observableChatId);
    }

    @Override
    public int getObservableConstructor() {
        return TdApi.UpdateChatReadOutbox.CONSTRUCTOR;
    }

    @Override
    public String getKey() {
        return key;
    }
    @Override
    public void onResult(TdApi.TLObject object) {
        if(!global && observableChatId != ((TdApi.UpdateChatReadOutbox) object).chatId)
            return;
        didChatReadOutboxUpdated((TdApi.UpdateChatReadOutbox) object);
    }
    protected abstract void didChatReadOutboxUpdated(TdApi.UpdateChatReadOutbox upd);

}
