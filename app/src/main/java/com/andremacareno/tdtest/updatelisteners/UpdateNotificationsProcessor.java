package com.andremacareno.tdtest.updatelisteners;

import android.os.Message;

import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.TelegramUpdatesHandler;

import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 09/08/15.
 */
public abstract class UpdateNotificationsProcessor extends UpdateListening {
    private TelegramUpdateListener updateNotificationSettingsListener;
    private PauseHandler updBridge;
    public UpdateNotificationsProcessor()
    {
        init();
    }
    private void init()
    {
        updBridge = new UpdateBridge(this);
        updateNotificationSettingsListener = new TelegramUpdateListener() {
            @Override
            public int getObservableConstructor() {
                return TdApi.UpdateNotificationSettings.CONSTRUCTOR;
            }

            @Override
            public String getKey() {
                return getKeyByUpdateConstructor(TdApi.UpdateNotificationSettings.CONSTRUCTOR);
            }

            @Override
            public void onResult(TdApi.TLObject object) {
                processUpdateInBackground((TdApi.UpdateNotificationSettings) object);
                Message msg = Message.obtain(updBridge);
                msg.obj = object;
                msg.sendToTarget();
            }
        };
    }
    @Override
    public void performAttachObservers(TelegramUpdatesHandler updateHandler)
    {
         updateHandler.addObserver(updateNotificationSettingsListener);
    }
    @Override
    public void performRemoveObservers(TelegramUpdatesHandler updateHandler)
    {
        updateHandler.removeObserver(updateNotificationSettingsListener);
    }
    @Override
    public void performResumeObservers()
    {
        updBridge.resume();
    }
    @Override
    public void performPauseObservers()
    {
        updBridge.pause();
    }
    protected abstract String getKeyByUpdateConstructor(int constructor);

    protected abstract void processUpdateInBackground(TdApi.Update upd);
    protected abstract void processUpdateInUiThread(TdApi.Update upd);

    private static class UpdateBridge extends PauseHandler
    {
        private WeakReference<UpdateNotificationsProcessor> processorRef;
        public UpdateBridge(UpdateNotificationsProcessor processor)
        {
            this.processorRef = new WeakReference<UpdateNotificationsProcessor>(processor);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }
        @Override
        protected boolean storeOnlyLastMessage() {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(processorRef.get() != null)
                processorRef.get().processUpdateInUiThread((TdApi.Update) message.obj);
        }
    }
}
