package com.andremacareno.tdtest.updatelisteners;

import org.drinkless.td.libcore.telegram.Client;

/**
 * Created by andremacareno on 05/04/15.
 */
public abstract class TelegramUpdateListener implements Client.ResultHandler {
    public TelegramUpdateListener()
    {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TelegramUpdateListener that = (TelegramUpdateListener) o;

        if (getObservableConstructor() != that.getObservableConstructor()) return false;
        if (!getKey().equals(that.getKey())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = getObservableConstructor();
        result = 31 * result + getKey().hashCode();
        return result;
    }

    public abstract int getObservableConstructor();
    public abstract String getKey();
}
