package com.andremacareno.tdtest.updatelisteners;

import com.andremacareno.tdtest.Constants;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 11.04.2015.
 */
public abstract class UpdateUserStatusListener extends TelegramUpdateListener {
    private Integer observableUser;
    public UpdateUserStatusListener()
    {
        this.observableUser = null;
    }
    public UpdateUserStatusListener(int idToObserve)
    {
        this.observableUser = idToObserve;
    }
    @Override
    public int getObservableConstructor() {
        return TdApi.UpdateUserStatus.CONSTRUCTOR;
    }

    @Override
    public String getKey() {
        if(observableUser != null)
            return String.format("user%d_status_observer", observableUser);
        return Constants.CONTACTLIST_USER_STATUS_OBSERVER;
    }

    @Override
    public void onResult(TdApi.TLObject object) {
        TdApi.UpdateUserStatus upd = (TdApi.UpdateUserStatus)object;
        if(observableUser != null && !observableUser.equals(upd.userId))
            return;
        didStatusUpdated(upd);
    }
    protected abstract void didStatusUpdated(TdApi.UpdateUserStatus st);
}
