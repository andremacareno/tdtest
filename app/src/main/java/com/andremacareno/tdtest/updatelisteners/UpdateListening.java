package com.andremacareno.tdtest.updatelisteners;

import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;

/**
 * Created by andremacareno on 11/08/15.
 */
public abstract class UpdateListening {
    private boolean isAttachedToService = false;
    public void attachObservers(TelegramUpdatesHandler service)
    {
        if(isAttachedToService)
            return;
        isAttachedToService = true;
        performAttachObservers(service);
    }
    public void removeObservers()
    {
        isAttachedToService = false;
        TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
        if(service != null)
            performRemoveObservers(service);
    }

    protected abstract void performAttachObservers(TelegramUpdatesHandler service);
    protected abstract void performRemoveObservers(TelegramUpdatesHandler service);
    public abstract void performPauseObservers();
    public abstract void performResumeObservers();
}
