package com.andremacareno.tdtest.updatelisteners;

import com.andremacareno.tdtest.Constants;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 29.04.2015.
 */
public abstract class UpdateChatReadInboxListener extends TelegramUpdateListener{
    private long observableChatId;
    private boolean global;
    private final String key;
    public UpdateChatReadInboxListener()
    {
        this.global = true;
        this.key = Constants.GENERAL_CHAT_READ_INBOX_OBSERVER;
    }
    public UpdateChatReadInboxListener(long chatId)
    {
        this.global = false;
        this.observableChatId = chatId;
        this.key = String.format("chat%d_read_inbox_observer", observableChatId);
    }

    @Override
    public int getObservableConstructor() {
        return TdApi.UpdateChatReadInbox.CONSTRUCTOR;
    }

    @Override
    public String getKey() {
        return key;
    }
    @Override
    public void onResult(TdApi.TLObject object) {
        if(!global && observableChatId != ((TdApi.UpdateChatReadInbox) object).chatId)
            return;
        didChatReadInboxUpdated((TdApi.UpdateChatReadInbox) object);
    }
    protected abstract void didChatReadInboxUpdated(TdApi.UpdateChatReadInbox upd);

}
