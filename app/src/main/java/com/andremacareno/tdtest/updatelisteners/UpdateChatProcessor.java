package com.andremacareno.tdtest.updatelisteners;

import android.os.Message;
import android.util.Log;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.TelegramUpdatesHandler;

import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 09/08/15.
 */
public abstract class UpdateChatProcessor extends UpdateListening {
    private TelegramUpdateListener updateChatTitleListener;
    private TelegramUpdateListener updateGroupListener;
    private TelegramUpdateListener updateChatReplyMarkupListener;
    private TelegramUpdateListener updateChatReadInboxListener;
    private TelegramUpdateListener updateChatReadOutboxListener;
    private TelegramUpdateListener updateChatListener;
    private TelegramUpdateListener updateChatPhotoListener;
    private TelegramUpdateListener updateChatOrderListener;
    private PauseHandler updBridge;
    public UpdateChatProcessor()
    {
        init();
    }
    private void init()
    {
        updBridge = new UpdateBridge(this);
        if(shouldObserveUpdateTitle())
            updateChatTitleListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateChatTitle.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateChatTitle.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateChatTitle) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        if(shouldObserveUpdateGroup())
            updateGroupListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateGroup.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateGroup.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateGroup) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        if(shouldObserveUpdateReplyMarkup())
            updateChatReplyMarkupListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateChatReplyMarkup.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateChatReplyMarkup.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateChatReplyMarkup) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        if(shouldObserveUpdateReadInbox())
            updateChatReadInboxListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateChatReadInbox.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateChatReadInbox.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateChatReadInbox) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        if(shouldObserveUpdateReadOutbox())
            updateChatReadOutboxListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateChatReadOutbox.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateChatReadOutbox.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateChatReadOutbox) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        if(shouldObserveUpdateChat())
            updateChatListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateChat.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateChat.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateChat) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        if(shouldObserveUpdateChatPhoto())
            updateChatPhotoListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateChatPhoto.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateChatPhoto.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateChatPhoto) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        if(shouldObserveUpdateChatOrder())
            updateChatOrderListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateChatOrder.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateChatOrder.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateChatOrder) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
    }
    @Override
    protected void performAttachObservers(TelegramUpdatesHandler updateHandler)
    {
        if(shouldObserveUpdateTitle())
            updateHandler.addObserver(updateChatTitleListener);
        if(shouldObserveUpdateGroup())
            updateHandler.addObserver(updateGroupListener);
        if(shouldObserveUpdateReplyMarkup())
            updateHandler.addObserver(updateChatReplyMarkupListener);
        if(shouldObserveUpdateReadInbox())
            updateHandler.addObserver(updateChatReadInboxListener);
        if(shouldObserveUpdateReadOutbox())
            updateHandler.addObserver(updateChatReadOutboxListener);
        if(shouldObserveUpdateChat())
            updateHandler.addObserver(updateChatListener);
        if(shouldObserveUpdateChatPhoto())
            updateHandler.addObserver(updateChatPhotoListener);
        if(shouldObserveUpdateChatOrder())
            updateHandler.addObserver(updateChatOrderListener);
    }
    @Override
    protected void performRemoveObservers(TelegramUpdatesHandler updateHandler)
    {
        if(shouldObserveUpdateTitle())
            updateHandler.removeObserver(updateChatTitleListener);
        if(shouldObserveUpdateGroup())
            updateHandler.removeObserver(updateGroupListener);
        if (shouldObserveUpdateReplyMarkup())
            updateHandler.removeObserver(updateChatReplyMarkupListener);
        if (shouldObserveUpdateReadInbox())
            updateHandler.removeObserver(updateChatReadInboxListener);
        if (shouldObserveUpdateReadOutbox())
            updateHandler.removeObserver(updateChatReadOutboxListener);
        if(shouldObserveUpdateChat())
            updateHandler.removeObserver(updateChatListener);
        if(shouldObserveUpdateChatPhoto())
            updateHandler.removeObserver(updateChatPhotoListener);
        if(shouldObserveUpdateChatOrder())
            updateHandler.removeObserver(updateChatOrderListener);
    }
    protected abstract String getKeyByUpdateConstructor(int constructor);
    protected abstract boolean shouldObserveUpdateGroup();
    protected abstract boolean shouldObserveUpdateTitle();
    protected abstract boolean shouldObserveUpdateReplyMarkup();
    protected abstract boolean shouldObserveUpdateReadInbox();
    protected abstract boolean shouldObserveUpdateReadOutbox();
    protected abstract boolean shouldObserveUpdateChat();
    protected abstract boolean shouldObserveUpdateChatPhoto();
    protected abstract boolean shouldObserveUpdateChatOrder();
    protected abstract void processUpdateInBackground(TdApi.Update upd);
    protected abstract void processUpdateInUiThread(TdApi.Update upd);
    @Override
    public void performPauseObservers()
    {
        updBridge.pause();
    }
    @Override
    public void performResumeObservers()
    {
        updBridge.resume();
    }

    private static class UpdateBridge extends PauseHandler
    {
        private WeakReference<UpdateChatProcessor> processorRef;
        public UpdateBridge(UpdateChatProcessor processor)
        {
            this.processorRef = new WeakReference<UpdateChatProcessor>(processor);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(processorRef.get() != null) {
                if(BuildConfig.DEBUG)
                    Log.d("UpdateChatProcessor", message.obj.toString());
                processorRef.get().processUpdateInUiThread((TdApi.Update) message.obj);
            }
        }
    }
}
