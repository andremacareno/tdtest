package com.andremacareno.tdtest.updatelisteners;

import android.os.Message;
import android.util.Log;

import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.TelegramUpdatesHandler;

import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 09/08/15.
 */
public abstract class UpdateMessageProcessor extends UpdateListening {
    public interface UpdateMessageDelegate
    {
        public void updateMessageContent(TdApi.UpdateMessageContent upd);
        public void updateMessageDate(TdApi.UpdateMessageDate upd);
        public void updateMessageId(TdApi.UpdateMessageId upd);
        public void updateMessageSendFailed(TdApi.UpdateMessageSendFailed upd);
        public void updateNewMessage(TdApi.UpdateNewMessage upd);
        public void updateDelMessages(TdApi.UpdateDeleteMessages upd);
        public void updateViewsCount(TdApi.UpdateMessageViews upd);
    }
    private TelegramUpdateListener updateMsgContentListener;
    private TelegramUpdateListener updateMsgDateListener;
    private TelegramUpdateListener updateMsgIdListener;
    private TelegramUpdateListener updateMsgSendFailListener;
    private TelegramUpdateListener updateNewMsgListener;
    private TelegramUpdateListener updateDeleteMessagesListener;
    private TelegramUpdateListener updateViewsCountListener;

    private UpdateMessageDelegate bgThreadDelegate;
    private UpdateMessageDelegate uiThreadDelegate;
    private PauseHandler updBridge;
    public UpdateMessageProcessor(UpdateMessageDelegate bgThreadDelegateRef, UpdateMessageDelegate uiThreadDelegateRef)
    {
        this.bgThreadDelegate = bgThreadDelegateRef;
        this.uiThreadDelegate = uiThreadDelegateRef;
        init();
    }
    private void init()
    {
        updBridge = new UpdateBridge(this);
        if(shouldObserveUpdateMsgContent())
            updateMsgContentListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateMessageContent.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateMessageContent.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateMessageContent) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        if(shouldObserveUpdateMsgDate())
            updateMsgDateListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateMessageDate.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateMessageDate.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateMessageDate) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        if(shouldObserveUpdateMsgId())
            updateMsgIdListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateMessageId.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateMessageId.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateMessageId) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        if(shouldObserveUpdateMsgSendFailed())
            updateMsgSendFailListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateMessageSendFailed.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateMessageSendFailed.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateMessageSendFailed) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        if(shouldObserveUpdateNewMsg())
            updateNewMsgListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateNewMessage.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateNewMessage.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateNewMessage) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        if(shouldObserveUpdateDeleteMessages())
        {
            updateDeleteMessagesListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateDeleteMessages.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateDeleteMessages.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateDeleteMessages) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        }
        if(shouldObserveViewsCount())
        {
            updateViewsCountListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateMessageViews.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateMessageViews.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateMessageViews) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        }
    }
    @Override
    protected void performAttachObservers(TelegramUpdatesHandler updateHandler)
    {
        if(shouldObserveUpdateNewMsg())
            updateHandler.addObserver(updateNewMsgListener);
        if(shouldObserveUpdateMsgId())
            updateHandler.addObserver(updateMsgIdListener);
        if(shouldObserveUpdateMsgSendFailed())
            updateHandler.addObserver(updateMsgSendFailListener);
        if(shouldObserveUpdateMsgDate())
            updateHandler.addObserver(updateMsgDateListener);
        if(shouldObserveUpdateMsgContent())
            updateHandler.addObserver(updateMsgContentListener);
        if(shouldObserveUpdateDeleteMessages())
            updateHandler.addObserver(updateDeleteMessagesListener);
        if(shouldObserveViewsCount())
            updateHandler.addObserver(updateViewsCountListener);
    }
    @Override
    protected void performRemoveObservers(TelegramUpdatesHandler updateHandler)
    {
        if(shouldObserveUpdateNewMsg())
            updateHandler.removeObserver(updateNewMsgListener);
        if(shouldObserveUpdateMsgId())
            updateHandler.removeObserver(updateMsgIdListener);
        if(shouldObserveUpdateMsgSendFailed())
            updateHandler.removeObserver(updateMsgSendFailListener);
        if(shouldObserveUpdateMsgDate())
            updateHandler.removeObserver(updateMsgDateListener);
        if(shouldObserveUpdateMsgContent())
            updateHandler.removeObserver(updateMsgContentListener);
        if(shouldObserveUpdateDeleteMessages())
            updateHandler.removeObserver(updateDeleteMessagesListener);
        if(shouldObserveViewsCount())
            updateHandler.removeObserver(updateViewsCountListener);
    }
    protected abstract String getKeyByUpdateConstructor(int constructor);
    protected abstract boolean shouldObserveUpdateMsgContent();
    protected abstract boolean shouldObserveUpdateMsgDate();
    protected abstract boolean shouldObserveUpdateMsgId();
    protected abstract boolean shouldObserveUpdateMsgSendFailed();
    protected abstract boolean shouldObserveUpdateNewMsg();
    protected abstract boolean shouldObserveUpdateDeleteMessages();
    protected abstract boolean shouldObserveViewsCount();

    private void processUpdateInBackground(TdApi.Update upd)
    {
        if(bgThreadDelegate == null)
            return;
        if(upd.getConstructor() == TdApi.UpdateMessageDate.CONSTRUCTOR)
            bgThreadDelegate.updateMessageDate((TdApi.UpdateMessageDate) upd);
        else if(upd.getConstructor() == TdApi.UpdateMessageContent.CONSTRUCTOR)
            bgThreadDelegate.updateMessageContent((TdApi.UpdateMessageContent) upd);
        else if(upd.getConstructor() == TdApi.UpdateMessageId.CONSTRUCTOR)
            bgThreadDelegate.updateMessageId((TdApi.UpdateMessageId) upd);
        else if(upd.getConstructor() == TdApi.UpdateMessageSendFailed.CONSTRUCTOR)
            bgThreadDelegate.updateMessageSendFailed((TdApi.UpdateMessageSendFailed) upd);
        else if(upd.getConstructor() == TdApi.UpdateNewMessage.CONSTRUCTOR)
            bgThreadDelegate.updateNewMessage((TdApi.UpdateNewMessage) upd);
        else if(upd.getConstructor() == TdApi.UpdateDeleteMessages.CONSTRUCTOR)
            bgThreadDelegate.updateDelMessages((TdApi.UpdateDeleteMessages) upd);
        else if(upd.getConstructor() == TdApi.UpdateMessageViews.CONSTRUCTOR)
            bgThreadDelegate.updateViewsCount((TdApi.UpdateMessageViews) upd);
    }
    private void processUpdateInUiThread(TdApi.Update upd)
    {
        if(uiThreadDelegate == null)
            return;
        if(upd.getConstructor() == TdApi.UpdateMessageDate.CONSTRUCTOR)
            uiThreadDelegate.updateMessageDate((TdApi.UpdateMessageDate) upd);
        else if(upd.getConstructor() == TdApi.UpdateMessageContent.CONSTRUCTOR)
            uiThreadDelegate.updateMessageContent((TdApi.UpdateMessageContent) upd);
        else if(upd.getConstructor() == TdApi.UpdateMessageId.CONSTRUCTOR)
            uiThreadDelegate.updateMessageId((TdApi.UpdateMessageId) upd);
        else if(upd.getConstructor() == TdApi.UpdateMessageSendFailed.CONSTRUCTOR)
            uiThreadDelegate.updateMessageSendFailed((TdApi.UpdateMessageSendFailed) upd);
        else if(upd.getConstructor() == TdApi.UpdateNewMessage.CONSTRUCTOR)
            uiThreadDelegate.updateNewMessage((TdApi.UpdateNewMessage) upd);
        else if(upd.getConstructor() == TdApi.UpdateDeleteMessages.CONSTRUCTOR)
            uiThreadDelegate.updateDelMessages((TdApi.UpdateDeleteMessages) upd);
        else if(upd.getConstructor() == TdApi.UpdateMessageViews.CONSTRUCTOR)
            uiThreadDelegate.updateViewsCount((TdApi.UpdateMessageViews) upd);
    }
    @Override
    public void performPauseObservers() {
        updBridge.pause();
    }

    @Override
    public void performResumeObservers() {
        updBridge.resume();
    }

    private static class UpdateBridge extends PauseHandler
    {
        private WeakReference<UpdateMessageProcessor> processorRef;
        public UpdateBridge(UpdateMessageProcessor processor)
        {
            this.processorRef = new WeakReference<UpdateMessageProcessor>(processor);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(processorRef.get() != null) {
                Log.d("UpdateMsgProcessor", message.obj.toString());
                processorRef.get().processUpdateInUiThread((TdApi.Update) message.obj);
            }
        }
    }
}
