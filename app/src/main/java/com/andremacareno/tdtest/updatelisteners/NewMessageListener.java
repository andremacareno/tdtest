package com.andremacareno.tdtest.updatelisteners;

import com.andremacareno.tdtest.Constants;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 08/04/15.
 */
public abstract class NewMessageListener extends TelegramUpdateListener {
    private boolean globalListener;
    private long observableChatId;
    private final String listenerKey;
    public NewMessageListener() {
        this.globalListener = true;
        listenerKey = Constants.GENERAL_NEW_MESSAGE_OBSERVER;
    }
    public NewMessageListener(long chatId) {
        this.observableChatId = chatId;
        this.globalListener = false;
        listenerKey = String.format(Constants.CHAT_NEW_MESSAGE_OBSERVER, this.observableChatId);
    }
    @Override
    public int getObservableConstructor() {
        return TdApi.UpdateNewMessage.CONSTRUCTOR;
    }

    @Override
    public String getKey() {
        return listenerKey;
    }

    @Override
    public void onResult(TdApi.TLObject object)
    {
        TdApi.UpdateNewMessage upd = (TdApi.UpdateNewMessage) object;
        if(!globalListener && upd.message.chatId != observableChatId)
            return;
        processResult(upd);
    }
    public abstract void processResult(TdApi.UpdateNewMessage upd);
}
