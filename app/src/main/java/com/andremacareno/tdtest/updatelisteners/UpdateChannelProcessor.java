package com.andremacareno.tdtest.updatelisteners;

import android.os.Message;
import android.util.Log;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.TelegramUpdatesHandler;

import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 09/08/15.
 */
public abstract class UpdateChannelProcessor extends UpdateListening {
    private TelegramUpdateListener updateChannelListener;
    private PauseHandler updBridge;
    public UpdateChannelProcessor()
    {
        init();
    }
    private void init()
    {
        updBridge = new UpdateBridge(this);
        updateChannelListener = new TelegramUpdateListener() {
            @Override
            public int getObservableConstructor() {
                return TdApi.UpdateChannel.CONSTRUCTOR;
            }

            @Override
            public String getKey() {
                return getKeyByUpdateConstructor(TdApi.UpdateChannel.CONSTRUCTOR);
            }

            @Override
            public void onResult(TdApi.TLObject object) {
                processUpdateInBackground((TdApi.UpdateChannel) object);
                Message msg = Message.obtain(updBridge);
                msg.obj = object;
                msg.sendToTarget();
            }
        };

    }
    @Override
    protected void performAttachObservers(TelegramUpdatesHandler updateHandler)
    {
        updateHandler.addObserver(updateChannelListener);
    }
    @Override
    protected void performRemoveObservers(TelegramUpdatesHandler updateHandler)
    {
        updateHandler.removeObserver(updateChannelListener);
    }
    protected abstract String getKeyByUpdateConstructor(int constructor);
    protected abstract void processUpdateInBackground(TdApi.Update upd);
    protected abstract void processUpdateInUiThread(TdApi.Update upd);
    @Override
    public void performPauseObservers()
    {
        updBridge.pause();
    }
    @Override
    public void performResumeObservers()
    {
        updBridge.resume();
    }

    private static class UpdateBridge extends PauseHandler
    {
        private WeakReference<UpdateChannelProcessor> processorRef;
        public UpdateBridge(UpdateChannelProcessor processor)
        {
            this.processorRef = new WeakReference<UpdateChannelProcessor>(processor);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(processorRef.get() != null) {
                if(BuildConfig.DEBUG)
                    Log.d("UpdateChatProcessor", message.obj.toString());
                processorRef.get().processUpdateInUiThread((TdApi.Update) message.obj);
            }
        }
    }
}
