package com.andremacareno.tdtest.updatelisteners;

import android.os.Message;

import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.TelegramUpdatesHandler;

import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 09/08/15.
 */
public abstract class UpdateUserProcessor extends UpdateListening {
    private TelegramUpdateListener updateUserStatusListener;
    private TelegramUpdateListener updateUserDataListener;
    private TelegramUpdateListener updateUserBlockedListener;
    private PauseHandler updBridge;
    public UpdateUserProcessor()
    {
        init();
    }
    private void init()
    {
        updBridge = new UpdateBridge(this);
        if(shouldObserveUpdateUserStatus())
            updateUserStatusListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateUserStatus.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateUserStatus.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateUserStatus) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        if(shouldObserveUpdateUserBlocked())
            updateUserBlockedListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateUserBlocked.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateUserBlocked.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateUserBlocked) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
        if(shouldObserveUpdateUser())
            updateUserDataListener = new TelegramUpdateListener() {
                @Override
                public int getObservableConstructor() {
                    return TdApi.UpdateUser.CONSTRUCTOR;
                }

                @Override
                public String getKey() {
                    return getKeyByUpdateConstructor(TdApi.UpdateUser.CONSTRUCTOR);
                }

                @Override
                public void onResult(TdApi.TLObject object) {
                    processUpdateInBackground((TdApi.UpdateUser) object);
                    Message msg = Message.obtain(updBridge);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            };
    }
    @Override
    protected void performAttachObservers(TelegramUpdatesHandler updateHandler)
    {
        if(shouldObserveUpdateUser() && updateUserDataListener != null)
            updateHandler.addObserver(updateUserDataListener);
        if(shouldObserveUpdateUserBlocked() && updateUserBlockedListener != null)
            updateHandler.addObserver(updateUserBlockedListener);
        if(shouldObserveUpdateUserStatus() && updateUserStatusListener != null)
            updateHandler.addObserver(updateUserStatusListener);
    }
    @Override
    protected void performRemoveObservers(TelegramUpdatesHandler updateHandler)
    {
        if(shouldObserveUpdateUser() && updateUserDataListener != null)
            updateHandler.removeObserver(updateUserDataListener);
        if(shouldObserveUpdateUserBlocked() && updateUserBlockedListener != null)
            updateHandler.removeObserver(updateUserBlockedListener);
        if(shouldObserveUpdateUserStatus() && updateUserStatusListener != null)
            updateHandler.removeObserver(updateUserStatusListener);
    }
    protected abstract String getKeyByUpdateConstructor(int constructor);
    protected abstract boolean shouldObserveUpdateUserStatus();
    protected abstract boolean shouldObserveUpdateUserBlocked();
    protected abstract boolean shouldObserveUpdateUser();

    protected abstract void processUpdateInBackground(TdApi.Update upd);
    protected abstract void processUpdateInUiThread(TdApi.Update upd);
    @Override
    public void performPauseObservers()
    {
        updBridge.pause();
    }
    @Override
    public void performResumeObservers()
    {
        updBridge.resume();
    }

    private static class UpdateBridge extends PauseHandler
    {
        private WeakReference<UpdateUserProcessor> processorRef;
        public UpdateBridge(UpdateUserProcessor processor)
        {
            this.processorRef = new WeakReference<UpdateUserProcessor>(processor);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(processorRef.get() != null)
                processorRef.get().processUpdateInUiThread((TdApi.Update) message.obj);
        }
    }
}
