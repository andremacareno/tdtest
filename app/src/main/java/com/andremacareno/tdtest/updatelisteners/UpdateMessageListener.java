package com.andremacareno.tdtest.updatelisteners;

import com.andremacareno.tdtest.Constants;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 08/04/15.
 */
public class UpdateMessageListener extends TelegramUpdateListener {
    private long observableChatId;
    private int observableConstructor;
    private Object processImplementation;
    private final String key;
    private boolean isGlobal;
    public UpdateMessageListener(int constructor, Object pi)
    {
        this.observableConstructor = constructor;
        this.processImplementation = pi;
        this.isGlobal = true;
        this.key = Constants.CHAT_UPDATE_MESSAGE_OBSERVER;
    }
    public UpdateMessageListener(int constructor, long chatId, Object pi)
    {
        this.observableConstructor = constructor;
        this.observableChatId = chatId;
        this.processImplementation = pi;
        this.isGlobal = false;
        this.key = String.format("observer%d_%d", observableChatId, observableConstructor);
    }
    @Override
    public int getObservableConstructor() {
        return this.observableConstructor;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void onResult(TdApi.TLObject object) {
        if(observableConstructor == TdApi.UpdateMessageId.CONSTRUCTOR)
        {
            TdApi.UpdateMessageId upd = ((TdApi.UpdateMessageId) object);
            if(isGlobal)
            {
                ((GlobalUpdateMessageIdListener) processImplementation).didNewIdReceived(upd.chatId, upd.newId, upd.oldId);
                return;
            }
            else if(upd.chatId != observableChatId)
                return;
            ((UpdateMessageIdListener) processImplementation).didNewIdReceived(upd.newId, upd.oldId);
        }
        else if(observableConstructor == TdApi.UpdateMessageContent.CONSTRUCTOR)
        {
            TdApi.UpdateMessageContent upd = ((TdApi.UpdateMessageContent) object);

            if(isGlobal)
            {
                ((GlobalUpdateMessageContentListener) processImplementation).didNewContentReceived(upd.chatId, upd.messageId, upd.newContent);
                return;
            }
            else if(upd.chatId != observableChatId)
                return;
            ((UpdateMessageContentListener) processImplementation).didNewContentReceived(upd.messageId, upd.newContent);
        }
        else if(observableConstructor == TdApi.UpdateMessageDate.CONSTRUCTOR)
        {
            TdApi.UpdateMessageDate upd = ((TdApi.UpdateMessageDate) object);
            if(isGlobal)
            {
                ((GlobalUpdateMessageDateListener) processImplementation).didNewDateReceived(upd.chatId, upd.messageId, upd.newDate);
                return;
            }
            else if(upd.chatId != observableChatId)
                return;
            ((UpdateMessageDateListener) processImplementation).didNewDateReceived(upd.messageId, upd.newDate);
        }
        else if(observableConstructor == TdApi.UpdateMessageSendFailed.CONSTRUCTOR)
        {
            TdApi.UpdateMessageSendFailed upd = (TdApi.UpdateMessageSendFailed) object;
            if(isGlobal)
            {
                ((GlobalUpdateMessageSendFailedListener) processImplementation).didSendingFailed(upd.chatId, upd.messageId, upd.errorCode, upd.errorDescription);
                return;
            }
            else if(upd.chatId != observableChatId)
                return;
            ((UpdateMessageSendFailedListener) processImplementation).didSendingFailed(upd.messageId, upd.errorCode, upd.errorDescription);
        }
    }

    public static interface GlobalUpdateMessageContentListener {
        public void didNewContentReceived(long chat_id, int msg_id, TdApi.MessageContent newContent);
    }

    public static interface GlobalUpdateMessageDateListener {
        public void didNewDateReceived(long chat_id, int msg_id, int date);
    }

    public static interface GlobalUpdateMessageIdListener {
        public void didNewIdReceived(long chat_id, int new_id, int old_id);
    }
    public static interface GlobalUpdateMessageSendFailedListener {
        public void didSendingFailed(long chat_id, int msg_id, int error_code, String error_description);
    }

    public static interface UpdateMessageContentListener {
        public void didNewContentReceived(int msg_id, TdApi.MessageContent newContent);
    }

    public static interface UpdateMessageDateListener {
        public void didNewDateReceived(int msg_id, int date);
    }

    public static interface UpdateMessageIdListener {
        public void didNewIdReceived(int new_id, int old_id);
    }
    public static interface UpdateMessageSendFailedListener {
        public void didSendingFailed(int msg_id, int error_code, String error_description);
    }
}
