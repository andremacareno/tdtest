package com.andremacareno.tdtest;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Build;
import android.os.Process;

import com.andremacareno.tdtest.models.VoiceMessageModel;

import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by andremacareno on 19/08/15.
 */
public class MessageVoicePlayer {
    public interface VoiceMessageDelegate
    {
        public void didPlaybackStarted();
        public void didPlaybackStopped();
        public void didPlaybackProgressUpdated();
        public void didPlaybackPaused();
        public String getDelegateKey();
    }
    private class AudioBuffer {
        public AudioBuffer(int capacity) {
            buffer = ByteBuffer.allocateDirect(capacity);
        }
        ByteBuffer buffer;
        int size;
        int finished;
        long pcmOffset;
    }
    private VoiceMessageModel nowPlaying;
    private static volatile MessageVoicePlayer instance;
    private int playerBufferSize;
    private AudioTrack player;
    private final Object readLock = new Object();
    private final AtomicBoolean cleaningUp = new AtomicBoolean(false);
    private volatile boolean seekFinished = true;
    private volatile boolean trackPaused = false;
    private volatile boolean decodingFinished = false;
    public static MessageVoicePlayer getInstance()
    {
        if(instance == null)
        {
            synchronized(MessageVoicePlayer.class)
            {
                if(instance == null)
                    instance = new MessageVoicePlayer();
            }
        }
        return instance;
    }
    private MessageVoicePlayer()
    {
        playerBufferSize = AudioTrack.getMinBufferSize(48000, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
        if (playerBufferSize <= 0) {
            playerBufferSize = 3840;
        }
    }
    private void initAudioSystem()
    {
        player = new AudioTrack(AudioManager.STREAM_MUSIC, 48000, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, playerBufferSize, AudioTrack.MODE_STREAM);
        if(Build.VERSION.SDK_INT >= 21)
            player.setVolume(1.0f);
        else
            player.setStereoVolume(1.0f, 1.0f);
    }
    private void releaseAudioSystem()
    {
        if(player == null)
            return;
        player.stop();
        player.release();
        player = null;
    }
    public void stopPlayback()
    {
        cleanup();
    }
    public void play(final VoiceMessageModel voiceMessage)
    {
        if(voiceMessage == null)
            return;
        if(nowPlaying != null && voiceMessage.getAudioFileRef().getFileId() == nowPlaying.getAudioFileRef().getFileId())
        {
            if(player != null)
            {
                if(player.getPlayState() == AudioTrack.PLAYSTATE_PLAYING)
                    pause(voiceMessage);
                else
                    unpause();
            }
            return;
        }
        else if(nowPlaying != null)
            cleanup();
        AudioController.getInstance().initPlaybackCommunicationBridge();
        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO);
                if(AudioController.getInstance().isOpusFile(voiceMessage.getAudioFileRef().getFile().path) != 1)
                    return;
                if(AudioController.getInstance().openOpusFile(voiceMessage.getAudioFileRef().getFile().path) != 1)
                    return;
                voiceMessage.setTotalPcmDuration(AudioController.getInstance().getTotalPcmDuration());
                AudioController.getInstance().seekOpusFile(voiceMessage.getStartFrom());
                initAudioSystem();
                nowPlaying = voiceMessage;
                AudioController.getInstance().updateVoiceMessageState(nowPlaying, AudioController.VOICE_PLAYBACK_STARTED);
                read();
            }
        }).start();
    }
    public void pause(VoiceMessageModel voiceMessage)
    {
        if(voiceMessage == null)
            return;
        if(nowPlaying == null || nowPlaying.getAudioFileRef().getFileId() != voiceMessage.getAudioFileRef().getFileId())
            return;
        AudioController.getInstance().updateVoiceMessageState(nowPlaying, AudioController.VOICE_PLAYBACK_PAUSED);
        trackPaused = true;
    }
    private void unpause()
    {
        trackPaused = false;
        AudioController.getInstance().updateVoiceMessageState(nowPlaying, AudioController.VOICE_PLAYBACK_STARTED);
        synchronized(readLock)
        {
            readLock.notifyAll();
        }
    }
    public void seek(final VoiceMessageModel voiceMessage)
    {
        if(player == null)
            return;
        if(nowPlaying == null)
            return;
        else if(nowPlaying.getAudioFileRef().getFileId() != voiceMessage.getAudioFileRef().getFileId())
            return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_LOWEST);
                lockReadForSeek();
                AudioController.getInstance().seekOpusFile(voiceMessage.getStartFrom());
                notifySeekFinished();
            }
        }).start();
    }
    private void lockReadForSeek()
    {
        seekFinished = false;
    }
    private void notifySeekFinished()
    {
        seekFinished = true;
        synchronized(readLock)
        {
            readLock.notifyAll();
        }
    }
    private void cleanup()
    {
        if(player == null || nowPlaying == null)
            return;
        synchronized(cleaningUp)
        {
            cleaningUp.set(true);
            if(nowPlaying != null) {
                nowPlaying.resetStartMark();
                AudioController.getInstance().updateVoiceMessageState(nowPlaying, AudioController.VOICE_PLAYBACK_STOPPED);
            }
            nowPlaying = null;
            trackPaused = false;
            decodingFinished = false;
            seekFinished = true;
            releaseAudioSystem();
            AudioController.getInstance().closeOpusFile();
            cleaningUp.set(false);
        }
    }
    private void read()
    {
        if(player == null)
            return;
        while(!decodingFinished)
        {
            while (!seekFinished || trackPaused) {
                synchronized(readLock)
                {
                    try {
                        if(player.getPlayState() == AudioTrack.PLAYSTATE_PLAYING)
                            player.pause();
                        readLock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            AudioBuffer buffer = new AudioBuffer(playerBufferSize);
            int readArgs[] = new int[3];
            AudioController.getInstance().readOpusFile(buffer.buffer, playerBufferSize, readArgs);
            buffer.size = readArgs[0];
            buffer.pcmOffset = readArgs[1];
            buffer.finished = readArgs[2];
            if (buffer.finished == 1) {
                decodingFinished = true;
            }
            synchronized(cleaningUp)
            {
                if(!cleaningUp.get() && nowPlaying != null)
                {
                    float progressValue = (float) buffer.pcmOffset / (float) nowPlaying.getTotalPcmDuration();
                    if(progressValue < 0)
                        progressValue = 0;
                    nowPlaying.updateProgress(progressValue);
                    AudioController.getInstance().updateVoiceMessageState(nowPlaying, AudioController.VOICE_PLAYBACK_UPDATE_PROGRESS);
                    player.write(buffer.buffer.array(), 0, buffer.size);
                    if (buffer.size != 0)
                        buffer.buffer.rewind();
                    if(player.getPlayState() != AudioTrack.PLAYSTATE_PLAYING) {
                        player.play();
                    }
                }
                else if(nowPlaying == null) {
                    break;
                }
            }
        }
        if(decodingFinished)
            cleanup();
    }
}
