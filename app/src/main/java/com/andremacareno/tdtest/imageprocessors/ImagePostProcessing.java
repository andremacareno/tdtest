package com.andremacareno.tdtest.imageprocessors;

import android.graphics.Bitmap;

/**
 * Created by Andrew on 12.07.2014.
 */
public interface ImagePostProcessing {
    public Bitmap process(Bitmap original);
}
