package com.andremacareno.tdtest.imageprocessors;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;

/**
 * Created by Andrew on 12.07.2014.
 */
public class ScaleImage implements ImagePostProcessing {
    private int scaleToWidth, scaleToHeight;
    public ScaleImage(int requiredWidth, int requiredHeight)
    {
        this.scaleToHeight = requiredHeight;
        this.scaleToWidth = requiredWidth;
    }
    @Override
    public Bitmap process(Bitmap bmp) {
        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0, 0, bmp.getWidth(), bmp.getHeight()), new RectF(0, 0, scaleToWidth, scaleToHeight), Matrix.ScaleToFit.CENTER);
        return Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), m, true);
    }
}
