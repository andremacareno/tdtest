package com.andremacareno.tdtest.imageprocessors;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by Andrew on 12.07.2014.
 */
public class AddPinToImage implements ImagePostProcessing {
    private boolean downscalePin;
    public AddPinToImage(boolean downscale)
    {
        this.downscalePin = downscale;
    }
    @Override
    public Bitmap process(Bitmap bmp) {
        final int sourceWidth = bmp.getWidth();
        final int sourceHeight = bmp.getHeight();
        final int pinWidth = downscalePin ? AndroidUtilities.dp(13) : AndroidUtilities.dp(26);
        final int pinHeight = downscalePin ? AndroidUtilities.dp(21) : AndroidUtilities.dp(42);
        final int x1 = (sourceWidth + pinWidth) / 2;
        final int y1 = (sourceHeight + pinHeight) / 2;
        final int x0 = x1 - pinWidth;
        final int y0 = y1 - pinHeight;
        final Rect rect = new Rect(x0, y0, x1, y1);
        final Drawable pin = TelegramApplication.sharedApplication().getApplicationContext().getResources().getDrawable(R.drawable.ic_map_pin);
        pin.setBounds(rect);
        Bitmap output = Bitmap.createBitmap(sourceWidth, sourceHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.OVERLAY));
        canvas.drawBitmap(bmp, 0, 0, paint);
        pin.draw(canvas);
        return output;
    }
}
