package com.andremacareno.tdtest.imageprocessors;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;

/**
 * Created by Andrew on 12.07.2014.
 */
public class CircleImage implements ImagePostProcessing {

    @Override
    public Bitmap process(Bitmap bmp) {
        final int sourceWidth = bmp.getWidth();
        final int sourceHeight = bmp.getHeight();
        Bitmap output = Bitmap.createBitmap(sourceWidth, sourceHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle(sourceWidth / 2, sourceHeight / 2, Math.min(sourceWidth, sourceHeight) / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bmp, 0, 0, paint);
        return output;
    }
}
