package com.andremacareno.tdtest;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

public class GifKeyboardCache {
    private static final String TAG = "StickersCache";
    private final static ArrayList<TdApi.Animation> animations = new ArrayList<>();
    public static ArrayList<TdApi.Animation> getAnimations()
    {
        return animations;
    }
    private static volatile boolean loadingList = false;
    private static volatile boolean listLoaded = false;
    public static void checkGifs() {
        if (!loadingList && !listLoaded) {
            loadGifs();
        }
    }
    public static void invalidate()
    {
        listLoaded = false;
        loadingList = false;
        animations.clear();
    }
    private static void loadGifs()
    {
        if (loadingList) {
            return;
        }
        TdApi.GetSavedAnimations getAnimationsReq = new TdApi.GetSavedAnimations();
        TG.getClientInstance().send(getAnimationsReq, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() != TdApi.Animations.CONSTRUCTOR)
                    return;
                try {
                    for (TdApi.Animation anim : ((TdApi.Animations) object).animations) {
                        synchronized (animations) {
                            animations.add(anim);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                listLoaded = true;
                loadingList = false;
                NotificationCenter.getInstance().postNotification(NotificationCenter.didSavedAnimationsLoaded, null);
            }
        });
    }
    public static void addSavedAnimation(final TdApi.Animation anim)
    {
        TdApi.AddSavedAnimation addAnim = new TdApi.AddSavedAnimation(new TdApi.InputFileId(anim.animation.id));
        TG.getClientInstance().send(addAnim, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(object == null || object.getConstructor() != TdApi.Ok.CONSTRUCTOR)
                    return;
                try
                {
                    synchronized(animations)
                    {
                        animations.add(0, anim);
                    }
                }
                catch(Exception e) { e.printStackTrace(); }
            }
        });
    }
    public static void deleteSavedAnimation(final TdApi.Animation anim)
    {
        TdApi.DeleteSavedAnimation removeAnim = new TdApi.DeleteSavedAnimation(new TdApi.InputFileId(anim.animation.id));
        TG.getClientInstance().send(removeAnim, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object == null || object.getConstructor() != TdApi.Ok.CONSTRUCTOR)
                    return;
                try {
                    synchronized (animations) {
                        int index = 0;
                        boolean found = false;
                        for (; index < animations.size(); index++) {
                            TdApi.Animation a = animations.get(index);
                            if (anim.animation.id == a.animation.id) {
                                found = true;
                                break;
                            }
                        }
                        if (found) {
                            animations.remove(index);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}