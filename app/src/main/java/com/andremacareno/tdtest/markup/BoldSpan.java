package com.andremacareno.tdtest.markup;


import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.TextPaint;
import android.view.View;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.TextMessageModel;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 20/02/16.
 */
public class BoldSpan extends TelegramMarkupSpan {

    public BoldSpan(TextMessageModel.TextMessageLinkDelegate delegateRef, TdApi.MessageEntity markupInfo, Spannable stringToSpan) {
        super(delegateRef, markupInfo, stringToSpan);
    }

    @Override
    protected int getMarkupStart() {
        return ((TdApi.MessageEntityBold) markupInfo).offset;
    }

    @Override
    protected int getMarkupEnd() {
        return ((TdApi.MessageEntityBold) markupInfo).offset + ((TdApi.MessageEntityBold) markupInfo).length;
    }

    @Override
    public void onClick(View view) {
    }

    @Override
    public void updateDrawState(TextPaint paint) {
        paint.setColor(ContextCompat.getColor(TelegramApplication.sharedApplication().getApplicationContext(), R.color.chat_text));
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setUnderlineText(false);
    }
}
