package com.andremacareno.tdtest.markup;

import android.text.Spannable;
import android.util.Log;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.models.TextMessageModel;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 20/02/16.
 */
public class TelegramMarkupFactory {
    public static Spannable addSpan(String txt, TdApi.MessageEntity[] entities, TextMessageModel.TextMessageLinkDelegate delegate)
    {
        Spannable result = Spannable.Factory.getInstance().newSpannable(txt);
        for(TdApi.MessageEntity entity : entities)
        {
            if(BuildConfig.DEBUG)
                Log.d("MarkupFactory", entity.toString());
            TelegramMarkupSpan suitableSpan;
            if(entity.getConstructor() == TdApi.MessageEntityMention.CONSTRUCTOR)
                suitableSpan = new MentionSpan(delegate, entity, result);
            else if(entity.getConstructor() == TdApi.MessageEntityHashtag.CONSTRUCTOR)
                suitableSpan = new HashtagSpan(delegate, entity, result);
            else if(entity.getConstructor() == TdApi.MessageEntityBotCommand.CONSTRUCTOR)
                suitableSpan = new BotCmdSpan(delegate, entity, result);
            else if(entity.getConstructor() == TdApi.MessageEntityEmail.CONSTRUCTOR)
                suitableSpan = new EmailSpan(delegate, entity, result);
            else if(entity.getConstructor() == TdApi.MessageEntityBold.CONSTRUCTOR)
                suitableSpan = new BoldSpan(delegate, entity, result);
            else if(entity.getConstructor() == TdApi.MessageEntityItalic.CONSTRUCTOR)
                suitableSpan = new ItalicSpan(delegate, entity, result);
            else if(entity.getConstructor() == TdApi.MessageEntityPre.CONSTRUCTOR)
                suitableSpan = new PreSpan(delegate, entity, result);
            else if(entity.getConstructor() == TdApi.MessageEntityCode.CONSTRUCTOR)
                suitableSpan = new CodeSpan(delegate, entity, result);
            else if(entity.getConstructor() == TdApi.MessageEntityPreCode.CONSTRUCTOR)
                suitableSpan = new PreCodeSpan(delegate, entity, result);
            else if(entity.getConstructor() == TdApi.MessageEntityUrl.CONSTRUCTOR)
                suitableSpan = new UrlSpan(delegate, entity, result);
            else if(entity.getConstructor() == TdApi.MessageEntityTextUrl.CONSTRUCTOR)
                suitableSpan = new TextUrlSpan(delegate, entity, result);
            else
                continue;
            suitableSpan.addSpan();
        }
        return result;
    }
}
