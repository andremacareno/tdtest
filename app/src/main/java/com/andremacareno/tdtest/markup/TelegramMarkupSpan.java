package com.andremacareno.tdtest.markup;

import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.ClickableSpan;

import com.andremacareno.tdtest.models.TextMessageModel;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 20/02/16.
 */
public abstract class TelegramMarkupSpan extends ClickableSpan{
    protected TextMessageModel.TextMessageLinkDelegate linkDelegate;
    protected TdApi.MessageEntity markupInfo;
    protected Spannable stringToSpan;
    protected String spannedSubstring;
    public TelegramMarkupSpan(TextMessageModel.TextMessageLinkDelegate delegateRef, TdApi.MessageEntity markupInfo, Spannable stringToSpan)
    {
        this.linkDelegate = delegateRef;
        this.markupInfo = markupInfo;
        this.stringToSpan = stringToSpan;
        this.spannedSubstring = stringToSpan.subSequence(getMarkupStart(), getMarkupEnd()).toString();
    }
    public void addSpan()
    {
        stringToSpan.setSpan(this, getMarkupStart(), getMarkupEnd(), 0);
    }
    protected abstract int getMarkupStart();
    protected abstract int getMarkupEnd();
    public abstract void updateDrawState(TextPaint paint);
}
