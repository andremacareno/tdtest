package com.andremacareno.tdtest.markup;


import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.TextPaint;
import android.view.View;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.fragments.BaseChatFragment;
import com.andremacareno.tdtest.models.TextMessageModel;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 20/02/16.
 */
public class UrlSpan extends TelegramMarkupSpan {

    public UrlSpan(TextMessageModel.TextMessageLinkDelegate delegateRef, TdApi.MessageEntity markupInfo, Spannable stringToSpan) {
        super(delegateRef, markupInfo, stringToSpan);
    }

    @Override
    protected int getMarkupStart() {
        return ((TdApi.MessageEntityUrl) markupInfo).offset;
    }

    @Override
    protected int getMarkupEnd() {
        return ((TdApi.MessageEntityUrl) markupInfo).offset + ((TdApi.MessageEntityUrl) markupInfo).length;
    }

    @Override
    public void onClick(View view) {
        BaseChatFragment.ChatDelegate chatDelegate = linkDelegate.getChatDelegate();
        if(chatDelegate != null)
            chatDelegate.didUrlClicked(spannedSubstring, false);
    }

    @Override
    public void updateDrawState(TextPaint paint) {
        paint.setColor(ContextCompat.getColor(TelegramApplication.sharedApplication().getApplicationContext(), R.color.chat_link));
        paint.setTypeface(Typeface.DEFAULT);
        paint.setUnderlineText(false);
    }
}
