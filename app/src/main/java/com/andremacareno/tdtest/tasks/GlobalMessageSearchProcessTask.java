package com.andremacareno.tdtest.tasks;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.models.GlobalSearchResult;
import com.andremacareno.tdtest.models.MessageSearchResult;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andremacareno on 24/04/15.
 */
public class GlobalMessageSearchProcessTask extends AsyncTaskManTask {
    private ArrayList<GlobalSearchResult> messages = new ArrayList<>();
    private TdApi.Messages msgs;
    public GlobalMessageSearchProcessTask(TdApi.Messages msgs)
    {
        this.msgs = msgs;
    }
    @Override
    public String getTaskKey() {
        return "process_search_messages";
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didChatModelsGenerated;
    }

    @Override
    protected void work() throws Exception {
        if(msgs == null)
            return;
        for(TdApi.Message msg : msgs.messages) {
            TdApi.Chat chat = ChatCache.getInstance().getChatById(msg.chatId);
            MessageSearchResult result = new MessageSearchResult(msg.id, msg.fromId, msg.chatId, CommonTools.dateToString(msg.date, true), chat.title, CommonTools.messageToText(msg), CommonTools.isChatServiceMessage(msg.getConstructor()), FileCache.getInstance().getFileModel(CommonTools.isLowDPIScreen() ? chat.photo.small : chat.photo.big), CommonTools.getInitialsByChatInfo(chat));
            messages.add(result);
        }
    }
    public List<GlobalSearchResult> getResult() {
        return messages;
    }
}
