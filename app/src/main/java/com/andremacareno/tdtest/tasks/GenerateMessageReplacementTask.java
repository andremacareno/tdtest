package com.andremacareno.tdtest.tasks;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.ChannelCache;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.MessageCache;
import com.andremacareno.tdtest.MessageModelFactory;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.UserCache;
import com.andremacareno.tdtest.models.ContactMessageModel;
import com.andremacareno.tdtest.models.MessageModel;
import com.andremacareno.tdtest.models.ReplyMessageShortModel;
import com.andremacareno.tdtest.models.TextMessageModel;
import com.andremacareno.tdtest.models.UserModel;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 24/04/15.
 */
public class GenerateMessageReplacementTask extends AsyncTaskManTask {
    private long chatId;
    private int msgId;
    private String taskKey;
    private MessageModel result;
    private TdApi.MessageContent newContent;
    public GenerateMessageReplacementTask(long chatId, int msgId, TdApi.MessageContent requiredContent)
    {
        this.chatId = chatId;
        this.msgId = msgId;
        this.taskKey = String.format("convert_msg_%d_%d", chatId, msgId);
        this.newContent = requiredContent;
    }

    @Override
    public String getTaskKey() {
        return taskKey;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didMessageModelGenerationFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didMessageModelsGenerated;
    }

    @Override
    protected void work() throws Exception {
        TdApi.Message msg = MessageCache.getInstance().getMessageById(chatId, msgId, true);
        if(msg == null)
            return;
        if(newContent != null)
            msg.content = newContent;
        String asText = CommonTools.messageToText(msg);
        MessageModel newModel = MessageModelFactory.createMsgModel(msg);
        newModel.addStringRepresentation(asText);
        TdApi.Chat chat = ChatCache.getInstance().getChatById(chatId);
        if(chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR && !((TdApi.ChannelChatInfo) chat.type).channel.isSupergroup)
        {
            String initials = CommonTools.getInitialsByChatInfo(chat);
            String name = chat.title;
            newModel.addFromInfo(name, CommonTools.isLowDPIScreen() ? chat.photo.small : chat.photo.big, initials);
            ChannelCache.getInstance().cacheChannel(((TdApi.ChannelChatInfo) chat.type).channel);
        }
        else if(newModel.getFromId() != 0)
        {
            UserModel fromUser = UserCache.getInstance().getUserById(newModel.getFromId());
            String initials = CommonTools.getInitialsFromUserName(fromUser.getFirstName(), fromUser.getLastName());
            String name = fromUser.isDeletedAccount() ? TelegramApplication.sharedApplication().getResources().getString(R.string.unknown_user) : String.format("%s %s", fromUser.getFirstName(), fromUser.getLastName());
            newModel.addFromInfo(name, fromUser.getPhoto().getFile(), initials);
            if(newModel.isChatServiceMessage())
                ((TextMessageModel) newModel).rerunProcessMessageContent(msg.content);
        }
        else
            newModel.addFromInfo(null, new TdApi.File(0, "", 0, ""), null);
        if(newModel.getForwardFromId() != 0)
        {
            if(newModel.forwardedFromChannel())
            {
                try
                {
                    TdApi.Chat channelChat = ChatCache.getInstance().getChatByChannelId(newModel.getForwardFromId());
                    String initials = CommonTools.getInitialsByChatInfo(channelChat);
                    String name = channelChat.title;
                    if(name != null)
                        newModel.addForwardFromInfo(name, CommonTools.isLowDPIScreen() ? channelChat.photo.small : channelChat.photo.big, initials);
                    else
                        newModel.nullForwardInfo();
                }
                catch(Exception e) {
                    newModel.nullForwardInfo();
                    e.printStackTrace(); }
            }
            else
            {
                try
                {
                    UserModel forwardedFromUser = UserCache.getInstance().getUserById(newModel.getForwardFromId());
                    String initials = CommonTools.getInitialsFromUserName(forwardedFromUser.getFirstName(), forwardedFromUser.getLastName());
                    String name = forwardedFromUser.isDeletedAccount() ? TelegramApplication.sharedApplication().getResources().getString(R.string.unknown_user) : String.format("%s %s", forwardedFromUser.getFirstName(), forwardedFromUser.getLastName());
                    if(name != null)
                        newModel.addForwardFromInfo(name, forwardedFromUser.getPhoto().getFile(), initials);
                    else
                        newModel.nullForwardInfo();
                }
                catch(Exception e)
                {
                    newModel.nullForwardInfo();
                    e.printStackTrace();
                }
            }
        }
        else
            newModel.addForwardFromInfo(null, new TdApi.File(0, "", 0, ""), null);
        if(newModel.getSuitableConstructor() == TdApi.MessageContact.CONSTRUCTOR)
        {
            ContactMessageModel mm = (ContactMessageModel) newModel;
            if(mm.getContactUserId() != 0)
            {
                UserModel u = UserCache.getInstance().getUserById(mm.getContactUserId());
                mm.setContactPhoto(u.getPhoto().getFile());
            }
        }
        if(msg.replyToMessageId != 0)
        {
            TdApi.Message replyTo = MessageCache.getInstance().getMessageById(msg.chatId, msg.replyToMessageId);
            if(replyTo != null && replyTo.content.getConstructor() != TdApi.MessageUnsupported.CONSTRUCTOR && replyTo.content.getConstructor() != TdApi.MessageDeleted.CONSTRUCTOR)
            {
                ReplyMessageShortModel replyInfo = new ReplyMessageShortModel(replyTo);
                newModel.addReplyInfo(replyInfo);
            }
        }
        if(msg.viaBotId != 0)
        {
            UserModel user = UserCache.getInstance().getUserById(msg.viaBotId);
            if(user != null)
            {
                String username = user.getDisplayUsername();
                if(username.length() > 1)
                    newModel.addViaInfo(user.getDisplayUsername(), msg.viaBotId);
            }
        }
        this.result = newModel;
    }
    public MessageModel getResult() {
        return result;
    }
}
