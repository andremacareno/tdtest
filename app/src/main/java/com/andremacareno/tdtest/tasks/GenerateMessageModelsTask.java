package com.andremacareno.tdtest.tasks;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.ChannelCache;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.MessageCache;
import com.andremacareno.tdtest.MessageModelFactory;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.UserCache;
import com.andremacareno.tdtest.models.ContactMessageModel;
import com.andremacareno.tdtest.models.DateSectionMessageModel;
import com.andremacareno.tdtest.models.MessageModel;
import com.andremacareno.tdtest.models.PseudoMessageModel;
import com.andremacareno.tdtest.models.ReplyMessageShortModel;
import com.andremacareno.tdtest.models.TextMessageModel;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.viewcontrollers.ChatController;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by andremacareno on 24/04/15.
 */
public class GenerateMessageModelsTask extends AsyncTaskManTask {
    private ArrayList<MessageModel> msgs = new ArrayList<MessageModel>();
    private final String taskKey;
    private final int addingStrategy;
    private final TdApi.Messages messages;
    private int startMessageId;
    private MessageModel startMessageRef = null;
    private long chatId;
    private int[] messageIds;
    private boolean justProcess = false;
    public GenerateMessageModelsTask(TdApi.Messages messages, ChatController controller, int addingStrategy, String taskKey)
    {
        this.chatId = controller.getChatId();
        this.messages = messages;
        this.addingStrategy = addingStrategy;
        this.startMessageId = controller.start_message;
        this.taskKey = taskKey;
    }
    public GenerateMessageModelsTask(TdApi.Message msg, ChatController controller, int addingStrategy, String taskKey)
    {
        this(new TdApi.Messages(1, new TdApi.Message[] {msg}), controller, addingStrategy, taskKey);
    }
    public GenerateMessageModelsTask(TdApi.Message msg)
    {
        this.chatId = msg.chatId;
        this.messages = new TdApi.Messages(1, new TdApi.Message[] {msg});
        this.addingStrategy = -1;
        this.startMessageId = -1;
        this.taskKey = String.format("convert_msg_%d_%d", msg.chatId, msg.id);
        justProcess = true;
    }

    @Override
    public String getTaskKey() {
        return taskKey;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didMessageModelGenerationFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didMessageModelsGenerated;
    }

    @Override
    protected void work() throws Exception {
        String previousDateKey = null;
        String currentDateKey;
        GregorianCalendar cal = new GregorianCalendar();
        messageIds = new int[messages.messages.length];
        int i = 0;
        for (TdApi.Message msg : messages.messages) {
            messageIds[i++] = msg.id;
            String asText = CommonTools.messageToText(msg);
            MessageModel newModel = MessageModelFactory.createMsgModel(msg);
            newModel.addStringRepresentation(asText);
            if(msg.id == startMessageId && startMessageRef == null)
                startMessageRef = newModel;
            TdApi.Chat chat = ChatCache.getInstance().getChatById(chatId);
            if(chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR && !((TdApi.ChannelChatInfo) chat.type).channel.isSupergroup)
            {
                String initials = CommonTools.getInitialsByChatInfo(chat);
                String name = chat.title;
                newModel.addFromInfo(name, CommonTools.isLowDPIScreen() ? chat.photo.small : chat.photo.big, initials);
                ChannelCache.getInstance().cacheChannel(((TdApi.ChannelChatInfo) chat.type).channel);
            }
            else if(newModel.getFromId() != 0)
            {
                UserModel fromUser = UserCache.getInstance().getUserById(newModel.getFromId());
                String initials = CommonTools.getInitialsFromUserName(fromUser.getFirstName(), fromUser.getLastName());
                String name = fromUser.isDeletedAccount() ? TelegramApplication.sharedApplication().getResources().getString(R.string.unknown_user) : String.format("%s %s", fromUser.getFirstName(), fromUser.getLastName());
                newModel.addFromInfo(name, fromUser.getPhoto().getFile(), initials);
                if(newModel.isChatServiceMessage())
                    ((TextMessageModel) newModel).rerunProcessMessageContent(msg.content);
            }
            else
                newModel.addFromInfo(null, new TdApi.File(0, "", 0, ""), null);
            if(newModel.getForwardFromId() != 0)
            {
                if(newModel.forwardedFromChannel())
                {
                    try
                    {
                        TdApi.Chat channelChat = ChatCache.getInstance().getChatByChannelId(newModel.getForwardFromId());
                        String initials = CommonTools.getInitialsByChatInfo(channelChat);
                        String name = channelChat.title;
                        if(name != null)
                            newModel.addForwardFromInfo(name, CommonTools.isLowDPIScreen() ? channelChat.photo.small : channelChat.photo.big, initials);
                        else
                            newModel.nullForwardInfo();
                    }
                    catch(Exception e) {
                        newModel.nullForwardInfo();
                        e.printStackTrace(); }
                }
                else
                {
                    try
                    {
                        UserModel forwardedFromUser = UserCache.getInstance().getUserById(newModel.getForwardFromId());
                        String initials = CommonTools.getInitialsFromUserName(forwardedFromUser.getFirstName(), forwardedFromUser.getLastName());
                        String name = forwardedFromUser.isDeletedAccount() ? TelegramApplication.sharedApplication().getResources().getString(R.string.unknown_user) : String.format("%s %s", forwardedFromUser.getFirstName(), forwardedFromUser.getLastName());
                        if(name != null)
                            newModel.addForwardFromInfo(name, forwardedFromUser.getPhoto().getFile(), initials);
                        else
                            newModel.nullForwardInfo();
                    }
                    catch(Exception e)
                    {
                        newModel.nullForwardInfo();
                        e.printStackTrace();
                    }
                }
            }
            else
                newModel.addForwardFromInfo(null, new TdApi.File(0, "", 0, ""), null);
            if(newModel.getSuitableConstructor() == TdApi.MessageContact.CONSTRUCTOR)
            {
                ContactMessageModel mm = (ContactMessageModel) newModel;
                if(mm.getContactUserId() != 0)
                {
                    UserModel u = UserCache.getInstance().getUserById(mm.getContactUserId());
                    mm.setContactPhoto(u.getPhoto().getFile());
                }
            }
            if(msg.replyToMessageId != 0)
            {
                TdApi.Message replyTo = MessageCache.getInstance().getMessageById(msg.chatId, msg.replyToMessageId);
                if(replyTo != null && replyTo.content.getConstructor() != TdApi.MessageUnsupported.CONSTRUCTOR && replyTo.content.getConstructor() != TdApi.MessageDeleted.CONSTRUCTOR)
                {
                    ReplyMessageShortModel replyInfo = new ReplyMessageShortModel(replyTo);
                    newModel.addReplyInfo(replyInfo);
                }
            }
            if(msg.viaBotId != 0)
            {
                UserModel user = UserCache.getInstance().getUserById(msg.viaBotId);
                if(user != null)
                {
                    String username = user.getDisplayUsername();
                    if(username.length() > 1)
                        newModel.addViaInfo(user.getDisplayUsername(), msg.viaBotId);
                }
            }

            cal.setTimeInMillis(((long) msg.date) * 1000L);
            currentDateKey = String.format("%02d_%02d", cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH));
            if (previousDateKey != null && !currentDateKey.equals(previousDateKey)) {
                PseudoMessageModel dateSeparator = new DateSectionMessageModel(msg);
                if(!justProcess)
                    msgs.add(dateSeparator);
            }
            previousDateKey = currentDateKey;
            msgs.add(newModel);
        }
    }
    public ArrayList<MessageModel> getMessages() {
        return msgs;
    }
    public int getActualLength() { return messages.totalCount; }
    public MessageModel getStartMessageRef()
    {
        return this.startMessageRef;
    }
    public int getAddingStrategy() { return this.addingStrategy; }
    public int[] getMessageIds() { return this.messageIds; }
}
