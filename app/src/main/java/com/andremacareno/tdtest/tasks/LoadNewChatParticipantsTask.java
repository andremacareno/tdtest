package com.andremacareno.tdtest.tasks;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.UserCache;
import com.andremacareno.tdtest.models.UserModel;

import java.util.ArrayList;

/**
 * Created by andremacareno on 18/08/15.
 */
public class LoadNewChatParticipantsTask extends AsyncTaskManTask {
    private final ArrayList<UserModel> participantModels = new ArrayList<>();
    private int[] participants;
    public LoadNewChatParticipantsTask(int[] participants)
    {
        this.participants = participants;
    }
    @Override
    public String getTaskKey() {
        return "newchat_loading_participants";
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didNewChatParticipantsLoaded;
    }

    @Override
    protected void work() throws Exception {
        for(int p : participants)
        {
            UserModel user = UserCache.getInstance().getUserById(p);
            participantModels.add(user);
        }
    }

    public ArrayList<UserModel> getResult() { return this.participantModels; }
}
