package com.andremacareno.tdtest.tasks;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.models.GalleryImageEntry;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.hash.TIntHashSet;

/**
 * Created by andremacareno on 24/04/15.
 */
public class ConvertGalleryEntriesToInputMsgTask extends AsyncTaskManTask {
    private ArrayList<GalleryImageEntry> entries;
    private final TIntObjectMap<GalleryImageEntry> idsToEntries;
    private int[] selection;
    private ArrayList<TdApi.InputMessageContent> inputMessages = new ArrayList<TdApi.InputMessageContent>();
    public ConvertGalleryEntriesToInputMsgTask(ArrayList<GalleryImageEntry> entries, TIntObjectMap<GalleryImageEntry> idsToEntries, TIntHashSet selection)
    {
        this.entries = entries;
        this.idsToEntries = idsToEntries;
        this.selection = selection.toArray();
    }

    @Override
    public String getTaskKey() {
        return "convert_gallery_entries_to_inputMessageContent";
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didQuickSendModelsCreated;
    }

    @Override
    protected void work() throws Exception {
        for(int selectedId : selection)
        {
            synchronized(idsToEntries)
            {
                GalleryImageEntry entry = idsToEntries.get(selectedId);
                if(entry == null)
                    continue;
                TdApi.InputMessageContent msg = new TdApi.InputMessagePhoto(new TdApi.InputFileLocal(entry.getPath()), null);
                inputMessages.add(msg);
            }
        }
    }
    public ArrayList<TdApi.InputMessageContent> getMessages() {
        return this.inputMessages;
    }
}
