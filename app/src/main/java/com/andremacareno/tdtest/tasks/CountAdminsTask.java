package com.andremacareno.tdtest.tasks;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.ChannelCache;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.GroupCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 23.04.2015.
 */
public class CountAdminsTask extends AsyncTaskManTask {
    private long chatId;
    private int adminsCount = 0;
    public CountAdminsTask(long chatId) {
        this.chatId = chatId;
    }

    @Override
    public String getTaskKey() {
        return String.format("count_admins_of_chat_%d", chatId);
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didChatAdminsCounted;
    }

    @Override
    protected void work() throws Exception {
        TdApi.Chat chat = ChatCache.getInstance().getChatById(chatId);
        if(chat == null)
            return;
        if(chat.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR)
        {
            adminsCount = 1;
            return;
        }
        else if(chat.type.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR)
        {
            int groupId = ((TdApi.GroupChatInfo) chat.type).group.id;
            TdApi.GroupFull group = GroupCache.getInstance().getGroupFull(groupId, false);
            if(group == null)
            {
                adminsCount = 1;
                return;
            }
            else
            {
                for(TdApi.ChatParticipant participant : group.participants)
                {
                    if(participant.role.getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor())
                        adminsCount++;
                }
            }
        }
        else if(chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR)
        {
            int channelId = ((TdApi.ChannelChatInfo) chat.type).channel.id;
            TdApi.ChannelFull ch = ChannelCache.getInstance().getChannelFull(channelId);
            this.adminsCount = ch.adminsCount;
        }
    }
    public int getResult() { return this.adminsCount; }
}
