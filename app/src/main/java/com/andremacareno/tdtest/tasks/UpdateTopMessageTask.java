package com.andremacareno.tdtest.tasks;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.MessageCache;
import com.andremacareno.tdtest.models.ChatModel;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 24/04/15.
 */
public class UpdateTopMessageTask extends AsyncTaskManTask {
    private ChatModel chat;
    private TdApi.Message newMessage;
    private int msgId;
    public UpdateTopMessageTask(TdApi.Message newMsg, ChatModel chat)
    {
        this.newMessage = newMsg;
        this.chat = chat;
    }
    public UpdateTopMessageTask(int msgId, ChatModel chat)
    {
        this.newMessage = null;
        this.msgId = msgId;
        this.chat = chat;
    }
    @Override
    public String getTaskKey() {
        return "update_top_message";
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didTopMessageUpdateFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didTopMessageUpdated;
    }

    @Override
    protected void work() throws Exception {
        if(chat != null && newMessage == null && msgId != 0)
            newMessage = MessageCache.getInstance().getMessageById(chat.getId(), msgId);
        if(newMessage != null && chat != null)
            chat.updateTopMessage(newMessage);
    }
}
