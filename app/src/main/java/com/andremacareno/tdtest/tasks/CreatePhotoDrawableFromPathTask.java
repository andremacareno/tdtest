package com.andremacareno.tdtest.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.images.RecyclingBitmapDrawable;

import java.io.RandomAccessFile;

/**
 * Created by Andrew on 08.05.2015.
 */
public class CreatePhotoDrawableFromPathTask extends AsyncTaskManTask {
    private RecyclingBitmapDrawable bitmapDrawable;
    private String path;
    public CreatePhotoDrawableFromPathTask(String p)
    {
        this.path = p;

    }
    @Override
    public String getTaskKey() {
        return null;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didPhotoDrawableCreationFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didPhotoDrawableCreated;
    }

    @Override
    protected void work() throws Exception {
        RandomAccessFile img = new RandomAccessFile(this.path, "r");
        Bitmap bmp = BitmapFactory.decodeFileDescriptor(img.getFD());
        img.close();
        if(bmp != null)
            bitmapDrawable = new RecyclingBitmapDrawable(TelegramApplication.sharedApplication().getResources(), bmp);
        else
            throw new Exception("image doesn't exist");
    }
    public RecyclingBitmapDrawable getDrawable() { return this.bitmapDrawable; }
}
