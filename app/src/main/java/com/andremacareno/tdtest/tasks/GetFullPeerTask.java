package com.andremacareno.tdtest.tasks;

import android.util.Log;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.ChannelCache;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.GroupCache;
import com.andremacareno.tdtest.PeerType;
import com.andremacareno.tdtest.UserCache;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 15/05/15.
 */
public class GetFullPeerTask extends AsyncTaskManTask {
    private Object peerFull = null;
    private int peerId = 0;
    private long chatId = 0;
    private PeerType type;
    private final String taskKey;

    public GetFullPeerTask(int peer, PeerType type)
    {
        this.peerId = peer;
        this.type = type;
        if(type == PeerType.USER)
            this.taskKey = String.format("getuserfull_%d_task", peerId);
        else if(type == PeerType.CHANNEL)
            this.taskKey = String.format("getchannelfull_%d_task", peerId);
        else
            this.taskKey = String.format("getgroupfull_%d_task", peerId);
    }
    public GetFullPeerTask(long chatId)
    {
        this.chatId = chatId;
        this.taskKey = String.format("getpeerfull_%d_task", chatId);
    }

    @Override
    public String getTaskKey() {
        return taskKey;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        if(type == PeerType.USER)
            return NotificationCenter.didUserFullForChatScreenReceived;
        else if(type == PeerType.GROUP)
            return NotificationCenter.didGroupFullReceived;
        return NotificationCenter.didChannelFullForChatScreenReceived;
    }

    @Override
    protected void work() throws Exception {
        if(BuildConfig.DEBUG)
            Log.d("GetFullPeer", String.format("peerId = %d; chatId = %d", peerId, chatId));
        if(peerId != 0)
        {
            if(type == PeerType.USER) {
                this.peerFull = UserCache.getInstance().getFullUserById(peerId);
                if(BuildConfig.DEBUG)
                    Log.d("GetFullPeer", "got peer");
                this.chatId = ChatCache.getInstance().getChatByUserId(peerId).id;
                if(BuildConfig.DEBUG)
                    Log.d("GetFullPeer", "got chat");
            }
            else if(type == PeerType.GROUP) {
                this.peerFull = GroupCache.getInstance().getGroupFull(peerId, false);
                this.chatId = ChatCache.getInstance().getChatByGroupId(peerId).id;
            }
            else if(type == PeerType.CHANNEL) {
                this.peerFull = ChannelCache.getInstance().getChannelFull(peerId);
                this.chatId = ChatCache.getInstance().getChatByChannelId(peerId).id;
            }
        }
        else if(chatId != 0)
        {
            TdApi.Chat chat = ChatCache.getInstance().getChatById(chatId);
            if(chat.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR) {
                this.peerId = ((TdApi.PrivateChatInfo) chat.type).user.id;
                this.peerFull = UserCache.getInstance().getFullUserById(peerId);
            }
            else if(chat.type.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR) {
                this.peerId = ((TdApi.GroupChatInfo) chat.type).group.id;
                this.peerFull = GroupCache.getInstance().getGroupFull(peerId, false);
            }
            else if(chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR) {
                this.peerId = ((TdApi.ChannelChatInfo) chat.type).channel.id;
                this.peerFull = ChannelCache.getInstance().getChannelFull(peerId);
            }
        }
        if(BuildConfig.DEBUG)
            Log.d("GetFullPeer", String.format("peerFull = %s", peerFull == null ? "null" : peerFull.toString()));
    }
    public Object getResult() {
        if(BuildConfig.DEBUG)
            Log.d("GetFullPeer", String.format("getResult(): peerFull = %s", peerFull == null ? "null" : peerFull.toString()));
        return this.peerFull; }
    public long getChatId() { return this.chatId; }
}
