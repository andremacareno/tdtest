package com.andremacareno.tdtest.tasks;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.models.BotCommandModel;
import com.andremacareno.tdtest.models.UserModel;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

/**
 * Created by andremacareno on 15/05/15.
 */
public class GenerateBotCommandModelsTask extends AsyncTaskManTask {
    private UserModel bot;
    private TdApi.ChatParticipant[] bots;
    private String taskKey;
    private ArrayList<BotCommandModel> result;
    public GenerateBotCommandModelsTask(UserModel bot, long chatId)
    {
        this.bot = bot;
        this.bots = null;
        this.taskKey = String.format("chat%d_generate_bot_cmd_list_task", chatId);
        this.result = new ArrayList<>();
    }
    public GenerateBotCommandModelsTask(TdApi.ChatParticipant[] botList, long chatId)
    {
        this.bots = botList;
        this.taskKey = String.format("chat%d_generate_bot_cmd_list_task", chatId);
        this.result = new ArrayList<>();
    }
    @Override
    public String getTaskKey() {
        return taskKey;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didBotCommandProcessed;
    }

    @Override
    protected void work() throws Exception {
        if(bots != null)
        {
            for(TdApi.ChatParticipant chatBot : bots)
            {
                if(chatBot.botInfo.getConstructor() != TdApi.BotInfoGeneral.CONSTRUCTOR)
                    continue;
                TdApi.BotCommand[] commands = ((TdApi.BotInfoGeneral)chatBot.botInfo).commands;
                for(TdApi.BotCommand cmd : commands)
                {
                    BotCommandModel cmdModel = new BotCommandModel(new UserModel(chatBot.user), cmd);
                    result.add(cmdModel);
                }
            }
        }
        else if(bot != null)
        {
            if(bot.getBotInfo() == null || bot.getBotInfo().getConstructor() != TdApi.BotInfoGeneral.CONSTRUCTOR)
                return;
            TdApi.BotCommand[] commands = ((TdApi.BotInfoGeneral)bot.getBotInfo()).commands;
            for(TdApi.BotCommand cmd : commands)
            {
                BotCommandModel cmdModel = new BotCommandModel(bot, cmd);
                result.add(cmdModel);
            }
        }
    }
    public ArrayList<BotCommandModel> getResult() { return this.result; }
}
