package com.andremacareno.tdtest.tasks;

import android.net.Uri;
import android.os.Environment;

import com.andremacareno.asynccore.AsyncTaskManTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by andremacareno on 05/05/15.
 */
public class CreateFileForCameraTask extends AsyncTaskManTask {
    private String photo_path;
    private Uri photo_uri;
    public CreateFileForCameraTask()
    {
    }

    @Override
    public String getTaskKey() {
        return null;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return 0;
    }

    @Override
    protected void work() throws Exception {
        File photoFile = createImageFile();
        if (photoFile != null) {
            Uri fileUri = Uri.fromFile(photoFile);
            this.photo_uri = fileUri;
            this.photo_path = fileUri.getPath();
        }
        else
            throw new NullPointerException("null photo reference");
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        this.photo_path = image.getAbsolutePath();
        return image;
    }
    public String getPhotoPath() { return this.photo_path; }
    public Uri getPhotoUri() { return this.photo_uri; }
}
