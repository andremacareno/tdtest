package com.andremacareno.tdtest.tasks;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.models.SharedMedia;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

/**
 * Created by Andrew on 28.04.2015.
 */
public class GenerateSharedImgListTask extends AsyncTaskManTask {
    private String taskKey;
    private TdApi.Message[] source;
    private ArrayList<SharedMedia> models = new ArrayList<>();
    public GenerateSharedImgListTask(long chatId, TdApi.Message[] messages)
    {
        this.taskKey = String.format("chat%d_shared_img_processing", chatId);
        this.source = messages;
    }
    @Override
    public String getTaskKey() {
        return this.taskKey;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didSharedImageModelsCreated;
    }

    @Override
    protected void work() throws Exception {

    }
    public ArrayList<SharedMedia> getResult() { return this.models; }
}
