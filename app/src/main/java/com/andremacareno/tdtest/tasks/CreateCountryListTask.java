package com.andremacareno.tdtest.tasks;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.Country;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * Created by Andrew on 23.04.2015.
 */
public class CreateCountryListTask extends AsyncTaskManTask {
    private ArrayList<Country> countries;
    private TIntObjectHashMap<Country> index;
    ArrayList<Country> unsorted = new ArrayList<Country>();
    public CreateCountryListTask(ArrayList<Country> listRef, TIntObjectHashMap<Country> index) {
        this.countries = listRef;
        this.index = index;
    }

    @Override
    public String getTaskKey() {
        return "create_country_list";
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didCountryModelGenerationFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didCountryModelsGenerated;
    }

    @Override
    protected void work() throws Exception {
        try {
            InputStream stream = TelegramApplication.sharedApplication().getResources().getAssets().open("countries.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] args = line.split(";");
                Country c = new Country(args[0], args[2]);
                unsorted.add(c);
                int code = Integer.valueOf(args[0]);
                index.put(code, c);
            }
            reader.close();
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Collections.sort(unsorted, new Comparator<Country>() {
            @Override
            public int compare(Country lhs, Country rhs) {
                return lhs.getFullName().compareTo(rhs.getFullName());
            }
        });
        countries.addAll(0, unsorted);
    }
}
