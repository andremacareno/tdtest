package com.andremacareno.tdtest.tasks;

import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.TelegramApplication;

/**
 * Created by Andrew on 07.05.2015.
 */
public class AudioPathTask extends AsyncTaskManTask {
    private Uri link;
    private String selectedImagePath;
    public AudioPathTask() {}

    public void setLink(Uri uri) {
        this.link = uri;
    }
    @Override
    public String getTaskKey() {
        return "gallery_image_path_task";
    }
    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didAudioPathRetrieveFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didAudioPathRetrieved;
    }

    @Override
    protected void work() throws Exception {
        if(Build.VERSION.SDK_INT >= 19)
            selectedImagePath = getKitkatPath();
        else
            selectedImagePath = getPath();
    }
    private String getPath() {
        String[] projection = { MediaStore.Audio.Media.DATA };
        Cursor cursor = TelegramApplication.sharedApplication().getContentResolver().query(link, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
            cursor.moveToFirst();
            String cur = cursor.getString(column_index);
            cursor.close();
            return cur;
        }
        return link.getPath();
    }
    private String getKitkatPath()
    {
        String id = link.getLastPathSegment().split(":")[1];
        final String[] imageColumns = {MediaStore.Audio.Media.DATA };
        Uri uri = getUri();

        Cursor imageCursor = TelegramApplication.sharedApplication().getContentResolver().query(uri, imageColumns,
                MediaStore.Audio.Media._ID + "="+id, null, null);

        if (imageCursor.moveToFirst()) {
            String ret = imageCursor.getString(imageCursor.getColumnIndex(MediaStore.Audio.Media.DATA));
            imageCursor.close();
            return ret;
        }
        return null;
    }
    private Uri getUri() {
        String state = Environment.getExternalStorageState();
        if(!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
            return MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
        return MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
    }
    public String getImagePath() { return this.selectedImagePath; }
}
