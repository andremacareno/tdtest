package com.andremacareno.tdtest.tasks;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.models.AudioTrackModel;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import gnu.trove.set.hash.TIntHashSet;

/**
 * Created by andremacareno on 18/08/15.
 */
public class CreatePlaylistTask extends AsyncTaskManTask {
    private volatile boolean cancelFlag = false;
    private volatile int startFromMessageId = 0;
    private long chatId;
    private boolean keepSearch = false;
    private final Object monitor = new Object();
    private final ArrayList<AudioTrackModel> playlist = new ArrayList<>();
    private AudioTrackModel initTrack;
    private final TIntHashSet writtenAudio;
    public CreatePlaylistTask(long chatId, AudioTrackModel initTrack, TIntHashSet writtenAudio)
    {
        this.writtenAudio = writtenAudio;
        this.chatId = chatId;
        this.initTrack = initTrack;
    }
    @Override
    public String getTaskKey() {
        return String.format("chat%d_playlist_loading", chatId);
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didPlaylistLoaded;
    }

    @Override
    protected void work() throws Exception {
        boolean stop = false;
        while(!stop)
        {
            if(cancelFlag)
                stop = true;
            else
            {
                ArrayList<AudioTrackModel> tracks = getTracks();
                if(tracks.isEmpty()) {
                    if(!keepSearch)
                        stop = true;
                }
                else
                    playlist.addAll(tracks);
            }
        }
    }
    public void cancel()
    {
        this.cancelFlag = true;
    }
    private ArrayList<AudioTrackModel> getTracks()
    {
        keepSearch = false;
        final AtomicBoolean ready = new AtomicBoolean(false);
        final ArrayList<AudioTrackModel> ret = new ArrayList<>();
        TdApi.SearchChatMessages getTracks = new TdApi.SearchChatMessages(chatId, "", startFromMessageId, 20, false, new TdApi.SearchMessagesFilterAudio());
        TG.getClientInstance().send(getTracks, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(object.getConstructor() == TdApi.Messages.CONSTRUCTOR)
                {
                    TdApi.Message[] msgs = ((TdApi.Messages) object).messages;
                    for(TdApi.Message msg : msgs)
                    {
                        startFromMessageId = msg.id;
                        synchronized(writtenAudio)
                        {
                            if(writtenAudio.contains(((TdApi.MessageAudio) msg.content).audio.audio.id)) {
                                keepSearch = true;
                                continue;
                            }
                        }
                        if(msg.content.getConstructor() != TdApi.MessageAudio.CONSTRUCTOR)
                            continue;
                        AudioTrackModel track = new AudioTrackModel(msg.id, msg.date, ((TdApi.MessageAudio) msg.content).audio);
                        if(initTrack != null && initTrack.equals(track))
                            continue;
                        synchronized(writtenAudio)
                        {
                            writtenAudio.add(track.getTrack().getFileId());
                        }
                        synchronized(ret)
                        {
                            ret.add(track);
                        }
                    }
                }
                ready.set(true);
                synchronized(monitor)
                {
                    monitor.notifyAll();
                }
            }
        });
        while (!ready.get()) {
            synchronized(monitor)
            {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
    public boolean isCancelled() { return this.cancelFlag; }
    public long getChatId() { return this.chatId; }
    public ArrayList<AudioTrackModel> getPlaylist() { return this.playlist; }
}
