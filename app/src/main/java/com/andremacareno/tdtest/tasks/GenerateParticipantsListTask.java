package com.andremacareno.tdtest.tasks;

import android.util.Log;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.GroupCache;
import com.andremacareno.tdtest.models.ChatParticipantModel;
import com.andremacareno.tdtest.models.UserModel;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by andremacareno on 24/04/15.
 */
public class GenerateParticipantsListTask extends AsyncTaskManTask {
    private ArrayList<ChatParticipantModel> participants = new ArrayList<>();
    private final Object monitor = new Object();
    private int groupId;
    private int offset, limit;
    private boolean adminsOnly;
    public GenerateParticipantsListTask(int channelId, int offset, int limit, boolean adminsOnly)
    {
        this.groupId = channelId;
        this.offset = offset;
        this.limit = limit;
        this.adminsOnly = adminsOnly;
    }
    public GenerateParticipantsListTask(int groupId)
    {
        this(groupId, 0, 0, false);
    }

    @Override
    public String getTaskKey() {
        return String.format("generate_participants_list_%d_%s_%d_%d", groupId, limit > 0 ? "channel" : "group", offset, limit);
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didChatParticipantsLoaded;
    }

    @Override
    protected void work() throws Exception {
        if(limit > 0)
        {
            final AtomicBoolean ready = new AtomicBoolean(false);
            TdApi.GetChannelParticipants getParticipants = new TdApi.GetChannelParticipants(groupId, adminsOnly ? Constants.CHANNEL_GET_ADMINS : Constants.CHANNEL_GET_RECENT, offset, limit);
            TG.getClientInstance().send(getParticipants, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    try {
                        if (object != null && object.getConstructor() == TdApi.ChatParticipants.CONSTRUCTOR) {
                            TdApi.ChatParticipants result = (TdApi.ChatParticipants) object;
                            for (TdApi.ChatParticipant p : result.participants) {
                                ChatParticipantModel user = new ChatParticipantModel(p);
                                participants.add(user);
                            }
                        } else if (object != null && BuildConfig.DEBUG)
                            Log.d("GenerateParticipants", object.toString());
                    } finally {
                        ready.set(true);
                        synchronized (monitor) {
                            monitor.notifyAll();
                        }
                    }
                }
            });
            while (!ready.get()) {
                synchronized(monitor)
                {
                    try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else
        {
            TdApi.GroupFull chatInfo = GroupCache.getInstance().getGroupFull(groupId, false);
            if(chatInfo == null)
                return;

            for(TdApi.ChatParticipant p : chatInfo.participants)
            {
                ChatParticipantModel user = new ChatParticipantModel(p);
                participants.add(user);
            }
            Collections.sort(participants, new Comparator<UserModel>() {
                @Override
                public int compare(UserModel userModel, UserModel userModel2) {
                    if (userModel.isOnline() && userModel2.isOnline())
                        return 0;
                    else if (userModel.isOnline())
                        return -1;
                    else {
                        if (userModel.getLastSeenDate() == userModel2.getLastSeenDate())
                            return 0;
                        else if (userModel.getLastSeenDate() > userModel2.getLastSeenDate())
                            return -1;
                    }
                    return 1;
                }
            });
        }
    }
    public ArrayList<ChatParticipantModel> getParticipants() {
        return participants;
    }
}
