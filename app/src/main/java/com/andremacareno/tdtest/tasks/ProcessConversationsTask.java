package com.andremacareno.tdtest.tasks;

import android.util.Log;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.ChannelCache;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.GroupCache;
import com.andremacareno.tdtest.MessageCache;
import com.andremacareno.tdtest.models.ChatModel;
import com.andremacareno.tdtest.models.ChatModelFactory;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.List;

import gnu.trove.set.hash.TLongHashSet;

/**
 * Created by andremacareno on 24/04/15.
 */
public class ProcessConversationsTask extends AsyncTaskManTask {
    private final ArrayList<ChatModel> chats = new ArrayList<ChatModel>();
    private TLongHashSet currentlyProcessingSet = new TLongHashSet();
    private TdApi.Chats chatObjects;
    private boolean groupsOnly;
    private boolean shareableOnly;
    public ProcessConversationsTask(TdApi.Chats chats, boolean groupsOnly)
    {
        this.chatObjects = chats;
        this.groupsOnly = groupsOnly;
        this.shareableOnly = false;
    }
    public ProcessConversationsTask(TdApi.Chats chats, boolean groupsOnly, boolean shareableOnly)
    {
        this(chats, groupsOnly);
        this.shareableOnly = shareableOnly;
    }
    public ProcessConversationsTask(TdApi.Chat chat, boolean groupsOnly, boolean shareableOnly)
    {
        this(new TdApi.Chats(new TdApi.Chat[] {chat}), groupsOnly, shareableOnly);
    }
    @Override
    public String getTaskKey() {
        return "process_chats_".concat(chats.toString());
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didChatModelGeneratingFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didChatModelsGenerated;
    }

    @Override
    protected void work() throws Exception {
        if(BuildConfig.DEBUG) {
            Log.d("ProcessConversations", String.format("started task; chats = %s", chatObjects.toString()));
        }
        synchronized(chats)
        {
            for(TdApi.Chat dialog : chatObjects.chats) {
                if(dialog.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR && groupsOnly) {
                    if(BuildConfig.DEBUG)
                        Log.d("ProcessConversations", "groupsOnly; skipping private chat...");
                    continue;
                }
                else if(dialog.type.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR)
                {
                    TdApi.Group group = ((TdApi.GroupChatInfo) dialog.type).group;
                    if((group.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor() || group.role.getConstructor() == Constants.PARTICIPANT_KICKED.getConstructor()) && shareableOnly) {
                        if(BuildConfig.DEBUG)
                            Log.d("ProcessConversations", "shareableOnly; skipping group...");
                        continue;
                    }
                }
                else if(dialog.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR)
                {
                    TdApi.Channel channel = ((TdApi.ChannelChatInfo) dialog.type).channel;
                    if(channel.isSupergroup && (channel.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor() || channel.role.getConstructor() == Constants.PARTICIPANT_KICKED.getConstructor()) && shareableOnly)
                        continue;
                    if(!channel.isSupergroup && shareableOnly && (channel.role.getConstructor() != Constants.PARTICIPANT_EDITOR.getConstructor() && channel.role.getConstructor() != Constants.PARTICIPANT_ADMIN.getConstructor()))
                        continue;
                }
                currentlyProcessingSet.add(dialog.id);
                ChatModel model = ChatModelFactory.createInstance(dialog);
                if(model.isGroupChat()) {
                    ChatCache.getInstance().setGroupLink(model.getId(), model.getPeer());
                    GroupCache.getInstance().cacheGroup(((TdApi.GroupChatInfo) dialog.type).group);
                }
                else if(model.isChannel()) {
                    ChatCache.getInstance().setChannelLink(model.getId(), ((TdApi.ChannelChatInfo) dialog.type).channel.id);
                    ChannelCache.getInstance().cacheChannel(((TdApi.ChannelChatInfo) dialog.type).channel);
                }
                if(model.getReplyMarkupMsgId() != 0)
                    MessageCache.getInstance().cacheMessage(model.getId(), model.getReplyMarkupMsgId());
                if(BuildConfig.DEBUG)
                    Log.d("ProcessConversations", "chats.add()");
                chats.add(model);
            }
        }
    }
    public List<ChatModel> getChats() {
        if(BuildConfig.DEBUG)
            Log.d("ProcessConversations", "getChats()");
        synchronized(chats)
        {
            return chats;
        }
    }
}
