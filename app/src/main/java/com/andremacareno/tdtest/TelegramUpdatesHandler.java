package com.andremacareno.tdtest;

import android.os.Looper;
import android.util.Log;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.updatelisteners.TelegramUpdateListener;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

import gnu.trove.TCollections;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.TIntSet;

/**
 * Created by andremacareno on 03/04/15.
 */
public class TelegramUpdatesHandler extends Thread implements Client.ResultHandler, LogOutDelegate {

    private final TIntObjectMap<CopyOnWriteArrayList<TelegramUpdateListener>> updateObservers = TCollections.synchronizedMap(new TIntObjectHashMap<CopyOnWriteArrayList<TelegramUpdateListener>>());
    public TelegramUpdatesHandler()
    {
    }
    @Override
    public void run()
    {
        Looper.prepare();
        TG.setUpdatesHandler(this);
        Looper.loop();
    }

    private final String TAG = "UpdateService";
    //add/remove observer methods
    public boolean addObserver(TelegramUpdateListener obs)
    {
        int observableConstructor = obs.getObservableConstructor();
        synchronized (updateObservers) {
            if (updateObservers.get(observableConstructor) == null)
                updateObservers.put(observableConstructor, new CopyOnWriteArrayList<TelegramUpdateListener>());
        }
        synchronized(updateObservers) {
            if (!updateObservers.get(observableConstructor).contains(obs)) {
                updateObservers.get(observableConstructor).add(obs);
                return true;
            }
        }
        return false;
    }
    public void removeObserver(TelegramUpdateListener obs)
    {
        int observableConstructor = obs.getObservableConstructor();
        String tag = obs.getKey();
        synchronized(updateObservers)
        {
            if(updateObservers.get(observableConstructor) != null && updateObservers.get(observableConstructor).contains(obs)) {
                updateObservers.get(observableConstructor).remove(obs);
            }
        }
    }
    //broadcast update to observers
    public void broadcastUpdate(TdApi.Update upd)
    {
        synchronized(updateObservers)
        {
            if(updateObservers.get(upd.getConstructor()) != null)
            {
                Iterator<TelegramUpdateListener> it = updateObservers.get(upd.getConstructor()).iterator();
                while(it.hasNext())
                {
                    TelegramUpdateListener obs = it.next();
                    obs.onResult(upd);
                }
            }
        }
    }

    //Client.ResultHandler implementation
    @Override
    public void onResult(TdApi.TLObject object) {
        try
        {
            if(object == null)
                return;
            if(object.getConstructor() == TdApi.UpdateStickers.CONSTRUCTOR) {
                StickersCache.invalidate();
                StickersCache.checkStickers();
            }
            else if(object.getConstructor() == TdApi.UpdateSavedAnimations.CONSTRUCTOR) {
                GifKeyboardCache.invalidate();
                GifKeyboardCache.checkGifs();
            }
            broadcastUpdate((TdApi.Update) object);
        }
        catch(Exception e) {
            if(BuildConfig.DEBUG)
                e.printStackTrace();
        }
    }
    //LogOutDelegate implementation
    @Override
    public void didLogOutRequested() {
        AsyncTaskManTask clearTask = new AsyncTaskManTask() {
            @Override
            public String getTaskKey() {
                return "clear_task";
            }

            @Override
            public int getTaskFailedNotificationId() {
                return NotificationCenter.didLogOutFailed;
            }

            @Override
            public int getTaskCompletedNotificationId() {
                return NotificationCenter.didLoggedOut;
            }

            @Override
            protected void work() throws Exception {
                removeAllObservers();
            }
        };
        clearTask.addToQueue();
    }
    public void removeAllObservers()
    {
        synchronized(updateObservers)
        {
            if(updateObservers.isEmpty() && BuildConfig.DEBUG)
            {
                Log.d(TAG, "requested updateservice cleanup but updateObservers is empty");
            }
            if(BuildConfig.DEBUG)
                Log.d(TAG, "wiping observer list");
            TIntSet keys = updateObservers.keySet();
            TIntIterator it = keys.iterator();
            while(it.hasNext()) {
                CopyOnWriteArrayList<TelegramUpdateListener> lst = updateObservers.get(it.next());
                lst.clear();
            }
        }
    }

}

