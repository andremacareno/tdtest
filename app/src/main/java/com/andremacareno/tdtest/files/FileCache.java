package com.andremacareno.tdtest.files;

import android.os.Message;
import android.util.Log;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.updatelisteners.TelegramUpdateListener;

import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.concurrent.ConcurrentHashMap;

import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * Created by andremacareno on 24/04/15.
 */
public class FileCache {
    public static final int RECEIVING_UPDATE_FILE = 0;
    public static final int RECEIVING_UPDATE_FILE_PROGRESS = 1;
    private static volatile FileCache _instance;
    private final TIntObjectHashMap<FileModel> files = new TIntObjectHashMap<>();
    private final TdApi.DownloadFile downloadFileFunc = new TdApi.DownloadFile();
    private final TdApi.CancelDownloadFile cancelDownloadFileFunc = new TdApi.CancelDownloadFile();
    private TelegramUpdateListener updateFileProgressListener, updateFileListener;
    private PauseHandler updateHandler;
    private volatile boolean observersAttached = false;
    private FileCache() {
        updateHandler = new UpdateBridge();
        updateFileProgressListener = new TelegramUpdateListener() {
            @Override
            public int getObservableConstructor() {
                return TdApi.UpdateFileProgress.CONSTRUCTOR;
            }

            @Override
            public String getKey() {
                return Constants.FILECACHE_UPDATE_FILE_PROGRESS_OBSERVER;
            }

            @Override
            public void onResult(TdApi.TLObject object) {
                TdApi.UpdateFileProgress upd = (TdApi.UpdateFileProgress) object;
                if(BuildConfig.DEBUG)
                    Log.d("FileCache", upd.toString());
                FileModel model = getFileModelById(upd.fileId);
                if(model == null) {
                    return;
                }
                model.updateLoadedSize(upd.ready);
                ConcurrentHashMap<String, FileModel.FileModelDelegate> delegates = model.getDelegates();
                for(String key : delegates.keySet())
                {
                    FileModel.FileModelDelegate delegate = delegates.get(key);
                    if(delegate != null)
                    {
                        Message msg = Message.obtain(updateHandler);
                        msg.what = RECEIVING_UPDATE_FILE_PROGRESS;
                        msg.obj = delegate;
                        msg.sendToTarget();
                    }
                }
            }
        };
        updateFileListener = new TelegramUpdateListener() {
            @Override
            public int getObservableConstructor() {
                return TdApi.UpdateFile.CONSTRUCTOR;
            }

            @Override
            public String getKey() {
                return Constants.FILECACHE_UPDATE_FILE_OBSERVER;
            }

            @Override
            public void onResult(TdApi.TLObject object) {
                TdApi.UpdateFile upd = (TdApi.UpdateFile) object;
                TdApi.File localFile = upd.file;
                FileModel model = getFileModel(localFile);
                model.markDownloaded(localFile);
                ConcurrentHashMap<String, FileModel.FileModelDelegate> delegates = model.getDelegates();
                for(String key : delegates.keySet())
                {
                    FileModel.FileModelDelegate delegate = delegates.get(key);
                    if(delegate != null)
                    {
                        Message msg = Message.obtain(updateHandler);
                        msg.what = RECEIVING_UPDATE_FILE;
                        msg.obj = delegate;
                        msg.sendToTarget();
                    }
                }
            }
        };
    }
    public static FileCache getInstance()
    {
        if(_instance == null)
        {
            synchronized(FileCache.class)
            {
                if(_instance == null)
                    _instance = new FileCache();
            }
        }
        return _instance;
    }
    public void clear()
    {
        synchronized(files)
        {
            files.clear();
        }
        removeObservers();
    }
    public FileModel getFileModelById(int fileId)
    {
        synchronized(files)
        {
            if(files.containsKey(fileId))
                return files.get(fileId);
        }
        return null;
    }
    public FileModel getFileModel(TdApi.File file)
    {
        int fileId = file.id;
        FileModel f;
        synchronized(files)
        {
            if(files.containsKey(fileId))
                f = files.get(fileId);
            else {
                f = FileModel.createInstance(file);
                files.put(fileId, f);
            }
        }
        return f;
    }
    public void downloadFile(FileModel fileModel)
    {
        if(fileModel == null || fileModel.getDownloadState() != FileModel.DownloadState.EMPTY)
            return;
        synchronized(downloadFileFunc)
        {
            fileModel.markLoading();
            ConcurrentHashMap<String, FileModel.FileModelDelegate> delegates = fileModel.getDelegates();
            for(String key : delegates.keySet())
            {
                FileModel.FileModelDelegate delegate = delegates.get(key);
                if(delegate != null)
                    delegate.didFileDownloadStarted();
            }
            downloadFileFunc.fileId = fileModel.getFileId();
            TG.getClientInstance().send(downloadFileFunc, TelegramApplication.dummyResultHandler);
        }
    }
    public void cancelDownload(FileModel fileModel)
    {
        if(fileModel == null)
            return;
        synchronized(cancelDownloadFileFunc)
        {
            fileModel.cancelLoading();
            ConcurrentHashMap<String, FileModel.FileModelDelegate> delegates = fileModel.getDelegates();
            for(String key : delegates.keySet())
            {
                FileModel.FileModelDelegate delegate = delegates.get(key);
                if(delegate != null)
                    delegate.didFileDownloadCancelled();
            }
            cancelDownloadFileFunc.fileId = fileModel.getFileId();
            TG.getClientInstance().send(cancelDownloadFileFunc, TelegramApplication.dummyResultHandler);
        }
    }
    public void didUiThreadPaused()
    {
        updateHandler.pause();
    }
    public void didUiThreadResumed()
    {
        updateHandler.resume();
    }
    public void attachObservers()
    {
        TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
        if(service == null)
            return;
        if(observersAttached)
            return;
        observersAttached = true;
        service.addObserver(updateFileListener);
        service.addObserver(updateFileProgressListener);
    }
    public void removeObservers()
    {
        observersAttached = false;
        TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
        if(service == null)
            return;
        service.removeObserver(updateFileProgressListener);
        service.removeObserver(updateFileListener);
    }


    private static class UpdateBridge extends PauseHandler
    {
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }
        @Override
        protected void processMessage(Message message) {
            FileModel.FileModelDelegate delegate = (FileModel.FileModelDelegate) message.obj;
            if(message.what == RECEIVING_UPDATE_FILE)
                delegate.didFileDownloaded();
            else if(message.what == RECEIVING_UPDATE_FILE_PROGRESS) {
                delegate.didFileProgressUpdated();
            }
        }
    }
}
