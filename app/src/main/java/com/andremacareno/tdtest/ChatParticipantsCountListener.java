package com.andremacareno.tdtest;

/**
 * Created by Andrew on 14.04.2015.
 */
public interface ChatParticipantsCountListener {
    public void didChatParticipantsCountChanged(int newCount);
}
