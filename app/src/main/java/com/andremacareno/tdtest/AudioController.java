package com.andremacareno.tdtest;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.VoiceMessageModel;

import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;

/**
 * Created by andremacareno on 13/08/15.
 */
public class AudioController {
    public static final int RECORDING_ERROR = 0;
    public static final int RECORDING_STARTED = 1;
    public static final int RECORDING_VOLUME_PROGRESS = 2;
    public static final int RECORDING_FINISHED = 3;

    public static final int VOICE_PLAYBACK_STARTED = 4;
    public static final int VOICE_PLAYBACK_PAUSED = 5;
    public static final int VOICE_PLAYBACK_STOPPED = 6;
    public static final int VOICE_PLAYBACK_UPDATE_PROGRESS = 7;


    public native int startRecord(String path);
    public native void stopRecord();
    public native int writeFrame(ByteBuffer frame, int len);
    public native int openOpusFile(String path);
    public native int seekOpusFile(float position);
    public native int isOpusFile(String path);
    public native void closeOpusFile();
    public native void readOpusFile(ByteBuffer buffer, int capacity, int[] args);
    public native long getTotalPcmDuration();

    private RecordWritingThread recThread;
    private VolumeCalculatingThread volThread;
    private Handler recordCommunicationBridge, playbackCommunicationBridge;

    private static volatile AudioController instance;
    public static AudioController getInstance()
    {
        if(instance == null)
        {
            synchronized(AudioController.class)
            {
                if(instance == null)
                    instance = new AudioController();
            }
        }
        return instance;
    }
    private AudioController()
    {
    }
    public void initPlaybackCommunicationBridge()
    {
        if(playbackCommunicationBridge == null)
            playbackCommunicationBridge = new CommunicationBridge(null);
    }
    public void initRecordingCommunicationBridge(MessageVoiceRecorder.RecordDelegate delegate)
    {
        recordCommunicationBridge = new CommunicationBridge(delegate);
        if(recThread == null)
        {
            recThread = new RecordWritingThread();
            recThread.start();
        }
        if(volThread == null)
        {
            volThread = new VolumeCalculatingThread();
            volThread.start();
        }
    }
    public RecordWritingThread getRecThread() { return this.recThread; }
    public VolumeCalculatingThread getVolThread() { return this.volThread; }

    public void updateVoiceMessageState(VoiceMessageModel voiceMsg, int code)
    {
        Message msg = Message.obtain(playbackCommunicationBridge);
        msg.what = code;
        msg.obj = voiceMsg.getAudioFileRef();
        msg.sendToTarget();
    }
    public void updateVolume(float value)
    {
        Message msg = Message.obtain(recordCommunicationBridge);
        msg.obj = value;
        msg.what = RECORDING_VOLUME_PROGRESS;
        msg.sendToTarget();
    }
    public void updateRecordingState(int code)
    {
        Message msg = Message.obtain(recordCommunicationBridge);
        msg.what = code;
        msg.sendToTarget();
    }
    public void recordingFinished(TdApi.InputMessageVoice file)
    {
        Message msg = Message.obtain(recordCommunicationBridge);
        msg.what = RECORDING_FINISHED;
        msg.obj = file;
        msg.sendToTarget();
    }
    public static class RecordWritingThread extends Thread
    {
        public Handler handler;
        @Override
        public void run()
        {
            Looper.prepare();
            handler = new Handler(Looper.myLooper());
            Looper.loop();
        }
    }
    public static class VolumeCalculatingThread extends Thread
    {
        public Handler handler;
        @Override
        public void run()
        {
            Looper.prepare();
            handler = new Handler(Looper.myLooper());
            Looper.loop();
        }
    }
    private static class CommunicationBridge extends Handler
    {
        private WeakReference<MessageVoiceRecorder.RecordDelegate> delegateRef;
        public CommunicationBridge(MessageVoiceRecorder.RecordDelegate delegate)
        {
            this.delegateRef = new WeakReference<MessageVoiceRecorder.RecordDelegate>(delegate);
        }

        @Override
        public void handleMessage(Message msg)
        {
            int code = msg.what;
            if(code == VOICE_PLAYBACK_STARTED || code == VOICE_PLAYBACK_STOPPED || code == VOICE_PLAYBACK_UPDATE_PROGRESS || code == VOICE_PLAYBACK_PAUSED)
            {
                if(msg.obj == null)
                    return;
                FileModel voiceMessage = (FileModel) msg.obj;
                for(String k : voiceMessage.getVoiceMsgDelegates().keySet())
                {
                    MessageVoicePlayer.VoiceMessageDelegate delegate = voiceMessage.getVoiceMsgDelegates().get(k);
                    if(delegate == null) {
                        if(BuildConfig.DEBUG)
                            Log.d("AudioController", "null delegate");
                        continue;
                    }
                    if(code == VOICE_PLAYBACK_STARTED)
                        delegate.didPlaybackStarted();
                    else if(code == VOICE_PLAYBACK_PAUSED)
                        delegate.didPlaybackPaused();
                    else if(code == VOICE_PLAYBACK_STOPPED)
                        delegate.didPlaybackStopped();
                    else if(code == VOICE_PLAYBACK_UPDATE_PROGRESS)
                        delegate.didPlaybackProgressUpdated();
                }
            }
            else
            {
                MessageVoiceRecorder.RecordDelegate recDelegate = delegateRef.get();
                if(recDelegate == null)
                    return;
                if(code == RECORDING_ERROR)
                    recDelegate.onRecordError();
                else if(code == RECORDING_STARTED)
                    recDelegate.onRecordStarted();
                else if(code == RECORDING_VOLUME_PROGRESS)
                {
                    float volume = (float) msg.obj;
                    recDelegate.didVolumeReceived(volume);
                }
                else if(code == RECORDING_FINISHED)
                {
                    if(msg.obj == null)
                        return;
                    TdApi.InputMessageVoice voiceMessage = (TdApi.InputMessageVoice) msg.obj;
                    recDelegate.didVoiceRecorded(voiceMessage);
                }
            }
        }
    }
}
