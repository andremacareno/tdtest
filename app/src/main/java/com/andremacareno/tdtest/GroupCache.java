package com.andremacareno.tdtest;

import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.updatelisteners.UpdateChatProcessor;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.concurrent.atomic.AtomicBoolean;

import gnu.trove.TCollections;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * Created by andremacareno on 24/04/15.
 */
public class GroupCache {
    private static volatile GroupCache _instance;
    private final Object monitor = new Object();
    private final Object monitor2 = new Object();
    private final TIntObjectMap<TdApi.Group> groups = TCollections.synchronizedMap(new TIntObjectHashMap<TdApi.Group>());
    private final TIntObjectMap<TdApi.GroupFull> fullGroups = TCollections.synchronizedMap(new TIntObjectHashMap<TdApi.GroupFull>());
    private final UpdateChatProcessor updateGroupListener;
    private GroupCache() {
        updateGroupListener = new UpdateChatProcessor() {

            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                return String.format("groupcache_constructor_%d_observer", constructor);
            }

            @Override
            protected boolean shouldObserveUpdateGroup() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateTitle() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateReplyMarkup() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateReadInbox() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateReadOutbox() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateChat() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateChatPhoto() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateChatOrder() {
                return false;
            }

            @Override
            protected void processUpdateInBackground(TdApi.Update upd) {
                if(upd.getConstructor() == TdApi.UpdateGroup.CONSTRUCTOR)
                {
                    final TdApi.UpdateGroup castUpd = (TdApi.UpdateGroup) upd;
                    synchronized(groups)
                    {
                        groups.put(castUpd.group.id, castUpd.group);
                    }
                    synchronized(fullGroups)
                    {
                        if(!fullGroups.containsKey(castUpd.group.id))
                            return;
                    }
                    TelegramApplication.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            getGroupFull(castUpd.group.id, true);
                        }
                    });
                }
            }

            @Override
            protected void processUpdateInUiThread(TdApi.Update upd) {
            }
        };
    }
    public void attachObserver()
    {
        TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
        if(service == null)
        {
            NotificationCenter.getInstance().addObserver(new NotificationObserver(NotificationCenter.didUpdateServiceStarted) {
                @Override
                public void didNotificationReceived(Notification notification) {
                    TelegramUpdatesHandler serviceInstance = (TelegramUpdatesHandler) notification.getObject();
                    updateGroupListener.attachObservers(serviceInstance);
                }
            });
        }
        else
            updateGroupListener.attachObservers(service);
    }
    public static GroupCache getInstance()
    {
        if(_instance == null)
        {
            synchronized(GroupCache.class)
            {
                if(_instance == null)
                    _instance = new GroupCache();
            }
        }
        return _instance;
    }
    public void clear()
    {
        synchronized(fullGroups)
        {
            fullGroups.clear();
        }
        updateGroupListener.removeObservers();
    }
    public TdApi.GroupFull getGroupFull(int id, boolean force)
    {
        if(id == 0)
            return null;
        synchronized(fullGroups) {
            if (fullGroups.containsKey(id) && !force)
                return fullGroups.get(id);
        }
        final AtomicBoolean ready = new AtomicBoolean(false);
        TdApi.GetGroupFull getGroupFunc = new TdApi.GetGroupFull(id);
        Client telegramClient = TG.getClientInstance();
        if(telegramClient == null)
            return null;
        telegramClient.send(getGroupFunc, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                try
                {
                    TdApi.GroupFull ch = (TdApi.GroupFull) object;
                    synchronized(fullGroups)
                    {
                        fullGroups.put(ch.group.id, ch);
                    }
                }
                finally
                {
                    ready.set(true);
                    synchronized(monitor)
                    {
                        monitor.notifyAll();
                    }
                }
            }
        });
        while (!ready.get()) {
            synchronized(monitor)
            {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        synchronized(fullGroups)
        {
            return fullGroups.get(id);
        }
    }
    public TdApi.Group getGroup(int id, boolean force)
    {
        if(id == 0)
            return null;
        synchronized(groups)
        {
            if(groups.containsKey(id) && !force)
                return groups.get(id);
        }
        synchronized(fullGroups)
        {
            if(fullGroups.containsKey(id) && !force)
                return fullGroups.get(id).group;
        }
        final AtomicBoolean ready = new AtomicBoolean(false);
        TdApi.GetGroup getGroupFunc = new TdApi.GetGroup(id);
        Client telegramClient = TG.getClientInstance();
        if(telegramClient == null)
            return null;
        telegramClient.send(getGroupFunc, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                try
                {
                    TdApi.Group ch = (TdApi.Group) object;
                    synchronized(groups)
                    {
                        groups.put(ch.id, ch);
                    }
                }
                finally
                {
                    ready.set(true);
                    synchronized(monitor2)
                    {
                        monitor2.notifyAll();
                    }
                }
            }
        });
        while (!ready.get()) {
            synchronized(monitor2)
            {
                try {
                    monitor2.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        synchronized(groups)
        {
            return groups.get(id);
        }
    }
    public void cacheGroup(TdApi.Group group)
    {
        synchronized(groups)
        {
            groups.put(group.id, group);
        }
    }

    public boolean isCached(int groupId)
    {
        synchronized(groups)
        {
            return groups.containsKey(groupId);
        }
    }
    public boolean isCachedFull(int groupId)
    {
        synchronized(fullGroups)
        {
            return fullGroups.containsKey(groupId);
        }
    }
}
