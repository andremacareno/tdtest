package com.andremacareno.tdtest.viewcontrollers.actionview;

import android.view.MenuInflater;
import android.view.View;
import android.widget.PopupMenu;

import com.andremacareno.tdtest.views.actionviews.SimpleActionView;

/**
 * Created by Andrew on 14.05.2015.
 */
public abstract class SimpleActionViewController extends ActionViewController {
    @Override
    public void onFragmentPaused() {
        SimpleActionView v = (SimpleActionView) getActionView();
    }

    @Override
    public void onFragmentResumed() {
        initView();
    }

    @Override
    public void onActionViewRemoved() {
        SimpleActionView v = (SimpleActionView) getActionView();
    }
    private void initView()
    {
        SimpleActionView v = (SimpleActionView) getActionView();
        if(!showBackButton())
            v.backButton.setVisibility(View.GONE);
        else {
            v.backButton.setVisibility(View.VISIBLE);
            v.backButton.setOnClickListener(getBackButtonListener());
        }
        if(!showDoneButton())
            v.doneButton.setVisibility(View.GONE);
        else {
            v.doneButton.setVisibility(View.VISIBLE);
            v.doneButton.setOnClickListener(getDoneButtonListener());
        }
        if(havePopup())
        {
            v.menuButton.setVisibility(View.VISIBLE);
            v.doneButton.setVisibility(View.INVISIBLE);
            final PopupMenu popup = new PopupMenu(v.getContext(), v.menuButton);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(getMenuResource(), popup.getMenu());
            v.menuButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popup.show();
                }
            });
            popup.setOnMenuItemClickListener(getMenuItemClickListener());
        }
        else
        {
            v.menuButton.setVisibility(View.GONE);
        }
        v.title.setText(getTitle());
    }

    public PopupMenu.OnMenuItemClickListener getMenuItemClickListener() {
        return null;
    }

    public abstract boolean havePopup();
    public abstract boolean showBackButton();
    public abstract boolean showDoneButton();
    public abstract String getTitle();
    public abstract View.OnClickListener getBackButtonListener();
    public abstract View.OnClickListener getDoneButtonListener();
    public abstract int getMenuResource();
}
