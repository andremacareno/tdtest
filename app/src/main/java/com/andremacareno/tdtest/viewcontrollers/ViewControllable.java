package com.andremacareno.tdtest.viewcontrollers;

/**
 * Created by andremacareno on 02/04/15.
 */
public interface ViewControllable {
    public void setController(ViewController vc);
    public ViewController getController();
}
