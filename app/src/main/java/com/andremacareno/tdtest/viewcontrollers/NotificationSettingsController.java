package com.andremacareno.tdtest.viewcontrollers;

import android.widget.ImageButton;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.updatelisteners.UpdateNotificationsProcessor;

import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 11/08/15.
 */
public class NotificationSettingsController {
    private long chatId;
    private boolean isPrivateChat;
    private TdApi.NotificationSettings currentSettings;
    private ImageButton notificationSettingsBell;
    private UpdateNotificationsProcessor updateProcessor;
    public NotificationSettingsController(long chat_id, boolean is_private, TdApi.NotificationSettings current_settings, ImageButton v)
    {
        this.chatId = chat_id;
        this.isPrivateChat = is_private;
        this.currentSettings = current_settings;
        this.notificationSettingsBell = v;
        notificationSettingsBell.setImageDrawable(currentSettings.muteFor > 0 ? TelegramApplication.sharedApplication().getResources().getDrawable(R.drawable.ic_notifications_off) : TelegramApplication.sharedApplication().getResources().getDrawable(R.drawable.ic_notifications_on));
        updateProcessor = new UpdateNotificationsProcessor() {
            private volatile boolean shouldUpdate = false;
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                return String.format(Constants.VIEWCHAT_UPDATE_NOTIFICATIONS_OBSERVER, chatId);
            }

            @Override
            protected void processUpdateInBackground(TdApi.Update upd) {
                shouldUpdate = isInScope(((TdApi.UpdateNotificationSettings) upd).scope);
            }

            @Override
            protected void processUpdateInUiThread(TdApi.Update upd) {
                if(shouldUpdate)
                    updateSettings(((TdApi.UpdateNotificationSettings) upd).notificationSettings);
            }
        };
    }
    public void attachObservers(TelegramUpdatesHandler service)
    {
        updateProcessor.attachObservers(service);
    }
    public void detachObservers()
    {
        updateProcessor.removeObservers();
    }
    public void onFragmentPaused()
    {
        updateProcessor.performPauseObservers();
    }
    public void onFragmentResumed()
    {
        updateProcessor.performResumeObservers();
    }
    private void updateSettings(TdApi.NotificationSettings newSettings)
    {
        this.currentSettings = newSettings;
        notificationSettingsBell.setImageDrawable(currentSettings.muteFor > 0 ? TelegramApplication.sharedApplication().getResources().getDrawable(R.drawable.ic_notifications_off) : TelegramApplication.sharedApplication().getResources().getDrawable(R.drawable.ic_notifications_on));
    }
    private boolean isInScope(TdApi.NotificationSettingsScope updateScope)
    {
        if(updateScope.getConstructor() == TdApi.NotificationSettingsForAllChats.CONSTRUCTOR)
            return true;
        else if(updateScope.getConstructor() == TdApi.NotificationSettingsForChat.CONSTRUCTOR)
        {
            if(((TdApi.NotificationSettingsForChat) updateScope).chatId == this.chatId)
                return true;
            return false;
        }
        else if(updateScope.getConstructor() == TdApi.NotificationSettingsForPrivateChats.CONSTRUCTOR && isPrivateChat)
            return true;
        return false;
    }
    public void setNotificationSettingsRequested(int id)
    {
        int mute_for = 0;
        if(id == R.id.action_mute_1h)
            mute_for = 3600;
        else if(id == R.id.action_mute_8h)
            mute_for = 28800;
        else if(id == R.id.action_mute_2d)
            mute_for = 172800;
        else if(id == R.id.action_disable)
            mute_for = Integer.MAX_VALUE;
        TdApi.NotificationSettings settingsToSend = new TdApi.NotificationSettings(mute_for, currentSettings.sound, currentSettings.showPreviews);
        TdApi.NotificationSettingsForChat scopeToSend = new TdApi.NotificationSettingsForChat(this.chatId);
        TdApi.SetNotificationSettings setNotificationSettingsReq = new TdApi.SetNotificationSettings(scopeToSend, settingsToSend);
        TG.getClientInstance().send(setNotificationSettingsReq, TelegramApplication.dummyResultHandler);
        updateSettings(settingsToSend);
    }
}
