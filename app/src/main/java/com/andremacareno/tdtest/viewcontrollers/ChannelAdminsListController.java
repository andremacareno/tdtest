package com.andremacareno.tdtest.viewcontrollers;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.fragments.EditChannelMembersFragment;
import com.andremacareno.tdtest.listadapters.ChannelAdminsAdapter;
import com.andremacareno.tdtest.listadapters.ChannelMembersAdapter;
import com.andremacareno.tdtest.models.ChatParticipantModel;
import com.andremacareno.tdtest.tasks.GenerateParticipantsListTask;
import com.andremacareno.tdtest.views.ChatParticipantsListView;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import gnu.trove.TCollections;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * Created by andremacareno on 04/04/15.
 */
public class ChannelAdminsListController extends ViewController {
    //statement variables
    protected final String TAG = "ChooseAdminsListControl";
    protected RecyclerView.Adapter adapter;
    private final AtomicBoolean preventLoading = new AtomicBoolean(false);
    private final ArrayList<ChatParticipantModel> participantsList = new ArrayList<>();
    private final TIntObjectMap<ChatParticipantModel> idToParticipants = TCollections.synchronizedMap(new TIntObjectHashMap<ChatParticipantModel>());
    private int channelId;
    private boolean adminsOnly;
    public ChannelAdminsListController(View v, int channelId, EditChannelMembersFragment.ChannelAdminsDelegate delegate, boolean inviteAdminsScreen) {
        super(v);
        this.channelId = channelId;
        this.adminsOnly = inviteAdminsScreen;
        this.adapter = adminsOnly ? new ChannelAdminsAdapter(participantsList, delegate, this) : new ChannelMembersAdapter(participantsList, delegate, this);
        initViewSettings();
    }

    public int[] getParticipantIds()
    {
        synchronized(idToParticipants)
        {
            return idToParticipants.keys();
        }
    }
    public void loadMoreParticipants()
    {

        synchronized(preventLoading)
        {
            if(preventLoading.get()) {
                return;
            }
            preventLoading.set(true);

        }
        GenerateParticipantsListTask task = new GenerateParticipantsListTask(channelId, participantsList.size(), 20, adminsOnly);
        task.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(AsyncTaskManTask task) {
                final ArrayList<ChatParticipantModel> participants = ((GenerateParticipantsListTask) task).getParticipants();
                final int lastIndex;
                synchronized (participantsList) {
                    synchronized(idToParticipants)
                    {
                        lastIndex = participantsList.size();
                        for (ChatParticipantModel p : participants) {
                            if(p.isBot() && adminsOnly)
                                continue;
                            participantsList.add(p);
                            idToParticipants.put(p.getId(), p);
                        }
                    }
                }
                TelegramApplication.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(participants.isEmpty())
                            return;
                        adapter.notifyItemRangeInserted(lastIndex, participants.size());
                        try
                        {
                            ChatParticipantsListView v = (ChatParticipantsListView) getView();
                            v.setNewElementCount(adapter.getItemCount());
                        }
                        catch(Exception e) { e.printStackTrace(); }

                        synchronized(preventLoading)
                        {
                            preventLoading.set(false);
                        }
                    }
                });
            }
        });
        task.addToQueue();
    }
    public void onAdminRemove(int userId)
    {
        synchronized(idToParticipants)
        {
            synchronized(participantsList)
            {
                if(!idToParticipants.containsKey(userId))
                    return;
                ChatParticipantModel u = idToParticipants.get(userId);
                idToParticipants.remove(userId);
                int index = participantsList.indexOf(u);
                participantsList.remove(index);
                adapter.notifyItemRemoved(index);
                try
                {
                    ChatParticipantsListView v = (ChatParticipantsListView) getView();
                    v.setNewElementCount(adapter.getItemCount());
                }
                catch(Exception e) { e.printStackTrace(); }
            }
        }
    }
    public void onNewAdmin(ChatParticipantModel newAdmin)
    {
        synchronized(idToParticipants)
        {
            synchronized(participantsList)
            {
                if(idToParticipants.containsKey(newAdmin.getId()))
                    return;
                idToParticipants.put(newAdmin.getId(), newAdmin);
                int index = participantsList.size();
                participantsList.add(newAdmin);
                adapter.notifyItemInserted(index);
                try
                {
                    ChatParticipantsListView v = (ChatParticipantsListView) getView();
                    v.setNewElementCount(adapter.getItemCount());
                }
                catch(Exception e) { e.printStackTrace(); }
            }
        }
    }
    @Override
    protected void onViewReplaced() {
        if(getView() != null) {
            initViewSettings();
        }
    }
    public void onFragmentPaused()
    {
    }
    public void onFragmentResumed()
    {
    }
    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
    }
    public void removeObservers()
    {
    }


    private void initViewSettings()
    {
        ChatParticipantsListView v = (ChatParticipantsListView) getView();
        synchronized(participantsList)
        {
            v.setNewElementCount(participantsList.size());
        }
        v.setAdapter(adapter);
        v.setMotionEventSplittingEnabled(false);
    }
}
