package com.andremacareno.tdtest.viewcontrollers;

import android.widget.EditText;

import com.andremacareno.tdtest.auth.AuthStateProcessor;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 03/04/15.
 */
public class SignUpButtonController {
    private EditText firstNameEditText, lastNameEditText;
    private final String TAG = "SignUpButtonController";
    public SignUpButtonController(EditText fn, EditText ln)
    {
        this.firstNameEditText = fn;
        this.lastNameEditText = ln;
    }
    public void didSignUpButtonClicked() {
        if(firstNameEditText == null || lastNameEditText == null)
            return;
        String firstName = firstNameEditText.getText().toString();
        String lastName = lastNameEditText.getText().toString();
        if(firstName.length() == 0)
            return;
        TdApi.TLFunction setNameFunc = new TdApi.SetAuthName(firstName, lastName);
        AuthStateProcessor.getInstance().changeAuthState(setNameFunc);
    }
    public void rebindToViews(EditText fn, EditText ln)
    {
        this.firstNameEditText = fn;
        this.lastNameEditText = ln;
    }
}
