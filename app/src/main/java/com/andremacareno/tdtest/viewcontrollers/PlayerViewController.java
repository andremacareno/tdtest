package com.andremacareno.tdtest.viewcontrollers;

import android.util.Log;

import com.andremacareno.tdtest.AudioPlaybackService;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.fragments.PlayerFragment;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.AudioTrackModel;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.views.AlbumArtImageView;
import com.andremacareno.tdtest.views.player.PlayerView;

/**
 * Created by andremacareno on 15/08/15.
 */
public class PlayerViewController implements PlayerView.PlayerDelegate, AudioPlaybackService.PlaybackDelegate {
    private PlayerView player;
    private int duration;
    private PlayerFragment.PlayerFragmentDelegate fragmentDelegate;
    private static final String durationFormat = "%d:%02d";
    private final AudioPlaybackService playbackService;
    public PlayerViewController(PlayerView player)
    {
        playbackService = TelegramApplication.sharedApplication().getAudioPlaybackService();
        refreshView(player);
    }
    @Override
    public void didPlayButtonPressed() {
        if(playbackService == null)
            return;
        playbackService.togglePlayback();
    }

    @Override
    public void didPreviousTrackButtonPressed() {
        if(playbackService == null)
            return;
        boolean wasOnRepeat = playbackService.isRepeatEnabled();
        if(wasOnRepeat)
            playbackService.toggleRepeat();
        playbackService.playPreviousTrack();
        if(wasOnRepeat)
            playbackService.toggleRepeat();
    }

    @Override
    public void didNextTrackButtonPressed() {
        if(playbackService == null)
            return;
        boolean wasOnRepeat = playbackService.isRepeatEnabled();
        if(wasOnRepeat)
            playbackService.toggleRepeat();
        playbackService.playNextTrack();
        if(wasOnRepeat)
            playbackService.toggleRepeat();
    }

    @Override
    public void didBackButtonPressed() {
        if(fragmentDelegate != null)
            fragmentDelegate.goBack();
    }

    @Override
    public void didPlaylistButtonPressed() {
        if(playbackService == null)
            return;
        if(fragmentDelegate != null)
            fragmentDelegate.openPlaylist(playbackService.getPlaylistChatId());
    }

    @Override
    public void didRepeatButtonPressed() {
        if(playbackService != null)
            playbackService.toggleRepeat();
    }

    @Override
    public void didShuffleButtonPressed() {
        if(playbackService != null)
            playbackService.toggleShuffle();
    }

    @Override
    public void didSeekBarMoved(int timestamp) {
        if(BuildConfig.DEBUG)
            Log.d("PlayerController", "didSeekBarMoved");
        updateTime(timestamp);

    }

    @Override
    public void didSeekBarDragStarted() {
    }

    @Override
    public void didSeekBarDragEnded() {
        playbackService.requestSeekTo(player.getSeekBarPosition());
    }

    @Override
    public void didCoverDarknessDetected(boolean dark) {
        player.changeButtons(dark);
    }


    @Override
    public void didPlaybackStarted(AudioTrackModel nowPlaying) {
        initPlayer(nowPlaying);
    }

    @Override
    public void didPlaybackStopped() {
        player.updatePlayButton(false);
    }

    @Override
    public void didPlaybackPaused() {
        player.updatePlayButton(false);
    }

    @Override
    public void didPlaybackUnpaused() {
        player.updatePlayButton(true);
    }

    @Override
    public void didTrackPositionChanged(int currentPosition) {
        updateTime(currentPosition);
    }

    @Override
    public void didTrackDownloadFinished() {
        try
        {
            AudioTrackModel nowPlaying = TelegramApplication.sharedApplication().getAudioPlaybackService().nowPlaying();
            if(nowPlaying != null && nowPlaying.getTrack().getDownloadState() == FileModel.DownloadState.LOADED)
                TelegramImageLoader.getInstance().extractAlbumArt(nowPlaying.getTrack().getFile().path, String.format("albumcover_%d", nowPlaying.getTrack().getFileId()), player.getCoverView());
        }
        catch(Exception ignored) {}
    }

    @Override
    public int getDelegateId() {
        return Constants.PLAYER_DELEGATE_ID;
    }
    private void initPlayer(AudioTrackModel nowPlaying)
    {
        if(nowPlaying == null)
            return;
        try
        {
            this.duration = nowPlaying.getDuration();
            player.setDelegate(this);
            player.setSeekBarMax(duration);
            player.updateSongData(nowPlaying);
            player.updatePlayButton(true);
            updateTime(0);
            if(nowPlaying.getTrack().getDownloadState() == FileModel.DownloadState.LOADED)
                TelegramImageLoader.getInstance().extractAlbumArt(nowPlaying.getTrack().getFile().path, String.format("albumcover_%d", nowPlaying.getTrack().getFileId()), player.getCoverView());
            else
                player.getCoverView().setImageDrawable(null);
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    private void updateTime(int timeElapsed)
    {
        int timeRemaining = duration - timeElapsed;
        if(timeRemaining < 0)
            timeRemaining = 0;
        String elapsedString = String.format(durationFormat, timeElapsed / 60000, (timeElapsed % 60000) / 1000);
        String remainingString = String.format(durationFormat, timeRemaining / 60000, (timeRemaining % 60000) / 1000);
        player.updateTime(elapsedString, remainingString);
        player.updateSeekBar(timeElapsed);
    }
    public void setFragmentDelegate(PlayerFragment.PlayerFragmentDelegate delegate)
    {
        this.fragmentDelegate = delegate;
    }
    public void refreshView(PlayerView player)
    {
        this.player = player;
        if(playbackService == null)
            return;
        ((AlbumArtImageView)player.getCoverView()).setPlayerDelegate(this);
        player.updateRepeatButtonState(playbackService.isRepeatEnabled());
        player.updateShuffleButtonState(playbackService.isShuffleEnabled());
        initPlayer(playbackService.nowPlaying());
    }

}
