package com.andremacareno.tdtest.viewcontrollers.passcode;

import android.content.SharedPreferences;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.views.PasscodeView;

/**
 * Created by andremacareno on 02/08/15.
 */
public abstract class PasscodeViewController {
    public enum Mode {LOCK, SET, CONFIRMATION}
    public enum PasscodeType {PIN, PASSWORD, PATTERN, GESTURE}

    protected Mode currentMode;
    protected View viewRef;
    private PasscodeType currentType;
    protected Animation shakeAnimation;
    public PasscodeViewController(PasscodeView view, Mode mode)
    {
        this.viewRef = view.getLayout();
        this.currentMode = mode;
        shakeAnimation = AnimationUtils.loadAnimation(viewRef.getContext(), R.anim.shake);
        shakeAnimation.setRepeatCount(4);
        init();
        didModeChanged();
    }

    public Mode getCurrentMode() { return this.currentMode; }
    public void setCurrentMode(Mode newMode) {
        this.currentMode = newMode;
        if(newMode == Mode.CONFIRMATION)
            enteredInConfirmationMode();
        didModeChanged();
    }

    //for confirmation mode
    private void enteredInConfirmationMode()
    {
        NotificationCenter.getInstance().postNotification(NotificationCenter.didEnteredInConfirmationMode, null);
    }
    protected void correctConfirmation()
    {
        //save passcode and go back
        SharedPreferences prefs = TelegramApplication.sharedApplication().sharedPreferences();
        SharedPreferences.Editor prefEdit = prefs.edit();
        prefEdit.putBoolean(Constants.PREFS_PASSCODE_LOCK_ENABLED, true);
        savePasscode(prefEdit);
        prefEdit.apply();
        goBack();
    }
    private void goBack()
    {
        NotificationCenter.getInstance().postNotification(NotificationCenter.didPasscodeSet, null);
    }
    public void reattachToView(PasscodeView view)
    {
        this.viewRef = view;
        init();
        didModeChanged();
    }
    protected void passcodeLockPassed()
    {
        NotificationCenter.getInstance().postNotification(NotificationCenter.didPasscodeLockPassed, null);
    }
    protected abstract void init();
    protected abstract void savePasscode(SharedPreferences.Editor prefEdit);
    protected abstract void incorrectConfirmation();
    protected abstract void didModeChanged();

}
