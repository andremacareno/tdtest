package com.andremacareno.tdtest.viewcontrollers;

import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.andremacareno.tdtest.AudioPlaybackService;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.UserCache;
import com.andremacareno.tdtest.fragments.SharedMediaFragment;
import com.andremacareno.tdtest.listadapters.SharedAudioAdapter;
import com.andremacareno.tdtest.listadapters.SharedMediaAdapter;
import com.andremacareno.tdtest.models.AudioTrackModel;
import com.andremacareno.tdtest.models.SharedImageModel;
import com.andremacareno.tdtest.models.SharedMedia;
import com.andremacareno.tdtest.models.SharedMediaSectionModel;
import com.andremacareno.tdtest.models.SharedVideoModel;
import com.andremacareno.tdtest.updatelisteners.UpdateMessageProcessor;
import com.andremacareno.tdtest.views.SharedMediaListView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.hash.TIntHashSet;

/**
 * Created by andremacareno on 19/08/15.
 */
public class SharedMediaViewController extends ViewController {
    public interface MediaListAdapter
    {
        public int getSelectionCount();
        public int[] getSelectedMessages();
        public List<SharedMedia> getSelectedModels();
        public void disableSelectionMode();
    }
    private static final int RECEIVING_AUDIO = 0;
    private static final int RECEIVING_IMAGES = 1;
    //adapters
    private SharedMediaAdapter imgAdapter;
    private SharedAudioAdapter audioAdapter;

    //chatSearch parameters
    private long chatId;
    private volatile Date imgLastDate = null;
    private volatile boolean imgInsertSectionToHead = false;
    private volatile Date audioLastDate = null;
    private volatile boolean audioInsertSectionToHead = false;
    private final AtomicBoolean loadingImages = new AtomicBoolean(false);
    private final AtomicBoolean loadingAudio = new AtomicBoolean(false);
    private volatile int imgSearchFromId = 0;
    private volatile int audioSearchFromId = 0;
    private volatile boolean keepAudioSearch = false;
    private final TIntHashSet writtenAudio = new TIntHashSet();
    private final TdApi.SearchMessagesFilter imgFilter = new TdApi.SearchMessagesFilterPhotoAndVideo();
    private final TdApi.SearchMessagesFilter audioFilter = new TdApi.SearchMessagesFilterAudio();
    private final TdApi.SearchChatMessages getImages = new TdApi.SearchChatMessages(0, "", 0, 5, false, imgFilter);
    private final TdApi.SearchChatMessages getAudio = new TdApi.SearchChatMessages(0, "", 0, 5, false, audioFilter);
    private final TIntObjectHashMap<SharedMedia> idToImage = new TIntObjectHashMap<>();
    private final TIntObjectHashMap<SharedMedia> idToAudio = new TIntObjectHashMap<>();
    private final UpdateMessageProcessor updateMessageProcessor;
    private final UpdateMessageProcessor.UpdateMessageDelegate updateMessageUiThreadDelegate;

    //images data
    private final ArrayList<SharedMedia> images = new ArrayList<SharedMedia>();
    //audio data
    private final ArrayList<SharedMedia> audio = new ArrayList<>();
    //handler
    private PauseHandler mediaReceiver;
    //saving position
    private int imgAdapterPosition = 0;
    private int audioAdapterPosition = 0;

    public SharedMediaViewController(View v, long chat_id) {
        super(v);
        this.chatId = chat_id;
        getImages.chatId = chat_id;
        getAudio.chatId = chat_id;
        updateMessageUiThreadDelegate = new UpdateMessageProcessor.UpdateMessageDelegate() {
            @Override
            public void updateMessageContent(TdApi.UpdateMessageContent upd) {
            }

            @Override
            public void updateMessageDate(TdApi.UpdateMessageDate upd) {
            }

            @Override
            public void updateMessageId(TdApi.UpdateMessageId upd) {
            }

            @Override
            public void updateMessageSendFailed(TdApi.UpdateMessageSendFailed upd) {
            }

            @Override
            public void updateNewMessage(TdApi.UpdateNewMessage upd) {
                if(upd.message.chatId != chatId)
                    return;
                if(upd.message.content.getConstructor() != TdApi.MessagePhoto.CONSTRUCTOR && upd.message.content.getConstructor() != TdApi.MessageAudio.CONSTRUCTOR && upd.message.content.getConstructor() != TdApi.MessageVideo.CONSTRUCTOR)
                    return;
                boolean addSeparator = false;
                Date firstMessageDate, currentMessageDate;
                if(upd.message.content.getConstructor() == TdApi.MessageAudio.CONSTRUCTOR)
                {
                    synchronized(writtenAudio)
                    {
                        if(writtenAudio.contains(((TdApi.MessageAudio) upd.message.content).audio.audio.id))
                            return;
                    }
                    currentMessageDate = new Date(((long) upd.message.date) * 1000L);
                    if(audio.isEmpty())
                        addSeparator = true;
                    else {
                        firstMessageDate = new Date(audio.get(1).getDate());
                        Calendar firstMessageCalendar = Calendar.getInstance();
                        Calendar currentMessageCalendar = Calendar.getInstance();
                        firstMessageCalendar.setTime(firstMessageDate);
                        currentMessageCalendar.setTime(currentMessageDate);
                        if(firstMessageCalendar.get(Calendar.MONTH) != currentMessageCalendar.get(Calendar.MONTH) || firstMessageCalendar.get(Calendar.MONTH) != currentMessageCalendar.get(Calendar.MONTH))
                            addSeparator = true;
                    }
                    AudioTrackModel track = new AudioTrackModel(upd.message.id, upd.message.date, ((TdApi.MessageAudio) upd.message.content).audio);
                    synchronized(idToAudio)
                    {
                        idToAudio.put(upd.message.id, track);
                    }
                    synchronized(writtenAudio)
                    {
                        if(writtenAudio.contains(track.getTrack().getFileId()))
                            return;
                        writtenAudio.add(track.getTrack().getFileId());
                    }
                    if(addSeparator)
                    {
                        audio.add(0, track);
                        audio.add(0, new SharedMediaSectionModel(currentMessageDate));
                        audioAdapter.notifyItemRangeInserted(0, 2);
                    }
                    else {
                        audio.add(1, track);
                        audioAdapter.notifyItemInserted(1);
                    }
                }
                else
                {
                    currentMessageDate = new Date(((long) upd.message.date) * 1000L);
                    if(images.isEmpty())
                        addSeparator = true;
                    else {
                        firstMessageDate = new Date(images.get(1).getDate());
                        Calendar firstMessageCalendar = Calendar.getInstance();
                        Calendar currentMessageCalendar = Calendar.getInstance();
                        firstMessageCalendar.setTime(firstMessageDate);
                        currentMessageCalendar.setTime(currentMessageDate);
                        if(firstMessageCalendar.get(Calendar.MONTH) != currentMessageCalendar.get(Calendar.MONTH) || firstMessageCalendar.get(Calendar.MONTH) != currentMessageCalendar.get(Calendar.MONTH))
                            addSeparator = true;
                    }
                    SharedMedia item;
                    if(upd.message.content.getConstructor() == TdApi.MessagePhoto.CONSTRUCTOR)
                        item = new SharedImageModel(upd.message);
                    else
                        item = new SharedVideoModel(upd.message);
                    synchronized(idToImage)
                    {
                        idToImage.put(upd.message.id, item);
                    }
                    if(addSeparator)
                    {
                        images.add(0, item);
                        images.add(0, new SharedMediaSectionModel(currentMessageDate));
                        imgAdapter.notifyItemRangeInserted(0, 2);
                    }
                    else {
                        images.add(1, item);
                        imgAdapter.notifyItemInserted(1);
                    }
                }
            }

            @Override
            public void updateDelMessages(TdApi.UpdateDeleteMessages upd) {
                if(upd.chatId != chatId)
                    return;
                for(int msg : upd.messageIds)
                {
                    removeAudio(msg);
                    removeImage(msg);
                }
            }

            @Override
            public void updateViewsCount(TdApi.UpdateMessageViews upd) {

            }
        };
        updateMessageProcessor = new UpdateMessageProcessor(null, updateMessageUiThreadDelegate) {
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                if(constructor == TdApi.UpdateDeleteMessages.CONSTRUCTOR)
                    return Constants.SHAREDMEDIA_DELETE_MESSAGES_OBSERVER;
                else if(constructor == TdApi.UpdateNewMessage.CONSTRUCTOR)
                    return Constants.SHAREDMEDIA_NEW_MESSAGE_OBSERVER;
                return null;
            }

            @Override
            protected boolean shouldObserveUpdateMsgContent() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateMsgDate() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateMsgId() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateMsgSendFailed() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateNewMsg() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateDeleteMessages() {
                return true;
            }

            @Override
            protected boolean shouldObserveViewsCount() {
                return false;
            }
        };
        init();
    }
    private void init()
    {
        imgAdapter = new SharedMediaAdapter(this);
        audioAdapter = new SharedAudioAdapter(this);
        mediaReceiver = new MediaReceiver(this);
    }

    @Override
    public void onFragmentPaused() {
        mediaReceiver.pause();
        updateMessageProcessor.performPauseObservers();
        if(getView() == null)
            return;
        SharedMediaFragment.SharedMediaType type = ((SharedMediaListView) getView()).getMediaType();
        if(type == SharedMediaFragment.SharedMediaType.AUDIO)
            audioAdapterPosition = ((LinearLayoutManager) ((SharedMediaListView) getView()).getLayoutManager()).findFirstVisibleItemPosition();
        else
            imgAdapterPosition = ((GridLayoutManager) ((SharedMediaListView) getView()).getLayoutManager()).findFirstVisibleItemPosition();
    }

    @Override
    public void onFragmentResumed() {
        mediaReceiver.resume();
        updateMessageProcessor.performResumeObservers();
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        updateMessageProcessor.attachObservers(service);
    }

    public void searchImages()
    {
        synchronized(loadingImages)
        {
            if(loadingImages.get())
                return;
            loadingImages.set(true);
        }
        getImages.fromMessageId = imgSearchFromId;
        TG.getClientInstance().send(getImages, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(object.getConstructor() != TdApi.Messages.CONSTRUCTOR)
                    return;
                ArrayList<SharedMedia> result = extractImageMedia(((TdApi.Messages) object).messages);
                    Message msg = Message.obtain(mediaReceiver);
                    msg.what = RECEIVING_IMAGES;
                    msg.obj = result;
                    msg.sendToTarget();
            }
        });
    }
    public void setImageAdapterDelegate(SharedMediaFragment.SharedImageDelegate delegate)
    {
        imgAdapter.setDelegate(delegate);
    }
    public void setAudioAdapterDelegate(SharedMediaFragment.SharedAudioDelegate delegate)
    {
        audioAdapter.setDelegate(delegate);
    }
    public void removeAudio(int msgId)
    {
        synchronized(idToAudio)
        {
            SharedMedia audioMessage = idToAudio.get(msgId);
            if(audioMessage == null)
                return;
            int index = audio.indexOf(audioMessage);
            if(index < 0)
                return;
            SharedMedia previous = index == 0 ? null : audio.get(index - 1);
            SharedMedia next = index == audio.size()-1 ? null : audio.get(index + 1);
            if((next == null || !next.isDummy()) && (previous != null && previous.isDummy())) {
                audio.remove(index);
                audio.remove(index - 1);
                audioAdapter.notifyItemRangeRemoved(index-1, 2);
            }
            else {
                audio.remove(index);
                audioAdapter.notifyItemRemoved(index);
            }
            idToAudio.remove(msgId);
            AudioPlaybackService playbackService = TelegramApplication.sharedApplication().getAudioPlaybackService();
            if(playbackService != null)
                playbackService.removeFromPlaylist(msgId, chatId);
        }
    }
    public void removeImage(int msgId)
    {
        synchronized(idToImage)
        {
            SharedMedia audioMessage = idToImage.get(msgId);
            if(audioMessage == null)
                return;
            int index = images.indexOf(audioMessage);
            if(index < 0)
                return;
            SharedMedia previous = index == 0 ? null : images.get(index - 1);
            SharedMedia next = index == images.size()-1 ? null : images.get(index + 1);
            if((next == null || !next.isDummy()) && (previous != null && previous.isDummy())) {
                images.remove(index);
                images.remove(index - 1);
                imgAdapter.notifyItemRangeRemoved(index-1, 2);
            }
            else {
                images.remove(index);
                imgAdapter.notifyItemRemoved(index);
            }
            idToImage.remove(msgId);
        }
    }
    public MediaListAdapter getAdapter(SharedMediaFragment.SharedMediaType type) {
        if(type == SharedMediaFragment.SharedMediaType.AUDIO)
            return this.audioAdapter;
        return this.imgAdapter;
    }
    public void searchAudio()
    {
        synchronized(loadingAudio)
        {
            if(loadingAudio.get()) {
                return;
            }
            loadingAudio.set(true);
        }
        getAudio.fromMessageId = audioSearchFromId;
        TG.getClientInstance().send(getAudio, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(object.getConstructor() != TdApi.Messages.CONSTRUCTOR)
                    return;
                ArrayList<SharedMedia> result = extractAudioMedia(((TdApi.Messages) object).messages);
                Message msg = Message.obtain(mediaReceiver);
                msg.what = RECEIVING_AUDIO;
                msg.obj = result;
                msg.sendToTarget();
            }
        });
    }
    public SharedMedia getImageById(int id)
    {
        synchronized(idToImage)
        {
            if(idToImage.containsKey(id))
                return idToImage.get(id);
        }
        return null;
    }
    public SharedMedia getAudioById(int id)
    {
        synchronized(idToAudio)
        {
            if(idToAudio.containsKey(id))
                return idToAudio.get(id);
        }
        return null;
    }
    public void didDisplayModeChanged(SharedMediaFragment.SharedMediaType requestedMediaType)
    {
        if(getView() == null)
            return;
        if(requestedMediaType == SharedMediaFragment.SharedMediaType.IMAGES_AND_VIDEOS) {
            ((SharedMediaListView) getView()).setAdapter(imgAdapter);
            final GridLayoutManager layoutManager = (GridLayoutManager) ((SharedMediaListView) getView()).getLayoutManager();
            layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return images.get(position).isDummy()? layoutManager.getSpanCount() : 1;
                }
            });
            try
            {
                layoutManager.scrollToPosition(imgAdapterPosition);
            }
            catch(Exception e) {}
        }
        else {
            ((SharedMediaListView) getView()).setAdapter(audioAdapter);
            try
            {
                ((SharedMediaListView) getView()).getLayoutManager().scrollToPosition(audioAdapterPosition);
            }
            catch(Exception e) {}
        }
    }
    public void removeObservers() {
        updateMessageProcessor.removeObservers();
    }
    public ArrayList<SharedMedia> getImages() { return this.images; }
    public ArrayList<SharedMedia> getAudio() { return this.audio; }
    private ArrayList<SharedMedia> extractImageMedia(TdApi.Message[] source)
    {
        imgInsertSectionToHead = false;
        ArrayList<SharedMedia> models = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        Calendar previousCal = Calendar.getInstance();
        Date currentDate = new Date();
        Date previousDate = null;
        Date dateOfFirstMessage = null;
        for(TdApi.Message msg : source)
        {
            UserCache.getInstance().cacheUser(msg.fromId);
            long date = ((long)msg.date) * 1000L;
            currentDate.setTime(date);
            cal.setTime(currentDate);
            if(previousDate != null)
            {
                previousCal.setTime(previousDate);
                if(cal.get(Calendar.MONTH) != previousCal.get(Calendar.MONTH) || cal.get(Calendar.YEAR) != previousCal.get(Calendar.YEAR))
                {
                    SharedMediaSectionModel section = new SharedMediaSectionModel(currentDate);
                    models.add(section);
                }
            }
            SharedMedia item;
            if(msg.content.getConstructor() == TdApi.MessagePhoto.CONSTRUCTOR)
                item = new SharedImageModel(msg);
            else if(msg.content.getConstructor() == TdApi.MessageVideo.CONSTRUCTOR)
                item = new SharedVideoModel(msg);
            else
                continue;
            if(dateOfFirstMessage == null) {
                dateOfFirstMessage = new Date(date);
            }
            synchronized(idToImage)
            {
                idToImage.put(msg.id, item);
            }
            models.add(item);
            if(previousDate == null)
                previousDate = new Date();
            previousDate.setTime(date);
        }
        if(dateOfFirstMessage != null && imgLastDate != null)
        {
            cal.setTime(dateOfFirstMessage);
            previousCal.setTime(imgLastDate);
            imgInsertSectionToHead = cal.get(Calendar.MONTH) != previousCal.get(Calendar.MONTH) || cal.get(Calendar.YEAR) != previousCal.get(Calendar.YEAR);
        }
        else if(imgLastDate == null) {
            imgInsertSectionToHead = false;
            SharedMedia firstSection = new SharedMediaSectionModel(dateOfFirstMessage);
            models.add(0, firstSection);
        }
        imgLastDate = previousDate != null ? new Date(previousDate.getTime()) : null;
        return models;
    }

    private ArrayList<SharedMedia> extractAudioMedia(TdApi.Message[] source)
    {
        audioInsertSectionToHead = false;
        keepAudioSearch = false;
        ArrayList<SharedMedia> models = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        Calendar previousCal = Calendar.getInstance();
        Date currentDate = new Date();
        Date previousDate = null;
        Date dateOfFirstMessage = null;
        for(TdApi.Message msg : source)
        {
            UserCache.getInstance().cacheUser(msg.fromId);
            audioSearchFromId = msg.id;
            long date = ((long)msg.date) * 1000L;
            currentDate.setTime(date);
            cal.setTime(currentDate);
            if(previousDate != null)
            {
                previousCal.setTime(previousDate);
                if(cal.get(Calendar.MONTH) != previousCal.get(Calendar.MONTH) || cal.get(Calendar.YEAR) != previousCal.get(Calendar.YEAR))
                {
                    SharedMediaSectionModel section = new SharedMediaSectionModel(currentDate);
                    models.add(section);
                }
            }
            SharedMedia item;
            if(msg.content.getConstructor() == TdApi.MessageAudio.CONSTRUCTOR) {
                synchronized(writtenAudio)
                {
                    if(writtenAudio.contains(((TdApi.MessageAudio)msg.content).audio.audio.id))
                    {
                        keepAudioSearch = true;
                        continue;
                    }
                }
                item = new AudioTrackModel(msg.id, msg.date, ((TdApi.MessageAudio) msg.content).audio);
            }
            else
                continue;
            if(dateOfFirstMessage == null) {
                dateOfFirstMessage = new Date(date);
            }
            synchronized(idToAudio)
            {
                idToAudio.put(msg.id, item);
            }
            models.add(item);
            if(previousDate == null)
                previousDate = new Date();
            previousDate.setTime(date);
            synchronized(writtenAudio)
            {
                writtenAudio.add(((TdApi.MessageAudio)msg.content).audio.audio.id);
            }
        }
        if(dateOfFirstMessage != null && audioLastDate != null)
        {
            cal.setTime(dateOfFirstMessage);
            previousCal.setTime(audioLastDate);
            imgInsertSectionToHead = cal.get(Calendar.MONTH) != previousCal.get(Calendar.MONTH) || cal.get(Calendar.YEAR) != previousCal.get(Calendar.YEAR);
        }
        else if(audioLastDate == null) {
            audioInsertSectionToHead = false;
            SharedMedia firstSection = new SharedMediaSectionModel(dateOfFirstMessage);
            models.add(0, firstSection);
        }
        audioLastDate = previousDate != null ? new Date(previousDate.getTime()) : null;
        return models;
    }

    private static class MediaReceiver extends PauseHandler
    {
        private WeakReference<SharedMediaViewController> controllerRef;

        public MediaReceiver(SharedMediaViewController controller)
        {
            this.controllerRef = new WeakReference<SharedMediaViewController>(controller);
        }

        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef.get() == null)
                return;
            if(message.obj == null)
                return;
            if(message.what == RECEIVING_IMAGES)
            {
                ArrayList<SharedMedia> result = (ArrayList<SharedMedia>) message.obj;
                int position = controllerRef.get().images.size();
                if(result.size() > 0) {
                    if(controllerRef.get().imgInsertSectionToHead)
                    {
                        SharedMediaSectionModel section = new SharedMediaSectionModel(controllerRef.get().imgLastDate);
                        result.add(0, section);
                    }
                    controllerRef.get().images.addAll(result);
                    controllerRef.get().imgAdapter.notifyItemRangeInserted(position, result.size());
                    controllerRef.get().imgSearchFromId = result.get(result.size()-1).getMessageId();
                    synchronized (controllerRef.get().loadingImages) {
                        controllerRef.get().loadingImages.set(false);
                    }
                }
            }
            else if(message.what == RECEIVING_AUDIO)
            {
                ArrayList<SharedMedia> result = (ArrayList<SharedMedia>) message.obj;
                int position = controllerRef.get().audio.size();
                if(result.size() > 0) {
                    if(controllerRef.get().audioInsertSectionToHead)
                    {
                        SharedMediaSectionModel section = new SharedMediaSectionModel(controllerRef.get().audioLastDate);
                        result.add(0, section);
                    }
                    controllerRef.get().audio.addAll(result);
                    controllerRef.get().audioAdapter.notifyItemRangeInserted(position, result.size());
                    synchronized (controllerRef.get().loadingImages) {
                        controllerRef.get().loadingAudio.set(false);
                    }
                }
                else if(controllerRef.get().keepAudioSearch)
                {
                    synchronized (controllerRef.get().loadingImages) {
                        controllerRef.get().loadingAudio.set(false);
                    }
                }
            }
        }
    }
}
