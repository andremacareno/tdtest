package com.andremacareno.tdtest.viewcontrollers.actionview;

import android.os.Message;
import android.view.MenuItem;
import android.widget.PopupMenu;

import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.models.CurrentUserModel;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.viewcontrollers.NotificationSettingsController;
import com.andremacareno.tdtest.views.actionviews.ProfileActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;

/**
 * Created by Andrew on 14.05.2015.
 */
public class ProfileActionViewController extends ActionViewController {
    private UserModel boundUser = null;
    private PauseHandler notificationSettingsHandler;
    private NotificationSettingsController notificationSettingsController;
    private TdApi.NotificationSettingsForChat scopeForThisChat;


    private boolean updatedName = false;
    private boolean updatedMyLink = false;
    private boolean updatedPhoto = false;
    private boolean updatedPhoneNumber = false;
    private boolean updatedUsername = false;

    public ProfileActionViewController(UserModel user)
    {
        this.boundUser = user;
        this.scopeForThisChat = null;
    }
    public ProfileActionViewController(UserModel user, long chatId)
    {
        this(user);
        if(CurrentUserModel.getInstance().getUserModel().getId() != user.getId())
            this.scopeForThisChat = new TdApi.NotificationSettingsForChat(chatId);
    }
    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        if(scopeForThisChat != null && notificationSettingsController == null)
        {
            notificationSettingsHandler = new GetNotificationSettingsHandler(this, service);
            TdApi.GetNotificationSettings getNotificationSettingsReq = new TdApi.GetNotificationSettings(scopeForThisChat);
            TG.getClientInstance().send(getNotificationSettingsReq, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    if(object != null && object.getConstructor() == TdApi.NotificationSettings.CONSTRUCTOR)
                    {
                        Message msg = Message.obtain(notificationSettingsHandler);
                        msg.obj = object;
                        msg.sendToTarget();
                    }
                }
            });
        }
    }
    private void initNotificationSettingsController(TdApi.NotificationSettings initialSettings, TelegramUpdatesHandler service)
    {

        notificationSettingsController = new NotificationSettingsController(scopeForThisChat.chatId, false, initialSettings, ((ProfileActionView) getActionView()).notificationsButton);
        notificationSettingsController.attachObservers(service);
        ((ProfileActionView) getActionView()).notificationsMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                notificationSettingsController.setNotificationSettingsRequested(menuItem.getItemId());
                return true;
            }
        });
    }


    @Override
    public void onFragmentPaused() {
    }

    @Override
    public void onFragmentResumed() {
        if(scopeForThisChat == null)
            ((ProfileActionView) getActionView()).hideNotificationsButton();
        fillLayout();
    }

    @Override
    public void onActionViewRemoved() {
        if(notificationSettingsController != null)
            notificationSettingsController.detachObservers();
    }

    public void updateStatus(TdApi.UserStatus newStatus)
    {
        ((ProfileActionView) getActionView()).userView.updateUserStatus(newStatus);
    }
    public void updateDisplayName(String newDisplayName)
    {
        ((ProfileActionView) getActionView()).userView.updateDisplayName(newDisplayName);
    }
    public void updatePhoto(TdApi.File newPhoto)
    {
        ((ProfileActionView) getActionView()).userView.updateUserPhoto(newPhoto);
    }

    protected void fillLayout()
    {
        ProfileActionView v = (ProfileActionView) getActionView();
        v.userView.bindUser(this.boundUser);
        UserModel currentUser = CurrentUserModel.getInstance().getUserModel();
        if(currentUser != null && (currentUser.getId() == this.boundUser.getId() || currentUser.isDeletedAccount()) && !forceAddMenu())
        {
            v.hideNotificationsButton();
            v.hideMenuButton();
        }
        else
           initMenu();
    }
    protected boolean forceAddMenu()
    {
        return false;
    }
    public void initMenu()
    {
        if(boundUser == null)
            return;
        ProfileActionView v = (ProfileActionView) getActionView();
        if(boundUser.isMyContact()) {
            if(boundUser.isBlocked())
                v.setMenuResource(R.menu.profile_blocked);
            else
                v.setMenuResource(R.menu.profile);
        }
        else if(boundUser.getPhoneNumber().length() == 0)
        {
            if(boundUser.isBlocked())
                v.setMenuResource(R.menu.profile_no_phone_blocked);
            else
                v.setMenuResource(R.menu.profile_no_phone);
        }
        else
        {
            if(boundUser.isBlocked())
                v.setMenuResource(R.menu.profile_knows_phone_blocked);
            else
                v.setMenuResource(R.menu.profile_knows_phone);
        }
    }


    public void setMenuListener(PopupMenu.OnMenuItemClickListener listener)
    {
        ((ProfileActionView) getActionView()).defaultMenu.setOnMenuItemClickListener(listener);
    }
    public void blockUser()
    {
        TdApi.BlockUser blockUserFunc = new TdApi.BlockUser(boundUser.getId());
        TG.getClientInstance().send(blockUserFunc, TelegramApplication.dummyResultHandler);
        boundUser.updateUserBlocked(true);
        initMenu();
    }
    public void unblockUser()
    {
        TdApi.UnblockUser unblockUserFunc = new TdApi.UnblockUser(boundUser.getId());
        TG.getClientInstance().send(unblockUserFunc, TelegramApplication.dummyResultHandler);
        boundUser.updateUserBlocked(false);
        initMenu();
    }
    public void removeUserFromContacts()
    {
        TdApi.DeleteContacts delContactsFunc = new TdApi.DeleteContacts(new int[] {boundUser.getId()});
        TG.getClientInstance().send(delContactsFunc, TelegramApplication.dummyResultHandler);
        boundUser.didRemovedFromContacts();
        initMenu();
    }
    private static class GetNotificationSettingsHandler extends PauseHandler
    {
        private WeakReference<ProfileActionViewController> controllerRef;
        private WeakReference<TelegramUpdatesHandler> serviceRef;
        public GetNotificationSettingsHandler(ProfileActionViewController controller, TelegramUpdatesHandler service)
        {
            this.controllerRef = new WeakReference<ProfileActionViewController>(controller);
            this.serviceRef = new WeakReference<TelegramUpdatesHandler>(service);
        }
        @Override
        protected boolean storeOnlyLastMessage()
        {
            return false;
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            TdApi.NotificationSettings settings = (TdApi.NotificationSettings) message.obj;

            if(controllerRef.get() == null || serviceRef.get() == null)
                return;
            controllerRef.get().initNotificationSettingsController(settings, serviceRef.get());
        }
    }
}
