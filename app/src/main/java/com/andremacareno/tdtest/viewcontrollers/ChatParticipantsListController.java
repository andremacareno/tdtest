package com.andremacareno.tdtest.viewcontrollers;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.GroupCache;
import com.andremacareno.tdtest.PeerType;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.fragments.GroupChatProfileFragment;
import com.andremacareno.tdtest.listadapters.ChatParticipantsAdapter;
import com.andremacareno.tdtest.models.ChatParticipantModel;
import com.andremacareno.tdtest.models.CurrentUserModel;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.tasks.GenerateParticipantsListTask;
import com.andremacareno.tdtest.tasks.GetFullPeerTask;
import com.andremacareno.tdtest.updatelisteners.UpdateChatProcessor;
import com.andremacareno.tdtest.updatelisteners.UpdateUserProcessor;
import com.andremacareno.tdtest.views.ChatParticipantsListView;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * Created by andremacareno on 04/04/15.
 */
public class ChatParticipantsListController extends ViewController {
    //statement variables
    protected final String TAG = "ParticipantsListControl";
    protected RecyclerView.Adapter adapter;
    private GroupChatProfileFragment.ParticipantListDelegate delegate = null;
    private UpdateUserProcessor updateUserProcessor;
    private UpdateChatProcessor updateChatProcessor;
    private TdApi.GroupFull chatInfo;
    private final TIntObjectHashMap<ChatParticipantModel> participantsMap = new TIntObjectHashMap<>();
    private final ArrayList<UserModel> participantsList = new ArrayList<>();
    private long chatId;
    public ChatParticipantsListController(View v, int groupId, long chatId) {
        super(v);
        this.chatInfo = GroupCache.getInstance().getGroupFull(groupId, false);
        this.chatId = chatId;
        init();
        generateParticipantsList();
    }
    private void generateParticipantsList()
    {
        if(chatInfo == null)
            return;
        GenerateParticipantsListTask task = new GenerateParticipantsListTask(chatInfo.group.id);
        task.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(AsyncTaskManTask task) {
                ArrayList<ChatParticipantModel> participants = ((GenerateParticipantsListTask) task).getParticipants();
                synchronized (participantsList) {
                    synchronized (participantsMap) {
                        participantsList.clear();
                        participantsMap.clear();
                        for (ChatParticipantModel p : participants) {
                            participantsList.add(p);
                            participantsMap.put(p.getId(), p);
                        }
                    }
                }
                TelegramApplication.applicationHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        synchronized (participantsList) {
                            ChatParticipantsListView v = (ChatParticipantsListView) getView();
                            v.setNewElementCount(participantsList.size());
                        }
                    }
                }, 300);
            }
        });
        task.addToQueue();
        /*for(TdApi.ChatParticipant p : chatInfo.participants)
        {
            ChatParticipantModel user = new ChatParticipantModel(p);
            synchronized(participantsList)
            {
                participantsList.add(user);
            }
            synchronized(participantsMap)
            {
                participantsMap.put(p.user.id, user);
            }
        }
        sortUsers();*/
    }
    @Override
    protected void onViewReplaced() {
        if(getView() != null) {
            initViewSettings();
        }
    }
    public void onFragmentPaused()
    {
        updateUserProcessor.performPauseObservers();
        updateChatProcessor.performPauseObservers();
    }
    public void onFragmentResumed()
    {
        updateUserProcessor.performResumeObservers();
        updateChatProcessor.performResumeObservers();
    }
    public void addDelegate(GroupChatProfileFragment.ParticipantListDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        updateUserProcessor.attachObservers(service);
        updateChatProcessor.attachObservers(service);
    }
    public void removeObservers()
    {
        updateUserProcessor.removeObservers();
        updateChatProcessor.removeObservers();
    }

    private void init()
    {
        this.adapter = getAdapter();
        this.updateChatProcessor = new UpdateChatProcessor() {
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                return String.format("viewchat_group_%d_observer", chatInfo.group.id);
            }

            @Override
            protected boolean shouldObserveUpdateGroup() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateTitle() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateReplyMarkup() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateReadInbox() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateReadOutbox() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateChat() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateChatPhoto() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateChatOrder() {
                return false;
            }

            @Override
            protected void processUpdateInBackground(TdApi.Update upd) {
            }

            @Override
            protected void processUpdateInUiThread(TdApi.Update upd) {
                if(upd != null && upd.getConstructor() == TdApi.UpdateGroup.CONSTRUCTOR)
                {
                    TdApi.UpdateGroup cast = (TdApi.UpdateGroup) upd;
                    if(cast.group.id != chatInfo.group.id)
                        return;
                    if(cast.group.participantsCount != chatInfo.group.participantsCount)
                    {
                        chatInfo = GroupCache.getInstance().getGroupFull(chatInfo.group.id, true);
                        generateParticipantsList();
                    }
                }
            }
        };
        this.updateUserProcessor = new UpdateUserProcessor() {
            private final String updateUserKey = String.format(Constants.VIEWCHAT_USER_DATA_OBSERVER, chatId);
            private final String updateStatusKey = String.format(Constants.VIEWCHAT_USER_STATUS_OBSERVER, chatId);
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                if(constructor == TdApi.UpdateUser.CONSTRUCTOR)
                    return updateUserKey;
                return updateStatusKey;
            }

            @Override
            protected boolean shouldObserveUpdateUserStatus() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateUserBlocked() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateUser() {
                return true;
            }

            @Override
            protected void processUpdateInBackground(TdApi.Update upd) {
                if(upd.getConstructor() == TdApi.UpdateUser.CONSTRUCTOR)
                {
                    TdApi.UpdateUser castUpd = (TdApi.UpdateUser) upd;
                    synchronized(participantsMap)
                    {
                        if(participantsMap.containsKey(castUpd.user.id))
                            participantsMap.get(castUpd.user.id).updateUser(castUpd.user);
                    }
                }
            }

            @Override
            protected void processUpdateInUiThread(TdApi.Update upd) {
                int constructor = upd.getConstructor();
                if(constructor != TdApi.UpdateUser.CONSTRUCTOR && constructor != TdApi.UpdateUserStatus.CONSTRUCTOR)
                    return;
                int index = getIndexOfUserById(constructor == TdApi.UpdateUser.CONSTRUCTOR ? ((TdApi.UpdateUser) upd).user.id : ((TdApi.UpdateUserStatus) upd).userId);
                if(index >= 0) {
                    adapter.notifyItemChanged(index);
                    if(upd.getConstructor() == TdApi.UpdateUserStatus.CONSTRUCTOR) {
                        int newIndex = updateUserStatus((TdApi.UpdateUserStatus) upd);
                        try
                        {
                            if(newIndex >= 0)
                                adapter.notifyItemMoved(index, newIndex);
                        }
                        catch(Exception ignored) {}
                    }
                }
            }
        };
        if(getView() != null) {
            initViewSettings();
        }
    }
    private int updateUserStatus(TdApi.UpdateUserStatus upd)
    {
        int index = getIndexOfUserById(upd.userId);
        if(index == -1)
            return -1;
        synchronized(participantsList)
        {
            UserModel user = participantsList.get(index);
            boolean wasOnline = user.isOnline();
            user.updateStatus(upd.status);
            int onlineCount = countOnlineMembers();
            boolean becameOnline = user.isOnline();
            if(wasOnline && !becameOnline)
            {
                //move to bottom
                participantsList.remove(index);
                participantsList.add(onlineCount, user);
                return onlineCount;
            }
            else if(!wasOnline && becameOnline)
            {
                //move to top
                participantsList.remove(index);
                participantsList.add(0, user);
                return 0;
            }
        }
        return -1;
    }
    private int countOnlineMembers()
    {
        int newCount = 0;
        synchronized (participantsList)
        {
            for(UserModel u : participantsList) {
                if(u.isOnline() && !u.isBot())
                    newCount++;
            }
        }
        return newCount;
    }
    private int getIndexOfUserById(int id)
    {
        synchronized(participantsList)
        {
            synchronized(participantsMap)
            {
                if(participantsMap.containsKey(id))
                    return participantsList.indexOf(participantsMap.get(id));
                return -1;
            }
        }
    }
    private void initViewSettings()
    {
        ChatParticipantsListView v = (ChatParticipantsListView) getView();
        synchronized(participantsList)
        {
            v.setNewElementCount(participantsList.size());
        }
        v.setAdapter(adapter);
        v.setMotionEventSplittingEnabled(false);
    }


    public void didItemClicked(int position)
    {
        synchronized(participantsList)
        {
            int id = participantsList.get(position).getId();
            GetFullPeerTask task = new GetFullPeerTask(id, PeerType.USER);
            task.onComplete(new OnTaskCompleteListener() {
                @Override
                public void taskComplete(AsyncTaskManTask task) {
                    final UserModel userInfo = (UserModel) ((GetFullPeerTask) task).getResult();
                    final long chatId = ((GetFullPeerTask) task).getChatId();
                    TelegramApplication.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(delegate != null)
                                delegate.goToParticipantProfile(userInfo.getId(), chatId);
                        }
                    });
                }
            });
            task.addToQueue();
        }
    }
    protected RecyclerView.Adapter getAdapter()
    {
        if(this.adapter == null)
            this.adapter = new ChatParticipantsAdapter(participantsList, this);
        return this.adapter;
    }
    public int[] getParticipantIds() {
        synchronized(participantsMap)
        {
            int[] arr = new int[participantsMap.size()];
            participantsMap.keys(arr);
            return arr;
        }
    }

    public void didItemLongClicked(int position) {
        synchronized(participantsList)
        {
            synchronized(participantsMap)
            {
                UserModel userInfo = participantsList.get(position);
                ChatParticipantModel participant = participantsMap.get(userInfo.getId());
                int inviterId = participant.getInviterId();
                int currentUserId = CurrentUserModel.getInstance().getUserModel().getId();
                int adminId = chatInfo.adminId;
                if(userInfo.getId() != currentUserId && (currentUserId == inviterId || currentUserId == adminId))
                {
                    if(delegate != null)
                        delegate.removeParticipant(participant.getId());
                }
            }
        }
    }
}
