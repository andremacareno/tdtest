package com.andremacareno.tdtest.viewcontrollers.actionview;

import android.os.Message;
import android.view.MenuItem;
import android.widget.PopupMenu;

import com.andremacareno.tdtest.ChannelCache;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.updatelisteners.UpdateChannelProcessor;
import com.andremacareno.tdtest.updatelisteners.UpdateChatProcessor;
import com.andremacareno.tdtest.viewcontrollers.NotificationSettingsController;
import com.andremacareno.tdtest.views.actionviews.ChannelActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;

/**
 * Created by Andrew on 14.05.2015.
 */
public class ChannelProfileActionViewController extends ActionViewController {
    private int boundChannel = 0;
    private long chatId = 0;
    private PauseHandler notificationSettingsHandler;
    private NotificationSettingsController notificationSettingsController;
    private TdApi.NotificationSettingsForChat scopeForThisChat;
    private UpdateChannelProcessor channelProcessor;
    private UpdateChatProcessor chatProcessor;

    public ChannelProfileActionViewController(int channelId)
    {
        this.boundChannel = channelId;
        this.chatId = ChatCache.getInstance().getChatByChannelId(channelId).id;
        this.scopeForThisChat = new TdApi.NotificationSettingsForChat(chatId);
        initObservers();
    }

    private void initObservers()
    {
        chatProcessor = new UpdateChatProcessor() {
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                if(constructor == TdApi.UpdateChatTitle.CONSTRUCTOR)
                    return String.format(Constants.VIEWCHANNEL_UPDATE_CHAT_TITLE_OBSERVER, chatId);
                return String.format(Constants.VIEWCHANNEL_UPDATE_CHAT_PHOTO_OBSERVER, chatId);
            }

            @Override
            protected boolean shouldObserveUpdateGroup() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateTitle() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateReplyMarkup() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateReadInbox() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateReadOutbox() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateChat() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateChatPhoto() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateChatOrder() {
                return false;
            }

            @Override
            protected void processUpdateInBackground(TdApi.Update upd) {

            }

            @Override
            protected void processUpdateInUiThread(TdApi.Update upd) {
                if(upd.getConstructor() == TdApi.UpdateChatTitle.CONSTRUCTOR && ((TdApi.UpdateChatTitle) upd).chatId == chatId)
                    updateTitle(((TdApi.UpdateChatTitle) upd).title);
                else if(upd.getConstructor() == TdApi.UpdateChatPhoto.CONSTRUCTOR && ((TdApi.UpdateChatPhoto) upd).chatId == chatId)
                    updatePhoto(CommonTools.isLowDPIScreen() ? ((TdApi.UpdateChatPhoto) upd).photo.small : ((TdApi.UpdateChatPhoto) upd).photo.big);
            }
        };
        //TODO observe role change
        channelProcessor = new UpdateChannelProcessor() {
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                return String.format(Constants.VIEWCHANNEL_UPDATE_CHANNEL_OBSERVER, boundChannel);
            }

            @Override
            protected void processUpdateInBackground(TdApi.Update upd) {

            }

            @Override
            protected void processUpdateInUiThread(TdApi.Update upd) {

            }
        };
    }
    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        if(scopeForThisChat != null && notificationSettingsController == null)
        {
            notificationSettingsHandler = new GetNotificationSettingsHandler(this, service);
            TdApi.GetNotificationSettings getNotificationSettingsReq = new TdApi.GetNotificationSettings(scopeForThisChat);
            TG.getClientInstance().send(getNotificationSettingsReq, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    if(object != null && object.getConstructor() == TdApi.NotificationSettings.CONSTRUCTOR)
                    {
                        Message msg = Message.obtain(notificationSettingsHandler);
                        msg.obj = object;
                        msg.sendToTarget();
                    }
                }
            });
        }
        chatProcessor.attachObservers(service);
        channelProcessor.attachObservers(service);
    }
    private void initNotificationSettingsController(TdApi.NotificationSettings initialSettings, TelegramUpdatesHandler service)
    {
        notificationSettingsController = new NotificationSettingsController(scopeForThisChat.chatId, false, initialSettings, ((ChannelActionView) getActionView()).notificationsButton);
        notificationSettingsController.attachObservers(service);
        ((ChannelActionView) getActionView()).notificationsMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                notificationSettingsController.setNotificationSettingsRequested(menuItem.getItemId());
                return true;
            }
        });
    }


    @Override
    public void onFragmentPaused() {
        chatProcessor.performPauseObservers();
        channelProcessor.performPauseObservers();
    }

    @Override
    public void onFragmentResumed() {
        chatProcessor.performResumeObservers();
        channelProcessor.performResumeObservers();
        if(scopeForThisChat == null)
            ((ChannelActionView) getActionView()).hideNotificationsButton();
        fillLayout();
    }

    @Override
    public void onActionViewRemoved() {
        if(notificationSettingsController != null)
            notificationSettingsController.detachObservers();
        chatProcessor.removeObservers();
        channelProcessor.removeObservers();
    }

    public void updatePhoto(TdApi.File newPhoto)
    {
        ((ChannelActionView) getActionView()).channelView.updateUserPhoto(FileCache.getInstance().getFileModel(newPhoto));
    }
    public void updateTitle(String newTitle)
    {
        ((ChannelActionView) getActionView()).channelView.updateTitle(newTitle);
    }

    protected void fillLayout()
    {
        ChannelActionView v = (ChannelActionView) getActionView();
        v.channelView.bindChannel(boundChannel);
        TdApi.Channel channel = ((TdApi.ChannelChatInfo) ChatCache.getInstance().getChatById(chatId).type).channel;
        if(channel.role.getConstructor() == Constants.PARTICIPANT_LEFT.getConstructor() || channel.role.getConstructor() == Constants.PARTICIPANT_KICKED.getConstructor())
        {
            v.hideNotificationsButton();
            v.hideMenuButton();
        }
        else
           initMenu();
    }
    protected boolean forceAddMenu()
    {
        return false;
    }
    protected void initMenu()
    {
        if(boundChannel == 0)
            return;
        TdApi.Channel ch = ChannelCache.getInstance().getChannel(boundChannel);
        if(ch == null)
            return;
        int res = ch.username.isEmpty() ? (ch.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() ? R.menu.channel_profile_private_admin : R.menu.channel_profile) : (ch.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() ? R.menu.channel_profile_public_admin : R.menu.channel_profile);
        ChannelActionView v = (ChannelActionView) getActionView();
        v.setMenuResource(res);
    }


    public void setMenuListener(PopupMenu.OnMenuItemClickListener listener)
    {
        ((ChannelActionView) getActionView()).defaultMenu.setOnMenuItemClickListener(listener);
    }
    private static class GetNotificationSettingsHandler extends PauseHandler
    {
        private WeakReference<ChannelProfileActionViewController> controllerRef;
        private WeakReference<TelegramUpdatesHandler> serviceRef;
        public GetNotificationSettingsHandler(ChannelProfileActionViewController controller, TelegramUpdatesHandler service)
        {
            this.controllerRef = new WeakReference<ChannelProfileActionViewController>(controller);
            this.serviceRef = new WeakReference<TelegramUpdatesHandler>(service);
        }
        @Override
        protected boolean storeOnlyLastMessage()
        {
            return false;
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            TdApi.NotificationSettings settings = (TdApi.NotificationSettings) message.obj;

            if(controllerRef.get() == null || serviceRef.get() == null)
                return;
            controllerRef.get().initNotificationSettingsController(settings, serviceRef.get());
        }
    }
}
