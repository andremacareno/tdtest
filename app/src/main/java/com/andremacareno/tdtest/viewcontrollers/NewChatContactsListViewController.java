package com.andremacareno.tdtest.viewcontrollers;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.andremacareno.tdtest.ChatParticipantsCountListener;
import com.andremacareno.tdtest.fragments.NewGroupFragment;
import com.andremacareno.tdtest.listadapters.ContactsAdapter;

import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 04/04/15.
 */
public class NewChatContactsListViewController extends ContactsListViewController implements ChatParticipantsCountListener {
    //statement variables
    private int chatParticipantsCount;
    private WeakReference<NewGroupFragment> fragmentRef;
    public NewChatContactsListViewController(View v) {
        super(v);
        this.chatParticipantsCount = 0;
    }
    @Override
    protected void onViewReplaced() {
        super.onViewReplaced();
    }
    @Override
    public void didItemClicked(int position)
    { /* do nothing */ }

    @Override
    protected RecyclerView.Adapter getAdapter()
    {
        if(this.adapter == null)
            this.adapter = new ContactsAdapter(data, this, true);
        return this.adapter;
    }

    //interfaces implementation
    @Override
    public void didChatParticipantsCountChanged(int newCount) {
        this.chatParticipantsCount = newCount;
        if(newCount < 1)
            getFragmentReference().disableDoneButton();
        else
            getFragmentReference().enableDoneButton();
        getFragmentReference().updateActionViewSubtitle(chatParticipantsCount);
    }


    public void setFragmentReference(NewGroupFragment fr)
    {
        this.fragmentRef = new WeakReference<NewGroupFragment>(fr);
    }

    public NewGroupFragment getFragmentReference() { return this.fragmentRef.get(); }

    public int[] getParticipants()
    {
        return ((ContactsAdapter)getAdapter()).getParticipantIds();
    }

}
