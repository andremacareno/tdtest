package com.andremacareno.tdtest.viewcontrollers;

import com.andremacareno.tdtest.TelegramApplication;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.concurrent.atomic.AtomicBoolean;

import gnu.trove.list.array.TIntArrayList;

/**
 * Created by andremacareno on 06/03/16.
 */
public class ChatSearchController {
    public interface ChatSearchDelegate
    {
        void requestScrollTo(int messageId);
        void onNoResults();
        void searchAboveFinished();
        void searchBelowFinished();
    }
    private volatile int index = 0;
    private final AtomicBoolean searching = new AtomicBoolean(false);
    private final AtomicBoolean searchFinished = new AtomicBoolean(false);
    private final TIntArrayList searchResults = new TIntArrayList();
    private final TdApi.SearchChatMessages searchReq = new TdApi.SearchChatMessages();
    private final ChatSearchDelegate delegate;
    private final Runnable searchAboveFinished, searchBelowFinished, onNoResultsRunnable;
    public ChatSearchController(long chatId, final ChatSearchDelegate delegate)
    {
        searchReq.chatId = chatId;
        searchReq.filter = new TdApi.SearchMessagesFilterEmpty();
        searchReq.importantOnly = false;
        searchReq.limit = 10;
        this.delegate = delegate;
        searchAboveFinished = new Runnable() {
            @Override
            public void run() {
                if(delegate != null)
                    delegate.searchAboveFinished();
            }
        };
        searchBelowFinished = new Runnable() {
            @Override
            public void run() {
                if(delegate != null)
                    delegate.searchBelowFinished();
            }
        };
        onNoResultsRunnable = new Runnable() {
            @Override
            public void run() {
                if(delegate != null)
                    delegate.onNoResults();
            }
        };
    }
    private boolean searchModeActive()
    {
        synchronized(searchResults)
        {
            return !searchResults.isEmpty();
        }
    }
    public void submit(String query)
    {
        synchronized(searching)
        {
            if(searching.get())
                return;
            searching.set(true);
        }
        synchronized(searchFinished)
        {
            searchFinished.set(false);
        }
        synchronized(searchResults)
        {
            searchResults.clear();
        }
        index = 0;
        searchReq.query = query;
        searchReq.fromMessageId = 0;
        TG.getClientInstance().send(searchReq, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                try
                {
                    TdApi.Messages cast = (TdApi.Messages) object;
                    synchronized(searchResults)
                    {
                        for(TdApi.Message msg : cast.messages)
                            searchResults.add(msg.id);
                        if(cast.totalCount < searchReq.limit)
                        {
                            synchronized(searchFinished)
                            {
                                searchFinished.set(true);
                            }
                        }
                    }
                }
                finally
                {
                    synchronized(searchResults)
                    {
                        if(searchResults.isEmpty()) {
                            TelegramApplication.applicationHandler.post(onNoResultsRunnable);
                            synchronized(searchFinished)
                            {
                                searchFinished.set(true);
                            }
                        }
                        else
                        {
                            final int firstMessage = searchResults.get(0);
                            TelegramApplication.applicationHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if(delegate != null)
                                        delegate.requestScrollTo(firstMessage);
                                }
                            });
                        }
                    }
                    synchronized(searching)
                    {
                        searching.set(false);
                    }
                }
            }
        });
    }
    public void searchAbove()
    {
        if(!searchModeActive())
            return;
        synchronized(searchResults)
        {
            if(index < searchResults.size() - 1)
            {
                index++;
                final int message = searchResults.get(index);
                if(delegate != null)
                {
                    TelegramApplication.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(delegate != null)
                                delegate.requestScrollTo(message);
                        }
                    });
                }
                return;
            }
        }
        synchronized(searching)
        {
            if(searching.get())
                return;
            searching.set(true);
        }
        synchronized(searchFinished)
        {
            if(searchFinished.get())
            {
                TelegramApplication.applicationHandler.post(searchAboveFinished);
                return;
            }
        }
        searchReq.fromMessageId = searchResults.get(index);
        TG.getClientInstance().send(searchReq, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                try
                {
                    TdApi.Messages cast = (TdApi.Messages) object;
                    synchronized(searchResults)
                    {
                        for(TdApi.Message msg : cast.messages)
                            searchResults.add(msg.id);
                        if(cast.totalCount < searchReq.limit)
                        {
                            synchronized(searchFinished)
                            {
                                searchFinished.set(true);
                            }
                        }
                    }
                }
                finally
                {
                    synchronized(searchResults)
                    {
                        if(searchResults.size() == index) {
                            TelegramApplication.applicationHandler.post(onNoResultsRunnable);
                            synchronized(searchFinished)
                            {
                                searchFinished.set(true);
                            }
                        }
                        else
                        {
                            index++;
                            final int message = searchResults.get(index);
                            if(delegate != null)
                            {
                                TelegramApplication.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(delegate != null)
                                            delegate.requestScrollTo(message);
                                    }
                                });
                            }
                        }
                    }
                    synchronized(searching)
                    {
                        searching.set(false);
                    }
                }
            }
        });
    }
    public void searchBelow()
    {
        if(!searchModeActive())
            return;
        if(index > 0)
        {
            index--;
            synchronized(searchResults)
            {
                final int message = searchResults.get(index);
                if(delegate != null)
                {
                    TelegramApplication.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(delegate != null)
                                delegate.requestScrollTo(message);
                        }
                    });
                }
            }
        }
        else
            TelegramApplication.applicationHandler.post(searchBelowFinished);
    }

}
