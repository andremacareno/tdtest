package com.andremacareno.tdtest.viewcontrollers;

import android.database.ContentObserver;
import android.os.Message;
import android.provider.MediaStore;
import android.view.View;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.listadapters.GalleryImagesListAdapter;
import com.andremacareno.tdtest.models.GalleryImageEntry;
import com.andremacareno.tdtest.tasks.ConvertGalleryEntriesToInputMsgTask;
import com.andremacareno.tdtest.tasks.LoadImagesFromGalleryTask;
import com.andremacareno.tdtest.views.AttachMenuView;
import com.andremacareno.tdtest.views.RecentFromGalleryListView;

import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import gnu.trove.TCollections;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.hash.TIntHashSet;

/**
 * Created by andremacareno on 17/04/15.
 */
public class GalleryRecentPhotosController extends ViewController{
    //statement variables
    private final String TAG = "GalleryRecent";
    private GalleryImagesListAdapter adapter;
    private ArrayList<GalleryImageEntry> entries = new ArrayList<>();
    private final TIntObjectMap<GalleryImageEntry> idsToEntries = TCollections.synchronizedMap(new TIntObjectHashMap<GalleryImageEntry>());
    private PauseHandler photoEntryReceiver;
    private PauseHandler sendImageReceiver;
    private ContentObserver externalObserver, internalObserver;
    private volatile LoadImagesFromGalleryTask loadingTask;
    private AttachMenuView.AttachMenuDelegate delegate;
    private Runnable refreshRunnable;
    public GalleryRecentPhotosController(View v) {
        super(v);
        init();
    }
    private void init()
    {
        this.adapter = new GalleryImagesListAdapter(this, idsToEntries, entries);
        this.photoEntryReceiver = new GalleryImgReceiver(this);
        this.sendImageReceiver = new InputMessagePhotoHandler(this);
        refreshRunnable = new Runnable() {
            @Override
            public void run() {
                requestPhotos();
            }
        };
        externalObserver = new GalleryUpdateObserver();
        internalObserver = new GalleryUpdateObserver();
        try {
            TelegramApplication.sharedApplication().getApplicationContext().getContentResolver().registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, false, externalObserver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            TelegramApplication.sharedApplication().getApplicationContext().getContentResolver().registerContentObserver(MediaStore.Images.Media.INTERNAL_CONTENT_URI, false, internalObserver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initViewSettings();
    }
    @Override
    protected void onViewReplaced() {
        if(getView() != null) {
            initViewSettings();
        }
    }
    public GalleryImagesListAdapter getAdapter() { return this.adapter; }
    private void initViewSettings() {
        RecentFromGalleryListView v = (RecentFromGalleryListView) getView();
        v.setAdapter(adapter);
        v.setMotionEventSplittingEnabled(false);
        if(entries.size() == 0)
            requestPhotos();
    }

    @Override
    public void onFragmentPaused() {
        if(photoEntryReceiver != null)
            photoEntryReceiver.pause();
    }

    @Override
    public void onFragmentResumed() {
        if(photoEntryReceiver != null)
            photoEntryReceiver.resume();
    }
    public void setDelegate(AttachMenuView.AttachMenuDelegate delegateRef)
    {
        this.delegate = delegateRef;
        adapter.setDelegate(this.delegate);
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }
    public void removeObservers()
    {
        try {
            TelegramApplication.sharedApplication().getApplicationContext().getContentResolver().unregisterContentObserver(externalObserver);
        } catch (Exception e) {
        }
        try {
            TelegramApplication.sharedApplication().getApplicationContext().getContentResolver().unregisterContentObserver(internalObserver);
        } catch (Exception e) {
        }
    }

    private void requestPhotos()
    {
        if(loadingTask != null)
            loadingTask.cancel();
        loadingTask = new LoadImagesFromGalleryTask();
        loadingTask.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(AsyncTaskManTask task) {
                if(((LoadImagesFromGalleryTask) task).isCancelled())
                    return;
                if(((LoadImagesFromGalleryTask) task).getEntries() == null)
                    return;
                try {
                    synchronized(((LoadImagesFromGalleryTask) task).getEntries())
                    {
                        if(!((LoadImagesFromGalleryTask) task).getEntries().isEmpty())
                        {
                            synchronized(idsToEntries)
                            {
                                idsToEntries.clear();
                                for(GalleryImageEntry e : ((LoadImagesFromGalleryTask) task).getEntries())
                                    idsToEntries.put(e.getThumbId(), e);
                            }
                            Message msg = Message.obtain(photoEntryReceiver);
                            msg.obj = ((LoadImagesFromGalleryTask) task).getEntries();
                            msg.sendToTarget();
                        }
                    }
                }
                catch(Exception e) { e.printStackTrace(); }
                finally {
                    loadingTask = null;
                }
            }
        });
        loadingTask.addToQueue();
    }
    public void sendSelection()
    {
        TIntHashSet selection = adapter.getSelectionIds();
        if(selection.size() == 0)
            return;
        ConvertGalleryEntriesToInputMsgTask task = new ConvertGalleryEntriesToInputMsgTask(entries, idsToEntries, selection);
        task.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(AsyncTaskManTask task) {
                ArrayList<TdApi.InputMessageContent> msgs = ((ConvertGalleryEntriesToInputMsgTask) task).getMessages();
                for(TdApi.InputMessageContent inputMessage : msgs)
                {
                    Message msg = Message.obtain(sendImageReceiver);
                    msg.obj = inputMessage;
                    msg.sendToTarget();
                }
            }
        });
        adapter.clearSelection();
        task.addToQueue();
    }
    public static class GalleryImgReceiver extends PauseHandler
    {
        private WeakReference<GalleryRecentPhotosController> controllerRef;
        public GalleryImgReceiver(GalleryRecentPhotosController c)
        {
            this.controllerRef = new WeakReference<GalleryRecentPhotosController>(c);
        }

        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef.get() == null)
                return;
            try
            {
                ArrayList<GalleryImageEntry> newEntries = (ArrayList<GalleryImageEntry>) message.obj;
                controllerRef.get().entries.clear();
                controllerRef.get().entries.addAll(newEntries);
                controllerRef.get().adapter.notifyDataSetChanged();
            }
            catch(Exception e) { e.printStackTrace(); }
        }
    }
    public static class InputMessagePhotoHandler extends PauseHandler
    {
        private WeakReference<GalleryRecentPhotosController> controllerRef;
        public InputMessagePhotoHandler(GalleryRecentPhotosController c)
        {
            this.controllerRef = new WeakReference<GalleryRecentPhotosController>(c);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef.get() == null)
                return;
            TdApi.InputMessageContent msg = (TdApi.InputMessageContent) message.obj;
            AttachMenuView.AttachMenuDelegate delegate = controllerRef.get().delegate;
            if(delegate != null)
                delegate.sendGalleryImage(msg);

        }
    }
    private class GalleryUpdateObserver extends ContentObserver
    {
        public GalleryUpdateObserver() {
            super(null);
        }
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            TelegramApplication.applicationHandler.postDelayed(refreshRunnable, 2000);
        }
    }
}
