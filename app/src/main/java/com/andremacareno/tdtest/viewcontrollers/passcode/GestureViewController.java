package com.andremacareno.tdtest.viewcontrollers.passcode;

import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.TelegramApplication;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by andremacareno on 05/08/15.
 */
public class GestureViewController implements GestureOverlayView.OnGesturePerformedListener {
    private GestureLibrary gLib;
    private PasscodeGestureController controller;
    public GestureViewController(PasscodeGestureController controllerRef)
    {
        File cacheDir = TelegramApplication.sharedApplication().getApplicationContext().getExternalCacheDir() == null ? TelegramApplication.sharedApplication().getApplicationContext().getCacheDir() : TelegramApplication.sharedApplication().getApplicationContext().getExternalCacheDir();
        gLib = GestureLibraries.fromFile(cacheDir.getAbsolutePath().concat("/gestures"));
        gLib.load();
        this.controller = controllerRef;
    }


    @Override
    public void onGesturePerformed(GestureOverlayView gestureOverlayView, Gesture gesture) {

        if(controller.currentMode == PasscodeViewController.Mode.SET)
        {
            gLib.removeEntry(Constants.GESTURE_LIBRARY);
            gLib.addGesture(Constants.GESTURE_LIBRARY, gesture);
            gLib.save();
            controller.setCurrentMode(PasscodeViewController.Mode.CONFIRMATION);
        }
        else if(controller.currentMode == PasscodeViewController.Mode.CONFIRMATION)
        {
            ArrayList<Gesture> tmp = gLib.getGestures(Constants.GESTURE_LIBRARY);
            if(tmp == null || tmp.size() == 0) {
                controller.setCurrentMode(PasscodeViewController.Mode.SET);
                return;
            }
            if(match(gesture))
                controller.correctConfirmation();
            else
                controller.incorrectConfirmation();
        }
        else if(controller.currentMode == PasscodeViewController.Mode.LOCK)
        {
            if(match(gesture)) {
                controller.passcodeLockPassed();
            }
            else
                controller.incorrectConfirmation();
        }
    }
    private boolean match(Gesture gesture)
    {
        ArrayList<Prediction> predictions = gLib.recognize(gesture);
        if (predictions != null && predictions.size() > 0) {
            Prediction prediction = predictions.get(0);
            if (prediction.score > 1.0) {
                return true;
            }
        }
        return false;
    }
}
