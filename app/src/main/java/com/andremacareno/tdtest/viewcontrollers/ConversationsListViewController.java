package com.andremacareno.tdtest.viewcontrollers;

import android.os.Looper;
import android.os.Message;
import android.support.v4.util.LongSparseArray;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.OnTaskFailureListener;
import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.LocalConfig;
import com.andremacareno.tdtest.MessageCache;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.fragments.RosterFragment;
import com.andremacareno.tdtest.listadapters.ConversationsAdapter;
import com.andremacareno.tdtest.models.ChannelChatModel;
import com.andremacareno.tdtest.models.ChatModel;
import com.andremacareno.tdtest.models.GroupChatModel;
import com.andremacareno.tdtest.tasks.ProcessConversationsTask;
import com.andremacareno.tdtest.tasks.UpdateTopMessageTask;
import com.andremacareno.tdtest.updatelisteners.TelegramUpdateListener;
import com.andremacareno.tdtest.updatelisteners.UpdateChatProcessor;
import com.andremacareno.tdtest.updatelisteners.UpdateMessageProcessor;
import com.andremacareno.tdtest.updatelisteners.UpdateNotificationsProcessor;
import com.andremacareno.tdtest.updatelisteners.UserActionListener;
import com.andremacareno.tdtest.views.ConversationsListView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import gnu.trove.map.hash.TLongLongHashMap;
import gnu.trove.set.hash.TIntHashSet;
import gnu.trove.set.hash.TLongHashSet;

/**
 * Created by andremacareno on 04/04/15.
 */
public class ConversationsListViewController extends ViewController {
    //statement variables
    private final String TAG = "ConvListController";
    private final AtomicBoolean preventLoading = new AtomicBoolean(false);
    private ConversationsAdapter adapter;
    private int scrollPosition = -1;

    //update handlers
    private PauseHandler listReceiver;
    private PauseHandler newMessageReceiver;
    private PauseHandler newDialogReceiver;      //for updateNewMessage
    private PauseHandler typingHandler;
    private PauseHandler cancelTypingHandler;
    private PauseHandler updateChatOrderHandler;
    private volatile boolean preventChatOpen = false;
    private final AtomicBoolean ignoreUpdateMessage = new AtomicBoolean(true);
    private RosterFragment.RosterFragmentDelegate fragmentDelegate;
    //data model
    private final ArrayList<ChatModel> data = new ArrayList<ChatModel>();
    private final LongSparseArray<ChatModel> idsToChat = new LongSparseArray<ChatModel>();
    private final TLongLongHashMap idsToChatOrders = new TLongLongHashMap();
    private final TLongHashSet topMessageWereRemoved = new TLongHashSet();
    //update observers
    private NotificationObserver emojiLoadObserver, selfDeleteObserver;
    private TelegramUpdateListener globalTypingListener;
    private TelegramUpdateListener globalCancelTypingListener;

    private UpdateMessageProcessor updateMsgProcessor;
    private UpdateChatProcessor updateChatProcessor;
    private UpdateNotificationsProcessor updateNotificationsProcessor;
    private UpdateMessageProcessor.UpdateMessageDelegate bgThreadDelegate, uiThreadDelegate;
    private final TdApi.GetChats getChatsFunc = new TdApi.GetChats(9223372036854775807L, 0, 15);
    private final boolean chooseMode;
    private final boolean groupChatsOnly;
    private final boolean shareableOnly;
    private Client.ResultHandler chatsHandler;

    public ConversationsListViewController(View v, boolean choose, boolean onlyGroups, boolean onlyShareable)
    {
        super(v);
        chooseMode = choose;
        this.groupChatsOnly = onlyGroups;
        this.shareableOnly = onlyShareable;
        init();
    }
    public ConversationsListViewController(View v, boolean choose, boolean onlyGroups)
    {
        super(v);
        chooseMode = choose;
        this.groupChatsOnly = onlyGroups;
        this.shareableOnly = false;
        init();
    }
    public ConversationsListViewController(View v) {
        super(v);
        chooseMode = false;
        this.groupChatsOnly = false;
        this.shareableOnly = false;
        init();
    }
    public void setFragmentDelegate(RosterFragment.RosterFragmentDelegate delegate)
    {
        this.fragmentDelegate = delegate;
    }
    private void init()
    {
        chatsHandler = new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "GetChats response handling");
                TdApi.Chats dialogs = (TdApi.Chats) object;
                synchronized(getChatsFunc)
                {
                    if(dialogs.chats.length > 0)
                    {
                        TdApi.Chat lastChat = dialogs.chats[dialogs.chats.length - 1];
                        getChatsFunc.offsetChatId = lastChat.id;
                        getChatsFunc.offsetOrder = lastChat.order;
                    }
                }
                for(TdApi.Chat ch : dialogs.chats)
                {
                    synchronized(idsToChatOrders)
                    {
                        idsToChatOrders.put(ch.id, ch.order);
                    }
                }
                didConversationsDownloaded(dialogs);
            }
        };
        initHandlers();
    }

    @Override
    protected void onViewReplaced() {
        if(getView() != null) {
            initViewSettings();
        }
    }

    @Override
    public void onFragmentPaused() {
        ConversationsListView v = (ConversationsListView) getView();
        scrollPosition = v.getScrollPosition();
        try
        {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "updateMsgProcessor: pause");
            updateMsgProcessor.performPauseObservers();
            updateChatProcessor.performPauseObservers();
            updateNotificationsProcessor.performPauseObservers();
            listReceiver.pause();
            newMessageReceiver.pause();
            newDialogReceiver.pause();
            typingHandler.pause();
            cancelTypingHandler.pause();
            updateChatOrderHandler.pause();
            synchronized(ignoreUpdateMessage)
            {
                ignoreUpdateMessage.set(false);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFragmentResumed() {
        try
        {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "updateMsgProcessor: resume");
            updateMsgProcessor.performResumeObservers();
            updateChatProcessor.performResumeObservers();
            updateNotificationsProcessor.performResumeObservers();
            listReceiver.resume();
            newMessageReceiver.resume();
            newDialogReceiver.resume();
            typingHandler.resume();
            cancelTypingHandler.resume();
            updateChatOrderHandler.resume();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        if(updateMsgProcessor == null)
            initObservers();
        //NotificationCenter.getInstance().addObserver(didOutMessageSentObserver);
        NotificationCenter.getInstance().addObserver(emojiLoadObserver);
        NotificationCenter.getInstance().addObserver(selfDeleteObserver);
        if(!chooseMode)
        {
            service.addObserver(globalTypingListener);
            service.addObserver(globalCancelTypingListener);
        }
        if(BuildConfig.DEBUG)
            Log.d(TAG, "updateMsgProcessor: attached");
        updateMsgProcessor.attachObservers(service);
        updateChatProcessor.attachObservers(service);
        updateNotificationsProcessor.attachObservers(service);
        continueInit();
    }

    public void removeObservers()
    {
        //NotificationCenter.getInstance().removeObserver(didOutMessageSentObserver);
        NotificationCenter.getInstance().removeObserver(emojiLoadObserver);
        NotificationCenter.getInstance().removeObserver(selfDeleteObserver);
        TelegramUpdatesHandler updateHandler = TelegramApplication.sharedApplication().getUpdateService();
        if(updateHandler != null && !chooseMode)
        {
            updateHandler.removeObserver(globalTypingListener);
            updateHandler.removeObserver(globalCancelTypingListener);
            globalTypingListener = null;
            globalCancelTypingListener = null;
        }
        //didOutMessageSentObserver = null;
        emojiLoadObserver = null;
        if(BuildConfig.DEBUG)
            Log.d(TAG, "updateMsgProcessor: removed");
        updateMsgProcessor.removeObservers();
        updateChatProcessor.removeObservers();
        updateNotificationsProcessor.removeObservers();
        updateMsgProcessor = null;
        updateChatProcessor = null;
        updateNotificationsProcessor = null;
    }

    public ArrayList<ChatModel> getData() { return this.data; }
    private void continueInit()
    {
        this.adapter = new ConversationsAdapter(data, this);
        ConversationsListView v = (ConversationsListView) getView();
        if(v != null) {
            initViewSettings();
        }
    }
    private void initViewSettings()
    {
        ConversationsListView v = (ConversationsListView) getView();
        v.setAdapter(adapter);
        v.setMotionEventSplittingEnabled(false);
        if(scrollPosition != -1)
            v.scrollToPosition(scrollPosition);
    }
    private void initHandlers()
    {
        this.listReceiver = new ListReceiver(this);
        this.newMessageReceiver = new NewMessageReceiver(this);
        this.newDialogReceiver = new NewDialogReceiver(this);
        this.typingHandler = new TypingHandler(this);
        this.cancelTypingHandler = new CancelTypingHandler(this);
        this.updateChatOrderHandler = new UpdateChatOrderReceiver(this);
    }

    private void initObservers()
    {
        initDelegates();
        this.selfDeleteObserver = new NotificationObserver(NotificationCenter.didChatMessageRemovedBySelf) {
            @Override
            public void didNotificationReceived(Notification notification) {
                try
                {
                    final long chatId = (long) notification.getObject();
                    final ChatModel chat;
                    synchronized(idsToChat)
                    {
                        chat = idsToChat.get(chatId);
                        if(chat == null)
                            return;
                    }
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            TdApi.Chat chatObj = ChatCache.getInstance().getChatById(chatId, true);
                            if(chatObj != null)
                            {
                                boolean shouldUpdate = false;
                                synchronized(idsToChat)
                                {
                                    shouldUpdate = idsToChat.get(chatObj.id) != null && idsToChat.get(chatObj.id).getTopMessageId() != chatObj.topMessage.id;
                                }
                                if(shouldUpdate)
                                {
                                    Looper.prepare();
                                    UpdateTopMessageTask task = new UpdateTopMessageTask(chatObj.topMessage.id, chat);
                                    task.onComplete(new OnTaskCompleteListener() {
                                        @Override
                                        public void taskComplete(AsyncTaskManTask task) {
                                            Message msg = Message.obtain(newMessageReceiver);
                                            msg.obj = chatId;
                                            msg.sendToTarget();
                                            try
                                            {
                                                Looper.myLooper().quit();
                                            }
                                            catch(Exception e) { e.printStackTrace(); }
                                        }
                                    });
                                    task.addToQueue();
                                    Looper.loop();
                                }
                            }
                        }
                    }).start();
                }
                catch(Exception ignored) {}
            }
        };
        this.emojiLoadObserver = new NotificationObserver(NotificationCenter.didEmojiLoaded) {
            @Override
            public void didNotificationReceived(Notification notification) {
                if(adapter != null)
                    adapter.notifyDataSetChanged();
            }
        };
        /*this.didOutMessageSentObserver = new NotificationObserver(NotificationCenter.didOutMessageSent) {
            @Override
            public void didNotificationReceived(Notification notification) {
                TdApi.Message msg = (TdApi.Message) notification.getObject();
                synchronized(data)
                {
                    int index = getIndexOfConversationById(msg.chatId);
                    if(index >= 0) {
                        data.get(index).updateTopMessage(msg);
                        Message m = Message.obtain(newMessageReceiver);
                        m.obj = msg.chatId;
                        m.sendToTarget();
                    }
                }
            }
        };*/
        if(!chooseMode)
        {
            this.globalTypingListener = new UserActionListener() {
                @Override
                protected int observableUserActionConstructor() {
                    return TdApi.SendMessageTypingAction.CONSTRUCTOR;
                }

                @Override
                protected void onUpdateReceived(TdApi.UpdateUserAction act) {
                    Message msg = Message.obtain(typingHandler);
                    msg.obj = act.chatId;
                    msg.sendToTarget();
                }
            };
            this.globalCancelTypingListener = new UserActionListener() {
                @Override
                protected int observableUserActionConstructor() {
                    return TdApi.SendMessageCancelAction.CONSTRUCTOR;
                }

                @Override
                protected void onUpdateReceived(TdApi.UpdateUserAction act) {
                    Message msg = Message.obtain(cancelTypingHandler);
                    msg.obj = act.chatId;
                    msg.sendToTarget();
                }
            };
        }
        updateNotificationsProcessor = new UpdateNotificationsProcessor() {
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                return Constants.VIEWCHAT_UPDATE_NOTIFICATIONS_OBSERVER;
            }

            @Override
            protected void processUpdateInBackground(TdApi.Update upd) {
                TdApi.NotificationSettingsScope scope = ((TdApi.UpdateNotificationSettings) upd).scope;
                TdApi.NotificationSettings settings = ((TdApi.UpdateNotificationSettings) upd).notificationSettings;
                if(scope.getConstructor() == TdApi.NotificationSettingsForChat.CONSTRUCTOR)
                {
                    //change for one chat
                    long chatId = ((TdApi.NotificationSettingsForChat) scope).chatId;
                    synchronized(idsToChat)
                    {
                        if(idsToChat.get(chatId) == null) {
                            return;
                        }
                        ChatModel chat = idsToChat.get(chatId);
                        chat.updateMuteFlag(settings);
                    }
                }
            }

            @Override
            protected void processUpdateInUiThread(TdApi.Update upd) {
                TdApi.NotificationSettingsScope scope = ((TdApi.UpdateNotificationSettings) upd).scope;
                TdApi.NotificationSettings settings = ((TdApi.UpdateNotificationSettings) upd).notificationSettings;
                if(scope.getConstructor() == TdApi.NotificationSettingsForAllChats.CONSTRUCTOR) {
                    boolean previousSettings = LocalConfig.getInstance().isChatsMutedByDefault();
                    LocalConfig.getInstance().setChatsMutedByDefault(settings.muteFor > 0);
                    if(previousSettings != (settings.muteFor > 0))
                        adapter.notifyDataSetChanged();
                }
                else if(scope.getConstructor() == TdApi.NotificationSettingsForChat.CONSTRUCTOR)
                {
                    long chatId = ((TdApi.NotificationSettingsForChat) scope).chatId;
                    int index = getIndexOfConversationById(chatId);
                    if(index >= 0)
                        adapter.notifyItemChanged(index);
                }
            }
        };
        updateChatProcessor = new UpdateChatProcessor() {
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                /*if(chooseMode)
                {
                    if(constructor == TdApi.UpdateChatTitle.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_TITLE_OBSERVER.concat("_choose_mode");
                    else if(constructor == TdApi.UpdateChatReadInbox.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_READ_INBOX_OBSERVER.concat("_choose_mode");
                    else if(constructor == TdApi.UpdateChatReadOutbox.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_READ_OUTBOX_OBSERVER.concat("_choose_mode");
                    else if(constructor == TdApi.UpdateChatPhoto.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_PHOTO_OBSERVER.concat("_choose_mode");
                }
                else
                {
                    if(constructor == TdApi.UpdateChatTitle.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_TITLE_OBSERVER;
                    else if(constructor == TdApi.UpdateChatReadInbox.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_READ_INBOX_OBSERVER;
                    else if(constructor == TdApi.UpdateChatReadOutbox.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_READ_OUTBOX_OBSERVER;
                    else if(constructor == TdApi.UpdateChatPhoto.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_PHOTO_OBSERVER;
                }
                return null;*/
                return chooseMode ? String.format("chatlist_observer_constructor%s_choose_mode", constructor) : String.format("chatlist_observer_constructor%s", constructor);
            }

            @Override
            protected boolean shouldObserveUpdateGroup() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateTitle() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateReplyMarkup() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateReadInbox() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateReadOutbox() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateChat() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateChatPhoto() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateChatOrder() {
                return true;
            }

            @Override
            protected void processUpdateInBackground(TdApi.Update upd) {
                if(upd.getConstructor() == TdApi.UpdateChatReadOutbox.CONSTRUCTOR)
                {
                    synchronized(idsToChat)
                    {
                        if(idsToChat.get(((TdApi.UpdateChatReadOutbox) upd).chatId) == null) {
                            return;
                        }
                        ChatModel chat = idsToChat.get(((TdApi.UpdateChatReadOutbox) upd).chatId);
                        chat.updateChatLastReadOutboxMessageId(((TdApi.UpdateChatReadOutbox) upd).lastReadOutboxMessageId);
                    }
                }
                else if(upd.getConstructor() == TdApi.UpdateChatReadInbox.CONSTRUCTOR)
                {
                    synchronized(idsToChat)
                    {
                        if(idsToChat.get(((TdApi.UpdateChatReadInbox) upd).chatId) == null) {
                            return;
                        }
                        ChatModel chat = idsToChat.get(((TdApi.UpdateChatReadInbox) upd).chatId);
                        chat.updateChatLastReadInboxMessageId(((TdApi.UpdateChatReadInbox) upd).lastReadInboxMessageId);
                        chat.updateUnreadCount(((TdApi.UpdateChatReadInbox) upd).unreadCount);
                    }
                }
                else if(upd.getConstructor() == TdApi.UpdateChatTitle.CONSTRUCTOR)
                {
                    synchronized(idsToChat)
                    {
                        if(idsToChat.get(((TdApi.UpdateChatTitle) upd).chatId) == null)
                            return;
                        ChatModel chat = idsToChat.get(((TdApi.UpdateChatTitle) upd).chatId);
                        if(chat.isGroupChat())
                            ((GroupChatModel) chat).updateTitle(((TdApi.UpdateChatTitle) upd).title);
                        else if(chat.isChannel() || chat.isSupergroup())
                            ((ChannelChatModel) chat).updateTitle(((TdApi.UpdateChatTitle) upd).title);
                    }
                }
                else if(upd.getConstructor() == TdApi.UpdateChatPhoto.CONSTRUCTOR)
                {
                    synchronized(idsToChat)
                    {
                        if(idsToChat.get(((TdApi.UpdateChatPhoto) upd).chatId) == null)
                            return;
                        ChatModel chat = idsToChat.get(((TdApi.UpdateChatPhoto) upd).chatId);
                        chat.updatePhoto((TdApi.UpdateChatPhoto) upd);
                    }
                }
                else if(upd.getConstructor() == TdApi.UpdateChatOrder.CONSTRUCTOR)
                {
                    synchronized(getChatsFunc)
                    {
                        if(getChatsFunc.offsetChatId == ((TdApi.UpdateChatOrder) upd).chatId)
                            getChatsFunc.offsetOrder = ((TdApi.UpdateChatOrder) upd).order;
                    }
                    onNewChatOrder(((TdApi.UpdateChatOrder) upd).chatId, ((TdApi.UpdateChatOrder) upd).order);
                }
                else if(upd.getConstructor() == TdApi.UpdateChatReplyMarkup.CONSTRUCTOR)
                {
                    TdApi.UpdateChatReplyMarkup cast = (TdApi.UpdateChatReplyMarkup) upd;
                    synchronized(idsToChat)
                    {
                        if(idsToChat.get(cast.chatId) != null)
                        {
                            idsToChat.get(cast.chatId).updateReplyMarkupMsgId(cast.replyMarkupMessageId);
                            MessageCache.getInstance().cacheMessage(cast.chatId, cast.replyMarkupMessageId);
                        }
                    }
                }
            }

            @Override
            protected void processUpdateInUiThread(TdApi.Update upd) {
                if (upd.getConstructor() == TdApi.UpdateChatReadOutbox.CONSTRUCTOR)
                {
                    int index = getIndexOfConversationById(((TdApi.UpdateChatReadOutbox) upd).chatId);
                    if(index >= 0) {
                        Log.d("ChatList", "updating read outbox (ui thread)");
                        adapter.notifyItemChanged(index);
                    }
                }
                else if(upd.getConstructor() == TdApi.UpdateChatReadInbox.CONSTRUCTOR)
                {
                    int index = getIndexOfConversationById(((TdApi.UpdateChatReadInbox) upd).chatId);
                    if(index >= 0) {
                        Log.d("ChatController", "updateChatReadInbox (ui thread)");
                        adapter.notifyItemChanged(index);
                    }
                }
                else if(upd.getConstructor() == TdApi.UpdateChatTitle.CONSTRUCTOR)
                {
                    int index = getIndexOfConversationById(((TdApi.UpdateChatTitle) upd).chatId);
                    if(index >= 0)
                        adapter.notifyItemChanged(index);
                }
                else if(upd.getConstructor() == TdApi.UpdateChatPhoto.CONSTRUCTOR)
                {
                    int index = getIndexOfConversationById(((TdApi.UpdateChatPhoto) upd).chatId);
                    if(index >= 0)
                        adapter.notifyItemChanged(index);
                }
            }
        };
        updateMsgProcessor = new UpdateMessageProcessor(bgThreadDelegate, uiThreadDelegate) {
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                if(chooseMode)
                {
                    if(constructor == TdApi.UpdateMessageId.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_MESSAGE_ID_OBSERVER.concat("_choose_mode");
                    else if(constructor == TdApi.UpdateMessageContent.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_MESSAGE_CONTENT_OBSERVER.concat("_choose_mode");
                    else if(constructor == TdApi.UpdateMessageDate.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_MESSAGE_DATE_OBSERVER.concat("_choose_mode");
                    else if(constructor == TdApi.UpdateMessageSendFailed.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_MESSAGE_SEND_FAIL_OBSERVER.concat("_choose_mode");
                    else if(constructor == TdApi.UpdateDeleteMessages.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_MESSAGE_DEL_OBSERVER.concat("_choose_mode");
                    else if(constructor == TdApi.UpdateNewMessage.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_NEW_MSG_OBSERVER.concat("_choose_mode");
                }
                else
                {
                    if(constructor == TdApi.UpdateMessageId.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_MESSAGE_ID_OBSERVER;
                    else if(constructor == TdApi.UpdateMessageContent.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_MESSAGE_CONTENT_OBSERVER;
                    else if(constructor == TdApi.UpdateMessageDate.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_MESSAGE_DATE_OBSERVER;
                    else if(constructor == TdApi.UpdateMessageSendFailed.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_MESSAGE_SEND_FAIL_OBSERVER;
                    else if(constructor == TdApi.UpdateDeleteMessages.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_MESSAGE_DEL_OBSERVER;
                    else if(constructor == TdApi.UpdateNewMessage.CONSTRUCTOR)
                        return Constants.CHATLIST_UPDATE_NEW_MSG_OBSERVER;
                }
                return null;
            }

            @Override
            protected boolean shouldObserveUpdateMsgContent() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateMsgDate() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateMsgId() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateMsgSendFailed() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateNewMsg() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateDeleteMessages() {
                return true;
            }

            @Override
            protected boolean shouldObserveViewsCount() {
                return false;
            }
        };
    }
    private void initDelegates()
    {
        bgThreadDelegate = new UpdateMessageProcessor.UpdateMessageDelegate() {
            @Override
            public void updateMessageContent(TdApi.UpdateMessageContent upd) {
                synchronized(ignoreUpdateMessage)
                {
                    if(ignoreUpdateMessage.get())
                        return;
                }
                synchronized(data) {
                    synchronized(idsToChat)
                    {
                        ChatModel chat = idsToChat.get(upd.chatId);
                        if(chat != null)
                        {
                            if(chat.getTopMessageId() == upd.messageId)
                                chat.updateTopMessageContent(upd.newContent);
                        }
                    }
                }
            }

            @Override
            public void updateMessageDate(TdApi.UpdateMessageDate upd) {
                synchronized(ignoreUpdateMessage)
                {
                    if(ignoreUpdateMessage.get())
                        return;
                }
                synchronized(data)
                {
                    synchronized(idsToChat)
                    {
                        ChatModel chat = idsToChat.get(upd.chatId);
                        if(chat != null)
                        {
                            if(chat.getTopMessageId() == upd.messageId)
                                chat.updateTopMessageDate(upd.newDate);
                        }
                    }
                }
            }

            @Override
            public void updateMessageId(final TdApi.UpdateMessageId upd) {
                synchronized(ignoreUpdateMessage)
                {
                    if(ignoreUpdateMessage.get())
                        return;
                }
                ChatModel chat;
                synchronized(idsToChat) {
                    chat = idsToChat.get(upd.chatId);
                }
                if(chat != null) {
                    if(chat.getTopMessageId() == upd.oldId)
                        chat.updateTopMessageId(upd.newId);
                    else if(chat.getTopMessageId() < upd.newId || chat.getTopMessageId() > 1000000000)
                    {
                        final ChatModel model = chat;
                        UpdateTopMessageTask task = new UpdateTopMessageTask(upd.newId, model);
                        task.onComplete(new OnTaskCompleteListener() {
                            @Override
                            public void taskComplete(AsyncTaskManTask task) {
                                Message msg = Message.obtain(newMessageReceiver);
                                msg.obj = upd.chatId;
                                msg.sendToTarget();
                            }
                        });
                        task.addToQueue();
                    }
                }
            }

            @Override
            public void updateMessageSendFailed(TdApi.UpdateMessageSendFailed upd) {
                synchronized(ignoreUpdateMessage)
                {
                    if(ignoreUpdateMessage.get())
                        return;
                }
                synchronized(data)
                {
                    synchronized(idsToChat)
                    {
                        ChatModel chat = idsToChat.get(upd.chatId);
                        if(chat != null)
                        {
                            if(chat.getTopMessageId() == upd.messageId)
                                chat.updateTopMessageDate(0);
                        }
                    }
                }
            }

            @Override
            public void updateNewMessage(final TdApi.UpdateNewMessage upd) {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "receiving updateNewMessage in background thread");
            }

            @Override
            public void updateDelMessages(TdApi.UpdateDeleteMessages upd) {
                int topMsg;
                synchronized(idsToChat)
                {
                    if(idsToChat.get(upd.chatId) == null)
                        return;
                    topMsg = idsToChat.get(upd.chatId).getTopMessageId();
                }
                TIntHashSet removedMsgs = new TIntHashSet(upd.messageIds);
                if(removedMsgs.contains(topMsg))
                    topMessageWereRemoved.add(upd.chatId);
            }

            @Override
            public void updateViewsCount(TdApi.UpdateMessageViews upd) {

            }
        };
        uiThreadDelegate = new UpdateMessageProcessor.UpdateMessageDelegate() {
            @Override
            public void updateMessageContent(TdApi.UpdateMessageContent upd) {
                synchronized(ignoreUpdateMessage)
                {
                    if(ignoreUpdateMessage.get())
                        return;
                }
                int index = getIndexOfConversationById(upd.chatId);
                if(index >= 0)
                    adapter.notifyItemChanged(index);
            }

            @Override
            public void updateMessageDate(TdApi.UpdateMessageDate upd) {
                synchronized(ignoreUpdateMessage)
                {
                    if(ignoreUpdateMessage.get())
                        return;
                }
                int index = getIndexOfConversationById(upd.chatId);
                if(index >= 0)
                    adapter.notifyItemChanged(index);
            }

            @Override
            public void updateMessageId(final TdApi.UpdateMessageId upd) {
                synchronized(ignoreUpdateMessage)
                {
                    if(ignoreUpdateMessage.get())
                        return;
                }
                int index = getIndexOfConversationById(upd.chatId);
                if(index >= 0)
                    adapter.notifyItemChanged(index);
                else
                {
                    TdApi.Chat chat = ChatCache.getInstance().getChatById(upd.chatId);
                    if(chat == null)
                        return;
                    synchronized(idsToChatOrders)
                    {
                        idsToChatOrders.put(chat.id, chat.order);
                    }
                    ProcessConversationsTask t = new ProcessConversationsTask(chat, groupChatsOnly, shareableOnly);
                    t.onComplete(new OnTaskCompleteListener() {
                        @Override
                        public void taskComplete(AsyncTaskManTask task) {
                            try
                            {
                                List<ChatModel> result = ((ProcessConversationsTask) task).getChats();
                                ChatModel m = result.get(0);
                                synchronized(data)
                                {
                                    synchronized(idsToChat)
                                    {
                                        if(idsToChat.get(m.getId()) != null && (idsToChat.get(m.getId()).getTopMessageId() < m.getTopMessageId() && idsToChat.get(m.getId()).getTopMessageId() < 1000000000))
                                        {
                                            return;
                                        }
                                        else if(idsToChat.get(m.getId()) == null)
                                        {
                                            data.add(0, m);
                                            idsToChat.put(m.getId(), m);
                                            Message msg = Message.obtain(newDialogReceiver);
                                            msg.obj = m.getId();
                                            msg.sendToTarget();
                                        }
                                    }
                                }
                            }
                            catch(Exception e) { e.printStackTrace(); }
                        }
                    });
                    t.addToQueue();
                }
            }

            @Override
            public void updateMessageSendFailed(TdApi.UpdateMessageSendFailed upd) {
                synchronized(ignoreUpdateMessage)
                {
                    if(ignoreUpdateMessage.get())
                        return;
                }
                int index = getIndexOfConversationById(upd.chatId);
                if(index >= 0)
                    adapter.notifyItemChanged(index);
            }

            @Override
            public void updateNewMessage(final TdApi.UpdateNewMessage upd) {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "receiving updateNewMessage in UI thread");
                synchronized(ignoreUpdateMessage)
                {
                    if(ignoreUpdateMessage.get())
                        return;
                }
                try
                {
                    boolean loadChat;
                    synchronized(idsToChat) {
                        loadChat = idsToChat.get(upd.message.chatId) == null;
                    }
                    if(loadChat)
                    {
                        TdApi.Chat chat = ChatCache.getInstance().getChatById(upd.message.chatId);
                        if(chat == null)
                            return;
                        synchronized(idsToChatOrders)
                        {
                            idsToChatOrders.put(chat.id, chat.order);
                        }
                        ProcessConversationsTask t = new ProcessConversationsTask(chat, groupChatsOnly, shareableOnly);
                        t.onComplete(new OnTaskCompleteListener() {
                            @Override
                            public void taskComplete(AsyncTaskManTask task) {
                                try
                                {
                                    List<ChatModel> result = ((ProcessConversationsTask) task).getChats();
                                    ChatModel m = result.get(0);
                                    synchronized(data)
                                    {
                                        synchronized(idsToChat)
                                        {
                                            if(idsToChat.get(m.getId()) != null && (idsToChat.get(m.getId()).getTopMessageId() < m.getTopMessageId() && idsToChat.get(m.getId()).getTopMessageId() < 1000000000))
                                            {
                                                final ChatModel model = idsToChat.get(upd.message.chatId);
                                                boolean shouldUpdateInBackground = CommonTools.needToProcessMessageInBackground(upd.message, model.isGroupChat() || model.isSupergroup());
                                                if(shouldUpdateInBackground)
                                                {
                                                    UpdateTopMessageTask updateTopMsgTask = new UpdateTopMessageTask(upd.message, model);
                                                    updateTopMsgTask.onComplete(new OnTaskCompleteListener() {
                                                        @Override
                                                        public void taskComplete(AsyncTaskManTask aTask) {
                                                            synchronized(data)
                                                            {
                                                                if(BuildConfig.DEBUG)
                                                                    Log.d(TAG, "UpdateTopMessageTask finished, proceeding to newMessageReceiver");
                                                                Message msg = Message.obtain(newMessageReceiver);
                                                                msg.obj = upd.message.chatId;
                                                                msg.sendToTarget();
                                                            }
                                                        }
                                                    });
                                                    updateTopMsgTask.addToQueue();

                                                }
                                                else {
                                                    synchronized(data)
                                                    {
                                                        model.updateTopMessage(upd.message);
                                                        Message msg = Message.obtain(newMessageReceiver);
                                                        msg.obj = upd.message.chatId;
                                                        msg.sendToTarget();
                                                    }
                                                }
                                            }
                                            else if(idsToChat.get(m.getId()) == null)
                                            {
                                                data.add(0, m);
                                                idsToChat.put(m.getId(), m);
                                                Message msg = Message.obtain(newDialogReceiver);
                                                msg.obj = m.getId();
                                                msg.sendToTarget();
                                            }
                                        }
                                    }
                                }
                                catch(Exception e) { e.printStackTrace(); }
                            }
                        });
                        t.addToQueue();
                    }
                    else
                    {
                        final ChatModel model = idsToChat.get(upd.message.chatId);
                        if(model == null)
                            return;
                        boolean shouldUpdateInBackground = CommonTools.needToProcessMessageInBackground(upd.message, model.isGroupChat() || model.isSupergroup());
                        if(shouldUpdateInBackground)
                        {
                            UpdateTopMessageTask task = new UpdateTopMessageTask(upd.message, model);
                            task.onComplete(new OnTaskCompleteListener() {
                                @Override
                                public void taskComplete(AsyncTaskManTask task) {
                                    synchronized(data)
                                    {
                                        if(BuildConfig.DEBUG)
                                            Log.d(TAG, "UpdateTopMessageTask finished, proceeding to newMessageReceiver");
                                        Message msg = Message.obtain(newMessageReceiver);
                                        msg.obj = upd.message.chatId;
                                        msg.sendToTarget();
                                    }
                                }
                            });
                            task.addToQueue();

                        }
                        else {
                            synchronized(data)
                            {
                                model.updateTopMessage(upd.message);
                                Message msg = Message.obtain(newMessageReceiver);
                                msg.obj = upd.message.chatId;
                                msg.sendToTarget();
                            }
                        }
                    }
                }
                catch(Exception e) { e.printStackTrace(); }
            }

            @Override
            public void updateDelMessages(final TdApi.UpdateDeleteMessages upd) {
                if(topMessageWereRemoved.contains(upd.chatId))
                    topMessageWereRemoved.remove(upd.chatId);
                else
                    return;
                final ChatModel chat;
                synchronized(idsToChat)
                {
                    chat = idsToChat.get(upd.chatId);
                    if(chat == null)
                        return;
                }
                TdApi.Chat chatObj = ChatCache.getInstance().getChatById(upd.chatId, true);
                if(chatObj != null)
                {
                    UpdateTopMessageTask task = new UpdateTopMessageTask(chatObj.topMessage.id, chat);
                    task.onComplete(new OnTaskCompleteListener() {
                        @Override
                        public void taskComplete(AsyncTaskManTask task) {
                            Message msg = Message.obtain(newMessageReceiver);
                            msg.obj = upd.chatId;
                            msg.sendToTarget();
                        }
                    });
                    task.addToQueue();
                }
            }

            @Override
            public void updateViewsCount(TdApi.UpdateMessageViews upd) {

            }
        };
    }

    public void loadMoreConversations()
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "loadMoreConversations() called");
        synchronized(preventLoading)
        {
            if(preventLoading.get()) {
                return;
            }
            preventLoading.set(true);

        }
        synchronized(ignoreUpdateMessage)
        {
            ignoreUpdateMessage.set(true);
        }
        synchronized(getChatsFunc)
        {
            TG.getClientInstance().send(getChatsFunc, chatsHandler);
        }
    }

    private void didConversationsDownloaded(final TdApi.Chats dialogs) {
        //convert to models
        if(BuildConfig.DEBUG)
            Log.d(TAG, "didConversationsDownloaded: ".concat(dialogs.toString()));
        ProcessConversationsTask t = new ProcessConversationsTask(dialogs, groupChatsOnly, shareableOnly);
        t.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(AsyncTaskManTask task) {
                try {
                    if(BuildConfig.DEBUG)
                        Log.d(TAG, "converted");
                    List<ChatModel> result = ((ProcessConversationsTask) task).getChats();
                    Message msg = Message.obtain(listReceiver);
                    msg.obj = result;
                    msg.sendToTarget();
                } catch (Exception e) {
                    synchronized(ignoreUpdateMessage)
                    {
                        ignoreUpdateMessage.set(false);
                    }
                    if(dialogs.chats.length > 0)
                        synchronized(preventLoading)
                        {
                            preventLoading.set(false);
                        }
                }
            }
        });
        t.onFailure(new OnTaskFailureListener() {
            @Override
            public void taskFailed(AsyncTaskManTask task) {
                synchronized(ignoreUpdateMessage)
                {
                    ignoreUpdateMessage.set(false);
                }
                if(dialogs.chats.length > 0)
                    synchronized(preventLoading)
                    {
                        preventLoading.set(false);
                    }
            }
        });
        t.addToQueue();
    }
    private void addItemsToList(ArrayList<ChatModel> dialogs)
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "addItemsToList");
        int chatsAdded = 0;
        int position;
        synchronized(data)
        {
            synchronized(idsToChat)
            {
                position = data.size();
                for (ChatModel obj : dialogs) {
                    if(idsToChat.get(obj.getId()) != null)
                        continue;
                    idsToChat.put(obj.getId(), obj);
                    data.add(obj);
                    chatsAdded++;
                }
            }
        }
        adapter.notifyItemRangeInserted(position, chatsAdded);
        synchronized(ignoreUpdateMessage)
        {
            ignoreUpdateMessage.set(false);
        }
        if(dialogs.size() > 0) {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "loading next");
            synchronized(preventLoading)
            {
                preventLoading.set(false);
            }
        }
    }

    public int getIndexOfConversationById(long id)
    {
        synchronized(data)
        {
            synchronized(idsToChat)
            {
                if(idsToChat.get(id) == null)
                    return -1;
                return data.indexOf(idsToChat.get(id));
            }
        }
    }

    public void didItemClicked(int position)
    {
        if(preventChatOpen || fragmentDelegate == null)
            return;
        if(position < 0)
            return;
        ChatModel model;
        synchronized(data)
        {
            model = data.get(position);
        }
        if(model == null || model.isUnknownPeer())
            return;
        preventChatOpen = true;
        fragmentDelegate.didChatSelected(model);
        preventChatOpen = false;
    }
    private void didStartedTyping(long dialogId)
    {
        //NOTE: UI thread
        adapter.setTyping(dialogId);
        synchronized(idsToChat)
        {
            if(idsToChat.get(dialogId) == null)
                return;
            ChatModel m = idsToChat.get(dialogId);
            adapter.notifyItemChanged(data.indexOf(m));
        }
    }
    private void didStoppedTyping(long dialogId)
    {
        //NOTE: UI thread
        adapter.unsetTyping(dialogId);
        synchronized(idsToChat)
        {
            if(idsToChat.get(dialogId) == null)
                return;
            ChatModel m = idsToChat.get(dialogId);
            adapter.notifyItemChanged(data.indexOf(m));
        }
    }
    private void onNewChatOrder(long chatId, long newChatOrder)
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "onNewChatOrder");
        synchronized(idsToChatOrders)
        {
            if(!idsToChatOrders.containsKey(chatId)) {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "no info about this chat");
                return; //new chat that will be added in updateNewMessage
            }
            long oldChatOrder = idsToChatOrders.get(chatId);
            final int oldIndex = getIndexOfConversationById(chatId);
            if(oldIndex < 0)
                return;
            int newIndex = 0;
            if(BuildConfig.DEBUG)
                Log.d(TAG, String.format("oldChatOrder = %d; newChatOrder = %d", oldChatOrder, newChatOrder));
            if(oldChatOrder < newChatOrder)
            {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "move chat above");
                //move above
                synchronized (data)
                {
                    final ChatModel updatingChat = data.get(oldIndex);
                    for(final ChatModel ch : data)
                    {
                        long currentChatOrder = idsToChatOrders.containsKey(ch.getId()) ? idsToChatOrders.get(ch.getId()) : 0;
                        if(newChatOrder >= currentChatOrder)
                        {
                            if(newChatOrder == currentChatOrder && chatId > ch.getId())
                                newIndex++;
                            try
                            {
                                if(BuildConfig.DEBUG)
                                    Log.d(TAG, String.format("moving chat: oldIndex = %d; newIndex = %d", oldIndex, newIndex));
                                if(newIndex != oldIndex && updateChatOrderHandler != null)
                                {
                                    updatingChat.updateChatOrder(newChatOrder);
                                    Message msg = Message.obtain(updateChatOrderHandler);
                                    msg.what = newIndex;
                                    msg.obj = updatingChat;
                                    msg.sendToTarget();
                                    /*TelegramApplication.applicationHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            synchronized(data)
                                            {
                                                if(updatingChat.equals(data.get(oldIndex)))
                                                {
                                                    data.remove(oldIndex);
                                                    data.add(finalIndex, updatingChat);
                                                    adapter.notifyItemMoved(oldIndex, finalIndex);
                                                    View list = getView();
                                                    if(list != null) {
                                                        int firstVisible = ((LinearLayoutManager) ((ConversationsListView) list).getLayoutManager()).findFirstVisibleItemPosition();
                                                        int lastVisible = ((LinearLayoutManager) ((ConversationsListView) list).getLayoutManager()).findLastVisibleItemPosition();
                                                        if(firstVisible != RecyclerView.NO_POSITION && lastVisible != RecyclerView.NO_POSITION && finalIndex >= firstVisible && finalIndex <= lastVisible)
                                                            ((LinearLayoutManager) ((ConversationsListView) list).getLayoutManager()).scrollToPosition(finalIndex);
                                                    }
                                                }
                                            }
                                        }
                                    });*/
                                }
                            }
                            catch(Exception ignored) {}
                            break;
                        }
                        else
                            newIndex++;
                    }
                }
            }
            else if(oldChatOrder > newChatOrder)
            {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "move chat below");
                synchronized(data)
                {
                    final ChatModel updatingChat = data.get(oldIndex);
                    for(newIndex = oldIndex + 1; newIndex < data.size(); newIndex++)
                    {
                        final ChatModel ch = data.get(newIndex);
                        long currentChatOrder = idsToChatOrders.containsKey(ch.getId()) ? idsToChatOrders.get(ch.getId()) : 0;
                        if(newChatOrder >= currentChatOrder)
                        {
                            if(newChatOrder == currentChatOrder && chatId < ch.getId())
                                newIndex++;
                            try
                            {
                                if(BuildConfig.DEBUG)
                                    Log.d(TAG, String.format("moving chat: oldIndex = %d; newIndex = %d", oldIndex, newIndex));
                                if(newIndex != oldIndex && updateChatOrderHandler != null)
                                {
                                    Message msg = Message.obtain(updateChatOrderHandler);
                                    msg.what = newIndex;
                                    msg.obj = updatingChat;
                                    msg.sendToTarget();
                                }
                            }
                            catch(Exception ignored) {}
                            break;
                        }
                    }
                }
            }
            else
               idsToChatOrders.put(chatId, newChatOrder);
        }
    }

    static abstract class ConversationsListViewHandler extends PauseHandler
    {
        protected WeakReference<ConversationsListViewController> controllerRef;
        public ConversationsListViewHandler(ConversationsListViewController controller)
        {
            this.controllerRef = new WeakReference<ConversationsListViewController>(controller);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }
    }
    static class ListReceiver extends ConversationsListViewHandler
    {

        public ListReceiver(ConversationsListViewController controller) {
            super(controller);
        }

        @Override
        protected void processMessage(Message message) {
            if(BuildConfig.DEBUG)
                Log.d("ListReceiver", "processing message");
            if(controllerRef.get() != null)
                controllerRef.get().addItemsToList((ArrayList<ChatModel>)message.obj);
        }
    }
    static class NewMessageReceiver extends ConversationsListViewHandler
    {

        public NewMessageReceiver(ConversationsListViewController controller) {
            super(controller);
        }

        @Override
        protected void processMessage(Message message) {
            if(BuildConfig.DEBUG)
                Log.d("NewMessageReceiver", "processing message");
            if(controllerRef.get() != null)
            {
                if(BuildConfig.DEBUG)
                    Log.d("NewMessageReceiver", "controllerRef are not null");
                long chatId = (long) message.obj;
                int index = controllerRef.get().getIndexOfConversationById(chatId);
                if(index < 0)
                    return;
                if(index == 0) {
                    controllerRef.get().adapter.notifyItemChanged(0);
                    return;
                }
                /*ChatModel chat = controllerRef.get().data.remove(index);
                controllerRef.get().data.add(0, chat);
                controllerRef.get().adapter.notifyItemMoved(index, 0);*/
                controllerRef.get().adapter.notifyItemChanged(index);
                View list = controllerRef.get().getView();
                if(list != null) {
                    int firstVisible = ((LinearLayoutManager) ((ConversationsListView) list).getLayoutManager()).findFirstVisibleItemPosition();
                    int lastVisible = ((LinearLayoutManager) ((ConversationsListView) list).getLayoutManager()).findLastVisibleItemPosition();
                    if(firstVisible != RecyclerView.NO_POSITION && lastVisible != RecyclerView.NO_POSITION && index >= firstVisible && index <= lastVisible)
                        ((LinearLayoutManager) ((ConversationsListView) list).getLayoutManager()).scrollToPosition(0);
                }
            }
        }
    }
    static class NewDialogReceiver extends ConversationsListViewHandler
    {

        public NewDialogReceiver(ConversationsListViewController controller) {
            super(controller);
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef.get() != null)
            {
                long dl = (long) message.obj;
                int index = controllerRef.get().getIndexOfConversationById(dl);
                if(index >= 0)
                    controllerRef.get().adapter.notifyItemInserted(index);
            }
        }
    }
    static class UpdateChatOrderReceiver extends ConversationsListViewHandler
    {

        public UpdateChatOrderReceiver(ConversationsListViewController controller) {
            super(controller);
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef.get() != null)
            {
                synchronized(controllerRef.get().idsToChatOrders)
                {
                    try
                    {
                        ChatModel updatingChat = (ChatModel) message.obj;
                        synchronized(controllerRef.get().data)
                        {
                            controllerRef.get().idsToChatOrders.put(updatingChat.getId(), updatingChat.getChatOrder());
                        }
                        int newIndex = message.what;
                        int oldIndex = controllerRef.get().getIndexOfConversationById(updatingChat.getId());
                        controllerRef.get().data.remove(oldIndex);
                        controllerRef.get().data.add(newIndex, updatingChat);
                        controllerRef.get().adapter.notifyItemMoved(oldIndex, newIndex);
                        View list = controllerRef.get().getView();
                        if(list != null && oldIndex < newIndex) {
                            int firstVisible = ((LinearLayoutManager) ((ConversationsListView) list).getLayoutManager()).findFirstVisibleItemPosition();
                            int lastVisible = ((LinearLayoutManager) ((ConversationsListView) list).getLayoutManager()).findLastVisibleItemPosition();
                            if(firstVisible != RecyclerView.NO_POSITION && lastVisible != RecyclerView.NO_POSITION && newIndex >= firstVisible && newIndex <= lastVisible && firstVisible < 10)
                                ((LinearLayoutManager) ((ConversationsListView) list).getLayoutManager()).scrollToPosition(newIndex);
                        }
                    }
                    catch(Exception ignored) {}
                }
            }
        }
    }
    static class TypingHandler extends ConversationsListViewHandler
    {

        public TypingHandler(ConversationsListViewController controller) {
            super(controller);
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef.get() != null)
            {
                long dialog = (Long) message.obj;
                controllerRef.get().didStartedTyping(dialog);
            }
        }
    }
    static class CancelTypingHandler extends ConversationsListViewHandler
    {

        public CancelTypingHandler(ConversationsListViewController controller) {
            super(controller);
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef.get() != null)
            {
                long dialog = (Long) message.obj;
                controllerRef.get().didStoppedTyping(dialog);
            }
        }
    }
}
