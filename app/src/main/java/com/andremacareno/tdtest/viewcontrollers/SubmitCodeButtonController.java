package com.andremacareno.tdtest.viewcontrollers;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.andremacareno.tdtest.auth.AuthStateProcessor;
import com.andremacareno.tdtest.utils.AndroidUtilities;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 03/04/15.
 */
public class SubmitCodeButtonController {
    private EditText smsCodeEditText;
    private TextView wrongCode;
    private final String TAG = "CodeButtonController";
    private AuthStateProcessor.SecondaryAuthDelegate secondaryAuthDelegate;
    public SubmitCodeButtonController(EditText smsCodeEditText, TextView wrongCodeTextView)
    {
        this.smsCodeEditText = smsCodeEditText;
        this.wrongCode = wrongCodeTextView;
        this.secondaryAuthDelegate = new AuthStateProcessor.SecondaryAuthDelegate() {
            @Override
            public String getPhoneNumber() {
                return null;
            }

            @Override
            public void processError() {
                wrongCode.setVisibility(View.VISIBLE);
            }
        };
    }
    public void didEnterCodeButtonClicked() {
        if(smsCodeEditText == null)
            return;
        String enteredCode = smsCodeEditText.getText().toString();
        if(enteredCode.length() == 0)
            return;
        TdApi.TLFunction setCodeFunc = new TdApi.CheckAuthCode(enteredCode);
        AuthStateProcessor.getInstance().changeAuthState(setCodeFunc);
        if(AndroidUtilities.isKeyboardShown(smsCodeEditText))
            AndroidUtilities.hideKeyboard(smsCodeEditText);
    }
    public void rebindToViews(EditText smsCodeEditText, TextView wrongCodeTextView)
    {
        this.smsCodeEditText = smsCodeEditText;
        this.wrongCode = wrongCodeTextView;
    }
    public AuthStateProcessor.SecondaryAuthDelegate getSecondaryAuthDelegate()
    {
        return this.secondaryAuthDelegate;
    }
}
