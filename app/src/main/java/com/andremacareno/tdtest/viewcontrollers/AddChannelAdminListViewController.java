package com.andremacareno.tdtest.viewcontrollers;

import android.app.Activity;
import android.view.View;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.TelegramMainActivity;
import com.andremacareno.tdtest.models.UserModel;

import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 10/08/15.
 */
public class AddChannelAdminListViewController extends ContactsListViewController {
    private WeakReference<Activity> activityRef;
    public AddChannelAdminListViewController(View v, int[] ignoreIds) {
        super(v, ignoreIds);
    }

    @Override
    public void didItemClicked(int position)
    {
        UserModel u = data.get(position);
        final int userId = u.getId();
        Activity act = activityRef != null && activityRef.get() != null ? activityRef.get() : null;
        if(act != null)
        {
            NotificationCenter.getInstance().postNotificationDelayed(NotificationCenter.didNewChannelMemberRequested, userId, 200);
            try
            {
                ((TelegramMainActivity) act).performGoBack();
            }
            catch(Exception ignored) {}
        }
    }
    public void setActivityRef(Activity act)
    {
        this.activityRef = new WeakReference<Activity>(act);
    }
}
