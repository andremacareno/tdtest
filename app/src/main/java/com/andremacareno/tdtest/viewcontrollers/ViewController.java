package com.andremacareno.tdtest.viewcontrollers;

import android.view.View;

import com.andremacareno.tdtest.TelegramUpdatesHandler;

/**
 * Created by andremacareno on 02/04/15.
 */
public abstract class ViewController {
    private View view;
    public ViewController(View v)
    {
        this.view = v;
    }
    protected View getView() { return this.view; }
    public void replaceView(View v) {
        this.view = v;
        onViewReplaced();
    }
    protected void onViewReplaced()
    {

    }
    public abstract void onFragmentPaused();
    public abstract void onFragmentResumed();
    public abstract void onUpdateServiceConnected(TelegramUpdatesHandler service);
}
