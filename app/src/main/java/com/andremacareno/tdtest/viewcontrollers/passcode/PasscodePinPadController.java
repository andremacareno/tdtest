package com.andremacareno.tdtest.viewcontrollers.passcode;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.views.PasscodePinPad;

/**
 * Created by andremacareno on 02/08/15.
 */
public class PasscodePinPadController {
    private String pin = "";
    private String pinConfirmation = "";
    private PasscodePinController controller;
    public PasscodePinPadController(PasscodePinController controllerRef)
    {
        this.controller = controllerRef;
    }

    public void didPinPadButtonPressed(PasscodePinPad.PinPadButton button)
    {
        if(button == PasscodePinPad.PinPadButton.BACKSPACE)
        {
            if(controller.getCurrentMode() == PasscodeViewController.Mode.SET || controller.getCurrentMode() == PasscodeViewController.Mode.LOCK) {
                if(pin.length() == 0)
                    return;
                pin = pin.substring(0, pin.length() - 1);
            }
            else if(controller.getCurrentMode() == PasscodeViewController.Mode.CONFIRMATION) {
                if(pinConfirmation.length() == 0)
                    return;
                pinConfirmation = pin.substring(0, pinConfirmation.length() - 1);
            }
            controller.removeLetter();
            return;
        }
        String btn;
        if(button == PasscodePinPad.PinPadButton.ONE)
            btn = "1";
        else if(button == PasscodePinPad.PinPadButton.TWO)
            btn = "2";
        else if(button == PasscodePinPad.PinPadButton.THREE)
            btn = "3";
        else if(button == PasscodePinPad.PinPadButton.FOUR)
            btn = "4";
        else if(button == PasscodePinPad.PinPadButton.FIVE)
            btn = "5";
        else if(button == PasscodePinPad.PinPadButton.SIX)
            btn = "6";
        else if(button == PasscodePinPad.PinPadButton.SEVEN)
            btn = "7";
        else if(button == PasscodePinPad.PinPadButton.EIGHT)
            btn = "8";
        else if(button == PasscodePinPad.PinPadButton.NINE)
            btn = "9";
        else
            btn = "0";
        if(controller.getCurrentMode() == PasscodeViewController.Mode.SET || controller.getCurrentMode() == PasscodeViewController.Mode.LOCK)
            pin = pin.concat(btn);
        else if(controller.getCurrentMode() == PasscodeViewController.Mode.CONFIRMATION)
            pinConfirmation = pinConfirmation.concat(btn);
        controller.addLetter();

        //is it last digit?

        if(controller.getCurrentMode() == PasscodeViewController.Mode.SET && pin.length() == 4)
            controller.setCurrentMode(PasscodeViewController.Mode.CONFIRMATION);
        else if(controller.getCurrentMode() == PasscodeViewController.Mode.CONFIRMATION && pinConfirmation.length() == 4)
        {
            if(pin.equals(pinConfirmation))
                controller.correctConfirmation();
            else {
                controller.incorrectConfirmation();
                pinConfirmation = "";
            }
        }
        else if(controller.getCurrentMode() == PasscodeViewController.Mode.LOCK && pin.length() == 4)
        {
            String realPin = TelegramApplication.sharedApplication().sharedPreferences().getString(Constants.PREFS_PASSCODE_LOCK_PIN, "");
            if(pin.equals(realPin))
                controller.passcodeLockPassed();
            else {
                controller.incorrectConfirmation();
                pin = "";
            }
        }
    }

    public String getPin()
    {
        return this.pin;
    }
    public String getPinConfirmation() { return this.pinConfirmation; }
}
