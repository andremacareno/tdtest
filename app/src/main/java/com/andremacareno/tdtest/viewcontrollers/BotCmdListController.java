package com.andremacareno.tdtest.viewcontrollers;

import android.os.Message;
import android.util.Log;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.MessageCache;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.listadapters.BotCommandListAdapter;
import com.andremacareno.tdtest.listadapters.BotKeyboardGridAdapter;
import com.andremacareno.tdtest.listadapters.ExactlyHeightLayoutManager;
import com.andremacareno.tdtest.models.BotCommandModel;
import com.andremacareno.tdtest.models.BotKeyboardModel;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.tasks.GenerateBotCommandModelsTask;
import com.andremacareno.tdtest.updatelisteners.UpdateChatProcessor;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.views.BotCmdListView;
import com.andremacareno.tdtest.views.BotKeyboardView;

import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by andremacareno on 17/04/15.
 */
public class BotCmdListController{
    public interface BotCmdListControllerDelegate
    {
        public void didBotCommandListUpdated();
        public void didCommandSelected(BotCommandModel cmd);
        public void didKeyboardButtonPressed(String msgToSend, boolean oneTimeKeyboard);
        public void didNonKbdReplyMarkupReceived(TdApi.ReplyMarkup replyMarkup);
        public void didKeyboardReceived(boolean personal);
        public void didReplyMarkupMessageIdUpdated(int newMessageId);
    }

    private BotCmdListView commandListView;
    private BotKeyboardView keyboard;

    private BotCommandListAdapter cmdListAdapter;
    private BotKeyboardGridAdapter keyboardAdapter;

    private long chatId;
    private int replyMarkupMsgId;
    private final String TAG = "BotCmdListController";
    private ArrayList<BotCommandModel> commands = new ArrayList<>();
    private TdApi.ChatParticipant[] bots;
    private UserModel bot;
    private PauseHandler cmdListReceiver;
    private BotCmdListControllerDelegate delegate;
    private UpdateChatProcessor replyMarkupUpdateProcessor;
    private NotificationObserver emojiLoadObserver;
    GenerateBotCommandModelsTask processCommands;
    public BotCmdListController(BotCmdListView cmdListView, BotKeyboardView botKeyboard, TdApi.ChatParticipant[] bots, long chat_id, int replyMarkupMessageId, BotCmdListControllerDelegate delegateRef) {
        this.commandListView = cmdListView;
        this.keyboard = botKeyboard;
        this.bots = bots;
        this.bot = null;
        this.chatId = chat_id;
        this.delegate = delegateRef;
        this.replyMarkupMsgId = replyMarkupMessageId;
        init();
    }
    public BotCmdListController(BotCmdListView cmdListView, BotKeyboardView botKeyboard, final UserModel bot, long chat_id, int replyMarkupMessageId, BotCmdListControllerDelegate delegateRef) {
        this.commandListView = cmdListView;
        this.keyboard = botKeyboard;
        this.bot = bot;
        this.bots = null;
        this.chatId = chat_id;
        this.delegate = delegateRef;
        this.replyMarkupMsgId = replyMarkupMessageId;
        init();
    }
    private void init()
    {
        this.emojiLoadObserver = new NotificationObserver(NotificationCenter.didEmojiLoaded) {
            @Override
            public void didNotificationReceived(Notification notification) {
                if(keyboardAdapter != null)
                    keyboardAdapter.notifyDataSetChanged();
            }
        };
        if(bots != null)
            processCommands = new GenerateBotCommandModelsTask(bots, chatId);
        else if(bot != null)
            processCommands = new GenerateBotCommandModelsTask(bot, chatId);
        if(processCommands == null)
            return;
        processCommands.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(AsyncTaskManTask task) {
                ArrayList<BotCommandModel> commands = ((GenerateBotCommandModelsTask) task).getResult();
                Message msg = Message.obtain(cmdListReceiver);
                msg.obj = commands;
                msg.sendToTarget();
            }
        });
        this.cmdListAdapter = new BotCommandListAdapter(commands);
        this.cmdListAdapter.setCommandSelectionCallback(new BotCommandListAdapter.CommandSelectionListener() {
            @Override
            public void onCommandSelected(BotCommandModel cmd) {
                if (delegate != null)
                    delegate.didCommandSelected(cmd);
            }
        });
        this.keyboardAdapter = new BotKeyboardGridAdapter();
        this.keyboardAdapter.setDelegate(delegate);
        this.cmdListReceiver = new BackgroundDataReceiver(this);
        TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
        if(service != null)
            onUpdateServiceConnected(service);
        initViewSettings();
        updateCmdList();
    }
    public void replaceView(BotKeyboardView v, BotCmdListView cmdList) {
        this.keyboard = v;
        this.commandListView = cmdList;
        onViewReplaced();
    }
    protected void onViewReplaced() {
        initViewSettings();
    }
    private void initViewSettings() {
        if(commandListView != null)
        {
            commandListView.setAdapter(cmdListAdapter);
            commandListView.setMotionEventSplittingEnabled(false);
        }
        if(keyboard != null)
        {
            keyboard.setAdapter(keyboardAdapter);
            keyboard.setMotionEventSplittingEnabled(false);
        }
    }
    private void requestReplyMarkup()
    {
        TdApi.Message msg = MessageCache.getInstance().getMessageById(chatId, replyMarkupMsgId);
        TdApi.ReplyMarkup replyMarkup = msg.replyMarkup;
        if (replyMarkup.getConstructor() == TdApi.ReplyMarkupShowKeyboard.CONSTRUCTOR) {
            BotKeyboardModel kbd = new BotKeyboardModel((TdApi.ReplyMarkupShowKeyboard) replyMarkup);
            boolean personal = ((TdApi.ReplyMarkupShowKeyboard) replyMarkup).personal;
            keyboard.updateGridLayout(kbd);
            keyboardAdapter.attachKeyboardModel(kbd);
            if(delegate != null)
                delegate.didKeyboardReceived(personal);
        }
        else
        {
            if(delegate != null)
            {
                delegate.didNonKbdReplyMarkupReceived(replyMarkup);
                delegate.didBotCommandListUpdated();
            }
        }
    }

    public void onFragmentPaused() {
        try
        {
            cmdListReceiver.pause();
            replyMarkupUpdateProcessor.performPauseObservers();
        }
        catch(Exception ignored) {}
    }

    public void onFragmentResumed() {
        try
        {
            cmdListReceiver.resume();
            replyMarkupUpdateProcessor.performResumeObservers();
        }
        catch(Exception ignored) {}
    }

    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        NotificationCenter.getInstance().addObserver(emojiLoadObserver);
        if(replyMarkupUpdateProcessor == null) {
            replyMarkupUpdateProcessor = new UpdateChatProcessor() {
                @Override
                protected String getKeyByUpdateConstructor(int constructor) {
                    return String.format(Constants.CHATSCREEN_REPLY_MARKUP_OBSERVER, chatId);
                }

                @Override
                protected boolean shouldObserveUpdateGroup() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateTitle() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateReplyMarkup() {
                    return true;
                }

                @Override
                protected boolean shouldObserveUpdateReadInbox() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateReadOutbox() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateChat() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateChatPhoto() {
                    return false;
                }

                @Override
                protected boolean shouldObserveUpdateChatOrder() {
                    return false;
                }

                @Override
                protected void processUpdateInBackground(TdApi.Update upd) {
                    if (BuildConfig.DEBUG)
                        Log.d(TAG, String.format("received update: %s", upd.toString()));
                }

                @Override
                protected void processUpdateInUiThread(TdApi.Update upd) {
                    if (BuildConfig.DEBUG)
                        Log.d(TAG, String.format("received update: %s", upd.toString()));
                    TdApi.UpdateChatReplyMarkup update = (TdApi.UpdateChatReplyMarkup) upd;
                    if (update.chatId == chatId) {
                        if (delegate != null)
                            delegate.didReplyMarkupMessageIdUpdated(update.replyMarkupMessageId);
                        replyMarkupMsgId = update.replyMarkupMessageId;
                        if (update.replyMarkupMessageId != 0)
                            requestReplyMarkup();
                    }
                }
            };
        }
        replyMarkupUpdateProcessor.attachObservers(service);
    }
    public void removeObservers()
    {
        if(replyMarkupUpdateProcessor != null)
            replyMarkupUpdateProcessor.removeObservers();
        NotificationCenter.getInstance().removeObserver(emojiLoadObserver);
    }
    public void updateCmdList()
    {
        processCommands.addToQueue();
    }
    public int getCmdListSize() { return this.commands.size(); }

    private static class BackgroundDataReceiver extends PauseHandler
    {
        private WeakReference<BotCmdListController> controllerRef;
        public BackgroundDataReceiver(BotCmdListController controller)
        {
            this.controllerRef = new WeakReference<BotCmdListController>(controller);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef.get() == null)
                return;
            ArrayList<BotCommandModel> additionalCommands = (ArrayList<BotCommandModel>) message.obj;
            int position = controllerRef.get().commands.size();
            ExactlyHeightLayoutManager lm = (ExactlyHeightLayoutManager) controllerRef.get().commandListView.getLayoutManager();
            controllerRef.get().commands.addAll(additionalCommands);
            int totalHeight = AndroidUtilities.dp(38) * controllerRef.get().commands.size();
            int maxHeight = AndroidUtilities.dp(128);
            lm.setSpecifiedHeight(Math.min(totalHeight, maxHeight));
            controllerRef.get().cmdListAdapter.notifyItemRangeInserted(position, additionalCommands.size());
            if(controllerRef.get().replyMarkupMsgId != 0)
                controllerRef.get().requestReplyMarkup();
            else if(controllerRef.get().delegate != null)
                controllerRef.get().delegate.didBotCommandListUpdated();
        }
    }
}
