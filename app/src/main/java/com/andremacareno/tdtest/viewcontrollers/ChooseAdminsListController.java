package com.andremacareno.tdtest.viewcontrollers;

import android.util.Log;
import android.view.View;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.fragments.EditGroupAdminsFragment;
import com.andremacareno.tdtest.listadapters.ChooseAdminsAdapter;
import com.andremacareno.tdtest.models.ChatParticipantModel;
import com.andremacareno.tdtest.tasks.GenerateParticipantsListTask;
import com.andremacareno.tdtest.views.ChatParticipantsListView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import gnu.trove.TCollections;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * Created by andremacareno on 04/04/15.
 */
public class ChooseAdminsListController extends ViewController {
    //statement variables
    public interface ChooseAdminsDelegate
    {
        public void grantEditorRights(int userId);
        public void revokeEditorRights(int userId);
    }
    protected final String TAG = "ChooseAdminsListControl";
    protected ChooseAdminsAdapter adapter;
    private volatile boolean areAllAdmins;
    private final ArrayList<ChatParticipantModel> participantsList = new ArrayList<>();
    private final TIntObjectMap<ChatParticipantModel> idToParticipants = TCollections.synchronizedMap(new TIntObjectHashMap<ChatParticipantModel>());
    private long chatId;
    private int groupId;
    private ChooseAdminsDelegate delegate;
    private WeakReference<EditGroupAdminsFragment> fragmentRef;
    public ChooseAdminsListController(View v, int groupId, long chatId, boolean areAllAdmins, EditGroupAdminsFragment fragmentRef) {
        super(v);
        this.chatId = chatId;
        this.groupId = groupId;
        this.areAllAdmins = areAllAdmins;
        this.fragmentRef = new WeakReference<>(fragmentRef);
        this.delegate = new ChooseAdminsDelegate() {
            @Override
            public void grantEditorRights(final int userId) {
                if(ChooseAdminsListController.this.fragmentRef != null && ChooseAdminsListController.this.fragmentRef.get() != null && ChooseAdminsListController.this.fragmentRef.get().updating)
                    return;
                final TdApi.ChangeChatParticipantRole editRole = new TdApi.ChangeChatParticipantRole(ChooseAdminsListController.this.chatId, userId, Constants.PARTICIPANT_EDITOR);
                TG.getClientInstance().send(editRole, new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        TelegramApplication.applicationHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                synchronized(idToParticipants)
                                {
                                    synchronized(participantsList)
                                    {
                                        if(!idToParticipants.containsKey(userId))
                                            return;
                                        idToParticipants.get(userId).setRole(Constants.PARTICIPANT_EDITOR);
                                        int index = participantsList.indexOf(idToParticipants.get(userId));
                                        if(index >= 0 && adapter != null)
                                            adapter.notifyItemChanged(index);
                                    }
                                }
                            }
                        });
                    }
                });
            }

            @Override
            public void revokeEditorRights(final int userId) {
                if(ChooseAdminsListController.this.fragmentRef != null && ChooseAdminsListController.this.fragmentRef.get() != null && ChooseAdminsListController.this.fragmentRef.get().updating)
                    return;
                final TdApi.ChangeChatParticipantRole editRole = new TdApi.ChangeChatParticipantRole(ChooseAdminsListController.this.chatId, userId, Constants.PARTICIPANT_GENERAL);
                TG.getClientInstance().send(editRole, new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        TelegramApplication.applicationHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                synchronized(idToParticipants)
                                {
                                    synchronized(participantsList)
                                    {
                                        if(!idToParticipants.containsKey(userId))
                                            return;
                                        idToParticipants.get(userId).setRole(Constants.PARTICIPANT_GENERAL);
                                        int index = participantsList.indexOf(idToParticipants.get(userId));
                                        if(index >= 0 && adapter != null)
                                            adapter.notifyItemChanged(index);
                                    }
                                }
                            }
                        });
                    }
                });
            }
        };
        this.adapter = new ChooseAdminsAdapter(participantsList, delegate, areAllAdmins);
        generateParticipantsList();
    }
    private void generateParticipantsList()
    {
        GenerateParticipantsListTask task = new GenerateParticipantsListTask(groupId);
        task.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(AsyncTaskManTask task) {
                ArrayList<ChatParticipantModel> participants = ((GenerateParticipantsListTask) task).getParticipants();
                synchronized (idToParticipants) {
                    synchronized(participantsList)
                    {
                        for (ChatParticipantModel p : participants) {
                            if(p.isBot())
                                continue;
                            participantsList.add(p);
                            idToParticipants.put(p.getId(), p);
                        }
                    }
                }
                TelegramApplication.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        initViewSettings();
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });
        task.addToQueue();
    }
    public void notifyAllAdminsFlagChanged(boolean areAllAdmins)
    {
        Log.d(TAG, "notifyAllAdminsFlagChanged");
        this.areAllAdmins = areAllAdmins;
        if(adapter != null)
            adapter.setAllAdminsFlag(this.areAllAdmins);
        else
            Log.d(TAG, "null");
    }
    @Override
    protected void onViewReplaced() {
        if(getView() != null) {
            initViewSettings();
        }
    }
    public void onFragmentPaused()
    {
    }
    public void onFragmentResumed()
    {
    }
    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
    }
    public void removeObservers()
    {
    }


    private void initViewSettings()
    {
        ChatParticipantsListView v = (ChatParticipantsListView) getView();
        synchronized(participantsList)
        {
            v.setNewElementCount(participantsList.size());
        }
        v.setAdapter(adapter);
        v.setMotionEventSplittingEnabled(false);
    }
}
