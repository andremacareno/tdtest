package com.andremacareno.tdtest.viewcontrollers.passcode;

import android.content.SharedPreferences;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.views.PasscodeView;

/**
 * Created by andremacareno on 02/08/15.
 */
public class PasscodePasswordController extends PasscodeViewController{
    private View telegramLogo;
    private TextView helpTextView;
    private EditText passcodeEditText;
    TextView.OnEditorActionListener doneButtonListener;
    public PasscodePasswordController(PasscodeView view, Mode mode) {
        super(view, mode);
    }
    @Override
    protected void init()
    {
        telegramLogo = viewRef.findViewById(R.id.logo);
        helpTextView = (TextView) viewRef.findViewById(R.id.help);
        passcodeEditText = (EditText) viewRef.findViewById(R.id.passwordEditText);
        doneButtonListener = new PasswordDoneButtonController(this);
        passcodeEditText.setOnEditorActionListener(doneButtonListener);
    }
    @Override
    protected void savePasscode(SharedPreferences.Editor prefEdit) {
        prefEdit.putInt(Constants.PREFS_PASSCODE_LOCK_TYPE, Constants.PASSCODE_TYPE_PASSWORD);
        prefEdit.putString(Constants.PREFS_PASSCODE_LOCK_PASSWORD, passcodeEditText.getText().toString());
        AndroidUtilities.hideKeyboard(passcodeEditText);
    }
    @Override
    public void incorrectConfirmation()
    {
        helpTextView.startAnimation(shakeAnimation);
        passcodeEditText.setText("");
    }

    @Override
    protected void didModeChanged() {
        if(currentMode == Mode.SET || currentMode == Mode.CONFIRMATION) {
            telegramLogo.setVisibility(View.GONE);
            AndroidUtilities.showKeyboard(passcodeEditText);
        }
        if(currentMode == Mode.SET) {
            helpTextView.setText(R.string.password_set);
        }
        else if(currentMode == Mode.CONFIRMATION) {
            helpTextView.setText(R.string.password_confirm);
            passcodeEditText.setText("");
        }
        else if(currentMode == Mode.LOCK) {
            helpTextView.setText(R.string.password_lock);
            AndroidUtilities.showKeyboard(passcodeEditText);
        }
    }
    public String getPassword()
    {
        return this.passcodeEditText.getText().toString();
    }
    public void hideKeyboard()
    {
        AndroidUtilities.hideKeyboard(passcodeEditText);
    }

}
