package com.andremacareno.tdtest.viewcontrollers;

import android.view.View;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.fragments.RosterFragment;
import com.andremacareno.tdtest.listadapters.GlobalSearchAdapter;
import com.andremacareno.tdtest.models.ChatSearchResult;
import com.andremacareno.tdtest.models.GlobalSearchResult;
import com.andremacareno.tdtest.models.MessageSearchResult;
import com.andremacareno.tdtest.models.SectionSearchResult;
import com.andremacareno.tdtest.tasks.GlobalMessageSearchProcessTask;
import com.andremacareno.tdtest.views.SearchResultListView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by andremacareno on 04/04/15.
 */
public class GlobalSearchListController extends ViewController {
    //statement variables
    private final String TAG = "SrchListController";
    private final AtomicBoolean preventLoading = new AtomicBoolean(false);
    private GlobalSearchAdapter adapter;
    private volatile String lastQuery = "";

    //update handlers
    private RosterFragment.RosterFragmentDelegate fragmentDelegate;
    private GlobalSearchResult.GlobalSearchDelegate searchDelegate;
    //data model
    private final ArrayList<GlobalSearchResult> data = new ArrayList<>();

    private NotificationObserver emojiLoadObserver;

    private final TdApi.SearchMessages lastQueryParams = new TdApi.SearchMessages();
    private Client.ResultHandler loadMoreHandler;
    public GlobalSearchListController(View v) {
        super(v);
        init();
    }
    public void setFragmentDelegate(RosterFragment.RosterFragmentDelegate delegate)
    {
        this.fragmentDelegate = delegate;
    }
    private void init()
    {
        lastQueryParams.limit = 20;
        loadMoreHandler = new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(object == null || object.getConstructor() != TdApi.Messages.CONSTRUCTOR)
                    return;
                TdApi.Messages msgs = (TdApi.Messages) object;
                synchronized(lastQueryParams)
                {
                    if(msgs.messages.length > 0)
                    {
                        TdApi.Message lastMsg = msgs.messages[msgs.messages.length - 1];
                        lastQueryParams.offsetChatId = lastMsg.chatId;
                        lastQueryParams.offsetDate = lastMsg.date;
                        lastQueryParams.offsetMessageId = lastMsg.id;
                    }
                }
                didMessagesFound(msgs, null);
            }
        };
        initObservers();
        continueInit();
    }
    public void clearData()
    {
        synchronized(data)
        {
            data.clear();
            adapter.notifyDataSetChanged();
        }
    }
    public void newQuery(final String query)
    {
        lastQuery = query;
        synchronized(data)
        {
            data.clear();
            adapter.notifyDataSetChanged();
        }
        synchronized(lastQueryParams)
        {
            lastQueryParams.offsetChatId = lastQueryParams.offsetDate = lastQueryParams.offsetMessageId = 0;
            lastQueryParams.query = "";
        }
        TdApi.SearchChats searchChatsReq = new TdApi.SearchChats(query, 3);
        Client.ResultHandler searchChatsHandler = new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(object == null || object.getConstructor() != TdApi.Chats.CONSTRUCTOR)
                    return;
                final ArrayList<GlobalSearchResult> results = new ArrayList<>();
                for(TdApi.Chat ch : ((TdApi.Chats) object).chats)
                {
                    ChatSearchResult result = new ChatSearchResult(ch, GlobalSearchResult.ResultType.CHAT_CONTACT);
                    results.add(result);
                }
                if(!results.isEmpty())
                    results.add(0, new SectionSearchResult(query.isEmpty() ? TelegramApplication.sharedApplication().getResources().getString(R.string.recent) : TelegramApplication.sharedApplication().getResources().getString(R.string.chats_and_contacts)));
                if(query.isEmpty() && lastQuery.isEmpty())
                {
                    didMessagesFound(null, results);
                    return;
                }
                TdApi.SearchPublicChats searchPublicReq = new TdApi.SearchPublicChats(query);
                TG.getClientInstance().send(searchPublicReq, new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        if(object == null || object.getConstructor() != TdApi.Chats.CONSTRUCTOR)
                            return;
                        ArrayList<GlobalSearchResult> publicChats = new ArrayList<>();
                        processPublicChats((TdApi.Chats) object, publicChats);
                        results.addAll(publicChats);
                        TdApi.SearchMessages searchMessages = new TdApi.SearchMessages(query, 0, 0, 0, 20);
                        TG.getClientInstance().send(searchMessages, new Client.ResultHandler() {
                            @Override
                            public void onResult(TdApi.TLObject object) {
                                if(object == null || object.getConstructor() != TdApi.Messages.CONSTRUCTOR || !lastQuery.equals(query))
                                    return;
                                TdApi.Messages msgs = (TdApi.Messages) object;
                                synchronized(lastQueryParams)
                                {
                                    if(msgs.messages.length > 0)
                                    {
                                        TdApi.Message lastMsg = msgs.messages[msgs.messages.length - 1];
                                        lastQueryParams.query = query;
                                        lastQueryParams.offsetChatId = lastMsg.chatId;
                                        lastQueryParams.offsetDate = lastMsg.date;
                                        lastQueryParams.offsetMessageId = lastMsg.id;
                                    }
                                }
                                didMessagesFound(msgs, results);
                            }
                        });
                    }
                });
            }
        };
        TG.getClientInstance().send(searchChatsReq, searchChatsHandler);
    }
    @Override
    protected void onViewReplaced() {
        if(getView() != null) {
            initViewSettings();
        }
    }

    @Override
    public void onFragmentPaused() {
    }

    @Override
    public void onFragmentResumed() {
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        NotificationCenter.getInstance().addObserver(emojiLoadObserver);
    }

    public void removeObservers()
    {
        NotificationCenter.getInstance().removeObserver(emojiLoadObserver);
        emojiLoadObserver = null;
    }

    public ArrayList<GlobalSearchResult> getData() { return this.data; }
    private void continueInit()
    {
        this.searchDelegate = new GlobalSearchResult.GlobalSearchDelegate() {
            @Override
            public void didMessageSelected(MessageSearchResult result) {
                if(fragmentDelegate != null)
                    fragmentDelegate.didSearchResultSelected(result);
            }

            @Override
            public void didChatSelected(ChatSearchResult result) {
                if(fragmentDelegate != null)
                    fragmentDelegate.didSearchResultSelected(result);
            }

            @Override
            public void loadMoreMessages() {
                synchronized(lastQueryParams)
                {
                    if(lastQueryParams.query == null || lastQueryParams.query.isEmpty())
                        return;
                }
                synchronized(preventLoading)
                {
                    if(preventLoading.get()) {
                        return;
                    }
                    preventLoading.set(true);
                }
                synchronized(lastQueryParams)
                {
                    TG.getClientInstance().send(lastQueryParams, loadMoreHandler);
                }
            }
        };
        this.adapter = new GlobalSearchAdapter(data, searchDelegate);
        SearchResultListView v = (SearchResultListView) getView();
        if(v != null) {
            initViewSettings();
        }
    }
    private void initViewSettings()
    {
        SearchResultListView v = (SearchResultListView) getView();
        v.setAdapter(adapter);
        v.setMotionEventSplittingEnabled(false);
    }
    private void initObservers()
    {
        this.emojiLoadObserver = new NotificationObserver(NotificationCenter.didEmojiLoaded) {
            @Override
            public void didNotificationReceived(Notification notification) {
                if(adapter != null)
                    adapter.notifyDataSetChanged();
            }
        };
    }
    private void processPublicChats(TdApi.Chats chats, ArrayList<GlobalSearchResult> results)
    {
        for(TdApi.Chat ch : chats.chats)
        {
            ChatSearchResult result = new ChatSearchResult(ch, GlobalSearchResult.ResultType.GLOBAL);
            results.add(result);
        }
        if(!results.isEmpty())
            results.add(0, new SectionSearchResult(TelegramApplication.sharedApplication().getResources().getString(R.string.global_search)));
    }
    private void didMessagesFound(TdApi.Messages msgs, final ArrayList<GlobalSearchResult> resultsList) {
        //convert to models
        GlobalMessageSearchProcessTask t = new GlobalMessageSearchProcessTask(msgs);
        t.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(AsyncTaskManTask task) {
                try {
                    final List<GlobalSearchResult> result = ((GlobalMessageSearchProcessTask) task).getResult();
                    synchronized(data)
                    {
                        if((resultsList != null || data.isEmpty()) && !result.isEmpty())
                            result.add(0, new SectionSearchResult(TelegramApplication.sharedApplication().getResources().getString(R.string.messages)));
                    }
                    if(resultsList != null)
                        resultsList.addAll(result);
                    TelegramApplication.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            synchronized(data)
                            {
                                int start = data.size();
                                if(fragmentDelegate != null)
                                    fragmentDelegate.notifySearchResultsCount(resultsList != null ? resultsList.size() : result.size() + start);
                                if(resultsList != null)
                                    data.clear();
                                data.addAll(resultsList != null ? resultsList : result);
                                if(resultsList != null)
                                    adapter.notifyDataSetChanged();
                                else
                                    adapter.notifyItemRangeInserted(start, result.size());
                                if(!result.isEmpty())
                                {
                                    synchronized(preventLoading)
                                    {
                                        preventLoading.set(false);
                                    }
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                }
            }
        });
        t.addToQueue();
    }
}
