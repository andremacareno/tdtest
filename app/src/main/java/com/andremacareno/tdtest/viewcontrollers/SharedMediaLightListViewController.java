package com.andremacareno.tdtest.viewcontrollers;

import android.os.Message;
import android.view.View;

import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.UserCache;
import com.andremacareno.tdtest.listadapters.LightSharedMediaAdapter;
import com.andremacareno.tdtest.models.SharedImageModel;
import com.andremacareno.tdtest.models.SharedMedia;
import com.andremacareno.tdtest.models.SharedVideoModel;
import com.andremacareno.tdtest.views.SharedMediaLightListView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by andremacareno on 19/08/15.
 */
public class SharedMediaLightListViewController extends ViewController {

    public interface SharedMediaLightListDelegate
    {
        public void didMediaCountReceived(int count);
        public void didItemClicked(SharedMedia item);
    }
    //adapters
    private LightSharedMediaAdapter imgAdapter;

    //chatSearch parameters
    private final AtomicBoolean loadingImages = new AtomicBoolean(false);
    private volatile int imgSearchFromId = 0;
    private final TdApi.SearchMessagesFilter imgFilter = new TdApi.SearchMessagesFilterPhotoAndVideo();
    private final TdApi.SearchChatMessages getImages = new TdApi.SearchChatMessages(0, "", 0, 5, false, imgFilter);
    //images data
    private final ArrayList<SharedMedia> images = new ArrayList<SharedMedia>();
    //handler
    private PauseHandler mediaReceiver;
    private int mediaCount = -1;
    private SharedMediaLightListDelegate listDelegate = null;

    public SharedMediaLightListViewController(View v, long chat_id) {
        super(v);
        getImages.chatId = chat_id;
        init();
    }
    private void init()
    {
        imgAdapter = new LightSharedMediaAdapter(this);
        mediaReceiver = new MediaReceiver(this);
        if(getView() != null)
            ((SharedMediaLightListView) getView()).setAdapter(imgAdapter);
    }
    @Override
    protected void onViewReplaced() {
        if(getView() == null)
            return;
        ((SharedMediaLightListView) getView()).setAdapter(imgAdapter);
    }
    public void setListDelegate(SharedMediaLightListDelegate callback)
    {
        if(mediaCount != -1)
            listDelegate.didMediaCountReceived(mediaCount);
        this.listDelegate = callback;
    }
    @Override
    public void onFragmentPaused() {
        mediaReceiver.pause();
    }

    @Override
    public void onFragmentResumed() {
        mediaReceiver.resume();
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
    }
    public void didItemClicked(SharedMedia item) {
        if(listDelegate != null)
            listDelegate.didItemClicked(item);
    }
    public void searchImages()
    {
        synchronized(loadingImages)
        {
            if(loadingImages.get())
                return;
            loadingImages.set(true);
        }
        getImages.fromMessageId = imgSearchFromId;
        TG.getClientInstance().send(getImages, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(object.getConstructor() != TdApi.Messages.CONSTRUCTOR)
                    return;
                ArrayList<SharedMedia> result = extractImageMedia(((TdApi.Messages) object).messages);
                    Message msg = Message.obtain(mediaReceiver);
                    msg.obj = result;
                    msg.what = ((TdApi.Messages) object).totalCount;
                    msg.sendToTarget();
            }
        });
    }
    public ArrayList<SharedMedia> getImages() { return this.images; }
    private ArrayList<SharedMedia> extractImageMedia(TdApi.Message[] source)
    {
        ArrayList<SharedMedia> models = new ArrayList<>();
        for(TdApi.Message msg : source)
        {
            UserCache.getInstance().cacheUser(msg.fromId);
            SharedMedia item;
            if(msg.content.getConstructor() == TdApi.MessagePhoto.CONSTRUCTOR)
                item = new SharedImageModel(msg);
            else if(msg.content.getConstructor() == TdApi.MessageVideo.CONSTRUCTOR)
                item = new SharedVideoModel(msg);
            else
                continue;
            models.add(item);
        }
        return models;
    }

    private static class MediaReceiver extends PauseHandler
    {
        private WeakReference<SharedMediaLightListViewController> controllerRef;

        public MediaReceiver(SharedMediaLightListViewController controller)
        {
            this.controllerRef = new WeakReference<SharedMediaLightListViewController>(controller);
        }

        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef.get() == null)
                return;
            if(message.obj == null)
                return;
            ArrayList<SharedMedia> result = (ArrayList<SharedMedia>) message.obj;
            int position = controllerRef.get().images.size();
            if(controllerRef.get().listDelegate != null && controllerRef.get().mediaCount == -1)
            {
                controllerRef.get().mediaCount = message.what;
                controllerRef.get().listDelegate.didMediaCountReceived(message.what);
            }
            if(result.size() > 0) {
                controllerRef.get().images.addAll(result);
                controllerRef.get().imgAdapter.notifyItemRangeInserted(position, result.size());
                controllerRef.get().imgSearchFromId = result.get(result.size()-1).getMessageId();
                synchronized (controllerRef.get().loadingImages) {
                    controllerRef.get().loadingImages.set(false);
                }
            }
        }
    }
}
