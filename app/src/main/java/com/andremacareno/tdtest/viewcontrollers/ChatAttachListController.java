package com.andremacareno.tdtest.viewcontrollers;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Message;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.OnTaskFailureListener;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.fragments.BaseChatFragment;
import com.andremacareno.tdtest.listadapters.AttachMenuAdapter;
import com.andremacareno.tdtest.models.TelegramAction;
import com.andremacareno.tdtest.tasks.CreateFileForCameraTask;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.views.AttachMenuView;
import com.andremacareno.tdtest.views.ChatAttachListView;

import java.io.File;
import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 17/04/15.
 */
public class ChatAttachListController extends ViewController{
    //statement variables
    private volatile String photo_path;
    private final String TAG = "AttachListController";
    private AttachMenuAdapter adapter;
    private int chosenAction = TelegramAction.ATTACH_DO_NOTHING;
    private WeakReference<BaseChatFragment> fragmentRef;
    private PauseHandler fileCreatedHandler;
    private AttachMenuView.AttachMenuDelegate delegate;
    public ChatAttachListController(View v) {
        super(v);
        init();
    }
    private void init()
    {
        this.adapter = new AttachMenuAdapter(this);
        this.fileCreatedHandler = new CameraFileReceiver(this);
        initViewSettings();
    }
    @Override
    protected void onViewReplaced() {
        if(getView() != null) {
            initViewSettings();
        }
    }
    public AttachMenuAdapter getAdapter() { return this.adapter; }
    public void setFragmentReference(BaseChatFragment ref) { this.fragmentRef = new WeakReference<BaseChatFragment>(ref); }
    private void initViewSettings() {
        ChatAttachListView v = (ChatAttachListView) getView();
        v.setAdapter(adapter);
        v.setMotionEventSplittingEnabled(false);
    }

    @Override
    public void onFragmentPaused() {
        if(fileCreatedHandler != null)
            fileCreatedHandler.pause();
    }

    @Override
    public void onFragmentResumed() {
        if(fileCreatedHandler != null)
            fileCreatedHandler.resume();
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }

    public void didActionChosen(TelegramAction act)
    {
        this.chosenAction = act.getAction();
        if(delegate != null)
            delegate.onActionChoose();
    }
    public void continueStartIntent()
    {
        if(chosenAction == TelegramAction.ATTACH_DO_NOTHING)
            return;
        if(chosenAction == TelegramAction.ATTACH_TAKE_A_PHOTO)
        {
            chosenAction = TelegramAction.ATTACH_DO_NOTHING;
            performCameraIntentLaunch();
        }
        else if(chosenAction == TelegramAction.ATTACH_CHOOSE_FROM_GALLERY)
        {
            chosenAction = TelegramAction.ATTACH_DO_NOTHING;
            if(adapter.getQuickSendSelectionCount() == 0)
                performGalleryIntentLaunch();
            else if(delegate != null)
                delegate.requestPerformQuickSend();
        }
        else if(chosenAction == TelegramAction.ATTACH_CONTACT)
        {
            chosenAction = TelegramAction.ATTACH_DO_NOTHING;
            if(delegate != null)
                delegate.shareContactRequested();
        }
        else if(chosenAction == TelegramAction.ATTACH_AUDIO)
        {
            chosenAction = TelegramAction.ATTACH_DO_NOTHING;
            performAudioIntentLaunch();
        }
    }
    private void performGalleryIntentLaunch()
    {
        if(fragmentRef.get() == null)
            return;
        if (Build.VERSION.SDK_INT <19){
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            fragmentRef.get().startActivityForResult(galleryIntent, TelegramAction.ATTACH_CHOOSE_FROM_GALLERY);
        } else {
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            fragmentRef.get().startActivityForResult(galleryIntent, TelegramAction.ATTACH_CHOOSE_FROM_GALLERY);
        }
    }
    private void performCameraIntentLaunch()
    {
        if(fragmentRef.get() == null)
            return;
        if(!AndroidUtilities.haveCamera())
        {
            Toast.makeText(TelegramApplication.sharedApplication().getApplicationContext(), R.string.no_camera, Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Activity activity = fragmentRef.get().getActivity();
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            CreateFileForCameraTask task = new CreateFileForCameraTask();
            task.onComplete(new OnTaskCompleteListener() {
                @Override
                public void taskComplete(AsyncTaskManTask task) {
                    CreateFileForCameraTask t = (CreateFileForCameraTask) task;
                    ChatAttachListController.this.photo_path = t.getPhotoPath();
                    Message msg = Message.obtain(fileCreatedHandler);
                    msg.obj = t.getPhotoUri();
                    msg.sendToTarget();
                }
            });
            task.onFailure(new OnTaskFailureListener() {
                @Override
                public void taskFailed(AsyncTaskManTask task) {
                    //Toast.makeText((fragmentRef.get().getActivity()), R.string.error_taking_photo, Toast.LENGTH_SHORT).show();
                }
            });
            task.addToQueue();
        }
    }
    private void performAudioIntentLaunch()
    {
        if(fragmentRef.get() == null)
            return;
        if (Build.VERSION.SDK_INT <19){
            Intent audioIntent = new Intent();
            audioIntent.setType("audio/*");
            audioIntent.setAction(Intent.ACTION_GET_CONTENT);
            audioIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            fragmentRef.get().startActivityForResult(audioIntent, TelegramAction.ATTACH_AUDIO);
        } else {
            Intent audioIntent = new Intent();
            audioIntent.setType("audio/*");
            audioIntent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            audioIntent.addCategory(Intent.CATEGORY_OPENABLE);
            audioIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            fragmentRef.get().startActivityForResult(audioIntent, TelegramAction.ATTACH_AUDIO);
        }
    }
    public void addPhotoToGallery() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File("file:".concat(this.photo_path));
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        fragmentRef.get().getActivity().sendBroadcast(mediaScanIntent);
    }
    public void setDelegate(AttachMenuView.AttachMenuDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    public String getPhotoPath() { return this.photo_path; }

    public static class CameraFileReceiver extends PauseHandler
    {
        private WeakReference<ChatAttachListController> controllerRef;
        public CameraFileReceiver(ChatAttachListController c)
        {
            this.controllerRef = new WeakReference<ChatAttachListController>(c);
        }

        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef.get() == null)
                return;
            Uri capturedImageURI = (Uri) message.obj;
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    capturedImageURI);
            controllerRef.get().fragmentRef.get().startActivityForResult(takePictureIntent, TelegramAction.ATTACH_TAKE_A_PHOTO);
        }
    }
}
