package com.andremacareno.tdtest.viewcontrollers;

import android.content.SharedPreferences;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.auth.AuthStateProcessor;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by andremacareno on 03/04/15.
 */
public class SubmitPhoneButtonController {
    private final AtomicBoolean lockSetPhone = new AtomicBoolean(false);
    private EditText codeEditText;
    private EditText phoneEditText;
    private TextView wrongPhone;
    private final String TAG = "PhoneButtonController";
    private AuthStateProcessor.SecondaryAuthDelegate secondaryAuthDelegate;
    public SubmitPhoneButtonController(EditText code, EditText phone, TextView wrongPhoneTextView)
    {
        this.codeEditText = code;
        this.phoneEditText = phone;
        this.wrongPhone = wrongPhoneTextView;
        this.secondaryAuthDelegate = new AuthStateProcessor.SecondaryAuthDelegate() {
            @Override
            public String getPhoneNumber() {
                return CommonTools.formatPhoneNumber(codeEditText.getText().toString().concat(phoneEditText.getText().toString()));
            }

            @Override
            public void processError() {
                wrongPhone.setVisibility(View.VISIBLE);
                unlockSetPhone();
            }
        };
    }
    public void didSetPhoneButtonClicked() {
        synchronized(lockSetPhone)
        {
            if(codeEditText == null || phoneEditText == null)
                return;
            if(lockSetPhone.get() == true)
                return;
            String country = codeEditText.getText().toString();
            String phonePart = phoneEditText.getText().toString();
            String phone = country.concat(phonePart);
            TdApi.SetAuthPhoneNumber setPhoneNumberReq = new TdApi.SetAuthPhoneNumber(phone);
            AuthStateProcessor.getInstance().changeAuthState(setPhoneNumberReq);
            SharedPreferences.Editor prefEdit = TelegramApplication.sharedApplication().sharedPreferences().edit();
            prefEdit.putString(Constants.PREFS_PHONE, CommonTools.formatPhoneNumber(phone));
            prefEdit.apply();
            lockSetPhone.set(true);
        }
    }
    public void unlockSetPhone()
    {
        lockSetPhone.set(false);
    }
    public void rebindToViews(EditText code, EditText phone, TextView wrongPhoneTextView)
    {
        this.codeEditText = code;
        this.phoneEditText = phone;
        this.wrongPhone = wrongPhoneTextView;
    }
    public AuthStateProcessor.SecondaryAuthDelegate getSecondaryAuthDelegate() { return this.secondaryAuthDelegate; }
}
