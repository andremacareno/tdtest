package com.andremacareno.tdtest.viewcontrollers.actionview;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.views.actionviews.ChatActionView;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 14.05.2015.
 */
public class PrivateChatActionViewController extends ActionViewController {
    private final int avatarSize;
    private volatile String avatarKey;
    private volatile String placeholderKey;
    private UserModel fullUserInfo = null;
    private boolean updatedName = false;
    private boolean updatedPhoto = false;

    public PrivateChatActionViewController()
    {
        this.avatarSize = TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size);
    }
    public void bindUserInfo(UserModel userInfo)
    {
        this.fullUserInfo = userInfo;
        this.placeholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, this.fullUserInfo.getId());
        this.avatarKey = CommonTools.makeFileKey(fullUserInfo.getPhoto().getFileId());
        TelegramUpdatesHandler srv = TelegramApplication.sharedApplication().getUpdateService();
        if(srv != null)
            onUpdateServiceConnected(srv);
        //fillLayout();
    }


    @Override
    public void onFragmentPaused() {
    }

    @Override
    public void onFragmentResumed() {
        //fillLayout();
    }

    @Override
    public void onActionViewRemoved() {
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
    }
    @Override
    protected void onActionViewSet() { fillLayout(); }
    private void fillLayout()
    {
        if(fullUserInfo == null)
            return;
        ChatActionView v = (ChatActionView) getActionView();
        if(v != null)
        {
            if(fullUserInfo.isDeletedAccount())
                v.firstLine.setText(R.string.unknown_user);
            else
               v.firstLine.setText(fullUserInfo.getDisplayName());
            if(fullUserInfo.getId() == 333000 || fullUserInfo.getId() == 777000)
                v.secondLine.setText(R.string.service_notifications);
            else if(fullUserInfo.isBot())
                v.secondLine.setText(R.string.bot);
            else
                v.secondLine.setText(fullUserInfo.getStringStatus());

            if(fullUserInfo.getPhoto() == null || fullUserInfo.getPhoto().getFileId() == 0)
                TelegramImageLoader.getInstance().loadPlaceholder(fullUserInfo.getId(), fullUserInfo.getInitials(), placeholderKey, v.avatar, false);
            else {
                TelegramImageLoader.getInstance().loadImage(fullUserInfo.getPhoto(), avatarKey, ((ChatActionView) getActionView()).avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, avatarSize, avatarSize, false);
            }
        }
    }
    public void updateStatus(TdApi.UserStatus newStatus)
    {
        if(fullUserInfo.getId() != 333000 && fullUserInfo.getId() != 777000)
            ((ChatActionView) getActionView()).secondLine.setText(CommonTools.statusToText(newStatus));
        else
            ((ChatActionView) getActionView()).secondLine.setText(R.string.service_notifications);
    }
    public void updateTitle(String newTitle)
    {
        ((ChatActionView) getActionView()).firstLine.setText(newTitle);
    }
    public void updatePhoto(TdApi.File newPhoto)
    {
        try
        {
            this.placeholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, this.fullUserInfo.getId());
            this.avatarKey = CommonTools.makeFileKey(newPhoto.id);

            if(newPhoto.id == 0)
                TelegramImageLoader.getInstance().loadPlaceholder(fullUserInfo.getId(), fullUserInfo.getInitials(), placeholderKey, ((ChatActionView) getActionView()).avatar, true);
            else {
                TelegramImageLoader.getInstance().loadImage(FileCache.getInstance().getFileModel(newPhoto), avatarKey, ((ChatActionView) getActionView()).avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, avatarSize, avatarSize, true);
            }
        }
        catch(Exception e) { e.printStackTrace(); }
    }
}
