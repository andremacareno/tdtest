package com.andremacareno.tdtest.viewcontrollers;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.listadapters.ContactsAdapter;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.tasks.LoadNewChatParticipantsTask;
import com.andremacareno.tdtest.updatelisteners.UpdateUserProcessor;
import com.andremacareno.tdtest.views.ContactsListView;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * Created by andremacareno on 04/04/15.
 */
public class NewChatParticipantsListController extends ViewController {
    //statement variables
    protected final String TAG = "ParticipantsListControl";
    protected RecyclerView.Adapter adapter;
    private UpdateUserProcessor updateUserProcessor;
    private final TIntObjectHashMap<UserModel> participantsMap = new TIntObjectHashMap<>();
    private final ArrayList<UserModel> participantsList = new ArrayList<>();
    private int[] participants;
    public NewChatParticipantsListController(View v, int[] participantIds) {
        super(v);
        this.participants = participantIds;
        init();
        loadParticipants();
    }
    @Override
    protected void onViewReplaced() {
        if(getView() != null) {
            initViewSettings();
        }
    }
    public void onFragmentPaused()
    {
        updateUserProcessor.performPauseObservers();
    }
    public void onFragmentResumed()
    {
        updateUserProcessor.performResumeObservers();
    }
    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        updateUserProcessor.attachObservers(service);
    }
    public void removeObservers()
    {
        updateUserProcessor.removeObservers();
    }

    private void init()
    {
        this.adapter = new ContactsAdapter(participantsList, null, false);

        this.updateUserProcessor = new UpdateUserProcessor() {
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                return String.format("newchat_participants_constructor_%d_observer", constructor);
            }

            @Override
            protected boolean shouldObserveUpdateUserStatus() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateUserBlocked() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateUser() {
                return false;
            }

            @Override
            protected void processUpdateInBackground(TdApi.Update upd) {
            }

            @Override
            protected void processUpdateInUiThread(TdApi.Update upd) {
                if(upd == null || upd.getConstructor() != TdApi.UpdateUserStatus.CONSTRUCTOR)
                    return;
                int index = getIndexOfUserById(((TdApi.UpdateUserStatus) upd).userId);
                if(index >= 0) {
                    synchronized(participantsList)
                    {
                        participantsList.get(index).updateStatus(((TdApi.UpdateUserStatus) upd).status);
                    }
                    adapter.notifyItemChanged(index);
                }
            }
        };
        initViewSettings();
    }
    private void loadParticipants()
    {
        if(BuildConfig.DEBUG)
            Log.d("Controller", "loading participants");
        LoadNewChatParticipantsTask task = new LoadNewChatParticipantsTask(participants);
        task.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(AsyncTaskManTask task) {
                ArrayList<UserModel> result = ((LoadNewChatParticipantsTask) task).getResult();
                for(UserModel u : result)
                {
                    synchronized(participantsMap)
                    {
                        synchronized(participantsList)
                        {
                            if(participantsMap.containsKey(u.getId()))
                                continue;
                            participantsMap.put(u.getId(), u);
                            participantsList.add(u);
                        }
                    }
                }
                TelegramApplication.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(BuildConfig.DEBUG)
                            Log.d("Controller", "loading finished");
                        if(adapter != null)
                            adapter.notifyDataSetChanged();
                    }
                });
            }
        });
        task.addToQueue();
    }
    private void initViewSettings()
    {
        View v = getView();
        if(v == null)
            return;
        ((ContactsListView) v).setAdapter(this.adapter);
    }
    private int getIndexOfUserById(int id)
    {
        synchronized(participantsMap)
        {
            synchronized(participantsList)
            {
                if(participantsMap.containsKey(id))
                    return participantsList.indexOf(participantsMap.get(id));
                return -1;
            }
        }
    }
}
