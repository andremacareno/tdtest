package com.andremacareno.tdtest.viewcontrollers;

import android.view.View;

import com.andremacareno.tdtest.TelegramUpdatesHandler;

/**
 * Created by andremacareno on 13/08/15.
 */
public class BotKeyboardViewController extends ViewController {
    public interface BotKeyboardDelegate
    {
        public void didButtonPressed(String button);
    }

    private BotKeyboardDelegate delegate;

    public BotKeyboardViewController(View v) {
        super(v);
    }

    @Override
    public void onFragmentPaused() {

    }

    @Override
    public void onFragmentResumed() {

    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        //TODO update reply markup
    }

    public void setDelegate(BotKeyboardDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
}
