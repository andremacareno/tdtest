package com.andremacareno.tdtest.viewcontrollers;

import android.view.View;

import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.listadapters.InlineBotsAdapter;
import com.andremacareno.tdtest.models.InlineQueryResultWrap;
import com.andremacareno.tdtest.views.BaseListView;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andremacareno on 17/04/15.
 */
public class InlineResultListController extends ViewController {
    public interface InlineResultListDelegate
    {
        public void didResultSelected(long queryId, String resultId);
        public void loadMore();
    }

    private InlineBotsAdapter resultsListAdapter;
    private final String TAG = "InlineListController";
    private ArrayList<InlineQueryResultWrap> results = new ArrayList<>();
    private InlineResultListDelegate delegate;
    public InlineResultListController(View v, InlineResultListDelegate delegateRef) {
        super(v);
        this.delegate = delegateRef;
        init();
    }
    private void init()
    {
        this.resultsListAdapter = new InlineBotsAdapter(results);
        this.resultsListAdapter.setSelectionCallback(new InlineBotsAdapter.ResultSelectionListener() {
            @Override
            public void onResultSelected(InlineQueryResultWrap res) {
                String resultId;
                TdApi.InlineQueryResult result = res.getResult();
                if(result.getConstructor() == TdApi.InlineQueryResultArticle.CONSTRUCTOR)
                    resultId = ((TdApi.InlineQueryResultArticle) result).id;
                else if(result.getConstructor() == TdApi.InlineQueryResultVideo.CONSTRUCTOR)
                    resultId = ((TdApi.InlineQueryResultVideo) result).id;
                else if(result.getConstructor() == TdApi.InlineQueryResultPhoto.CONSTRUCTOR)
                    resultId = ((TdApi.InlineQueryResultPhoto) result).id;
                else if(result.getConstructor() == TdApi.InlineQueryResultCachedPhoto.CONSTRUCTOR)
                    resultId = ((TdApi.InlineQueryResultCachedPhoto) result).id;
                else if(result.getConstructor() == TdApi.InlineQueryResultAnimatedGif.CONSTRUCTOR)
                    resultId = ((TdApi.InlineQueryResultAnimatedGif) result).id;
                else if(result.getConstructor() == TdApi.InlineQueryResultAnimatedMpeg4.CONSTRUCTOR)
                    resultId = ((TdApi.InlineQueryResultAnimatedMpeg4) result).id;
                else if(result.getConstructor() == TdApi.InlineQueryResultCachedAnimation.CONSTRUCTOR)
                    resultId = ((TdApi.InlineQueryResultCachedAnimation) result).id;
                else
                    return;
                if(delegate != null)
                    delegate.didResultSelected(res.getQueryResultsId(), resultId);
            }

            @Override
            public void loadMore() {
                if(delegate != null)
                    delegate.loadMore();
            }
        });
        initViewSettings();
    }
    @Override
    protected void onViewReplaced() {
        initViewSettings();
    }
    private void initViewSettings() {
        if(getView() == null)
            return;
        BaseListView list = (BaseListView) getView();
        list.setMotionEventSplittingEnabled(false);
        list.setAdapter(resultsListAdapter);
    }
    @Override
    public void onFragmentPaused() {
    }
    @Override
    public void onFragmentResumed() {
    }
    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }
    public void clearList()
    {
        results.clear();
        resultsListAdapter.notifyDataSetChanged();
    }
    public void newQueryResultsReceived(List<InlineQueryResultWrap> newResults, boolean fromLoadMore)
    {
        /*int currentConstructor = !results.isEmpty() ? results.get(0).getConstructor() : 0;
        int newConstructor = !newResults.isEmpty() ? newResults.get(0).getConstructor() : 0;*/
        if(!fromLoadMore)
        {
            results.clear();
            results.addAll(newResults);
            resultsListAdapter.notifyDataSetChanged();
        }
        else
        {
            int startPos = results.size();
            results.addAll(newResults);
            resultsListAdapter.notifyItemRangeInserted(startPos, newResults.size());
        }
    }
}
