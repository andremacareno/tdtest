package com.andremacareno.tdtest.viewcontrollers;

import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.fragments.ContactsFragment;
import com.andremacareno.tdtest.listadapters.ContactsAdapter;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.updatelisteners.UpdateUserProcessor;
import com.andremacareno.tdtest.views.ContactsListView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ConcurrentHashMap;

import gnu.trove.set.hash.TIntHashSet;

/**
 * Created by andremacareno on 04/04/15.
 */
public class ContactsListViewController extends ViewController {
    //statement variables
    protected final String TAG = "ContactsListController";
    protected final Object lockObject = new Object();
    protected RecyclerView.Adapter adapter;

    //update handlers
    protected PauseHandler listReceiver;

    //data model
    protected ArrayList<UserModel> data;
    protected ConcurrentHashMap<Integer, WeakReference<UserModel>> dataMap;

    //update observers
    protected UpdateUserProcessor updateUserProcessor;
    protected TIntHashSet ignoreSet = null;
    private ContactsFragment.ContactListDelegate contactListDelegate;
    public ContactsListViewController(View v, int[] ignoreIds)
    {
        super(v);
        if(ignoreIds != null)
            this.ignoreSet = new TIntHashSet(ignoreIds);
        init();
    }
    public ContactsListViewController(View v) {
        super(v);
        init();
    }
    public void setContactListDelegate(ContactsFragment.ContactListDelegate delegateRef)
    {
        this.contactListDelegate = delegateRef;
    }
    @Override
    protected void onViewReplaced() {
        if(getView() != null) {
            initViewSettings();
        }
    }
    public void removeObservers()
    {
        updateUserProcessor.removeObservers();
    }
    public void onFragmentPaused()
    {
        try
        {
            listReceiver.pause();
            updateUserProcessor.performPauseObservers();
        }
        catch(Exception ignored) {}
    }
    public void onFragmentResumed()
    {
        try
        {
            listReceiver.resume();
            updateUserProcessor.performResumeObservers();
        }
        catch(Exception ignored) {}
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        updateUserProcessor.attachObservers(service);
    }

    public ArrayList<UserModel> getData() { return this.data; }
    public ConcurrentHashMap<Integer, WeakReference<UserModel>> getDataMap() { return this.dataMap; }

    private void init()
    {
        this.data = new ArrayList<UserModel>();
        this.dataMap = new ConcurrentHashMap<Integer, WeakReference<UserModel>>();
        this.adapter = getAdapter();

        if(getView() != null) {
            initViewSettings();
            initHandlers();
            initObservers();
            loadContacts();
        }
    }
    private void initViewSettings()
    {
        ContactsListView v = (ContactsListView) getView();
        v.setAdapter(adapter);
        v.setMotionEventSplittingEnabled(false);
    }
    private void initHandlers()
    {
        this.listReceiver = new ListReceiver(this);
    }
    private void initObservers()
    {
        this.updateUserProcessor = new UpdateUserProcessor() {
            private final String updateUserKey = Constants.CONTACTLIST_USER_DATA_OBSERVER;
            private final String updateStatusKey = Constants.CONTACTLIST_USER_STATUS_OBSERVER;
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                if(constructor == TdApi.UpdateUser.CONSTRUCTOR)
                    return updateUserKey;
                return updateStatusKey;
            }

            @Override
            protected boolean shouldObserveUpdateUserStatus() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateUserBlocked() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateUser() {
                return true;
            }

            @Override
            protected void processUpdateInBackground(TdApi.Update upd) {
                if(upd.getConstructor() == TdApi.UpdateUser.CONSTRUCTOR) {
                    if(!dataMap.containsKey(((TdApi.UpdateUser) upd).user.id))
                        return;
                    dataMap.get(((TdApi.UpdateUser) upd).user.id).get().updateUser(((TdApi.UpdateUser) upd).user);
                }
                else if(upd.getConstructor() == TdApi.UpdateUserStatus.CONSTRUCTOR) {
                    if(!dataMap.containsKey(((TdApi.UpdateUserStatus) upd).userId))
                        return;
                    dataMap.get(((TdApi.UpdateUserStatus) upd).userId).get().updateStatus(((TdApi.UpdateUserStatus) upd).status);
                }
            }

            @Override
            protected void processUpdateInUiThread(TdApi.Update upd) {
                int constructor = upd.getConstructor();
                if(constructor != TdApi.UpdateUser.CONSTRUCTOR && constructor != TdApi.UpdateUserStatus.CONSTRUCTOR)
                    return;

                int index = getIndexOfContactById(constructor == TdApi.UpdateUser.CONSTRUCTOR ? ((TdApi.UpdateUser) upd).user.id : ((TdApi.UpdateUserStatus) upd).userId);
                if(index >= 0)
                    adapter.notifyItemChanged(index);
            }
        };
    }

    private void loadContacts()
    {
        TelegramApplication.applicationHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final TdApi.GetContacts getContactsFunc = new TdApi.GetContacts();
                Client telegramClient = TG.getClientInstance();
                telegramClient.send(getContactsFunc, new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        TdApi.Contacts contacts = (TdApi.Contacts) object;
                        ArrayList<UserModel> users = new ArrayList<UserModel>();
                        for(TdApi.User u : contacts.users) {
                            if(ignoreSet != null && ignoreSet.contains(u.id))
                                continue;
                            UserModel model = new UserModel(u);
                            users.add(model);
                            dataMap.put(u.id, new WeakReference<UserModel>(model));
                        }
                        Collections.sort(users, new Comparator<UserModel>() {
                            @Override
                            public int compare(UserModel lhs, UserModel rhs) {
                                return lhs.getDisplayName().compareTo(rhs.getDisplayName());
                            }
                        });
                        Message msg = Message.obtain(listReceiver);
                        msg.obj = users;
                        msg.sendToTarget();
                    }
                });
            }
        }, 400);
    }
    public int getIndexOfContactById(int id)
    {
        if(!dataMap.containsKey(id))
            return -1;
        return data.indexOf(dataMap.get(id).get());
    }

    private void addItemsToList(ArrayList<UserModel> users)
    {
        data.addAll(users);
        adapter.notifyItemRangeInserted(0, users.size());
    }
    public void didItemClicked(int position)
    {
        UserModel u = data.get(position);
        if(contactListDelegate != null)
            contactListDelegate.onContactChoose(u);
    }
    protected RecyclerView.Adapter getAdapter()
    {
        if(this.adapter == null)
            this.adapter = new ContactsAdapter(data, this, false);
        return this.adapter;
    }

    static abstract class ContactsListHandler extends PauseHandler
    {
        protected WeakReference<ContactsListViewController> controllerRef;
        public ContactsListHandler(ContactsListViewController controller)
        {
            this.controllerRef = new WeakReference<ContactsListViewController>(controller);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }
    }
    static class ListReceiver extends ContactsListHandler
    {
        public ListReceiver(ContactsListViewController controller) {
            super(controller);
        }
        @Override
        protected void processMessage(Message message) {
            if(controllerRef.get() != null)
                controllerRef.get().addItemsToList((ArrayList<UserModel>) message.obj);
        }
    }
}
