package com.andremacareno.tdtest.viewcontrollers.passcode;

import android.content.SharedPreferences;
import android.view.View;
import android.widget.TextView;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.views.PasscodeView;
import com.eftimoff.patternview.PatternView;

/**
 * Created by andremacareno on 02/08/15.
 */
public class PasscodePatternController extends PasscodeViewController{
    private View telegramLogo;
    private TextView helpTextView;
    private PatternView patternView;
    private PatternViewController patternViewController;
    public PasscodePatternController(PasscodeView view, Mode mode) {
        super(view, mode);
    }
    @Override
    protected void init()
    {
        telegramLogo = viewRef.findViewById(R.id.logo);
        helpTextView = (TextView) viewRef.findViewById(R.id.help);
        patternView = (PatternView) viewRef.findViewById(R.id.pattern);
        patternViewController = new PatternViewController(this);
        patternView.setOnPatternDetectedListener(patternViewController);
    }
    @Override
    protected void savePasscode(SharedPreferences.Editor prefEdit) {
        prefEdit.putInt(Constants.PREFS_PASSCODE_LOCK_TYPE, Constants.PASSCODE_TYPE_PATTERN);
        prefEdit.putString(Constants.PREFS_PASSCODE_LOCK_PATTERN, patternView.patternToString());
    }
    @Override
    public void incorrectConfirmation()
    {
        helpTextView.startAnimation(shakeAnimation);
        patternView.clearPattern();
    }

    @Override
    protected void didModeChanged() {
        if(currentMode == Mode.SET || currentMode == Mode.CONFIRMATION) {
            telegramLogo.setVisibility(View.GONE);
        }
        if(currentMode == Mode.SET) {
            helpTextView.setText(R.string.pattern_set);
        }
        else if(currentMode == Mode.CONFIRMATION) {
            helpTextView.setText(R.string.pattern_confirm);
            patternView.clearPattern();
        }
        else if(currentMode == Mode.LOCK) {
            helpTextView.setText(R.string.pattern_lock);
        }
    }
    public String getPassword()
    {
        return this.patternView.getPatternString();
    }

}
