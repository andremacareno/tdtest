package com.andremacareno.tdtest.viewcontrollers.actionview;

import android.os.Message;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.fragments.GroupChatProfileFragment;
import com.andremacareno.tdtest.updatelisteners.UpdateChatProcessor;
import com.andremacareno.tdtest.viewcontrollers.NotificationSettingsController;
import com.andremacareno.tdtest.views.actionviews.GroupChatActionView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;

/**
 * Created by Andrew on 14.05.2015.
 */
public class ChatInfoActionViewController extends ActionViewController {
    private int groupId;
    private long chatId;
    private NotificationSettingsController notificationSettingsController;
    private PauseHandler notificationSettingsHandler;
    private TdApi.NotificationSettingsForChat scopeForThisChat;
    private UpdateChatProcessor updateChatProcessor;
    private GroupChatProfileFragment.ProfileDelegate profileDelegate;
    private TextView.OnEditorActionListener doneButtonListener;
    public ChatInfoActionViewController(final int groupId, long chat_id)
    {
        this.groupId = groupId;
        this.chatId = chat_id;
        doneButtonListener = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i == EditorInfo.IME_ACTION_DONE && profileDelegate != null) {
                    profileDelegate.groupNameDoneButtonPressed();
                    return true;
                }
                return false;
            }
        };
        this.scopeForThisChat = new TdApi.NotificationSettingsForChat(chatId);
        updateChatProcessor = new UpdateChatProcessor() {
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                return String.format("ChatInfoActionView_%d_constructor_%d_observer", chatId, constructor);
            }

            @Override
            protected boolean shouldObserveUpdateGroup() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateTitle() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateReplyMarkup() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateReadInbox() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateReadOutbox() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateChat() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateChatPhoto() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateChatOrder() {
                return false;
            }

            @Override
            protected void processUpdateInBackground(TdApi.Update upd) {

            }

            @Override
            protected void processUpdateInUiThread(TdApi.Update upd) {
                if(upd == null)
                    return;
                if(upd.getConstructor() == TdApi.UpdateGroup.CONSTRUCTOR)
                {
                    TdApi.UpdateGroup cast = (TdApi.UpdateGroup) upd;
                    if(cast.group.id != groupId)
                        return;
                    GroupChatActionView v = (GroupChatActionView) getActionView();
                    v.chatView.updateOnlineCount();
                    if(profileDelegate != null)
                        profileDelegate.refreshAdminsCount();
                }
                if(upd.getConstructor() == TdApi.UpdateChatTitle.CONSTRUCTOR)
                {
                    TdApi.UpdateChatTitle cast = (TdApi.UpdateChatTitle) upd;
                    if(cast.chatId != chatId)
                        return;
                    GroupChatActionView v = (GroupChatActionView) getActionView();
                    v.chatView.updateTitle(cast.title);
                }
                if(upd.getConstructor() == TdApi.UpdateChatPhoto.CONSTRUCTOR)
                {
                    TdApi.UpdateChatPhoto cast = (TdApi.UpdateChatPhoto) upd;
                    if(cast.chatId != chatId)
                        return;
                    GroupChatActionView v = (GroupChatActionView) getActionView();
                    v.chatView.updatePhoto(CommonTools.isLowDPIScreen() ? cast.photo.small : cast.photo.big);
                }
            }
        };
    }
    @Override
    public void onFragmentPaused() {
        if(notificationSettingsController != null)
           notificationSettingsController.onFragmentPaused();
        updateChatProcessor.performPauseObservers();
    }

    @Override
    public void onFragmentResumed() {
        fillLayout();
        if(notificationSettingsController != null)
            notificationSettingsController.onFragmentResumed();
        updateChatProcessor.performResumeObservers();
    }

    @Override
    public void onActionViewRemoved() {
        updateChatProcessor.removeObservers();
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        updateChatProcessor.attachObservers(service);
        notificationSettingsHandler = new GetNotificationSettingsHandler(this, service);
        TdApi.GetNotificationSettings getNotificationSettingsReq = new TdApi.GetNotificationSettings(scopeForThisChat);
        TG.getClientInstance().send(getNotificationSettingsReq, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object != null && object.getConstructor() == TdApi.NotificationSettings.CONSTRUCTOR) {
                    Message msg = Message.obtain(notificationSettingsHandler);
                    msg.obj = object;
                    msg.sendToTarget();
                }
            }
        });
    }
    public void setEditMode(boolean editMode)
    {
        try
        {
            GroupChatActionView v = (GroupChatActionView) getActionView();
            if(editMode)
            {
                v.notificationsButton.setVisibility(View.GONE);
                v.menuButton.setVisibility(View.GONE);
            }
            else
            {
                v.notificationsButton.setVisibility(View.VISIBLE);
                v.menuButton.setVisibility(View.VISIBLE);
            }
            v.chatView.setEditMode(editMode);
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    protected void fillLayout()
    {
        try
        {
            GroupChatActionView v = (GroupChatActionView) getActionView();
            v.chatView.newGroupName.setOnEditorActionListener(doneButtonListener);
            v.chatView.editModeAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(profileDelegate != null)
                        profileDelegate.editPhotoAlertRequested();
                }
            });
            if(groupId != 0)
                v.chatView.bindChat(groupId);
            addMenu();
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    public void setProfileDelegate(GroupChatProfileFragment.ProfileDelegate delegate)
    {
        this.profileDelegate = delegate;
    }
    protected void addMenu()
    {
        GroupChatActionView v = (GroupChatActionView) getActionView();
        v.setMenuResource(R.menu.group);
    }
    private void initNotificationSettingsController(TdApi.NotificationSettings initialSettings, TelegramUpdatesHandler service)
    {

        notificationSettingsController = new NotificationSettingsController(scopeForThisChat.chatId, false, initialSettings, ((GroupChatActionView) getActionView()).notificationsButton);
        notificationSettingsController.attachObservers(service);
        ((GroupChatActionView) getActionView()).notificationsMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                notificationSettingsController.setNotificationSettingsRequested(menuItem.getItemId());
                return true;
            }
        });
    }

    public void setMenuListener(PopupMenu.OnMenuItemClickListener listener)
    {
        ((GroupChatActionView) getActionView()).defaultMenu.setOnMenuItemClickListener(listener);
    }
    private static class GetNotificationSettingsHandler extends PauseHandler
    {
        private WeakReference<ChatInfoActionViewController> controllerRef;
        private WeakReference<TelegramUpdatesHandler> serviceRef;
        public GetNotificationSettingsHandler(ChatInfoActionViewController controller, TelegramUpdatesHandler service)
        {
            this.controllerRef = new WeakReference<ChatInfoActionViewController>(controller);
            this.serviceRef = new WeakReference<TelegramUpdatesHandler>(service);
        }
        @Override
        protected boolean storeOnlyLastMessage()
        {
            return false;
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            TdApi.NotificationSettings settings = (TdApi.NotificationSettings) message.obj;

            if(controllerRef.get() == null || serviceRef.get() == null)
                return;
            controllerRef.get().initNotificationSettingsController(settings, serviceRef.get());
        }
    }
}
