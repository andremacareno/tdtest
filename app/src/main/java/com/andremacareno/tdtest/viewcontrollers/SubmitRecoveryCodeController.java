package com.andremacareno.tdtest.viewcontrollers;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.auth.AuthStateProcessor;
import com.andremacareno.tdtest.utils.AndroidUtilities;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 03/04/15.
 */
public class SubmitRecoveryCodeController {
    private EditText recoveryCode;
    private TextView helpTextView;
    private final String TAG = "CodeButtonController";
    private AuthStateProcessor.SecondaryAuthDelegate secondaryAuthDelegate;
    private Animation shakeAnimation;
    public SubmitRecoveryCodeController(EditText code, TextView help)
    {
        this.recoveryCode = code;
        this.helpTextView = help;
        this.shakeAnimation = AnimationUtils.loadAnimation(help.getContext(), R.anim.shake);
        this.secondaryAuthDelegate = new AuthStateProcessor.SecondaryAuthDelegate() {
            @Override
            public String getPhoneNumber() {
                return null;
            }

            @Override
            public void processError() {
                helpTextView.startAnimation(shakeAnimation);
            }
        };
    }
    public void didEnterCodeButtonClicked() {
        if(recoveryCode == null)
            return;
        String enteredCode = recoveryCode.getText().toString();
        if(enteredCode.length() == 0)
            return;
        TdApi.TLFunction recoverPasswordRequest = new TdApi.RecoverAuthPassword(enteredCode);
        AuthStateProcessor.getInstance().changeAuthState(recoverPasswordRequest);
        if(AndroidUtilities.isKeyboardShown(recoveryCode))
            AndroidUtilities.hideKeyboard(recoveryCode);
    }
    public void rebindToViews(EditText code, TextView help)
    {
        this.recoveryCode = code;
        this.helpTextView = help;
    }
    public AuthStateProcessor.SecondaryAuthDelegate getSecondaryAuthDelegate()
    {
        return this.secondaryAuthDelegate;
    }
}
