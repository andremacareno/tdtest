package com.andremacareno.tdtest.viewcontrollers;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.auth.AuthStateProcessor;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by andremacareno on 03/04/15.
 */
public class SubmitPasswordController {
    private EditText passwordEditText;
    private TextView helpTextView;
    private Animation shakeAnimation;
    private final String TAG = "SubmitPasswordController";
    private AuthStateProcessor.SecondaryAuthDelegate secondaryAuthDelegate;
    public SubmitPasswordController(EditText password, TextView help)
    {
        this.passwordEditText = password;
        this.helpTextView = help;
        shakeAnimation = AnimationUtils.loadAnimation(help.getContext(), R.anim.shake);
        this.secondaryAuthDelegate = new AuthStateProcessor.SecondaryAuthDelegate() {
            @Override
            public String getPhoneNumber() {
                return null;
            }

            @Override
            public void processError() {
                helpTextView.startAnimation(shakeAnimation);
            }
        };
    }
    public void didSubmitPasswordButtonClicked() {
        if(passwordEditText == null)
            return;
        String enteredPassword = passwordEditText.getText().toString();
        if(enteredPassword.length() == 0)
            return;
        TdApi.TLFunction checkPasswordReq = new TdApi.CheckAuthPassword(enteredPassword);
        AuthStateProcessor.getInstance().changeAuthState(checkPasswordReq);
    }
    public void rebindToViews(EditText password, TextView help)
    {
        this.passwordEditText = password;
        this.helpTextView = help;
    }
    public AuthStateProcessor.SecondaryAuthDelegate getSecondaryAuthDelegate()
    {
        return this.secondaryAuthDelegate;
    }
}
