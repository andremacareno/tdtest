package com.andremacareno.tdtest.viewcontrollers;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.views.AddMemberConfirmView;

import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 10/08/15.
 */
public class AddMemberListViewController extends ContactsListViewController {
    private Runnable addMemberCallback = null;
    private long chatId;
    private WeakReference<Activity> activityRef;
    private boolean channelInvite;
    public AddMemberListViewController(View v, int[] ignoreIds, boolean channelInvite) {
        super(v, ignoreIds);
        this.channelInvite = channelInvite;
    }

    @Override
    public void didItemClicked(int position)
    {
        if(position < 0)
            return;
        UserModel u = data.get(position);
        final int userId = u.getId();
        if(addMemberCallback != null)
        {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "adding participant");
            if(channelInvite)
            {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "adding participant to channel");
                TdApi.AddChatParticipant addChatParticipantReq = new TdApi.AddChatParticipant(chatId, userId, 0);
                TG.getClientInstance().send(addChatParticipantReq, TelegramApplication.dummyResultHandler);
                TdApi.ChangeChatParticipantRole changeRole = new TdApi.ChangeChatParticipantRole(chatId, userId, Constants.PARTICIPANT_GENERAL);
                TG.getClientInstance().send(changeRole, TelegramApplication.dummyResultHandler);
                addMemberCallback.run();
                return;
            }
            Activity act = activityRef != null && activityRef.get() != null ? activityRef.get() : null;
            AlertDialog.Builder adb = new AlertDialog.Builder(act);
            final AddMemberConfirmView confirmView = new AddMemberConfirmView(adb.getContext(), u.getDisplayName());
            adb.setView(confirmView);
            adb.setTitle(R.string.add_member);
            adb.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            adb.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    TdApi.AddChatParticipant addChatParticipantReq = new TdApi.AddChatParticipant(chatId, userId, confirmView.getForwardNumber());
                    TG.getClientInstance().send(addChatParticipantReq, TelegramApplication.dummyResultHandler);
                    TdApi.ChangeChatParticipantRole changeRole = new TdApi.ChangeChatParticipantRole(chatId, userId, Constants.PARTICIPANT_GENERAL);
                    TG.getClientInstance().send(changeRole, TelegramApplication.dummyResultHandler);
                    addMemberCallback.run();
                }
            });
            adb.create().show();
        }
    }
    public void setActivityRef(Activity act)
    {
        this.activityRef = new WeakReference<Activity>(act);
    }
    public void setChatId(long chatId)
    {
        this.chatId = chatId;
    }
    public void setAddMemberCallback(Runnable runnable)
    {
        this.addMemberCallback = runnable;
    }
}
