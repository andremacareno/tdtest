package com.andremacareno.tdtest.viewcontrollers;

import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.listadapters.ExactlyHeightLayoutManager;
import com.andremacareno.tdtest.listadapters.RecentInlineBotsAdapter;
import com.andremacareno.tdtest.models.InlineBotShortModel;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.views.BotCmdListView;
import com.andremacareno.tdtest.views.ComposeMessageView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

/**
 * Created by andremacareno on 17/04/15.
 */
public class RecentInlineBotsListController {

    private BotCmdListView commandListView;

    private RecentInlineBotsAdapter cmdListAdapter;

    private final String TAG = "InlineBotsListCtrl";
    private ArrayList<InlineBotShortModel> commands = new ArrayList<>();
    private ComposeMessageView.ComposeViewDelegate delegate;

    public RecentInlineBotsListController(BotCmdListView cmdListView, ComposeMessageView.ComposeViewDelegate delegateRef) {
        this.commandListView = cmdListView;
        this.delegate = delegateRef;
        init();
    }

    private void init() {
        TdApi.GetRecentInlineBots getRecentInlineBots = new TdApi.GetRecentInlineBots();

        TG.getClientInstance().send(getRecentInlineBots, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object == null || object.getConstructor() != TdApi.Users.CONSTRUCTOR)
                    return;
                TdApi.Users cast = (TdApi.Users) object;
                final ArrayList<InlineBotShortModel> inlineBots = new ArrayList<InlineBotShortModel>();
                try {
                    for (TdApi.User u : cast.users) {
                        InlineBotShortModel bot = new InlineBotShortModel(u);
                        inlineBots.add(bot);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                TelegramApplication.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        commands.addAll(inlineBots);
                        cmdListAdapter.notifyDataSetChanged();
                        updateListHeight();
                    }
                });
            }
        });

        this.cmdListAdapter = new RecentInlineBotsAdapter(commands);
        this.cmdListAdapter.setCommandSelectionCallback(new RecentInlineBotsAdapter.CommandSelectionListener() {
            @Override
            public void onCommandSelected(InlineBotShortModel bot) {
                if(delegate != null)
                    delegate.inlineBotSelected(bot);
            }
        });
        initViewSettings();
    }

    public void replaceView(BotCmdListView cmdList) {
        this.commandListView = cmdList;
        onViewReplaced();
    }

    protected void onViewReplaced() {
        initViewSettings();
    }

    private void initViewSettings() {
        if (commandListView != null) {
            commandListView.setAdapter(cmdListAdapter);
            commandListView.setMotionEventSplittingEnabled(false);
        }
        updateListHeight();
    }

    private void updateListHeight() {
        int cmdListSize = commands.size();
        ExactlyHeightLayoutManager lm = (ExactlyHeightLayoutManager) commandListView.getLayoutManager();
        int totalHeight = AndroidUtilities.dp(38) * cmdListSize;
        int maxHeight = AndroidUtilities.dp(128);
        lm.setSpecifiedHeight(Math.min(totalHeight, maxHeight));
    }

    public void onFragmentPaused() {
    }

    public void onFragmentResumed() {
    }

    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
    }

    public void removeObservers() {
    }

    public int getCmdListSize() {
        return this.commands.size();
    }
}
