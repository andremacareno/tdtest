package com.andremacareno.tdtest.viewcontrollers;

import android.view.View;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.listadapters.MessageMenuAdapter;
import com.andremacareno.tdtest.models.MessageModel;
import com.andremacareno.tdtest.models.TelegramAction;
import com.andremacareno.tdtest.views.MessageActionsListView;

import java.util.ArrayList;

/**
 * Created by andremacareno on 17/04/15.
 */
public class MessageMenuListController extends ViewController{
    private final String TAG = "MessageMenuController";
    private TelegramAction chosenAction = null;
    private MessageMenuAdapter adapter;
    private MessageActionsListView.MessageActionsDelegate delegate;
    private MessageModel attachedMsg;
    private final ArrayList<TelegramAction> actions = new ArrayList<>();
    private final TelegramAction REPLY = new TelegramAction(TelegramAction.MESSAGE_REPLY, TelegramApplication.sharedApplication().getString(R.string.reply), R.drawable.ic_reply_gray);
    private final TelegramAction SHARE = new TelegramAction(TelegramAction.MESSAGE_SHARE, TelegramApplication.sharedApplication().getString(R.string.share), R.drawable.ic_share_gray);
    private final TelegramAction COPY = new TelegramAction(TelegramAction.MESSAGE_COPY, TelegramApplication.sharedApplication().getString(R.string.copy), R.drawable.ic_copy_gray);
    private final TelegramAction SELECT = new TelegramAction(TelegramAction.MESSAGE_SELECT, TelegramApplication.sharedApplication().getString(R.string.select), R.drawable.ic_select_gray);
    private final TelegramAction DELETE = new TelegramAction(TelegramAction.MESSAGE_DELETE, TelegramApplication.sharedApplication().getString(R.string.delete), R.drawable.ic_delete_gray);
    public MessageMenuListController(View v) {
        super(v);
        init();
    }
    private void init()
    {
        this.adapter = new MessageMenuAdapter(this, actions);
        initViewSettings();
    }
    @Override
    protected void onViewReplaced() {
        if(getView() != null) {
            initViewSettings();
        }
    }
    public MessageMenuAdapter getAdapter() { return this.adapter; }
    private void initViewSettings() {
        MessageActionsListView v = (MessageActionsListView) getView();
        v.setNewElementCount(actions.size());
        v.setAdapter(adapter);
        v.setMotionEventSplittingEnabled(false);
    }

    @Override
    public void onFragmentPaused() {
    }

    @Override
    public void onFragmentResumed() {
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }

    public void didActionChosen(TelegramAction act)
    {
        this.chosenAction = act;
        if(delegate != null)
            delegate.onActionChosen();
    }
    public void continueProcessing()
    {
        if(delegate != null && attachedMsg != null && chosenAction != null)
        {
            if(chosenAction.getAction() == TelegramAction.MESSAGE_REPLY)
                delegate.onReplyRequested(attachedMsg);
            else if(chosenAction.getAction() == TelegramAction.MESSAGE_SHARE)
                delegate.onShareRequested(attachedMsg);
            else if(chosenAction.getAction() == TelegramAction.MESSAGE_COPY)
                delegate.onCopyRequested(attachedMsg);
            else if(chosenAction.getAction() == TelegramAction.MESSAGE_SELECT)
                delegate.onSelectRequested(attachedMsg);
            else if(chosenAction.getAction() == TelegramAction.MESSAGE_DELETE)
                delegate.onDeleteRequested(attachedMsg);
        }
        chosenAction = null;
        attachedMsg = null;
    }

    public void setDelegate(MessageActionsListView.MessageActionsDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    public void attachMessage(MessageModel msg)
    {
        this.attachedMsg = msg;
        if(delegate == null)
            return;
        actions.clear();
        if(delegate.canSendMessage())
            actions.add(REPLY);
        actions.add(SHARE);
        if(delegate.canCopyMessage(msg))
            actions.add(COPY);
        actions.add(SELECT);
        if(delegate.canDeleteMessage(msg))
            actions.add(DELETE);
        if(getView() != null && getView() instanceof MessageActionsListView)
        {
            ((MessageActionsListView) getView()).setNewElementCount(actions.size());
        }
        adapter.notifyDataSetChanged();
    }
}
