package com.andremacareno.tdtest.viewcontrollers;

import android.os.Message;
import android.os.Process;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.AudioPlaybackService;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.ChannelCache;
import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.MessageModelFactory;
import com.andremacareno.tdtest.PauseHandler;
import com.andremacareno.tdtest.PeerType;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.UserCache;
import com.andremacareno.tdtest.fragments.BaseChatFragment;
import com.andremacareno.tdtest.listadapters.BaseChatAdapter;
import com.andremacareno.tdtest.models.AudioMessageModel;
import com.andremacareno.tdtest.models.BotHeaderMessageModel;
import com.andremacareno.tdtest.models.ContactMessageModel;
import com.andremacareno.tdtest.models.CurrentUserModel;
import com.andremacareno.tdtest.models.DateSectionMessageModel;
import com.andremacareno.tdtest.models.MessageModel;
import com.andremacareno.tdtest.models.NewMessagesMessageModel;
import com.andremacareno.tdtest.models.PseudoMessageModel;
import com.andremacareno.tdtest.models.ReplyMessageShortModel;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.tasks.GenerateMessageModelsTask;
import com.andremacareno.tdtest.tasks.GenerateMessageReplacementTask;
import com.andremacareno.tdtest.tasks.GetFullPeerTask;
import com.andremacareno.tdtest.updatelisteners.UpdateChatProcessor;
import com.andremacareno.tdtest.updatelisteners.UpdateMessageProcessor;
import com.andremacareno.tdtest.updatelisteners.UpdateNotificationsProcessor;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.views.BaseChatListView;
import com.andremacareno.tdtest.views.ScrollButtonDelegate;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import gnu.trove.TCollections;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.hash.TIntHashSet;

/**
 * Created by andremacareno on 17/04/15.
 */
public class ChatController extends ViewController{
    private static final String TAG = "BaseChatLController";
    private static final int LOADING_STEP = 10;
    private static final int ADD_TO_TOP = 0;
    private static final int ADD_TO_BOTTOM = 1;

    //statement variables
    private long chatId;
    private final boolean isChannel;
    private volatile boolean chatHistoryCleared = false;
    private final int initialLastReadInbox;
    private final int initialUnreadCount;
    private volatile int lastReadInbox;
    private volatile int lastReadOutbox;
    private volatile int unreadCount;
    private BaseChatAdapter adapter;
    //for getChatHistory()
    public int start_message;
    private volatile int last_message_on_top;
    private volatile int last_message_on_bottom;
    private final AtomicBoolean dontLoadTop;
    private final AtomicBoolean dontLoadBottom;
    private final boolean isChatWithBot;
    private volatile boolean dontAddNewMsgsBar = false;
    private volatile int indexOfNewMsgsBar = -1;
    private NotificationObserver viewProfileByLinkRequestObserver;
    private BaseChatFragment.ChatDelegate chatDelegate;
    private final TIntList removedMessages = TCollections.synchronizedList(new TIntArrayList());
    private final TIntObjectHashMap<TdApi.MessageContent> needUpdateContent = new TIntObjectHashMap<>();
    private volatile int offsetAdd = 0;
    private TdApi.BotInfoGeneral botInfo = null;
    private volatile int chatTopMsgId = 0;
    private final TIntHashSet undeletableSelection = new TIntHashSet();
    private TIntObjectHashMap<String> fwdFrom = new TIntObjectHashMap<>();
    private TIntIntHashMap fwdFromCount = new TIntIntHashMap();
    private volatile boolean disableUpdateNewMessage = true;
    private int highlightMsg = 0;
    private int previousHighlight = 0;
    TdApi.GetChatHistory getChatHistoryFunc_top =  new TdApi.GetChatHistory(0, 0, 0, LOADING_STEP);
    TdApi.GetChatHistory getChatHistoryFunc_bottom = new TdApi.GetChatHistory(0, 0, -LOADING_STEP, LOADING_STEP);
    //async bridges
    private PauseHandler listReceiver;
    private PauseHandler newMessageReceiver;
    private PauseHandler errorHandler;

    //data model
    private final List<MessageModel> data = new ArrayList<MessageModel>();
    private final TIntObjectHashMap<MessageModel> dataMap = new TIntObjectHashMap<>();

    //update observers
    private HistoryLoadingThread historyLoading;
    private ScrollButtonDelegate scrollButtonDelegate = null;
    private UpdateMessageProcessor updateMsgProcessor = null;
    private UpdateChatProcessor updateChatProcessor = null;
    private UpdateNotificationsProcessor notificationSettingsProcessor = null;
    private UpdateMessageProcessor.UpdateMessageDelegate bgThreadDelegate, uiThreadDelegate;

    //for date separator adding algorithm
    final Date lastMsgDate = new Date();
    final Date currentMsgDate = new Date();

    //jumpTo() additional variables
    private final AtomicBoolean dismissGetHistory = new AtomicBoolean(false);

    //chat search
    private ChatSearchController searchController;
    private ChatSearchController.ChatSearchDelegate searchDelegate;

    @Override
    public void onFragmentPaused()
    {
        try
        {
            newMessageReceiver.pause();
            errorHandler.pause();
            listReceiver.pause();
            updateMsgProcessor.performPauseObservers();
            updateChatProcessor.performPauseObservers();
            notificationSettingsProcessor.performPauseObservers();
            scrollButtonDelegate = null;
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onFragmentResumed()
    {
        try
        {
            errorHandler.resume();
            newMessageReceiver.resume();
            listReceiver.resume();
            updateMsgProcessor.performResumeObservers();
            updateChatProcessor.performResumeObservers();
            notificationSettingsProcessor.performResumeObservers();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
        initHandlers();
        updateMsgProcessor.attachObservers(service);
        updateChatProcessor.attachObservers(service);
        notificationSettingsProcessor.attachObservers(service);
        init();
    }

    @Override
    protected void onViewReplaced() {
        if(chatDelegate != null)
            chatDelegate.didSelectionCountChanged(adapter.getSelectionCount());
    }
    public void delSelectedMessages()
    {
        int[] arr = adapter.getSelection();
        TdApi.DeleteMessages del = new TdApi.DeleteMessages(chatId, arr);
        TG.getClientInstance().send(del, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                ChatCache.getInstance().invalidate(chatId);
                NotificationCenter.getInstance().postNotification(NotificationCenter.didChatMessageRemovedBySelf, chatId);
            }
        });
        for(int i : arr)
        {
            int index = getIndexByMessageId(i);
            if(index >= 0)
            {
                synchronized(data)
                {
                    data.remove(index);
                }
                adapter.notifyItemRemoved(index);
            }
        }
        fwdFrom.clear();
        fwdFromCount.clear();
        adapter.clearSelectionSet();
        undeletableSelection.clear();
        if(chatDelegate != null)
            chatDelegate.didSelectionCountChanged(0);
    }
    public void clearSelection(boolean withNotify)
    {
        fwdFrom.clear();
        fwdFromCount.clear();
        if(withNotify)
        {
            int[] arr = adapter.getSelection();
            for(int i : arr)
            {
                int index = getIndexByMessageId(i);
                if(index >= 0)
                {
                    synchronized(data)
                    {
                        MessageModel msg = data.get(index);
                        adapter.toggleSelection(msg.getMessageId(), index);
                    }
                }
            }
        }
        adapter.clearSelectionSet();
        undeletableSelection.clear();
    }
    public void stopLoadingThread()
    {
        if(historyLoading != null)
            historyLoading.interrupt();
    }
    public void removeObservers()
    {
        try
        {
            updateMsgProcessor.removeObservers();
            updateChatProcessor.removeObservers();
            notificationSettingsProcessor.removeObservers();
            NotificationCenter.getInstance().removeObserver(viewProfileByLinkRequestObserver);
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    public ChatController(boolean chatWithBot, long chatId, int lastReadInbox, int lastReadOutbox, int startMessageId, int unreadCount, View v, boolean fromSearch, int lastMessageId) {
        super(v);
        TdApi.Chat chatObj = ChatCache.getInstance().getChatById(chatId);
        this.isChannel = chatObj == null ? false : chatObj.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR;
        this.searchDelegate = new ChatSearchController.ChatSearchDelegate() {
            @Override
            public void requestScrollTo(int messageId) {
                requestHighlight(messageId);
                jumpTo(messageId);
            }

            @Override
            public void onNoResults() {
                if(chatDelegate != null)
                    chatDelegate.didChatSearchHaveNoResults();
            }

            @Override
            public void searchAboveFinished() {
                if(chatDelegate != null)
                    chatDelegate.didChatSearchReachedEndAbove();
            }

            @Override
            public void searchBelowFinished() {
                if(chatDelegate != null)
                    chatDelegate.didChatSearchReachedEndBelow();
            }
        };
        this.searchController = new ChatSearchController(chatId, searchDelegate);
        this.chatTopMsgId = lastMessageId;
        this.isChatWithBot = chatWithBot;
        this.historyLoading = new HistoryLoadingThread();
        this.historyLoading.start();
        this.chatId = chatId;
        this.initialLastReadInbox = lastReadInbox;
        this.initialUnreadCount = unreadCount;
        this.lastReadInbox = lastReadInbox;
        this.lastReadOutbox = lastReadOutbox;
        this.unreadCount = unreadCount;
        if(initialUnreadCount > 0 && initialLastReadInbox != 0 && !fromSearch) {
            if(BuildConfig.DEBUG)
                Log.d("ChatController", String.format("start from first unread msg; unreadCount = %d", initialUnreadCount));
            this.start_message = this.last_message_on_bottom = this.last_message_on_top = lastReadInbox;
        }
        else {
            dontAddNewMsgsBar = true;
            this.start_message = this.last_message_on_bottom = this.last_message_on_top = startMessageId;
        }
        getChatHistoryFunc_bottom.chatId = getChatHistoryFunc_top.chatId = this.chatId;
        if(start_message != 0)
            getChatHistoryFunc_bottom.fromId = getChatHistoryFunc_top.fromId = start_message;
        else
            getChatHistoryFunc_bottom.fromId = getChatHistoryFunc_top.fromId = 1000000000;
        this.dontLoadBottom = new AtomicBoolean(false);
        this.dontLoadTop = new AtomicBoolean(false);
    }
    public void setScrollButtonDelegate(ScrollButtonDelegate delegate) {
        this.scrollButtonDelegate = delegate;
    }
    public ScrollButtonDelegate getScrollButtonDelegate() {
        return this.scrollButtonDelegate;
    }
    public void scrollToBottom()
    {
        if(getView() != null)
            ((BaseChatListView) getView()).scrollToPosition(adapter.getItemCount() - 1);
    }
    private void scrollToMsgId(int messageId)
    {
        int index = getIndexByMessageId(messageId);
        if(BuildConfig.DEBUG)
            Log.d(TAG, String.format("index = %d", index));
        if(getView() != null && index >= 0) {
            if(highlightMsg == messageId) {
                int previousHightlightIndex = previousHighlight != 0 ? getIndexByMessageId(previousHighlight) : -1;
                if(previousHightlightIndex >= 0)
                    adapter.notifyItemChanged(previousHightlightIndex);
                adapter.notifyItemChanged(index);
            }
            int firstVisible = ((LinearLayoutManager)((BaseChatListView) getView()).getLayoutManager()).findFirstVisibleItemPosition();
            int lastVisible = ((LinearLayoutManager)((BaseChatListView) getView()).getLayoutManager()).findLastVisibleItemPosition();
            if(firstVisible > lastVisible)
            {
                int tmp = firstVisible;
                firstVisible = lastVisible;
                lastVisible = tmp;
            }
            int diff = 0;
            if(index > lastVisible)
                diff = index - lastVisible;
            else if(index < firstVisible)
                diff = firstVisible - index;

            if(diff <= 15)
                ((BaseChatListView) getView()).smoothScrollToPosition(index);
            else
                ((BaseChatListView) getView()).scrollToPosition(index);
        }
    }
    private void init()
    {
        if(adapter == null)
            this.adapter = new BaseChatAdapter(data, this);
        BaseChatListView v = (BaseChatListView) getView();
        if(v != null)
            initViewSettings();
    }
    public BaseChatAdapter getAdapter() { return this.adapter; }
    private void initViewSettings() {
        BaseChatListView v = (BaseChatListView) getView();
        v.setMotionEventSplittingEnabled(false);
        synchronized(data)
        {
            if(data.isEmpty())
                checkLoading(start_message);
            else
                v.setAdapter(adapter);
        }
    }
    private void initDelegates()
    {
        bgThreadDelegate = new UpdateMessageProcessor.UpdateMessageDelegate() {
            @Override
            public void updateMessageContent(TdApi.UpdateMessageContent upd) {
            }

            @Override
            public void updateMessageDate(TdApi.UpdateMessageDate upd) {
                synchronized(data)
                {
                    synchronized(dataMap)
                    {
                        if(dataMap.containsKey(upd.messageId) && chatId == upd.chatId)
                        {
                            MessageModel msgRef = dataMap.get(upd.messageId);
                            msgRef.updateDate(upd.newDate);
                        }
                    }
                }
            }

            @Override
            public void updateMessageId(TdApi.UpdateMessageId upd) {
            }

            @Override
            public void updateMessageSendFailed(TdApi.UpdateMessageSendFailed upd) {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, upd.toString());
            }
            @Override
            public void updateNewMessage(final TdApi.UpdateNewMessage upd) {
            }
            @Override
            public void updateDelMessages(TdApi.UpdateDeleteMessages upd) {
                synchronized(data) {
                    synchronized (dataMap) {
                        for (int msg_id : upd.messageIds) {
                            int index = getIndexByMessageId(msg_id);
                            if (index != -1) {
                                boolean removeWithPrevious = false;
                                if (data.size() == index + 1 && data.get(index - 1) instanceof DateSectionMessageModel)
                                    removeWithPrevious = true;
                                if (data.size() == index + 2 && data.get(index - 1) instanceof DateSectionMessageModel)
                                    removeWithPrevious = true;
                                synchronized (removedMessages) {
                                    data.remove(index);
                                    dataMap.remove(msg_id);
                                    removedMessages.add(index);
                                    if (removeWithPrevious) {
                                        data.remove(index - 1);
                                        removedMessages.add(index - 1);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void updateViewsCount(TdApi.UpdateMessageViews upd) {
                synchronized(data)
                {
                    synchronized(dataMap)
                    {
                        if(dataMap.containsKey(upd.messageId)) {
                            MessageModel msgRef = dataMap.get(upd.messageId);
                            msgRef.updateViews(upd);
                        }
                    }
                }
            }
        };
        uiThreadDelegate = new UpdateMessageProcessor.UpdateMessageDelegate() {
            @Override
            public void updateMessageContent(final TdApi.UpdateMessageContent upd) {
                if(chatId != upd.chatId)
                    return;
                synchronized(data)
                {
                    synchronized(dataMap)
                    {
                        if(dataMap.containsKey(upd.messageId) == false)
                            return;
                        MessageModel msg = dataMap.get(upd.messageId);
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, "Updating content");
                        boolean replaceMsg = msg.getSuitableConstructor() != upd.newContent.getConstructor();
                        if(!replaceMsg) {
                            msg.updateContent(upd.newContent);
                            int index = getIndexByMessageId(upd.messageId);
                            if(index >= 0) {
                                adapter.notifyItemChanged(index);
                            }
                        }
                        else {
                            if(upd.messageId < 1000000000)
                            {
                                GenerateMessageReplacementTask replace = new GenerateMessageReplacementTask(upd.chatId, upd.messageId, upd.newContent);
                                replace.onComplete(new OnTaskCompleteListener() {
                                    @Override
                                    public void taskComplete(AsyncTaskManTask task) {
                                        try
                                        {
                                            if(BuildConfig.DEBUG)
                                                Log.d(TAG, "replacing message due to different UpdateMessageContent constructor");
                                            final MessageModel newMsg = ((GenerateMessageReplacementTask) task).getResult();
                                            if(newMsg == null) {
                                                if(BuildConfig.DEBUG)
                                                    Log.d(TAG, "received NULL");
                                                return;
                                            }

                                            TelegramApplication.applicationHandler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    synchronized(data)
                                                    {
                                                        synchronized(dataMap)
                                                        {
                                                            int index = getIndexByMessageId(upd.messageId);
                                                            if(index < 0) {
                                                                if(BuildConfig.DEBUG)
                                                                    Log.d(TAG, "message not found");
                                                                return;
                                                            }
                                                            dataMap.put(upd.messageId, newMsg);
                                                            data.remove(index);
                                                            data.add(index, newMsg);
                                                            adapter.notifyItemChanged(index);
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                        catch(Exception ignored) {}
                                    }
                                });
                                replace.addToQueue();
                            }
                            else {
                                synchronized(needUpdateContent)
                                {
                                    needUpdateContent.put(upd.messageId, upd.newContent);
                                    if(BuildConfig.DEBUG)
                                        Log.d(TAG, "inserted to needUpdateContent");
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void updateMessageDate(TdApi.UpdateMessageDate upd) {
                if(chatId != upd.chatId)
                    return;
                synchronized(data)
                {
                    synchronized(dataMap)
                    {
                        if(dataMap.containsKey(upd.messageId) == false)
                            return;
                        MessageModel msg = dataMap.get(upd.messageId);
                        msg.updateDate(upd.newDate);
                        int index = getIndexByMessageId(upd.messageId);
                        if(index >= 0)
                            adapter.notifyItemChanged(index);
                    }
                }
            }

            @Override
            public void updateMessageId(final TdApi.UpdateMessageId upd) {
                if(chatId != upd.chatId) {
                    return;
                }
                synchronized(data)
                {
                    synchronized(dataMap)
                    {
                        if(!dataMap.containsKey(upd.oldId)) {
                            jumpTo(upd.newId);
                            return;
                        }
                        else
                        {
                            MessageModel msgRef = dataMap.get(upd.oldId);
                            msgRef.markSent();
                            msgRef.updateId(upd.newId);
                            if(chatTopMsgId == upd.oldId)
                                chatTopMsgId = upd.newId;
                            dataMap.remove(upd.oldId);
                            dataMap.put(upd.newId, msgRef);
                            if(last_message_on_bottom == upd.oldId)
                                last_message_on_bottom = upd.newId;
                        }
                    }
                }
                adapter.updateId(upd.oldId, upd.newId);
                synchronized(needUpdateContent)
                {
                    if(!needUpdateContent.containsKey(upd.oldId)) {
                        return;
                    }
                    TdApi.MessageContent content = needUpdateContent.get(upd.oldId);
                    needUpdateContent.remove(upd.oldId);
                    GenerateMessageReplacementTask replace = new GenerateMessageReplacementTask(upd.chatId, upd.newId, content);
                    replace.onComplete(new OnTaskCompleteListener() {
                        @Override
                        public void taskComplete(AsyncTaskManTask task) {
                            try
                            {
                                if(BuildConfig.DEBUG)
                                    Log.d(TAG, "replacing message due to different UpdateMessageContent constructor");
                                final MessageModel newMsg = ((GenerateMessageReplacementTask) task).getResult();
                                if(newMsg == null)
                                    return;
                                TelegramApplication.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        synchronized(data)
                                        {
                                            synchronized(dataMap)
                                            {
                                                int index = getIndexByMessageId(upd.newId);
                                                if(index < 0)
                                                    return;
                                                dataMap.put(upd.newId, newMsg);
                                                data.remove(index);
                                                data.add(index, newMsg);
                                                adapter.notifyItemChanged(index);
                                            }
                                        }
                                    }
                                });
                            }
                            catch(Exception ignored) {}
                        }
                    });
                    replace.addToQueue();
                }
                /*int index = getIndexByMessageId(upd.newId);
                if(index >= 0)
                    adapter.notifyItemChanged(index);
                else
                {
                    index = getIndexByMessageId(upd.oldId);
                    if(index >= 0)
                        adapter.notifyItemChanged(index);
                }*/
            }

            @Override
            public void updateMessageSendFailed(TdApi.UpdateMessageSendFailed upd) {
                if(chatId != upd.chatId)
                    return;
                synchronized(data)
                {
                    synchronized(dataMap)
                    {
                        if(dataMap.containsKey(upd.messageId) == false)
                            return;
                        MessageModel msg = dataMap.get(upd.messageId);
                        int index = getIndexByMessageId(upd.messageId);
                        msg.updateDate(0);
                        if(index >= 0)
                            adapter.notifyItemChanged(index);
                    }
                }
            }

            @Override
            public void updateNewMessage(final TdApi.UpdateNewMessage upd) {
                if(chatId != upd.message.chatId) {
                    if(BuildConfig.DEBUG)
                        Log.d(TAG, "wrong chatId, ignoring update");
                    return;
                }
                if(disableUpdateNewMessage)
                    return;
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "generating message model");
                GenerateMessageModelsTask t = new GenerateMessageModelsTask(upd.message, ChatController.this, ADD_TO_BOTTOM, String.format("chat%d_newmessage_%d", upd.message.chatId, upd.message.id));
                t.onComplete(new OnTaskCompleteListener() {
                    @Override
                    public void taskComplete(AsyncTaskManTask task) {
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, "message model generated");
                        try
                        {
                            synchronized(data)
                            {
                                synchronized(dataMap)
                                {
                                    if(dataMap.containsKey(upd.message.id)) {
                                        if(BuildConfig.DEBUG)
                                            Log.d(TAG, "return");
                                        return;
                                    }
                                    boolean addSeparator = needWrapSeparator(((long) upd.message.date) * 1000L, true);
                                    if(addSeparator)
                                    {
                                        PseudoMessageModel dateSeparator = new DateSectionMessageModel(upd.message.date);
                                        int index = insertNewMessage(dateSeparator, false);
                                        Message msg = Message.obtain(newMessageReceiver);
                                        msg.obj = dateSeparator;
                                        msg.what = index;
                                        msg.sendToTarget();
                                    }
                                    MessageModel model = ((GenerateMessageModelsTask) task).getMessages().get(0);
                                    int index = insertNewMessage(model, true);
                                    Message msg = Message.obtain(newMessageReceiver);
                                    msg.obj = model;
                                    msg.what = index;
                                    msg.sendToTarget();
                                    if(BuildConfig.DEBUG)
                                        Log.d(TAG, "messages inserted");
                                }
                            }
                        }
                        catch(Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                t.addToQueue();
            }

            @Override
            public void updateDelMessages(TdApi.UpdateDeleteMessages upd) {
                if(chatId != upd.chatId)
                    return;
                synchronized(removedMessages)
                {
                    //TODO potential weakness (if deleted 1000+ messages)
                    for(int i : removedMessages.toArray()) {
                        adapter.removeFromSelectionSet(i);
                        adapter.notifyItemRemoved(i);
                    }
                    removedMessages.clear();
                }
            }

            @Override
            public void updateViewsCount(TdApi.UpdateMessageViews upd) {
                if(chatId != upd.chatId)
                    return;
                synchronized(data)
                {
                    synchronized(dataMap)
                    {
                        if(dataMap.containsKey(upd.messageId) == false)
                            return;
                        MessageModel msg = dataMap.get(upd.messageId);
                        int index = getIndexByMessageId(upd.messageId);
                        if(index >= 0 && msg.messageInChannelChat() || msg.forwardedFromChannel())
                            adapter.notifyItemChanged(index);
                    }
                }
            }
        };
    }
    public int[] getSelectionIds()
    {
        return adapter.getSelection();
    }
    public List<MessageModel> getSelection()
    {
        int[] selection = adapter.getSelection();
        MessageModel[] arr = new MessageModel[selection.length];
        int i = 0;
        for(int id : selection)
        {
            int index = getIndexByMessageId(id);
            if(index < 0)
                continue;
            synchronized(data)
            {
                MessageModel msg = data.get(index);
                arr[i++] = msg;
            }
        }
        return Arrays.asList(arr);
    }
    public String getFwdTitle()
    {
        int[] keys = fwdFrom.keys();
        if(keys.length == 1)
            return fwdFrom.get(keys[0]);
        else if(keys.length == 2)
            return String.format(TelegramApplication.sharedApplication().getString(R.string.fwd_title_two), fwdFrom.get(keys[0]), fwdFrom.get(keys[1]));
        else if(keys.length == 3)
            return String.format(TelegramApplication.sharedApplication().getString(R.string.fwd_title_three), fwdFrom.get(keys[0]), fwdFrom.get(keys[1]), fwdFrom.get(keys[2]));
        else if(keys.length >= 4)
            return String.format(TelegramApplication.sharedApplication().getString(R.string.fwd_title_four_and_more), fwdFrom.get(keys[0]), fwdFrom.get(keys[1]), keys.length-2);
        return "";
    }
    public void initHandlers()
    {
        try
        {
            if(listReceiver == null)
            {
                initDelegates();
                this.listReceiver = new ListReceiver(this);
                this.newMessageReceiver = new NewMessageReceiver(this);
                this.errorHandler = new ErrorHandler(this);
                this.notificationSettingsProcessor = new UpdateNotificationsProcessor() {
                    @Override
                    protected String getKeyByUpdateConstructor(int constructor) {
                        return String.format(Constants.CHATSCREEN_UPDATE_NOTIFICATION_SETTINGS_OBSERVER, chatId);
                    }

                    @Override
                    protected void processUpdateInBackground(TdApi.Update upd) {
                    }

                    @Override
                    protected void processUpdateInUiThread(TdApi.Update upd) {
                        if(upd == null || upd.getConstructor() != TdApi.UpdateNotificationSettings.CONSTRUCTOR)
                            return;
                        TdApi.UpdateNotificationSettings updCast = (TdApi.UpdateNotificationSettings) upd;
                        if(chatDelegate != null)
                            chatDelegate.didNotificationSettingsUpdated(updCast.notificationSettings.muteFor != 0, updCast.scope);
                    }
                };
                this.updateChatProcessor = new UpdateChatProcessor() {
                    @Override
                    protected String getKeyByUpdateConstructor(int constructor) {
                        if(constructor == TdApi.UpdateChatReadInbox.CONSTRUCTOR)
                            return String.format(Constants.CHATSCREEN_GENERAL_READ_INBOX_OBSERVER, chatId);
                        else if(constructor == TdApi.UpdateChatReadOutbox.CONSTRUCTOR)
                            return String.format(Constants.CHATSCREEN_GENERAL_READ_OUTBOX_OBSERVER, chatId);
                        return null;
                    }

                    @Override
                    protected boolean shouldObserveUpdateGroup() {
                        return false;
                    }

                    @Override
                    protected boolean shouldObserveUpdateTitle() {
                        return false;
                    }

                    @Override
                    protected boolean shouldObserveUpdateReplyMarkup() {
                        return false;
                    }

                    @Override
                    protected boolean shouldObserveUpdateReadInbox() {
                        return true;
                    }

                    @Override
                    protected boolean shouldObserveUpdateReadOutbox() {
                        return true;
                    }

                    @Override
                    protected boolean shouldObserveUpdateChat() {
                        return false;
                    }

                    @Override
                    protected boolean shouldObserveUpdateChatPhoto() {
                        return false;
                    }

                    @Override
                    protected boolean shouldObserveUpdateChatOrder() {
                        return false;
                    }

                    @Override
                    protected void processUpdateInBackground(TdApi.Update upd) {
                        Log.d("ChatController", "updateChatRead");
                        if(upd.getConstructor() == TdApi.UpdateChatReadInbox.CONSTRUCTOR)
                        {
                            if(((TdApi.UpdateChatReadInbox)upd).chatId != chatId)
                                return;
                            lastReadInbox = ((TdApi.UpdateChatReadInbox)upd).lastReadInboxMessageId;
                            unreadCount = ((TdApi.UpdateChatReadInbox)upd).unreadCount;
                        }
                        else if(upd.getConstructor() == TdApi.UpdateChatReadOutbox.CONSTRUCTOR)
                        {
                            if(((TdApi.UpdateChatReadOutbox)upd).chatId != chatId)
                                return;
                            lastReadOutbox = ((TdApi.UpdateChatReadOutbox)upd).lastReadOutboxMessageId;
                        }
                    }

                    @Override
                    protected void processUpdateInUiThread(TdApi.Update upd) {
                        if(upd.getConstructor() == TdApi.UpdateChatReadOutbox.CONSTRUCTOR && ((TdApi.UpdateChatReadOutbox)upd).chatId == chatId) {
                            getAdapter().updateStatusViews();
                        }
                    }
                };
                this.updateMsgProcessor = new UpdateMessageProcessor(bgThreadDelegate, uiThreadDelegate) {
                    @Override
                    protected String getKeyByUpdateConstructor(int constructor) {
                        return String.format("chatscreen_observer%d_%d", chatId, constructor);
                    }

                    @Override
                    protected boolean shouldObserveUpdateMsgContent() {
                        return true;
                    }

                    @Override
                    protected boolean shouldObserveUpdateMsgDate() {
                        return true;
                    }

                    @Override
                    protected boolean shouldObserveUpdateMsgId() {
                        return true;
                    }

                    @Override
                    protected boolean shouldObserveUpdateMsgSendFailed() {
                        return true;
                    }

                    @Override
                    protected boolean shouldObserveUpdateNewMsg() {
                        return true;
                    }

                    @Override
                    protected boolean shouldObserveUpdateDeleteMessages() {
                        return true;
                    }

                    @Override
                    protected boolean shouldObserveViewsCount() {
                        return true;
                    }
                };
                this.viewProfileByLinkRequestObserver = new NotificationObserver(NotificationCenter.didViewProfileByLinkRequested) {
                    @Override
                    public void didNotificationReceived(Notification notification) {
                        int userId = (Integer) notification.getObject();
                        didAvatarClicked(userId);
                    }
                };
                NotificationCenter.getInstance().addObserver(viewProfileByLinkRequestObserver);
            }
        }
        catch(Exception e) {e.printStackTrace();}
    }

    public ChatSearchController getSearchController() {
        return this.searchController;
    }
    public void checkLoading(final int id)
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, String.format("checkLoading: lastMessageOnBottom = %d; lastMessageOnTop = %d; id = %d", last_message_on_bottom, last_message_on_top, id));
        boolean delay;
        synchronized(data)
        {
            delay = data.isEmpty();
        }
        Runnable r = new Runnable() {
            @Override
            public void run() {
                synchronized(dontLoadBottom)
                {
                    if(last_message_on_bottom <= id && dontLoadBottom.get() == false) {
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, "addToBottom task requested");
                        dontLoadBottom.set(true);
                        historyLoading.addTask(ADD_TO_BOTTOM);
                    }
                }
                synchronized(dontLoadTop)
                {
                    if(last_message_on_top >= id && dontLoadTop.get() == false) {
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, "addToTop task requested");
                        dontLoadTop.set(true);
                        historyLoading.addTask(ADD_TO_TOP);
                    }
                }
            }
        };
        if(delay)
            TelegramApplication.applicationHandler.postDelayed(r, 400);
        else
            r.run();
    }

    private void didMessagesDownloaded(final TdApi.Messages messages, final int addingStr, int offset, final int startFrom) {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "didMessagesDownloaded");
        offsetAdd = messages.messages.length;
        if(messages.messages.length > 0)
        {
            if(addingStr == ADD_TO_BOTTOM) {
                last_message_on_bottom = messages.messages[0].id;
            }
            else if(addingStr == ADD_TO_TOP) {
                last_message_on_top = messages.messages[messages.messages.length - 1].id;
            }
        }
        //convert to models
        GenerateMessageModelsTask t = new GenerateMessageModelsTask(messages, this, addingStr, String.format("chat%d_loading_%d_%d_%d", chatId, startFrom, offset, addingStr));
        t.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(AsyncTaskManTask task) {
                if(chatHistoryCleared)
                    return;
                try
                {
                    TdApi.ViewMessages viewRequest = new TdApi.ViewMessages(chatId, ((GenerateMessageModelsTask) task).getMessageIds());
                    TG.getClientInstance().send(viewRequest, TelegramApplication.dummyResultHandler);
                    List<MessageModel> msgs = ((GenerateMessageModelsTask) task).getMessages();
                    for(MessageModel msg : msgs)
                    {
                        if(msg.isPseudoMessage() || msg.getMessageId() == 0)
                            continue;
                        synchronized(dataMap)
                        {
                            dataMap.put(msg.getMessageId(), msg);
                        }
                    }
                    int addingStrategy = ((GenerateMessageModelsTask) task).getAddingStrategy();
                    if(msgs.size() > 0)
                    {
                        boolean addSeparator;
                        synchronized(data) {
                            if(addingStrategy == ADD_TO_BOTTOM) {
                                Collections.reverse(msgs);
                                addSeparator = needWrapSeparator(((long) msgs.get(0).getIntegerDate()) * 1000L, true);
                            }
                            else
                                addSeparator = needWrapSeparator(((long) msgs.get(msgs.size()-1).getIntegerDate()) * 1000L, false);
                            if(data.isEmpty())
                                addSeparator = false;
                        }
                        if(addSeparator)
                        {
                            if(addingStrategy == ADD_TO_BOTTOM) {
                                PseudoMessageModel dateSeparator = new DateSectionMessageModel(msgs.get(0).getIntegerDate());
                                msgs.add(0, dateSeparator);
                            }
                            else {
                                synchronized(data)
                                {
                                    PseudoMessageModel dateSeparator = new DateSectionMessageModel(data.get(0).getIntegerDate());
                                    msgs.add(dateSeparator);
                                }
                            }
                        }
                        MessageModel startMessage = ((GenerateMessageModelsTask) task).getStartMessageRef();
                        if(startMessage != null && initialUnreadCount > 0 && !dontAddNewMsgsBar)
                        {
                            //add "N new messages" bar after last read msg
                            dontAddNewMsgsBar = true;
                            int index = msgs.indexOf(startMessage);
                            if(index >= 0)
                            {
                                PseudoMessageModel newMessagesSeparator = new NewMessagesMessageModel(initialUnreadCount);
                                msgs.add(index + 1, newMessagesSeparator);
                                synchronized(data)
                                {
                                    indexOfNewMsgsBar = index + 1;
                                }
                            }
                        }
                    }
                    synchronized(dismissGetHistory)
                    {
                        if(dismissGetHistory.get()) {
                            if(BuildConfig.DEBUG)
                                Log.d(TAG, "ignoring getChatHistory() result");
                            if(addingStrategy == ADD_TO_TOP)
                                synchronized(dontLoadTop)
                                {
                                    dontLoadTop.set(false);
                                }
                            else if(addingStr == ADD_TO_BOTTOM)
                                synchronized(dontLoadBottom)
                                {
                                    dontLoadBottom.set(false);
                                }
                            return;
                        }
                    }
                    Message msg = Message.obtain(listReceiver);
                    msg.arg1 = addingStrategy;
                    msg.arg2 = ((GenerateMessageModelsTask) task).getActualLength();
                    msg.obj = msgs;
                    msg.sendToTarget();
                }
                catch(Exception e) { e.printStackTrace(); }
            }
        });
        t.addToQueue();
    }
    public void updateBotInfo(TdApi.BotInfoGeneral info)
    {
        synchronized(data)
        {
            this.botInfo = info;
            if(!data.isEmpty())
            {
                MessageModel msg = data.get(0);
                if(msg instanceof BotHeaderMessageModel)
                {
                    ((BotHeaderMessageModel) msg).setBotInfo(info);
                    adapter.notifyItemChanged(0);
                }
            }
        }
    }
    private void addItemsToList(List<MessageModel> msgs, int addingStrategy, int messagesCount)
    {
        boolean checkMessageCount;
        synchronized(data)
        {
            checkMessageCount = data.isEmpty();
        }
        if(BuildConfig.DEBUG)
            Log.d(TAG, "adding items to list");
        synchronized(data)
        {
            if(msgs.isEmpty() && adapter.getItemCount() == 0 && chatDelegate != null) {
                chatDelegate.didNoMessagesInChatConfirmed();
            }
            else if(chatDelegate != null) {
                if(!isChatWithBot)
                    chatDelegate.didChatHasMessagesConfirmed();
                else
                {
                    if(addingStrategy == ADD_TO_BOTTOM && (!msgs.isEmpty() || (adapter.getItemCount() > 1 && !data.isEmpty() && !(data.get(0) instanceof BotHeaderMessageModel))))
                        chatDelegate.didChatHasMessagesConfirmed();
                }
            }
        }
        if(msgs.isEmpty() && isChatWithBot && addingStrategy == ADD_TO_TOP)
        {
            synchronized(data) {
                try
                {
                    if(data.isEmpty() || !(data.get(0) instanceof BotHeaderMessageModel))
                    {
                        BotHeaderMessageModel msg = new BotHeaderMessageModel(botInfo);
                        data.add(0, msg);
                        adapter.notifyItemInserted(0);
                    }
                }
                catch(Exception e) { e.printStackTrace(); }
            }
        }

        if(messagesCount > 0)
        {
            if(addingStrategy == ADD_TO_TOP && getChatHistoryFunc_top.limit == messagesCount) {
                synchronized (dontLoadTop) {
                    dontLoadTop.set(false);
                }
            }
            else if(addingStrategy == ADD_TO_BOTTOM && getChatHistoryFunc_bottom.limit == messagesCount) {
                synchronized (dontLoadBottom) {
                    dontLoadBottom.set(false);
                }
            }
        }
        int preciseIndexOfNewMsgsBar = -1;
        int size = 0;
        synchronized(data) {
            if (addingStrategy == ADD_TO_TOP) {
                if(indexOfNewMsgsBar >= 0)
                    preciseIndexOfNewMsgsBar = indexOfNewMsgsBar;
                data.addAll(0, msgs);
                //getChatHistoryFunc_top.offset += offsetAdd;
            } else if (addingStrategy == ADD_TO_BOTTOM) {
                size = data.size();
                if(indexOfNewMsgsBar >= 0)
                    preciseIndexOfNewMsgsBar = size + indexOfNewMsgsBar - 1;
                data.addAll(msgs);
            }
        }
        if(addingStrategy == ADD_TO_TOP)
            adapter.notifyItemRangeInserted(0, msgs.size());
        else if(addingStrategy == ADD_TO_BOTTOM)
            adapter.notifyItemRangeInserted(size, msgs.size());
        if(preciseIndexOfNewMsgsBar >= 0) {
            synchronized(dataMap)
            {
                if(dataMap.containsKey(start_message))
                {
                    if(getView() != null) {
                        ((LinearLayoutManager) ((BaseChatListView) getView()).getLayoutManager()).scrollToPositionWithOffset(preciseIndexOfNewMsgsBar, AndroidUtilities.dp(26));
                        indexOfNewMsgsBar = -1;
                    }
                }
            }
        }
        if(checkMessageCount && adapter.getItemCount() > 0) {
            try
            {
                if (getView() != null)
                    ((BaseChatListView) getView()).setAdapter(adapter);
            }
            catch(Exception e) { e.printStackTrace(); }
        }
        historyLoading.ready = true;
        synchronized(historyLoading.monitor)
        {
            historyLoading.monitor.notifyAll();
        }
    }
    public boolean needWrapSeparator(long dateMs, boolean bottom)
    {
        MessageModel lastMessage;
        synchronized(data)
        {
            if(data.isEmpty())
            {
                if(dontLoadBottom.get() && dontLoadTop.get())
                    return true;
                return false;
            }
            lastMessage = bottom ? data.get(data.size()-1) : data.get(0);
            if(lastMessage.isPseudoMessage())
                return false;
        }
        Calendar lastMsgCal = Calendar.getInstance();
        Calendar currentMsgCal = Calendar.getInstance();
        lastMsgDate.setTime(((long) (lastMessage.getIntegerDate()) * 1000L));
        currentMsgDate.setTime(dateMs);
        lastMsgCal.setTime(lastMsgDate);
        currentMsgCal.setTime(currentMsgDate);
        if(lastMsgCal.get(Calendar.YEAR) != currentMsgCal.get(Calendar.YEAR) || lastMsgCal.get(Calendar.MONTH) != currentMsgCal.get(Calendar.MONTH) || lastMsgCal.get(Calendar.DAY_OF_MONTH) != currentMsgCal.get(Calendar.DAY_OF_MONTH))
            return true;
        return false;
    }
    public void didItemClicked(int position)
    {
        if(position < 0)
            return;
        synchronized(data)
        {
            synchronized(dataMap)
            {
                MessageModel msg = data.get(position);
                if(msg.isPseudoMessage())
                    return;
                if(isInSelectionMode())
                    selectMessage(msg.getMessageId());
                else if(chatDelegate != null)
                    chatDelegate.didChatItemClicked(msg);
            }
        }
    }
    public void didChannelAvatarClicked(final long channelChatId)
    {
        if(!ChatCache.getInstance().isCached(channelChatId))
        {
            GetFullPeerTask task = new GetFullPeerTask(channelChatId);
            task.onComplete(new OnTaskCompleteListener() {
                @Override
                public void taskComplete(AsyncTaskManTask task) {
                    TdApi.ChannelFull channel = (TdApi.ChannelFull) ((GetFullPeerTask) task).getResult();
                    chatDelegate.didOpenChannelProfileRequested(channel.channel.id, channelChatId);
                }
            });
            task.addToQueue();
        }
        else {
            TdApi.Chat chat = ChatCache.getInstance().getChatById(channelChatId);
            int channelId = ((TdApi.ChannelChatInfo) chat.type).channel.id;
            chatDelegate.didOpenChannelProfileRequested(channelId, channelChatId);
        }
    }
    public void didChannelAvatarClicked(int channelId)
    {
        final TdApi.Chat chat = ChatCache.getInstance().getChatByChannelId(channelId);
        if(!ChannelCache.getInstance().isCachedFull(channelId))
        {
            GetFullPeerTask task = new GetFullPeerTask(channelId, PeerType.CHANNEL);
            task.onComplete(new OnTaskCompleteListener() {
                @Override
                public void taskComplete(AsyncTaskManTask task) {
                    final TdApi.ChannelFull channel = (TdApi.ChannelFull) ((GetFullPeerTask) task).getResult();
                    TelegramApplication.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (chat != null)
                                chatDelegate.didOpenChannelProfileRequested(channel.channel.id, chat.id);
                        }
                    });
                }
            });
            task.addToQueue();
        }
        else if(chat != null)
            chatDelegate.didOpenChannelProfileRequested(channelId, chat.id);
    }
    public void didAvatarClicked(int id)
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "didAvatarClicked");
        GetFullPeerTask task = new GetFullPeerTask(id, PeerType.USER);
        task.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(AsyncTaskManTask task) {
                try
                {
                    if(BuildConfig.DEBUG)
                        Log.d(TAG, "received user info");
                    final UserModel userInfo = (UserModel) ((GetFullPeerTask) task).getResult();
                    final long chatId = ((GetFullPeerTask) task).getChatId();
                    TelegramApplication.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(BuildConfig.DEBUG)
                                Log.d(TAG, "(UI thread) received user info");
                            chatDelegate.didOpenProfileRequested(userInfo.getId(), chatId);
                        }
                    });
                }
                catch(Exception e) { e.printStackTrace(); }
            }
        });
        task.addToQueue();
    }
    public void selectMessage(int messageId)
    {
        int index = getIndexByMessageId(messageId);
        MessageModel msg;
        synchronized(data)
        {
            msg = data.get(index);
        }
        if(adapter.hasInSelection(messageId))
        {
            if(fwdFromCount.containsKey(msg.getFromId()))
            {
                int cnt = fwdFromCount.get(msg.getFromId());
                if(cnt > 1)
                    fwdFromCount.put(msg.getFromId(), cnt - 1);
                if(cnt <= 1) {
                    fwdFromCount.remove(msg.getFromId());
                    fwdFrom.remove(msg.getFromId());
                }
            }
        }
        else {
            if(fwdFromCount.containsKey(msg.getFromId()))
            {
                int cnt = fwdFromCount.get(msg.getFromId());
                fwdFromCount.put(msg.getFromId(), cnt + 1);
            }
            else {
                fwdFromCount.put(msg.getFromId(), 1);
                fwdFrom.put(msg.getFromId(), msg.getFromUsername());
            }
        }
        adapter.toggleSelection(messageId, index);
        if(chatDelegate != null) {
            chatDelegate.didSelectionCountChanged(adapter.getSelectionCount());
            boolean canDelete;
            if(undeletableSelection.contains(messageId)) {
                undeletableSelection.remove(messageId);
                canDelete = undeletableSelection.isEmpty();
            }
            else {
                synchronized(data)
                {
                    msg = data.get(index);
                    canDelete = chatDelegate.canDeleteMessage(msg);
                    if(!canDelete)
                        undeletableSelection.add(messageId);
                }
            }
            chatDelegate.didSelectionDelButtonVisibilityChanged(canDelete && undeletableSelection.isEmpty());
        }
    }
    public boolean isInSelectionMode()
    {
        return adapter.isInSelectionMode();
    }
    public void deleteMessage(final int messageId)
    {
        int index = getIndexByMessageId(messageId);
        adapter.removeFromSelectionSet(messageId);
        if(index != -1) {
            synchronized(data) {
                synchronized (dataMap) {
                    boolean removeWithPrevious = false;
                    if (data.size() == index + 1 && data.get(index - 1) instanceof DateSectionMessageModel)
                        removeWithPrevious = true;
                    if (data.size() == index + 2 && data.get(index - 1) instanceof DateSectionMessageModel)
                        removeWithPrevious = true;
                    data.remove(index);
                    dataMap.remove(messageId);
                    if (removeWithPrevious) {
                        data.remove(index - 1);
                        adapter.notifyItemRangeRemoved(index - 1, 2);
                    }
                    else
                        adapter.notifyItemRemoved(index);
                }
            }
        }
        final int[] msgsToRemove = new int[] {messageId};
        TdApi.DeleteMessages del = new TdApi.DeleteMessages(chatId, msgsToRemove);
        TG.getClientInstance().send(del, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                ChatCache.getInstance().invalidate(chatId);
                NotificationCenter.getInstance().postNotification(NotificationCenter.didChatMessageRemovedBySelf, chatId);
            }
        });
        AudioPlaybackService playbackService = TelegramApplication.sharedApplication().getAudioPlaybackService();
        if(playbackService != null)
            playbackService.removeFromPlaylist(messageId, chatId);
    }
    public void forwardMessages(long sourceChat, int[] messageIds)
    {
        final TdApi.Chat chat = ChatCache.getInstance().getChatById(chatId);
        boolean shouldInsertNewMessage = false;
        if(chat == null)
            scrollToBottom();
        else {
            synchronized(dataMap)
            {
                if(dataMap.containsKey(chat.topMessage.id))
                    shouldInsertNewMessage = true;
            }
        }
        if(shouldInsertNewMessage)
            jumpTo(chat.topMessage.id);
        final boolean fromChannel = chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR && !(((TdApi.ChannelChatInfo) chat.type).channel.isSupergroup) && (((TdApi.ChannelChatInfo) chat.type).channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() || ((TdApi.ChannelChatInfo) chat.type).channel.role.getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor());
        TdApi.ForwardMessages sendMessageFunction = new TdApi.ForwardMessages(chatId, sourceChat, messageIds, fromChannel, false, false);
        Client cl = TG.getClientInstance();
        cl.send(sendMessageFunction, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() != TdApi.Messages.CONSTRUCTOR)
                    return;
                synchronized (dataMap) {
                    synchronized (data) {
                        TdApi.Messages result = (TdApi.Messages) object;
                        for (TdApi.Message m : result.messages) {
                            final MessageModel msgModel = transformSentMessage(m, true, fromChannel);
                            if (msgModel == null)
                                continue;
                            try {
                                TelegramApplication.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (msgModel.getSuitableConstructor() == TdApi.MessageAudio.CONSTRUCTOR) {
                                            AudioPlaybackService playbackService = TelegramApplication.sharedApplication().getAudioPlaybackService();
                                            if (playbackService != null)
                                                playbackService.addToPlaylist((AudioMessageModel) msgModel);
                                        }
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            boolean shouldInsertNow = false;
                            synchronized(dataMap)
                            {
                                if(dataMap.containsKey(chat.topMessage.id))
                                    shouldInsertNow = true;
                            }
                            if (shouldInsertNow) {
                                int index = insertNewMessage(msgModel, true);
                                Message msg = Message.obtain(newMessageReceiver);
                                msg.obj = msgModel;
                                msg.what = index;
                                msg.sendToTarget();
                            }
                        }
                    }
                }
            }
        });
    }
    public void startChatWithBot(int botUserId)
    {
        TdApi.SendBotStartMessage req = new TdApi.SendBotStartMessage(botUserId, chatId, "start");       //TODO deep linking (in next stage of Telegram Challenge)
        TG.getClientInstance().send(req, new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                if (object.getConstructor() != TdApi.Message.CONSTRUCTOR)
                    return;
                TelegramApplication.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (dataMap) {
                            synchronized (data) {
                                TdApi.Message result = (TdApi.Message) object;
                                MessageModel msgModel = transformSentMessage(result, false, false);
                                if (msgModel == null)
                                    return;
                                int index = insertNewMessage(msgModel, true);
                                Message msg = Message.obtain(newMessageReceiver);
                                msg.obj = msgModel;
                                msg.what = index;
                                msg.sendToTarget();
                            }
                        }
                    }
                });
            }
        });
    }
    public void sendMessage(TdApi.InputMessageContent content)
    {
        sendMessage(content, 0);
    }
    public void sendMessage(long queryId, String resultId)
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "sending message via inline bot");
        sendMessage(queryId, resultId, 0);
    }
    public void sendMessage(long queryId, String resultId, final int replyTo)
    {
        final TdApi.Chat chat = ChatCache.getInstance().getChatById(chatId);
        boolean shouldInsertNewMessage = false;
        if(chat == null)
            scrollToBottom();
        else {
            synchronized(dataMap)
            {
                if(dataMap.containsKey(chat.topMessage.id))
                    shouldInsertNewMessage = true;
            }
        }
        if(shouldInsertNewMessage)
            jumpTo(chat.topMessage.id);
        if(BuildConfig.DEBUG)
            Log.d(TAG, "sending message via inline bot");
        final boolean fromChannel = chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR && !(((TdApi.ChannelChatInfo) chat.type).channel.isSupergroup) && (((TdApi.ChannelChatInfo) chat.type).channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() || ((TdApi.ChannelChatInfo) chat.type).channel.role.getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor());
        TG.getClientInstance().send(new TdApi.SendInlineQueryResultMessage(chatId, replyTo, fromChannel, false, false, queryId, resultId), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object == null || object.getConstructor() != TdApi.Message.CONSTRUCTOR)
                    return;
                boolean shouldInsertNow = false;
                synchronized(dataMap)
                {
                    if(dataMap.containsKey(chat.topMessage.id))
                        shouldInsertNow = true;
                }
                onMessageSend((TdApi.Message) object, fromChannel, replyTo, shouldInsertNow);
            }
        });
    }
    public void sendMessage(TdApi.InputMessageContent content, final int replyTo)
    {
        final TdApi.Chat chat = ChatCache.getInstance().getChatById(chatId);
        boolean shouldInsertNewMessage = false;
        if(chat == null)
            scrollToBottom();
        else {
            synchronized(dataMap)
            {
                if(dataMap.containsKey(chat.topMessage.id))
                    shouldInsertNewMessage = true;
            }
        }
        if(shouldInsertNewMessage)
            jumpTo(chat.topMessage.id);
        final boolean fromChannel = chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR && !(((TdApi.ChannelChatInfo) chat.type).channel.isSupergroup) && (((TdApi.ChannelChatInfo) chat.type).channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor() || ((TdApi.ChannelChatInfo) chat.type).channel.role.getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor());
        TG.getClientInstance().send(new TdApi.SendMessage(chatId, replyTo, fromChannel, false, false, TelegramApplication.dummyMarkup, content), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object == null || object.getConstructor() != TdApi.Message.CONSTRUCTOR)
                    return;
                boolean shouldInsertNow = false;
                synchronized(dataMap)
                {
                    if(dataMap.containsKey(chat.topMessage.id))
                        shouldInsertNow = true;
                }
                onMessageSend((TdApi.Message) object, fromChannel, replyTo, shouldInsertNow);
            }
        });
    }
    private void onMessageSend(TdApi.Message result, boolean fromChannel, int replyTo, final boolean shouldInsert)
    {
        synchronized (dataMap) {
            synchronized (data) {
                boolean addSeparator;
                addSeparator = data.isEmpty() || needWrapSeparator(((long) result.date) * 1000L, true);
                if (addSeparator && shouldInsert) {
                    PseudoMessageModel dateSeparator = new DateSectionMessageModel(result.date);
                    int index = insertNewMessage(dateSeparator, false);
                    Message msg = Message.obtain(newMessageReceiver);
                    msg.obj = dateSeparator;
                    msg.what = index;
                    msg.sendToTarget();
                }
                MessageModel msgModel = transformSentMessage(result, false, fromChannel);
                if(msgModel == null)
                    return;
                try
                {
                    if(replyTo != 0 && dataMap.containsKey(replyTo))
                    {
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, "adding reply info");
                        ReplyMessageShortModel replyInfo = new ReplyMessageShortModel(dataMap.get(replyTo));
                        msgModel.addReplyInfo(replyInfo);
                    }
                }
                catch(Exception e) { e.printStackTrace(); }
                if(shouldInsert)
                {
                    int index = insertNewMessage(msgModel, true);
                    Message msg = Message.obtain(newMessageReceiver);
                    msg.obj = msgModel;
                    msg.what = index;
                    msg.sendToTarget();
                    if (BuildConfig.DEBUG)
                        Log.d(TAG, "transmitting sent message to UI thread");
                }
                if (chatDelegate != null)
                    chatDelegate.didClearComposeMessageFormRequested();
            }
        }
    }
    private MessageModel transformSentMessage(TdApi.Message result, boolean forwarded, boolean onBehalfOfChannel)
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "convert message");
        try
        {
            MessageModel msgModel = MessageModelFactory.createMsgModel(result);
            if(BuildConfig.DEBUG)
                Log.d(TAG, "object instantiated");
            msgModel.addStringRepresentation(CommonTools.messageToText(result));
            if(BuildConfig.DEBUG)
                Log.d(TAG, "added string representation");
            if(onBehalfOfChannel)
            {
                TdApi.Chat chat = ChatCache.getInstance().getChatById(chatId);
                msgModel.addFromInfo(chat.title, CommonTools.isLowDPIScreen() ? chat.photo.small : chat.photo.big, CommonTools.getInitialsByChatInfo(chat));
            }
            else
            {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "adding from info");
                UserModel from = CurrentUserModel.getInstance().getUserModel();
                msgModel.addFromInfo(from.getDisplayName(), from.getPhoto().getFile(), from.getInitials());
            }
            if(BuildConfig.DEBUG)
                Log.d(TAG, "adding fwd info");
            if(!forwarded)
                msgModel.addForwardFromInfo(null, new TdApi.File(0, "", 0, ""), null);
            else
            {
                boolean fwdFromChannel = CommonTools.forwardFromChannel(result.forwardInfo);
                if(fwdFromChannel)
                {
                    TdApi.Chat chatFrom = ChatCache.getInstance().getChatByChannelId(((TdApi.MessageForwardedFromChannel) result.forwardInfo).channelId);
                    String fromName = chatFrom.title;
                    TdApi.File photo = CommonTools.isLowDPIScreen() ? chatFrom.photo.small : chatFrom.photo.big;
                    String initials = CommonTools.getInitialsByChatInfo(chatFrom);
                    msgModel.addForwardFromInfo(fromName, photo, initials);
                }
                else
                {
                    //guaranteed that UserCache will contain user info
                    UserModel fwdFrom = UserCache.getInstance().getUserById(((TdApi.MessageForwardedFromUser) result.forwardInfo).userId);
                    String fromName = String.format("%s %s", fwdFrom.getFirstName(), fwdFrom.getLastName());
                    TdApi.File photo = fwdFrom.getPhoto().getFile();
                    String initials = CommonTools.getInitialsFromUserName(fwdFrom.getFirstName(), fwdFrom.getLastName());
                    msgModel.addForwardFromInfo(fromName, photo, initials);
                }
            }
            if(result.viaBotId != 0 && UserCache.getInstance().isCached(result.viaBotId))
            {
                UserModel user = UserCache.getInstance().getUserById(result.viaBotId);
                if(user != null)
                {
                    String username = user.getDisplayUsername();
                    if(username.length() > 1)
                        msgModel.addViaInfo(user.getDisplayUsername(), result.viaBotId);
                }
            }
            if(msgModel.getSuitableConstructor() == TdApi.MessageContact.CONSTRUCTOR)
            {
                ContactMessageModel mm = (ContactMessageModel) msgModel;
                if(mm.getContactUserId() != 0)
                {
                    UserModel u = UserCache.getInstance().getUserById(mm.getContactUserId());
                    mm.setContactPhoto(u.getPhoto().getFile());
                }
            }
            msgModel.markSentFromThisClient();
            msgModel.markSending();
            return msgModel;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    public void requestHighlight(int messageId)
    {
        highlightMsg = messageId;
    }
    public void onHighlight()
    {
        previousHighlight = highlightMsg;
        highlightMsg = 0;
    }
    public int getHighlightRequest() { return this.highlightMsg; }
    public void jumpTo(int messageId)
    {
        synchronized(dataMap)
        {
            if(dataMap.containsKey(messageId))
            {
                scrollToMsgId(messageId);
                return;
            }
        }
        synchronized(dismissGetHistory)
        {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "dismissGetHistory = true");
            dismissGetHistory.set(true);
        }
        disableUpdateNewMessage = true;
        this.start_message = this.last_message_on_bottom = this.last_message_on_top = messageId;
        getChatHistoryFunc_bottom.fromId = getChatHistoryFunc_top.fromId = messageId;
        getChatHistoryFunc_bottom.offset = -LOADING_STEP;
        getChatHistoryFunc_top.offset = 0;
        synchronized(dontLoadBottom)
        {
            dontLoadBottom.set(false);
        }
        synchronized(dontLoadTop)
        {
            dontLoadTop.set(false);
        }
        clearChatHistory(true);
        adapter.notifyDataSetChanged();
        if(BuildConfig.DEBUG)
            Log.d(TAG, "jumpTo: checkLoading");
        historyLoading.clearQueue();
        checkLoading(messageId);
        historyLoading.ready = true;
        synchronized(historyLoading.monitor)
        {
            historyLoading.monitor.notifyAll();
        }
    }

    public long getChatId() { return this.chatId; }
    private int insertNewMessage(MessageModel msg, boolean increaseOffset)
    {
        synchronized(data)
        {
            synchronized(dataMap)
            {
                int index = data.size();
                data.add(msg);
                dataMap.put(msg.getMessageId(), msg);
                if(increaseOffset) {
                    synchronized(dontLoadBottom)
                    {
                        if(!dontLoadBottom.get())
                            getChatHistoryFunc_bottom.fromId = msg.getFromId();
                    }
                    last_message_on_bottom = msg.getMessageId();
                }
                return index;
            }
        }
    }
    public int getLastReadOutMessageId() { return lastReadOutbox; }
    public int getUnreadCount() { return unreadCount; }
    public int getIndexByMessageId(int msgId)
    {
        synchronized(data)
        {
            synchronized(dataMap)
            {
                if(dataMap.containsKey(msgId) == false)
                    return -1;
                MessageModel model = dataMap.get(msgId);
                return data.indexOf(model);
            }
        }
    }
    public void clearChatHistory() { clearChatHistory(false); }
    public void clearChatHistory(boolean silent)
    {
        if(!silent)
        {
            chatHistoryCleared = true;
            synchronized(dontLoadBottom)
            {
                dontLoadBottom.set(true);
            }
            synchronized(dontLoadTop)
            {
                dontLoadTop.set(true);
            }
            if(chatDelegate != null)
                chatDelegate.didNoMessagesInChatConfirmed();
        }
        synchronized(data)
        {
            synchronized(dataMap)
            {
                dataMap.clear();
                if(!data.isEmpty() && data.get(0) instanceof BotHeaderMessageModel) {
                    MessageModel msg = data.get(0);
                    data.clear();
                    data.add(msg);
                }
                else
                    data.clear();
                if(!silent)
                    adapter.notifyDataSetChanged();
            }
        }
    }

    public void didViaBotClicked(String username) {
        if(chatDelegate != null)
            chatDelegate.didViaBotClicked(username);
    }


    static abstract class BaseChatListHandler extends PauseHandler
    {
        protected WeakReference<ChatController> controllerRef;
        public BaseChatListHandler(ChatController controller)
        {
            this.controllerRef = new WeakReference<ChatController>(controller);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }
    }
    private static class ListReceiver extends BaseChatListHandler
    {

        public ListReceiver(ChatController controller) {
            super(controller);
        }

        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef != null) {
                controllerRef.get().addItemsToList((List<MessageModel>) message.obj, message.arg1, message.arg2);
            }
        }
    }
    private static class NewMessageReceiver extends BaseChatListHandler
    {

        public NewMessageReceiver(ChatController controller) {
            super(controller);
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef != null) {
                try
                {
                    MessageModel msg = (MessageModel) message.obj;
                    if(!msg.isPseudoMessage())
                    {
                        if(msg.getMessageId() > controllerRef.get().chatTopMsgId)
                            controllerRef.get().chatTopMsgId = msg.getMessageId();
                    }
                    if(controllerRef.get().chatDelegate != null)
                        controllerRef.get().chatDelegate.didChatHasMessagesConfirmed();
                    int index = message.what;
                    View v = controllerRef.get().getView();
                    boolean visible = false;
                    try
                    {
                        if(v instanceof BaseChatListView)
                        {
                            int firstVisible = ((LinearLayoutManager)((BaseChatListView) v).getLayoutManager()).findFirstVisibleItemPosition();
                            int lastVisible = ((LinearLayoutManager)((BaseChatListView) v).getLayoutManager()).findLastVisibleItemPosition();
                            synchronized(controllerRef.get().data)
                            {
                                if(firstVisible != RecyclerView.NO_POSITION && lastVisible != RecyclerView.NO_POSITION) {
                                    synchronized(controllerRef.get().data)
                                    {
                                        if(controllerRef.get().data.size() <= lastVisible + 3)
                                            visible = true;
                                    }
                                }
                            }
                        }
                    }
                    catch(Exception e) { e.printStackTrace(); }
                    int realIndex = controllerRef.get().getIndexByMessageId(msg.getMessageId());
                    if(realIndex == index)
                        controllerRef.get().adapter.notifyItemInserted(index);
                    if((msg.isSentFromThisClient() && !msg.isPseudoMessage()) || (visible && !msg.isPseudoMessage()))
                        ((BaseChatListView)controllerRef.get().getView()).scrollToPosition(index);
                    if(!msg.isPseudoMessage() && !msg.isOut())
                    {
                        TdApi.ViewMessages readMessageReq = new TdApi.ViewMessages(controllerRef.get().chatId, new int[] {msg.getMessageId()});
                        TG.getClientInstance().send(readMessageReq, TelegramApplication.dummyResultHandler);
                    }
                }
                catch(Exception ignored) {}
            }
        }
    }
    private static class ErrorHandler extends BaseChatListHandler
    {

        public ErrorHandler(ChatController controller) {
            super(controller);
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef != null) {
                try
                {
                    final TdApi.Chat chat = ChatCache.getInstance().getChatById(controllerRef.get().chatId);
                    if(chat.topMessage.id != 0) {
                        controllerRef.get().jumpTo(chat.topMessage.id);
                    }
                }
                catch(Exception e) { e.printStackTrace(); }
            }
        }
    }

    private class HistoryLoadingThread extends Thread {
        BlockingQueue<Integer> tasks;
        private Client.ResultHandler loadingToTopHandler;
        private Client.ResultHandler loadingToBottomHandler;
        public volatile boolean ready = false;
        private final Object monitor = new Object();
        public HistoryLoadingThread()
        {
            tasks = new LinkedBlockingQueue<Integer>();
            this.loadingToBottomHandler = new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    if(BuildConfig.DEBUG && object != null)
                    {
                        Log.d(TAG, "loadingToBottomHandler: ");
                        Log.d(TAG, object.toString());
                    }
                    android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_DEFAULT);
                    synchronized(dismissGetHistory)
                    {
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, "dismissGetHistory = false");
                        dismissGetHistory.set(false);
                    }
                    android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_LOWEST);
                    if(object.getConstructor() == TdApi.Error.CONSTRUCTOR) {
                        synchronized(data)
                        {
                            if(!data.isEmpty()) {
                                historyLoading.reset();
                                return;
                            }
                        }
                        onError();
                        return;
                    }
                    TdApi.Messages messages;
                    final int oldFromId = getChatHistoryFunc_top.fromId;
                    try
                    {
                        messages = (TdApi.Messages) object;
                        if(messages.messages.length == getChatHistoryFunc_bottom.limit)
                            getChatHistoryFunc_bottom.fromId = messages.messages[0].id+1;
                        else {
                            synchronized(dontLoadBottom)
                            {
                                dontLoadBottom.set(true);
                            }
                            disableUpdateNewMessage = false;
                        }
                    }
                    catch(ClassCastException e)
                    {
                        synchronized(data)
                        {
                            if(!data.isEmpty()) {
                                historyLoading.reset();
                                return;
                            }
                        }
                        onError();
                        return;
                    }
                    didMessagesDownloaded(messages, ADD_TO_BOTTOM, getChatHistoryFunc_bottom.offset, oldFromId);
                }
            };
            this.loadingToTopHandler = new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_DEFAULT);
                    if(BuildConfig.DEBUG && object != null)
                    {
                        Log.d(TAG, "loadingToTopHandler: ");
                        Log.d(TAG, object.toString());
                    }
                    synchronized(dismissGetHistory)
                    {
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, "dismissGetHistory = false");
                        dismissGetHistory.set(false);
                    }
                    android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_LOWEST);
                    if(object.getConstructor() == TdApi.Error.CONSTRUCTOR) {
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, object.toString());
                        synchronized(data)
                        {
                            if(!data.isEmpty())
                                return;
                        }
                        onError();
                        return;
                    }
                    TdApi.Messages messages;
                    final int oldFromId = getChatHistoryFunc_top.fromId;
                    try
                    {
                        messages = (TdApi.Messages) object;
                        if(messages.messages.length > 0)
                            getChatHistoryFunc_top.fromId = messages.messages[messages.messages.length - 1].id;
                        else {
                            synchronized(dontLoadTop)
                            {
                                dontLoadTop.set(true);
                            }
                        }
                    }
                    catch(ClassCastException e)
                    {
                        synchronized(data)
                        {
                            if(!data.isEmpty())
                                return;
                        }
                        onError();
                        return;
                    }
                    int len = messages.messages.length;
                    for(int i = 0; i < len / 2; i++)
                    {
                        TdApi.Message tmp = messages.messages[i];
                        messages.messages[i] = messages.messages[len-1-i];
                        messages.messages[len-1-i] = tmp;
                    }
                    didMessagesDownloaded(messages, ADD_TO_TOP, getChatHistoryFunc_top.offset, oldFromId);
                }
            };
        }
        @Override
        public void run()
        {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_LOWEST);
            while(!isInterrupted())
            {
                Integer task;
                try {
                    task = tasks.take();
                } catch (InterruptedException e) {
                    return;
                }
                ready = false;
                if(task == ADD_TO_TOP)
                    TG.getClientInstance().send(getChatHistoryFunc_top, loadingToTopHandler);
                else if(task == ADD_TO_BOTTOM)
                    TG.getClientInstance().send(getChatHistoryFunc_bottom, loadingToBottomHandler);
                while (!ready) {
                    synchronized(monitor)
                    {
                        if(!ready)
                        {
                            try {
                                monitor.wait();
                            } catch (InterruptedException e) {
                                return;
                            }
                        }
                    }
                }
            }
        }
        public void addTask(Integer task) {
            tasks.add(task);
        }
        public void reset() {
            clearQueue();
            ready = true;
            synchronized(monitor)
            {
                monitor.notifyAll();
            }
        }
        public void clearQueue() { tasks.clear(); }
    }
    private void onError()
    {
        try
        {
            dontAddNewMsgsBar = true;
            if(BuildConfig.DEBUG)
                Log.d(TAG, "Handling error…");
            Message msg = Message.obtain(errorHandler);
            msg.sendToTarget();
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    public void setChatDelegate(BaseChatFragment.ChatDelegate delegateRef)
    {
        this.chatDelegate = delegateRef;
    }
}
