package com.andremacareno.tdtest.viewcontrollers.passcode;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.TelegramApplication;
import com.eftimoff.patternview.PatternView;

/**
 * Created by andremacareno on 05/08/15.
 */
public class PatternViewController implements PatternView.OnPatternDetectedListener {
    private String pattern = "";
    private String patternConfirmation = "";
    private PasscodePatternController controller;
    public PatternViewController(PasscodePatternController controllerRef)
    {
        this.controller = controllerRef;
    }

    @Override
    public void onPatternDetected() {
        if(controller.currentMode == PasscodeViewController.Mode.SET)
        {
            pattern = controller.getPassword();
            if(pattern.length() < 2)
                controller.incorrectConfirmation();
            else
                controller.setCurrentMode(PasscodeViewController.Mode.CONFIRMATION);
        }
        else if(controller.currentMode == PasscodeViewController.Mode.CONFIRMATION)
        {
            if(pattern.length() == 0) {
                controller.setCurrentMode(PasscodeViewController.Mode.SET);
                return;
            }
            patternConfirmation = controller.getPassword();
            if(pattern.equals(patternConfirmation))
                controller.correctConfirmation();
            else
                controller.incorrectConfirmation();
        }
        else if(controller.currentMode == PasscodeViewController.Mode.LOCK)
        {
            String realPassword = TelegramApplication.sharedApplication().sharedPreferences().getString(Constants.PREFS_PASSCODE_LOCK_PATTERN, "");
            pattern = controller.getPassword();
            if(pattern.equals(realPassword)) {
                controller.passcodeLockPassed();
            }
            else
                controller.incorrectConfirmation();
        }
    }
}
