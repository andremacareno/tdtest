package com.andremacareno.tdtest.viewcontrollers.actionview;

import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.views.actionviews.ActionView;

import java.lang.ref.WeakReference;

/**
 * Created by Andrew on 13.05.2015.
 */
public abstract class ActionViewController {
    protected WeakReference<ActionView> actionView;
    public void setActionView(ActionView v)
    {
        this.actionView = new WeakReference<ActionView>(v);
        this.onActionViewSet();
    }
    public ActionView getActionView() { return this.actionView.get(); }
    public abstract void onFragmentPaused();
    public abstract void onFragmentResumed();
    public abstract void onActionViewRemoved();
    public abstract void onUpdateServiceConnected(TelegramUpdatesHandler service);
    protected void onActionViewSet() {};
}
