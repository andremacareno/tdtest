package com.andremacareno.tdtest.viewcontrollers.passcode;

import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.TelegramApplication;

/**
 * Created by andremacareno on 05/08/15.
 */
public class PasswordDoneButtonController implements TextView.OnEditorActionListener {
    private String password = "";
    private String passwordConfirmation = "";
    private PasscodePasswordController controller;
    public PasswordDoneButtonController(PasscodePasswordController controllerRef)
    {
        this.controller = controllerRef;
    }
    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if(i == EditorInfo.IME_ACTION_DONE)
        {
            if(controller.currentMode == PasscodeViewController.Mode.SET)
            {
                password = controller.getPassword();
                if(password.length() == 0)
                    controller.incorrectConfirmation();
                else
                    controller.setCurrentMode(PasscodeViewController.Mode.CONFIRMATION);
            }
            else if(controller.currentMode == PasscodeViewController.Mode.CONFIRMATION)
            {
                if(password.length() == 0) {
                    controller.setCurrentMode(PasscodeViewController.Mode.SET);
                    return true;
                }
                passwordConfirmation = controller.getPassword();
                if(password.equals(passwordConfirmation))
                    controller.correctConfirmation();
                else
                    controller.incorrectConfirmation();
            }
            else if(controller.currentMode == PasscodeViewController.Mode.LOCK)
            {
                //TODO encrypt it?
                String realPassword = TelegramApplication.sharedApplication().sharedPreferences().getString(Constants.PREFS_PASSCODE_LOCK_PASSWORD, "");
                password = controller.getPassword();
                if(password.equals(realPassword)) {
                    controller.hideKeyboard();
                    controller.passcodeLockPassed();
                }
                else
                    controller.incorrectConfirmation();
            }
            return true;
        }
        return false;
    }
}
