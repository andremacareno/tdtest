package com.andremacareno.tdtest.viewcontrollers.passcode;

import android.content.SharedPreferences;
import android.view.View;
import android.widget.TextView;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.views.LetterSpacingTextView;
import com.andremacareno.tdtest.views.PasscodePinPad;
import com.andremacareno.tdtest.views.PasscodeView;

/**
 * Created by andremacareno on 02/08/15.
 */
public class PasscodePinController extends PasscodeViewController{
    private View telegramLogo;
    private TextView helpTextView;
    private LetterSpacingTextView passcodeTextView;
    private PasscodePinPad pinPad;
    private PasscodePinPadController pinPadController;
    public PasscodePinController(PasscodeView view, Mode mode) {
        super(view, mode);
    }
    @Override
    protected void init()
    {
        telegramLogo = viewRef.findViewById(R.id.logo);
        helpTextView = (TextView) viewRef.findViewById(R.id.help);
        passcodeTextView = (LetterSpacingTextView) viewRef.findViewById(R.id.passcode);
        pinPad = (PasscodePinPad) viewRef.findViewById(R.id.pinpad);
        if(pinPadController == null)
            pinPadController = new PasscodePinPadController(this);
        pinPad.setPinPadControllerReference(pinPadController);
    }
    public void addLetter()
    {
        passcodeTextView.setText(passcodeTextView.getText() + "\u2022");
    }
    public void removeLetter()
    {
        if(passcodeTextView.getText().length() > 0)
            passcodeTextView.setText(passcodeTextView.getText().subSequence(0, passcodeTextView.getText().length()-1));
    }

    @Override
    protected void savePasscode(SharedPreferences.Editor prefEdit) {
        prefEdit.putInt(Constants.PREFS_PASSCODE_LOCK_TYPE, Constants.PASSCODE_TYPE_PIN);
        prefEdit.putString(Constants.PREFS_PASSCODE_LOCK_PIN, pinPadController.getPin());
    }
    @Override
    public void incorrectConfirmation()
    {
        //animate "Re-enter…" label and reset TextView
        helpTextView.startAnimation(shakeAnimation);
        passcodeTextView.setText("");
    }

    @Override
    protected void didModeChanged() {
        if(currentMode == Mode.CONFIRMATION && passcodeTextView.getText().length() == 4)
            passcodeTextView.setText("");
        if(currentMode == Mode.SET || currentMode == Mode.CONFIRMATION) {
            telegramLogo.setVisibility(View.GONE);
        }
        if(currentMode == Mode.SET) {
            helpTextView.setText(R.string.pin_set);
            restorePasscodeTextView(pinPadController.getPin().length());
        }
        else if(currentMode == Mode.CONFIRMATION) {
            helpTextView.setText(R.string.pin_confirm);
            restorePasscodeTextView(pinPadController.getPinConfirmation().length());
        }
        else if(currentMode == Mode.LOCK) {
            helpTextView.setText(R.string.pin_lock);
            restorePasscodeTextView(pinPadController.getPin().length());
        }
    }
    private void restorePasscodeTextView(int pinLength)
    {
        StringBuilder sb = new StringBuilder(pinLength);
        for (int i = 0; i < pinLength; ++i)
        {
            sb.append("&#8226;");
        }
        passcodeTextView.setText(sb.toString());
    }
}
