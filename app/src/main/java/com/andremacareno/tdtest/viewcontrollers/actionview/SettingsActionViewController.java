package com.andremacareno.tdtest.viewcontrollers.actionview;

import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.views.actionviews.ProfileActionView;

/**
 * Created by Andrew on 14.05.2015.
 */
public class SettingsActionViewController extends ProfileActionViewController {
    public SettingsActionViewController(UserModel user)
    {
        super(user);
    }
    @Override
    public void initMenu()
    {
        ProfileActionView v = (ProfileActionView) getActionView();
        v.hideNotificationsButton();
        v.setMenuResource(R.menu.settings);
    }
    @Override
    protected boolean forceAddMenu()
    {
        return true;
    }

}
