package com.andremacareno.tdtest.viewcontrollers.actionview;

import com.andremacareno.tdtest.ChatCache;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.images.TelegramImageLoader;
import com.andremacareno.tdtest.views.actionviews.ChatActionView;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 14.05.2015.
 */
public class ChannelChatActionViewController extends ActionViewController {
    private final int avatarSize;
    private String initials;
    private volatile String avatarKey;
    private volatile String placeholderKey;
    private TdApi.Chat chatRef;
    private TdApi.File photo;

    public ChannelChatActionViewController(){
        this.avatarSize = TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size);
    }
    public void bindChatInfo(long chatId)
    {
        this.chatRef = ChatCache.getInstance().getChatById(chatId);
        this.placeholderKey = String.format(Constants.AVATAR_PLACEHOLDER_FMT, chatId);
        if(chatRef != null)
        {
            initials = chatRef.title.substring(0, 1).toUpperCase();
            photo = CommonTools.isLowDPIScreen() ? chatRef.photo.small : chatRef.photo.big;
            if(photo.id != 0)
                avatarKey = CommonTools.makeFileKey(photo.id);
        }
    }

    @Override
    public void onFragmentPaused() {
    }

    @Override
    public void onFragmentResumed() {
        //fillLayout();
    }

    @Override
    public void onActionViewRemoved() {
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
    }
    @Override
    protected void onActionViewSet() { fillLayout(); }
    private void fillLayout()
    {
        if(chatRef == null)
            return;
        ChatActionView v = (ChatActionView) getActionView();
        if(v != null)
        {
            v.firstLine.setText(chatRef.title);
            v.secondLine.setText(CommonTools.generateChannelDescription(((TdApi.ChannelChatInfo) chatRef.type).channel.id));
            if(photo.id == 0)
                TelegramImageLoader.getInstance().loadPlaceholder(((TdApi.ChannelChatInfo) chatRef.type).channel.id, initials, placeholderKey, v.avatar, false);
            else
                TelegramImageLoader.getInstance().loadImage(FileCache.getInstance().getFileModel(photo), avatarKey, ((ChatActionView) getActionView()).avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, avatarSize, avatarSize, false);
        }
    }
    public void updateTitle(String newTitle)
    {
        ChatActionView v = (ChatActionView) getActionView();
        if(v != null)
        {
            v.firstLine.setText(newTitle);
        }
    }
    public void updatePhoto(TdApi.File newPhoto)
    {
        ChatActionView v = (ChatActionView) getActionView();
        if(v != null && chatRef != null)
        {
            if(newPhoto.id == 0)
                TelegramImageLoader.getInstance().loadPlaceholder(((TdApi.ChannelChatInfo) chatRef.type).channel.id, initials, placeholderKey, v.avatar, false);
            else
                TelegramImageLoader.getInstance().loadImage(FileCache.getInstance().getFileModel(newPhoto), avatarKey, ((ChatActionView) getActionView()).avatar, TelegramImageLoader.POST_PROCESSING_CIRCLE | TelegramImageLoader.POST_PROCESSING_SCALE, avatarSize, avatarSize, false);
        }
    }
}
