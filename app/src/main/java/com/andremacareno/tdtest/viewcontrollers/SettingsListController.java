package com.andremacareno.tdtest.viewcontrollers;

import android.view.View;

import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.fragments.SettingsFragment;
import com.andremacareno.tdtest.listadapters.SettingsAdapter;
import com.andremacareno.tdtest.models.TelegramAction;
import com.andremacareno.tdtest.views.SettingsListView;

import java.lang.ref.WeakReference;

/**
 * Created by andremacareno on 17/04/15.
 */
public class SettingsListController extends ViewController{
    //statement variables
    private final String TAG = "AttachListController";
    private SettingsAdapter adapter;
    private int chosenAction = TelegramAction.ATTACH_DO_NOTHING;
    private WeakReference<SettingsFragment> fragmentRef;
    public SettingsListController(View v) {
        super(v);
        init();
    }
    private void init()
    {
        this.adapter = new SettingsAdapter(this);
        initViewSettings();
    }
    @Override
    protected void onViewReplaced() {
        if(getView() != null) {
            initViewSettings();
        }
    }
    public SettingsAdapter getAdapter() { return this.adapter; }
    public void setFragmentReference(SettingsFragment ref) { this.fragmentRef = new WeakReference<SettingsFragment>(ref); }
    private void initViewSettings() {
        SettingsListView v = (SettingsListView) getView();
        v.setAdapter(adapter);
        v.setMotionEventSplittingEnabled(false);
    }

    @Override
    public void onFragmentPaused() {
    }

    @Override
    public void onFragmentResumed() {
    }

    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {

    }

    public void didActionChosen(TelegramAction act)
    {
        this.chosenAction = act.getAction();
        if(this.fragmentRef != null)
            this.fragmentRef.get().processAction(act);
    }
}
