package com.andremacareno.tdtest.viewcontrollers.passcode;

import android.content.SharedPreferences;
import android.gesture.GestureOverlayView;
import android.view.View;
import android.widget.TextView;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.views.PasscodeView;

/**
 * Created by andremacareno on 02/08/15.
 */
public class PasscodeGestureController extends PasscodeViewController{
    private View telegramLogo;
    private TextView helpTextView;
    private GestureOverlayView gestureView;
    private GestureViewController gestureViewController;
    public PasscodeGestureController(PasscodeView view, Mode mode) {
        super(view, mode);
    }
    @Override
    protected void init()
    {
        telegramLogo = viewRef.findViewById(R.id.logo);
        helpTextView = (TextView) viewRef.findViewById(R.id.help);
        gestureView = (GestureOverlayView) viewRef.findViewById(R.id.gesture_surface);
        gestureViewController = new GestureViewController(this);
        gestureView.addOnGesturePerformedListener(gestureViewController);
    }
    @Override
    protected void savePasscode(SharedPreferences.Editor prefEdit) {
        prefEdit.putInt(Constants.PREFS_PASSCODE_LOCK_TYPE, Constants.PASSCODE_TYPE_GESTURE);
    }
    @Override
    public void incorrectConfirmation()
    {
        helpTextView.startAnimation(shakeAnimation);
        gestureView.clear(false);
    }

    @Override
    protected void didModeChanged() {
        if(currentMode == Mode.SET || currentMode == Mode.CONFIRMATION) {
            telegramLogo.setVisibility(View.GONE);
        }
        if(currentMode == Mode.SET) {
            helpTextView.setText(R.string.gesture_set);
        }
        else if(currentMode == Mode.CONFIRMATION) {
            helpTextView.setText(R.string.gesture_confirm);
            gestureView.clear(false);
        }
        else if(currentMode == Mode.LOCK) {
            helpTextView.setText(R.string.gesture_lock);
        }
    }
}
