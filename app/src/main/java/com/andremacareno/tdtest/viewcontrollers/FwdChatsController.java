package com.andremacareno.tdtest.viewcontrollers;

import android.os.Handler;
import android.os.Looper;
import android.view.View;

import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.TelegramUpdatesHandler;
import com.andremacareno.tdtest.listadapters.FwdChatsAdapter;
import com.andremacareno.tdtest.views.ForwardBottomSheetGridView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import gnu.trove.TCollections;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.hash.TLongObjectHashMap;

/**
 * Created by andremacareno on 04/04/15.
 */
public class FwdChatsController extends ViewController {
    //statement variables
    public interface FwdChatsDelegate
    {
        public void toggleSelection(int index);
        public void loadMoreConversations();
        public boolean inSearchMode();
    }
    protected final String TAG = "FwdChatsController";
    protected FwdChatsAdapter adapter;
    private final ArrayList<TdApi.Chat> chats = new ArrayList<>();
    private final ArrayList<TdApi.Chat> searchResults = new ArrayList<>();
    private final TLongObjectMap<TdApi.Chat> idsToChats = TCollections.synchronizedMap(new TLongObjectHashMap<TdApi.Chat>());
    private FwdChatsDelegate delegate;
    private final TdApi.GetChats getChatsFunc = new TdApi.GetChats(9223372036854775807L, 0, 20);
    private final TdApi.SearchChats searchChatsFunc = new TdApi.SearchChats("", 20);
    private static Handler searchQueryHandler = new Handler(Looper.getMainLooper());
    private Client.ResultHandler chatsHandler, searchResultsHandler;
    private final AtomicBoolean preventLoading = new AtomicBoolean(false);
    private volatile boolean searchMode = false;
    public FwdChatsController(View v) {
        super(v);
        this.delegate = new FwdChatsDelegate() {
            @Override
            public void toggleSelection(int index) {

            }
            @Override
            public void loadMoreConversations()
            {
                if(searchMode)
                    return;
                synchronized(preventLoading)
                {
                    if(preventLoading.get()) {
                        return;
                    }
                    preventLoading.set(true);
                }
                synchronized(getChatsFunc)
                {
                    TG.getClientInstance().send(getChatsFunc, chatsHandler);
                }
            }

            @Override
            public boolean inSearchMode() {
                return searchMode;
            }
        };
        chatsHandler = new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(searchMode) {
                    synchronized(preventLoading)
                    {
                        preventLoading.set(false);
                    }
                    return;
                }
                final TdApi.Chats dialogs = (TdApi.Chats) object;
                final ArrayList<TdApi.Chat> newRecords = new ArrayList<>();
                for(TdApi.Chat chat : dialogs.chats)
                {
                    synchronized(idsToChats)
                    {
                        synchronized(chats)
                        {
                            if(idsToChats.containsKey(chat.id))
                                continue;
                            if(!isShareable(chat))
                                continue;
                            idsToChats.put(chat.id, chat);
                            newRecords.add(chat);
                        }
                    }
                }
                TelegramApplication.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        synchronized(chats) {
                            if(!newRecords.isEmpty())
                            {
                                int position = chats.size();
                                chats.addAll(newRecords);
                                adapter.notifyItemRangeInserted(position, newRecords.size());
                            }
                        }
                        if (searchMode) {
                            if (dialogs.chats.length > 0) {
                                synchronized (preventLoading) {
                                    preventLoading.set(false);
                                }
                            }
                            return;
                        }
                        synchronized (getChatsFunc) {
                            if (dialogs.chats.length > 0) {
                                TdApi.Chat lastChat = dialogs.chats[dialogs.chats.length - 1];
                                getChatsFunc.offsetChatId = lastChat.id;
                                getChatsFunc.offsetOrder = lastChat.order;
                            }
                        }
                        if (dialogs.chats.length > 0) {
                            synchronized (preventLoading) {
                                preventLoading.set(false);
                            }
                        }
                    }
                });
            }
        };
        searchResultsHandler = new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(!searchMode) {
                    return;
                }
                final TdApi.Chats dialogs = (TdApi.Chats) object;
                final ArrayList<TdApi.Chat> newRecords = new ArrayList<>();
                for(TdApi.Chat chat : dialogs.chats)
                {
                    if(!isShareable(chat))
                        continue;
                    newRecords.add(chat);
                }
                synchronized(searchResults)
                {
                    searchResults.clear();
                    searchResults.addAll(newRecords);
                }
                if(!newRecords.isEmpty())
                    TelegramApplication.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(!searchMode)
                                return;
                            adapter.notifyDataSetChanged();
                        }
                    });
            }
        };
        this.adapter = new FwdChatsAdapter(chats, searchResults, delegate);
        initViewSettings();
    }
    private boolean isShareable(TdApi.Chat chat)
    {
        if(chat.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR)
            return ((TdApi.PrivateChatInfo) chat.type).user.type.getConstructor() != TdApi.UserTypeDeleted.CONSTRUCTOR && ((TdApi.PrivateChatInfo) chat.type).user.type.getConstructor() != TdApi.UserTypeUnknown.CONSTRUCTOR;
        else if(chat.type.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR)
            return ((TdApi.GroupChatInfo) chat.type).group.role.getConstructor() != Constants.PARTICIPANT_LEFT.getConstructor() && ((TdApi.GroupChatInfo) chat.type).group.role.getConstructor() != Constants.PARTICIPANT_KICKED.getConstructor();
        else if(chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR && !((TdApi.ChannelChatInfo) chat.type).channel.isSupergroup)
            return ((TdApi.ChannelChatInfo) chat.type).channel.role.getConstructor() == Constants.PARTICIPANT_EDITOR.getConstructor()|| ((TdApi.ChannelChatInfo) chat.type).channel.role.getConstructor() == Constants.PARTICIPANT_ADMIN.getConstructor();
        else if(chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR)
            return ((TdApi.ChannelChatInfo) chat.type).channel.role.getConstructor() != Constants.PARTICIPANT_LEFT.getConstructor() && ((TdApi.ChannelChatInfo) chat.type).channel.role.getConstructor() != Constants.PARTICIPANT_KICKED.getConstructor();
        return false;
    }
    public void searchChats(final String query)
    {
        if(!searchMode && !query.isEmpty())
        {
            searchMode = true;
            adapter.notifySearchModeStateChanged();
        }
        else if(searchMode && query.isEmpty())
        {
            searchQueryHandler.removeCallbacksAndMessages(null);
            searchMode = false;
            synchronized(searchResults)
            {
                searchResults.clear();
            }
            adapter.notifySearchModeStateChanged();
            return;
        }
        searchQueryHandler.removeCallbacksAndMessages(null);
        searchQueryHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                synchronized(searchChatsFunc)
                {
                    searchChatsFunc.query = query;
                    TG.getClientInstance().send(searchChatsFunc, searchResultsHandler);
                }
            }
        }, 600);
    }
    public void setCurrentChat(long chat)
    {
        if(adapter != null) {
            this.adapter.setCurrentChatId(chat);
            adapter.notifyDataSetChanged();
        }
    }
    public void reset()
    {
        searchMode = false;
        synchronized(idsToChats)
        {
            idsToChats.clear();
        }
        synchronized(searchResults)
        {
            searchResults.clear();
        }
        synchronized(chats)
        {
            chats.clear();
        }
        adapter.clearSelection();
        synchronized (preventLoading)
        {
            preventLoading.set(false);
        }
        synchronized(getChatsFunc)
        {
            getChatsFunc.offsetOrder = 9223372036854775807L;
            getChatsFunc.offsetChatId = 0;
        }
        adapter.notifyDataSetChanged();
    }
    public long[] getSelection() { return adapter.getSelection(); }
    public boolean forwardToSelf() { return adapter.forwardToSelf(); }
    @Override
    protected void onViewReplaced() {
        if(getView() != null) {
            initViewSettings();
        }
    }
    public void onFragmentPaused()
    {
    }
    public void onFragmentResumed()
    {
    }
    @Override
    public void onUpdateServiceConnected(TelegramUpdatesHandler service) {
    }
    public void removeObservers()
    {
    }


    private void initViewSettings()
    {
        ForwardBottomSheetGridView v = (ForwardBottomSheetGridView) getView();
        v.setAdapter(adapter);
        v.setMotionEventSplittingEnabled(false);
    }
}
