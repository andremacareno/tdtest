package com.andremacareno.tdtest;

import android.view.MotionEvent;

import uk.co.senab.photoview.DefaultOnDoubleTapListener;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by andremacareno on 13/12/15.
 */
public abstract class AdvancedDoubleTapListener extends DefaultOnDoubleTapListener {
    /**
     * Default constructor
     *
     * @param photoViewAttacher PhotoViewAttacher to bind to
     */
    public AdvancedDoubleTapListener(PhotoViewAttacher photoViewAttacher) {
        super(photoViewAttacher);
    }
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        didSingleTapConfirmed();
        return super.onSingleTapConfirmed(e);
    }
    public abstract void didSingleTapConfirmed();
}
