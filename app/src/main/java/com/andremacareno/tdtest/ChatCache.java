package com.andremacareno.tdtest;

import android.util.Log;

import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.updatelisteners.UpdateChatProcessor;
import com.andremacareno.tdtest.updatelisteners.UpdateMessageProcessor;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.concurrent.atomic.AtomicBoolean;

import gnu.trove.TCollections;
import gnu.trove.map.TIntLongMap;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.hash.TIntLongHashMap;
import gnu.trove.map.hash.TLongObjectHashMap;

/**
 * Created by andremacareno on 24/04/15.
 */
public class ChatCache {
    private static volatile ChatCache _instance;

    private final Object createPrivateMonitor = new Object();
    private final Object createGroupMonitor = new Object();
    private final Object createChannelMonitor = new Object();

    private final Object monitor = new Object();
    private final TLongObjectMap<TdApi.Chat> chats = TCollections.synchronizedMap(new TLongObjectHashMap<TdApi.Chat>());
    private final TIntLongMap groupToChat = TCollections.synchronizedMap(new TIntLongHashMap());
    private final TIntLongMap channelToChat = TCollections.synchronizedMap(new TIntLongHashMap());
    private final TIntLongMap userToChat = TCollections.synchronizedMap(new TIntLongHashMap());
    private final UpdateChatProcessor updateProcessor;
    private final UpdateMessageProcessor updateMsgProcessor;
    private ChatCache() {
        UpdateMessageProcessor.UpdateMessageDelegate bgThreadDelegate = new UpdateMessageProcessor.UpdateMessageDelegate() {
            @Override
            public void updateMessageContent(TdApi.UpdateMessageContent upd) {
            }

            @Override
            public void updateMessageDate(TdApi.UpdateMessageDate upd) {
            }

            @Override
            public void updateMessageId(TdApi.UpdateMessageId upd) {
                synchronized(chats)
                {
                    if(chats.containsKey(upd.chatId))
                    {
                        TdApi.Chat chat = chats.get(upd.chatId);
                        if(chat.topMessage.id == upd.oldId)
                            chat.topMessage.id = upd.newId;
                        else if(chat.topMessage.id < upd.newId)
                            cacheChat(upd.chatId);
                    }
                }
            }

            @Override
            public void updateMessageSendFailed(TdApi.UpdateMessageSendFailed upd) {
            }

            @Override
            public void updateNewMessage(TdApi.UpdateNewMessage upd) {
                synchronized(chats)
                {
                    if(chats.containsKey(upd.message.chatId))
                    {
                        TdApi.Chat chat = chats.get(upd.message.chatId);
                        chat.topMessage = upd.message;
                    }
                }
            }

            @Override
            public void updateDelMessages(final TdApi.UpdateDeleteMessages upd) {
                synchronized(chats)
                {
                    if(chats.containsKey(upd.chatId))
                    {
                        TdApi.Chat chat = chats.get(upd.chatId);
                        for(int id : upd.messageIds)
                        {
                            if(chat.topMessage.id == id)
                            {
                                chats.remove(upd.chatId);
                                /*new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        getChatById(upd.chatId);
                                    }
                                }).start();*/
                                return;
                            }
                        }
                    }
                }
            }

            @Override
            public void updateViewsCount(TdApi.UpdateMessageViews upd) {
            }
        };
        updateMsgProcessor = new UpdateMessageProcessor(bgThreadDelegate, null) {
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                return "chatcache_update_msg_observer";
            }

            @Override
            protected boolean shouldObserveUpdateMsgContent() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateMsgDate() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateMsgId() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateMsgSendFailed() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateNewMsg() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateDeleteMessages() {
                return true;
            }

            @Override
            protected boolean shouldObserveViewsCount() {
                return false;
            }
        };
        updateProcessor = new UpdateChatProcessor() {
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                return String.format("chatcache_constructor_%d_observer", constructor);
            }

            @Override
            protected boolean shouldObserveUpdateGroup() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateTitle() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateReplyMarkup() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateReadInbox() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateReadOutbox() {
                return false;
            }

            @Override
            protected boolean shouldObserveUpdateChat() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateChatPhoto() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateChatOrder() {
                return false;
            }

            @Override
            protected void processUpdateInBackground(TdApi.Update upd) {
                if(upd.getConstructor() == TdApi.UpdateChatTitle.CONSTRUCTOR)
                {
                    TdApi.UpdateChatTitle castUpd = (TdApi.UpdateChatTitle) upd;
                    synchronized(chats)
                    {
                        if(chats.containsKey(castUpd.chatId))
                            chats.get(castUpd.chatId).title = castUpd.title;
                        else
                            cacheChat(castUpd.chatId);
                    }
                }
                else if(upd.getConstructor() == TdApi.UpdateChat.CONSTRUCTOR)
                {
                    TdApi.UpdateChat castUpd = (TdApi.UpdateChat) upd;
                    addChatToCache(castUpd.chat);
                }
                else if(upd.getConstructor() == TdApi.UpdateChatPhoto.CONSTRUCTOR)
                {
                    TdApi.UpdateChatPhoto castUpd = (TdApi.UpdateChatPhoto) upd;
                    synchronized(chats)
                    {
                        if(chats.containsKey(castUpd.chatId))
                            chats.get(castUpd.chatId).photo = castUpd.photo;
                        else
                            cacheChat(castUpd.chatId);
                    }
                }
                else if(upd.getConstructor() == TdApi.UpdateChatReplyMarkup.CONSTRUCTOR)
                {
                    TdApi.UpdateChatReplyMarkup castUpd = (TdApi.UpdateChatReplyMarkup) upd;
                    synchronized(chats)
                    {
                        if(chats.containsKey(castUpd.chatId))
                            chats.get(castUpd.chatId).replyMarkupMessageId = castUpd.replyMarkupMessageId;
                        else
                            cacheChat(castUpd.chatId);
                    }
                }
            }

            @Override
            protected void processUpdateInUiThread(TdApi.Update upd) {
            }
        };
    }
    public void attachObserver()
    {
        TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
        if(service == null)
        {
            NotificationCenter.getInstance().addObserver(new NotificationObserver(NotificationCenter.didUpdateServiceStarted) {
                @Override
                public void didNotificationReceived(Notification notification) {
                    TelegramUpdatesHandler serviceInstance = (TelegramUpdatesHandler) notification.getObject();
                    updateProcessor.attachObservers(serviceInstance);
                    updateMsgProcessor.attachObservers(serviceInstance);
                }
            });
        }
        else {
            updateProcessor.attachObservers(service);
            updateMsgProcessor.attachObservers(service);
        }
    }
    public static ChatCache getInstance()
    {
        if(_instance == null)
        {
            synchronized(ChatCache.class)
            {
                if(_instance == null)
                    _instance = new ChatCache();
            }
        }
        return _instance;
    }
    public void clear()
    {
        synchronized(chats)
        {
            chats.clear();
        }
        synchronized(channelToChat)
        {
            channelToChat.clear();
        }
        synchronized(userToChat)
        {
            userToChat.clear();
        }
        updateProcessor.removeObservers();
        updateMsgProcessor.removeObservers();
        /*UpdateService service = TelegramApplication.sharedApplication().getUpdateService();
        if(service != null)
            service.removeObserver(updateStatusListener);*/
    }
    public void setChannelLink(long chatId, int channelId)
    {
        synchronized(channelToChat)
        {
            channelToChat.put(channelId, chatId);
        }
    }
    public void setGroupLink(long chatId, int groupChatId)
    {
        synchronized(groupToChat)
        {
            groupToChat.put(groupChatId, chatId);
        }
    }
    public void setUserLink(long chatId, int userId)
    {
        synchronized(userToChat)
        {
            userToChat.put(userId, chatId);
        }
    }
    public void invalidate(long chatId)
    {
        synchronized(chats)
        {
            chats.remove(chatId);
        }
    }
    public void addChatToCache(TdApi.Chat chat)
    {
        synchronized(chats)
        {
            chats.put(chat.id, chat);
            if(BuildConfig.DEBUG)
                Log.d("ChatCache", "added chat to cache");
            if(chat.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR)
                setUserLink(chat.id, ((TdApi.PrivateChatInfo) chat.type).user.id);
            else if(chat.type.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR)
                setGroupLink(chat.id, ((TdApi.GroupChatInfo) chat.type).group.id);
            else if(chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR)
                setChannelLink(chat.id, ((TdApi.ChannelChatInfo) chat.type).channel.id);
            if(BuildConfig.DEBUG)
                Log.d("ChatCache", "link set");
        }
    }
    public boolean isCached(long chatId) {
        synchronized(chats)
        {
            return chats.containsKey(chatId);
        }
    }
    public TdApi.Chat getChatById(long id)
    {
        return getChatById(id, false);
    }
    public TdApi.Chat getChatById(long id, boolean force)
    {
        if(!force)
        {
            synchronized(chats)
            {
                if(chats.containsKey(id)) {
                    TdApi.Chat ch = chats.get(id);
                    if(ch.topMessage.id != 0)
                        return chats.get(id);
                }
            }
        }
        final AtomicBoolean ready = new AtomicBoolean(false);
        TdApi.GetChat getChatFunc = new TdApi.GetChat(id);
        Client telegramClient = TG.getClientInstance();
        telegramClient.send(getChatFunc, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                try
                {
                    TdApi.Chat ch = (TdApi.Chat) object;
                    addChatToCache(ch);
                }
                finally {
                    ready.set(true);
                    synchronized(monitor)
                    {
                        monitor.notifyAll();
                    }
                }
            }
        });
        while (!ready.get()) {
            synchronized(monitor)
            {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return chats.get(id);
    }

    public TdApi.Chat getChatByUserId(int userId)
    {
        if(userId == 0)
            return null;
        synchronized(chats) {
            synchronized (userToChat) {
                if (userToChat.containsKey(userId) && chats.containsKey(userToChat.get(userId)))
                    return chats.get(userToChat.get(userId));
            }
        }
        return createPrivateChat(userId);
    }
    public TdApi.Chat getChatByGroupId(int groupId)
    {
        if(groupId == 0)
            return null;
        synchronized(chats) {
            synchronized (groupToChat) {
                if (groupToChat.containsKey(groupId) && chats.containsKey(groupToChat.get(groupId)))
                    return chats.get(groupToChat.get(groupId));
            }
        }
        return createGroupChat(groupId);
    }

    public TdApi.Chat getChatByChannelId(final int channelId)
    {
        if(channelId == 0)
            return null;
        synchronized(chats) {
            synchronized (channelToChat) {
                if (channelToChat.containsKey(channelId) && chats.containsKey(channelToChat.get(channelId)))
                    return chats.get(channelToChat.get(channelId));
            }
        }
        return createChannelChat(channelId);
    }
    public TdApi.Chat createChannelChat(final int channelId)
    {
        final AtomicBoolean ready = new AtomicBoolean(false);
        TdApi.CreateChannelChat createChat = new TdApi.CreateChannelChat(channelId);
        TG.getClientInstance().send(createChat, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                try
                {
                    addChatToCache((TdApi.Chat) object);
                }
                finally
                {
                    ready.set(true);
                    synchronized(createChannelMonitor)
                    {
                        createChannelMonitor.notifyAll();
                    }
                }
            }
        });
        while (!ready.get()) {
            synchronized(createChannelMonitor)
            {
                try {
                    createChannelMonitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        synchronized(chats)
        {
            synchronized(channelToChat)
            {
                TdApi.Chat ret = chats.get(channelToChat.get(channelId));
                return ret;
            }
        }
    }
    public TdApi.Chat createGroupChat(final int groupId)
    {
        final AtomicBoolean ready = new AtomicBoolean(false);
        TdApi.CreateGroupChat createChat = new TdApi.CreateGroupChat(groupId);
        TG.getClientInstance().send(createChat, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                try
                {
                    addChatToCache((TdApi.Chat) object);
                }
                finally
                {
                    ready.set(true);
                    synchronized(createGroupMonitor)
                    {
                        createGroupMonitor.notifyAll();
                    }
                }
            }
        });
        while (!ready.get()) {
            synchronized(createGroupMonitor)
            {
                try {
                    createGroupMonitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        synchronized(chats)
        {
            synchronized(groupToChat)
            {
                TdApi.Chat ret = chats.get(groupToChat.get(groupId));
                return ret;
            }
        }
    }
    public TdApi.Chat createPrivateChat(final int userId)
    {
        final AtomicBoolean ready = new AtomicBoolean(false);
        TdApi.CreatePrivateChat createChat = new TdApi.CreatePrivateChat(userId);
        TG.getClientInstance().send(createChat, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                try
                {
                    addChatToCache((TdApi.Chat) object);
                }
                finally
                {
                    ready.set(true);
                    synchronized(createPrivateMonitor)
                    {
                        createPrivateMonitor.notifyAll();
                    }
                }
            }
        });
        while (!ready.get()) {
            synchronized(createPrivateMonitor)
            {
                try {
                    createPrivateMonitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        if(BuildConfig.DEBUG)
            Log.d("ChatCache", "returning value");
        synchronized(chats)
        {
            synchronized(userToChat)
            {
                TdApi.Chat ret = chats.get(userToChat.get(userId));
                return ret;
            }
        }
    }
    public void cacheChat(final long id)
    {
        synchronized(chats)
        {
            if(chats.containsKey(id))
                return;
        }
        TdApi.GetChat getChatReq = new TdApi.GetChat(id);
        TG.getClientInstance().send(getChatReq, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                try
                {
                    addChatToCache((TdApi.Chat) object);
                }
                catch(Exception ignored) {}
            }
        });
    }
}
