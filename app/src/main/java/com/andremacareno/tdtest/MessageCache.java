package com.andremacareno.tdtest;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.concurrent.atomic.AtomicBoolean;

import gnu.trove.TCollections;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TLongObjectHashMap;

/**
 * Created by andremacareno on 24/04/15.
 */
public class MessageCache {
    private static volatile MessageCache _instance;
    private final Object monitor = new Object();
    private final TLongObjectMap<TIntObjectHashMap<TdApi.Message>> messages = TCollections.synchronizedMap(new TLongObjectHashMap<TIntObjectHashMap<TdApi.Message>>());
    //private final ConcurrentHashMap<Integer, TdApi.UserFull> messages = new ConcurrentHashMap<>();
    private MessageCache() {
    }
    public static MessageCache getInstance()
    {
        if(_instance == null)
        {
            synchronized(MessageCache.class)
            {
                if(_instance == null)
                    _instance = new MessageCache();
            }
        }
        return _instance;
    }
    public void clear()
    {
        synchronized(messages)
        {
            messages.clear();
        }
    }
    public TdApi.Message getMessageById(long chatId, int msgId)
    {
        return getMessageById(chatId, msgId, false);
    }
    public TdApi.Message getMessageById(long chatId, int msgId, boolean force)
    {
        if(chatId == 0 || msgId == 0)
            return null;
        if(!force)
        {
            synchronized(messages) {
                if(messages.containsKey(chatId))
                {
                    try
                    {
                        TIntObjectMap<TdApi.Message> chatMessages = messages.get(chatId);
                        if(chatMessages.containsKey(msgId))
                            return chatMessages.get(msgId);
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            }
        }
        final AtomicBoolean ready = new AtomicBoolean(false);
        TdApi.GetMessage getMessageFunc = new TdApi.GetMessage(chatId, msgId);
        Client telegramClient = TG.getClientInstance();
        if(telegramClient == null)
            return null;
        telegramClient.send(getMessageFunc, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                try
                {
                    TdApi.Message msg = (TdApi.Message) object;
                    synchronized(messages)
                    {
                        if(!messages.containsKey(msg.chatId))
                            messages.put(msg.chatId, new TIntObjectHashMap<TdApi.Message>());
                        messages.get(msg.chatId).put(msg.id, msg);
                    }
                }
                finally
                {
                    ready.set(true);
                    synchronized(monitor)
                    {
                        monitor.notifyAll();
                    }
                }
            }
        });
        while (!ready.get()) {
            synchronized(monitor)
            {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        synchronized(messages)
        {
            try
            {
                return messages.get(chatId).get(msgId);
            }
            catch(Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
    }
    public void cacheMessage(long chatId, int msgId)
    {
        if(chatId == 0 || msgId == 0)
            return;
        synchronized(messages) {
            if (messages.containsKey(chatId)) {
                try {
                    TIntObjectMap<TdApi.Message> chatMessages = messages.get(chatId);
                    if (chatMessages.containsKey(msgId))
                        return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }

            TdApi.GetMessage getMessageFunc = new TdApi.GetMessage(chatId, msgId);
            Client telegramClient = TG.getClientInstance();
            if (telegramClient == null)
                return;
            telegramClient.send(getMessageFunc, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    TdApi.Message msg = (TdApi.Message) object;
                    synchronized (messages) {
                        if (!messages.containsKey(msg.chatId))
                            messages.put(msg.chatId, new TIntObjectHashMap<TdApi.Message>());
                        messages.get(msg.chatId).put(msg.id, msg);
                    }
                }
            });
        }
    }

}
