package com.andremacareno.tdtest;

import com.andremacareno.tdtest.models.AnimationMessageModel;
import com.andremacareno.tdtest.models.AudioMessageModel;
import com.andremacareno.tdtest.models.ChannelChatCreateMessageModel;
import com.andremacareno.tdtest.models.ChatJoinByLinkMessageModel;
import com.andremacareno.tdtest.models.ChatMigrateFromMessageModel;
import com.andremacareno.tdtest.models.ChatMigrateToMessageModel;
import com.andremacareno.tdtest.models.ContactMessageModel;
import com.andremacareno.tdtest.models.DeletedMessageModel;
import com.andremacareno.tdtest.models.DocumentMessageModel;
import com.andremacareno.tdtest.models.GeoPointMessageModel;
import com.andremacareno.tdtest.models.GroupChatAddParticipantMessageModel;
import com.andremacareno.tdtest.models.GroupChatChangePhotoMessageModel;
import com.andremacareno.tdtest.models.GroupChatChangeTitleMessageModel;
import com.andremacareno.tdtest.models.GroupChatCreateMessageModel;
import com.andremacareno.tdtest.models.GroupChatDelParticipantMessageModel;
import com.andremacareno.tdtest.models.GroupChatDelPhotoMessageModel;
import com.andremacareno.tdtest.models.MessageModel;
import com.andremacareno.tdtest.models.PhotoMessageModel;
import com.andremacareno.tdtest.models.StickerMessageModel;
import com.andremacareno.tdtest.models.TextMessageModel;
import com.andremacareno.tdtest.models.UnsupportedMessageModel;
import com.andremacareno.tdtest.models.VenueMessageModel;
import com.andremacareno.tdtest.models.VideoMessageModel;
import com.andremacareno.tdtest.models.VoiceMessageModel;
import com.andremacareno.tdtest.models.WebPageMessageModel;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Created by Andrew on 19.04.2015.
 */
public class MessageModelFactory {
    public static MessageModel createMsgModel(TdApi.Message msg)
    {
        int msgContentConstructor = msg.content.getConstructor();

        if(msgContentConstructor == TdApi.MessageText.CONSTRUCTOR)
            return new TextMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageVoice.CONSTRUCTOR)
            return new VoiceMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageAudio.CONSTRUCTOR)
            return new AudioMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageDocument.CONSTRUCTOR)
            return new DocumentMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessagePhoto.CONSTRUCTOR)
            return new PhotoMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageVideo.CONSTRUCTOR)
            return new VideoMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageLocation.CONSTRUCTOR)
            return new GeoPointMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageContact.CONSTRUCTOR)
            return new ContactMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageDeleted.CONSTRUCTOR)
            return new DeletedMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageGroupChatCreate.CONSTRUCTOR)
            return new GroupChatCreateMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageChatAddParticipants.CONSTRUCTOR)
            return new GroupChatAddParticipantMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageChatChangePhoto.CONSTRUCTOR)
            return new GroupChatChangePhotoMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageChatDeletePhoto.CONSTRUCTOR)
            return new GroupChatDelPhotoMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageChatDeleteParticipant.CONSTRUCTOR)
            return new GroupChatDelParticipantMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageChatJoinByLink.CONSTRUCTOR)
            return new ChatJoinByLinkMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageChatChangeTitle.CONSTRUCTOR)
            return new GroupChatChangeTitleMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageSticker.CONSTRUCTOR)
            return new StickerMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageWebPage.CONSTRUCTOR)
            return new WebPageMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageVenue.CONSTRUCTOR)
            return new VenueMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageChatMigrateFrom.CONSTRUCTOR)
            return new ChatMigrateFromMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageChatMigrateTo.CONSTRUCTOR)
            return new ChatMigrateToMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageChannelChatCreate.CONSTRUCTOR)
            return new ChannelChatCreateMessageModel(msg);
        else if(msgContentConstructor == TdApi.MessageAnimation.CONSTRUCTOR)
            return new AnimationMessageModel(msg);
        return new UnsupportedMessageModel(msg);
    }
}
