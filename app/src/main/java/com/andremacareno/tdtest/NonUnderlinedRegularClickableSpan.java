package com.andremacareno.tdtest;

import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.text.style.ClickableSpan;

/**
 * Created by andremacareno on 12/08/15.
 */
public abstract class NonUnderlinedRegularClickableSpan extends ClickableSpan {
    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setColor(ContextCompat.getColor(TelegramApplication.sharedApplication().getApplicationContext(), R.color.chat_link));
        ds.setUnderlineText(false);
    }
}
