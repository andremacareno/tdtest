package com.andremacareno.tdtest;

import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.updatelisteners.UpdateChannelProcessor;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.concurrent.atomic.AtomicBoolean;

import gnu.trove.TCollections;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * Created by andremacareno on 24/04/15.
 */
public class ChannelCache {
    private static volatile ChannelCache _instance;
    private final Object monitor = new Object();
    private final TIntObjectMap<TdApi.Channel> channels = TCollections.synchronizedMap(new TIntObjectHashMap<TdApi.Channel>());
    private final TIntObjectMap<TdApi.ChannelFull> fullChannels = TCollections.synchronizedMap(new TIntObjectHashMap<TdApi.ChannelFull>());
    private final UpdateChannelProcessor updateChannelListener;
    private ChannelCache() {
        updateChannelListener = new UpdateChannelProcessor() {
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                return Constants.CHANNEL_CACHE_UPDATE_OBSERVER;
            }
            @Override
            protected void processUpdateInBackground(TdApi.Update upd) {
                if(upd.getConstructor() == TdApi.UpdateChannel.CONSTRUCTOR)
                {
                    TdApi.UpdateChannel castUpd = (TdApi.UpdateChannel) upd;
                    synchronized(channels)
                    {
                        channels.put(castUpd.channel.id, castUpd.channel);
                        synchronized(fullChannels)
                        {
                            if(fullChannels.containsKey(castUpd.channel.id))
                                fullChannels.get(castUpd.channel.id).channel = castUpd.channel;
                        }
                    }
                }
            }

            @Override
            protected void processUpdateInUiThread(TdApi.Update upd) {
            }
        };
    }
    public void attachObserver()
    {
        TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
        if(service == null)
        {
            NotificationCenter.getInstance().addObserver(new NotificationObserver(NotificationCenter.didUpdateServiceStarted) {
                @Override
                public void didNotificationReceived(Notification notification) {
                    TelegramUpdatesHandler serviceInstance = (TelegramUpdatesHandler) notification.getObject();
                    updateChannelListener.attachObservers(serviceInstance);
                }
            });
        }
        else
            updateChannelListener.attachObservers(service);
    }
    public static ChannelCache getInstance()
    {
        if(_instance == null)
        {
            synchronized(ChannelCache.class)
            {
                if(_instance == null)
                    _instance = new ChannelCache();
            }
        }
        return _instance;
    }
    public void clear()
    {
        synchronized(channels)
        {
            channels.clear();
        }
        updateChannelListener.removeObservers();
    }
    public void removeChannelFull(int id)
    {
        synchronized(fullChannels)
        {
            fullChannels.remove(id);
        }
    }
    public TdApi.ChannelFull getChannelFull(int id)
    {
        if(id == 0)
            return null;
        synchronized(fullChannels) {
            if (fullChannels.containsKey(id))
                return fullChannels.get(id);
        }
        final AtomicBoolean ready = new AtomicBoolean(false);
        TdApi.GetChannelFull getChannelFunc = new TdApi.GetChannelFull(id);
        Client telegramClient = TG.getClientInstance();
        if(telegramClient == null)
            return null;
        telegramClient.send(getChannelFunc, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                TdApi.ChannelFull ch = (TdApi.ChannelFull) object;
                synchronized(fullChannels)
                {
                    fullChannels.put(ch.channel.id, ch);
                }
                ready.set(true);
                synchronized(monitor)
                {
                    monitor.notifyAll();
                }
            }
        });
        while (!ready.get()) {
            synchronized(monitor)
            {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        synchronized(fullChannels)
        {
            return fullChannels.get(id);
        }
    }
    public TdApi.Channel getChannel(int id)
    {
        if(id == 0)
            return null;
        synchronized(channels)
        {
            if(channels.containsKey(id))
                return channels.get(id);
        }
        synchronized(fullChannels)
        {
            if(fullChannels.containsKey(id))
                return fullChannels.get(id).channel;
        }
        final AtomicBoolean ready = new AtomicBoolean(false);
        TdApi.GetChannel getChannelFunc = new TdApi.GetChannel(id);
        Client telegramClient = TG.getClientInstance();
        if(telegramClient == null)
            return null;
        telegramClient.send(getChannelFunc, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                TdApi.Channel ch = (TdApi.Channel) object;
                synchronized(channels)
                {
                    channels.put(ch.id, ch);
                }
                ready.set(true);
                synchronized(monitor)
                {
                    monitor.notifyAll();
                }
            }
        });
        while (!ready.get()) {
            synchronized(monitor)
            {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        synchronized(channels)
        {
            return channels.get(id);
        }
    }
    public void cacheChannel(TdApi.Channel channel)
    {
        synchronized(channels)
        {
            channels.put(channel.id, channel);
        }
        synchronized(fullChannels)
        {
            if(fullChannels.containsKey(channel.id))
                fullChannels.get(channel.id).channel = channel;
        }
    }
    public boolean isCached(int channelId)
    {
        synchronized(channels)
        {
            return channels.containsKey(channelId);
        }
    }
    public boolean isCachedFull(int channelId)
    {
        synchronized(fullChannels)
        {
            return fullChannels.containsKey(channelId);
        }
    }
}
