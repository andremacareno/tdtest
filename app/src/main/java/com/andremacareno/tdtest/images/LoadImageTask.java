package com.andremacareno.tdtest.images;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.imageprocessors.BlurImage;
import com.andremacareno.tdtest.imageprocessors.CircleImage;
import com.andremacareno.tdtest.imageprocessors.ScaleImage;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.updatelisteners.TelegramUpdateListener;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.RandomAccessFile;

public class LoadImageTask extends AsyncTaskManTask {
    private final Object monitor = new Object();
    private volatile boolean ready = false;
    private String key;
    private FileModel f;
    private int postProcessingMask;
    private int scaleWidth, scaleHeight;
    public LoadImageTask(FileModel f, String k, int postProcessing, int scaleWidth, int scaleHeight)
    {
        this.key = k;
        this.f = f;
        this.postProcessingMask = postProcessing;
        this.scaleWidth = scaleWidth;
        this.scaleHeight = scaleHeight;
    }
    @Override
    public String getTaskKey() {
        return this.key;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didImageProcessingFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didImageProcessed;
    }

    @Override
    protected void work() throws Exception {

        if(!f.isDownloaded()) {
            TdApi.File local = downloadEmptyFile(f.getFileId());
            f.markDownloaded(local);
        }

        TdApi.File localFile = f.getFile();
        RandomAccessFile img = new RandomAccessFile(localFile.path, "r");
        try
        {
            Bitmap bmp = BitmapFactory.decodeFileDescriptor(img.getFD());
            if((postProcessingMask & TelegramImageLoader.POST_PROCESSING_SCALE) != 0)
            {
                ScaleImage scaleImage = new ScaleImage(scaleWidth, scaleHeight);
                Bitmap newBitmap = scaleImage.process(bmp);
                bmp.recycle();
                System.gc();
                bmp = newBitmap;
            }
            if((postProcessingMask & TelegramImageLoader.POST_PROCESSING_CIRCLE) != 0)
            {
                CircleImage circleImage = new CircleImage();
                Bitmap newBitmap = circleImage.process(bmp);
                bmp.recycle();
                System.gc();
                bmp = newBitmap;
            }
            if((postProcessingMask & TelegramImageLoader.POST_PROCESSING_BLUR) != 0)
            {
                BlurImage blurImage = new BlurImage();
                Bitmap newBitmap = blurImage.process(bmp);
                bmp.recycle();
                System.gc();
                bmp = newBitmap;
            }
            if(bmp != null)
            {
                ImageCache cache = ImageCache.sharedInstance();
                cache.put(this.key, new RecyclingBitmapDrawable(TelegramApplication.sharedApplication().getResources(), bmp));
            }
        }
        finally
        {
            img.close();
        }
    }

    private TdApi.File downloadEmptyFile(int id)
    {
        TdApi.DownloadFile downloadFile = new TdApi.DownloadFile(id);
        final TdApi.File newFile = new TdApi.File();
        FileDelegate delegate = new FileDelegate() {
            @Override
            public void didFileDownloaded(TdApi.File localFile) {
                newFile.id = localFile.id;
                newFile.size = localFile.size;
                newFile.path = localFile.path;
                newFile.persistentId = localFile.persistentId;
                ready = true;
                synchronized(monitor)
                {
                    monitor.notifyAll();
                }
            }
        };
        FileDownloadListener listener = new FileDownloadListener(id, delegate);
        TelegramApplication.sharedApplication().getUpdateService().addObserver(listener);
        Client telegramClient = TG.getClientInstance();
        telegramClient.send(downloadFile, TelegramApplication.dummyResultHandler);


        while (!ready) {
            try {
                synchronized (monitor) {
                    monitor.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return newFile;
    }

    private abstract class FileUpdateListener extends TelegramUpdateListener
    {
        protected int file_id;
        protected FileDelegate delegate;
        public FileUpdateListener(int f, FileDelegate delegate)
        {
            this.file_id = f;
            this.delegate = delegate;
        }
    }

    private class FileDownloadListener extends FileUpdateListener
    {
        private String key;
        public FileDownloadListener(int f, FileDelegate delegate)
        {
            super(f, delegate);
            this.key = String.format("file%d_loading_observer", file_id);
        }
        @Override
        public int getObservableConstructor() {
            return TdApi.UpdateFile.CONSTRUCTOR;
        }

        @Override
        public String getKey() {
            return this.key;
        }

        @Override
        public void onResult(TdApi.TLObject object) {
            TdApi.UpdateFile upd = (TdApi.UpdateFile) object;
            if(upd.file.id != this.file_id)
                return;
            TdApi.File fl = upd.file;
            NotificationCenter.getInstance().postNotification(NotificationCenter.didFileDownloaded, fl);
            TelegramApplication.sharedApplication().getUpdateService().removeObserver(this);
            delegate.didFileDownloaded(fl);
        }
    }
    private interface FileDelegate
    {
        public void didFileDownloaded(TdApi.File f);
    }
}
