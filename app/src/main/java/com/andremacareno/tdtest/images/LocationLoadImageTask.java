package com.andremacareno.tdtest.images;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.imageprocessors.AddPinToImage;
import com.andremacareno.tdtest.utils.AndroidUtilities;

import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class LocationLoadImageTask extends AsyncTaskManTask {
    private TdApi.Location location;
    private String key;
    private int w, h;
    public LocationLoadImageTask(TdApi.Location loc, String k, int width, int height)
    {
        this.key = k;
        this.w = width;
        this.h = height;
        this.location = loc;
    }
    @Override
    public String getTaskKey() {
        return this.key;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didImageProcessingFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didImageProcessed;
    }

    @Override
    protected void work() throws Exception {
        File cacheDir = TelegramApplication.sharedApplication().getApplicationContext().getExternalCacheDir();
        File locationsDirectory = new File(cacheDir, "/locations");
        locationsDirectory.mkdir();
        File image = new File(locationsDirectory, String.format("%f_%f_%dx%d.png", location.latitude, location.longitude, w, h));
        Bitmap bmp = image.exists() ? BitmapFactory.decodeFile(image.getAbsolutePath()) : download(image);
        if(bmp == null)
            return;
        ImageCache cache = ImageCache.sharedInstance();
        cache.put(this.key, new RecyclingBitmapDrawable(TelegramApplication.sharedApplication().getResources(), bmp));
    }

    protected Bitmap download(File pathToWrite) {
        Bitmap img = null;
        HttpURLConnection connection = null;
        try {
            URL link = new URL(String.format(Constants.STATIC_MAP_URL_FORMAT, location.latitude, location.longitude, w, h));
            connection = (HttpURLConnection) link.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            img = BitmapFactory.decodeStream(input);
            AddPinToImage processing = new AddPinToImage(w <= AndroidUtilities.dp(50));
            Bitmap out = processing.process(img);
            if(out != null)
            {
                img.recycle();
                img = out;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally
        {
            if(connection != null)
                connection.disconnect();
        }
        if(img != null)
        {
            try {
                FileOutputStream stream = new FileOutputStream(pathToWrite);
                img.compress(Bitmap.CompressFormat.PNG, 100, stream);
                stream.close();
                return img;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
