package com.andremacareno.tdtest.images;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ImageView;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.CommonTools;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.models.GalleryImageEntry;
import com.andremacareno.tdtest.utils.AndroidUtilities;
import com.andremacareno.tdtest.views.AlbumArtImageView;

import org.drinkless.td.libcore.telegram.TdApi;

import java.lang.ref.WeakReference;

/**
 * Created by Andrew on 03.05.2015.
 */
public class TelegramImageLoader {
    private static final String TAG = "TelegramImageLoader";
    public static final int POST_PROCESSING_NOTHING = 0;
    public static final int POST_PROCESSING_CIRCLE = 1;
    public static final int POST_PROCESSING_SCALE = 2;
    public static final int POST_PROCESSING_BLUR = 4;

    public static final int CMD_RETURN_STICKER_THUMB = 0;
    public static final int CMD_RETURN_STICKER_FULL_SIZE = 1;

    private static volatile TelegramImageLoader _instance;

    private TelegramImageLoader()
    {
    }

    public static TelegramImageLoader getInstance()
    {
        if(_instance == null)
        {
            synchronized(TelegramImageLoader.class)
            {
                if(_instance == null)
                    _instance = new TelegramImageLoader();
            }
        }
        return _instance;
    }
    public void loadImage(FileModel f, String key, ImageView imageView, int postProcessingMask, int scaleWidth, int scaleHeight, boolean forceUpdate)
    {
        if (key == null) {
            return;
        }
        BitmapDrawable value = ImageCache.sharedInstance().get(key);

        if (value != null && !forceUpdate) {
            // Bitmap found in memory cache
            imageView.setImageDrawable(value);
        } else if (cancelPotentialWork(key, imageView)) {
            //BEGIN_INCLUDE(execute_background_task)
            final ImageObserver obs = new ImageObserver(key, imageView);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(TelegramApplication.sharedApplication().getResources(), null, obs);
            imageView.setImageDrawable(asyncDrawable);
            NotificationCenter.getInstance().addObserver(obs);
            LoadImageTask task = new LoadImageTask(f, key, postProcessingMask, scaleWidth, scaleHeight);
            task.addToQueue();
        }
    }
    public void loadAnimation(TdApi.Animation anim, String key, ImageView imageView)
    {
        if (key == null) {
            return;
        }
        if (cancelPotentialWork(key, imageView)) {
            //BEGIN_INCLUDE(execute_background_task)
            final ImageObserver obs = new ImageObserver(key, imageView);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(TelegramApplication.sharedApplication().getResources(), null, obs);
            imageView.setImageDrawable(asyncDrawable);
            NotificationCenter.getInstance().addObserver(obs);
            LoadAnimationTask task = new LoadAnimationTask(anim, key);
            task.addToQueue();
        }
    }
    public void extractAlbumArt(String pathToFile, String key, ImageView imageView)
    {
        if (key == null) {
            return;
        }
        BitmapDrawable value = ImageCache.sharedInstance().get(key);

        if (value != null) {
            // Bitmap found in memory cache
            imageView.setImageDrawable(value);
        } else if (cancelPotentialWork(key, imageView)) {
            //BEGIN_INCLUDE(execute_background_task)
            final ImageObserver obs = new ImageObserver(key, imageView);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(TelegramApplication.sharedApplication().getResources(), null, obs);
            imageView.setImageDrawable(asyncDrawable);
            NotificationCenter.getInstance().addObserver(obs);
            LoadAlbumCoverTask task = new LoadAlbumCoverTask(pathToFile, key);
            task.addToQueue();
        }
    }
    public void loadSticker(TdApi.Sticker obj, int whatToDisplay, ImageView imageView)
    {
        String thumbKey = String.format(Constants.STICKER_THUMB_KEY, obj.setId, obj.sticker.id);
        String fullSizeKey = String.format(Constants.STICKER_FULL_KEY, obj.setId, obj.sticker.id);
        String key = whatToDisplay == CMD_RETURN_STICKER_THUMB ? thumbKey : fullSizeKey;
        BitmapDrawable value = ImageCache.sharedInstance().get(key);

        if (value != null) {
            // Bitmap found in memory cache
            imageView.setImageDrawable(value);
        } else if (cancelPotentialWork(key, imageView)) {
            //BEGIN_INCLUDE(execute_background_task)
            final ImageObserver obs = new ImageObserver(key, imageView);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(TelegramApplication.sharedApplication().getResources(), null, obs);
            imageView.setImageDrawable(asyncDrawable);
            NotificationCenter.getInstance().addObserver(obs);
            if(whatToDisplay == CMD_RETURN_STICKER_THUMB) {
                WebPLoadImageTask thumbTask = new WebPLoadImageTask(FileCache.getInstance().getFileModel(obj.thumb.photo), thumbKey);
                thumbTask.addToQueue();
            }
            else {
                WebPLoadImageTask fullSizeTask = new WebPLoadImageTask(FileCache.getInstance().getFileModel(obj.sticker), fullSizeKey);
                fullSizeTask.addToQueue();
            }
        }
    }
    public void loadPlaceholder(int id, String text, String key, ImageView imageView, boolean forceUpdate)
    {
        if(text == null || key == null)
            return;
        BitmapDrawable value = ImageCache.sharedInstance().get(key);
        if(value != null && !forceUpdate)
            imageView.setImageDrawable(value);
        else if(cancelPotentialWork(key, imageView))
        {
            final ImageObserver obs = new ImageObserver(key, imageView);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(TelegramApplication.sharedApplication().getResources(), null, obs);
            imageView.setImageDrawable(asyncDrawable);
            NotificationCenter.getInstance().addObserver(obs);
            CreatePlaceholderTask task = new CreatePlaceholderTask(key, text, id);
            task.addToQueue();
        }
    }
    public void loadLocation(TdApi.Location location, String key, ImageView imageView, int width, int height)
    {
        if(key == null)
            return;
        BitmapDrawable value = ImageCache.sharedInstance().get(key);
        if(value != null)
            imageView.setImageDrawable(value);
        else if(cancelPotentialWork(key, imageView))
        {
            final ImageObserver obs = new ImageObserver(key, imageView);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(TelegramApplication.sharedApplication().getResources(), null, obs);
            imageView.setImageDrawable(asyncDrawable);
            NotificationCenter.getInstance().addObserver(obs);
            LocationLoadImageTask task = new LocationLoadImageTask(location, key, width, height);
            task.addToQueue();
        }
    }
    public void loadArticleThumb(String thumbUrl, ImageView imageView)
    {
        if(thumbUrl == null)
            return;
        String key = thumbUrl;
        BitmapDrawable value = ImageCache.sharedInstance().get(key);
        if(value != null)
            imageView.setImageDrawable(value);
        else if(cancelPotentialWork(key, imageView))
        {
            final ImageObserver obs = new ImageObserver(key, imageView);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(TelegramApplication.sharedApplication().getResources(), null, obs);
            imageView.setImageDrawable(asyncDrawable);
            NotificationCenter.getInstance().addObserver(obs);
            LoadArticleThumbTask task = new LoadArticleThumbTask(thumbUrl);
            task.addToQueue();
        }
    }
    public void loadInlinePhoto(TdApi.InlineQueryResult result, ImageView iv)
    {
        if(result == null || (result.getConstructor() != TdApi.InlineQueryResultCachedPhoto.CONSTRUCTOR && result.getConstructor() != TdApi.InlineQueryResultPhoto.CONSTRUCTOR))
            return;
        String key = null;
        FileModel suitableCachedPhoto = null;
        float aspectRatio = 1.0f;
        if(result.getConstructor() == TdApi.InlineQueryResultCachedPhoto.CONSTRUCTOR)
        {
            TdApi.InlineQueryResultCachedPhoto cast = (TdApi.InlineQueryResultCachedPhoto) result;
            suitableCachedPhoto = FileCache.getInstance().getFileModel(cast.photo.photos[CommonTools.indexOfSuitablePreview(cast.photo.photos)].photo);
            key = CommonTools.makeFileKey(suitableCachedPhoto.getFileId());
            aspectRatio = cast.photo.photos[CommonTools.indexOfMaxResPhoto(cast.photo.photos)].width / cast.photo.photos[CommonTools.indexOfMaxResPhoto(cast.photo.photos)].height;
        }
        else
        {
            TdApi.InlineQueryResultPhoto cast = (TdApi.InlineQueryResultPhoto) result;
            key = cast.thumbUrl;
        }
        BitmapDrawable value = ImageCache.sharedInstance().get(key);
        if(value != null)
            iv.setImageDrawable(value);
        else if(cancelPotentialWork(key, iv))
        {
            final ImageObserver obs = new ImageObserver(key, iv);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(TelegramApplication.sharedApplication().getResources(), null, obs);
            iv.setImageDrawable(asyncDrawable);
            NotificationCenter.getInstance().addObserver(obs);
            AsyncTaskManTask task = result.getConstructor() == TdApi.InlineQueryResultPhoto.CONSTRUCTOR ? new LoadNonCachedInlinePhotoTask(((TdApi.InlineQueryResultPhoto) result).thumbUrl) : new LoadImageTask(suitableCachedPhoto, key, POST_PROCESSING_SCALE, Math.round(AndroidUtilities.dp(128)*aspectRatio), AndroidUtilities.dp(128));
            task.addToQueue();
        }
    }
    public void loadInlineAnimation(TdApi.InlineQueryResult result, AnimationView iv)
    {
        if(result == null || (result.getConstructor() != TdApi.InlineQueryResultCachedAnimation.CONSTRUCTOR && result.getConstructor() != TdApi.InlineQueryResultAnimatedGif.CONSTRUCTOR && result.getConstructor() != TdApi.InlineQueryResultAnimatedMpeg4.CONSTRUCTOR))
            return;
        TdApi.Animation anim = null;
        String key;
        if(result.getConstructor() == TdApi.InlineQueryResultCachedAnimation.CONSTRUCTOR)
        {
            TdApi.InlineQueryResultCachedAnimation cast = (TdApi.InlineQueryResultCachedAnimation) result;
            anim = cast.animation;
            key = CommonTools.makeFileKey(anim.animation.id);
        }
        else if(result.getConstructor() == TdApi.InlineQueryResultAnimatedGif.CONSTRUCTOR)
        {
            TdApi.InlineQueryResultAnimatedGif cast = (TdApi.InlineQueryResultAnimatedGif) result;
            key = cast.gifUrl;
        }
        else
        {
            TdApi.InlineQueryResultAnimatedMpeg4 cast = (TdApi.InlineQueryResultAnimatedMpeg4) result;
            key = cast.mpeg4Url;
        }
        BitmapDrawable value = ImageCache.sharedInstance().get(key);
        if(value != null)
            iv.setImageDrawable(value);
        else if(cancelPotentialWork(key, iv))
        {
            final ImageObserver obs = new ImageObserver(key, iv);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(TelegramApplication.sharedApplication().getResources(), null, obs);
            iv.setImageDrawable(asyncDrawable);
            NotificationCenter.getInstance().addObserver(obs);
            AsyncTaskManTask task = result.getConstructor() == TdApi.InlineQueryResultCachedAnimation.CONSTRUCTOR ? new LoadAnimationTask(anim, key) : new LoadNonCachedInlineAnimationTask(key);
            task.addToQueue();
        }
    }
    public void loadGalleryThumb(GalleryImageEntry entry, ImageView imageView)
    {
        if(entry == null)
            return;
        BitmapDrawable value = ImageCache.sharedInstance().get(entry.getCacheKey());
        if(value != null)
            imageView.setImageDrawable(value);
        else if(cancelPotentialWork(entry.getCacheKey(), imageView))
        {
            final ImageObserver obs = new ImageObserver(entry.getCacheKey(), imageView);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(TelegramApplication.sharedApplication().getResources(), null, obs);
            imageView.setImageDrawable(asyncDrawable);
            NotificationCenter.getInstance().addObserver(obs);
            LoadThumbnailTask task = new LoadThumbnailTask(entry);
            task.addToQueue();
        }
    }
    public void loadAvatarPreview(String path, ImageView imageView)
    {
        if(path == null)
            return;
        String cacheKey = String.format(Constants.AVATAR_PREVIEW_FORMAT, path);
        BitmapDrawable value = ImageCache.sharedInstance().get(cacheKey);
        if(value != null)
            imageView.setImageDrawable(value);
        else if(cancelPotentialWork(cacheKey, imageView))
        {
            final ImageObserver obs = new ImageObserver(cacheKey, imageView);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(TelegramApplication.sharedApplication().getResources(), null, obs);
            imageView.setImageDrawable(asyncDrawable);
            NotificationCenter.getInstance().addObserver(obs);
            LoadAvatarPreviewTask task = new LoadAvatarPreviewTask(path);
            task.addToQueue();
        }

    }

    /**
     * A custom Drawable that will be attached to the imageView while the work is in progress.
     * Contains a reference to the actual notification observer, so that it can be removed if a new binding is
     * required, and makes sure that only the last observer can bind its result,
     * independently of the finish order.
     */
    private static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<ImageObserver> observerRef;

        public AsyncDrawable(Resources res, Bitmap bitmap, ImageObserver obs) {
            super(res, bitmap);
            observerRef =
                    new WeakReference<ImageObserver>(obs);
        }

        public ImageObserver getImageObserver() {
            return observerRef.get();
        }
    }

    private static class ImageObserver extends NotificationObserver {
        private final WeakReference<ImageView> imageViewReference;
        private Runnable setImageDrawableRunnable;
        public ImageObserver(String k, ImageView imageView) {
            super(NotificationCenter.didImageCached, k);
            imageViewReference = new WeakReference<ImageView>(imageView);
            setImageDrawableRunnable = new Runnable() {
                @Override
                public void run() {
                    BitmapDrawable dr = ImageCache.sharedInstance().get(getStringKey());
                    ImageView attachedIV = getAttachedImageView();
                    if(attachedIV != null) {
                        setImageDrawable(attachedIV, dr);
                    }
                }
            };
        }
        private void setImageDrawable(ImageView imageView, Drawable drawable) {
            try
            {
                if(imageView == null)
                    return;
                if(drawable == null) {
                    imageView.setImageDrawable(null);
                    return;
                }
                if(imageView instanceof AlbumArtImageView || imageView instanceof AnimationView) {
                    imageView.setImageDrawable(drawable);
                }
                else
                {
                    final TransitionDrawable td = new TransitionDrawable(new Drawable[] {
                            new ColorDrawable(ContextCompat.getColor(imageView.getContext(), android.R.color.transparent)),
                            drawable
                    });
                    imageView.setImageDrawable(td);
                    td.startTransition(200);
                }
            }
            catch(Exception e) { e.printStackTrace(); }
        }
        @Override
        public void didNotificationReceived(Notification notification) {
            if(notification.getObject() instanceof AnimatedFileDrawable)
            {
                final AnimatedFileDrawable anim = (AnimatedFileDrawable) notification.getObject();
                if(anim.getKey() != null && getStringKey().equals(anim.getKey()))
                {
                    final ImageView attachedIV = getAttachedImageView();
                    if(attachedIV != null) {
                        attachedIV.post(new Runnable() {
                            @Override
                            public void run() {
                                if(BuildConfig.DEBUG)
                                    Log.d(TAG, anim.toString());
                                setImageDrawable(attachedIV, anim);
                            }
                        });
                        NotificationCenter.getInstance().removeObserver(this);
                    }
                }
                return;
            }
            if(getStringKey().equals(notification.getObject()))
            {
                ImageView attachedIV = getAttachedImageView();
                if(attachedIV != null) {
                    attachedIV.post(setImageDrawableRunnable);
                    NotificationCenter.getInstance().removeObserver(this);
                }
            }
        }
        public String getStringKey() { return (String) getKey(); }
        /**
         * Returns the ImageView associated with this task as long as the ImageView's task still
         * points to this task as well. Returns null otherwise.
         */
        private ImageView getAttachedImageView() {
            final ImageView imageView = imageViewReference.get();
            final ImageObserver bitmapWorkerTask = getImageObserver(imageView);

            if (this == bitmapWorkerTask) {
                return imageView;
            }

            return null;
        }
    }
    private static ImageObserver getImageObserver(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getImageObserver();
            }
        }
        return null;
    }
    public static boolean cancelPotentialWork(String key, ImageView imageView) {
        //BEGIN_INCLUDE(cancel_potential_work)
        final ImageObserver observer = getImageObserver(imageView);

        if (observer != null) {
            final String bmpKey = observer.getStringKey();
            if (bmpKey == null || !bmpKey.equals(key)) {
                NotificationCenter.getInstance().removeObserver(observer);
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "cancelPotentialWork - removed observer for " + key);
                }
            } else {
                // The same work is already in progress.
                return false;
            }
        }
        return true;
        //END_INCLUDE(cancel_potential_work)
    }
}
