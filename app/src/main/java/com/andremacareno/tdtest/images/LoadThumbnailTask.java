package com.andremacareno.tdtest.images;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.GalleryImageEntry;
import com.andremacareno.tdtest.utils.AndroidUtilities;

public class LoadThumbnailTask extends AsyncTaskManTask {
    private GalleryImageEntry entry;
    public LoadThumbnailTask(GalleryImageEntry e)
    {
        this.entry = e;
    }
    @Override
    public String getTaskKey() {
        return this.entry.getCacheKey();
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didGalleryPhotoThumbnailCached;
    }

    @Override
    protected void work() throws Exception {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(entry.getPath(), o);
        final int REQUIRED_WIDTH=AndroidUtilities.dp(100);
        final int REQUIRED_HEIGHT=REQUIRED_WIDTH;
        int scale=1;
        while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HEIGHT)
            scale*=2;

        //Decode with inSampleSize
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = false;
        opts.inSampleSize=scale;

        Bitmap bmp = BitmapFactory.decodeFile(entry.getPath(), opts);
        if(bmp == null)
            return;
        if(BuildConfig.DEBUG)
            Log.d("LoadThumbs", String.format("image size: %d x %d", opts.outWidth, opts.outHeight));
        float scaleFactor = opts.outWidth > opts.outHeight ? opts.outWidth / AndroidUtilities.dp(100) : opts.outHeight / AndroidUtilities.dp(100);
        opts.outWidth /= scaleFactor;
        opts.outHeight /= scaleFactor;
        Bitmap finalBmp = Bitmap.createScaledBitmap(bmp, opts.outWidth, opts.outHeight, true);
        ImageCache cache = ImageCache.sharedInstance();
        cache.put(this.entry.getCacheKey(), new RecyclingBitmapDrawable(TelegramApplication.sharedApplication().getResources(), finalBmp));
    }
}
