package com.andremacareno.tdtest.images;

import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.support.v4.content.ContextCompat;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.R;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.utils.AndroidUtilities;

/**
 * Created by Andrew on 03.05.2015.
 */
public class CreatePlaceholderTask extends AsyncTaskManTask {
    private String text;
    private String key;
    private String taskKey;
    private int id;
    public CreatePlaceholderTask(String key, String text, int id)
    {
        this.text = text;
        this.key = key;
        this.id = id;
        this.taskKey = String.format("generate_avatar_%d", id);
    }
    @Override
    public String getTaskKey() {
        return this.taskKey;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didPlaceholderCreationFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didPlaceholderGenerated;
    }

    @Override
    protected void work() throws Exception {
        TypedArray ta = TelegramApplication.sharedApplication().getResources().obtainTypedArray(R.array.avatar_colors);
        int[] backgroundColors = new int[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            backgroundColors[i] = ta.getColor(i, 0);
        }
        ta.recycle();
        Paint textPaint = new Paint(Paint.SUBPIXEL_TEXT_FLAG);
        textPaint.setAntiAlias(true);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_placeholder_text));
        textPaint.setColor(ContextCompat.getColor(TelegramApplication.sharedApplication().getApplicationContext(), R.color.white));
        textPaint.setTypeface(AndroidUtilities.getTypeface("rmedium.ttf"));
        int avatarSize = TelegramApplication.sharedApplication().getResources().getDimensionPixelSize(R.dimen.avatar_size);
        Bitmap bmp1 = Bitmap.createBitmap(avatarSize, avatarSize, Bitmap.Config.ARGB_8888);
        Canvas c1 = new Canvas(bmp1);
        Paint p1 = new Paint();
        p1.setColor(backgroundColors[Math.abs(id) % backgroundColors.length]);
        c1.drawRect(0, 0, avatarSize, avatarSize, p1);
        c1.drawText(text, avatarSize / 2, (avatarSize + textPaint.getTextSize() / 2)/ 2, textPaint);


        Bitmap output = Bitmap.createBitmap(avatarSize, avatarSize, Bitmap.Config.ARGB_8888);
        Canvas c2 = new Canvas(output);
        Paint p2 = new Paint();
        p2.setColor(Color.WHITE);
        p2.setAntiAlias(true);
        c2.drawARGB(0, 0, 0, 0);
        c2.drawCircle(avatarSize / 2, avatarSize / 2, Math.min(avatarSize, avatarSize) / 2, p2);
        p2.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        c2.drawBitmap(bmp1, 0, 0, p2);
        bmp1.recycle();
        RecyclingBitmapDrawable drawable = new RecyclingBitmapDrawable(TelegramApplication.sharedApplication().getResources(), output);
        ImageCache.sharedInstance().put(key, drawable);
    }
}
