package com.andremacareno.tdtest.images;

import android.graphics.Bitmap;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.StickersCache;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.updatelisteners.TelegramUpdateListener;
import com.google.webp.libwebp;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

/**
 * Created by Andrew on 03.05.2015.
 */
public class WebPLoadImageTask extends AsyncTaskManTask {
    private final Object monitor = new Object();
    private volatile boolean ready = false;
    private String key;
    private FileModel f;
    public WebPLoadImageTask(FileModel f, String k)
    {
        this.key = k;
        this.f = f;
    }
    @Override
    public String getTaskKey() {
        return this.key;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didImageProcessingFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didImageProcessed;
    }

    @Override
    protected void work() throws Exception {

        if(!f.isDownloaded()) {
            TdApi.File local = downloadEmptyFile(f.getFileId());
            f.markDownloaded(local);
        }

        TdApi.File localFile = f.getFile();
        RandomAccessFile img = new RandomAccessFile(localFile.path, "r");
        byte[] webpEncoded = new byte[(int) img.length()];
        img.readFully(webpEncoded);
        img.close();
        Bitmap bmp = webpToBitmap(webpEncoded);

        if(bmp != null)
        {
            StickersCache.addImageCacheKey(this.key);
            ImageCache cache = ImageCache.sharedInstance();
            cache.put(this.key, new RecyclingBitmapDrawable(TelegramApplication.sharedApplication().getResources(), bmp));
        }
    }

    private TdApi.File downloadEmptyFile(int id)
    {
        TdApi.DownloadFile downloadFile = new TdApi.DownloadFile(id);
        final TdApi.File newFile = new TdApi.File();
        FileDelegate delegate = new FileDelegate() {
            @Override
            public void didFileDownloaded(TdApi.File localFile) {
            synchronized(monitor)
            {
                newFile.id = localFile.id;
                newFile.size = localFile.size;
                newFile.path = localFile.path;
                newFile.persistentId = localFile.persistentId;
                ready = true;
                monitor.notifyAll();
            }
            }
        };
        synchronized (monitor) {
            FileDownloadListener listener = new FileDownloadListener(id, delegate);
            TelegramApplication.sharedApplication().getUpdateService().addObserver(listener);
            Client telegramClient = TG.getClientInstance();
            telegramClient.send(downloadFile, TelegramApplication.dummyResultHandler);
            while (!ready) {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return newFile;
    }

    private abstract class FileUpdateListener extends TelegramUpdateListener
    {
        protected int file_id;
        protected FileDelegate delegate;
        public FileUpdateListener(int f, FileDelegate delegate)
        {
            this.file_id = f;
            this.delegate = delegate;
        }
    }

    private class FileDownloadListener extends FileUpdateListener
    {
        private String key;
        public FileDownloadListener(int f, FileDelegate delegate)
        {
            super(f, delegate);
            this.key = String.format("file%d_loading_observer", file_id);
        }
        @Override
        public int getObservableConstructor() {
            return TdApi.UpdateFile.CONSTRUCTOR;
        }

        @Override
        public String getKey() {
            return this.key;
        }

        @Override
        public void onResult(TdApi.TLObject object) {
            TdApi.UpdateFile upd = (TdApi.UpdateFile) object;
            if(upd.file.id != this.file_id)
                return;
            TdApi.File fl = upd.file;
            NotificationCenter.getInstance().postNotification(NotificationCenter.didFileDownloaded, fl);
            TelegramApplication.sharedApplication().getUpdateService().removeObserver(this);
            delegate.didFileDownloaded(fl);
        }
    }
    private interface FileDelegate
    {
        public void didFileDownloaded(TdApi.File f);
    }

    private Bitmap webpToBitmap(byte[] encoded) {

        int[] width = new int[] { 0 };
        int[] height = new int[] { 0 };
        byte[] decoded = libwebp.WebPDecodeARGB(encoded, encoded.length, width, height);

        int[] pixels = new int[decoded.length / 4];
        ByteBuffer.wrap(decoded).asIntBuffer().get(pixels);

        return Bitmap.createBitmap(pixels, width[0], height[0], Bitmap.Config.ARGB_8888);

    }
}
