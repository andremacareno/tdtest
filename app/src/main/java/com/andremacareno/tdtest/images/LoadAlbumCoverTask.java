package com.andremacareno.tdtest.images;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.models.AlbumCoverDrawable;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoadAlbumCoverTask extends AsyncTaskManTask {
    private final Object monitor = new Object();
    private String key;
    private String path;
    public LoadAlbumCoverTask(String p, String k)
    {
        this.key = k;
        this.path = p;
    }
    @Override
    public String getTaskKey() {
        return this.key;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didAlbumArtExtractingFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didAlbumArtExtracted;
    }

    @Override
    protected void work() throws Exception {
        android.media.MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        File fileObj = new File(path);
        FileInputStream fis;
        fis = new FileInputStream(fileObj);
        FileDescriptor fd = fis.getFD();
        mmr.setDataSource(fd);
        byte [] data = mmr.getEmbeddedPicture();
        try
        {
            if(data != null)
            {
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                ImageCache cache = ImageCache.sharedInstance();

                AlbumCoverDrawable drawable = new AlbumCoverDrawable(TelegramApplication.sharedApplication().getResources(), bitmap);
                drawable.setHasCover(true);
                drawable.setHasDarkCover(isColorDark(getDominantColor(bitmap)));
                cache.put(this.key, drawable);
            }
            else
            {
                Bitmap bmp = Bitmap.createBitmap(1, 1, Bitmap.Config.RGB_565);
                Canvas c = new Canvas(bmp);
                c.drawColor(Color.rgb(0, 0, 0));
                AlbumCoverDrawable drawable = new AlbumCoverDrawable(TelegramApplication.sharedApplication().getResources(), bmp);
                drawable.setHasCover(false);
                ImageCache.sharedInstance().put(this.key, drawable);
            }
        }
        catch(Exception e)
        {
            Bitmap bmp = Bitmap.createBitmap(1, 1, Bitmap.Config.RGB_565);
            Canvas c = new Canvas(bmp);
            c.drawColor(Color.rgb(0, 0, 0));
            AlbumCoverDrawable drawable = new AlbumCoverDrawable(TelegramApplication.sharedApplication().getResources(), bmp);
            drawable.setHasCover(false);
            ImageCache.sharedInstance().put(this.key, drawable);
        }
        finally {
            fis.close();
        }
    }

    private int getDominantColor(Bitmap bitmap) {

        if (bitmap == null)
            throw new NullPointerException();

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int size = width * height;
        int pixels[] = new int[size];

        Bitmap bitmap2 = bitmap.copy(Bitmap.Config.ARGB_4444, false);

        bitmap2.getPixels(pixels, 0, width, 0, 0, width, height);

        final List<HashMap<Integer, Integer>> colorMap = new ArrayList<HashMap<Integer, Integer>>();
        colorMap.add(new HashMap<Integer, Integer>());
        colorMap.add(new HashMap<Integer, Integer>());
        colorMap.add(new HashMap<Integer, Integer>());

        int color = 0;
        int r = 0;
        int g = 0;
        int b = 0;
        Integer rC, gC, bC;
        for (int pixel : pixels) {
            color = pixel;

            r = Color.red(color);
            g = Color.green(color);
            b = Color.blue(color);

            rC = colorMap.get(0).get(r);
            if (rC == null)
                rC = 0;
            colorMap.get(0).put(r, ++rC);

            gC = colorMap.get(1).get(g);
            if (gC == null)
                gC = 0;
            colorMap.get(1).put(g, ++gC);

            bC = colorMap.get(2).get(b);
            if (bC == null)
                bC = 0;
            colorMap.get(2).put(b, ++bC);
        }

        int[] rgb = new int[3];
        for (int i = 0; i < 3; i++) {
            int max = 0;
            int val = 0;
            for (Map.Entry<Integer, Integer> entry : colorMap.get(i).entrySet()) {
                if (entry.getValue() > max) {
                    max = entry.getValue();
                    val = entry.getKey();
                }
            }
            rgb[i] = val;
        }

        int dominantColor = Color.rgb(rgb[0], rgb[1], rgb[2]);
        bitmap2.recycle();
        return dominantColor;
    }
    private boolean isColorDark(int color){
        double darkness = 1-(0.299*Color.red(color) + 0.587*Color.green(color) + 0.114*Color.blue(color))/255;
        if(darkness<0.5){
            return false; // It's a light color
        }else{
            return true; // It's a dark color
        }
    }
}
