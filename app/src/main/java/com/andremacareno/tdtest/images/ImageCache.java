package com.andremacareno.tdtest.images;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;

import com.andremacareno.asynccore.LruCache;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.StickersCache;
import com.andremacareno.tdtest.TelegramApplication;

/**
 * Created by Andrew on 19.01.14.
 */
public class ImageCache {
    private final String TAG = "ImageCache";
    private static volatile ImageCache _instance;
    private final LruCache<String, RecyclingBitmapDrawable> cache;
    //String is an id; bitmap is an image
    public ImageCache(int maxSizeBytes)
    {
        this.cache = new LruCache<String, RecyclingBitmapDrawable>(maxSizeBytes)
        {
            @Override
            protected int sizeOf(String key, RecyclingBitmapDrawable value)
            {
                Bitmap bmp = value.getBitmap();
                if(Build.VERSION.SDK_INT >= 19)
                    return bmp.getAllocationByteCount();
                if(Build.VERSION.SDK_INT >= 12)
                    return bmp.getByteCount();
                else
                    return bmp.getRowBytes() * bmp.getHeight();
            }
            @Override
            protected void entryRemoved(boolean evicted, String key, RecyclingBitmapDrawable oldValue, RecyclingBitmapDrawable newValue)
            {
                oldValue.setIsCached(false);
                if(newValue == null)
                    StickersCache.removeImageCacheKey(key);
            }
        };
    }
    public RecyclingBitmapDrawable get(String key)
    {
        synchronized(cache)
        {
            BitmapDrawable drawable = cache.get(key);
            if(drawable != null && (drawable.getBitmap() == null || drawable.getBitmap().isRecycled())) {
                cache.remove(key);
                return null;
            }
            return cache.get(key);
        }
    }
    public void put(String key, RecyclingBitmapDrawable bmp)
    {
        if(bmp instanceof AnimatedFileDrawable)
        {
            NotificationCenter.getInstance().postNotification(NotificationCenter.didImageCached, bmp);
            return;
        }
        synchronized(cache)
        {
            cache.put(key, bmp);
            bmp.setIsCached(true);
            NotificationCenter.getInstance().postNotification(NotificationCenter.didImageCached, key);
        }
    }
    public void evictAll()
    {
        synchronized(cache)
        {
            cache.evictAll();
        }
    }
    public int size() { return cache.size(); }
    public void trimToSize(int size) {
        synchronized(cache)
        {
            cache.trimToSize(size);
        }
    }

    public static ImageCache sharedInstance() {
        if(_instance == null)
        {
            synchronized(ImageCache.class)
            {
                if(_instance == null)
                {
                    ActivityManager am = (ActivityManager) TelegramApplication.sharedApplication().getSystemService(Context.ACTIVITY_SERVICE);
                    int memoryClassBytes = am.getMemoryClass() * 1024 * 1024 / 6;
                    _instance = new ImageCache(memoryClassBytes);
                }
            }
        }
        return _instance;
    }
}
