package com.andremacareno.tdtest.images;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.models.FileModel;
import com.andremacareno.tdtest.updatelisteners.TelegramUpdateListener;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

public class LoadAnimationTask extends AsyncTaskManTask {
    private final Object monitor = new Object();
    private volatile boolean ready = false;
    private String key;
    private TdApi.Animation anim;
    public LoadAnimationTask(TdApi.Animation animation, String k)
    {
        this.key = k;
        this.anim = animation;
    }
    @Override
    public String getTaskKey() {
        return this.key;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didAnimProcessingFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didAnimProcessed;
    }

    @Override
    protected void work() throws Exception {
        FileModel f = FileCache.getInstance().getFileModel(anim.animation);
        if(!f.isDownloaded()) {
            TdApi.File local = downloadEmptyFile(f.getFileId());
            f.markDownloaded(local);
        }
        AnimatedFileDrawable drawable = new AnimatedFileDrawable(f, true);
        ImageCache cache = ImageCache.sharedInstance();
        cache.put(this.key, drawable);
    }

    private TdApi.File downloadEmptyFile(int id)
    {
        TdApi.DownloadFile downloadFile = new TdApi.DownloadFile(id);
        final TdApi.File newFile = new TdApi.File();
        FileDelegate delegate = new FileDelegate() {
            @Override
            public void didFileDownloaded(TdApi.File localFile) {
                newFile.id = localFile.id;
                newFile.size = localFile.size;
                newFile.path = localFile.path;
                newFile.persistentId = localFile.persistentId;
                ready = true;
                synchronized(monitor)
                {
                    monitor.notifyAll();
                }
            }
        };
        FileDownloadListener listener = new FileDownloadListener(id, delegate);
        TelegramApplication.sharedApplication().getUpdateService().addObserver(listener);
        Client telegramClient = TG.getClientInstance();
        telegramClient.send(downloadFile, TelegramApplication.dummyResultHandler);


        while (!ready) {
            try {
                synchronized (monitor) {
                    monitor.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return newFile;
    }

    private abstract class FileUpdateListener extends TelegramUpdateListener
    {
        protected int file_id;
        protected FileDelegate delegate;
        public FileUpdateListener(int f, FileDelegate delegate)
        {
            this.file_id = f;
            this.delegate = delegate;
        }
    }

    private class FileDownloadListener extends FileUpdateListener
    {
        private String key;
        public FileDownloadListener(int f, FileDelegate delegate)
        {
            super(f, delegate);
            this.key = String.format("file%d_loading_observer", file_id);
        }
        @Override
        public int getObservableConstructor() {
            return TdApi.UpdateFile.CONSTRUCTOR;
        }

        @Override
        public String getKey() {
            return this.key;
        }

        @Override
        public void onResult(TdApi.TLObject object) {
            TdApi.UpdateFile upd = (TdApi.UpdateFile) object;
            if(upd.file.id != this.file_id)
                return;
            TdApi.File fl = upd.file;
            NotificationCenter.getInstance().postNotification(NotificationCenter.didFileDownloaded, fl);
            TelegramApplication.sharedApplication().getUpdateService().removeObserver(this);
            delegate.didFileDownloaded(fl);
        }
    }
    private interface FileDelegate
    {
        public void didFileDownloaded(TdApi.File f);
    }
}
