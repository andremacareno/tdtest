package com.andremacareno.tdtest.images;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.imageprocessors.CircleImage;
import com.andremacareno.tdtest.utils.AndroidUtilities;

public class LoadAvatarPreviewTask extends AsyncTaskManTask {
    private String path;
    public LoadAvatarPreviewTask(String path)
    {
        this.path = path;
    }
    @Override
    public String getTaskKey() {
        return String.format("load_avatar_preview_%s", path);
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didAvatarPreviewCached;
    }

    @Override
    protected void work() throws Exception {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, o);
        final int REQUIRED_WIDTH=AndroidUtilities.dp(100);
        final int REQUIRED_HEIGHT=REQUIRED_WIDTH;
        int scale=1;
        while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HEIGHT)
            scale*=2;

        //Decode with inSampleSize
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = false;
        opts.inSampleSize=scale;

        Bitmap bmp = BitmapFactory.decodeFile(path, opts);
        if(bmp == null)
            return;
        float scaleFactor = opts.outWidth > opts.outHeight ? opts.outWidth / AndroidUtilities.dp(60) : opts.outHeight / AndroidUtilities.dp(60);
        opts.outWidth /= scaleFactor;
        opts.outHeight /= scaleFactor;
        Bitmap scaledBmp = Bitmap.createScaledBitmap(bmp, opts.outWidth, opts.outHeight, true);
        CircleImage circleImage = new CircleImage();
        Bitmap finalBmp = circleImage.process(scaledBmp);
        bmp.recycle();
        scaledBmp.recycle();
        System.gc();
        ImageCache cache = ImageCache.sharedInstance();
        cache.put(String.format(Constants.AVATAR_PREVIEW_FORMAT, path), new RecyclingBitmapDrawable(TelegramApplication.sharedApplication().getResources(), finalBmp));
    }
}
