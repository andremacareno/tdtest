package com.andremacareno.tdtest.images;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.TelegramApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class LoadNonCachedInlineAnimationTask extends AsyncTaskManTask {
    private String urlToDownload;
    public LoadNonCachedInlineAnimationTask(String url)
    {
        this.urlToDownload = url;
    }
    @Override
    public String getTaskKey() {
        return this.urlToDownload;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didAnimProcessingFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didAnimProcessed;
    }

    @Override
    protected void work() throws Exception {
        File cacheDir = TelegramApplication.sharedApplication().getApplicationContext().getExternalCacheDir();
        File thumbsDirectory = new File(cacheDir, Constants.INLINE_THUMBS_FOLDER);
        thumbsDirectory.mkdir();
        File image = new File(thumbsDirectory, String.format("%s.png", URLEncoder.encode(urlToDownload, "UTF-8")));
        try
        {
            if(!image.exists())
                download(image);
            if(!image.exists())
                return;
        }
        catch(Exception e) { e.printStackTrace(); return; }
        AnimatedFileDrawable drawable = new AnimatedFileDrawable(image.getAbsolutePath(), urlToDownload, true);
        ImageCache cache = ImageCache.sharedInstance();
        cache.put(urlToDownload, drawable);
    }

    private void download(File pathToSave) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(urlToDownload);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
                throw new IllegalStateException("Expected 200 OK status, got" + connection.getResponseCode());

            // download buffers
            input = connection.getInputStream();
            output = new FileOutputStream(pathToSave);

            byte data[] = new byte[4096];
            int count;
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }
        } catch (Exception ignored) {
        } finally {
            try {
                if (output != null) {
                    output.close();
                }
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }
            if (connection != null)
                connection.disconnect();
        }
    }
}
