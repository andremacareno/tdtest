package com.andremacareno.tdtest.images;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.andremacareno.asynccore.AsyncTaskManTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.Constants;
import com.andremacareno.tdtest.TelegramApplication;
import com.andremacareno.tdtest.imageprocessors.ScaleImage;
import com.andremacareno.tdtest.utils.AndroidUtilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class LoadNonCachedInlinePhotoTask extends AsyncTaskManTask {
    private String urlToDownload;
    private int h;
    public LoadNonCachedInlinePhotoTask(String url)
    {
        this.h = AndroidUtilities.dp(128);
        this.urlToDownload = url;
    }
    @Override
    public String getTaskKey() {
        return this.urlToDownload;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didImageProcessingFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didImageProcessed;
    }

    @Override
    protected void work() throws Exception {
        File cacheDir = TelegramApplication.sharedApplication().getApplicationContext().getExternalCacheDir();
        File thumbsDirectory = new File(cacheDir, Constants.INLINE_THUMBS_FOLDER);
        thumbsDirectory.mkdir();
        File image = new File(thumbsDirectory, String.format("%s.png", URLEncoder.encode(urlToDownload, "UTF-8")));
        Bitmap bmp = image.exists() ? BitmapFactory.decodeFile(image.getAbsolutePath()) : download(image);
        if(bmp == null)
            return;
        ImageCache cache = ImageCache.sharedInstance();
        cache.put(this.urlToDownload, new RecyclingBitmapDrawable(TelegramApplication.sharedApplication().getResources(), bmp));
    }

    protected Bitmap download(File pathToWrite) {
        Bitmap img = null;
        HttpURLConnection connection = null;
        try {
            URL link = new URL(urlToDownload);
            connection = (HttpURLConnection) link.openConnection();
            connection.setDoInput(true);
            connection.connect();
            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inJustDecodeBounds = false;
            InputStream input = connection.getInputStream();
            img = BitmapFactory.decodeStream(input, null, bitmapOptions);
            int imageWidth = bitmapOptions.outWidth;
            float thumbAspectRatio = (float) imageWidth / bitmapOptions.outHeight;
            imageWidth = Math.round(h * thumbAspectRatio);
            ScaleImage scaleImage = new ScaleImage(imageWidth, h);
            Bitmap newBmp = scaleImage.process(img);
            img.recycle();
            System.gc();
            img = newBmp;
        }
        catch(Exception ignored) {}
        finally
        {
            if(connection != null)
                connection.disconnect();
        }
        if(img != null)
        {
            try {
                FileOutputStream stream = new FileOutputStream(pathToWrite);
                img.compress(Bitmap.CompressFormat.PNG, 100, stream);
                stream.close();
                return img;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
