package com.andremacareno.tdtest;

/**
 * Created by andremacareno on 20/02/16.
 */
public enum PeerType {
    USER,
    GROUP,
    CHANNEL
}
