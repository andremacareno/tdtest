package com.andremacareno.tdtest;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Process;
import android.os.Vibrator;

import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by andremacareno on 19/08/15.
 */
public class MessageVoiceRecorder {
    public interface RecordDelegate
    {
        public void onRecordStarted();
        public void didVolumeReceived(float volumeRelative);
        public void didVoiceRecorded(TdApi.InputMessageVoice pathToRecord);
        public void onRecordError();
    }
    private static volatile MessageVoiceRecorder instance;
    private ByteBuffer fileBuffer;
    private int recordBufferSize;
    private AudioRecord recorder;
    private ArrayList<ByteBuffer> recordBuffers = new ArrayList<>();
    private volatile boolean recording = false;
    private volatile long recordingStarted;
    private volatile long lastVolumeSent = 0;
    private File recordingAudioFile;
    private int tempIndex;
    private volatile boolean cancelled = false;
    public static MessageVoiceRecorder getInstance()
    {
        if(instance == null)
        {
            synchronized(MessageVoiceRecorder.class)
            {
                if(instance == null)
                    instance = new MessageVoiceRecorder();
            }
        }
        return instance;
    }
    private MessageVoiceRecorder()
    {
        recordBufferSize = AudioRecord.getMinBufferSize(16000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        if (recordBufferSize <= 0) {
            recordBufferSize = 1280;
        }
        fileBuffer = ByteBuffer.allocateDirect(1920);
        for (int a = 0; a < 5; a++) {
            ByteBuffer buffer = ByteBuffer.allocateDirect(4096);
            recordBuffers.add(buffer);
        }
    }
    private void initRecordSystem()
    {
        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, 16000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, recordBufferSize * 10);
    }
    private void releaseAudioSystem()
    {
        if(recorder == null)
            return;
            recorder.release();
            recorder = null;
    }
    public void startRecording(RecordDelegate delegate)
    {
        if(recorder != null)
        {
            AudioController.getInstance().updateRecordingState(AudioController.RECORDING_ERROR);
            return;
        }
        AudioController.getInstance().initRecordingCommunicationBridge(delegate);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO);
                    File cacheDir = TelegramApplication.sharedApplication().getApplicationContext().getExternalCacheDir() == null ? TelegramApplication.sharedApplication().getApplicationContext().getCacheDir() : TelegramApplication.sharedApplication().getApplicationContext().getExternalCacheDir();
                    recordingAudioFile = new File(cacheDir, "inputMessageVoice".concat(String.valueOf(System.currentTimeMillis())).concat(".ogg"));
                    initRecordSystem();
                    if (AudioController.getInstance().startRecord(recordingAudioFile.getAbsolutePath()) == 0) {
                        AudioController.getInstance().updateRecordingState(AudioController.RECORDING_ERROR);
                        return;
                    }
                    recorder.startRecording();
                    recordingStarted = System.currentTimeMillis();
                    recording = true;
                    vibrate();
                    AudioController.getInstance().updateRecordingState(AudioController.RECORDING_STARTED);
                    write();
                }
                catch(Exception e)
                {
                    try
                    {
                        recordingAudioFile.delete();
                        cleanup();
                        AudioController.getInstance().updateRecordingState(AudioController.RECORDING_ERROR);
                    }
                    catch(Exception e1) { e1.printStackTrace(); }
                }
            }
        }).start();
    }

    private void cleanup()
    {
        if(recorder == null)
            return;
        try
        {
            cancelled = false;
            releaseAudioSystem();
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    private void write()
    {
        if(recorder == null)
            return;
        tempIndex = 0;
        final float tempFloatBuffer[] = new float[3];
        while(recording)
        {
            ByteBuffer buffer;
            if (!recordBuffers.isEmpty()) {
                buffer = recordBuffers.get(0);
                recordBuffers.remove(0);
            } else {
                buffer = ByteBuffer.allocateDirect(recordBufferSize);
            }
            buffer.rewind();
            final int size = recorder.read(buffer, recordBufferSize);
            if(size <= 0) {
                recordBuffers.add(buffer);
                continue;
            }
            buffer.limit(size);
            final boolean flush = size != recordBufferSize;
            final ByteBuffer finalBuffer = buffer;
            final long timestamp = System.currentTimeMillis();
            if(timestamp - lastVolumeSent > 300)       //hello 301 views
            {
                AudioController.getInstance().getVolThread().handler.post(new Runnable() {
                    @Override
                    public void run() {
                        ByteBuffer copy = finalBuffer.duplicate();
                        short sample = 0;
                        float totalAbsValue = 0;
                        for( int i=0; i<copy.limit()-1; i+=2 )
                        {
                            sample = (short)( (copy.get(i) | copy.get(i + 1) << 8 ));
                            totalAbsValue += Math.abs( sample ) / (size/2);
                        }
                        tempFloatBuffer[tempIndex%3] = totalAbsValue;
                        float temp = 0.0f;
                        for( int i=0; i<3; ++i )
                            temp += tempFloatBuffer[i];
                        float volRel;
                        if(temp <= 0)
                            volRel = 0;
                        else if(temp > 0 && temp < 1000)
                            volRel = temp / 1000.0f;
                        else
                            volRel = 1.0f;
                        lastVolumeSent = timestamp;
                        AudioController.getInstance().updateVolume(volRel);
                        tempIndex++;
                    }
                });
            }
            AudioController.getInstance().getRecThread().handler.post(new Runnable() {
                @Override
                public void run() {
                    synchronized (finalBuffer) {
                        while (finalBuffer.hasRemaining()) {
                            int oldLimit = -1;
                            if (finalBuffer.remaining() > fileBuffer.remaining()) {
                                oldLimit = finalBuffer.limit();
                                finalBuffer.limit(fileBuffer.remaining() + finalBuffer.position());
                            }
                            fileBuffer.put(finalBuffer);
                            if (fileBuffer.position() == fileBuffer.limit() || flush) {
                                if (AudioController.getInstance().writeFrame(fileBuffer, !flush ? fileBuffer.limit() : finalBuffer.position()) != 0) {
                                    fileBuffer.rewind();
                                    //recordTimeCount += fileBuffer.limit() / 2 / 16;
                                }
                            }
                            if (oldLimit != -1) {
                                finalBuffer.limit(oldLimit);
                            }
                        }
                    }
                }
            });
            recordBuffers.add(buffer);
        }
        stopRecording();
    }
    public void requestStopRecording()
    {
        cancelled = false;
        recording = false;
    }
    public void requestCancelRecording()
    {
        cancelled = true;
        recording = false;
    }

    private void stopRecording()
    {
        vibrate();
        try {
            recorder.stop();
            AudioController.getInstance().stopRecord();
        } catch (Exception e) {
            e.printStackTrace();
            if (recordingAudioFile != null) {
                recordingAudioFile.delete();
            }
            cleanup();
            return;
        }
        int duration = (int)  (System.currentTimeMillis() - recordingStarted) / 1000;
        if(cancelled)
        {
            try
            {
                recordingAudioFile.delete();
            }
            catch(Exception e) { e.printStackTrace(); }
        }
        else
        {
            TdApi.InputMessageVoice msg = new TdApi.InputMessageVoice(new TdApi.InputFileLocal(recordingAudioFile.getAbsolutePath()), duration, null);
            AudioController.getInstance().recordingFinished(msg);
        }
        cleanup();
    }


    private void vibrate()
    {
        try {
            Vibrator v = (Vibrator) TelegramApplication.sharedApplication().getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(20);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
