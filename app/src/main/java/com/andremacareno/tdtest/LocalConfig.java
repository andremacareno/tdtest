package com.andremacareno.tdtest;

/**
 * Created by andremacareno on 09/12/15.
 */
public class LocalConfig {
    private static volatile LocalConfig _instance = null;

    private boolean chatsMutedByDefault = false;
    private LocalConfig() {}
    public static LocalConfig getInstance()
    {
        if(_instance == null)
        {
            synchronized(LocalConfig.class)
            {
                if(_instance == null)
                    _instance = new LocalConfig();
            }
        }
        return _instance;
    }
    public boolean isChatsMutedByDefault() { return this.chatsMutedByDefault; }
    public void setChatsMutedByDefault(boolean val) { this.chatsMutedByDefault = val; }

}
