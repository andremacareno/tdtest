package com.andremacareno.tdtest;

import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tdtest.models.UserModel;
import com.andremacareno.tdtest.updatelisteners.UpdateUserProcessor;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.concurrent.atomic.AtomicBoolean;

import gnu.trove.TCollections;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * Created by andremacareno on 24/04/15.
 */
public class UserCache {
    private static volatile UserCache _instance;
    private final Object monitor = new Object();
    private final TIntObjectMap<UserModel> users = TCollections.synchronizedMap(new TIntObjectHashMap<UserModel>());
    private final UpdateUserProcessor updateStatusListener;
    //private final ConcurrentHashMap<Integer, TdApi.UserFull> users = new ConcurrentHashMap<>();
    private UserCache() {
        updateStatusListener = new UpdateUserProcessor() {
            @Override
            protected String getKeyByUpdateConstructor(int constructor) {
                if(constructor == TdApi.UpdateUserStatus.CONSTRUCTOR)
                    return Constants.USERCACHE_USER_STATUS_OBSERVER;
                else if(constructor == TdApi.UpdateUserBlocked.CONSTRUCTOR)
                    return Constants.USERCACHE_USER_BLOCK_OBSERVER;
                else if(constructor == TdApi.UpdateUser.CONSTRUCTOR)
                    return Constants.USERCACHE_USER_DATA_OBSERVER;
                return null;
            }

            @Override
            protected boolean shouldObserveUpdateUserStatus() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateUserBlocked() {
                return true;
            }

            @Override
            protected boolean shouldObserveUpdateUser() {
                return true;
            }

            @Override
            protected void processUpdateInBackground(TdApi.Update upd) {
                if(upd.getConstructor() == TdApi.UpdateUserStatus.CONSTRUCTOR)
                {
                    TdApi.UpdateUserStatus castUpd = (TdApi.UpdateUserStatus) upd;
                    synchronized(users)
                    {
                        if(users.containsKey(castUpd.userId))
                            users.get(castUpd.userId).updateStatus(castUpd.status);
                    }
                }
                else if(upd.getConstructor() == TdApi.UpdateUserBlocked.CONSTRUCTOR)
                {
                    TdApi.UpdateUserBlocked castUpd = (TdApi.UpdateUserBlocked) upd;
                    synchronized(users)
                    {
                        if(users.containsKey(castUpd.userId))
                            users.get(castUpd.userId).updateUserBlocked(castUpd.isBlocked);
                    }
                }
                else if(upd.getConstructor() == TdApi.UpdateUser.CONSTRUCTOR)
                {
                    TdApi.UpdateUser castUpd = (TdApi.UpdateUser) upd;
                    synchronized(users)
                    {
                        if(users.containsKey(castUpd.user.id))
                            users.get(castUpd.user.id).updateUser(castUpd.user);
                        else
                            cacheUser(castUpd.user.id);
                    }
                }

            }

            @Override
            protected void processUpdateInUiThread(TdApi.Update upd) {
            }
        };
    }
    public void attachObserver()
    {
        TelegramUpdatesHandler service = TelegramApplication.sharedApplication().getUpdateService();
        if(service == null)
        {
            NotificationCenter.getInstance().addObserver(new NotificationObserver(NotificationCenter.didUpdateServiceStarted) {
                @Override
                public void didNotificationReceived(Notification notification) {
                    TelegramUpdatesHandler serviceInstance = (TelegramUpdatesHandler) notification.getObject();
                    updateStatusListener.attachObservers(serviceInstance);
                }
            });
        }
        else
            updateStatusListener.attachObservers(service);
    }
    public static UserCache getInstance()
    {
        if(_instance == null)
        {
            synchronized(UserCache.class)
            {
                if(_instance == null)
                    _instance = new UserCache();
            }
        }
        return _instance;
    }
    public void clear()
    {
        synchronized(users)
        {
            users.clear();
        }
        updateStatusListener.removeObservers();
    }
    public UserModel getUserById(int id)
    {
        if(id == 0)
            return null;
        synchronized(users) {
            if (users.containsKey(id))
                return users.get(id);
        }
        final AtomicBoolean ready = new AtomicBoolean(false);
        TdApi.GetUser getUserFunc = new TdApi.GetUser(id);
        Client telegramClient = TG.getClientInstance();
        if(telegramClient == null)
            return null;
        telegramClient.send(getUserFunc, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(object == null || object.getConstructor() != TdApi.User.CONSTRUCTOR)
                    return;
                try
                {
                    UserModel u = new UserModel((TdApi.User) object);
                    synchronized(users)
                    {
                        users.put(u.getId(), u);
                    }
                }
                finally
                {
                    ready.set(true);
                    synchronized(monitor)
                    {
                        monitor.notifyAll();
                    }
                }
            }
        });
        while (!ready.get()) {
            synchronized(monitor)
            {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        synchronized(users)
        {
            return users.get(id);
        }
    }
    public UserModel getFullUserById(int id)
    {
        if(id == 0)
            return null;
        synchronized(users) {
            if (users.containsKey(id))
            {
                UserModel u = users.get(id);
                if(u.getBotInfo() != null)
                    return u;
            }
        }
        final AtomicBoolean ready = new AtomicBoolean(false);
        final AtomicBoolean failed = new AtomicBoolean(false);
        TdApi.GetUserFull getUserFunc = new TdApi.GetUserFull(id);
        Client telegramClient = TG.getClientInstance();
        if(telegramClient == null)
            return null;
        telegramClient.send(getUserFunc, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                try
                {
                    if(object == null || object.getConstructor() != TdApi.UserFull.CONSTRUCTOR)
                        failed.set(true);
                    else
                    {
                        synchronized(users)
                        {
                            UserModel cached = users.get(((TdApi.UserFull) object).user.id);
                            if(cached == null)
                            {
                                UserModel u = new UserModel((TdApi.UserFull) object);
                                users.put(u.getId(), u);
                            }
                            else
                                cached.addFullInfo((TdApi.UserFull) object);
                        }
                    }
                }
                finally
                {
                    ready.set(true);
                    synchronized(monitor)
                    {
                        monitor.notifyAll();
                    }
                }
            }
        });
        while (!ready.get()) {
            synchronized(monitor)
            {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        if(failed.get())
            return null;            //"Repeat till success" way leads to StackOverflow if baneed on server
        synchronized(users)
        {
            return users.get(id);
        }
    }
    public void cacheUser(final int id)
    {
        if(id == 0)
            return;
        synchronized(users)
        {
            if(users.containsKey(id))
                return;
        }
        TdApi.GetUser getUserFunc = new TdApi.GetUser(id);
        Client telegramClient = TG.getClientInstance();
        if(telegramClient == null)
            return;
        telegramClient.send(getUserFunc, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if(object == null || object.getConstructor() != TdApi.User.CONSTRUCTOR)
                    cacheUser(id);
                else
                {
                    UserModel u = new UserModel((TdApi.User) object);
                    synchronized(users)
                    {
                        if(!users.containsKey(u.getId()))
                            users.put(u.getId(), u);
                    }
                }
            }
        });
    }
    public void cacheUsers(TdApi.ChatParticipant[] participants)
    {
        for(TdApi.ChatParticipant p : participants)
            cacheUser(p.user.id);
    }
    public boolean isCached(int userId)
    {
        synchronized(users)
        {
            return users.containsKey(userId);
        }
    }
    public boolean isCachedFull(int userId)
    {
        synchronized(users)
        {
            if(!users.containsKey(userId))
                return false;
            UserModel user = users.get(userId);
            if(user.getBotInfo() == null)
                return false;
        }
        return true;
    }
    public boolean isCached(TdApi.User[] arr)
    {
        synchronized(users)
        {
            for(TdApi.User u : arr)
            {
                if(!users.containsKey(u.id))
                    return false;
            }
        }
        return true;
    }

}
