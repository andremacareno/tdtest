package com.andremacareno.tdtest;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Log;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tdtest.files.FileCache;
import com.andremacareno.tdtest.images.ImageCache;
import com.crashlytics.android.Crashlytics;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;

import io.fabric.sdk.android.Fabric;


/**
 * Created by andremacareno on 03/04/15.
 */
public class TelegramApplication extends Application {
    static {
        System.loadLibrary("webp");
        System.loadLibrary("opus_backport");
        System.loadLibrary("ffmpeg_backport");
    }
    public static boolean iWantToTest = true;
    private final String TAG = "TelegramApplication";
    private static volatile TelegramApplication sharedApp;

    //cache variables
    private volatile SharedPreferences prefs;
    private volatile TelegramUpdatesHandler updatesHandler;
    private boolean has_connection = false;
    private boolean tdlibReloaded = false;
    public static boolean testDc = false;
    private TdApi.SetOption connectionEstablishedFunction;
    private TdApi.SetOption connectionLostFunction;
    public static volatile Handler applicationHandler;
    public static final TdApi.ReplyMarkup dummyMarkup = new TdApi.ReplyMarkupNone();

    public static final Client.ResultHandler dummyResultHandler = new Client.ResultHandler() {
        @Override
        public void onResult(TdApi.TLObject object) {
        }
    };
    //network change receiver
    IntentFilter connectivityFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();

    public static TelegramApplication sharedApplication() {
        return sharedApp;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        //telegram client setting up
        File cacheDir = getApplicationContext().getExternalCacheDir() == null ? getApplicationContext().getCacheDir() : getApplicationContext().getExternalCacheDir();
        String pathToCache = cacheDir.getPath();
        TG.setDir(pathToCache);
        TG.setFilesDir(pathToCache);
        TG.setLogVerbosity(iWantToTest && BuildConfig.DEBUG ? 5 : 2);
        TG.setFileLogEnabled(iWantToTest && BuildConfig.DEBUG);
        TG.setUseTestDc(testDc && BuildConfig.DEBUG);
        connectionEstablishedFunction = new TdApi.SetOption(Constants.OPTION_NETWORK_UNREACHABLE, new TdApi.OptionBoolean(false));
        connectionLostFunction = new TdApi.SetOption(Constants.OPTION_NETWORK_UNREACHABLE, new TdApi.OptionBoolean(true));
        applicationHandler = new Handler(getMainLooper());
        UserCache.getInstance();
        FileCache.getInstance();
        sharedApp = this;
    }

    private class NetworkChangeReceiver extends BroadcastReceiver {
        public boolean isNetworkAvailable() {
            try {
                ConnectivityManager connectivityManager
                        = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                return activeNetworkInfo != null && activeNetworkInfo.isConnected() && activeNetworkInfo.isAvailable();
            } catch (NullPointerException e) {
                return true;
            }
        }

        @Override
        public void onReceive(final Context context, final Intent intent) {
            has_connection = isNetworkAvailable();
            Client telegramClient = TG.getClientInstance();
            if(has_connection) {
                if(telegramClient != null)
                    telegramClient.send(connectionEstablishedFunction, dummyResultHandler);
                NotificationCenter.getInstance().postNotification(NotificationCenter.didNetworkConnectionEstablished, null);
            }
            else {
                if(telegramClient != null)
                    telegramClient.send(connectionLostFunction, dummyResultHandler);
                NotificationCenter.getInstance().postNotification(NotificationCenter.didNetworkConnectionLost, null);
            }
        }
    }

    public SharedPreferences sharedPreferences() {
        if (prefs == null) {
            synchronized (SharedPreferences.class) {
                if (prefs == null)
                    prefs = getSharedPreferences(Constants.APP_PREFS, MODE_PRIVATE);
            }
        }
        return prefs;
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level >= TRIM_MEMORY_MODERATE) {
            if(BuildConfig.DEBUG)
                Log.d("TelegramApplication", "TRIM_MEMORY_MODERATE detected");
            tdlibReloaded = true;
            ImageCache.sharedInstance().evictAll();
            //writeThatTDLibMightBeReloaded();
        } else if (level >= TRIM_MEMORY_BACKGROUND) {
            if(BuildConfig.DEBUG)
                Log.d("TelegramApplication", "TRIM_MEMORY_BACKGROUND detected");
            //writeThatTDLibMightBeReloaded();
            ImageCache.sharedInstance().trimToSize(ImageCache.sharedInstance().size() / 2);
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        tdlibReloaded = true;
        ImageCache.sharedInstance().evictAll();
        //writeThatTDLibMightBeReloaded();
    }


    public boolean hasConnection() { return this.has_connection; }

    public void startUpdateService() {
        NotificationCenter.getInstance().postNotification(NotificationCenter.didUpdateServiceStarted, getUpdateService());
    }
    private void releaseUpdateService()
    {
        if (updatesHandler != null) {
            synchronized (TelegramUpdatesHandler.class) {
                if (updatesHandler != null) {
                    updatesHandler.removeAllObservers();
                    updatesHandler = null;
                }
            }
        }
    }
    public TelegramUpdatesHandler getUpdateService() {
        if (updatesHandler == null) {
            synchronized (TelegramUpdatesHandler.class) {
                if (updatesHandler == null) {
                    updatesHandler = new TelegramUpdatesHandler();
                    updatesHandler.start();
                }
            }
        }
        return updatesHandler;
    }

    public void stopConnectionChangeBroadcasting()
    {
        this.unregisterReceiver(networkChangeReceiver);
    }
    public void startConnectionChangeBroadcasting()
    {
        this.registerReceiver(networkChangeReceiver, connectivityFilter);
    }

    public void notifyAppBecameBackground()
    {
        SharedPreferences.Editor prefEdit = sharedPreferences().edit();
        prefEdit.putLong(Constants.PREFS_AFK_TIMESTAMP, System.currentTimeMillis());
        prefEdit.apply();
    }
    public long getBackgroundTimestamp()
    {
        return sharedPreferences().getLong(Constants.PREFS_AFK_TIMESTAMP, System.currentTimeMillis());
    }
    public AudioPlaybackService getAudioPlaybackService() { return AudioPlaybackService.getInstance(); }
    public boolean isTDLibReloaded() { return this.tdlibReloaded; }
    public void tdlibReloadingProcessed() {
        this.tdlibReloaded = false;
        SharedPreferences.Editor prefEdit = sharedPreferences().edit();
        prefEdit.remove(Constants.PREFS_TDLIB_RELOADED);
        prefEdit.apply();
    }
    public void readTDLibReloadingRecord()
    {
        this.tdlibReloaded = sharedPreferences().getBoolean(Constants.PREFS_TDLIB_RELOADED, false);
    }
    public void writeThatTDLibMightBeReloaded()
    {
        SharedPreferences.Editor prefEdit = sharedPreferences().edit();
        prefEdit.putBoolean(Constants.PREFS_TDLIB_RELOADED, true);
        prefEdit.apply();
    }
    public void confirmTDLibNotReloading()
    {
        this.tdlibReloaded = false;
        SharedPreferences.Editor prefEdit = sharedPreferences().edit();
        prefEdit.remove(Constants.PREFS_TDLIB_RELOADED);
        prefEdit.apply();
    }
    public void reInitTDLibClient()
    {
        TG.stopClient();
        File cacheDir = getApplicationContext().getExternalCacheDir() == null ? getApplicationContext().getCacheDir() : getApplicationContext().getExternalCacheDir();
        String pathToCache = cacheDir.getPath();
        TG.setDir(pathToCache);
        TG.setFilesDir(pathToCache);
        releaseUpdateService();
        AudioPlaybackService.stop();
    }
}
