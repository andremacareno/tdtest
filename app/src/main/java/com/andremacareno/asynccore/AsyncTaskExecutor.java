package com.andremacareno.asynccore;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Andrew on 09.06.2014.
 */
public class AsyncTaskExecutor {
    BlockingQueue<AsyncTaskManTask> tasks;
    CountDownLatch cdl;
    public volatile boolean paused = false;
    public final Object lockObject = new Object();
    public static final int maxThreads = Runtime.getRuntime().availableProcessors() == 1 ? 1 : Runtime.getRuntime().availableProcessors() - 1;
    public AsyncTaskExecutor()
    {
        tasks = new LinkedBlockingQueue<AsyncTaskManTask>();
        cdl = new CountDownLatch(maxThreads);
    }

    public void run()
    {
        for(int i = 0; !Thread.interrupted(); i++)
        {
            synchronized(lockObject)
            {
                if(paused)
                    try {
                        lockObject.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
            }
            try
            {
                if(i != 0 && (i % maxThreads) == 0) {
                    i = 0;
                    cdl.await();
                }

                AsyncTaskManTask task = tasks.take();
                WorkerThread wtr = new WorkerThread(task, cdl);
                wtr.start();
            }
            catch (InterruptedException e) {
            }
        }
    }
    public void addTask(AsyncTaskManTask task)
    {
        tasks.add(task);
    }
}
