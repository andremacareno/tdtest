package com.andremacareno.asynccore;

import android.os.Process;

/**
 * Created by Andrew on 09.06.2014.
 */
public class AsyncTaskManager extends Thread{
    private static volatile AsyncTaskManager _instance;
    private AsyncTaskExecutor executor;
    public AsyncTaskManager()
    {
        executor = new AsyncTaskExecutor();
    }

    @Override
    public void run()
    {
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
        executor.run();
    }
    public void addTask(AsyncTaskManTask t)
    {
        executor.addTask(t);
        if(!this.isAlive())
            this.start();
    }
    public void pauseThread()
    {
        executor.paused = true;
    }
    public void unpauseThread() {
        synchronized(executor.lockObject)
        {
            executor.lockObject.notifyAll();
        }
    }


    public static AsyncTaskManager sharedInstance()
    {
        if(_instance == null)
        {
            synchronized (AsyncTaskManager.class)
            {
                if(_instance == null)
                    _instance = new AsyncTaskManager();
            }
        }
        return _instance;
    }
    public static void stopManager()
    {
        if(_instance != null)
        {
            synchronized(AsyncTaskManager.class)
            {
                if(_instance != null)
                {
                    _instance.interrupt();
                    _instance.executor = null;
                    _instance = null;
                }
            }
        }
    }
}
