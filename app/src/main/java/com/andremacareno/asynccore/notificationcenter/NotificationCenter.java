package com.andremacareno.asynccore.notificationcenter;

import android.util.Log;

import com.andremacareno.tdtest.BuildConfig;
import com.andremacareno.tdtest.TelegramApplication;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by andremacareno on 24/04/15.
 */
public class NotificationCenter {
    private static final String TAG = "NotificationCenter";
    private static int totalEvents = 1;

    public static final int didMessageModelsGenerated = totalEvents++;
    public static final int didMessageModelGenerationFailed = totalEvents++;
    public static final int didFileDownloaded = totalEvents++;
    public static final int didCountryModelsGenerated = totalEvents++;
    public static final int didCountryModelGenerationFailed = totalEvents++;
    public static final int didLoggedOut = totalEvents++;
    public static final int didLogOutFailed = totalEvents++;
    public static final int didCountrySelected = totalEvents++;
    public static final int didImageCached = totalEvents++;
    public static final int didImageProcessed = totalEvents++;
    public static final int didImageProcessingFailed = totalEvents++;
    public static final int didAnimProcessed = totalEvents++;
    public static final int didAnimProcessingFailed = totalEvents++;
    public static final int didAlbumArtExtracted = totalEvents++;
    public static final int didAlbumArtExtractingFailed = totalEvents++;
    public static final int didPlaceholderGenerated = totalEvents++;
    public static final int didPlaceholderCreationFailed = totalEvents++;
    public static final int didNetworkConnectionEstablished = totalEvents++;
    public static final int didNetworkConnectionLost = totalEvents++;
    public static final int didContactsListViewItemClicked = totalEvents++;
    public static final int didChatsListViewItemClicked = totalEvents++;
    public static final int didImagePathRetrieved = totalEvents++;
    public static final int didImagePathRetrieveFailed = totalEvents++;
    public static final int didAudioPathRetrieved = totalEvents++;
    public static final int didAudioPathRetrieveFailed = totalEvents++;
    public static final int didFullSizedPhotoDownloaded = totalEvents++;
    public static final int didPhotoDrawableCreated = totalEvents++;
    public static final int didPhotoDrawableCreationFailed = totalEvents++;
    public static final int didNavDrawerActionSelected = totalEvents++;
    public static final int didNavDrawerClosed = totalEvents++;
    public static final int didChatInfoReceived = totalEvents++;
    public static final int didUpdateServiceStarted = totalEvents++;
    public static final int didAudioServiceStarted = totalEvents++;
    public static final int didEnteredInConfirmationMode = totalEvents++;
    public static final int didPasscodeSet = totalEvents++;
    public static final int didPasscodeUnset = totalEvents++;
    public static final int didScreenStateChanged = totalEvents++;
    public static final int didPasscodeLockPassed = totalEvents++;
    public static final int didChatModelsGenerated = totalEvents++;
    public static final int didChatModelGeneratingFailed = totalEvents++;
    public static final int didTopMessageUpdated = totalEvents++;
    public static final int didTopMessageUpdateFailed = totalEvents++;
    public static final int didSupergroupRequestedByLink = totalEvents++;
    public static final int didChannelProfileRequested = totalEvents++;
    public static final int didViewProfileByLinkRequested = totalEvents++;
    public static final int didChatParticipantsListItemClicked = totalEvents++;
    public static final int didUserFullForChatScreenReceived = totalEvents++;
    public static final int didGroupFullReceived = totalEvents++;
    public static final int didChannelFullForChatScreenReceived = totalEvents++;
    public static final int didEmojiLoaded = totalEvents++;
    public static final int didStickersLoaded = totalEvents++;
    public static final int didSavedAnimationsLoaded = totalEvents++;
    public static final int didSavedAnimationsUpdated = totalEvents++;
    public static final int didStickersUpdated = totalEvents++;
    public static final int didRemoveParticipantWindowRequested = totalEvents++;
    public static final int didBotCommandProcessed = totalEvents++;
    public static final int didRecentFromGalleryLoaded = totalEvents++;
    public static final int didGalleryPhotoThumbnailCached = totalEvents++;
    public static final int didAvatarPreviewCached = totalEvents++;
    public static final int didQuickSendModelsCreated = totalEvents++;
    public static final int didSharedImageModelsCreated = totalEvents++;
    public static final int didPasscodeLockScreenShown = totalEvents++;
    public static final int didNewChatParticipantsLoaded = totalEvents++;
    public static final int didChatParticipantsLoaded = totalEvents++;
    public static final int didChatAdminsCounted = totalEvents++;
    public static final int didNewChannelMemberRequested = totalEvents++;
    public static final int didPlaylistLoaded = totalEvents++;
    public static final int didChannelDescriptionUpdated = totalEvents++;
    public static final int didChatMessageRemovedBySelf = totalEvents++;

    private ConcurrentHashMap<Integer, CopyOnWriteArrayList<NotificationObserver>> observers = new ConcurrentHashMap<Integer, CopyOnWriteArrayList<NotificationObserver>>();
    private static volatile NotificationCenter _instance;

    private NotificationCenter() {}
    public static NotificationCenter getInstance()
    {
        if(_instance == null)
        {
            synchronized(NotificationCenter.class)
            {
                if(_instance == null)
                    _instance = new NotificationCenter();
            }
        }
        return _instance;
    }
    public static void stop()
    {
        if(_instance != null)
        {
            synchronized(NotificationCenter.class)
            {
                if(_instance != null) {
                    _instance.removeAllObservers();
                    _instance = null;
                }
            }
        }
    }

    public void postNotification(int id, Object obj)
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "broadcasting…");
        Notification notification = new Notification(id, obj);
        CopyOnWriteArrayList<NotificationObserver> observersOfId = observers.get(id);
        if(observersOfId == null)
            return;
        for(NotificationObserver obs : observersOfId)
        {
            obs.didNotificationReceived(notification);
        }
    }
    public void postNotificationDelayed(final int id, final Object obj, long delay)
    {
        TelegramApplication.applicationHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                postNotification(id, obj);
            }
        }, delay);
    }

    public void addObserver(NotificationObserver obs) {
        if(obs == null)
            return;
        int id = obs.getId();
        CopyOnWriteArrayList<NotificationObserver> idObservers = observers.get(id);
        if(idObservers == null)
            observers.put(id, (idObservers = new CopyOnWriteArrayList<NotificationObserver>()));
        idObservers.add(obs);
    }

    public void removeObserver(NotificationObserver obs) {
        if(obs == null)
            return;
        int id = obs.getId();
        CopyOnWriteArrayList<NotificationObserver> idObservers = observers.get(id);
        if(idObservers != null)
        {
            idObservers.remove(obs);
            if(idObservers.size() == 0)
                observers.remove(id);
        }
    }
    public void removeAllObservers()
    {
        for(Integer i : observers.keySet())
            observers.get(i).clear();
        observers.clear();
    }

}
