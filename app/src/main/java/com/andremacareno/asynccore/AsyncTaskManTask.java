package com.andremacareno.asynccore;

/**
 * Created by Andrew on 09.06.2014.
 */
public abstract class AsyncTaskManTask {

    protected OnTaskCompleteListener onTaskCompleteListener = null;
    protected OnTaskFailureListener onTaskFailureListener = null;

    public Object getNotificationObject() { return null; }
    public abstract String getTaskKey();
    public abstract int getTaskFailedNotificationId();
    public abstract int getTaskCompletedNotificationId();
    public void onComplete(OnTaskCompleteListener l)
    {
        this.onTaskCompleteListener = l;
    }
    public void onFailure(OnTaskFailureListener l)
    {
        this.onTaskFailureListener = l;
    }

    public void addToQueue()
    {
        AsyncTaskManager.sharedInstance().addTask(this);
    }
    protected abstract void work() throws Exception;
    public OnTaskCompleteListener getTaskCompleteListener() { return this.onTaskCompleteListener; }
    public OnTaskFailureListener getTaskFailureListener() { return this.onTaskFailureListener; }
}
