package com.andremacareno.asynccore;

import android.os.Process;

import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Andrew on 09.06.2014.
 */
public class WorkerThread extends Thread {
    AsyncTaskManTask task;
    CountDownLatch cdl;
    public WorkerThread(AsyncTaskManTask t, CountDownLatch c)
    {
        this.task = t;
        this.cdl = c;
    }
    @Override
    public void run()
    {
        android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_LOWEST);
        //Looper.prepare();
        NotificationObserver taskCompleteObserver, taskFailObserver;
        taskCompleteObserver = taskFailObserver = null;
        if(task.getTaskCompleteListener() != null && task.getTaskCompletedNotificationId() != 0) {
            taskCompleteObserver = new NotificationObserver(task.getTaskCompletedNotificationId(), task.getTaskKey()) {
                @Override
                public void didNotificationReceived(Notification notification) {
                    NotificationCenter.getInstance().removeObserver(this);
                    task.getTaskCompleteListener().taskComplete(task);
                }
            };
            NotificationCenter.getInstance().addObserver(taskCompleteObserver);
        }
        if(task.getTaskFailureListener() != null && task.getTaskFailedNotificationId() != 0) {
            taskFailObserver = new NotificationObserver(task.getTaskFailedNotificationId(), task.getTaskKey()) {
                @Override
                public void didNotificationReceived(Notification notification) {
                    task.getTaskFailureListener().taskFailed(task);
                    NotificationCenter.getInstance().removeObserver(this);
                }
            };
            NotificationCenter.getInstance().addObserver(taskFailObserver);
        }
        boolean failed = false;
        try
        {
            task.work();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            failed = true;
        }
        if(failed && task.getTaskFailedNotificationId() != 0)
            NotificationCenter.getInstance().postNotification(task.getTaskFailedNotificationId(), task.getNotificationObject());
        else if(task.getTaskCompletedNotificationId() != 0)
            NotificationCenter.getInstance().postNotification(task.getTaskCompletedNotificationId(), task.getNotificationObject());
        if(taskFailObserver != null)
            NotificationCenter.getInstance().removeObserver(taskFailObserver);
        if(taskCompleteObserver != null)
            NotificationCenter.getInstance().removeObserver(taskCompleteObserver);
        cdl.countDown();
        //Looper.loop();
    }
}
