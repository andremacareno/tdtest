package com.andremacareno.asynccore;

/**
 * Created by andremacareno on 19/03/15.
 */
public interface OnTaskFailureListener {
    void taskFailed(AsyncTaskManTask task);
}
